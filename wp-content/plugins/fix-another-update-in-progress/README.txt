=== Fix Another Update In Progress ===
Contributors: raviry
Tags: fix another update, wordpress another update is already in progress
Requires at least: 4.3
Tested up to: 4.8
Stable tag: trunk
License: GPL v3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

Get rid of WordPress another update is already in progress

== Description ==

Sometimes though there is no update in progress, you might unable to update your WordPress site to the newest version of WordPress because you got a message like "Another update is currently in progress". It is an automatic lock state to prevent WordPress from simultaneous core updates. So if you want to get rid of that message and upgrade your WordPress site without waiting, then this plugin is for you.
If you want to do it by yourself then <a href="https://www.proy.info/wordpress-another-update-is-currently-in-progress/">Read more..</a>

== Installation ==

1. Upload the fix-another-update-in-progress folder to the /wp-content/plugins/ directory
2. Activate the Fix Another Update In Progress plugin through the 'Plugins' menu in WordPress
3. Navigate to Settings - Fix Another Update In Progress on the menu.