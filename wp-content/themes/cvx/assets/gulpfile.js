var gulp = require("gulp")
var postcss = require("gulp-postcss")
var autoprefixer = require("autoprefixer")
var sass = require("gulp-sass")
var sourcemaps = require('gulp-sourcemaps')
var concat = require("gulp-concat")
var browserify = require("browserify")
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var babelify = require('babelify');
var watch = require("gulp-watch")
var util = require("gulp-util")
var uglify = require("gulp-uglify")
var gulpif = require("gulp-if")
var clean = require('gulp-clean')
var runSequence = require("run-sequence")


gulp.task("styles", () => compileCss({
    outputStyle: "nested",
    destination: "./../",
}))

gulp.task("scripts", () => compileJs({
    prod: false,
    destination: "./../js"
}))

gulp.task("styles-prod", () => compileCss({
    outputStyle: "compressed",
    destination: "./dist/css"
}))

gulp.task("scripts-prod", () => compileJs({
    prod: true,
    destination: "./dist/js"
}))

gulp.task("watch", () => {
    gulp.watch("./src/scss/**/*.scss", gulp.series("styles"))
    gulp.watch("./src/js/**/*.js", gulp.series("scripts"))
})

gulp.task('clean', () => {
    return gulp.src('./dist', {read: false})
    .pipe(clean({force: true}))
})

gulp.task("build", () => {
    runSequence("clean", "styles-prod", "scripts-prod")
})

gulp.task("build:vendor", () => compileVendor({
    prod: true,
    destination: "./../dist/js"
}))

function compileCss(options) {
    return gulp.src(["./src/scss/index.scss"])
        .pipe(sourcemaps.init())
        .pipe(sass.sync({
            outputStyle: options.outputStyle
        }).on("error", sass.logError))
        .pipe(concat("style.css"))
        .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest("./../"))
}

function compileJs(options) {
    return browserify({
        entries: ["./src/js/index.js"],
        debug: true,
        transform: [babelify.configure({
            presets: ["@babel/env"]
        })]})
        .bundle()
        .pipe(source("script.js"))
        .pipe(buffer())
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(gulpif(options.prod, uglify()))
        .on("error", util.log)
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest("./../js"))
}

function compileVendor(options) {
    return browserify({
        entries: ["./js/vendor.js"],
        debug: true,
        transform: [babelify.configure({
            presets: ["@babel/env"]
        })]})
    
        .bundle()
        .pipe(source("vendor.js"))
        .pipe(buffer())
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        // Add other gulp transformations (eg. uglify) to the pipeline here.
        .pipe(gulpif(options.prod, uglify()))
        .on("error", util.log)
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(options.destination))
}
