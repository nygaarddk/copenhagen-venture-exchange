 // Add slick carousel
 $('.slick-carousel .elementor-row').slick({
    arrows: false,
    slidesToShow: 4,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
        {
          breakpoint: 1024,
          variableWidth: true,
          settings: {
            arrows: false,
            centerMode: true,
            slidesToShow: 1,
            infinite: false
          }
        },
    ]
})