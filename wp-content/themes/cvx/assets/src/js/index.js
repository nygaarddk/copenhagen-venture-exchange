import "./_jquery"
import * as ko from "knockout"
import "slick-carousel"

class App {
    constructor() {
        
        this.scrolledTop = ko.observable(0)
        this.menuOpen = ko.observable(false)
        this.membershiptype = ko.observable('monthly')
        
        this.loading = ko.observable(false)

        this.firstname = ko.observable("")
        this.lastname = ko.observable("")
        this.email = ko.observable("")
        this.phone = ko.observable("")
        this.jobtitle = ko.observable("")
        this.company = ko.observable("")
        this.message = ko.observable("")

        this.toc = ko.observable(false)

        this.membership = ko.observable("")

        this.formSubmitted = ko.observable(false)
        this.formError = ko.observable(false)
        this.formErrorMessage = ko.observable("")

        // Eventlistener
        $(window).on('scroll', () => this.scrolledTop($(window).scrollTop()))
        this.scrolledTop($(window).scrollTop())

        // Membership type
        $(".js-membership-elite .elementor-button-text").on("click", () => this.membership("membership-elite"))
        $(".js-membership-pro .elementor-button-text").on("click", () => this.membership("membership-pro"))
        $(".js-membership-starter .elementor-button-text").on("click", () => this.membership("membership-starter"))

        $(window).on('submit', () => {
            this.loading(true)
            
        })

        $(window).on('wpcf7mailsent', () => {
            $('.form__wrapper').remove();
            this.formSubmitted(true)
        })

        $(window).on('wpcf7invalid', () => {
            this.loading(false)
        })


        this.formSubmitted.subscribe(() => {
            
            if ($(".modal.fade.in").length > 0) {
                $('.modal .hide-on-form-submit').hide()
                $('.modal .show-on-form-submit').show()
            } else {
                $('.hide-on-form-submit').not('.modal .hide-on-form-submit').hide()
                $('.show-on-form-submit').not('.modal .show-on-form-submit').show()
            }

            setTimeout(() => {
                this.formSubmitted(false)
            }, 7000)
        })
    }

    setLoading() {
        this.loading(true)
    }

    toggleMenu() {
        this.menuOpen(!this.menuOpen())
    }

    submit(url, listid) {
        this.loading(true)
        
        var info = {
            listid: listid,
            firstname: this.firstname(),
            lastname: this.lastname(),
            email: this.email(),
            phone: this.phone(),
            jobtitle: this.jobtitle(),
            company: this.company()
        }

        if (listid == 2) {
            info['tags'] = this.membership() + "," + this.membershiptype()
        }

        form(url, info)
        .then(() => {
            this.formSubmitted(true)
        })

        .catch((response) => {
            this.formErrorMessage(response)
            this.formError(true)
        })

        .finally((response) => {
            this.loading(false)
        })
    }
}

let form = function(url, info) {
    return new Promise((resolve, reject) => {
        let newXHR = new XMLHttpRequest()

        newXHR.addEventListener('load', (e) => {
            if (e.currentTarget.response === "SUCCESS") {
                resolve()
            } else {
                reject(e.currentTarget.response)
            }
        })

        newXHR.open('POST', url)
        
        let data = new FormData()
        
        Object.keys(info).forEach((obj) => {
            data.append(obj, info[obj])
        })

        newXHR.send(data)
    })
}


ko.applyBindings(new App())