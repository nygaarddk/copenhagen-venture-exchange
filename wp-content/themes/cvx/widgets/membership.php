<?php
namespace Elementor; 

if ( ! defined( 'ABSPATH' ) ) exit; 

class Membership extends Widget_Base {
    
	public function get_name() {
		return __( 'membership', 'cvx-widgets' );
	}
	
	public function get_title() {
		return __( 'Membership', 'cvx-widgets' );
	}

    public function get_icon() {
      return 'eicon-wordpress';
    }

	protected function _register_controls() {
		
		$this->start_controls_section(
			'membership', // Section key
			[
				'label' => __( 'Settings', 'cvx-widgets' ), // Section display name
				'type' => Controls_Manager::SECTION, 
				'tab' => Controls_Manager::TAB_CONTENT, // Which tab to display the section in.
			]
        );
        
        $this->add_control(
			'highlight',
			[
				'label' => __( 'Highlighted', 'cvx' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'cvx' ),
				'label_off' => __( 'No', 'cvx' ),
				'return_value' => 'yes',
				'default' => 'no',
			]
        );
        
        $this->add_control(
			'highlight-text', // Control key
			[
				'label' => __( 'Highlight Text', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
        );

		$this->add_control(
			'name', // Control key
			[
				'label' => __( 'Name', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
        );
        
        $this->add_control(
			'price-text-month', // Control key
			[
				'label' => __( 'Price Text (Month)', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
        );
        
        $this->add_control(
			'price-month', // Control key
			[
				'label' => __( 'Price (Month)', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
        );

        $this->add_control(
			'price-text-year', // Control key
			[
				'label' => __( 'Price Text (Year)', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
        );
        
        $this->add_control(
			'price-year', // Control key
			[
				'label' => __( 'Price (Year)', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
		);

        $this->add_control(
			'includes', // Control key
			[
				'label' => __( 'Membership Includes', 'cvx' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'default' => '',
			]
		);
		
		$this->end_controls_section();
	}
	
	protected function render() {
		$sample_schedule = $this->get_settings();
		
		include( get_template_directory() . '/widgets/templates/membership-template.php' );
	}
	
	public function get_categories() {
		return [ 'cvx-widgets' ];
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Membership() );