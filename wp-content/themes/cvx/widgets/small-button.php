<?php
namespace Elementor; 

if ( ! defined( 'ABSPATH' ) ) exit; 

class SmallButton extends Widget_Base {
    
	public function get_name() {
		return __( 'small-button', 'cvx-widgets' );
	}
	
	public function get_title() {
		return __( 'Small Button', 'cvx-widgets' );
	}

    public function get_icon() {
      return 'eicon-wordpress';
    }

	protected function _register_controls() {
		
		$this->start_controls_section(
			'small-button', // Section key
			[
				'label' => __( 'Settings', 'cvx-widgets' ), // Section display name
				'type' => Controls_Manager::SECTION, 
				'tab' => Controls_Manager::TAB_CONTENT, // Which tab to display the section in.
			]
		);

		$this->add_control(
			'type',
			[
				'label' => __( 'Type', 'cvx' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Primary', 'cvx' ),
				'label_off' => __( 'Secondary', 'cvx' ),
				'return_value' => 'yes',
				'default' => 'no',
			]
        );

		$this->add_control(
			'button-text', // Control key
			[
				'label' => __( 'Button Text', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
		);

		$this->add_control(
			'url', // Control key
			[
				'label' => __( 'URL', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::URL, // Type of control
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'elementor' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor' ),
						'icon' => 'fa fa-align-right',
					]
				]
			]
		);

		$this->end_controls_section();
	}
	
	protected function render() {
		$sample_schedule = $this->get_settings();
		
		include( get_template_directory() . '/widgets/templates/small-button-template.php' );
	}
	
	public function get_categories() {
		return [ 'cvx-widgets' ];
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\SmallButton() );