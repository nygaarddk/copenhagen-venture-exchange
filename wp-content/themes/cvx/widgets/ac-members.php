<?php
namespace Elementor; 

if ( ! defined( 'ABSPATH' ) ) exit; 

class ACFormMembers extends Widget_Base {
    
	public function get_name() {
		return __( 'ac-form-members', 'cvx-widgets' );
	}
	
	public function get_title() {
		return __( 'Active Campaign Members Form', 'cvx-widgets' );
	}

    public function get_icon() {
        return 'eicon-wordpress';
    }

	protected function _register_controls() {
		
		$this->start_controls_section(
			'settings', // Section key
			[
				'label' => __( 'Settings', 'cvx-widgets' ), // Section display name
				'type' => Controls_Manager::SECTION, 
				'tab' => Controls_Manager::TAB_CONTENT, // Which tab to display the section in.
			]
		);
		
		$this->add_control(
			'firstname', // Control key
			[
				'label' => __( 'First Name', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
        );
        
        $this->add_control(
			'lastname', // Control key
			[
				'label' => __( 'Last Name', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
		);
		
		$this->add_control(
			'email', // Control key
			[
				'label' => __( 'Email', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
        );
        
        $this->add_control(
			'jobtitle', // Control key
			[
				'label' => __( 'Job Title', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
		);
		
		$this->add_control(
			'phone', // Control key
			[
				'label' => __( 'Phone', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
        );
        
        $this->add_control(
			'company', // Control key
			[
				'label' => __( 'Company', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
		);
		
		$this->add_control(
			'submit', // Control key
			[
				'label' => __( 'Submit Button', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
		);
	
		$this->add_control(
			'toc', // Control key
			[
				'label' => __( 'TOC Text', 'cvx' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'default' => '',
			]
		);
		
		$this->end_controls_section();
	}
	
	protected function render() {
		$sample_schedule = $this->get_settings();
		
		include( get_template_directory() . '/widgets/templates/ac-members-template.php' );
	}
	
	public function get_categories() {
		return [ 'cvx-widgets' ];
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\ACFormMembers() );