<?php
namespace Elementor; 

if ( ! defined( 'ABSPATH' ) ) exit; 

class ListItem extends Widget_Base {
    
	public function get_name() {
		return __( 'list-item', 'cvx-widgets' );
	}
	
	public function get_title() {
		return __( 'List Item', 'cvx-widgets' );
	}

    public function get_icon() {
      return 'eicon-wordpress';
    }

	protected function _register_controls() {
		
		$this->start_controls_section(
			'list-item', // Section key
			[
				'label' => __( 'Settings', 'cvx-widgets' ), // Section display name
				'type' => Controls_Manager::SECTION, 
				'tab' => Controls_Manager::TAB_CONTENT, // Which tab to display the section in.
			]
        );
        
        $this->add_control(
			'item-number', // Control key
			[
				'label' => __( 'Item Number', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::NUMBER, // Type of control
				'default' => '', // Default value for control
			]
        );

        $this->add_control(
			'item-text', // Control key
			[
				'label' => __( 'Item Text', 'cvx' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'default' => '',
			]
		);
		
		$this->end_controls_section();
	}
	
	protected function render() {
		$sample_schedule = $this->get_settings();
		
		include( get_template_directory() . '/widgets/templates/list-item-template.php' );
	}
	
	public function get_categories() {
		return [ 'cvx-widgets' ];
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\ListItem() );