<?php
namespace Elementor; 

if ( ! defined( 'ABSPATH' ) ) exit; 

class BoardMember extends Widget_Base {
    
	public function get_name() {
		return __( 'board-member', 'cvx-widgets' );
	}
	
	public function get_title() {
		return __( 'Board Member', 'cvx-widgets' );
	}

    public function get_icon() {
        return 'eicon-wordpress';
    }

	protected function _register_controls() {
		
		$this->start_controls_section(
			'board-member', // Section key
			[
				'label' => __( 'Settings', 'cvx-widgets' ), // Section display name
				'type' => Controls_Manager::SECTION, 
				'tab' => Controls_Manager::TAB_CONTENT, // Which tab to display the section in.
			]
        );

        $this->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
        
        $this->add_control(
			'name', // Control key
			[
				'label' => __( 'Name', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
        );

        $this->add_control(
			'title', // Control key
			[
				'label' => __( 'Title', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::TEXT, // Type of control
				'default' => '', // Default value for control
			]
        );

        $this->add_control(
			'linkedin', // Control key
			[
				'label' => __( 'LinkedIn URL', 'cvx-widgets' ), // Control label
				'type' => Controls_Manager::URL, // Type of control
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
        );
        

		
		$this->end_controls_section();
	}
	
	protected function render() {
		$sample_schedule = $this->get_settings();
		
		include( get_template_directory() . '/widgets/templates/board-member-template.php' );
	}
	
	public function get_categories() {
		return [ 'cvx-widgets' ];
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\BoardMember() );