<?php
    $settings = $this->get_settings_for_display();
    $target = $settings['linkedin']['is_external'] ? ' target="_blank"' : '';
    $nofollow = $settings['linkedin']['nofollow'] ? ' rel="nofollow"' : '';
?>

<div class="board-member">
    <div class="board-member__image">
        <?php echo \Elementor\Group_Control_Image_Size::get_attachment_image_html( $settings ); ?>
    </div>
    <div class="board-member__info-wrapper">
        <a class="linkedin-icon" href="<?php echo $settings['linkedin']['url'] ?>" <?php echo $target, $nofollow ?>></a>
        <div class="board-member__info">
            <p class="board-member__name"><?php echo $settings['name'] ?></p>
            <p class="board-member__title"><?php echo $settings['title'] ?></p>
        </div>
    </div>
</div>