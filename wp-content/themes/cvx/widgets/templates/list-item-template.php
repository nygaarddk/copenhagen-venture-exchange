<?php
    $settings = $this->get_settings_for_display();
?>

<div class="list-item">
    <div class="list-item__number">
        <?php echo $settings['item-number'] ?>    
    </div>
    <div class="list-item__text">
        <?php echo $settings['item-text'] ?>
    </div>
</div>