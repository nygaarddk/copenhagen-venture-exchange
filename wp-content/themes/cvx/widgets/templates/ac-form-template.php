<?php 
	$ac = get_template_directory_uri() . "/ac/sync.php"; 
	$settings = $this->get_settings_for_display();
?>

<form class="form hide-on-form-submit" data-bind="submit: function() { submit('<?php echo $ac ?>', 3) }">
	<div class="form__wrapper">
		<input placeholder="<?php echo $settings['firstname'] ?>" type="text" class="input" required data-bind="value: firstname" />
		<input placeholder="<?php echo $settings['lastname'] ?>" type="text" class="input" required data-bind="value: lastname" />
		<input placeholder="<?php echo $settings['email'] ?>" type="email" class="input" required data-bind="value: email" />
		<input placeholder="<?php echo $settings['phone'] ?>" type="phone" class="input" data-bind="value: phone" />
		<input placeholder="<?php echo $settings['jobtitle'] ?>" type="text" class="input" required data-bind="value: jobtitle" />
		<input placeholder="<?php echo $settings['company'] ?>" type="text" class="input" required data-bind="value: company" />
		<button class="button button--primary" type="submit" data-bind="disable: loading, css: { 'button--loading': loading }"><?php echo $settings['submit'] ?></button>
	</div>
    <div style="display: none" data-bind="visible: formError">
        <p data-bind="text: formErrorMessage" class="form__message form__message--error">Error</p>
    </div>
</form>