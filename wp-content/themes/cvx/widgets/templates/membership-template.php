<?php
    $settings = $this->get_settings_for_display();
    $highlight = $settings['highlight'] === 'yes' ? 'membership--highlight' : '';
?>

<div class="membership <?php echo $highlight ?>">
    <h2 class="membership__name"><?php echo $settings['name'] ?></h3>
    
    <?php if ($settings['highlight'] === 'yes') { ?>
        <p class="membership__highlight-text"><?php echo $settings['highlight-text'] ?></p>
    <?php } ?>
    
    <div class="membership__info-wrapper">
        <!-- ko if: membershiptype() === 'monthly' -->
        <p class="membership__price-text"><?php echo $settings['price-text-month'] ?></p>
        <p class="membership__price"><?php echo $settings['price-month'] ?></p>
        <!-- /ko -->

        <!-- ko if: membershiptype() === 'yearly' -->
        <p class="membership__price-text"><?php echo $settings['price-text-year'] ?></p>
        <p class="membership__price"><?php echo $settings['price-year'] ?></p>
        <!-- /ko -->

        <div class="membership__includes"><?php echo $settings['includes'] ?></div>
    </div>

</div>