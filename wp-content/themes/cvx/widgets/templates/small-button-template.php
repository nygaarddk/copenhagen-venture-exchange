<?php
    $settings = $this->get_settings_for_display();
    $target = $settings['url']['is_external'] ? ' target="_blank"' : '';
    $nofollow = $settings['url']['nofollow'] ? ' rel="nofollow"' : '';
    $buttontype = $settings['type'] === 'yes' ? 'button--primary' : 'button--secondary';
?>

<div class="button-wrapper align-<?php echo $settings["align"] ?>">
    <a class="button <?php echo $buttontype ?>" href="<?php echo $settings['url']['url'] ?>" <?php echo $target, $nofollow ?>><?php echo $settings["button-text"] ?></a>
</div>