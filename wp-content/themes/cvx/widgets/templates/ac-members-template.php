<?php 
	$ac = get_template_directory_uri() . "/ac/sync.php"; 
	$settings = $this->get_settings_for_display();
?>

<form class="form hide-on-form-submit" data-bind="submit: function() { submit('<?php echo $ac ?>', 2) }">
    <div class="form__wrapper">
        <input placeholder="<?php echo $settings['firstname'] ?>" type="text" class="input" required data-bind="value: firstname" />
        <input placeholder="<?php echo $settings['lastname'] ?>" type="text" class="input" required data-bind="value: lastname" />
        <input placeholder="<?php echo $settings['email'] ?>" type="email" class="input" required data-bind="value: email" />
        <input placeholder="<?php echo $settings['phone'] ?>" type="phone" class="input" required data-bind="value: phone" />
        <input placeholder="<?php echo $settings['company'] ?>" type="text" class="input" required data-bind="value: company" />
        <input placeholder="<?php echo $settings['jobtitle'] ?>" type="text" class="input" required data-bind="value: jobtitle" />
        <label for="toc" class="checkbox">
            <input id="toc" type="checkbox" value="toc" data-bind="checked: toc"><?php echo $settings['toc'] ?>
        </label>
        <button class="button button--primary" type="submit" data-bind="disable: !toc() || loading, css: { 'button--loading': loading }"><?php echo $settings['submit'] ?></button>
    </div>
    <div style="display: none" data-bind="visible: formError">
        <p data-bind="text: formErrorMessage" class="form__message form__message--error">Error</p>
    </div>
</form>