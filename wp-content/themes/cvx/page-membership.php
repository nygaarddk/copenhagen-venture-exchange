<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Copenhagen_Venture_Exchange
 * 
 * Template Name: Membership Page
 * 
 */

get_header();
?>

	<header class="header">
		<div class="header__hero">
			<?php the_post_thumbnail( 'full', array( 'class' => 'alignleft' ) ); ?>
		</div>
		<div class="header__text-wrapper content">
			<?php the_title( '<h1 class="header__heading large-heading">', '</h1>' ); ?>
			<?php if (has_excerpt( $post->ID )) { ?>
				<p class="header__text"><?php echo get_the_excerpt(); ?></p>
			<?php } ?>
		</div>
		<div class="header__button-wrapper">
			<?php if (get_theme_mod('global-button-header') == "1" && get_page_template_slug() != 'page-membership.php') { ?>
				<a href="<?php echo esc_url( get_permalink(get_theme_mod('global-button-url'))); ?>" class="button button--primary"><?php echo get_theme_mod('global-button-text'); ?></a>
			<?php } ?>
			<?php if (get_page_template_slug() === 'page-membership.php') { ?>
				<div class="radio-buttons">
					<div class="radio-buttons__single">
						<input type="radio" name="radio" id="monthly" value="monthly" data-bind="checked: membershiptype">
						<label for="monthly">Månedlig</label>
					</div>
					<div class="radio-buttons__single">
						<input type="radio" name="radio" id="yearly" value="yearly" data-bind="checked: membershiptype">
						<label for="yearly">Årlig</label>
					</div>
				</div>
			<?php } ?>
		</div>
	</header>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			

			get_template_part( 'template-parts/content', 'page' );

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
