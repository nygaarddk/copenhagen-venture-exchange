<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Copenhagen_Venture_Exchange
 */

get_header();
?>

	<header class="header">
		<div class="header__hero">
			<?php the_post_thumbnail( 'full', array( 'class' => 'alignleft' ) ); ?>
		</div>
		<div class="header__text-wrapper content">
			<?php the_title( '<h1 class="header__heading large-heading">', '</h1>' ); ?>
			<?php if (has_excerpt( $post->ID )) { ?>
				<p class="header__text"><?php echo get_the_excerpt(); ?></p>
			<?php } ?>
		</div>
		<div class="header__button-wrapper">
			
			<?php 
				$link = get_field('header_button_url');

				if( $link ) {
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
						<a class="button button--primary" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
					<?php
				}
			?>
			
			
		
		</div>
	</header>
	
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			

			get_template_part( 'template-parts/content', 'page' );

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
