<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Copenhagen_Venture_Exchange
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Roboto:300,400,500,700" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> style="background-color: <?php echo get_theme_mod('background-color', '#000000'); ?>">

<div class="wrapper" data-bind="css: { 'wrapper--menu-open': menuOpen }">

	<div class="top-menu" data-bind="css: { 'top-menu--small' : scrolledTop() > 100 }">
		<div class="top-menu__bg-color" style="background-color: <?php echo get_theme_mod('menu-color', '#000000'); ?>"></div>
		<div class="top-menu__logo display-ipad-up"><?php the_custom_logo(); ?></div>
		<div class="top-menu__logo display-mobile">
			<a href="<?php echo home_url() ?>" class="custom-logo-link" rel="home" itemprop="url">
				<img src="<?php echo get_theme_mod( 'mobile-logo' ) ; ?>" class="custom-logo" alt="Copenhagen Venture Exchange" itemprop="logo">
			</a>
		</div>
		<?php wp_nav_menu( array(
			'theme_location'  => 'menu-1',
			'menu_id'         => 'primary-menu',
			'container'		  => 'nav',
			'container_class'      => 'navigation',
		) ); ?>
		<div class="top-menu__button-wrapper">
			<?php if (get_theme_mod('global-button-menu') == "1") { ?>
				<a href="<?php echo esc_url( get_permalink(get_theme_mod('global-button-url'))); ?>" class="button button--primary"><?php echo get_theme_mod('global-button-text'); ?></a>
			<?php } ?>
		</div>
		<div class="top-menu__hamburger" data-bind="click: toggleMenu">
			<div class="hamburger-menu" data-bind="css: { 'hamburger-menu--open': menuOpen }"><span></span><span></span><span></span></div>
		</div>
	</div>

	<div class="mobile-menu" data-bind="css: { 'mobile-menu--open': menuOpen }">
		<div class="mobile-menu__logo">
			<a href="<?php echo home_url() ?>" class="custom-logo-link" rel="home" itemprop="url">
				<img src="<?php echo get_theme_mod( 'mobile-logo' ) ; ?>" class="custom-logo" alt="Copenhagen Venture Exchange" itemprop="logo">
			</a>
		</div>
		
		<?php wp_nav_menu( array(
			'theme_location'  => 'menu-1',
			'menu_id'         => 'primary-menu',
			'container'		  => 'nav',
			'container_class' => 'navigation',
		) ); ?>

	</div>

	<div class="black-overlay" data-bind="click: toggleMenu"></div>

	<div class="page-wrapper">
		<div id="content" class="site-content">
