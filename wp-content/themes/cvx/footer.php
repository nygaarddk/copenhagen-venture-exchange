<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Copenhagen_Venture_Exchange
 */

?>

	</div><!-- #content -->

	<footer class="footer" style="background-color: <?php echo get_theme_mod('footer-color', '#000000'); ?>">
		<div class="footer__button-wrapper">
		<?php 
				$link = get_field('footer_button_url');

				if( $link ) {
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
						<a class="button button--primary" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
					<?php
				}
			?>
			
			
		</div>
		<div class="footer__widget-wrapper content">
			<div class="footer__widget"><?php dynamic_sidebar( 'footer_1' ); ?></div>
			<div class="footer__widget"><?php dynamic_sidebar( 'footer_2' ); ?></div>
			<div class="footer__widget"><?php dynamic_sidebar( 'footer_3' ); ?></div>
			<div class="footer__widget"><?php dynamic_sidebar( 'footer_4' ); ?></div>
		</div>
	</footer><!-- #colophon -->
</div><!-- page-wrapper -->
</div><!-- /wrapper -->
<?php wp_footer(); ?>

</body>
</html>
