<?php
/**
 * Copenhagen Venture Exchange Theme Customizer
 *
 * @package Copenhagen_Venture_Exchange
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function cvx_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'cvx_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'cvx_customize_partial_blogdescription',
		) );
	}

	// Styling

	$wp_customize->add_section( 'styling', array(
		'title'      => 'Styling',
		'priority'   => 30,
	) );

	$wp_customize->add_setting( 'menu-color' , array(
		'default'     => '#000000',
		'transport'   => 'refresh',
	) );

	$wp_customize->add_setting( 'background-color' , array(
		'default'     => '#ffffff',
		'transport'   => 'refresh',
	) );

	$wp_customize->add_setting( 'footer-color' , array(
		'default'     => '#ffffff',
		'transport'   => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu-color', array(
		'label'      => 'Menu Color',
		'section'    => 'styling',
		'settings'   => 'menu-color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background-color', array(
		'label'      => 'Background Color',
		'section'    => 'styling',
		'settings'   => 'background-color',
	) ) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footr-color', array(
		'label'      => 'Footer Color',
		'section'    => 'styling',
		'settings'   => 'footer-color',
	) ) );

	$wp_customize->add_setting( 'mobile-logo' , array(
		'transport'   => 'refresh',
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'logo',
			array(
				'label'      => __( 'Mobile top-menu logo', 'cvx' ),
				'section'    => 'styling',
				'settings'   => 'mobile-logo'
			)
		)
	);

	// Global button

	$wp_customize->add_section( 'global-button', array(
		'title'      => 'Global Button',
		'priority'   => 31,
	) );

	$wp_customize->add_setting( 'global-button-text', array(
		'capability' => 'edit_theme_options'
	  ) );
	  
	$wp_customize->add_control( 'global-button-text', array(
		'type' => 'text',
		'section' => 'global-button',
		'label' => __( 'Button text' )
	) );
		
	$wp_customize->add_setting( 'global-button-url', array(
		'capability' => 'edit_theme_options'
	) );
	  
	$wp_customize->add_control( 'global-button-url', array(
		'type' => 'dropdown-pages',
		'section' => 'global-button',
		'label' => __( 'Button url' ),
		'description' => __( 'Select which page to go to' ),
	) );
		
	$wp_customize->add_setting( 'global-button-menu',
		array(
			'default' => 1,
			'transport' => 'refresh'
		)
	);

	$wp_customize->add_control( 'global-button-menu',
		array(
			'label' => 'Show in menu',
			'section' => 'global-button',
			'type'=> 'checkbox',
			'settings' => 'global-button-menu'
		)
	);
}

add_action( 'customize_register', 'cvx_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function cvx_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function cvx_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function cvx_customize_preview_js() {
	wp_enqueue_script( 'cvx-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'cvx_customize_preview_js' );


/**
* Create Logo Setting and Upload Control
*/
function your_theme_new_customizer_settings($wp_customize) {
	// add a setting for the site logo
	
}
add_action('customize_register', 'your_theme_new_customizer_settings');