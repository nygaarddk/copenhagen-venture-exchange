<?php

	require_once("includes/ActiveCampaign.class.php");

	$ac = new ActiveCampaign("https://cvx880.api-us1.com", "9499f6ef392cd847b08477cdf0ed4d5b5fa860d258145874029927a694d40f3556e2b600");
    

	/* TEST API CREDENTIALS */

	if (!(int)$ac->credentials_test()) {
		echo "Access denied: Invalid credentials (URL and/or API key)";
		exit();
    }
	
	$contact = array();
    
    $list_id = $_REQUEST['listid'];
    $contact["p[{$list_id}]"] = $list_id;
    $contact["status[{$list_id}]"] = 1;

    $contact["email"] = $_REQUEST['email'];

    if (isset($_REQUEST['firstname'])) {
        $contact["first_name"] = $_REQUEST['firstname'];
    }

    if (isset($_REQUEST['lastname'])) {
        $contact["last_name"] = $_REQUEST['lastname'];
    }

    if (isset($_REQUEST['company'])) {
        $contact["field[1,0]"] = $_REQUEST['company'];
    }

    if (isset($_REQUEST['jobtitle'])) {
        $contact["field[2,0]"] = $_REQUEST['jobtitle'];
    }

    if (isset($_REQUEST['phone'])) {
        $contact["phone"] = $_REQUEST['phone'];
    }

    if (isset($_REQUEST['tags'])) {
        $contact["tags"] = $_REQUEST['tags'];
    }

    

	$contact_sync = $ac->api("contact/sync", $contact);

	if (!(int)$contact_sync->success) {
		// request failed
		echo "Syncing contact failed. Error returned: " . $contact_sync->error;
		exit();
	}
        
    // successful request
    $contact_id = (int)$contact_sync->subscriber_id;
    
    if (isset($_REQUEST['message'])) {

        $note_array = array();

        $note_array['id'] = $contact_id;
        $note_array['listid'] = $list_id;
        $note_array['note'] = $_REQUEST['message'];

        $note_add = $ac->api("contact/note/add", $note_array);
    }

    echo "SUCCESS";
?>