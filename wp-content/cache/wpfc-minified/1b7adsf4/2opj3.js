if(typeof(php_data.ac_settings.site_tracking)!="undefined"&&php_data.ac_settings.site_tracking=="1"){
var trackByDefault=php_data.ac_settings.site_tracking_default;
function acEnableTracking(){
var expiration=new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30);
document.cookie="ac_enable_tracking=1; expires=" + expiration + "; path=/";
acTrackVisit();
}
function acTrackVisit(){
var trackcmp_email=php_data.user_email;
var trackcmp=document.createElement("script");
trackcmp.async=true;
trackcmp.type='text/javascript';
trackcmp.src='//trackcmp.net/visit?actid=' + php_data.ac_settings.tracking_actid + '&e=' + encodeURIComponent(trackcmp_email) + '&r=' + encodeURIComponent(document.referrer) + '&u=' + encodeURIComponent(window.location.href);
var trackcmp_s=document.getElementsByTagName("script");
if(trackcmp_s.length){
trackcmp_s[0].parentNode.appendChild(trackcmp);
}else{
var trackcmp_h=document.getElementsByTagName("head");
trackcmp_h.length&&trackcmp_h[0].appendChild(trackcmp);
}}
if(trackByDefault||/(^|;)ac_enable_tracking=([^;]+)/.test(document.cookie)){
acEnableTracking();
}};
(function($){
'use strict';
if(typeof wpcf7==='undefined'||wpcf7===null){
return;
}
wpcf7=$.extend({
cached: 0,
inputs: []
}, wpcf7);
$(function(){
wpcf7.supportHtml5=(function(){
var features={};
var input=document.createElement('input');
features.placeholder='placeholder' in input;
var inputTypes=[ 'email', 'url', 'tel', 'number', 'range', 'date' ];
$.each(inputTypes, function(index, value){
input.setAttribute('type', value);
features[ value ]=input.type!=='text';
});
return features;
})();
$('div.wpcf7 > form').each(function(){
var $form=$(this);
wpcf7.initForm($form);
if(wpcf7.cached){
wpcf7.refill($form);
}});
});
wpcf7.getId=function(form){
return parseInt($('input[name="_wpcf7"]', form).val(), 10);
};
wpcf7.initForm=function(form){
var $form=$(form);
$form.submit(function(event){
if(! wpcf7.supportHtml5.placeholder){
$('[placeholder].placeheld', $form).each(function(i, n){
$(n).val('').removeClass('placeheld');
});
}
if(typeof window.FormData==='function'){
wpcf7.submit($form);
event.preventDefault();
}});
$('.wpcf7-submit', $form).after('<span class="ajax-loader"></span>');
wpcf7.toggleSubmit($form);
$form.on('click', '.wpcf7-acceptance', function(){
wpcf7.toggleSubmit($form);
});
$('.wpcf7-exclusive-checkbox', $form).on('click', 'input:checkbox', function(){
var name=$(this).attr('name');
$form.find('input:checkbox[name="' + name + '"]').not(this).prop('checked', false);
});
$('.wpcf7-list-item.has-free-text', $form).each(function(){
var $freetext=$(':input.wpcf7-free-text', this);
var $wrap=$(this).closest('.wpcf7-form-control');
if($(':checkbox, :radio', this).is(':checked')){
$freetext.prop('disabled', false);
}else{
$freetext.prop('disabled', true);
}
$wrap.on('change', ':checkbox, :radio', function(){
var $cb=$('.has-free-text', $wrap).find(':checkbox, :radio');
if($cb.is(':checked')){
$freetext.prop('disabled', false).focus();
}else{
$freetext.prop('disabled', true);
}});
});
if(! wpcf7.supportHtml5.placeholder){
$('[placeholder]', $form).each(function(){
$(this).val($(this).attr('placeholder'));
$(this).addClass('placeheld');
$(this).focus(function(){
if($(this).hasClass('placeheld')){
$(this).val('').removeClass('placeheld');
}});
$(this).blur(function(){
if(''===$(this).val()){
$(this).val($(this).attr('placeholder'));
$(this).addClass('placeheld');
}});
});
}
if(wpcf7.jqueryUi&&! wpcf7.supportHtml5.date){
$form.find('input.wpcf7-date[type="date"]').each(function(){
$(this).datepicker({
dateFormat: 'yy-mm-dd',
minDate: new Date($(this).attr('min')),
maxDate: new Date($(this).attr('max'))
});
});
}
if(wpcf7.jqueryUi&&! wpcf7.supportHtml5.number){
$form.find('input.wpcf7-number[type="number"]').each(function(){
$(this).spinner({
min: $(this).attr('min'),
max: $(this).attr('max'),
step: $(this).attr('step')
});
});
}
$('.wpcf7-character-count', $form).each(function(){
var $count=$(this);
var name=$count.attr('data-target-name');
var down=$count.hasClass('down');
var starting=parseInt($count.attr('data-starting-value'), 10);
var maximum=parseInt($count.attr('data-maximum-value'), 10);
var minimum=parseInt($count.attr('data-minimum-value'), 10);
var updateCount=function(target){
var $target=$(target);
var length=$target.val().length;
var count=down ? starting - length:length;
$count.attr('data-current-value', count);
$count.text(count);
if(maximum&&maximum < length){
$count.addClass('too-long');
}else{
$count.removeClass('too-long');
}
if(minimum&&length < minimum){
$count.addClass('too-short');
}else{
$count.removeClass('too-short');
}};
$(':input[name="' + name + '"]', $form).each(function(){
updateCount(this);
$(this).keyup(function(){
updateCount(this);
});
});
});
$form.on('change', '.wpcf7-validates-as-url', function(){
var val=$.trim($(this).val());
if(val
&& ! val.match(/^[a-z][a-z0-9.+-]*:/i)
&& -1!==val.indexOf('.')){
val=val.replace(/^\/+/, '');
val='http://' + val;
}
$(this).val(val);
});
};
wpcf7.submit=function(form){
if(typeof window.FormData!=='function'){
return;
}
var $form=$(form);
$('.ajax-loader', $form).addClass('is-active');
wpcf7.clearResponse($form);
var formData=new FormData($form.get(0));
var detail={
id: $form.closest('div.wpcf7').attr('id'),
status: 'init',
inputs: [],
formData: formData
};
$.each($form.serializeArray(), function(i, field){
if('_wpcf7'==field.name){
detail.contactFormId=field.value;
}else if('_wpcf7_version'==field.name){
detail.pluginVersion=field.value;
}else if('_wpcf7_locale'==field.name){
detail.contactFormLocale=field.value;
}else if('_wpcf7_unit_tag'==field.name){
detail.unitTag=field.value;
}else if('_wpcf7_container_post'==field.name){
detail.containerPostId=field.value;
}else if(field.name.match(/^_wpcf7_\w+_free_text_/)){
var owner=field.name.replace(/^_wpcf7_\w+_free_text_/, '');
detail.inputs.push({
name: owner + '-free-text',
value: field.value
});
}else if(field.name.match(/^_/)){
}else{
detail.inputs.push(field);
}});
wpcf7.triggerEvent($form.closest('div.wpcf7'), 'beforesubmit', detail);
var ajaxSuccess=function(data, status, xhr, $form){
detail.id=$(data.into).attr('id');
detail.status=data.status;
detail.apiResponse=data;
var $message=$('.wpcf7-response-output', $form);
switch(data.status){
case 'validation_failed':
$.each(data.invalidFields, function(i, n){
$(n.into, $form).each(function(){
wpcf7.notValidTip(this, n.message);
$('.wpcf7-form-control', this).addClass('wpcf7-not-valid');
$('[aria-invalid]', this).attr('aria-invalid', 'true');
});
});
$message.addClass('wpcf7-validation-errors');
$form.addClass('invalid');
wpcf7.triggerEvent(data.into, 'invalid', detail);
break;
case 'acceptance_missing':
$message.addClass('wpcf7-acceptance-missing');
$form.addClass('unaccepted');
wpcf7.triggerEvent(data.into, 'unaccepted', detail);
break;
case 'spam':
$message.addClass('wpcf7-spam-blocked');
$form.addClass('spam');
wpcf7.triggerEvent(data.into, 'spam', detail);
break;
case 'aborted':
$message.addClass('wpcf7-aborted');
$form.addClass('aborted');
wpcf7.triggerEvent(data.into, 'aborted', detail);
break;
case 'mail_sent':
$message.addClass('wpcf7-mail-sent-ok');
$form.addClass('sent');
wpcf7.triggerEvent(data.into, 'mailsent', detail);
break;
case 'mail_failed':
$message.addClass('wpcf7-mail-sent-ng');
$form.addClass('failed');
wpcf7.triggerEvent(data.into, 'mailfailed', detail);
break;
default:
var customStatusClass='custom-'
+ data.status.replace(/[^0-9a-z]+/i, '-');
$message.addClass('wpcf7-' + customStatusClass);
$form.addClass(customStatusClass);
}
wpcf7.refill($form, data);
wpcf7.triggerEvent(data.into, 'submit', detail);
if('mail_sent'==data.status){
$form.each(function(){
this.reset();
});
wpcf7.toggleSubmit($form);
}
if(! wpcf7.supportHtml5.placeholder){
$form.find('[placeholder].placeheld').each(function(i, n){
$(n).val($(n).attr('placeholder'));
});
}
$message.html('').append(data.message).slideDown('fast');
$message.attr('role', 'alert');
$('.screen-reader-response', $form.closest('.wpcf7')).each(function(){
var $response=$(this);
$response.html('').attr('role', '').append(data.message);
if(data.invalidFields){
var $invalids=$('<ul></ul>');
$.each(data.invalidFields, function(i, n){
if(n.idref){
var $li=$('<li></li>').append($('<a></a>').attr('href', '#' + n.idref).append(n.message));
}else{
var $li=$('<li></li>').append(n.message);
}
$invalids.append($li);
});
$response.append($invalids);
}
$response.attr('role', 'alert').focus();
});
};
$.ajax({
type: 'POST',
url: wpcf7.apiSettings.getRoute('/contact-forms/' + wpcf7.getId($form) + '/feedback'),
data: formData,
dataType: 'json',
processData: false,
contentType: false
}).done(function(data, status, xhr){
ajaxSuccess(data, status, xhr, $form);
$('.ajax-loader', $form).removeClass('is-active');
}).fail(function(xhr, status, error){
var $e=$('<div class="ajax-error"></div>').text(error.message);
$form.after($e);
});
};
wpcf7.triggerEvent=function(target, name, detail){
var $target=$(target);
var event=new CustomEvent('wpcf7' + name, {
bubbles: true,
detail: detail
});
$target.get(0).dispatchEvent(event);
$target.trigger('wpcf7:' + name, detail);
$target.trigger(name + '.wpcf7', detail);
};
wpcf7.toggleSubmit=function(form, state){
var $form=$(form);
var $submit=$('input:submit', $form);
if(typeof state!=='undefined'){
$submit.prop('disabled', ! state);
return;
}
if($form.hasClass('wpcf7-acceptance-as-validation')){
return;
}
$submit.prop('disabled', false);
$('.wpcf7-acceptance', $form).each(function(){
var $span=$(this);
var $input=$('input:checkbox', $span);
if(! $span.hasClass('optional')){
if($span.hasClass('invert')&&$input.is(':checked')
|| ! $span.hasClass('invert')&&! $input.is(':checked')){
$submit.prop('disabled', true);
return false;
}}
});
};
wpcf7.notValidTip=function(target, message){
var $target=$(target);
$('.wpcf7-not-valid-tip', $target).remove();
$('<span role="alert" class="wpcf7-not-valid-tip"></span>')
.text(message).appendTo($target);
if($target.is('.use-floating-validation-tip *')){
var fadeOut=function(target){
$(target).not(':hidden').animate({
opacity: 0
}, 'fast', function(){
$(this).css({ 'z-index': -100 });
});
};
$target.on('mouseover', '.wpcf7-not-valid-tip', function(){
fadeOut(this);
});
$target.on('focus', ':input', function(){
fadeOut($('.wpcf7-not-valid-tip', $target));
});
}};
wpcf7.refill=function(form, data){
var $form=$(form);
var refillCaptcha=function($form, items){
$.each(items, function(i, n){
$form.find(':input[name="' + i + '"]').val('');
$form.find('img.wpcf7-captcha-' + i).attr('src', n);
var match=/([0-9]+)\.(png|gif|jpeg)$/.exec(n);
$form.find('input:hidden[name="_wpcf7_captcha_challenge_' + i + '"]').attr('value', match[ 1 ]);
});
};
var refillQuiz=function($form, items){
$.each(items, function(i, n){
$form.find(':input[name="' + i + '"]').val('');
$form.find(':input[name="' + i + '"]').siblings('span.wpcf7-quiz-label').text(n[ 0 ]);
$form.find('input:hidden[name="_wpcf7_quiz_answer_' + i + '"]').attr('value', n[ 1 ]);
});
};
if(typeof data==='undefined'){
$.ajax({
type: 'GET',
url: wpcf7.apiSettings.getRoute('/contact-forms/' + wpcf7.getId($form) + '/refill'),
beforeSend: function(xhr){
var nonce=$form.find(':input[name="_wpnonce"]').val();
if(nonce){
xhr.setRequestHeader('X-WP-Nonce', nonce);
}},
dataType: 'json'
}).done(function(data, status, xhr){
if(data.captcha){
refillCaptcha($form, data.captcha);
}
if(data.quiz){
refillQuiz($form, data.quiz);
}});
}else{
if(data.captcha){
refillCaptcha($form, data.captcha);
}
if(data.quiz){
refillQuiz($form, data.quiz);
}}
};
wpcf7.clearResponse=function(form){
var $form=$(form);
$form.removeClass('invalid spam sent failed');
$form.siblings('.screen-reader-response').html('').attr('role', '');
$('.wpcf7-not-valid-tip', $form).remove();
$('[aria-invalid]', $form).attr('aria-invalid', 'false');
$('.wpcf7-form-control', $form).removeClass('wpcf7-not-valid');
$('.wpcf7-response-output', $form)
.hide().empty().removeAttr('role')
.removeClass('wpcf7-mail-sent-ok wpcf7-mail-sent-ng wpcf7-validation-errors wpcf7-spam-blocked');
};
wpcf7.apiSettings.getRoute=function(path){
var url=wpcf7.apiSettings.root;
url=url.replace(wpcf7.apiSettings.namespace,
wpcf7.apiSettings.namespace + path);
return url;
};})(jQuery);
(function (){
if(typeof window.CustomEvent==="function") return false;
function CustomEvent(event, params){
params=params||{ bubbles: false, cancelable: false, detail: undefined };
var evt=document.createEvent('CustomEvent');
evt.initCustomEvent(event,
params.bubbles, params.cancelable, params.detail);
return evt;
}
CustomEvent.prototype=window.Event.prototype;
window.CustomEvent=CustomEvent;
})();
+function ($){
'use strict';
function transitionEnd(){
var el=document.createElement('bootstrap')
var transEndEventNames={
WebkitTransition:'webkitTransitionEnd',
MozTransition:'transitionend',
OTransition:'oTransitionEnd otransitionend',
transition:'transitionend'
}
for (var name in transEndEventNames){
if(el.style[name]!==undefined){
return { end: transEndEventNames[name] }}
}
return false
}
$.fn.emulateTransitionEnd=function (duration){
var called=false
var $el=this
$(this).one('bsTransitionEnd', function (){ called=true })
var callback=function (){ if(!called) $($el).trigger($.support.transition.end) }
setTimeout(callback, duration)
return this
}
$(function (){
$.support.transition=transitionEnd()
if(!$.support.transition) return
$.event.special.bsTransitionEnd={
bindType: $.support.transition.end,
delegateType: $.support.transition.end,
handle: function (e){
if($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
}}
})
}(jQuery);
+function ($){
'use strict';
var Modal=function (element, options){
this.options=options
this.$body=$(document.body)
this.$element=$(element)
this.$dialog=this.$element.find('.modal-dialog')
this.$backdrop=null
this.isShown=null
this.originalBodyPad=null
this.scrollbarWidth=0
this.ignoreBackdropClick=false
if(this.options.remote){
this.$element
.find('.modal-content')
.load(this.options.remote, $.proxy(function (){
this.$element.trigger('loaded.bs.modal')
}, this))
}}
Modal.VERSION='3.3.6'
Modal.TRANSITION_DURATION=300
Modal.BACKDROP_TRANSITION_DURATION=150
Modal.DEFAULTS={
backdrop: true,
keyboard: true,
show: true
}
Modal.prototype.toggle=function (_relatedTarget){
return this.isShown ? this.hide():this.show(_relatedTarget)
}
Modal.prototype.show=function (_relatedTarget){
var that=this
var e=$.Event('show.bs.modal', { relatedTarget: _relatedTarget })
this.$element.trigger(e)
if(this.isShown||e.isDefaultPrevented()) return
this.isShown=true
this.checkScrollbar()
this.setScrollbar()
this.$body.addClass('modal-open')
this.escape()
this.resize()
this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))
this.$dialog.on('mousedown.dismiss.bs.modal', function (){
that.$element.one('mouseup.dismiss.bs.modal', function (e){
if($(e.target).is(that.$element)) that.ignoreBackdropClick=true
})
})
this.backdrop(function (){
var transition=$.support.transition&&that.$element.hasClass('fade')
if(!that.$element.parent().length){
that.$element.appendTo(that.$body)
}
that.$element
.show()
.scrollTop(0)
that.adjustDialog()
if(transition){
that.$element[0].offsetWidth
}
that.$element.addClass('in')
that.enforceFocus()
var e=$.Event('shown.bs.modal', { relatedTarget: _relatedTarget })
transition ?
that.$dialog
.one('bsTransitionEnd', function (){
that.$element.trigger('focus').trigger(e)
})
.emulateTransitionEnd(Modal.TRANSITION_DURATION) :
that.$element.trigger('focus').trigger(e)
})
}
Modal.prototype.hide=function (e){
if(e) e.preventDefault()
e=$.Event('hide.bs.modal')
this.$element.trigger(e)
if(!this.isShown||e.isDefaultPrevented()) return
this.isShown=false
this.escape()
this.resize()
$(document).off('focusin.bs.modal')
this.$element
.removeClass('in')
.off('click.dismiss.bs.modal')
.off('mouseup.dismiss.bs.modal')
this.$dialog.off('mousedown.dismiss.bs.modal')
$.support.transition&&this.$element.hasClass('fade') ?
this.$element
.one('bsTransitionEnd', $.proxy(this.hideModal, this))
.emulateTransitionEnd(Modal.TRANSITION_DURATION) :
this.hideModal()
}
Modal.prototype.enforceFocus=function (){
$(document)
.off('focusin.bs.modal')
.on('focusin.bs.modal', $.proxy(function (e){
if(this.$element[0]!==e.target&&!this.$element.has(e.target).length){
this.$element.trigger('focus')
}}, this))
}
Modal.prototype.escape=function (){
if(this.isShown&&this.options.keyboard){
this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e){
e.which==27&&this.hide()
}, this))
}else if(!this.isShown){
this.$element.off('keydown.dismiss.bs.modal')
}}
Modal.prototype.resize=function (){
if(this.isShown){
$(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
}else{
$(window).off('resize.bs.modal')
}}
Modal.prototype.hideModal=function (){
var that=this
this.$element.hide()
this.backdrop(function (){
that.$body.removeClass('modal-open')
that.resetAdjustments()
that.resetScrollbar()
that.$element.trigger('hidden.bs.modal')
})
}
Modal.prototype.removeBackdrop=function (){
this.$backdrop&&this.$backdrop.remove()
this.$backdrop=null
}
Modal.prototype.backdrop=function (callback){
var that=this
var animate=this.$element.hasClass('fade') ? 'fade':''
if(this.isShown&&this.options.backdrop){
var doAnimate=$.support.transition&&animate
this.$backdrop=$(document.createElement('div'))
.addClass('modal-backdrop ' + animate)
.appendTo(this.$body)
this.$element.on('click.dismiss.bs.modal', $.proxy(function (e){
if(this.ignoreBackdropClick){
this.ignoreBackdropClick=false
return
}
if(e.target!==e.currentTarget) return
this.options.backdrop=='static'
? this.$element[0].focus()
: this.hide()
}, this))
if(doAnimate) this.$backdrop[0].offsetWidth
this.$backdrop.addClass('in')
if(!callback) return
doAnimate ?
this.$backdrop
.one('bsTransitionEnd', callback)
.emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
callback()
}else if(!this.isShown&&this.$backdrop){
this.$backdrop.removeClass('in')
var callbackRemove=function (){
that.removeBackdrop()
callback&&callback()
}
$.support.transition&&this.$element.hasClass('fade') ?
this.$backdrop
.one('bsTransitionEnd', callbackRemove)
.emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
callbackRemove()
}else if(callback){
callback()
}}
Modal.prototype.handleUpdate=function (){
this.adjustDialog()
}
Modal.prototype.adjustDialog=function (){
var lmmodalIsOverflowing=this.$element[0].scrollHeight > document.documentElement.clientHeight
this.$element.css({
paddingLeft:  !this.bodyIsOverflowing&&lmmodalIsOverflowing ? this.scrollbarWidth:'',
paddingRight: this.bodyIsOverflowing&&!lmmodalIsOverflowing ? this.scrollbarWidth:''
})
}
Modal.prototype.resetAdjustments=function (){
this.$element.css({
paddingLeft: '',
paddingRight: ''
})
}
Modal.prototype.checkScrollbar=function (){
var fullWindowWidth=window.innerWidth
if(!fullWindowWidth){
var documentElementRect=document.documentElement.getBoundingClientRect()
fullWindowWidth=documentElementRect.right - Math.abs(documentElementRect.left)
}
this.bodyIsOverflowing=document.body.clientWidth < fullWindowWidth
this.scrollbarWidth=this.measureScrollbar()
}
Modal.prototype.setScrollbar=function (){
var bodyPad=parseInt((this.$body.css('padding-right')||0), 10)
this.originalBodyPad=document.body.style.paddingRight||''
if(this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
}
Modal.prototype.resetScrollbar=function (){
this.$body.css('padding-right', this.originalBodyPad)
}
Modal.prototype.measureScrollbar=function (){
var scrollDiv=document.createElement('div')
scrollDiv.className='modal-scrollbar-measure'
this.$body.append(scrollDiv)
var scrollbarWidth=scrollDiv.offsetWidth - scrollDiv.clientWidth
this.$body[0].removeChild(scrollDiv)
return scrollbarWidth
}
function Plugin(option, _relatedTarget){
return this.each(function (){
var $this=$(this)
var data=$this.data('bs.modal')
var options=$.extend({}, Modal.DEFAULTS, $this.data(), typeof option=='object'&&option)
if(!data) $this.data('bs.modal', (data=new Modal(this, options)))
if(typeof option=='string') data[option](_relatedTarget)
else if(options.show) data.show(_relatedTarget)
})
}
var old=$.fn.modal
$.fn.modal=Plugin
$.fn.modal.Constructor=Modal
$.fn.modal.noConflict=function (){
$.fn.modal=old
return this
}
$(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e){
var $this=$(this)
var href=$this.attr('href')
var $target=$($this.attr('data-target')||(href&&href.replace(/.*(?=#[^\s]+$)/, '')))
var option=$target.data('bs.modal') ? 'toggle':$.extend({ remote: !/#/.test(href)&&href }, $target.data(), $this.data())
if($this.is('a')) e.preventDefault()
$target.one('show.bs.modal', function (showEvent){
if(showEvent.isDefaultPrevented()) return
$target.one('hidden.bs.modal', function (){
$this.is(':visible')&&$this.trigger('focus')
})
})
Plugin.call($target, option, this)
})
}(jQuery);
var isMobile={
Android: function(){
return navigator.userAgent.match(/Android/i);
},
BlackBerry: function(){
return navigator.userAgent.match(/BlackBerry/i);
},
iOS: function(){
return navigator.userAgent.match(/iPhone|iPad|iPod/i);
},
Opera: function(){
return navigator.userAgent.match(/Opera Mini/i);
},
Windows: function(){
return navigator.userAgent.match(/IEMobile/i);
},
any: function(){
return (isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows());
}};
jQuery(document).ready(function($){
$('.modal-popup').click(function(){
var popup_id=$(this).attr('data-target');
$(popup_id).modal('show');
$('.modal-backdrop').hide();
});
});
(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function(global, factory){
"use strict";
if(typeof module==="object"&&typeof module.exports==="object"){
module.exports=global.document ?
factory(global, true) :
function(w){
if(!w.document){
throw new Error("jQuery requires a window with a document");
}
return factory(w);
};}else{
factory(global);
}})(typeof window!=="undefined" ? window:this, function(window, noGlobal){
"use strict";
var arr=[];
var document=window.document;
var getProto=Object.getPrototypeOf;
var slice=arr.slice;
var concat=arr.concat;
var push=arr.push;
var indexOf=arr.indexOf;
var class2type={};
var toString=class2type.toString;
var hasOwn=class2type.hasOwnProperty;
var fnToString=hasOwn.toString;
var ObjectFunctionString=fnToString.call(Object);
var support={};
var isFunction=function isFunction(obj){
return typeof obj==="function"&&typeof obj.nodeType!=="number";
};
var isWindow=function isWindow(obj){
return obj!=null&&obj===obj.window;
};
var preservedScriptAttributes={
type: true,
src: true,
noModule: true
};
function DOMEval(code, doc, node){
doc=doc||document;
var i,
script=doc.createElement("script");
script.text=code;
if(node){
for(i in preservedScriptAttributes){
if(node[ i ]){
script[ i ]=node[ i ];
}}
}
doc.head.appendChild(script).parentNode.removeChild(script);
}
function toType(obj){
if(obj==null){
return obj + "";
}
return typeof obj==="object"||typeof obj==="function" ?
class2type[ toString.call(obj) ]||"object" :
typeof obj;
}
var
version="3.3.1",
jQuery=function(selector, context){
return new jQuery.fn.init(selector, context);
},
rtrim=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
jQuery.fn=jQuery.prototype={
jquery: version,
constructor: jQuery,
length: 0,
toArray: function(){
return slice.call(this);
},
get: function(num){
if(num==null){
return slice.call(this);
}
return num < 0 ? this[ num + this.length ]:this[ num ];
},
pushStack: function(elems){
var ret=jQuery.merge(this.constructor(), elems);
ret.prevObject=this;
return ret;
},
each: function(callback){
return jQuery.each(this, callback);
},
map: function(callback){
return this.pushStack(jQuery.map(this, function(elem, i){
return callback.call(elem, i, elem);
}));
},
slice: function(){
return this.pushStack(slice.apply(this, arguments));
},
first: function(){
return this.eq(0);
},
last: function(){
return this.eq(-1);
},
eq: function(i){
var len=this.length,
j=+i +(i < 0 ? len:0);
return this.pushStack(j >=0&&j < len ? [ this[ j ] ]:[]);
},
end: function(){
return this.prevObject||this.constructor();
},
push: push,
sort: arr.sort,
splice: arr.splice
};
jQuery.extend=jQuery.fn.extend=function(){
var options, name, src, copy, copyIsArray, clone,
target=arguments[ 0 ]||{},
i=1,
length=arguments.length,
deep=false;
if(typeof target==="boolean"){
deep=target;
target=arguments[ i ]||{};
i++;
}
if(typeof target!=="object"&&!isFunction(target)){
target={};}
if(i===length){
target=this;
i--;
}
for(; i < length; i++){
if(( options=arguments[ i ])!=null){
for(name in options){
src=target[ name ];
copy=options[ name ];
if(target===copy){
continue;
}
if(deep&&copy&&(jQuery.isPlainObject(copy) ||
(copyIsArray=Array.isArray(copy)))){
if(copyIsArray){
copyIsArray=false;
clone=src&&Array.isArray(src) ? src:[];
}else{
clone=src&&jQuery.isPlainObject(src) ? src:{};}
target[ name ]=jQuery.extend(deep, clone, copy);
}else if(copy!==undefined){
target[ name ]=copy;
}}
}}
return target;
};
jQuery.extend({
expando: "jQuery" +(version + Math.random()).replace(/\D/g, ""),
isReady: true,
error: function(msg){
throw new Error(msg);
},
noop: function(){},
isPlainObject: function(obj){
var proto, Ctor;
if(!obj||toString.call(obj)!=="[object Object]"){
return false;
}
proto=getProto(obj);
if(!proto){
return true;
}
Ctor=hasOwn.call(proto, "constructor")&&proto.constructor;
return typeof Ctor==="function"&&fnToString.call(Ctor)===ObjectFunctionString;
},
isEmptyObject: function(obj){
var name;
for(name in obj){
return false;
}
return true;
},
globalEval: function(code){
DOMEval(code);
},
each: function(obj, callback){
var length, i=0;
if(isArrayLike(obj)){
length=obj.length;
for(; i < length; i++){
if(callback.call(obj[ i ], i, obj[ i ])===false){
break;
}}
}else{
for(i in obj){
if(callback.call(obj[ i ], i, obj[ i ])===false){
break;
}}
}
return obj;
},
trim: function(text){
return text==null ?
"" :
(text + "").replace(rtrim, "");
},
makeArray: function(arr, results){
var ret=results||[];
if(arr!=null){
if(isArrayLike(Object(arr))){
jQuery.merge(ret,
typeof arr==="string" ?
[ arr ]:arr
);
}else{
push.call(ret, arr);
}}
return ret;
},
inArray: function(elem, arr, i){
return arr==null ? -1:indexOf.call(arr, elem, i);
},
merge: function(first, second){
var len=+second.length,
j=0,
i=first.length;
for(; j < len; j++){
first[ i++ ]=second[ j ];
}
first.length=i;
return first;
},
grep: function(elems, callback, invert){
var callbackInverse,
matches=[],
i=0,
length=elems.length,
callbackExpect = !invert;
for(; i < length; i++){
callbackInverse = !callback(elems[ i ], i);
if(callbackInverse!==callbackExpect){
matches.push(elems[ i ]);
}}
return matches;
},
map: function(elems, callback, arg){
var length, value,
i=0,
ret=[];
if(isArrayLike(elems)){
length=elems.length;
for(; i < length; i++){
value=callback(elems[ i ], i, arg);
if(value!=null){
ret.push(value);
}}
}else{
for(i in elems){
value=callback(elems[ i ], i, arg);
if(value!=null){
ret.push(value);
}}
}
return concat.apply([], ret);
},
guid: 1,
support: support
});
if(typeof Symbol==="function"){
jQuery.fn[ Symbol.iterator ]=arr[ Symbol.iterator ];
}
jQuery.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),
function(i, name){
class2type[ "[object " + name + "]" ]=name.toLowerCase();
});
function isArrayLike(obj){
var length = !!obj&&"length" in obj&&obj.length,
type=toType(obj);
if(isFunction(obj)||isWindow(obj)){
return false;
}
return type==="array"||length===0 ||
typeof length==="number"&&length > 0&&(length - 1) in obj;
}
var Sizzle =
(function(window){
var i,
support,
Expr,
getText,
isXML,
tokenize,
compile,
select,
outermostContext,
sortInput,
hasDuplicate,
setDocument,
document,
docElem,
documentIsHTML,
rbuggyQSA,
rbuggyMatches,
matches,
contains,
expando="sizzle" + 1 * new Date(),
preferredDoc=window.document,
dirruns=0,
done=0,
classCache=createCache(),
tokenCache=createCache(),
compilerCache=createCache(),
sortOrder=function(a, b){
if(a===b){
hasDuplicate=true;
}
return 0;
},
hasOwn=({}).hasOwnProperty,
arr=[],
pop=arr.pop,
push_native=arr.push,
push=arr.push,
slice=arr.slice,
indexOf=function(list, elem){
var i=0,
len=list.length;
for(; i < len; i++){
if(list[i]===elem){
return i;
}}
return -1;
},
booleans="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
whitespace="[\\x20\\t\\r\\n\\f]",
identifier="(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
attributes="\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
"*([*^$|!~]?=)" + whitespace +
"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
"*\\]",
pseudos=":(" + identifier + ")(?:\\((" +
"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
".*" +
")\\)|)",
rwhitespace=new RegExp(whitespace + "+", "g"),
rtrim=new RegExp("^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g"),
rcomma=new RegExp("^" + whitespace + "*," + whitespace + "*"),
rcombinators=new RegExp("^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*"),
rattributeQuotes=new RegExp("=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g"),
rpseudo=new RegExp(pseudos),
ridentifier=new RegExp("^" + identifier + "$"),
matchExpr={
"ID": new RegExp("^#(" + identifier + ")"),
"CLASS": new RegExp("^\\.(" + identifier + ")"),
"TAG": new RegExp("^(" + identifier + "|[*])"),
"ATTR": new RegExp("^" + attributes),
"PSEUDO": new RegExp("^" + pseudos),
"CHILD": new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
"*(\\d+)|))" + whitespace + "*\\)|)", "i"),
"bool": new RegExp("^(?:" + booleans + ")$", "i"),
"needsContext": new RegExp("^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i")
},
rinputs=/^(?:input|select|textarea|button)$/i,
rheader=/^h\d$/i,
rnative=/^[^{]+\{\s*\[native \w/,
rquickExpr=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
rsibling=/[+~]/,
runescape=new RegExp("\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig"),
funescape=function(_, escaped, escapedWhitespace){
var high="0x" + escaped - 0x10000;
return high!==high||escapedWhitespace ?
escaped :
high < 0 ?
String.fromCharCode(high + 0x10000) :
String.fromCharCode(high >> 10 | 0xD800, high & 0x3FF | 0xDC00);
},
rcssescape=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
fcssescape=function(ch, asCodePoint){
if(asCodePoint){
if(ch==="\0"){
return "\uFFFD";
}
return ch.slice(0, -1) + "\\" + ch.charCodeAt(ch.length - 1).toString(16) + " ";
}
return "\\" + ch;
},
unloadHandler=function(){
setDocument();
},
disabledAncestor=addCombinator(
function(elem){
return elem.disabled===true&&("form" in elem||"label" in elem);
},
{ dir: "parentNode", next: "legend" }
);
try {
push.apply((arr=slice.call(preferredDoc.childNodes)),
preferredDoc.childNodes
);
arr[ preferredDoc.childNodes.length ].nodeType;
} catch(e){
push={ apply: arr.length ?
function(target, els){
push_native.apply(target, slice.call(els));
} :
function(target, els){
var j=target.length,
i=0;
while((target[j++]=els[i++])){}
target.length=j - 1;
}};}
function Sizzle(selector, context, results, seed){
var m, i, elem, nid, match, groups, newSelector,
newContext=context&&context.ownerDocument,
nodeType=context ? context.nodeType:9;
results=results||[];
if(typeof selector!=="string"||!selector ||
nodeType!==1&&nodeType!==9&&nodeType!==11){
return results;
}
if(!seed){
if(( context ? context.ownerDocument||context:preferredDoc)!==document){
setDocument(context);
}
context=context||document;
if(documentIsHTML){
if(nodeType!==11&&(match=rquickExpr.exec(selector))){
if((m=match[1])){
if(nodeType===9){
if((elem=context.getElementById(m))){
if(elem.id===m){
results.push(elem);
return results;
}}else{
return results;
}}else{
if(newContext&&(elem=newContext.getElementById(m)) &&
contains(context, elem) &&
elem.id===m){
results.push(elem);
return results;
}}
}else if(match[2]){
push.apply(results, context.getElementsByTagName(selector));
return results;
}else if((m=match[3])&&support.getElementsByClassName &&
context.getElementsByClassName){
push.apply(results, context.getElementsByClassName(m));
return results;
}}
if(support.qsa &&
!compilerCache[ selector + " " ] &&
(!rbuggyQSA||!rbuggyQSA.test(selector))){
if(nodeType!==1){
newContext=context;
newSelector=selector;
}else if(context.nodeName.toLowerCase()!=="object"){
if((nid=context.getAttribute("id"))){
nid=nid.replace(rcssescape, fcssescape);
}else{
context.setAttribute("id", (nid=expando));
}
groups=tokenize(selector);
i=groups.length;
while(i--){
groups[i]="#" + nid + " " + toSelector(groups[i]);
}
newSelector=groups.join(",");
newContext=rsibling.test(selector)&&testContext(context.parentNode) ||
context;
}
if(newSelector){
try {
push.apply(results,
newContext.querySelectorAll(newSelector)
);
return results;
} catch(qsaError){
} finally {
if(nid===expando){
context.removeAttribute("id");
}}
}}
}}
return select(selector.replace(rtrim, "$1"), context, results, seed);
}
/**
* Create key-value caches of limited size
* @returns {function(string, object)} Returns the Object data after storing it on itself with
*	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
*	deleting the oldest entry
*/
function createCache(){
var keys=[];
function cache(key, value){
if(keys.push(key + " ") > Expr.cacheLength){
delete cache[ keys.shift() ];
}
return (cache[ key + " " ]=value);
}
return cache;
}
function markFunction(fn){
fn[ expando ]=true;
return fn;
}
function assert(fn){
var el=document.createElement("fieldset");
try {
return !!fn(el);
} catch (e){
return false;
} finally {
if(el.parentNode){
el.parentNode.removeChild(el);
}
el=null;
}}
function addHandle(attrs, handler){
var arr=attrs.split("|"),
i=arr.length;
while(i--){
Expr.attrHandle[ arr[i] ]=handler;
}}
function siblingCheck(a, b){
var cur=b&&a,
diff=cur&&a.nodeType===1&&b.nodeType===1 &&
a.sourceIndex - b.sourceIndex;
if(diff){
return diff;
}
if(cur){
while((cur=cur.nextSibling)){
if(cur===b){
return -1;
}}
}
return a ? 1:-1;
}
function createInputPseudo(type){
return function(elem){
var name=elem.nodeName.toLowerCase();
return name==="input"&&elem.type===type;
};}
function createButtonPseudo(type){
return function(elem){
var name=elem.nodeName.toLowerCase();
return (name==="input"||name==="button")&&elem.type===type;
};}
function createDisabledPseudo(disabled){
return function(elem){
if("form" in elem){
if(elem.parentNode&&elem.disabled===false){
if("label" in elem){
if("label" in elem.parentNode){
return elem.parentNode.disabled===disabled;
}else{
return elem.disabled===disabled;
}}
return elem.isDisabled===disabled ||
elem.isDisabled!==!disabled &&
disabledAncestor(elem)===disabled;
}
return elem.disabled===disabled;
}else if("label" in elem){
return elem.disabled===disabled;
}
return false;
};}
function createPositionalPseudo(fn){
return markFunction(function(argument){
argument=+argument;
return markFunction(function(seed, matches){
var j,
matchIndexes=fn([], seed.length, argument),
i=matchIndexes.length;
while(i--){
if(seed[ (j=matchIndexes[i]) ]){
seed[j] = !(matches[j]=seed[j]);
}}
});
});
}
function testContext(context){
return context&&typeof context.getElementsByTagName!=="undefined"&&context;
}
support=Sizzle.support={};
isXML=Sizzle.isXML=function(elem){
var documentElement=elem&&(elem.ownerDocument||elem).documentElement;
return documentElement ? documentElement.nodeName!=="HTML":false;
};
setDocument=Sizzle.setDocument=function(node){
var hasCompare, subWindow,
doc=node ? node.ownerDocument||node:preferredDoc;
if(doc===document||doc.nodeType!==9||!doc.documentElement){
return document;
}
document=doc;
docElem=document.documentElement;
documentIsHTML = !isXML(document);
if(preferredDoc!==document &&
(subWindow=document.defaultView)&&subWindow.top!==subWindow){
if(subWindow.addEventListener){
subWindow.addEventListener("unload", unloadHandler, false);
}else if(subWindow.attachEvent){
subWindow.attachEvent("onunload", unloadHandler);
}}
support.attributes=assert(function(el){
el.className="i";
return !el.getAttribute("className");
});
support.getElementsByTagName=assert(function(el){
el.appendChild(document.createComment(""));
return !el.getElementsByTagName("*").length;
});
support.getElementsByClassName=rnative.test(document.getElementsByClassName);
support.getById=assert(function(el){
docElem.appendChild(el).id=expando;
return !document.getElementsByName||!document.getElementsByName(expando).length;
});
if(support.getById){
Expr.filter["ID"]=function(id){
var attrId=id.replace(runescape, funescape);
return function(elem){
return elem.getAttribute("id")===attrId;
};};
Expr.find["ID"]=function(id, context){
if(typeof context.getElementById!=="undefined"&&documentIsHTML){
var elem=context.getElementById(id);
return elem ? [ elem ]:[];
}};}else{
Expr.filter["ID"]=function(id){
var attrId=id.replace(runescape, funescape);
return function(elem){
var node=typeof elem.getAttributeNode!=="undefined" &&
elem.getAttributeNode("id");
return node&&node.value===attrId;
};};
Expr.find["ID"]=function(id, context){
if(typeof context.getElementById!=="undefined"&&documentIsHTML){
var node, i, elems,
elem=context.getElementById(id);
if(elem){
node=elem.getAttributeNode("id");
if(node&&node.value===id){
return [ elem ];
}
elems=context.getElementsByName(id);
i=0;
while((elem=elems[i++])){
node=elem.getAttributeNode("id");
if(node&&node.value===id){
return [ elem ];
}}
}
return [];
}};}
Expr.find["TAG"]=support.getElementsByTagName ?
function(tag, context){
if(typeof context.getElementsByTagName!=="undefined"){
return context.getElementsByTagName(tag);
}else if(support.qsa){
return context.querySelectorAll(tag);
}} :
function(tag, context){
var elem,
tmp=[],
i=0,
results=context.getElementsByTagName(tag);
if(tag==="*"){
while((elem=results[i++])){
if(elem.nodeType===1){
tmp.push(elem);
}}
return tmp;
}
return results;
};
Expr.find["CLASS"]=support.getElementsByClassName&&function(className, context){
if(typeof context.getElementsByClassName!=="undefined"&&documentIsHTML){
return context.getElementsByClassName(className);
}};
rbuggyMatches=[];
rbuggyQSA=[];
if((support.qsa=rnative.test(document.querySelectorAll))){
assert(function(el){
docElem.appendChild(el).innerHTML="<a id='" + expando + "'></a>" +
"<select id='" + expando + "-\r\\' msallowcapture=''>" +
"<option selected=''></option></select>";
if(el.querySelectorAll("[msallowcapture^='']").length){
rbuggyQSA.push("[*^$]=" + whitespace + "*(?:''|\"\")");
}
if(!el.querySelectorAll("[selected]").length){
rbuggyQSA.push("\\[" + whitespace + "*(?:value|" + booleans + ")");
}
if(!el.querySelectorAll("[id~=" + expando + "-]").length){
rbuggyQSA.push("~=");
}
if(!el.querySelectorAll(":checked").length){
rbuggyQSA.push(":checked");
}
if(!el.querySelectorAll("a#" + expando + "+*").length){
rbuggyQSA.push(".#.+[+~]");
}});
assert(function(el){
el.innerHTML="<a href='' disabled='disabled'></a>" +
"<select disabled='disabled'><option/></select>";
var input=document.createElement("input");
input.setAttribute("type", "hidden");
el.appendChild(input).setAttribute("name", "D");
if(el.querySelectorAll("[name=d]").length){
rbuggyQSA.push("name" + whitespace + "*[*^$|!~]?=");
}
if(el.querySelectorAll(":enabled").length!==2){
rbuggyQSA.push(":enabled", ":disabled");
}
docElem.appendChild(el).disabled=true;
if(el.querySelectorAll(":disabled").length!==2){
rbuggyQSA.push(":enabled", ":disabled");
}
el.querySelectorAll("*,:x");
rbuggyQSA.push(",.*:");
});
}
if((support.matchesSelector=rnative.test((matches=docElem.matches ||
docElem.webkitMatchesSelector ||
docElem.mozMatchesSelector ||
docElem.oMatchesSelector ||
docElem.msMatchesSelector)))){
assert(function(el){
support.disconnectedMatch=matches.call(el, "*");
matches.call(el, "[s!='']:x");
rbuggyMatches.push("!=", pseudos);
});
}
rbuggyQSA=rbuggyQSA.length&&new RegExp(rbuggyQSA.join("|"));
rbuggyMatches=rbuggyMatches.length&&new RegExp(rbuggyMatches.join("|"));
hasCompare=rnative.test(docElem.compareDocumentPosition);
contains=hasCompare||rnative.test(docElem.contains) ?
function(a, b){
var adown=a.nodeType===9 ? a.documentElement:a,
bup=b&&b.parentNode;
return a===bup||!!(bup&&bup.nodeType===1&&(
adown.contains ?
adown.contains(bup) :
a.compareDocumentPosition&&a.compareDocumentPosition(bup) & 16
));
} :
function(a, b){
if(b){
while((b=b.parentNode)){
if(b===a){
return true;
}}
}
return false;
};
sortOrder=hasCompare ?
function(a, b){
if(a===b){
hasDuplicate=true;
return 0;
}
var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
if(compare){
return compare;
}
compare=(a.ownerDocument||a)===(b.ownerDocument||b) ?
a.compareDocumentPosition(b) :
1;
if(compare & 1 ||
(!support.sortDetached&&b.compareDocumentPosition(a)===compare)){
if(a===document||a.ownerDocument===preferredDoc&&contains(preferredDoc, a)){
return -1;
}
if(b===document||b.ownerDocument===preferredDoc&&contains(preferredDoc, b)){
return 1;
}
return sortInput ?
(indexOf(sortInput, a) - indexOf(sortInput, b)) :
0;
}
return compare & 4 ? -1:1;
} :
function(a, b){
if(a===b){
hasDuplicate=true;
return 0;
}
var cur,
i=0,
aup=a.parentNode,
bup=b.parentNode,
ap=[ a ],
bp=[ b ];
if(!aup||!bup){
return a===document ? -1 :
b===document ? 1 :
aup ? -1 :
bup ? 1 :
sortInput ?
(indexOf(sortInput, a) - indexOf(sortInput, b)) :
0;
}else if(aup===bup){
return siblingCheck(a, b);
}
cur=a;
while((cur=cur.parentNode)){
ap.unshift(cur);
}
cur=b;
while((cur=cur.parentNode)){
bp.unshift(cur);
}
while(ap[i]===bp[i]){
i++;
}
return i ?
siblingCheck(ap[i], bp[i]) :
ap[i]===preferredDoc ? -1 :
bp[i]===preferredDoc ? 1 :
0;
};
return document;
};
Sizzle.matches=function(expr, elements){
return Sizzle(expr, null, null, elements);
};
Sizzle.matchesSelector=function(elem, expr){
if(( elem.ownerDocument||elem)!==document){
setDocument(elem);
}
expr=expr.replace(rattributeQuotes, "='$1']");
if(support.matchesSelector&&documentIsHTML &&
!compilerCache[ expr + " " ] &&
(!rbuggyMatches||!rbuggyMatches.test(expr)) &&
(!rbuggyQSA||!rbuggyQSA.test(expr))){
try {
var ret=matches.call(elem, expr);
if(ret||support.disconnectedMatch ||
elem.document&&elem.document.nodeType!==11){
return ret;
}} catch (e){}}
return Sizzle(expr, document, null, [ elem ]).length > 0;
};
Sizzle.contains=function(context, elem){
if(( context.ownerDocument||context)!==document){
setDocument(context);
}
return contains(context, elem);
};
Sizzle.attr=function(elem, name){
if(( elem.ownerDocument||elem)!==document){
setDocument(elem);
}
var fn=Expr.attrHandle[ name.toLowerCase() ],
val=fn&&hasOwn.call(Expr.attrHandle, name.toLowerCase()) ?
fn(elem, name, !documentIsHTML) :
undefined;
return val!==undefined ?
val :
support.attributes||!documentIsHTML ?
elem.getAttribute(name) :
(val=elem.getAttributeNode(name))&&val.specified ?
val.value :
null;
};
Sizzle.escape=function(sel){
return (sel + "").replace(rcssescape, fcssescape);
};
Sizzle.error=function(msg){
throw new Error("Syntax error, unrecognized expression: " + msg);
};
Sizzle.uniqueSort=function(results){
var elem,
duplicates=[],
j=0,
i=0;
hasDuplicate = !support.detectDuplicates;
sortInput = !support.sortStable&&results.slice(0);
results.sort(sortOrder);
if(hasDuplicate){
while((elem=results[i++])){
if(elem===results[ i ]){
j=duplicates.push(i);
}}
while(j--){
results.splice(duplicates[ j ], 1);
}}
sortInput=null;
return results;
};
getText=Sizzle.getText=function(elem){
var node,
ret="",
i=0,
nodeType=elem.nodeType;
if(!nodeType){
while((node=elem[i++])){
ret +=getText(node);
}}else if(nodeType===1||nodeType===9||nodeType===11){
if(typeof elem.textContent==="string"){
return elem.textContent;
}else{
for(elem=elem.firstChild; elem; elem=elem.nextSibling){
ret +=getText(elem);
}}
}else if(nodeType===3||nodeType===4){
return elem.nodeValue;
}
return ret;
};
Expr=Sizzle.selectors={
cacheLength: 50,
createPseudo: markFunction,
match: matchExpr,
attrHandle: {},
find: {},
relative: {
">": { dir: "parentNode", first: true },
" ": { dir: "parentNode" },
"+": { dir: "previousSibling", first: true },
"~": { dir: "previousSibling" }},
preFilter: {
"ATTR": function(match){
match[1]=match[1].replace(runescape, funescape);
match[3]=(match[3]||match[4]||match[5]||"").replace(runescape, funescape);
if(match[2]==="~="){
match[3]=" " + match[3] + " ";
}
return match.slice(0, 4);
},
"CHILD": function(match){
match[1]=match[1].toLowerCase();
if(match[1].slice(0, 3)==="nth"){
if(!match[3]){
Sizzle.error(match[0]);
}
match[4]=+(match[4] ? match[5] + (match[6]||1):2 *(match[3]==="even"||match[3]==="odd"));
match[5]=+(( match[7] + match[8])||match[3]==="odd");
}else if(match[3]){
Sizzle.error(match[0]);
}
return match;
},
"PSEUDO": function(match){
var excess,
unquoted = !match[6]&&match[2];
if(matchExpr["CHILD"].test(match[0])){
return null;
}
if(match[3]){
match[2]=match[4]||match[5]||"";
}else if(unquoted&&rpseudo.test(unquoted) &&
(excess=tokenize(unquoted, true)) &&
(excess=unquoted.indexOf(")", unquoted.length - excess) - unquoted.length)){
match[0]=match[0].slice(0, excess);
match[2]=unquoted.slice(0, excess);
}
return match.slice(0, 3);
}},
filter: {
"TAG": function(nodeNameSelector){
var nodeName=nodeNameSelector.replace(runescape, funescape).toLowerCase();
return nodeNameSelector==="*" ?
function(){ return true; } :
function(elem){
return elem.nodeName&&elem.nodeName.toLowerCase()===nodeName;
};},
"CLASS": function(className){
var pattern=classCache[ className + " " ];
return pattern ||
(pattern=new RegExp("(^|" + whitespace + ")" + className + "(" + whitespace + "|$)")) &&
classCache(className, function(elem){
return pattern.test(typeof elem.className==="string"&&elem.className||typeof elem.getAttribute!=="undefined"&&elem.getAttribute("class")||"");
});
},
"ATTR": function(name, operator, check){
return function(elem){
var result=Sizzle.attr(elem, name);
if(result==null){
return operator==="!=";
}
if(!operator){
return true;
}
result +="";
return operator==="=" ? result===check :
operator==="!=" ? result!==check :
operator==="^=" ? check&&result.indexOf(check)===0 :
operator==="*=" ? check&&result.indexOf(check) > -1 :
operator==="$=" ? check&&result.slice(-check.length)===check :
operator==="~=" ?(" " + result.replace(rwhitespace, " ") + " ").indexOf(check) > -1 :
operator==="|=" ? result===check||result.slice(0, check.length + 1)===check + "-" :
false;
};},
"CHILD": function(type, what, argument, first, last){
var simple=type.slice(0, 3)!=="nth",
forward=type.slice(-4)!=="last",
ofType=what==="of-type";
return first===1&&last===0 ?
function(elem){
return !!elem.parentNode;
} :
function(elem, context, xml){
var cache, uniqueCache, outerCache, node, nodeIndex, start,
dir=simple!==forward ? "nextSibling":"previousSibling",
parent=elem.parentNode,
name=ofType&&elem.nodeName.toLowerCase(),
useCache = !xml&&!ofType,
diff=false;
if(parent){
if(simple){
while(dir){
node=elem;
while((node=node[ dir ])){
if(ofType ?
node.nodeName.toLowerCase()===name :
node.nodeType===1){
return false;
}}
start=dir=type==="only"&&!start&&"nextSibling";
}
return true;
}
start=[ forward ? parent.firstChild:parent.lastChild ];
if(forward&&useCache){
node=parent;
outerCache=node[ expando ]||(node[ expando ]={});
uniqueCache=outerCache[ node.uniqueID ] ||
(outerCache[ node.uniqueID ]={});
cache=uniqueCache[ type ]||[];
nodeIndex=cache[ 0 ]===dirruns&&cache[ 1 ];
diff=nodeIndex&&cache[ 2 ];
node=nodeIndex&&parent.childNodes[ nodeIndex ];
while((node=++nodeIndex&&node&&node[ dir ] ||
(diff=nodeIndex=0)||start.pop())){
if(node.nodeType===1&&++diff&&node===elem){
uniqueCache[ type ]=[ dirruns, nodeIndex, diff ];
break;
}}
}else{
if(useCache){
node=elem;
outerCache=node[ expando ]||(node[ expando ]={});
uniqueCache=outerCache[ node.uniqueID ] ||
(outerCache[ node.uniqueID ]={});
cache=uniqueCache[ type ]||[];
nodeIndex=cache[ 0 ]===dirruns&&cache[ 1 ];
diff=nodeIndex;
}
if(diff===false){
while((node=++nodeIndex&&node&&node[ dir ] ||
(diff=nodeIndex=0)||start.pop())){
if(( ofType ?
node.nodeName.toLowerCase()===name :
node.nodeType===1) &&
++diff){
if(useCache){
outerCache=node[ expando ]||(node[ expando ]={});
uniqueCache=outerCache[ node.uniqueID ] ||
(outerCache[ node.uniqueID ]={});
uniqueCache[ type ]=[ dirruns, diff ];
}
if(node===elem){
break;
}}
}}
}
diff -=last;
return diff===first||(diff % first===0&&diff / first >=0);
}};},
"PSEUDO": function(pseudo, argument){
var args,
fn=Expr.pseudos[ pseudo ]||Expr.setFilters[ pseudo.toLowerCase() ] ||
Sizzle.error("unsupported pseudo: " + pseudo);
if(fn[ expando ]){
return fn(argument);
}
if(fn.length > 1){
args=[ pseudo, pseudo, "", argument ];
return Expr.setFilters.hasOwnProperty(pseudo.toLowerCase()) ?
markFunction(function(seed, matches){
var idx,
matched=fn(seed, argument),
i=matched.length;
while(i--){
idx=indexOf(seed, matched[i]);
seed[ idx ] = !(matches[ idx ]=matched[i]);
}}) :
function(elem){
return fn(elem, 0, args);
};}
return fn;
}},
pseudos: {
"not": markFunction(function(selector){
var input=[],
results=[],
matcher=compile(selector.replace(rtrim, "$1"));
return matcher[ expando ] ?
markFunction(function(seed, matches, context, xml){
var elem,
unmatched=matcher(seed, null, xml, []),
i=seed.length;
while(i--){
if((elem=unmatched[i])){
seed[i] = !(matches[i]=elem);
}}
}) :
function(elem, context, xml){
input[0]=elem;
matcher(input, null, xml, results);
input[0]=null;
return !results.pop();
};}),
"has": markFunction(function(selector){
return function(elem){
return Sizzle(selector, elem).length > 0;
};}),
"contains": markFunction(function(text){
text=text.replace(runescape, funescape);
return function(elem){
return(elem.textContent||elem.innerText||getText(elem)).indexOf(text) > -1;
};}),
"lang": markFunction(function(lang){
if(!ridentifier.test(lang||"")){
Sizzle.error("unsupported lang: " + lang);
}
lang=lang.replace(runescape, funescape).toLowerCase();
return function(elem){
var elemLang;
do {
if((elemLang=documentIsHTML ?
elem.lang :
elem.getAttribute("xml:lang")||elem.getAttribute("lang"))){
elemLang=elemLang.toLowerCase();
return elemLang===lang||elemLang.indexOf(lang + "-")===0;
}} while((elem=elem.parentNode)&&elem.nodeType===1);
return false;
};}),
"target": function(elem){
var hash=window.location&&window.location.hash;
return hash&&hash.slice(1)===elem.id;
},
"root": function(elem){
return elem===docElem;
},
"focus": function(elem){
return elem===document.activeElement&&(!document.hasFocus||document.hasFocus())&&!!(elem.type||elem.href||~elem.tabIndex);
},
"enabled": createDisabledPseudo(false),
"disabled": createDisabledPseudo(true),
"checked": function(elem){
var nodeName=elem.nodeName.toLowerCase();
return (nodeName==="input"&&!!elem.checked)||(nodeName==="option"&&!!elem.selected);
},
"selected": function(elem){
if(elem.parentNode){
elem.parentNode.selectedIndex;
}
return elem.selected===true;
},
"empty": function(elem){
for(elem=elem.firstChild; elem; elem=elem.nextSibling){
if(elem.nodeType < 6){
return false;
}}
return true;
},
"parent": function(elem){
return !Expr.pseudos["empty"](elem);
},
"header": function(elem){
return rheader.test(elem.nodeName);
},
"input": function(elem){
return rinputs.test(elem.nodeName);
},
"button": function(elem){
var name=elem.nodeName.toLowerCase();
return name==="input"&&elem.type==="button"||name==="button";
},
"text": function(elem){
var attr;
return elem.nodeName.toLowerCase()==="input" &&
elem.type==="text" &&
((attr=elem.getAttribute("type"))==null||attr.toLowerCase()==="text");
},
"first": createPositionalPseudo(function(){
return [ 0 ];
}),
"last": createPositionalPseudo(function(matchIndexes, length){
return [ length - 1 ];
}),
"eq": createPositionalPseudo(function(matchIndexes, length, argument){
return [ argument < 0 ? argument + length:argument ];
}),
"even": createPositionalPseudo(function(matchIndexes, length){
var i=0;
for(; i < length; i +=2){
matchIndexes.push(i);
}
return matchIndexes;
}),
"odd": createPositionalPseudo(function(matchIndexes, length){
var i=1;
for(; i < length; i +=2){
matchIndexes.push(i);
}
return matchIndexes;
}),
"lt": createPositionalPseudo(function(matchIndexes, length, argument){
var i=argument < 0 ? argument + length:argument;
for(; --i >=0;){
matchIndexes.push(i);
}
return matchIndexes;
}),
"gt": createPositionalPseudo(function(matchIndexes, length, argument){
var i=argument < 0 ? argument + length:argument;
for(; ++i < length;){
matchIndexes.push(i);
}
return matchIndexes;
})
}};
Expr.pseudos["nth"]=Expr.pseudos["eq"];
for(i in { radio: true, checkbox: true, file: true, password: true, image: true }){
Expr.pseudos[ i ]=createInputPseudo(i);
}
for(i in { submit: true, reset: true }){
Expr.pseudos[ i ]=createButtonPseudo(i);
}
function setFilters(){}
setFilters.prototype=Expr.filters=Expr.pseudos;
Expr.setFilters=new setFilters();
tokenize=Sizzle.tokenize=function(selector, parseOnly){
var matched, match, tokens, type,
soFar, groups, preFilters,
cached=tokenCache[ selector + " " ];
if(cached){
return parseOnly ? 0:cached.slice(0);
}
soFar=selector;
groups=[];
preFilters=Expr.preFilter;
while(soFar){
if(!matched||(match=rcomma.exec(soFar))){
if(match){
soFar=soFar.slice(match[0].length)||soFar;
}
groups.push((tokens=[]));
}
matched=false;
if((match=rcombinators.exec(soFar))){
matched=match.shift();
tokens.push({
value: matched,
type: match[0].replace(rtrim, " ")
});
soFar=soFar.slice(matched.length);
}
for(type in Expr.filter){
if((match=matchExpr[ type ].exec(soFar))&&(!preFilters[ type ] ||
(match=preFilters[ type ](match)))){
matched=match.shift();
tokens.push({
value: matched,
type: type,
matches: match
});
soFar=soFar.slice(matched.length);
}}
if(!matched){
break;
}}
return parseOnly ?
soFar.length :
soFar ?
Sizzle.error(selector) :
tokenCache(selector, groups).slice(0);
};
function toSelector(tokens){
var i=0,
len=tokens.length,
selector="";
for(; i < len; i++){
selector +=tokens[i].value;
}
return selector;
}
function addCombinator(matcher, combinator, base){
var dir=combinator.dir,
skip=combinator.next,
key=skip||dir,
checkNonElements=base&&key==="parentNode",
doneName=done++;
return combinator.first ?
function(elem, context, xml){
while((elem=elem[ dir ])){
if(elem.nodeType===1||checkNonElements){
return matcher(elem, context, xml);
}}
return false;
} :
function(elem, context, xml){
var oldCache, uniqueCache, outerCache,
newCache=[ dirruns, doneName ];
if(xml){
while((elem=elem[ dir ])){
if(elem.nodeType===1||checkNonElements){
if(matcher(elem, context, xml)){
return true;
}}
}}else{
while((elem=elem[ dir ])){
if(elem.nodeType===1||checkNonElements){
outerCache=elem[ expando ]||(elem[ expando ]={});
uniqueCache=outerCache[ elem.uniqueID ]||(outerCache[ elem.uniqueID ]={});
if(skip&&skip===elem.nodeName.toLowerCase()){
elem=elem[ dir ]||elem;
}else if((oldCache=uniqueCache[ key ]) &&
oldCache[ 0 ]===dirruns&&oldCache[ 1 ]===doneName){
return (newCache[ 2 ]=oldCache[ 2 ]);
}else{
uniqueCache[ key ]=newCache;
if((newCache[ 2 ]=matcher(elem, context, xml))){
return true;
}}
}}
}
return false;
};}
function elementMatcher(matchers){
return matchers.length > 1 ?
function(elem, context, xml){
var i=matchers.length;
while(i--){
if(!matchers[i](elem, context, xml)){
return false;
}}
return true;
} :
matchers[0];
}
function multipleContexts(selector, contexts, results){
var i=0,
len=contexts.length;
for(; i < len; i++){
Sizzle(selector, contexts[i], results);
}
return results;
}
function condense(unmatched, map, filter, context, xml){
var elem,
newUnmatched=[],
i=0,
len=unmatched.length,
mapped=map!=null;
for(; i < len; i++){
if((elem=unmatched[i])){
if(!filter||filter(elem, context, xml)){
newUnmatched.push(elem);
if(mapped){
map.push(i);
}}
}}
return newUnmatched;
}
function setMatcher(preFilter, selector, matcher, postFilter, postFinder, postSelector){
if(postFilter&&!postFilter[ expando ]){
postFilter=setMatcher(postFilter);
}
if(postFinder&&!postFinder[ expando ]){
postFinder=setMatcher(postFinder, postSelector);
}
return markFunction(function(seed, results, context, xml){
var temp, i, elem,
preMap=[],
postMap=[],
preexisting=results.length,
elems=seed||multipleContexts(selector||"*", context.nodeType ? [ context ]:context, []),
matcherIn=preFilter&&(seed||!selector) ?
condense(elems, preMap, preFilter, context, xml) :
elems,
matcherOut=matcher ?
postFinder||(seed ? preFilter:preexisting||postFilter) ?
[] :
results :
matcherIn;
if(matcher){
matcher(matcherIn, matcherOut, context, xml);
}
if(postFilter){
temp=condense(matcherOut, postMap);
postFilter(temp, [], context, xml);
i=temp.length;
while(i--){
if((elem=temp[i])){
matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ]=elem);
}}
}
if(seed){
if(postFinder||preFilter){
if(postFinder){
temp=[];
i=matcherOut.length;
while(i--){
if((elem=matcherOut[i])){
temp.push((matcherIn[i]=elem));
}}
postFinder(null, (matcherOut=[]), temp, xml);
}
i=matcherOut.length;
while(i--){
if((elem=matcherOut[i]) &&
(temp=postFinder ? indexOf(seed, elem):preMap[i]) > -1){
seed[temp] = !(results[temp]=elem);
}}
}}else{
matcherOut=condense(
matcherOut===results ?
matcherOut.splice(preexisting, matcherOut.length) :
matcherOut
);
if(postFinder){
postFinder(null, results, matcherOut, xml);
}else{
push.apply(results, matcherOut);
}}
});
}
function matcherFromTokens(tokens){
var checkContext, matcher, j,
len=tokens.length,
leadingRelative=Expr.relative[ tokens[0].type ],
implicitRelative=leadingRelative||Expr.relative[" "],
i=leadingRelative ? 1:0,
matchContext=addCombinator(function(elem){
return elem===checkContext;
}, implicitRelative, true),
matchAnyContext=addCombinator(function(elem){
return indexOf(checkContext, elem) > -1;
}, implicitRelative, true),
matchers=[ function(elem, context, xml){
var ret=(!leadingRelative&&(xml||context!==outermostContext))||(
(checkContext=context).nodeType ?
matchContext(elem, context, xml) :
matchAnyContext(elem, context, xml));
checkContext=null;
return ret;
} ];
for(; i < len; i++){
if((matcher=Expr.relative[ tokens[i].type ])){
matchers=[ addCombinator(elementMatcher(matchers), matcher) ];
}else{
matcher=Expr.filter[ tokens[i].type ].apply(null, tokens[i].matches);
if(matcher[ expando ]){
j=++i;
for(; j < len; j++){
if(Expr.relative[ tokens[j].type ]){
break;
}}
return setMatcher(
i > 1&&elementMatcher(matchers),
i > 1&&toSelector(
tokens.slice(0, i - 1).concat({ value: tokens[ i - 2 ].type===" " ? "*":"" })
).replace(rtrim, "$1"),
matcher,
i < j&&matcherFromTokens(tokens.slice(i, j)),
j < len&&matcherFromTokens((tokens=tokens.slice(j))),
j < len&&toSelector(tokens)
);
}
matchers.push(matcher);
}}
return elementMatcher(matchers);
}
function matcherFromGroupMatchers(elementMatchers, setMatchers){
var bySet=setMatchers.length > 0,
byElement=elementMatchers.length > 0,
superMatcher=function(seed, context, xml, results, outermost){
var elem, j, matcher,
matchedCount=0,
i="0",
unmatched=seed&&[],
setMatched=[],
contextBackup=outermostContext,
elems=seed||byElement&&Expr.find["TAG"]("*", outermost),
dirrunsUnique=(dirruns +=contextBackup==null ? 1:Math.random()||0.1),
len=elems.length;
if(outermost){
outermostContext=context===document||context||outermost;
}
for(; i!==len&&(elem=elems[i])!=null; i++){
if(byElement&&elem){
j=0;
if(!context&&elem.ownerDocument!==document){
setDocument(elem);
xml = !documentIsHTML;
}
while((matcher=elementMatchers[j++])){
if(matcher(elem, context||document, xml)){
results.push(elem);
break;
}}
if(outermost){
dirruns=dirrunsUnique;
}}
if(bySet){
if((elem = !matcher&&elem)){
matchedCount--;
}
if(seed){
unmatched.push(elem);
}}
}
matchedCount +=i;
if(bySet&&i!==matchedCount){
j=0;
while((matcher=setMatchers[j++])){
matcher(unmatched, setMatched, context, xml);
}
if(seed){
if(matchedCount > 0){
while(i--){
if(!(unmatched[i]||setMatched[i])){
setMatched[i]=pop.call(results);
}}
}
setMatched=condense(setMatched);
}
push.apply(results, setMatched);
if(outermost&&!seed&&setMatched.length > 0 &&
(matchedCount + setMatchers.length) > 1){
Sizzle.uniqueSort(results);
}}
if(outermost){
dirruns=dirrunsUnique;
outermostContext=contextBackup;
}
return unmatched;
};
return bySet ?
markFunction(superMatcher) :
superMatcher;
}
compile=Sizzle.compile=function(selector, match ){
var i,
setMatchers=[],
elementMatchers=[],
cached=compilerCache[ selector + " " ];
if(!cached){
if(!match){
match=tokenize(selector);
}
i=match.length;
while(i--){
cached=matcherFromTokens(match[i]);
if(cached[ expando ]){
setMatchers.push(cached);
}else{
elementMatchers.push(cached);
}}
cached=compilerCache(selector, matcherFromGroupMatchers(elementMatchers, setMatchers));
cached.selector=selector;
}
return cached;
};
select=Sizzle.select=function(selector, context, results, seed){
var i, tokens, token, type, find,
compiled=typeof selector==="function"&&selector,
match = !seed&&tokenize((selector=compiled.selector||selector));
results=results||[];
if(match.length===1){
tokens=match[0]=match[0].slice(0);
if(tokens.length > 2&&(token=tokens[0]).type==="ID" &&
context.nodeType===9&&documentIsHTML&&Expr.relative[ tokens[1].type ]){
context=(Expr.find["ID"](token.matches[0].replace(runescape, funescape), context)||[])[0];
if(!context){
return results;
}else if(compiled){
context=context.parentNode;
}
selector=selector.slice(tokens.shift().value.length);
}
i=matchExpr["needsContext"].test(selector) ? 0:tokens.length;
while(i--){
token=tokens[i];
if(Expr.relative[ (type=token.type) ]){
break;
}
if((find=Expr.find[ type ])){
if((seed=find(
token.matches[0].replace(runescape, funescape),
rsibling.test(tokens[0].type)&&testContext(context.parentNode)||context
))){
tokens.splice(i, 1);
selector=seed.length&&toSelector(tokens);
if(!selector){
push.apply(results, seed);
return results;
}
break;
}}
}}
(compiled||compile(selector, match))(
seed,
context,
!documentIsHTML,
results,
!context||rsibling.test(selector)&&testContext(context.parentNode)||context
);
return results;
};
support.sortStable=expando.split("").sort(sortOrder).join("")===expando;
support.detectDuplicates = !!hasDuplicate;
setDocument();
support.sortDetached=assert(function(el){
return el.compareDocumentPosition(document.createElement("fieldset")) & 1;
});
if(!assert(function(el){
el.innerHTML="<a href='#'></a>";
return el.firstChild.getAttribute("href")==="#" ;
})){
addHandle("type|href|height|width", function(elem, name, isXML){
if(!isXML){
return elem.getAttribute(name, name.toLowerCase()==="type" ? 1:2);
}});
}
if(!support.attributes||!assert(function(el){
el.innerHTML="<input/>";
el.firstChild.setAttribute("value", "");
return el.firstChild.getAttribute("value")==="";
})){
addHandle("value", function(elem, name, isXML){
if(!isXML&&elem.nodeName.toLowerCase()==="input"){
return elem.defaultValue;
}});
}
if(!assert(function(el){
return el.getAttribute("disabled")==null;
})){
addHandle(booleans, function(elem, name, isXML){
var val;
if(!isXML){
return elem[ name ]===true ? name.toLowerCase() :
(val=elem.getAttributeNode(name))&&val.specified ?
val.value :
null;
}});
}
return Sizzle;
})(window);
jQuery.find=Sizzle;
jQuery.expr=Sizzle.selectors;
jQuery.expr[ ":" ]=jQuery.expr.pseudos;
jQuery.uniqueSort=jQuery.unique=Sizzle.uniqueSort;
jQuery.text=Sizzle.getText;
jQuery.isXMLDoc=Sizzle.isXML;
jQuery.contains=Sizzle.contains;
jQuery.escapeSelector=Sizzle.escape;
var dir=function(elem, dir, until){
var matched=[],
truncate=until!==undefined;
while(( elem=elem[ dir ])&&elem.nodeType!==9){
if(elem.nodeType===1){
if(truncate&&jQuery(elem).is(until)){
break;
}
matched.push(elem);
}}
return matched;
};
var siblings=function(n, elem){
var matched=[];
for(; n; n=n.nextSibling){
if(n.nodeType===1&&n!==elem){
matched.push(n);
}}
return matched;
};
var rneedsContext=jQuery.expr.match.needsContext;
function nodeName(elem, name){
return elem.nodeName&&elem.nodeName.toLowerCase()===name.toLowerCase();
};
var rsingleTag=(/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i);
function winnow(elements, qualifier, not){
if(isFunction(qualifier)){
return jQuery.grep(elements, function(elem, i){
return !!qualifier.call(elem, i, elem)!==not;
});
}
if(qualifier.nodeType){
return jQuery.grep(elements, function(elem){
return(elem===qualifier)!==not;
});
}
if(typeof qualifier!=="string"){
return jQuery.grep(elements, function(elem){
return(indexOf.call(qualifier, elem) > -1)!==not;
});
}
return jQuery.filter(qualifier, elements, not);
}
jQuery.filter=function(expr, elems, not){
var elem=elems[ 0 ];
if(not){
expr=":not(" + expr + ")";
}
if(elems.length===1&&elem.nodeType===1){
return jQuery.find.matchesSelector(elem, expr) ? [ elem ]:[];
}
return jQuery.find.matches(expr, jQuery.grep(elems, function(elem){
return elem.nodeType===1;
}));
};
jQuery.fn.extend({
find: function(selector){
var i, ret,
len=this.length,
self=this;
if(typeof selector!=="string"){
return this.pushStack(jQuery(selector).filter(function(){
for(i=0; i < len; i++){
if(jQuery.contains(self[ i ], this)){
return true;
}}
}));
}
ret=this.pushStack([]);
for(i=0; i < len; i++){
jQuery.find(selector, self[ i ], ret);
}
return len > 1 ? jQuery.uniqueSort(ret):ret;
},
filter: function(selector){
return this.pushStack(winnow(this, selector||[], false));
},
not: function(selector){
return this.pushStack(winnow(this, selector||[], true));
},
is: function(selector){
return !!winnow(
this,
typeof selector==="string"&&rneedsContext.test(selector) ?
jQuery(selector) :
selector||[],
false
).length;
}});
var rootjQuery,
rquickExpr=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
init=jQuery.fn.init=function(selector, context, root){
var match, elem;
if(!selector){
return this;
}
root=root||rootjQuery;
if(typeof selector==="string"){
if(selector[ 0 ]==="<" &&
selector[ selector.length - 1 ]===">" &&
selector.length >=3){
match=[ null, selector, null ];
}else{
match=rquickExpr.exec(selector);
}
if(match&&(match[ 1 ]||!context)){
if(match[ 1 ]){
context=context instanceof jQuery ? context[ 0 ]:context;
jQuery.merge(this, jQuery.parseHTML(match[ 1 ],
context&&context.nodeType ? context.ownerDocument||context:document,
true
));
if(rsingleTag.test(match[ 1 ])&&jQuery.isPlainObject(context)){
for(match in context){
if(isFunction(this[ match ])){
this[ match ](context[ match ]);
}else{
this.attr(match, context[ match ]);
}}
}
return this;
}else{
elem=document.getElementById(match[ 2 ]);
if(elem){
this[ 0 ]=elem;
this.length=1;
}
return this;
}}else if(!context||context.jquery){
return(context||root).find(selector);
}else{
return this.constructor(context).find(selector);
}}else if(selector.nodeType){
this[ 0 ]=selector;
this.length=1;
return this;
}else if(isFunction(selector)){
return root.ready!==undefined ?
root.ready(selector) :
selector(jQuery);
}
return jQuery.makeArray(selector, this);
};
init.prototype=jQuery.fn;
rootjQuery=jQuery(document);
var rparentsprev=/^(?:parents|prev(?:Until|All))/,
guaranteedUnique={
children: true,
contents: true,
next: true,
prev: true
};
jQuery.fn.extend({
has: function(target){
var targets=jQuery(target, this),
l=targets.length;
return this.filter(function(){
var i=0;
for(; i < l; i++){
if(jQuery.contains(this, targets[ i ])){
return true;
}}
});
},
closest: function(selectors, context){
var cur,
i=0,
l=this.length,
matched=[],
targets=typeof selectors!=="string"&&jQuery(selectors);
if(!rneedsContext.test(selectors)){
for(; i < l; i++){
for(cur=this[ i ]; cur&&cur!==context; cur=cur.parentNode){
if(cur.nodeType < 11&&(targets ?
targets.index(cur) > -1 :
cur.nodeType===1 &&
jQuery.find.matchesSelector(cur, selectors))){
matched.push(cur);
break;
}}
}}
return this.pushStack(matched.length > 1 ? jQuery.uniqueSort(matched):matched);
},
index: function(elem){
if(!elem){
return(this[ 0 ]&&this[ 0 ].parentNode) ? this.first().prevAll().length:-1;
}
if(typeof elem==="string"){
return indexOf.call(jQuery(elem), this[ 0 ]);
}
return indexOf.call(this,
elem.jquery ? elem[ 0 ]:elem
);
},
add: function(selector, context){
return this.pushStack(jQuery.uniqueSort(jQuery.merge(this.get(), jQuery(selector, context))
)
);
},
addBack: function(selector){
return this.add(selector==null ?
this.prevObject:this.prevObject.filter(selector)
);
}});
function sibling(cur, dir){
while(( cur=cur[ dir ])&&cur.nodeType!==1){}
return cur;
}
jQuery.each({
parent: function(elem){
var parent=elem.parentNode;
return parent&&parent.nodeType!==11 ? parent:null;
},
parents: function(elem){
return dir(elem, "parentNode");
},
parentsUntil: function(elem, i, until){
return dir(elem, "parentNode", until);
},
next: function(elem){
return sibling(elem, "nextSibling");
},
prev: function(elem){
return sibling(elem, "previousSibling");
},
nextAll: function(elem){
return dir(elem, "nextSibling");
},
prevAll: function(elem){
return dir(elem, "previousSibling");
},
nextUntil: function(elem, i, until){
return dir(elem, "nextSibling", until);
},
prevUntil: function(elem, i, until){
return dir(elem, "previousSibling", until);
},
siblings: function(elem){
return siblings(( elem.parentNode||{}).firstChild, elem);
},
children: function(elem){
return siblings(elem.firstChild);
},
contents: function(elem){
if(nodeName(elem, "iframe")){
return elem.contentDocument;
}
if(nodeName(elem, "template")){
elem=elem.content||elem;
}
return jQuery.merge([], elem.childNodes);
}}, function(name, fn){
jQuery.fn[ name ]=function(until, selector){
var matched=jQuery.map(this, fn, until);
if(name.slice(-5)!=="Until"){
selector=until;
}
if(selector&&typeof selector==="string"){
matched=jQuery.filter(selector, matched);
}
if(this.length > 1){
if(!guaranteedUnique[ name ]){
jQuery.uniqueSort(matched);
}
if(rparentsprev.test(name)){
matched.reverse();
}}
return this.pushStack(matched);
};});
var rnothtmlwhite=(/[^\x20\t\r\n\f]+/g);
function createOptions(options){
var object={};
jQuery.each(options.match(rnothtmlwhite)||[], function(_, flag){
object[ flag ]=true;
});
return object;
}
jQuery.Callbacks=function(options){
options=typeof options==="string" ?
createOptions(options) :
jQuery.extend({}, options);
var
firing,
memory,
fired,
locked,
list=[],
queue=[],
firingIndex=-1,
fire=function(){
locked=locked||options.once;
fired=firing=true;
for(; queue.length; firingIndex=-1){
memory=queue.shift();
while ( ++firingIndex < list.length){
if(list[ firingIndex ].apply(memory[ 0 ], memory[ 1 ])===false &&
options.stopOnFalse){
firingIndex=list.length;
memory=false;
}}
}
if(!options.memory){
memory=false;
}
firing=false;
if(locked){
if(memory){
list=[];
}else{
list="";
}}
},
self={
add: function(){
if(list){
if(memory&&!firing){
firingIndex=list.length - 1;
queue.push(memory);
}
(function add(args){
jQuery.each(args, function(_, arg){
if(isFunction(arg)){
if(!options.unique||!self.has(arg)){
list.push(arg);
}}else if(arg&&arg.length&&toType(arg)!=="string"){
add(arg);
}});
})(arguments);
if(memory&&!firing){
fire();
}}
return this;
},
remove: function(){
jQuery.each(arguments, function(_, arg){
var index;
while(( index=jQuery.inArray(arg, list, index)) > -1){
list.splice(index, 1);
if(index <=firingIndex){
firingIndex--;
}}
});
return this;
},
has: function(fn){
return fn ?
jQuery.inArray(fn, list) > -1 :
list.length > 0;
},
empty: function(){
if(list){
list=[];
}
return this;
},
disable: function(){
locked=queue=[];
list=memory="";
return this;
},
disabled: function(){
return !list;
},
lock: function(){
locked=queue=[];
if(!memory&&!firing){
list=memory="";
}
return this;
},
locked: function(){
return !!locked;
},
fireWith: function(context, args){
if(!locked){
args=args||[];
args=[ context, args.slice ? args.slice():args ];
queue.push(args);
if(!firing){
fire();
}}
return this;
},
fire: function(){
self.fireWith(this, arguments);
return this;
},
fired: function(){
return !!fired;
}};
return self;
};
function Identity(v){
return v;
}
function Thrower(ex){
throw ex;
}
function adoptValue(value, resolve, reject, noValue){
var method;
try {
if(value&&isFunction(( method=value.promise))){
method.call(value).done(resolve).fail(reject);
}else if(value&&isFunction(( method=value.then))){
method.call(value, resolve, reject);
}else{
resolve.apply(undefined, [ value ].slice(noValue));
}} catch(value){
reject.apply(undefined, [ value ]);
}}
jQuery.extend({
Deferred: function(func){
var tuples=[
[ "notify", "progress", jQuery.Callbacks("memory"),
jQuery.Callbacks("memory"), 2 ],
[ "resolve", "done", jQuery.Callbacks("once memory"),
jQuery.Callbacks("once memory"), 0, "resolved" ],
[ "reject", "fail", jQuery.Callbacks("once memory"),
jQuery.Callbacks("once memory"), 1, "rejected" ]
],
state="pending",
promise={
state: function(){
return state;
},
always: function(){
deferred.done(arguments).fail(arguments);
return this;
},
"catch": function(fn){
return promise.then(null, fn);
},
pipe: function(){
var fns=arguments;
return jQuery.Deferred(function(newDefer){
jQuery.each(tuples, function(i, tuple){
var fn=isFunction(fns[ tuple[ 4 ] ])&&fns[ tuple[ 4 ] ];
deferred[ tuple[ 1 ] ](function(){
var returned=fn&&fn.apply(this, arguments);
if(returned&&isFunction(returned.promise)){
returned.promise()
.progress(newDefer.notify)
.done(newDefer.resolve)
.fail(newDefer.reject);
}else{
newDefer[ tuple[ 0 ] + "With" ](
this,
fn ? [ returned ]:arguments
);
}});
});
fns=null;
}).promise();
},
then: function(onFulfilled, onRejected, onProgress){
var maxDepth=0;
function resolve(depth, deferred, handler, special){
return function(){
var that=this,
args=arguments,
mightThrow=function(){
var returned, then;
if(depth < maxDepth){
return;
}
returned=handler.apply(that, args);
if(returned===deferred.promise()){
throw new TypeError("Thenable self-resolution");
}
then=returned &&
(typeof returned==="object" ||
typeof returned==="function") &&
returned.then;
if(isFunction(then)){
if(special){
then.call(returned,
resolve(maxDepth, deferred, Identity, special),
resolve(maxDepth, deferred, Thrower, special)
);
}else{
maxDepth++;
then.call(returned,
resolve(maxDepth, deferred, Identity, special),
resolve(maxDepth, deferred, Thrower, special),
resolve(maxDepth, deferred, Identity,
deferred.notifyWith)
);
}}else{
if(handler!==Identity){
that=undefined;
args=[ returned ];
}
(special||deferred.resolveWith)(that, args);
}},
process=special ?
mightThrow :
function(){
try {
mightThrow();
} catch(e){
if(jQuery.Deferred.exceptionHook){
jQuery.Deferred.exceptionHook(e,
process.stackTrace);
}
if(depth + 1 >=maxDepth){
if(handler!==Thrower){
that=undefined;
args=[ e ];
}
deferred.rejectWith(that, args);
}}
};
if(depth){
process();
}else{
if(jQuery.Deferred.getStackHook){
process.stackTrace=jQuery.Deferred.getStackHook();
}
window.setTimeout(process);
}};}
return jQuery.Deferred(function(newDefer){
tuples[ 0 ][ 3 ].add(resolve(
0,
newDefer,
isFunction(onProgress) ?
onProgress :
Identity,
newDefer.notifyWith
)
);
tuples[ 1 ][ 3 ].add(resolve(
0,
newDefer,
isFunction(onFulfilled) ?
onFulfilled :
Identity
)
);
tuples[ 2 ][ 3 ].add(resolve(
0,
newDefer,
isFunction(onRejected) ?
onRejected :
Thrower
)
);
}).promise();
},
promise: function(obj){
return obj!=null ? jQuery.extend(obj, promise):promise;
}},
deferred={};
jQuery.each(tuples, function(i, tuple){
var list=tuple[ 2 ],
stateString=tuple[ 5 ];
promise[ tuple[ 1 ] ]=list.add;
if(stateString){
list.add(function(){
state=stateString;
},
tuples[ 3 - i ][ 2 ].disable,
tuples[ 3 - i ][ 3 ].disable,
tuples[ 0 ][ 2 ].lock,
tuples[ 0 ][ 3 ].lock
);
}
list.add(tuple[ 3 ].fire);
deferred[ tuple[ 0 ] ]=function(){
deferred[ tuple[ 0 ] + "With" ](this===deferred ? undefined:this, arguments);
return this;
};
deferred[ tuple[ 0 ] + "With" ]=list.fireWith;
});
promise.promise(deferred);
if(func){
func.call(deferred, deferred);
}
return deferred;
},
when: function(singleValue){
var
remaining=arguments.length,
i=remaining,
resolveContexts=Array(i),
resolveValues=slice.call(arguments),
master=jQuery.Deferred(),
updateFunc=function(i){
return function(value){
resolveContexts[ i ]=this;
resolveValues[ i ]=arguments.length > 1 ? slice.call(arguments):value;
if(!(--remaining)){
master.resolveWith(resolveContexts, resolveValues);
}};};
if(remaining <=1){
adoptValue(singleValue, master.done(updateFunc(i)).resolve, master.reject,
!remaining);
if(master.state()==="pending" ||
isFunction(resolveValues[ i ]&&resolveValues[ i ].then)){
return master.then();
}}
while(i--){
adoptValue(resolveValues[ i ], updateFunc(i), master.reject);
}
return master.promise();
}});
var rerrorNames=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
jQuery.Deferred.exceptionHook=function(error, stack){
if(window.console&&window.console.warn&&error&&rerrorNames.test(error.name)){
window.console.warn("jQuery.Deferred exception: " + error.message, error.stack, stack);
}};
jQuery.readyException=function(error){
window.setTimeout(function(){
throw error;
});
};
var readyList=jQuery.Deferred();
jQuery.fn.ready=function(fn){
readyList
.then(fn)
.catch(function(error){
jQuery.readyException(error);
});
return this;
};
jQuery.extend({
isReady: false,
readyWait: 1,
ready: function(wait){
if(wait===true ? --jQuery.readyWait:jQuery.isReady){
return;
}
jQuery.isReady=true;
if(wait!==true&&--jQuery.readyWait > 0){
return;
}
readyList.resolveWith(document, [ jQuery ]);
}});
jQuery.ready.then=readyList.then;
function completed(){
document.removeEventListener("DOMContentLoaded", completed);
window.removeEventListener("load", completed);
jQuery.ready();
}
if(document.readyState==="complete" ||
(document.readyState!=="loading"&&!document.documentElement.doScroll)){
window.setTimeout(jQuery.ready);
}else{
document.addEventListener("DOMContentLoaded", completed);
window.addEventListener("load", completed);
}
var access=function(elems, fn, key, value, chainable, emptyGet, raw){
var i=0,
len=elems.length,
bulk=key==null;
if(toType(key)==="object"){
chainable=true;
for(i in key){
access(elems, fn, i, key[ i ], true, emptyGet, raw);
}}else if(value!==undefined){
chainable=true;
if(!isFunction(value)){
raw=true;
}
if(bulk){
if(raw){
fn.call(elems, value);
fn=null;
}else{
bulk=fn;
fn=function(elem, key, value){
return bulk.call(jQuery(elem), value);
};}}
if(fn){
for(; i < len; i++){
fn(
elems[ i ], key, raw ?
value :
value.call(elems[ i ], i, fn(elems[ i ], key))
);
}}
}
if(chainable){
return elems;
}
if(bulk){
return fn.call(elems);
}
return len ? fn(elems[ 0 ], key):emptyGet;
};
var rmsPrefix=/^-ms-/,
rdashAlpha=/-([a-z])/g;
function fcamelCase(all, letter){
return letter.toUpperCase();
}
function camelCase(string){
return string.replace(rmsPrefix, "ms-").replace(rdashAlpha, fcamelCase);
}
var acceptData=function(owner){
return owner.nodeType===1||owner.nodeType===9||!( +owner.nodeType);
};
function Data(){
this.expando=jQuery.expando + Data.uid++;
}
Data.uid=1;
Data.prototype={
cache: function(owner){
var value=owner[ this.expando ];
if(!value){
value={};
if(acceptData(owner)){
if(owner.nodeType){
owner[ this.expando ]=value;
}else{
Object.defineProperty(owner, this.expando, {
value: value,
configurable: true
});
}}
}
return value;
},
set: function(owner, data, value){
var prop,
cache=this.cache(owner);
if(typeof data==="string"){
cache[ camelCase(data) ]=value;
}else{
for(prop in data){
cache[ camelCase(prop) ]=data[ prop ];
}}
return cache;
},
get: function(owner, key){
return key===undefined ?
this.cache(owner) :
owner[ this.expando ]&&owner[ this.expando ][ camelCase(key) ];
},
access: function(owner, key, value){
if(key===undefined ||
(( key&&typeof key==="string")&&value===undefined)){
return this.get(owner, key);
}
this.set(owner, key, value);
return value!==undefined ? value:key;
},
remove: function(owner, key){
var i,
cache=owner[ this.expando ];
if(cache===undefined){
return;
}
if(key!==undefined){
if(Array.isArray(key)){
key=key.map(camelCase);
}else{
key=camelCase(key);
key=key in cache ?
[ key ] :
(key.match(rnothtmlwhite)||[]);
}
i=key.length;
while(i--){
delete cache[ key[ i ] ];
}}
if(key===undefined||jQuery.isEmptyObject(cache)){
if(owner.nodeType){
owner[ this.expando ]=undefined;
}else{
delete owner[ this.expando ];
}}
},
hasData: function(owner){
var cache=owner[ this.expando ];
return cache!==undefined&&!jQuery.isEmptyObject(cache);
}};
var dataPriv=new Data();
var dataUser=new Data();
var rbrace=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
rmultiDash=/[A-Z]/g;
function getData(data){
if(data==="true"){
return true;
}
if(data==="false"){
return false;
}
if(data==="null"){
return null;
}
if(data===+data + ""){
return +data;
}
if(rbrace.test(data)){
return JSON.parse(data);
}
return data;
}
function dataAttr(elem, key, data){
var name;
if(data===undefined&&elem.nodeType===1){
name="data-" + key.replace(rmultiDash, "-$&").toLowerCase();
data=elem.getAttribute(name);
if(typeof data==="string"){
try {
data=getData(data);
} catch(e){}
dataUser.set(elem, key, data);
}else{
data=undefined;
}}
return data;
}
jQuery.extend({
hasData: function(elem){
return dataUser.hasData(elem)||dataPriv.hasData(elem);
},
data: function(elem, name, data){
return dataUser.access(elem, name, data);
},
removeData: function(elem, name){
dataUser.remove(elem, name);
},
_data: function(elem, name, data){
return dataPriv.access(elem, name, data);
},
_removeData: function(elem, name){
dataPriv.remove(elem, name);
}});
jQuery.fn.extend({
data: function(key, value){
var i, name, data,
elem=this[ 0 ],
attrs=elem&&elem.attributes;
if(key===undefined){
if(this.length){
data=dataUser.get(elem);
if(elem.nodeType===1&&!dataPriv.get(elem, "hasDataAttrs")){
i=attrs.length;
while(i--){
if(attrs[ i ]){
name=attrs[ i ].name;
if(name.indexOf("data-")===0){
name=camelCase(name.slice(5));
dataAttr(elem, name, data[ name ]);
}}
}
dataPriv.set(elem, "hasDataAttrs", true);
}}
return data;
}
if(typeof key==="object"){
return this.each(function(){
dataUser.set(this, key);
});
}
return access(this, function(value){
var data;
if(elem&&value===undefined){
data=dataUser.get(elem, key);
if(data!==undefined){
return data;
}
data=dataAttr(elem, key);
if(data!==undefined){
return data;
}
return;
}
this.each(function(){
dataUser.set(this, key, value);
});
}, null, value, arguments.length > 1, null, true);
},
removeData: function(key){
return this.each(function(){
dataUser.remove(this, key);
});
}});
jQuery.extend({
queue: function(elem, type, data){
var queue;
if(elem){
type=(type||"fx") + "queue";
queue=dataPriv.get(elem, type);
if(data){
if(!queue||Array.isArray(data)){
queue=dataPriv.access(elem, type, jQuery.makeArray(data));
}else{
queue.push(data);
}}
return queue||[];
}},
dequeue: function(elem, type){
type=type||"fx";
var queue=jQuery.queue(elem, type),
startLength=queue.length,
fn=queue.shift(),
hooks=jQuery._queueHooks(elem, type),
next=function(){
jQuery.dequeue(elem, type);
};
if(fn==="inprogress"){
fn=queue.shift();
startLength--;
}
if(fn){
if(type==="fx"){
queue.unshift("inprogress");
}
delete hooks.stop;
fn.call(elem, next, hooks);
}
if(!startLength&&hooks){
hooks.empty.fire();
}},
_queueHooks: function(elem, type){
var key=type + "queueHooks";
return dataPriv.get(elem, key)||dataPriv.access(elem, key, {
empty: jQuery.Callbacks("once memory").add(function(){
dataPriv.remove(elem, [ type + "queue", key ]);
})
});
}});
jQuery.fn.extend({
queue: function(type, data){
var setter=2;
if(typeof type!=="string"){
data=type;
type="fx";
setter--;
}
if(arguments.length < setter){
return jQuery.queue(this[ 0 ], type);
}
return data===undefined ?
this :
this.each(function(){
var queue=jQuery.queue(this, type, data);
jQuery._queueHooks(this, type);
if(type==="fx"&&queue[ 0 ]!=="inprogress"){
jQuery.dequeue(this, type);
}});
},
dequeue: function(type){
return this.each(function(){
jQuery.dequeue(this, type);
});
},
clearQueue: function(type){
return this.queue(type||"fx", []);
},
promise: function(type, obj){
var tmp,
count=1,
defer=jQuery.Deferred(),
elements=this,
i=this.length,
resolve=function(){
if(!(--count)){
defer.resolveWith(elements, [ elements ]);
}};
if(typeof type!=="string"){
obj=type;
type=undefined;
}
type=type||"fx";
while(i--){
tmp=dataPriv.get(elements[ i ], type + "queueHooks");
if(tmp&&tmp.empty){
count++;
tmp.empty.add(resolve);
}}
resolve();
return defer.promise(obj);
}});
var pnum=(/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;
var rcssNum=new RegExp("^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i");
var cssExpand=[ "Top", "Right", "Bottom", "Left" ];
var isHiddenWithinTree=function(elem, el){
elem=el||elem;
return elem.style.display==="none" ||
elem.style.display==="" &&
jQuery.contains(elem.ownerDocument, elem) &&
jQuery.css(elem, "display")==="none";
};
var swap=function(elem, options, callback, args){
var ret, name,
old={};
for(name in options){
old[ name ]=elem.style[ name ];
elem.style[ name ]=options[ name ];
}
ret=callback.apply(elem, args||[]);
for(name in options){
elem.style[ name ]=old[ name ];
}
return ret;
};
function adjustCSS(elem, prop, valueParts, tween){
var adjusted, scale,
maxIterations=20,
currentValue=tween ?
function(){
return tween.cur();
} :
function(){
return jQuery.css(elem, prop, "");
},
initial=currentValue(),
unit=valueParts&&valueParts[ 3 ]||(jQuery.cssNumber[ prop ] ? "":"px"),
initialInUnit=(jQuery.cssNumber[ prop ]||unit!=="px"&&+initial) &&
rcssNum.exec(jQuery.css(elem, prop));
if(initialInUnit&&initialInUnit[ 3 ]!==unit){
initial=initial / 2;
unit=unit||initialInUnit[ 3 ];
initialInUnit=+initial||1;
while(maxIterations--){
jQuery.style(elem, prop, initialInUnit + unit);
if(( 1 - scale) *(1 -(scale=currentValue() / initial||0.5)) <=0){
maxIterations=0;
}
initialInUnit=initialInUnit / scale;
}
initialInUnit=initialInUnit * 2;
jQuery.style(elem, prop, initialInUnit + unit);
valueParts=valueParts||[];
}
if(valueParts){
initialInUnit=+initialInUnit||+initial||0;
adjusted=valueParts[ 1 ] ?
initialInUnit +(valueParts[ 1 ] + 1) * valueParts[ 2 ] :
+valueParts[ 2 ];
if(tween){
tween.unit=unit;
tween.start=initialInUnit;
tween.end=adjusted;
}}
return adjusted;
}
var defaultDisplayMap={};
function getDefaultDisplay(elem){
var temp,
doc=elem.ownerDocument,
nodeName=elem.nodeName,
display=defaultDisplayMap[ nodeName ];
if(display){
return display;
}
temp=doc.body.appendChild(doc.createElement(nodeName));
display=jQuery.css(temp, "display");
temp.parentNode.removeChild(temp);
if(display==="none"){
display="block";
}
defaultDisplayMap[ nodeName ]=display;
return display;
}
function showHide(elements, show){
var display, elem,
values=[],
index=0,
length=elements.length;
for(; index < length; index++){
elem=elements[ index ];
if(!elem.style){
continue;
}
display=elem.style.display;
if(show){
if(display==="none"){
values[ index ]=dataPriv.get(elem, "display")||null;
if(!values[ index ]){
elem.style.display="";
}}
if(elem.style.display===""&&isHiddenWithinTree(elem)){
values[ index ]=getDefaultDisplay(elem);
}}else{
if(display!=="none"){
values[ index ]="none";
dataPriv.set(elem, "display", display);
}}
}
for(index=0; index < length; index++){
if(values[ index ]!=null){
elements[ index ].style.display=values[ index ];
}}
return elements;
}
jQuery.fn.extend({
show: function(){
return showHide(this, true);
},
hide: function(){
return showHide(this);
},
toggle: function(state){
if(typeof state==="boolean"){
return state ? this.show():this.hide();
}
return this.each(function(){
if(isHiddenWithinTree(this)){
jQuery(this).show();
}else{
jQuery(this).hide();
}});
}});
var rcheckableType=(/^(?:checkbox|radio)$/i);
var rtagName=(/<([a-z][^\/\0>\x20\t\r\n\f]+)/i);
var rscriptType=(/^$|^module$|\/(?:java|ecma)script/i);
var wrapMap={
option: [ 1, "<select multiple='multiple'>", "</select>" ],
thead: [ 1, "<table>", "</table>" ],
col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
tr: [ 2, "<table><tbody>", "</tbody></table>" ],
td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
_default: [ 0, "", "" ]
};
wrapMap.optgroup=wrapMap.option;
wrapMap.tbody=wrapMap.tfoot=wrapMap.colgroup=wrapMap.caption=wrapMap.thead;
wrapMap.th=wrapMap.td;
function getAll(context, tag){
var ret;
if(typeof context.getElementsByTagName!=="undefined"){
ret=context.getElementsByTagName(tag||"*");
}else if(typeof context.querySelectorAll!=="undefined"){
ret=context.querySelectorAll(tag||"*");
}else{
ret=[];
}
if(tag===undefined||tag&&nodeName(context, tag)){
return jQuery.merge([ context ], ret);
}
return ret;
}
function setGlobalEval(elems, refElements){
var i=0,
l=elems.length;
for(; i < l; i++){
dataPriv.set(elems[ i ],
"globalEval",
!refElements||dataPriv.get(refElements[ i ], "globalEval")
);
}}
var rhtml=/<|&#?\w+;/;
function buildFragment(elems, context, scripts, selection, ignored){
var elem, tmp, tag, wrap, contains, j,
fragment=context.createDocumentFragment(),
nodes=[],
i=0,
l=elems.length;
for(; i < l; i++){
elem=elems[ i ];
if(elem||elem===0){
if(toType(elem)==="object"){
jQuery.merge(nodes, elem.nodeType ? [ elem ]:elem);
}else if(!rhtml.test(elem)){
nodes.push(context.createTextNode(elem));
}else{
tmp=tmp||fragment.appendChild(context.createElement("div"));
tag=(rtagName.exec(elem)||[ "", "" ])[ 1 ].toLowerCase();
wrap=wrapMap[ tag ]||wrapMap._default;
tmp.innerHTML=wrap[ 1 ] + jQuery.htmlPrefilter(elem) + wrap[ 2 ];
j=wrap[ 0 ];
while(j--){
tmp=tmp.lastChild;
}
jQuery.merge(nodes, tmp.childNodes);
tmp=fragment.firstChild;
tmp.textContent="";
}}
}
fragment.textContent="";
i=0;
while(( elem=nodes[ i++ ])){
if(selection&&jQuery.inArray(elem, selection) > -1){
if(ignored){
ignored.push(elem);
}
continue;
}
contains=jQuery.contains(elem.ownerDocument, elem);
tmp=getAll(fragment.appendChild(elem), "script");
if(contains){
setGlobalEval(tmp);
}
if(scripts){
j=0;
while(( elem=tmp[ j++ ])){
if(rscriptType.test(elem.type||"")){
scripts.push(elem);
}}
}}
return fragment;
}
(function(){
var fragment=document.createDocumentFragment(),
div=fragment.appendChild(document.createElement("div")),
input=document.createElement("input");
input.setAttribute("type", "radio");
input.setAttribute("checked", "checked");
input.setAttribute("name", "t");
div.appendChild(input);
support.checkClone=div.cloneNode(true).cloneNode(true).lastChild.checked;
div.innerHTML="<textarea>x</textarea>";
support.noCloneChecked = !!div.cloneNode(true).lastChild.defaultValue;
})();
var documentElement=document.documentElement;
var
rkeyEvent=/^key/,
rmouseEvent=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,
rtypenamespace=/^([^.]*)(?:\.(.+)|)/;
function returnTrue(){
return true;
}
function returnFalse(){
return false;
}
function safeActiveElement(){
try {
return document.activeElement;
} catch(err){ }}
function on(elem, types, selector, data, fn, one){
var origFn, type;
if(typeof types==="object"){
if(typeof selector!=="string"){
data=data||selector;
selector=undefined;
}
for(type in types){
on(elem, type, selector, data, types[ type ], one);
}
return elem;
}
if(data==null&&fn==null){
fn=selector;
data=selector=undefined;
}else if(fn==null){
if(typeof selector==="string"){
fn=data;
data=undefined;
}else{
fn=data;
data=selector;
selector=undefined;
}}
if(fn===false){
fn=returnFalse;
}else if(!fn){
return elem;
}
if(one===1){
origFn=fn;
fn=function(event){
jQuery().off(event);
return origFn.apply(this, arguments);
};
fn.guid=origFn.guid||(origFn.guid=jQuery.guid++);
}
return elem.each(function(){
jQuery.event.add(this, types, fn, data, selector);
});
}
jQuery.event={
global: {},
add: function(elem, types, handler, data, selector){
var handleObjIn, eventHandle, tmp,
events, t, handleObj,
special, handlers, type, namespaces, origType,
elemData=dataPriv.get(elem);
if(!elemData){
return;
}
if(handler.handler){
handleObjIn=handler;
handler=handleObjIn.handler;
selector=handleObjIn.selector;
}
if(selector){
jQuery.find.matchesSelector(documentElement, selector);
}
if(!handler.guid){
handler.guid=jQuery.guid++;
}
if(!(events=elemData.events)){
events=elemData.events={};}
if(!(eventHandle=elemData.handle)){
eventHandle=elemData.handle=function(e){
return typeof jQuery!=="undefined"&&jQuery.event.triggered!==e.type ?
jQuery.event.dispatch.apply(elem, arguments):undefined;
};}
types=(types||"").match(rnothtmlwhite)||[ "" ];
t=types.length;
while(t--){
tmp=rtypenamespace.exec(types[ t ])||[];
type=origType=tmp[ 1 ];
namespaces=(tmp[ 2 ]||"").split(".").sort();
if(!type){
continue;
}
special=jQuery.event.special[ type ]||{};
type=(selector ? special.delegateType:special.bindType)||type;
special=jQuery.event.special[ type ]||{};
handleObj=jQuery.extend({
type: type,
origType: origType,
data: data,
handler: handler,
guid: handler.guid,
selector: selector,
needsContext: selector&&jQuery.expr.match.needsContext.test(selector),
namespace: namespaces.join(".")
}, handleObjIn);
if(!(handlers=events[ type ])){
handlers=events[ type ]=[];
handlers.delegateCount=0;
if(!special.setup ||
special.setup.call(elem, data, namespaces, eventHandle)===false){
if(elem.addEventListener){
elem.addEventListener(type, eventHandle);
}}
}
if(special.add){
special.add.call(elem, handleObj);
if(!handleObj.handler.guid){
handleObj.handler.guid=handler.guid;
}}
if(selector){
handlers.splice(handlers.delegateCount++, 0, handleObj);
}else{
handlers.push(handleObj);
}
jQuery.event.global[ type ]=true;
}},
remove: function(elem, types, handler, selector, mappedTypes){
var j, origCount, tmp,
events, t, handleObj,
special, handlers, type, namespaces, origType,
elemData=dataPriv.hasData(elem)&&dataPriv.get(elem);
if(!elemData||!(events=elemData.events)){
return;
}
types=(types||"").match(rnothtmlwhite)||[ "" ];
t=types.length;
while(t--){
tmp=rtypenamespace.exec(types[ t ])||[];
type=origType=tmp[ 1 ];
namespaces=(tmp[ 2 ]||"").split(".").sort();
if(!type){
for(type in events){
jQuery.event.remove(elem, type + types[ t ], handler, selector, true);
}
continue;
}
special=jQuery.event.special[ type ]||{};
type=(selector ? special.delegateType:special.bindType)||type;
handlers=events[ type ]||[];
tmp=tmp[ 2 ] &&
new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)");
origCount=j = handlers.length;
while(j--){
handleObj=handlers[ j ];
if(( mappedTypes||origType===handleObj.origType) &&
(!handler||handler.guid===handleObj.guid) &&
(!tmp||tmp.test(handleObj.namespace)) &&
(!selector||selector===handleObj.selector ||
selector==="**"&&handleObj.selector)){
handlers.splice(j, 1);
if(handleObj.selector){
handlers.delegateCount--;
}
if(special.remove){
special.remove.call(elem, handleObj);
}}
}
if(origCount&&!handlers.length){
if(!special.teardown ||
special.teardown.call(elem, namespaces, elemData.handle)===false){
jQuery.removeEvent(elem, type, elemData.handle);
}
delete events[ type ];
}}
if(jQuery.isEmptyObject(events)){
dataPriv.remove(elem, "handle events");
}},
dispatch: function(nativeEvent){
var event=jQuery.event.fix(nativeEvent);
var i, j, ret, matched, handleObj, handlerQueue,
args=new Array(arguments.length),
handlers=(dataPriv.get(this, "events")||{})[ event.type ]||[],
special=jQuery.event.special[ event.type ]||{};
args[ 0 ]=event;
for(i=1; i < arguments.length; i++){
args[ i ]=arguments[ i ];
}
event.delegateTarget=this;
if(special.preDispatch&&special.preDispatch.call(this, event)===false){
return;
}
handlerQueue=jQuery.event.handlers.call(this, event, handlers);
i=0;
while(( matched=handlerQueue[ i++ ])&&!event.isPropagationStopped()){
event.currentTarget=matched.elem;
j=0;
while(( handleObj=matched.handlers[ j++ ]) &&
!event.isImmediatePropagationStopped()){
if(!event.rnamespace||event.rnamespace.test(handleObj.namespace)){
event.handleObj=handleObj;
event.data=handleObj.data;
ret=(( jQuery.event.special[ handleObj.origType ]||{}).handle ||
handleObj.handler).apply(matched.elem, args);
if(ret!==undefined){
if(( event.result=ret)===false){
event.preventDefault();
event.stopPropagation();
}}
}}
}
if(special.postDispatch){
special.postDispatch.call(this, event);
}
return event.result;
},
handlers: function(event, handlers){
var i, handleObj, sel, matchedHandlers, matchedSelectors,
handlerQueue=[],
delegateCount=handlers.delegateCount,
cur=event.target;
if(delegateCount &&
cur.nodeType &&
!(event.type==="click"&&event.button >=1)){
for(; cur!==this; cur=cur.parentNode||this){
if(cur.nodeType===1&&!(event.type==="click"&&cur.disabled===true)){
matchedHandlers=[];
matchedSelectors={};
for(i=0; i < delegateCount; i++){
handleObj=handlers[ i ];
sel=handleObj.selector + " ";
if(matchedSelectors[ sel ]===undefined){
matchedSelectors[ sel ]=handleObj.needsContext ?
jQuery(sel, this).index(cur) > -1 :
jQuery.find(sel, this, null, [ cur ]).length;
}
if(matchedSelectors[ sel ]){
matchedHandlers.push(handleObj);
}}
if(matchedHandlers.length){
handlerQueue.push({ elem: cur, handlers: matchedHandlers });
}}
}}
cur=this;
if(delegateCount < handlers.length){
handlerQueue.push({ elem: cur, handlers: handlers.slice(delegateCount) });
}
return handlerQueue;
},
addProp: function(name, hook){
Object.defineProperty(jQuery.Event.prototype, name, {
enumerable: true,
configurable: true,
get: isFunction(hook) ?
function(){
if(this.originalEvent){
return hook(this.originalEvent);
}} :
function(){
if(this.originalEvent){
return this.originalEvent[ name ];
}},
set: function(value){
Object.defineProperty(this, name, {
enumerable: true,
configurable: true,
writable: true,
value: value
});
}});
},
fix: function(originalEvent){
return originalEvent[ jQuery.expando ] ?
originalEvent :
new jQuery.Event(originalEvent);
},
special: {
load: {
noBubble: true
},
focus: {
trigger: function(){
if(this!==safeActiveElement()&&this.focus){
this.focus();
return false;
}},
delegateType: "focusin"
},
blur: {
trigger: function(){
if(this===safeActiveElement()&&this.blur){
this.blur();
return false;
}},
delegateType: "focusout"
},
click: {
trigger: function(){
if(this.type==="checkbox"&&this.click&&nodeName(this, "input")){
this.click();
return false;
}},
_default: function(event){
return nodeName(event.target, "a");
}},
beforeunload: {
postDispatch: function(event){
if(event.result!==undefined&&event.originalEvent){
event.originalEvent.returnValue=event.result;
}}
}}
};
jQuery.removeEvent=function(elem, type, handle){
if(elem.removeEventListener){
elem.removeEventListener(type, handle);
}};
jQuery.Event=function(src, props){
if(!(this instanceof jQuery.Event)){
return new jQuery.Event(src, props);
}
if(src&&src.type){
this.originalEvent=src;
this.type=src.type;
this.isDefaultPrevented=src.defaultPrevented ||
src.defaultPrevented===undefined &&
src.returnValue===false ?
returnTrue :
returnFalse;
this.target=(src.target&&src.target.nodeType===3) ?
src.target.parentNode :
src.target;
this.currentTarget=src.currentTarget;
this.relatedTarget=src.relatedTarget;
}else{
this.type=src;
}
if(props){
jQuery.extend(this, props);
}
this.timeStamp=src&&src.timeStamp||Date.now();
this[ jQuery.expando ]=true;
};
jQuery.Event.prototype={
constructor: jQuery.Event,
isDefaultPrevented: returnFalse,
isPropagationStopped: returnFalse,
isImmediatePropagationStopped: returnFalse,
isSimulated: false,
preventDefault: function(){
var e=this.originalEvent;
this.isDefaultPrevented=returnTrue;
if(e&&!this.isSimulated){
e.preventDefault();
}},
stopPropagation: function(){
var e=this.originalEvent;
this.isPropagationStopped=returnTrue;
if(e&&!this.isSimulated){
e.stopPropagation();
}},
stopImmediatePropagation: function(){
var e=this.originalEvent;
this.isImmediatePropagationStopped=returnTrue;
if(e&&!this.isSimulated){
e.stopImmediatePropagation();
}
this.stopPropagation();
}};
jQuery.each({
altKey: true,
bubbles: true,
cancelable: true,
changedTouches: true,
ctrlKey: true,
detail: true,
eventPhase: true,
metaKey: true,
pageX: true,
pageY: true,
shiftKey: true,
view: true,
"char": true,
charCode: true,
key: true,
keyCode: true,
button: true,
buttons: true,
clientX: true,
clientY: true,
offsetX: true,
offsetY: true,
pointerId: true,
pointerType: true,
screenX: true,
screenY: true,
targetTouches: true,
toElement: true,
touches: true,
which: function(event){
var button=event.button;
if(event.which==null&&rkeyEvent.test(event.type)){
return event.charCode!=null ? event.charCode:event.keyCode;
}
if(!event.which&&button!==undefined&&rmouseEvent.test(event.type)){
if(button & 1){
return 1;
}
if(button & 2){
return 3;
}
if(button & 4){
return 2;
}
return 0;
}
return event.which;
}}, jQuery.event.addProp);
jQuery.each({
mouseenter: "mouseover",
mouseleave: "mouseout",
pointerenter: "pointerover",
pointerleave: "pointerout"
}, function(orig, fix){
jQuery.event.special[ orig ]={
delegateType: fix,
bindType: fix,
handle: function(event){
var ret,
target=this,
related=event.relatedTarget,
handleObj=event.handleObj;
if(!related||(related!==target&&!jQuery.contains(target, related))){
event.type=handleObj.origType;
ret=handleObj.handler.apply(this, arguments);
event.type=fix;
}
return ret;
}};});
jQuery.fn.extend({
on: function(types, selector, data, fn){
return on(this, types, selector, data, fn);
},
one: function(types, selector, data, fn){
return on(this, types, selector, data, fn, 1);
},
off: function(types, selector, fn){
var handleObj, type;
if(types&&types.preventDefault&&types.handleObj){
handleObj=types.handleObj;
jQuery(types.delegateTarget).off(handleObj.namespace ?
handleObj.origType + "." + handleObj.namespace :
handleObj.origType,
handleObj.selector,
handleObj.handler
);
return this;
}
if(typeof types==="object"){
for(type in types){
this.off(type, selector, types[ type ]);
}
return this;
}
if(selector===false||typeof selector==="function"){
fn=selector;
selector=undefined;
}
if(fn===false){
fn=returnFalse;
}
return this.each(function(){
jQuery.event.remove(this, types, fn, selector);
});
}});
var
rxhtmlTag=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
rnoInnerhtml=/<script|<style|<link/i,
rchecked=/checked\s*(?:[^=]|=\s*.checked.)/i,
rcleanScript=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
function manipulationTarget(elem, content){
if(nodeName(elem, "table") &&
nodeName(content.nodeType!==11 ? content:content.firstChild, "tr")){
return jQuery(elem).children("tbody")[ 0 ]||elem;
}
return elem;
}
function disableScript(elem){
elem.type=(elem.getAttribute("type")!==null) + "/" + elem.type;
return elem;
}
function restoreScript(elem){
if(( elem.type||"").slice(0, 5)==="true/"){
elem.type=elem.type.slice(5);
}else{
elem.removeAttribute("type");
}
return elem;
}
function cloneCopyEvent(src, dest){
var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;
if(dest.nodeType!==1){
return;
}
if(dataPriv.hasData(src)){
pdataOld=dataPriv.access(src);
pdataCur=dataPriv.set(dest, pdataOld);
events=pdataOld.events;
if(events){
delete pdataCur.handle;
pdataCur.events={};
for(type in events){
for(i=0, l=events[ type ].length; i < l; i++){
jQuery.event.add(dest, type, events[ type ][ i ]);
}}
}}
if(dataUser.hasData(src)){
udataOld=dataUser.access(src);
udataCur=jQuery.extend({}, udataOld);
dataUser.set(dest, udataCur);
}}
function fixInput(src, dest){
var nodeName=dest.nodeName.toLowerCase();
if(nodeName==="input"&&rcheckableType.test(src.type)){
dest.checked=src.checked;
}else if(nodeName==="input"||nodeName==="textarea"){
dest.defaultValue=src.defaultValue;
}}
function domManip(collection, args, callback, ignored){
args=concat.apply([], args);
var fragment, first, scripts, hasScripts, node, doc,
i=0,
l=collection.length,
iNoClone=l - 1,
value=args[ 0 ],
valueIsFunction=isFunction(value);
if(valueIsFunction ||
(l > 1&&typeof value==="string" &&
!support.checkClone&&rchecked.test(value))){
return collection.each(function(index){
var self=collection.eq(index);
if(valueIsFunction){
args[ 0 ]=value.call(this, index, self.html());
}
domManip(self, args, callback, ignored);
});
}
if(l){
fragment=buildFragment(args, collection[ 0 ].ownerDocument, false, collection, ignored);
first=fragment.firstChild;
if(fragment.childNodes.length===1){
fragment=first;
}
if(first||ignored){
scripts=jQuery.map(getAll(fragment, "script"), disableScript);
hasScripts=scripts.length;
for(; i < l; i++){
node=fragment;
if(i!==iNoClone){
node=jQuery.clone(node, true, true);
if(hasScripts){
jQuery.merge(scripts, getAll(node, "script"));
}}
callback.call(collection[ i ], node, i);
}
if(hasScripts){
doc=scripts[ scripts.length - 1 ].ownerDocument;
jQuery.map(scripts, restoreScript);
for(i=0; i < hasScripts; i++){
node=scripts[ i ];
if(rscriptType.test(node.type||"") &&
!dataPriv.access(node, "globalEval") &&
jQuery.contains(doc, node)){
if(node.src&&(node.type||"").toLowerCase()!=="module"){
if(jQuery._evalUrl){
jQuery._evalUrl(node.src);
}}else{
DOMEval(node.textContent.replace(rcleanScript, ""), doc, node);
}}
}}
}}
return collection;
}
function remove(elem, selector, keepData){
var node,
nodes=selector ? jQuery.filter(selector, elem):elem,
i=0;
for(;(node=nodes[ i ])!=null; i++){
if(!keepData&&node.nodeType===1){
jQuery.cleanData(getAll(node));
}
if(node.parentNode){
if(keepData&&jQuery.contains(node.ownerDocument, node)){
setGlobalEval(getAll(node, "script"));
}
node.parentNode.removeChild(node);
}}
return elem;
}
jQuery.extend({
htmlPrefilter: function(html){
return html.replace(rxhtmlTag, "<$1></$2>");
},
clone: function(elem, dataAndEvents, deepDataAndEvents){
var i, l, srcElements, destElements,
clone=elem.cloneNode(true),
inPage=jQuery.contains(elem.ownerDocument, elem);
if(!support.noCloneChecked&&(elem.nodeType===1||elem.nodeType===11) &&
!jQuery.isXMLDoc(elem)){
destElements=getAll(clone);
srcElements=getAll(elem);
for(i=0, l=srcElements.length; i < l; i++){
fixInput(srcElements[ i ], destElements[ i ]);
}}
if(dataAndEvents){
if(deepDataAndEvents){
srcElements=srcElements||getAll(elem);
destElements=destElements||getAll(clone);
for(i=0, l=srcElements.length; i < l; i++){
cloneCopyEvent(srcElements[ i ], destElements[ i ]);
}}else{
cloneCopyEvent(elem, clone);
}}
destElements=getAll(clone, "script");
if(destElements.length > 0){
setGlobalEval(destElements, !inPage&&getAll(elem, "script"));
}
return clone;
},
cleanData: function(elems){
var data, elem, type,
special=jQuery.event.special,
i=0;
for(;(elem=elems[ i ])!==undefined; i++){
if(acceptData(elem)){
if(( data=elem[ dataPriv.expando ])){
if(data.events){
for(type in data.events){
if(special[ type ]){
jQuery.event.remove(elem, type);
}else{
jQuery.removeEvent(elem, type, data.handle);
}}
}
elem[ dataPriv.expando ]=undefined;
}
if(elem[ dataUser.expando ]){
elem[ dataUser.expando ]=undefined;
}}
}}
});
jQuery.fn.extend({
detach: function(selector){
return remove(this, selector, true);
},
remove: function(selector){
return remove(this, selector);
},
text: function(value){
return access(this, function(value){
return value===undefined ?
jQuery.text(this) :
this.empty().each(function(){
if(this.nodeType===1||this.nodeType===11||this.nodeType===9){
this.textContent=value;
}});
}, null, value, arguments.length);
},
append: function(){
return domManip(this, arguments, function(elem){
if(this.nodeType===1||this.nodeType===11||this.nodeType===9){
var target=manipulationTarget(this, elem);
target.appendChild(elem);
}});
},
prepend: function(){
return domManip(this, arguments, function(elem){
if(this.nodeType===1||this.nodeType===11||this.nodeType===9){
var target=manipulationTarget(this, elem);
target.insertBefore(elem, target.firstChild);
}});
},
before: function(){
return domManip(this, arguments, function(elem){
if(this.parentNode){
this.parentNode.insertBefore(elem, this);
}});
},
after: function(){
return domManip(this, arguments, function(elem){
if(this.parentNode){
this.parentNode.insertBefore(elem, this.nextSibling);
}});
},
empty: function(){
var elem,
i=0;
for(;(elem=this[ i ])!=null; i++){
if(elem.nodeType===1){
jQuery.cleanData(getAll(elem, false));
elem.textContent="";
}}
return this;
},
clone: function(dataAndEvents, deepDataAndEvents){
dataAndEvents=dataAndEvents==null ? false:dataAndEvents;
deepDataAndEvents=deepDataAndEvents==null ? dataAndEvents:deepDataAndEvents;
return this.map(function(){
return jQuery.clone(this, dataAndEvents, deepDataAndEvents);
});
},
html: function(value){
return access(this, function(value){
var elem=this[ 0 ]||{},
i=0,
l=this.length;
if(value===undefined&&elem.nodeType===1){
return elem.innerHTML;
}
if(typeof value==="string"&&!rnoInnerhtml.test(value) &&
!wrapMap[(rtagName.exec(value)||[ "", "" ])[ 1 ].toLowerCase() ]){
value=jQuery.htmlPrefilter(value);
try {
for(; i < l; i++){
elem=this[ i ]||{};
if(elem.nodeType===1){
jQuery.cleanData(getAll(elem, false));
elem.innerHTML=value;
}}
elem=0;
} catch(e){}}
if(elem){
this.empty().append(value);
}}, null, value, arguments.length);
},
replaceWith: function(){
var ignored=[];
return domManip(this, arguments, function(elem){
var parent=this.parentNode;
if(jQuery.inArray(this, ignored) < 0){
jQuery.cleanData(getAll(this));
if(parent){
parent.replaceChild(elem, this);
}}
}, ignored);
}});
jQuery.each({
appendTo: "append",
prependTo: "prepend",
insertBefore: "before",
insertAfter: "after",
replaceAll: "replaceWith"
}, function(name, original){
jQuery.fn[ name ]=function(selector){
var elems,
ret=[],
insert=jQuery(selector),
last=insert.length - 1,
i=0;
for(; i <=last; i++){
elems=i===last ? this:this.clone(true);
jQuery(insert[ i ])[ original ](elems);
push.apply(ret, elems.get());
}
return this.pushStack(ret);
};});
var rnumnonpx=new RegExp("^(" + pnum + ")(?!px)[a-z%]+$", "i");
var getStyles=function(elem){
var view=elem.ownerDocument.defaultView;
if(!view||!view.opener){
view=window;
}
return view.getComputedStyle(elem);
};
var rboxStyle=new RegExp(cssExpand.join("|"), "i");
(function(){
function computeStyleTests(){
if(!div){
return;
}
container.style.cssText="position:absolute;left:-11111px;width:60px;" +
"margin-top:1px;padding:0;border:0";
div.style.cssText =
"position:relative;display:block;box-sizing:border-box;overflow:scroll;" +
"margin:auto;border:1px;padding:1px;" +
"width:60%;top:1%";
documentElement.appendChild(container).appendChild(div);
var divStyle=window.getComputedStyle(div);
pixelPositionVal=divStyle.top!=="1%";
reliableMarginLeftVal=roundPixelMeasures(divStyle.marginLeft)===12;
div.style.right="60%";
pixelBoxStylesVal=roundPixelMeasures(divStyle.right)===36;
boxSizingReliableVal=roundPixelMeasures(divStyle.width)===36;
div.style.position="absolute";
scrollboxSizeVal=div.offsetWidth===36||"absolute";
documentElement.removeChild(container);
div=null;
}
function roundPixelMeasures(measure){
return Math.round(parseFloat(measure));
}
var pixelPositionVal, boxSizingReliableVal, scrollboxSizeVal, pixelBoxStylesVal,
reliableMarginLeftVal,
container=document.createElement("div"),
div=document.createElement("div");
if(!div.style){
return;
}
div.style.backgroundClip="content-box";
div.cloneNode(true).style.backgroundClip="";
support.clearCloneStyle=div.style.backgroundClip==="content-box";
jQuery.extend(support, {
boxSizingReliable: function(){
computeStyleTests();
return boxSizingReliableVal;
},
pixelBoxStyles: function(){
computeStyleTests();
return pixelBoxStylesVal;
},
pixelPosition: function(){
computeStyleTests();
return pixelPositionVal;
},
reliableMarginLeft: function(){
computeStyleTests();
return reliableMarginLeftVal;
},
scrollboxSize: function(){
computeStyleTests();
return scrollboxSizeVal;
}});
})();
function curCSS(elem, name, computed){
var width, minWidth, maxWidth, ret,
style=elem.style;
computed=computed||getStyles(elem);
if(computed){
ret=computed.getPropertyValue(name)||computed[ name ];
if(ret===""&&!jQuery.contains(elem.ownerDocument, elem)){
ret=jQuery.style(elem, name);
}
if(!support.pixelBoxStyles()&&rnumnonpx.test(ret)&&rboxStyle.test(name)){
width=style.width;
minWidth=style.minWidth;
maxWidth=style.maxWidth;
style.minWidth=style.maxWidth=style.width=ret;
ret=computed.width;
style.width=width;
style.minWidth=minWidth;
style.maxWidth=maxWidth;
}}
return ret!==undefined ?
ret + "" :
ret;
}
function addGetHookIf(conditionFn, hookFn){
return {
get: function(){
if(conditionFn()){
delete this.get;
return;
}
return(this.get=hookFn).apply(this, arguments);
}};}
var
rdisplayswap=/^(none|table(?!-c[ea]).+)/,
rcustomProp=/^--/,
cssShow={ position: "absolute", visibility: "hidden", display: "block" },
cssNormalTransform={
letterSpacing: "0",
fontWeight: "400"
},
cssPrefixes=[ "Webkit", "Moz", "ms" ],
emptyStyle=document.createElement("div").style;
function vendorPropName(name){
if(name in emptyStyle){
return name;
}
var capName=name[ 0 ].toUpperCase() + name.slice(1),
i=cssPrefixes.length;
while(i--){
name=cssPrefixes[ i ] + capName;
if(name in emptyStyle){
return name;
}}
}
function finalPropName(name){
var ret=jQuery.cssProps[ name ];
if(!ret){
ret=jQuery.cssProps[ name ]=vendorPropName(name)||name;
}
return ret;
}
function setPositiveNumber(elem, value, subtract){
var matches=rcssNum.exec(value);
return matches ?
Math.max(0, matches[ 2 ] -(subtract||0)) +(matches[ 3 ]||"px") :
value;
}
function boxModelAdjustment(elem, dimension, box, isBorderBox, styles, computedVal){
var i=dimension==="width" ? 1:0,
extra=0,
delta=0;
if(box===(isBorderBox ? "border":"content")){
return 0;
}
for(; i < 4; i +=2){
if(box==="margin"){
delta +=jQuery.css(elem, box + cssExpand[ i ], true, styles);
}
if(!isBorderBox){
delta +=jQuery.css(elem, "padding" + cssExpand[ i ], true, styles);
if(box!=="padding"){
delta +=jQuery.css(elem, "border" + cssExpand[ i ] + "Width", true, styles);
}else{
extra +=jQuery.css(elem, "border" + cssExpand[ i ] + "Width", true, styles);
}}else{
if(box==="content"){
delta -=jQuery.css(elem, "padding" + cssExpand[ i ], true, styles);
}
if(box!=="margin"){
delta -=jQuery.css(elem, "border" + cssExpand[ i ] + "Width", true, styles);
}}
}
if(!isBorderBox&&computedVal >=0){
delta +=Math.max(0, Math.ceil(elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice(1) ] -
computedVal -
delta -
extra -
0.5
));
}
return delta;
}
function getWidthOrHeight(elem, dimension, extra){
var styles=getStyles(elem),
val=curCSS(elem, dimension, styles),
isBorderBox=jQuery.css(elem, "boxSizing", false, styles)==="border-box",
valueIsBorderBox=isBorderBox;
if(rnumnonpx.test(val)){
if(!extra){
return val;
}
val="auto";
}
valueIsBorderBox=valueIsBorderBox &&
(support.boxSizingReliable()||val===elem.style[ dimension ]);
if(val==="auto" ||
!parseFloat(val)&&jQuery.css(elem, "display", false, styles)==="inline"){
val=elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice(1) ];
valueIsBorderBox=true;
}
val=parseFloat(val)||0;
return(val +
boxModelAdjustment(
elem,
dimension,
extra||(isBorderBox ? "border":"content"),
valueIsBorderBox,
styles,
val
)
) + "px";
}
jQuery.extend({
cssHooks: {
opacity: {
get: function(elem, computed){
if(computed){
var ret=curCSS(elem, "opacity");
return ret==="" ? "1":ret;
}}
}},
cssNumber: {
"animationIterationCount": true,
"columnCount": true,
"fillOpacity": true,
"flexGrow": true,
"flexShrink": true,
"fontWeight": true,
"lineHeight": true,
"opacity": true,
"order": true,
"orphans": true,
"widows": true,
"zIndex": true,
"zoom": true
},
cssProps: {},
style: function(elem, name, value, extra){
if(!elem||elem.nodeType===3||elem.nodeType===8||!elem.style){
return;
}
var ret, type, hooks,
origName=camelCase(name),
isCustomProp=rcustomProp.test(name),
style=elem.style;
if(!isCustomProp){
name=finalPropName(origName);
}
hooks=jQuery.cssHooks[ name ]||jQuery.cssHooks[ origName ];
if(value!==undefined){
type=typeof value;
if(type==="string"&&(ret=rcssNum.exec(value))&&ret[ 1 ]){
value=adjustCSS(elem, name, ret);
type="number";
}
if(value==null||value!==value){
return;
}
if(type==="number"){
value +=ret&&ret[ 3 ]||(jQuery.cssNumber[ origName ] ? "":"px");
}
if(!support.clearCloneStyle&&value===""&&name.indexOf("background")===0){
style[ name ]="inherit";
}
if(!hooks||!("set" in hooks) ||
(value=hooks.set(elem, value, extra))!==undefined){
if(isCustomProp){
style.setProperty(name, value);
}else{
style[ name ]=value;
}}
}else{
if(hooks&&"get" in hooks &&
(ret=hooks.get(elem, false, extra))!==undefined){
return ret;
}
return style[ name ];
}},
css: function(elem, name, extra, styles){
var val, num, hooks,
origName=camelCase(name),
isCustomProp=rcustomProp.test(name);
if(!isCustomProp){
name=finalPropName(origName);
}
hooks=jQuery.cssHooks[ name ]||jQuery.cssHooks[ origName ];
if(hooks&&"get" in hooks){
val=hooks.get(elem, true, extra);
}
if(val===undefined){
val=curCSS(elem, name, styles);
}
if(val==="normal"&&name in cssNormalTransform){
val=cssNormalTransform[ name ];
}
if(extra===""||extra){
num=parseFloat(val);
return extra===true||isFinite(num) ? num||0:val;
}
return val;
}});
jQuery.each([ "height", "width" ], function(i, dimension){
jQuery.cssHooks[ dimension ]={
get: function(elem, computed, extra){
if(computed){
return rdisplayswap.test(jQuery.css(elem, "display")) &&
(!elem.getClientRects().length||!elem.getBoundingClientRect().width) ?
swap(elem, cssShow, function(){
return getWidthOrHeight(elem, dimension, extra);
}) :
getWidthOrHeight(elem, dimension, extra);
}},
set: function(elem, value, extra){
var matches,
styles=getStyles(elem),
isBorderBox=jQuery.css(elem, "boxSizing", false, styles)==="border-box",
subtract=extra&&boxModelAdjustment(
elem,
dimension,
extra,
isBorderBox,
styles
);
if(isBorderBox&&support.scrollboxSize()===styles.position){
subtract -=Math.ceil(elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice(1) ] -
parseFloat(styles[ dimension ]) -
boxModelAdjustment(elem, dimension, "border", false, styles) -
0.5
);
}
if(subtract&&(matches=rcssNum.exec(value)) &&
(matches[ 3 ]||"px")!=="px"){
elem.style[ dimension ]=value;
value=jQuery.css(elem, dimension);
}
return setPositiveNumber(elem, value, subtract);
}};});
jQuery.cssHooks.marginLeft=addGetHookIf(support.reliableMarginLeft,
function(elem, computed){
if(computed){
return(parseFloat(curCSS(elem, "marginLeft")) ||
elem.getBoundingClientRect().left -
swap(elem, { marginLeft: 0 }, function(){
return elem.getBoundingClientRect().left;
})
) + "px";
}}
);
jQuery.each({
margin: "",
padding: "",
border: "Width"
}, function(prefix, suffix){
jQuery.cssHooks[ prefix + suffix ]={
expand: function(value){
var i=0,
expanded={},
parts=typeof value==="string" ? value.split(" "):[ value ];
for(; i < 4; i++){
expanded[ prefix + cssExpand[ i ] + suffix ] =
parts[ i ]||parts[ i - 2 ]||parts[ 0 ];
}
return expanded;
}};
if(prefix!=="margin"){
jQuery.cssHooks[ prefix + suffix ].set=setPositiveNumber;
}});
jQuery.fn.extend({
css: function(name, value){
return access(this, function(elem, name, value){
var styles, len,
map={},
i=0;
if(Array.isArray(name)){
styles=getStyles(elem);
len=name.length;
for(; i < len; i++){
map[ name[ i ] ]=jQuery.css(elem, name[ i ], false, styles);
}
return map;
}
return value!==undefined ?
jQuery.style(elem, name, value) :
jQuery.css(elem, name);
}, name, value, arguments.length > 1);
}});
function Tween(elem, options, prop, end, easing){
return new Tween.prototype.init(elem, options, prop, end, easing);
}
jQuery.Tween=Tween;
Tween.prototype={
constructor: Tween,
init: function(elem, options, prop, end, easing, unit){
this.elem=elem;
this.prop=prop;
this.easing=easing||jQuery.easing._default;
this.options=options;
this.start=this.now=this.cur();
this.end=end;
this.unit=unit||(jQuery.cssNumber[ prop ] ? "":"px");
},
cur: function(){
var hooks=Tween.propHooks[ this.prop ];
return hooks&&hooks.get ?
hooks.get(this) :
Tween.propHooks._default.get(this);
},
run: function(percent){
var eased,
hooks=Tween.propHooks[ this.prop ];
if(this.options.duration){
this.pos=eased=jQuery.easing[ this.easing ](
percent, this.options.duration * percent, 0, 1, this.options.duration
);
}else{
this.pos=eased=percent;
}
this.now=(this.end - this.start) * eased + this.start;
if(this.options.step){
this.options.step.call(this.elem, this.now, this);
}
if(hooks&&hooks.set){
hooks.set(this);
}else{
Tween.propHooks._default.set(this);
}
return this;
}};
Tween.prototype.init.prototype=Tween.prototype;
Tween.propHooks={
_default: {
get: function(tween){
var result;
if(tween.elem.nodeType!==1 ||
tween.elem[ tween.prop ]!=null&&tween.elem.style[ tween.prop ]==null){
return tween.elem[ tween.prop ];
}
result=jQuery.css(tween.elem, tween.prop, "");
return !result||result==="auto" ? 0:result;
},
set: function(tween){
if(jQuery.fx.step[ tween.prop ]){
jQuery.fx.step[ tween.prop ](tween);
}else if(tween.elem.nodeType===1 &&
(tween.elem.style[ jQuery.cssProps[ tween.prop ] ]!=null ||
jQuery.cssHooks[ tween.prop ])){
jQuery.style(tween.elem, tween.prop, tween.now + tween.unit);
}else{
tween.elem[ tween.prop ]=tween.now;
}}
}};
Tween.propHooks.scrollTop=Tween.propHooks.scrollLeft={
set: function(tween){
if(tween.elem.nodeType&&tween.elem.parentNode){
tween.elem[ tween.prop ]=tween.now;
}}
};
jQuery.easing={
linear: function(p){
return p;
},
swing: function(p){
return 0.5 - Math.cos(p * Math.PI) / 2;
},
_default: "swing"
};
jQuery.fx=Tween.prototype.init;
jQuery.fx.step={};
var
fxNow, inProgress,
rfxtypes=/^(?:toggle|show|hide)$/,
rrun=/queueHooks$/;
function schedule(){
if(inProgress){
if(document.hidden===false&&window.requestAnimationFrame){
window.requestAnimationFrame(schedule);
}else{
window.setTimeout(schedule, jQuery.fx.interval);
}
jQuery.fx.tick();
}}
function createFxNow(){
window.setTimeout(function(){
fxNow=undefined;
});
return(fxNow=Date.now());
}
function genFx(type, includeWidth){
var which,
i=0,
attrs={ height: type };
includeWidth=includeWidth ? 1:0;
for(; i < 4; i +=2 - includeWidth){
which=cssExpand[ i ];
attrs[ "margin" + which ]=attrs[ "padding" + which ]=type;
}
if(includeWidth){
attrs.opacity=attrs.width=type;
}
return attrs;
}
function createTween(value, prop, animation){
var tween,
collection=(Animation.tweeners[ prop ]||[]).concat(Animation.tweeners[ "*" ]),
index=0,
length=collection.length;
for(; index < length; index++){
if(( tween=collection[ index ].call(animation, prop, value))){
return tween;
}}
}
function defaultPrefilter(elem, props, opts){
var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
isBox="width" in props||"height" in props,
anim=this,
orig={},
style=elem.style,
hidden=elem.nodeType&&isHiddenWithinTree(elem),
dataShow=dataPriv.get(elem, "fxshow");
if(!opts.queue){
hooks=jQuery._queueHooks(elem, "fx");
if(hooks.unqueued==null){
hooks.unqueued=0;
oldfire=hooks.empty.fire;
hooks.empty.fire=function(){
if(!hooks.unqueued){
oldfire();
}};}
hooks.unqueued++;
anim.always(function(){
anim.always(function(){
hooks.unqueued--;
if(!jQuery.queue(elem, "fx").length){
hooks.empty.fire();
}});
});
}
for(prop in props){
value=props[ prop ];
if(rfxtypes.test(value)){
delete props[ prop ];
toggle=toggle||value==="toggle";
if(value===(hidden ? "hide":"show")){
if(value==="show"&&dataShow&&dataShow[ prop ]!==undefined){
hidden=true;
}else{
continue;
}}
orig[ prop ]=dataShow&&dataShow[ prop ]||jQuery.style(elem, prop);
}}
propTween = !jQuery.isEmptyObject(props);
if(!propTween&&jQuery.isEmptyObject(orig)){
return;
}
if(isBox&&elem.nodeType===1){
opts.overflow=[ style.overflow, style.overflowX, style.overflowY ];
restoreDisplay=dataShow&&dataShow.display;
if(restoreDisplay==null){
restoreDisplay=dataPriv.get(elem, "display");
}
display=jQuery.css(elem, "display");
if(display==="none"){
if(restoreDisplay){
display=restoreDisplay;
}else{
showHide([ elem ], true);
restoreDisplay=elem.style.display||restoreDisplay;
display=jQuery.css(elem, "display");
showHide([ elem ]);
}}
if(display==="inline"||display==="inline-block"&&restoreDisplay!=null){
if(jQuery.css(elem, "float")==="none"){
if(!propTween){
anim.done(function(){
style.display=restoreDisplay;
});
if(restoreDisplay==null){
display=style.display;
restoreDisplay=display==="none" ? "":display;
}}
style.display="inline-block";
}}
}
if(opts.overflow){
style.overflow="hidden";
anim.always(function(){
style.overflow=opts.overflow[ 0 ];
style.overflowX=opts.overflow[ 1 ];
style.overflowY=opts.overflow[ 2 ];
});
}
propTween=false;
for(prop in orig){
if(!propTween){
if(dataShow){
if("hidden" in dataShow){
hidden=dataShow.hidden;
}}else{
dataShow=dataPriv.access(elem, "fxshow", { display: restoreDisplay });
}
if(toggle){
dataShow.hidden = !hidden;
}
if(hidden){
showHide([ elem ], true);
}
anim.done(function(){
if(!hidden){
showHide([ elem ]);
}
dataPriv.remove(elem, "fxshow");
for(prop in orig){
jQuery.style(elem, prop, orig[ prop ]);
}});
}
propTween=createTween(hidden ? dataShow[ prop ]:0, prop, anim);
if(!(prop in dataShow)){
dataShow[ prop ]=propTween.start;
if(hidden){
propTween.end=propTween.start;
propTween.start=0;
}}
}}
function propFilter(props, specialEasing){
var index, name, easing, value, hooks;
for(index in props){
name=camelCase(index);
easing=specialEasing[ name ];
value=props[ index ];
if(Array.isArray(value)){
easing=value[ 1 ];
value=props[ index ]=value[ 0 ];
}
if(index!==name){
props[ name ]=value;
delete props[ index ];
}
hooks=jQuery.cssHooks[ name ];
if(hooks&&"expand" in hooks){
value=hooks.expand (value);
delete props[ name ];
for(index in value){
if(!(index in props)){
props[ index ]=value[ index ];
specialEasing[ index ]=easing;
}}
}else{
specialEasing[ name ]=easing;
}}
}
function Animation(elem, properties, options){
var result,
stopped,
index=0,
length=Animation.prefilters.length,
deferred=jQuery.Deferred().always(function(){
delete tick.elem;
}),
tick=function(){
if(stopped){
return false;
}
var currentTime=fxNow||createFxNow(),
remaining=Math.max(0, animation.startTime + animation.duration - currentTime),
temp=remaining / animation.duration||0,
percent=1 - temp,
index=0,
length=animation.tweens.length;
for(; index < length; index++){
animation.tweens[ index ].run(percent);
}
deferred.notifyWith(elem, [ animation, percent, remaining ]);
if(percent < 1&&length){
return remaining;
}
if(!length){
deferred.notifyWith(elem, [ animation, 1, 0 ]);
}
deferred.resolveWith(elem, [ animation ]);
return false;
},
animation=deferred.promise({
elem: elem,
props: jQuery.extend({}, properties),
opts: jQuery.extend(true, {
specialEasing: {},
easing: jQuery.easing._default
}, options),
originalProperties: properties,
originalOptions: options,
startTime: fxNow||createFxNow(),
duration: options.duration,
tweens: [],
createTween: function(prop, end){
var tween=jQuery.Tween(elem, animation.opts, prop, end,
animation.opts.specialEasing[ prop ]||animation.opts.easing);
animation.tweens.push(tween);
return tween;
},
stop: function(gotoEnd){
var index=0,
length=gotoEnd ? animation.tweens.length:0;
if(stopped){
return this;
}
stopped=true;
for(; index < length; index++){
animation.tweens[ index ].run(1);
}
if(gotoEnd){
deferred.notifyWith(elem, [ animation, 1, 0 ]);
deferred.resolveWith(elem, [ animation, gotoEnd ]);
}else{
deferred.rejectWith(elem, [ animation, gotoEnd ]);
}
return this;
}}),
props=animation.props;
propFilter(props, animation.opts.specialEasing);
for(; index < length; index++){
result=Animation.prefilters[ index ].call(animation, elem, props, animation.opts);
if(result){
if(isFunction(result.stop)){
jQuery._queueHooks(animation.elem, animation.opts.queue).stop =
result.stop.bind(result);
}
return result;
}}
jQuery.map(props, createTween, animation);
if(isFunction(animation.opts.start)){
animation.opts.start.call(elem, animation);
}
animation
.progress(animation.opts.progress)
.done(animation.opts.done, animation.opts.complete)
.fail(animation.opts.fail)
.always(animation.opts.always);
jQuery.fx.timer(jQuery.extend(tick, {
elem: elem,
anim: animation,
queue: animation.opts.queue
})
);
return animation;
}
jQuery.Animation=jQuery.extend(Animation, {
tweeners: {
"*": [ function(prop, value){
var tween=this.createTween(prop, value);
adjustCSS(tween.elem, prop, rcssNum.exec(value), tween);
return tween;
} ]
},
tweener: function(props, callback){
if(isFunction(props)){
callback=props;
props=[ "*" ];
}else{
props=props.match(rnothtmlwhite);
}
var prop,
index=0,
length=props.length;
for(; index < length; index++){
prop=props[ index ];
Animation.tweeners[ prop ]=Animation.tweeners[ prop ]||[];
Animation.tweeners[ prop ].unshift(callback);
}},
prefilters: [ defaultPrefilter ],
prefilter: function(callback, prepend){
if(prepend){
Animation.prefilters.unshift(callback);
}else{
Animation.prefilters.push(callback);
}}
});
jQuery.speed=function(speed, easing, fn){
var opt=speed&&typeof speed==="object" ? jQuery.extend({}, speed):{
complete: fn||!fn&&easing ||
isFunction(speed)&&speed,
duration: speed,
easing: fn&&easing||easing&&!isFunction(easing)&&easing
};
if(jQuery.fx.off){
opt.duration=0;
}else{
if(typeof opt.duration!=="number"){
if(opt.duration in jQuery.fx.speeds){
opt.duration=jQuery.fx.speeds[ opt.duration ];
}else{
opt.duration=jQuery.fx.speeds._default;
}}
}
if(opt.queue==null||opt.queue===true){
opt.queue="fx";
}
opt.old=opt.complete;
opt.complete=function(){
if(isFunction(opt.old)){
opt.old.call(this);
}
if(opt.queue){
jQuery.dequeue(this, opt.queue);
}};
return opt;
};
jQuery.fn.extend({
fadeTo: function(speed, to, easing, callback){
return this.filter(isHiddenWithinTree).css("opacity", 0).show()
.end().animate({ opacity: to }, speed, easing, callback);
},
animate: function(prop, speed, easing, callback){
var empty=jQuery.isEmptyObject(prop),
optall=jQuery.speed(speed, easing, callback),
doAnimation=function(){
var anim=Animation(this, jQuery.extend({}, prop), optall);
if(empty||dataPriv.get(this, "finish")){
anim.stop(true);
}};
doAnimation.finish=doAnimation;
return empty||optall.queue===false ?
this.each(doAnimation) :
this.queue(optall.queue, doAnimation);
},
stop: function(type, clearQueue, gotoEnd){
var stopQueue=function(hooks){
var stop=hooks.stop;
delete hooks.stop;
stop(gotoEnd);
};
if(typeof type!=="string"){
gotoEnd=clearQueue;
clearQueue=type;
type=undefined;
}
if(clearQueue&&type!==false){
this.queue(type||"fx", []);
}
return this.each(function(){
var dequeue=true,
index=type!=null&&type + "queueHooks",
timers=jQuery.timers,
data=dataPriv.get(this);
if(index){
if(data[ index ]&&data[ index ].stop){
stopQueue(data[ index ]);
}}else{
for(index in data){
if(data[ index ]&&data[ index ].stop&&rrun.test(index)){
stopQueue(data[ index ]);
}}
}
for(index=timers.length; index--;){
if(timers[ index ].elem===this &&
(type==null||timers[ index ].queue===type)){
timers[ index ].anim.stop(gotoEnd);
dequeue=false;
timers.splice(index, 1);
}}
if(dequeue||!gotoEnd){
jQuery.dequeue(this, type);
}});
},
finish: function(type){
if(type!==false){
type=type||"fx";
}
return this.each(function(){
var index,
data=dataPriv.get(this),
queue=data[ type + "queue" ],
hooks=data[ type + "queueHooks" ],
timers=jQuery.timers,
length=queue ? queue.length:0;
data.finish=true;
jQuery.queue(this, type, []);
if(hooks&&hooks.stop){
hooks.stop.call(this, true);
}
for(index=timers.length; index--;){
if(timers[ index ].elem===this&&timers[ index ].queue===type){
timers[ index ].anim.stop(true);
timers.splice(index, 1);
}}
for(index=0; index < length; index++){
if(queue[ index ]&&queue[ index ].finish){
queue[ index ].finish.call(this);
}}
delete data.finish;
});
}});
jQuery.each([ "toggle", "show", "hide" ], function(i, name){
var cssFn=jQuery.fn[ name ];
jQuery.fn[ name ]=function(speed, easing, callback){
return speed==null||typeof speed==="boolean" ?
cssFn.apply(this, arguments) :
this.animate(genFx(name, true), speed, easing, callback);
};});
jQuery.each({
slideDown: genFx("show"),
slideUp: genFx("hide"),
slideToggle: genFx("toggle"),
fadeIn: { opacity: "show" },
fadeOut: { opacity: "hide" },
fadeToggle: { opacity: "toggle" }}, function(name, props){
jQuery.fn[ name ]=function(speed, easing, callback){
return this.animate(props, speed, easing, callback);
};});
jQuery.timers=[];
jQuery.fx.tick=function(){
var timer,
i=0,
timers=jQuery.timers;
fxNow=Date.now();
for(; i < timers.length; i++){
timer=timers[ i ];
if(!timer()&&timers[ i ]===timer){
timers.splice(i--, 1);
}}
if(!timers.length){
jQuery.fx.stop();
}
fxNow=undefined;
};
jQuery.fx.timer=function(timer){
jQuery.timers.push(timer);
jQuery.fx.start();
};
jQuery.fx.interval=13;
jQuery.fx.start=function(){
if(inProgress){
return;
}
inProgress=true;
schedule();
};
jQuery.fx.stop=function(){
inProgress=null;
};
jQuery.fx.speeds={
slow: 600,
fast: 200,
_default: 400
};
jQuery.fn.delay=function(time, type){
time=jQuery.fx ? jQuery.fx.speeds[ time ]||time:time;
type=type||"fx";
return this.queue(type, function(next, hooks){
var timeout=window.setTimeout(next, time);
hooks.stop=function(){
window.clearTimeout(timeout);
};});
};
(function(){
var input=document.createElement("input"),
select=document.createElement("select"),
opt=select.appendChild(document.createElement("option"));
input.type="checkbox";
support.checkOn=input.value!=="";
support.optSelected=opt.selected;
input=document.createElement("input");
input.value="t";
input.type="radio";
support.radioValue=input.value==="t";
})();
var boolHook,
attrHandle=jQuery.expr.attrHandle;
jQuery.fn.extend({
attr: function(name, value){
return access(this, jQuery.attr, name, value, arguments.length > 1);
},
removeAttr: function(name){
return this.each(function(){
jQuery.removeAttr(this, name);
});
}});
jQuery.extend({
attr: function(elem, name, value){
var ret, hooks,
nType=elem.nodeType;
if(nType===3||nType===8||nType===2){
return;
}
if(typeof elem.getAttribute==="undefined"){
return jQuery.prop(elem, name, value);
}
if(nType!==1||!jQuery.isXMLDoc(elem)){
hooks=jQuery.attrHooks[ name.toLowerCase() ] ||
(jQuery.expr.match.bool.test(name) ? boolHook:undefined);
}
if(value!==undefined){
if(value===null){
jQuery.removeAttr(elem, name);
return;
}
if(hooks&&"set" in hooks &&
(ret=hooks.set(elem, value, name))!==undefined){
return ret;
}
elem.setAttribute(name, value + "");
return value;
}
if(hooks&&"get" in hooks&&(ret=hooks.get(elem, name))!==null){
return ret;
}
ret=jQuery.find.attr(elem, name);
return ret==null ? undefined:ret;
},
attrHooks: {
type: {
set: function(elem, value){
if(!support.radioValue&&value==="radio" &&
nodeName(elem, "input")){
var val=elem.value;
elem.setAttribute("type", value);
if(val){
elem.value=val;
}
return value;
}}
}},
removeAttr: function(elem, value){
var name,
i=0,
attrNames=value&&value.match(rnothtmlwhite);
if(attrNames&&elem.nodeType===1){
while(( name=attrNames[ i++ ])){
elem.removeAttribute(name);
}}
}});
boolHook={
set: function(elem, value, name){
if(value===false){
jQuery.removeAttr(elem, name);
}else{
elem.setAttribute(name, name);
}
return name;
}};
jQuery.each(jQuery.expr.match.bool.source.match(/\w+/g), function(i, name){
var getter=attrHandle[ name ]||jQuery.find.attr;
attrHandle[ name ]=function(elem, name, isXML){
var ret, handle,
lowercaseName=name.toLowerCase();
if(!isXML){
handle=attrHandle[ lowercaseName ];
attrHandle[ lowercaseName ]=ret;
ret=getter(elem, name, isXML)!=null ?
lowercaseName :
null;
attrHandle[ lowercaseName ]=handle;
}
return ret;
};});
var rfocusable=/^(?:input|select|textarea|button)$/i,
rclickable=/^(?:a|area)$/i;
jQuery.fn.extend({
prop: function(name, value){
return access(this, jQuery.prop, name, value, arguments.length > 1);
},
removeProp: function(name){
return this.each(function(){
delete this[ jQuery.propFix[ name ]||name ];
});
}});
jQuery.extend({
prop: function(elem, name, value){
var ret, hooks,
nType=elem.nodeType;
if(nType===3||nType===8||nType===2){
return;
}
if(nType!==1||!jQuery.isXMLDoc(elem)){
name=jQuery.propFix[ name ]||name;
hooks=jQuery.propHooks[ name ];
}
if(value!==undefined){
if(hooks&&"set" in hooks &&
(ret=hooks.set(elem, value, name))!==undefined){
return ret;
}
return(elem[ name ]=value);
}
if(hooks&&"get" in hooks&&(ret=hooks.get(elem, name))!==null){
return ret;
}
return elem[ name ];
},
propHooks: {
tabIndex: {
get: function(elem){
var tabindex=jQuery.find.attr(elem, "tabindex");
if(tabindex){
return parseInt(tabindex, 10);
}
if(rfocusable.test(elem.nodeName) ||
rclickable.test(elem.nodeName) &&
elem.href
){
return 0;
}
return -1;
}}
},
propFix: {
"for": "htmlFor",
"class": "className"
}});
if(!support.optSelected){
jQuery.propHooks.selected={
get: function(elem){
var parent=elem.parentNode;
if(parent&&parent.parentNode){
parent.parentNode.selectedIndex;
}
return null;
},
set: function(elem){
var parent=elem.parentNode;
if(parent){
parent.selectedIndex;
if(parent.parentNode){
parent.parentNode.selectedIndex;
}}
}};}
jQuery.each([
"tabIndex",
"readOnly",
"maxLength",
"cellSpacing",
"cellPadding",
"rowSpan",
"colSpan",
"useMap",
"frameBorder",
"contentEditable"
], function(){
jQuery.propFix[ this.toLowerCase() ]=this;
});
function stripAndCollapse(value){
var tokens=value.match(rnothtmlwhite)||[];
return tokens.join(" ");
}
function getClass(elem){
return elem.getAttribute&&elem.getAttribute("class")||"";
}
function classesToArray(value){
if(Array.isArray(value)){
return value;
}
if(typeof value==="string"){
return value.match(rnothtmlwhite)||[];
}
return [];
}
jQuery.fn.extend({
addClass: function(value){
var classes, elem, cur, curValue, clazz, j, finalValue,
i=0;
if(isFunction(value)){
return this.each(function(j){
jQuery(this).addClass(value.call(this, j, getClass(this)));
});
}
classes=classesToArray(value);
if(classes.length){
while(( elem=this[ i++ ])){
curValue=getClass(elem);
cur=elem.nodeType===1&&(" " + stripAndCollapse(curValue) + " ");
if(cur){
j=0;
while(( clazz=classes[ j++ ])){
if(cur.indexOf(" " + clazz + " ") < 0){
cur +=clazz + " ";
}}
finalValue=stripAndCollapse(cur);
if(curValue!==finalValue){
elem.setAttribute("class", finalValue);
}}
}}
return this;
},
removeClass: function(value){
var classes, elem, cur, curValue, clazz, j, finalValue,
i=0;
if(isFunction(value)){
return this.each(function(j){
jQuery(this).removeClass(value.call(this, j, getClass(this)));
});
}
if(!arguments.length){
return this.attr("class", "");
}
classes=classesToArray(value);
if(classes.length){
while(( elem=this[ i++ ])){
curValue=getClass(elem);
cur=elem.nodeType===1&&(" " + stripAndCollapse(curValue) + " ");
if(cur){
j=0;
while(( clazz=classes[ j++ ])){
while(cur.indexOf(" " + clazz + " ") > -1){
cur=cur.replace(" " + clazz + " ", " ");
}}
finalValue=stripAndCollapse(cur);
if(curValue!==finalValue){
elem.setAttribute("class", finalValue);
}}
}}
return this;
},
toggleClass: function(value, stateVal){
var type=typeof value,
isValidValue=type==="string"||Array.isArray(value);
if(typeof stateVal==="boolean"&&isValidValue){
return stateVal ? this.addClass(value):this.removeClass(value);
}
if(isFunction(value)){
return this.each(function(i){
jQuery(this).toggleClass(value.call(this, i, getClass(this), stateVal),
stateVal
);
});
}
return this.each(function(){
var className, i, self, classNames;
if(isValidValue){
i=0;
self=jQuery(this);
classNames=classesToArray(value);
while(( className=classNames[ i++ ])){
if(self.hasClass(className)){
self.removeClass(className);
}else{
self.addClass(className);
}}
}else if(value===undefined||type==="boolean"){
className=getClass(this);
if(className){
dataPriv.set(this, "__className__", className);
}
if(this.setAttribute){
this.setAttribute("class",
className||value===false ?
"" :
dataPriv.get(this, "__className__")||""
);
}}
});
},
hasClass: function(selector){
var className, elem,
i=0;
className=" " + selector + " ";
while(( elem=this[ i++ ])){
if(elem.nodeType===1 &&
(" " + stripAndCollapse(getClass(elem)) + " ").indexOf(className) > -1){
return true;
}}
return false;
}});
var rreturn=/\r/g;
jQuery.fn.extend({
val: function(value){
var hooks, ret, valueIsFunction,
elem=this[ 0 ];
if(!arguments.length){
if(elem){
hooks=jQuery.valHooks[ elem.type ] ||
jQuery.valHooks[ elem.nodeName.toLowerCase() ];
if(hooks &&
"get" in hooks &&
(ret=hooks.get(elem, "value"))!==undefined
){
return ret;
}
ret=elem.value;
if(typeof ret==="string"){
return ret.replace(rreturn, "");
}
return ret==null ? "":ret;
}
return;
}
valueIsFunction=isFunction(value);
return this.each(function(i){
var val;
if(this.nodeType!==1){
return;
}
if(valueIsFunction){
val=value.call(this, i, jQuery(this).val());
}else{
val=value;
}
if(val==null){
val="";
}else if(typeof val==="number"){
val +="";
}else if(Array.isArray(val)){
val=jQuery.map(val, function(value){
return value==null ? "":value + "";
});
}
hooks=jQuery.valHooks[ this.type ]||jQuery.valHooks[ this.nodeName.toLowerCase() ];
if(!hooks||!("set" in hooks)||hooks.set(this, val, "value")===undefined){
this.value=val;
}});
}});
jQuery.extend({
valHooks: {
option: {
get: function(elem){
var val=jQuery.find.attr(elem, "value");
return val!=null ?
val :
stripAndCollapse(jQuery.text(elem));
}},
select: {
get: function(elem){
var value, option, i,
options=elem.options,
index=elem.selectedIndex,
one=elem.type==="select-one",
values=one ? null:[],
max=one ? index + 1:options.length;
if(index < 0){
i=max;
}else{
i=one ? index:0;
}
for(; i < max; i++){
option=options[ i ];
if(( option.selected||i===index) &&
!option.disabled &&
(!option.parentNode.disabled ||
!nodeName(option.parentNode, "optgroup"))){
value=jQuery(option).val();
if(one){
return value;
}
values.push(value);
}}
return values;
},
set: function(elem, value){
var optionSet, option,
options=elem.options,
values=jQuery.makeArray(value),
i=options.length;
while(i--){
option=options[ i ];
if(option.selected =
jQuery.inArray(jQuery.valHooks.option.get(option), values) > -1
){
optionSet=true;
}
}
if(!optionSet){
elem.selectedIndex=-1;
}
return values;
}}
}});
jQuery.each([ "radio", "checkbox" ], function(){
jQuery.valHooks[ this ]={
set: function(elem, value){
if(Array.isArray(value)){
return(elem.checked=jQuery.inArray(jQuery(elem).val(), value) > -1);
}}
};
if(!support.checkOn){
jQuery.valHooks[ this ].get=function(elem){
return elem.getAttribute("value")===null ? "on":elem.value;
};}});
support.focusin="onfocusin" in window;
var rfocusMorph=/^(?:focusinfocus|focusoutblur)$/,
stopPropagationCallback=function(e){
e.stopPropagation();
};
jQuery.extend(jQuery.event, {
trigger: function(event, data, elem, onlyHandlers){
var i, cur, tmp, bubbleType, ontype, handle, special, lastElement,
eventPath=[ elem||document ],
type=hasOwn.call(event, "type") ? event.type:event,
namespaces=hasOwn.call(event, "namespace") ? event.namespace.split("."):[];
cur=lastElement=tmp=elem=elem||document;
if(elem.nodeType===3||elem.nodeType===8){
return;
}
if(rfocusMorph.test(type + jQuery.event.triggered)){
return;
}
if(type.indexOf(".") > -1){
namespaces=type.split(".");
type=namespaces.shift();
namespaces.sort();
}
ontype=type.indexOf(":") < 0&&"on" + type;
event=event[ jQuery.expando ] ?
event :
new jQuery.Event(type, typeof event==="object"&&event);
event.isTrigger=onlyHandlers ? 2:3;
event.namespace=namespaces.join(".");
event.rnamespace=event.namespace ?
new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)") :
null;
event.result=undefined;
if(!event.target){
event.target=elem;
}
data=data==null ?
[ event ] :
jQuery.makeArray(data, [ event ]);
special=jQuery.event.special[ type ]||{};
if(!onlyHandlers&&special.trigger&&special.trigger.apply(elem, data)===false){
return;
}
if(!onlyHandlers&&!special.noBubble&&!isWindow(elem)){
bubbleType=special.delegateType||type;
if(!rfocusMorph.test(bubbleType + type)){
cur=cur.parentNode;
}
for(; cur; cur=cur.parentNode){
eventPath.push(cur);
tmp=cur;
}
if(tmp===(elem.ownerDocument||document)){
eventPath.push(tmp.defaultView||tmp.parentWindow||window);
}}
i=0;
while(( cur=eventPath[ i++ ])&&!event.isPropagationStopped()){
lastElement=cur;
event.type=i > 1 ?
bubbleType :
special.bindType||type;
handle=(dataPriv.get(cur, "events")||{})[ event.type ] &&
dataPriv.get(cur, "handle");
if(handle){
handle.apply(cur, data);
}
handle=ontype&&cur[ ontype ];
if(handle&&handle.apply&&acceptData(cur)){
event.result=handle.apply(cur, data);
if(event.result===false){
event.preventDefault();
}}
}
event.type=type;
if(!onlyHandlers&&!event.isDefaultPrevented()){
if(( !special._default ||
special._default.apply(eventPath.pop(), data)===false) &&
acceptData(elem)){
if(ontype&&isFunction(elem[ type ])&&!isWindow(elem)){
tmp=elem[ ontype ];
if(tmp){
elem[ ontype ]=null;
}
jQuery.event.triggered=type;
if(event.isPropagationStopped()){
lastElement.addEventListener(type, stopPropagationCallback);
}
elem[ type ]();
if(event.isPropagationStopped()){
lastElement.removeEventListener(type, stopPropagationCallback);
}
jQuery.event.triggered=undefined;
if(tmp){
elem[ ontype ]=tmp;
}}
}}
return event.result;
},
simulate: function(type, elem, event){
var e=jQuery.extend(new jQuery.Event(),
event,
{
type: type,
isSimulated: true
}
);
jQuery.event.trigger(e, null, elem);
}});
jQuery.fn.extend({
trigger: function(type, data){
return this.each(function(){
jQuery.event.trigger(type, data, this);
});
},
triggerHandler: function(type, data){
var elem=this[ 0 ];
if(elem){
return jQuery.event.trigger(type, data, elem, true);
}}
});
if(!support.focusin){
jQuery.each({ focus: "focusin", blur: "focusout" }, function(orig, fix){
var handler=function(event){
jQuery.event.simulate(fix, event.target, jQuery.event.fix(event));
};
jQuery.event.special[ fix ]={
setup: function(){
var doc=this.ownerDocument||this,
attaches=dataPriv.access(doc, fix);
if(!attaches){
doc.addEventListener(orig, handler, true);
}
dataPriv.access(doc, fix,(attaches||0) + 1);
},
teardown: function(){
var doc=this.ownerDocument||this,
attaches=dataPriv.access(doc, fix) - 1;
if(!attaches){
doc.removeEventListener(orig, handler, true);
dataPriv.remove(doc, fix);
}else{
dataPriv.access(doc, fix, attaches);
}}
};});
}
var location=window.location;
var nonce=Date.now();
var rquery=(/\?/);
jQuery.parseXML=function(data){
var xml;
if(!data||typeof data!=="string"){
return null;
}
try {
xml=(new window.DOMParser()).parseFromString(data, "text/xml");
} catch(e){
xml=undefined;
}
if(!xml||xml.getElementsByTagName("parsererror").length){
jQuery.error("Invalid XML: " + data);
}
return xml;
};
var
rbracket=/\[\]$/,
rCRLF=/\r?\n/g,
rsubmitterTypes=/^(?:submit|button|image|reset|file)$/i,
rsubmittable=/^(?:input|select|textarea|keygen)/i;
function buildParams(prefix, obj, traditional, add){
var name;
if(Array.isArray(obj)){
jQuery.each(obj, function(i, v){
if(traditional||rbracket.test(prefix)){
add(prefix, v);
}else{
buildParams(
prefix + "[" +(typeof v==="object"&&v!=null ? i:"") + "]",
v,
traditional,
add
);
}});
}else if(!traditional&&toType(obj)==="object"){
for(name in obj){
buildParams(prefix + "[" + name + "]", obj[ name ], traditional, add);
}}else{
add(prefix, obj);
}}
jQuery.param=function(a, traditional){
var prefix,
s=[],
add=function(key, valueOrFunction){
var value=isFunction(valueOrFunction) ?
valueOrFunction() :
valueOrFunction;
s[ s.length ]=encodeURIComponent(key) + "=" +
encodeURIComponent(value==null ? "":value);
};
if(Array.isArray(a)||(a.jquery&&!jQuery.isPlainObject(a))){
jQuery.each(a, function(){
add(this.name, this.value);
});
}else{
for(prefix in a){
buildParams(prefix, a[ prefix ], traditional, add);
}}
return s.join("&");
};
jQuery.fn.extend({
serialize: function(){
return jQuery.param(this.serializeArray());
},
serializeArray: function(){
return this.map(function(){
var elements=jQuery.prop(this, "elements");
return elements ? jQuery.makeArray(elements):this;
})
.filter(function(){
var type=this.type;
return this.name&&!jQuery(this).is(":disabled") &&
rsubmittable.test(this.nodeName)&&!rsubmitterTypes.test(type) &&
(this.checked||!rcheckableType.test(type));
})
.map(function(i, elem){
var val=jQuery(this).val();
if(val==null){
return null;
}
if(Array.isArray(val)){
return jQuery.map(val, function(val){
return { name: elem.name, value: val.replace(rCRLF, "\r\n") };});
}
return { name: elem.name, value: val.replace(rCRLF, "\r\n") };}).get();
}});
var
r20=/%20/g,
rhash=/#.*$/,
rantiCache=/([?&])_=[^&]*/,
rheaders=/^(.*?):[ \t]*([^\r\n]*)$/mg,
rlocalProtocol=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
rnoContent=/^(?:GET|HEAD)$/,
rprotocol=/^\/\//,
prefilters={},
transports={},
allTypes="*/".concat("*"),
originAnchor=document.createElement("a");
originAnchor.href=location.href;
function addToPrefiltersOrTransports(structure){
return function(dataTypeExpression, func){
if(typeof dataTypeExpression!=="string"){
func=dataTypeExpression;
dataTypeExpression="*";
}
var dataType,
i=0,
dataTypes=dataTypeExpression.toLowerCase().match(rnothtmlwhite)||[];
if(isFunction(func)){
while(( dataType=dataTypes[ i++ ])){
if(dataType[ 0 ]==="+"){
dataType=dataType.slice(1)||"*";
(structure[ dataType ]=structure[ dataType ]||[]).unshift(func);
}else{
(structure[ dataType ]=structure[ dataType ]||[]).push(func);
}}
}};}
function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR){
var inspected={},
seekingTransport=(structure===transports);
function inspect(dataType){
var selected;
inspected[ dataType ]=true;
jQuery.each(structure[ dataType ]||[], function(_, prefilterOrFactory){
var dataTypeOrTransport=prefilterOrFactory(options, originalOptions, jqXHR);
if(typeof dataTypeOrTransport==="string" &&
!seekingTransport&&!inspected[ dataTypeOrTransport ]){
options.dataTypes.unshift(dataTypeOrTransport);
inspect(dataTypeOrTransport);
return false;
}else if(seekingTransport){
return !(selected=dataTypeOrTransport);
}});
return selected;
}
return inspect(options.dataTypes[ 0 ])||!inspected[ "*" ]&&inspect("*");
}
function ajaxExtend(target, src){
var key, deep,
flatOptions=jQuery.ajaxSettings.flatOptions||{};
for(key in src){
if(src[ key ]!==undefined){
(flatOptions[ key ] ? target:(deep||(deep={})))[ key ]=src[ key ];
}}
if(deep){
jQuery.extend(true, target, deep);
}
return target;
}
function ajaxHandleResponses(s, jqXHR, responses){
var ct, type, finalDataType, firstDataType,
contents=s.contents,
dataTypes=s.dataTypes;
while(dataTypes[ 0 ]==="*"){
dataTypes.shift();
if(ct===undefined){
ct=s.mimeType||jqXHR.getResponseHeader("Content-Type");
}}
if(ct){
for(type in contents){
if(contents[ type ]&&contents[ type ].test(ct)){
dataTypes.unshift(type);
break;
}}
}
if(dataTypes[ 0 ] in responses){
finalDataType=dataTypes[ 0 ];
}else{
for(type in responses){
if(!dataTypes[ 0 ]||s.converters[ type + " " + dataTypes[ 0 ] ]){
finalDataType=type;
break;
}
if(!firstDataType){
firstDataType=type;
}}
finalDataType=finalDataType||firstDataType;
}
if(finalDataType){
if(finalDataType!==dataTypes[ 0 ]){
dataTypes.unshift(finalDataType);
}
return responses[ finalDataType ];
}}
function ajaxConvert(s, response, jqXHR, isSuccess){
var conv2, current, conv, tmp, prev,
converters={},
dataTypes=s.dataTypes.slice();
if(dataTypes[ 1 ]){
for(conv in s.converters){
converters[ conv.toLowerCase() ]=s.converters[ conv ];
}}
current=dataTypes.shift();
while(current){
if(s.responseFields[ current ]){
jqXHR[ s.responseFields[ current ] ]=response;
}
if(!prev&&isSuccess&&s.dataFilter){
response=s.dataFilter(response, s.dataType);
}
prev=current;
current=dataTypes.shift();
if(current){
if(current==="*"){
current=prev;
}else if(prev!=="*"&&prev!==current){
conv=converters[ prev + " " + current ]||converters[ "* " + current ];
if(!conv){
for(conv2 in converters){
tmp=conv2.split(" ");
if(tmp[ 1 ]===current){
conv=converters[ prev + " " + tmp[ 0 ] ] ||
converters[ "* " + tmp[ 0 ] ];
if(conv){
if(conv===true){
conv=converters[ conv2 ];
}else if(converters[ conv2 ]!==true){
current=tmp[ 0 ];
dataTypes.unshift(tmp[ 1 ]);
}
break;
}}
}}
if(conv!==true){
if(conv&&s.throws){
response=conv(response);
}else{
try {
response=conv(response);
} catch(e){
return {
state: "parsererror",
error: conv ? e:"No conversion from " + prev + " to " + current
};}}
}}
}}
return { state: "success", data: response };}
jQuery.extend({
active: 0,
lastModified: {},
etag: {},
ajaxSettings: {
url: location.href,
type: "GET",
isLocal: rlocalProtocol.test(location.protocol),
global: true,
processData: true,
async: true,
contentType: "application/x-www-form-urlencoded; charset=UTF-8",
accepts: {
"*": allTypes,
text: "text/plain",
html: "text/html",
xml: "application/xml, text/xml",
json: "application/json, text/javascript"
},
contents: {
xml: /\bxml\b/,
html: /\bhtml/,
json: /\bjson\b/
},
responseFields: {
xml: "responseXML",
text: "responseText",
json: "responseJSON"
},
converters: {
"* text": String,
"text html": true,
"text json": JSON.parse,
"text xml": jQuery.parseXML
},
flatOptions: {
url: true,
context: true
}},
ajaxSetup: function(target, settings){
return settings ?
ajaxExtend(ajaxExtend(target, jQuery.ajaxSettings), settings) :
ajaxExtend(jQuery.ajaxSettings, target);
},
ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
ajaxTransport: addToPrefiltersOrTransports(transports),
ajax: function(url, options){
if(typeof url==="object"){
options=url;
url=undefined;
}
options=options||{};
var transport,
cacheURL,
responseHeadersString,
responseHeaders,
timeoutTimer,
urlAnchor,
completed,
fireGlobals,
i,
uncached,
s=jQuery.ajaxSetup({}, options),
callbackContext=s.context||s,
globalEventContext=s.context &&
(callbackContext.nodeType||callbackContext.jquery) ?
jQuery(callbackContext) :
jQuery.event,
deferred=jQuery.Deferred(),
completeDeferred=jQuery.Callbacks("once memory"),
statusCode=s.statusCode||{},
requestHeaders={},
requestHeadersNames={},
strAbort="canceled",
jqXHR={
readyState: 0,
getResponseHeader: function(key){
var match;
if(completed){
if(!responseHeaders){
responseHeaders={};
while(( match=rheaders.exec(responseHeadersString))){
responseHeaders[ match[ 1 ].toLowerCase() ]=match[ 2 ];
}}
match=responseHeaders[ key.toLowerCase() ];
}
return match==null ? null:match;
},
getAllResponseHeaders: function(){
return completed ? responseHeadersString:null;
},
setRequestHeader: function(name, value){
if(completed==null){
name=requestHeadersNames[ name.toLowerCase() ] =
requestHeadersNames[ name.toLowerCase() ]||name;
requestHeaders[ name ]=value;
}
return this;
},
overrideMimeType: function(type){
if(completed==null){
s.mimeType=type;
}
return this;
},
statusCode: function(map){
var code;
if(map){
if(completed){
jqXHR.always(map[ jqXHR.status ]);
}else{
for(code in map){
statusCode[ code ]=[ statusCode[ code ], map[ code ] ];
}}
}
return this;
},
abort: function(statusText){
var finalText=statusText||strAbort;
if(transport){
transport.abort(finalText);
}
done(0, finalText);
return this;
}};
deferred.promise(jqXHR);
s.url=(( url||s.url||location.href) + "")
.replace(rprotocol, location.protocol + "//");
s.type=options.method||options.type||s.method||s.type;
s.dataTypes=(s.dataType||"*").toLowerCase().match(rnothtmlwhite)||[ "" ];
if(s.crossDomain==null){
urlAnchor=document.createElement("a");
try {
urlAnchor.href=s.url;
urlAnchor.href=urlAnchor.href;
s.crossDomain=originAnchor.protocol + "//" + originAnchor.host!==urlAnchor.protocol + "//" + urlAnchor.host;
} catch(e){
s.crossDomain=true;
}}
if(s.data&&s.processData&&typeof s.data!=="string"){
s.data=jQuery.param(s.data, s.traditional);
}
inspectPrefiltersOrTransports(prefilters, s, options, jqXHR);
if(completed){
return jqXHR;
}
fireGlobals=jQuery.event&&s.global;
if(fireGlobals&&jQuery.active++===0){
jQuery.event.trigger("ajaxStart");
}
s.type=s.type.toUpperCase();
s.hasContent = !rnoContent.test(s.type);
cacheURL=s.url.replace(rhash, "");
if(!s.hasContent){
uncached=s.url.slice(cacheURL.length);
if(s.data&&(s.processData||typeof s.data==="string")){
cacheURL +=(rquery.test(cacheURL) ? "&":"?") + s.data;
delete s.data;
}
if(s.cache===false){
cacheURL=cacheURL.replace(rantiCache, "$1");
uncached=(rquery.test(cacheURL) ? "&":"?") + "_=" +(nonce++) + uncached;
}
s.url=cacheURL + uncached;
}else if(s.data&&s.processData &&
(s.contentType||"").indexOf("application/x-www-form-urlencoded")===0){
s.data=s.data.replace(r20, "+");
}
if(s.ifModified){
if(jQuery.lastModified[ cacheURL ]){
jqXHR.setRequestHeader("If-Modified-Since", jQuery.lastModified[ cacheURL ]);
}
if(jQuery.etag[ cacheURL ]){
jqXHR.setRequestHeader("If-None-Match", jQuery.etag[ cacheURL ]);
}}
if(s.data&&s.hasContent&&s.contentType!==false||options.contentType){
jqXHR.setRequestHeader("Content-Type", s.contentType);
}
jqXHR.setRequestHeader("Accept",
s.dataTypes[ 0 ]&&s.accepts[ s.dataTypes[ 0 ] ] ?
s.accepts[ s.dataTypes[ 0 ] ] +
(s.dataTypes[ 0 ]!=="*" ? ", " + allTypes + "; q=0.01":"") :
s.accepts[ "*" ]
);
for(i in s.headers){
jqXHR.setRequestHeader(i, s.headers[ i ]);
}
if(s.beforeSend &&
(s.beforeSend.call(callbackContext, jqXHR, s)===false||completed)){
return jqXHR.abort();
}
strAbort="abort";
completeDeferred.add(s.complete);
jqXHR.done(s.success);
jqXHR.fail(s.error);
transport=inspectPrefiltersOrTransports(transports, s, options, jqXHR);
if(!transport){
done(-1, "No Transport");
}else{
jqXHR.readyState=1;
if(fireGlobals){
globalEventContext.trigger("ajaxSend", [ jqXHR, s ]);
}
if(completed){
return jqXHR;
}
if(s.async&&s.timeout > 0){
timeoutTimer=window.setTimeout(function(){
jqXHR.abort("timeout");
}, s.timeout);
}
try {
completed=false;
transport.send(requestHeaders, done);
} catch(e){
if(completed){
throw e;
}
done(-1, e);
}}
function done(status, nativeStatusText, responses, headers){
var isSuccess, success, error, response, modified,
statusText=nativeStatusText;
if(completed){
return;
}
completed=true;
if(timeoutTimer){
window.clearTimeout(timeoutTimer);
}
transport=undefined;
responseHeadersString=headers||"";
jqXHR.readyState=status > 0 ? 4:0;
isSuccess=status >=200&&status < 300||status===304;
if(responses){
response=ajaxHandleResponses(s, jqXHR, responses);
}
response=ajaxConvert(s, response, jqXHR, isSuccess);
if(isSuccess){
if(s.ifModified){
modified=jqXHR.getResponseHeader("Last-Modified");
if(modified){
jQuery.lastModified[ cacheURL ]=modified;
}
modified=jqXHR.getResponseHeader("etag");
if(modified){
jQuery.etag[ cacheURL ]=modified;
}}
if(status===204||s.type==="HEAD"){
statusText="nocontent";
}else if(status===304){
statusText="notmodified";
}else{
statusText=response.state;
success=response.data;
error=response.error;
isSuccess = !error;
}}else{
error=statusText;
if(status||!statusText){
statusText="error";
if(status < 0){
status=0;
}}
}
jqXHR.status=status;
jqXHR.statusText=(nativeStatusText||statusText) + "";
if(isSuccess){
deferred.resolveWith(callbackContext, [ success, statusText, jqXHR ]);
}else{
deferred.rejectWith(callbackContext, [ jqXHR, statusText, error ]);
}
jqXHR.statusCode(statusCode);
statusCode=undefined;
if(fireGlobals){
globalEventContext.trigger(isSuccess ? "ajaxSuccess":"ajaxError",
[ jqXHR, s, isSuccess ? success:error ]);
}
completeDeferred.fireWith(callbackContext, [ jqXHR, statusText ]);
if(fireGlobals){
globalEventContext.trigger("ajaxComplete", [ jqXHR, s ]);
if(!(--jQuery.active)){
jQuery.event.trigger("ajaxStop");
}}
}
return jqXHR;
},
getJSON: function(url, data, callback){
return jQuery.get(url, data, callback, "json");
},
getScript: function(url, callback){
return jQuery.get(url, undefined, callback, "script");
}});
jQuery.each([ "get", "post" ], function(i, method){
jQuery[ method ]=function(url, data, callback, type){
if(isFunction(data)){
type=type||callback;
callback=data;
data=undefined;
}
return jQuery.ajax(jQuery.extend({
url: url,
type: method,
dataType: type,
data: data,
success: callback
}, jQuery.isPlainObject(url)&&url));
};});
jQuery._evalUrl=function(url){
return jQuery.ajax({
url: url,
type: "GET",
dataType: "script",
cache: true,
async: false,
global: false,
"throws": true
});
};
jQuery.fn.extend({
wrapAll: function(html){
var wrap;
if(this[ 0 ]){
if(isFunction(html)){
html=html.call(this[ 0 ]);
}
wrap=jQuery(html, this[ 0 ].ownerDocument).eq(0).clone(true);
if(this[ 0 ].parentNode){
wrap.insertBefore(this[ 0 ]);
}
wrap.map(function(){
var elem=this;
while(elem.firstElementChild){
elem=elem.firstElementChild;
}
return elem;
}).append(this);
}
return this;
},
wrapInner: function(html){
if(isFunction(html)){
return this.each(function(i){
jQuery(this).wrapInner(html.call(this, i));
});
}
return this.each(function(){
var self=jQuery(this),
contents=self.contents();
if(contents.length){
contents.wrapAll(html);
}else{
self.append(html);
}});
},
wrap: function(html){
var htmlIsFunction=isFunction(html);
return this.each(function(i){
jQuery(this).wrapAll(htmlIsFunction ? html.call(this, i):html);
});
},
unwrap: function(selector){
this.parent(selector).not("body").each(function(){
jQuery(this).replaceWith(this.childNodes);
});
return this;
}});
jQuery.expr.pseudos.hidden=function(elem){
return !jQuery.expr.pseudos.visible(elem);
};
jQuery.expr.pseudos.visible=function(elem){
return !!(elem.offsetWidth||elem.offsetHeight||elem.getClientRects().length);
};
jQuery.ajaxSettings.xhr=function(){
try {
return new window.XMLHttpRequest();
} catch(e){}};
var xhrSuccessStatus={
0: 200,
1223: 204
},
xhrSupported=jQuery.ajaxSettings.xhr();
support.cors = !!xhrSupported&&("withCredentials" in xhrSupported);
support.ajax=xhrSupported = !!xhrSupported;
jQuery.ajaxTransport(function(options){
var callback, errorCallback;
if(support.cors||xhrSupported&&!options.crossDomain){
return {
send: function(headers, complete){
var i,
xhr=options.xhr();
xhr.open(options.type,
options.url,
options.async,
options.username,
options.password
);
if(options.xhrFields){
for(i in options.xhrFields){
xhr[ i ]=options.xhrFields[ i ];
}}
if(options.mimeType&&xhr.overrideMimeType){
xhr.overrideMimeType(options.mimeType);
}
if(!options.crossDomain&&!headers[ "X-Requested-With" ]){
headers[ "X-Requested-With" ]="XMLHttpRequest";
}
for(i in headers){
xhr.setRequestHeader(i, headers[ i ]);
}
callback=function(type){
return function(){
if(callback){
callback=errorCallback=xhr.onload =
xhr.onerror=xhr.onabort=xhr.ontimeout =
xhr.onreadystatechange=null;
if(type==="abort"){
xhr.abort();
}else if(type==="error"){
if(typeof xhr.status!=="number"){
complete(0, "error");
}else{
complete(
xhr.status,
xhr.statusText
);
}}else{
complete(
xhrSuccessStatus[ xhr.status ]||xhr.status,
xhr.statusText,
(xhr.responseType||"text")!=="text"  ||
typeof xhr.responseText!=="string" ?
{ binary: xhr.response } :
{ text: xhr.responseText },
xhr.getAllResponseHeaders()
);
}}
};};
xhr.onload=callback();
errorCallback=xhr.onerror=xhr.ontimeout=callback("error");
if(xhr.onabort!==undefined){
xhr.onabort=errorCallback;
}else{
xhr.onreadystatechange=function(){
if(xhr.readyState===4){
window.setTimeout(function(){
if(callback){
errorCallback();
}});
}};}
callback=callback("abort");
try {
xhr.send(options.hasContent&&options.data||null);
} catch(e){
if(callback){
throw e;
}}
},
abort: function(){
if(callback){
callback();
}}
};}});
jQuery.ajaxPrefilter(function(s){
if(s.crossDomain){
s.contents.script=false;
}});
jQuery.ajaxSetup({
accepts: {
script: "text/javascript, application/javascript, " +
"application/ecmascript, application/x-ecmascript"
},
contents: {
script: /\b(?:java|ecma)script\b/
},
converters: {
"text script": function(text){
jQuery.globalEval(text);
return text;
}}
});
jQuery.ajaxPrefilter("script", function(s){
if(s.cache===undefined){
s.cache=false;
}
if(s.crossDomain){
s.type="GET";
}});
jQuery.ajaxTransport("script", function(s){
if(s.crossDomain){
var script, callback;
return {
send: function(_, complete){
script=jQuery("<script>").prop({
charset: s.scriptCharset,
src: s.url
}).on("load error",
callback=function(evt){
script.remove();
callback=null;
if(evt){
complete(evt.type==="error" ? 404:200, evt.type);
}}
);
document.head.appendChild(script[ 0 ]);
},
abort: function(){
if(callback){
callback();
}}
};}});
var oldCallbacks=[],
rjsonp=/(=)\?(?=&|$)|\?\?/;
jQuery.ajaxSetup({
jsonp: "callback",
jsonpCallback: function(){
var callback=oldCallbacks.pop()||(jQuery.expando + "_" +(nonce++));
this[ callback ]=true;
return callback;
}});
jQuery.ajaxPrefilter("json jsonp", function(s, originalSettings, jqXHR){
var callbackName, overwritten, responseContainer,
jsonProp=s.jsonp!==false&&(rjsonp.test(s.url) ?
"url" :
typeof s.data==="string" &&
(s.contentType||"")
.indexOf("application/x-www-form-urlencoded")===0 &&
rjsonp.test(s.data)&&"data"
);
if(jsonProp||s.dataTypes[ 0 ]==="jsonp"){
callbackName=s.jsonpCallback=isFunction(s.jsonpCallback) ?
s.jsonpCallback() :
s.jsonpCallback;
if(jsonProp){
s[ jsonProp ]=s[ jsonProp ].replace(rjsonp, "$1" + callbackName);
}else if(s.jsonp!==false){
s.url +=(rquery.test(s.url) ? "&":"?") + s.jsonp + "=" + callbackName;
}
s.converters[ "script json" ]=function(){
if(!responseContainer){
jQuery.error(callbackName + " was not called");
}
return responseContainer[ 0 ];
};
s.dataTypes[ 0 ]="json";
overwritten=window[ callbackName ];
window[ callbackName ]=function(){
responseContainer=arguments;
};
jqXHR.always(function(){
if(overwritten===undefined){
jQuery(window).removeProp(callbackName);
}else{
window[ callbackName ]=overwritten;
}
if(s[ callbackName ]){
s.jsonpCallback=originalSettings.jsonpCallback;
oldCallbacks.push(callbackName);
}
if(responseContainer&&isFunction(overwritten)){
overwritten(responseContainer[ 0 ]);
}
responseContainer=overwritten=undefined;
});
return "script";
}});
support.createHTMLDocument=(function(){
var body=document.implementation.createHTMLDocument("").body;
body.innerHTML="<form></form><form></form>";
return body.childNodes.length===2;
})();
jQuery.parseHTML=function(data, context, keepScripts){
if(typeof data!=="string"){
return [];
}
if(typeof context==="boolean"){
keepScripts=context;
context=false;
}
var base, parsed, scripts;
if(!context){
if(support.createHTMLDocument){
context=document.implementation.createHTMLDocument("");
base=context.createElement("base");
base.href=document.location.href;
context.head.appendChild(base);
}else{
context=document;
}}
parsed=rsingleTag.exec(data);
scripts = !keepScripts&&[];
if(parsed){
return [ context.createElement(parsed[ 1 ]) ];
}
parsed=buildFragment([ data ], context, scripts);
if(scripts&&scripts.length){
jQuery(scripts).remove();
}
return jQuery.merge([], parsed.childNodes);
};
jQuery.fn.load=function(url, params, callback){
var selector, type, response,
self=this,
off=url.indexOf(" ");
if(off > -1){
selector=stripAndCollapse(url.slice(off));
url=url.slice(0, off);
}
if(isFunction(params)){
callback=params;
params=undefined;
}else if(params&&typeof params==="object"){
type="POST";
}
if(self.length > 0){
jQuery.ajax({
url: url,
type: type||"GET",
dataType: "html",
data: params
}).done(function(responseText){
response=arguments;
self.html(selector ?
jQuery("<div>").append(jQuery.parseHTML(responseText)).find(selector) :
responseText);
}).always(callback&&function(jqXHR, status){
self.each(function(){
callback.apply(this, response||[ jqXHR.responseText, status, jqXHR ]);
});
});
}
return this;
};
jQuery.each([
"ajaxStart",
"ajaxStop",
"ajaxComplete",
"ajaxError",
"ajaxSuccess",
"ajaxSend"
], function(i, type){
jQuery.fn[ type ]=function(fn){
return this.on(type, fn);
};});
jQuery.expr.pseudos.animated=function(elem){
return jQuery.grep(jQuery.timers, function(fn){
return elem===fn.elem;
}).length;
};
jQuery.offset={
setOffset: function(elem, options, i){
var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
position=jQuery.css(elem, "position"),
curElem=jQuery(elem),
props={};
if(position==="static"){
elem.style.position="relative";
}
curOffset=curElem.offset();
curCSSTop=jQuery.css(elem, "top");
curCSSLeft=jQuery.css(elem, "left");
calculatePosition=(position==="absolute"||position==="fixed") &&
(curCSSTop + curCSSLeft).indexOf("auto") > -1;
if(calculatePosition){
curPosition=curElem.position();
curTop=curPosition.top;
curLeft=curPosition.left;
}else{
curTop=parseFloat(curCSSTop)||0;
curLeft=parseFloat(curCSSLeft)||0;
}
if(isFunction(options)){
options=options.call(elem, i, jQuery.extend({}, curOffset));
}
if(options.top!=null){
props.top=(options.top - curOffset.top) + curTop;
}
if(options.left!=null){
props.left=(options.left - curOffset.left) + curLeft;
}
if("using" in options){
options.using.call(elem, props);
}else{
curElem.css(props);
}}
};
jQuery.fn.extend({
offset: function(options){
if(arguments.length){
return options===undefined ?
this :
this.each(function(i){
jQuery.offset.setOffset(this, options, i);
});
}
var rect, win,
elem=this[ 0 ];
if(!elem){
return;
}
if(!elem.getClientRects().length){
return { top: 0, left: 0 };}
rect=elem.getBoundingClientRect();
win=elem.ownerDocument.defaultView;
return {
top: rect.top + win.pageYOffset,
left: rect.left + win.pageXOffset
};},
position: function(){
if(!this[ 0 ]){
return;
}
var offsetParent, offset, doc,
elem=this[ 0 ],
parentOffset={ top: 0, left: 0 };
if(jQuery.css(elem, "position")==="fixed"){
offset=elem.getBoundingClientRect();
}else{
offset=this.offset();
doc=elem.ownerDocument;
offsetParent=elem.offsetParent||doc.documentElement;
while(offsetParent &&
(offsetParent===doc.body||offsetParent===doc.documentElement) &&
jQuery.css(offsetParent, "position")==="static"){
offsetParent=offsetParent.parentNode;
}
if(offsetParent&&offsetParent!==elem&&offsetParent.nodeType===1){
parentOffset=jQuery(offsetParent).offset();
parentOffset.top +=jQuery.css(offsetParent, "borderTopWidth", true);
parentOffset.left +=jQuery.css(offsetParent, "borderLeftWidth", true);
}}
return {
top: offset.top - parentOffset.top - jQuery.css(elem, "marginTop", true),
left: offset.left - parentOffset.left - jQuery.css(elem, "marginLeft", true)
};},
offsetParent: function(){
return this.map(function(){
var offsetParent=this.offsetParent;
while(offsetParent&&jQuery.css(offsetParent, "position")==="static"){
offsetParent=offsetParent.offsetParent;
}
return offsetParent||documentElement;
});
}});
jQuery.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function(method, prop){
var top="pageYOffset"===prop;
jQuery.fn[ method ]=function(val){
return access(this, function(elem, method, val){
var win;
if(isWindow(elem)){
win=elem;
}else if(elem.nodeType===9){
win=elem.defaultView;
}
if(val===undefined){
return win ? win[ prop ]:elem[ method ];
}
if(win){
win.scrollTo(!top ? val:win.pageXOffset,
top ? val:win.pageYOffset
);
}else{
elem[ method ]=val;
}}, method, val, arguments.length);
};});
jQuery.each([ "top", "left" ], function(i, prop){
jQuery.cssHooks[ prop ]=addGetHookIf(support.pixelPosition,
function(elem, computed){
if(computed){
computed=curCSS(elem, prop);
return rnumnonpx.test(computed) ?
jQuery(elem).position()[ prop ] + "px" :
computed;
}}
);
});
jQuery.each({ Height: "height", Width: "width" }, function(name, type){
jQuery.each({ padding: "inner" + name, content: type, "": "outer" + name },
function(defaultExtra, funcName){
jQuery.fn[ funcName ]=function(margin, value){
var chainable=arguments.length&&(defaultExtra||typeof margin!=="boolean"),
extra=defaultExtra||(margin===true||value===true ? "margin":"border");
return access(this, function(elem, type, value){
var doc;
if(isWindow(elem)){
return funcName.indexOf("outer")===0 ?
elem[ "inner" + name ] :
elem.document.documentElement[ "client" + name ];
}
if(elem.nodeType===9){
doc=elem.documentElement;
return Math.max(elem.body[ "scroll" + name ], doc[ "scroll" + name ],
elem.body[ "offset" + name ], doc[ "offset" + name ],
doc[ "client" + name ]
);
}
return value===undefined ?
jQuery.css(elem, type, extra) :
jQuery.style(elem, type, value, extra);
}, type, chainable ? margin:undefined, chainable);
};});
});
jQuery.each(( "blur focus focusin focusout resize scroll click dblclick " +
"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
"change select submit keydown keypress keyup contextmenu").split(" "),
function(i, name){
jQuery.fn[ name ]=function(data, fn){
return arguments.length > 0 ?
this.on(name, null, data, fn) :
this.trigger(name);
};});
jQuery.fn.extend({
hover: function(fnOver, fnOut){
return this.mouseenter(fnOver).mouseleave(fnOut||fnOver);
}});
jQuery.fn.extend({
bind: function(types, data, fn){
return this.on(types, null, data, fn);
},
unbind: function(types, fn){
return this.off(types, null, fn);
},
delegate: function(selector, types, data, fn){
return this.on(types, selector, data, fn);
},
undelegate: function(selector, types, fn){
return arguments.length===1 ?
this.off(selector, "**") :
this.off(types, selector||"**", fn);
}});
jQuery.proxy=function(fn, context){
var tmp, args, proxy;
if(typeof context==="string"){
tmp=fn[ context ];
context=fn;
fn=tmp;
}
if(!isFunction(fn)){
return undefined;
}
args=slice.call(arguments, 2);
proxy=function(){
return fn.apply(context||this, args.concat(slice.call(arguments)));
};
proxy.guid=fn.guid=fn.guid||jQuery.guid++;
return proxy;
};
jQuery.holdReady=function(hold){
if(hold){
jQuery.readyWait++;
}else{
jQuery.ready(true);
}};
jQuery.isArray=Array.isArray;
jQuery.parseJSON=JSON.parse;
jQuery.nodeName=nodeName;
jQuery.isFunction=isFunction;
jQuery.isWindow=isWindow;
jQuery.camelCase=camelCase;
jQuery.type=toType;
jQuery.now=Date.now;
jQuery.isNumeric=function(obj){
var type=jQuery.type(obj);
return(type==="number"||type==="string") &&
!isNaN(obj - parseFloat(obj));
};
if(typeof define==="function"&&define.amd){
define("jquery", [], function(){
return jQuery;
});
}
var
_jQuery=window.jQuery,
_$=window.$;
jQuery.noConflict=function(deep){
if(window.$===jQuery){
window.$=_$;
}
if(deep&&window.jQuery===jQuery){
window.jQuery=_jQuery;
}
return jQuery;
};
if(!noGlobal){
window.jQuery=window.$=jQuery;
}
return jQuery;
});
},{}],2:[function(require,module,exports){
(function(){
var DEBUG=true;
(function(undefined){
var window=this||(0, eval)('this'),
document=window['document'],
navigator=window['navigator'],
jQueryInstance=window["jQuery"],
JSON=window["JSON"];
(function(factory){
if(typeof define==='function'&&define['amd']){
define(['exports', 'require'], factory);
}else if(typeof exports==='object'&&typeof module==='object'){
factory(module['exports']||exports);
}else{
factory(window['ko']={});
}}(function(koExports, amdRequire){
var ko=typeof koExports!=='undefined' ? koExports:{};
ko.exportSymbol=function(koPath, object){
var tokens=koPath.split(".");
var target=ko;
for (var i=0; i < tokens.length - 1; i++)
target=target[tokens[i]];
target[tokens[tokens.length - 1]]=object;
};
ko.exportProperty=function(owner, publicName, object){
owner[publicName]=object;
};
ko.version="3.4.2";
ko.exportSymbol('version', ko.version);
ko.options={
'deferUpdates': false,
'useOnlyNativeEvents': false
};
ko.utils=(function (){
function objectForEach(obj, action){
for (var prop in obj){
if(obj.hasOwnProperty(prop)){
action(prop, obj[prop]);
}}
}
function extend(target, source){
if(source){
for(var prop in source){
if(source.hasOwnProperty(prop)){
target[prop]=source[prop];
}}
}
return target;
}
function setPrototypeOf(obj, proto){
obj.__proto__=proto;
return obj;
}
var canSetPrototype=({ __proto__: [] } instanceof Array);
var canUseSymbols = !DEBUG&&typeof Symbol==='function';
var knownEvents={}, knownEventTypesByEventName={};
var keyEventTypeName=(navigator&&/Firefox\/2/i.test(navigator.userAgent)) ? 'KeyboardEvent':'UIEvents';
knownEvents[keyEventTypeName]=['keyup', 'keydown', 'keypress'];
knownEvents['MouseEvents']=['click', 'dblclick', 'mousedown', 'mouseup', 'mousemove', 'mouseover', 'mouseout', 'mouseenter', 'mouseleave'];
objectForEach(knownEvents, function(eventType, knownEventsForType){
if(knownEventsForType.length){
for (var i=0, j=knownEventsForType.length; i < j; i++)
knownEventTypesByEventName[knownEventsForType[i]]=eventType;
}});
var eventsThatMustBeRegisteredUsingAttachEvent={ 'propertychange': true };
var ieVersion=document&&(function(){
var version=3, div=document.createElement('div'), iElems=div.getElementsByTagName('i');
while (
div.innerHTML='<!--[if gt IE ' + (++version) + ']><i></i><![endif]-->',
iElems[0]
){}
return version > 4 ? version:undefined;
}());
var isIe6=ieVersion===6,
isIe7=ieVersion===7;
function isClickOnCheckableElement(element, eventType){
if((ko.utils.tagNameLower(element)!=="input")||!element.type) return false;
if(eventType.toLowerCase()!="click") return false;
var inputType=element.type;
return (inputType=="checkbox")||(inputType=="radio");
}
var cssClassNameRegex=/\S+/g;
function toggleDomNodeCssClass(node, classNames, shouldHaveClass){
var addOrRemoveFn;
if(classNames){
if(typeof node.classList==='object'){
addOrRemoveFn=node.classList[shouldHaveClass ? 'add':'remove'];
ko.utils.arrayForEach(classNames.match(cssClassNameRegex), function(className){
addOrRemoveFn.call(node.classList, className);
});
}else if(typeof node.className['baseVal']==='string'){
toggleObjectClassPropertyString(node.className, 'baseVal', classNames, shouldHaveClass);
}else{
toggleObjectClassPropertyString(node, 'className', classNames, shouldHaveClass);
}}
}
function toggleObjectClassPropertyString(obj, prop, classNames, shouldHaveClass){
var currentClassNames=obj[prop].match(cssClassNameRegex)||[];
ko.utils.arrayForEach(classNames.match(cssClassNameRegex), function(className){
ko.utils.addOrRemoveItem(currentClassNames, className, shouldHaveClass);
});
obj[prop]=currentClassNames.join(" ");
}
return {
fieldsIncludedWithJsonPost: ['authenticity_token', /^__RequestVerificationToken(_.*)?$/],
arrayForEach: function (array, action){
for (var i=0, j=array.length; i < j; i++)
action(array[i], i);
},
arrayIndexOf: function (array, item){
if(typeof Array.prototype.indexOf=="function")
return Array.prototype.indexOf.call(array, item);
for (var i=0, j=array.length; i < j; i++)
if(array[i]===item)
return i;
return -1;
},
arrayFirst: function (array, predicate, predicateOwner){
for (var i=0, j=array.length; i < j; i++)
if(predicate.call(predicateOwner, array[i], i))
return array[i];
return null;
},
arrayRemoveItem: function (array, itemToRemove){
var index=ko.utils.arrayIndexOf(array, itemToRemove);
if(index > 0){
array.splice(index, 1);
}
else if(index===0){
array.shift();
}},
arrayGetDistinctValues: function (array){
array=array||[];
var result=[];
for (var i=0, j=array.length; i < j; i++){
if(ko.utils.arrayIndexOf(result, array[i]) < 0)
result.push(array[i]);
}
return result;
},
arrayMap: function (array, mapping){
array=array||[];
var result=[];
for (var i=0, j=array.length; i < j; i++)
result.push(mapping(array[i], i));
return result;
},
arrayFilter: function (array, predicate){
array=array||[];
var result=[];
for (var i=0, j=array.length; i < j; i++)
if(predicate(array[i], i))
result.push(array[i]);
return result;
},
arrayPushAll: function (array, valuesToPush){
if(valuesToPush instanceof Array)
array.push.apply(array, valuesToPush);
else
for (var i=0, j=valuesToPush.length; i < j; i++)
array.push(valuesToPush[i]);
return array;
},
addOrRemoveItem: function(array, value, included){
var existingEntryIndex=ko.utils.arrayIndexOf(ko.utils.peekObservable(array), value);
if(existingEntryIndex < 0){
if(included)
array.push(value);
}else{
if(!included)
array.splice(existingEntryIndex, 1);
}},
canSetPrototype: canSetPrototype,
extend: extend,
setPrototypeOf: setPrototypeOf,
setPrototypeOfOrExtend: canSetPrototype ? setPrototypeOf:extend,
objectForEach: objectForEach,
objectMap: function(source, mapping){
if(!source)
return source;
var target={};
for (var prop in source){
if(source.hasOwnProperty(prop)){
target[prop]=mapping(source[prop], prop, source);
}}
return target;
},
emptyDomNode: function (domNode){
while (domNode.firstChild){
ko.removeNode(domNode.firstChild);
}},
moveCleanedNodesToContainerElement: function(nodes){
var nodesArray=ko.utils.makeArray(nodes);
var templateDocument=(nodesArray[0]&&nodesArray[0].ownerDocument)||document;
var container=templateDocument.createElement('div');
for (var i=0, j=nodesArray.length; i < j; i++){
container.appendChild(ko.cleanNode(nodesArray[i]));
}
return container;
},
cloneNodes: function (nodesArray, shouldCleanNodes){
for (var i=0, j=nodesArray.length, newNodesArray=[]; i < j; i++){
var clonedNode=nodesArray[i].cloneNode(true);
newNodesArray.push(shouldCleanNodes ? ko.cleanNode(clonedNode):clonedNode);
}
return newNodesArray;
},
setDomNodeChildren: function (domNode, childNodes){
ko.utils.emptyDomNode(domNode);
if(childNodes){
for (var i=0, j=childNodes.length; i < j; i++)
domNode.appendChild(childNodes[i]);
}},
replaceDomNodes: function (nodeToReplaceOrNodeArray, newNodesArray){
var nodesToReplaceArray=nodeToReplaceOrNodeArray.nodeType ? [nodeToReplaceOrNodeArray]:nodeToReplaceOrNodeArray;
if(nodesToReplaceArray.length > 0){
var insertionPoint=nodesToReplaceArray[0];
var parent=insertionPoint.parentNode;
for (var i=0, j=newNodesArray.length; i < j; i++)
parent.insertBefore(newNodesArray[i], insertionPoint);
for (var i=0, j=nodesToReplaceArray.length; i < j; i++){
ko.removeNode(nodesToReplaceArray[i]);
}}
},
fixUpContinuousNodeArray: function(continuousNodeArray, parentNode){
if(continuousNodeArray.length){
parentNode=(parentNode.nodeType===8&&parentNode.parentNode)||parentNode;
while (continuousNodeArray.length&&continuousNodeArray[0].parentNode!==parentNode)
continuousNodeArray.splice(0, 1);
while (continuousNodeArray.length > 1&&continuousNodeArray[continuousNodeArray.length - 1].parentNode!==parentNode)
continuousNodeArray.length--;
if(continuousNodeArray.length > 1){
var current=continuousNodeArray[0], last=continuousNodeArray[continuousNodeArray.length - 1];
continuousNodeArray.length=0;
while (current!==last){
continuousNodeArray.push(current);
current=current.nextSibling;
}
continuousNodeArray.push(last);
}}
return continuousNodeArray;
},
setOptionNodeSelectionState: function (optionNode, isSelected){
if(ieVersion < 7)
optionNode.setAttribute("selected", isSelected);
else
optionNode.selected=isSelected;
},
stringTrim: function (string){
return string===null||string===undefined ? '' :
string.trim ?
string.trim() :
string.toString().replace(/^[\s\xa0]+|[\s\xa0]+$/g, '');
},
stringStartsWith: function (string, startsWith){
string=string||"";
if(startsWith.length > string.length)
return false;
return string.substring(0, startsWith.length)===startsWith;
},
domNodeIsContainedBy: function (node, containedByNode){
if(node===containedByNode)
return true;
if(node.nodeType===11)
return false;
if(containedByNode.contains)
return containedByNode.contains(node.nodeType===3 ? node.parentNode:node);
if(containedByNode.compareDocumentPosition)
return (containedByNode.compareDocumentPosition(node) & 16)==16;
while (node&&node!=containedByNode){
node=node.parentNode;
}
return !!node;
},
domNodeIsAttachedToDocument: function (node){
return ko.utils.domNodeIsContainedBy(node, node.ownerDocument.documentElement);
},
anyDomNodeIsAttachedToDocument: function(nodes){
return !!ko.utils.arrayFirst(nodes, ko.utils.domNodeIsAttachedToDocument);
},
tagNameLower: function(element){
return element&&element.tagName&&element.tagName.toLowerCase();
},
catchFunctionErrors: function (delegate){
return ko['onError'] ? function (){
try {
return delegate.apply(this, arguments);
} catch (e){
ko['onError']&&ko['onError'](e);
throw e;
}}:delegate;
},
setTimeout: function (handler, timeout){
return setTimeout(ko.utils.catchFunctionErrors(handler), timeout);
},
deferError: function (error){
setTimeout(function (){
ko['onError']&&ko['onError'](error);
throw error;
}, 0);
},
registerEventHandler: function (element, eventType, handler){
var wrappedHandler=ko.utils.catchFunctionErrors(handler);
var mustUseAttachEvent=ieVersion&&eventsThatMustBeRegisteredUsingAttachEvent[eventType];
if(!ko.options['useOnlyNativeEvents']&&!mustUseAttachEvent&&jQueryInstance){
jQueryInstance(element)['bind'](eventType, wrappedHandler);
}else if(!mustUseAttachEvent&&typeof element.addEventListener=="function")
element.addEventListener(eventType, wrappedHandler, false);
else if(typeof element.attachEvent!="undefined"){
var attachEventHandler=function (event){ wrappedHandler.call(element, event); },
attachEventName="on" + eventType;
element.attachEvent(attachEventName, attachEventHandler);
ko.utils.domNodeDisposal.addDisposeCallback(element, function(){
element.detachEvent(attachEventName, attachEventHandler);
});
} else
throw new Error("Browser doesn't support addEventListener or attachEvent");
},
triggerEvent: function (element, eventType){
if(!(element&&element.nodeType))
throw new Error("element must be a DOM node when calling triggerEvent");
var useClickWorkaround=isClickOnCheckableElement(element, eventType);
if(!ko.options['useOnlyNativeEvents']&&jQueryInstance&&!useClickWorkaround){
jQueryInstance(element)['trigger'](eventType);
}else if(typeof document.createEvent=="function"){
if(typeof element.dispatchEvent=="function"){
var eventCategory=knownEventTypesByEventName[eventType]||"HTMLEvents";
var event=document.createEvent(eventCategory);
event.initEvent(eventType, true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, element);
element.dispatchEvent(event);
}
else
throw new Error("The supplied element doesn't support dispatchEvent");
}else if(useClickWorkaround&&element.click){
element.click();
}else if(typeof element.fireEvent!="undefined"){
element.fireEvent("on" + eventType);
}else{
throw new Error("Browser doesn't support triggering events");
}},
unwrapObservable: function (value){
return ko.isObservable(value) ? value():value;
},
peekObservable: function (value){
return ko.isObservable(value) ? value.peek():value;
},
toggleDomNodeCssClass: toggleDomNodeCssClass,
setTextContent: function(element, textContent){
var value=ko.utils.unwrapObservable(textContent);
if((value===null)||(value===undefined))
value="";
var innerTextNode=ko.virtualElements.firstChild(element);
if(!innerTextNode||innerTextNode.nodeType!=3||ko.virtualElements.nextSibling(innerTextNode)){
ko.virtualElements.setDomNodeChildren(element, [element.ownerDocument.createTextNode(value)]);
}else{
innerTextNode.data=value;
}
ko.utils.forceRefresh(element);
},
setElementName: function(element, name){
element.name=name;
if(ieVersion <=7){
try {
element.mergeAttributes(document.createElement("<input name='" + element.name + "'/>"), false);
}
catch(e){}}
},
forceRefresh: function(node){
if(ieVersion >=9){
var elem=node.nodeType==1 ? node:node.parentNode;
if(elem.style)
elem.style.zoom=elem.style.zoom;
}},
ensureSelectElementIsRenderedCorrectly: function(selectElement){
if(ieVersion){
var originalWidth=selectElement.style.width;
selectElement.style.width=0;
selectElement.style.width=originalWidth;
}},
range: function (min, max){
min=ko.utils.unwrapObservable(min);
max=ko.utils.unwrapObservable(max);
var result=[];
for (var i=min; i <=max; i++)
result.push(i);
return result;
},
makeArray: function(arrayLikeObject){
var result=[];
for (var i=0, j=arrayLikeObject.length; i < j; i++){
result.push(arrayLikeObject[i]);
};
return result;
},
createSymbolOrString: function(identifier){
return canUseSymbols ? Symbol(identifier):identifier;
},
isIe6:isIe6,
isIe7:isIe7,
ieVersion:ieVersion,
getFormFields: function(form, fieldName){
var fields=ko.utils.makeArray(form.getElementsByTagName("input")).concat(ko.utils.makeArray(form.getElementsByTagName("textarea")));
var isMatchingField=(typeof fieldName=='string')
? function(field){ return field.name===fieldName }
: function(field){ return fieldName.test(field.name) };
var matches=[];
for (var i=fields.length - 1; i >=0; i--){
if(isMatchingField(fields[i]))
matches.push(fields[i]);
};
return matches;
},
parseJson: function (jsonString){
if(typeof jsonString=="string"){
jsonString=ko.utils.stringTrim(jsonString);
if(jsonString){
if(JSON&&JSON.parse)
return JSON.parse(jsonString);
return (new Function("return " + jsonString))();
}}
return null;
},
stringifyJson: function (data, replacer, space){
if(!JSON||!JSON.stringify)
throw new Error("Cannot find JSON.stringify(). Some browsers (e.g., IE < 8) don't support it natively, but you can overcome this by adding a script reference to json2.js, downloadable from http://www.json.org/json2.js");
return JSON.stringify(ko.utils.unwrapObservable(data), replacer, space);
},
postJson: function (urlOrForm, data, options){
options=options||{};
var params=options['params']||{};
var includeFields=options['includeFields']||this.fieldsIncludedWithJsonPost;
var url=urlOrForm;
if((typeof urlOrForm=='object')&&(ko.utils.tagNameLower(urlOrForm)==="form")){
var originalForm=urlOrForm;
url=originalForm.action;
for (var i=includeFields.length - 1; i >=0; i--){
var fields=ko.utils.getFormFields(originalForm, includeFields[i]);
for (var j=fields.length - 1; j >=0; j--)
params[fields[j].name]=fields[j].value;
}}
data=ko.utils.unwrapObservable(data);
var form=document.createElement("form");
form.style.display="none";
form.action=url;
form.method="post";
for (var key in data){
var input=document.createElement("input");
input.type="hidden";
input.name=key;
input.value=ko.utils.stringifyJson(ko.utils.unwrapObservable(data[key]));
form.appendChild(input);
}
objectForEach(params, function(key, value){
var input=document.createElement("input");
input.type="hidden";
input.name=key;
input.value=value;
form.appendChild(input);
});
document.body.appendChild(form);
options['submitter'] ? options['submitter'](form):form.submit();
setTimeout(function (){ form.parentNode.removeChild(form); }, 0);
}}
}());
ko.exportSymbol('utils', ko.utils);
ko.exportSymbol('utils.arrayForEach', ko.utils.arrayForEach);
ko.exportSymbol('utils.arrayFirst', ko.utils.arrayFirst);
ko.exportSymbol('utils.arrayFilter', ko.utils.arrayFilter);
ko.exportSymbol('utils.arrayGetDistinctValues', ko.utils.arrayGetDistinctValues);
ko.exportSymbol('utils.arrayIndexOf', ko.utils.arrayIndexOf);
ko.exportSymbol('utils.arrayMap', ko.utils.arrayMap);
ko.exportSymbol('utils.arrayPushAll', ko.utils.arrayPushAll);
ko.exportSymbol('utils.arrayRemoveItem', ko.utils.arrayRemoveItem);
ko.exportSymbol('utils.extend', ko.utils.extend);
ko.exportSymbol('utils.fieldsIncludedWithJsonPost', ko.utils.fieldsIncludedWithJsonPost);
ko.exportSymbol('utils.getFormFields', ko.utils.getFormFields);
ko.exportSymbol('utils.peekObservable', ko.utils.peekObservable);
ko.exportSymbol('utils.postJson', ko.utils.postJson);
ko.exportSymbol('utils.parseJson', ko.utils.parseJson);
ko.exportSymbol('utils.registerEventHandler', ko.utils.registerEventHandler);
ko.exportSymbol('utils.stringifyJson', ko.utils.stringifyJson);
ko.exportSymbol('utils.range', ko.utils.range);
ko.exportSymbol('utils.toggleDomNodeCssClass', ko.utils.toggleDomNodeCssClass);
ko.exportSymbol('utils.triggerEvent', ko.utils.triggerEvent);
ko.exportSymbol('utils.unwrapObservable', ko.utils.unwrapObservable);
ko.exportSymbol('utils.objectForEach', ko.utils.objectForEach);
ko.exportSymbol('utils.addOrRemoveItem', ko.utils.addOrRemoveItem);
ko.exportSymbol('utils.setTextContent', ko.utils.setTextContent);
ko.exportSymbol('unwrap', ko.utils.unwrapObservable);
if(!Function.prototype['bind']){
Function.prototype['bind']=function (object){
var originalFunction=this;
if(arguments.length===1){
return function (){
return originalFunction.apply(object, arguments);
};}else{
var partialArgs=Array.prototype.slice.call(arguments, 1);
return function (){
var args=partialArgs.slice(0);
args.push.apply(args, arguments);
return originalFunction.apply(object, args);
};}};}
ko.utils.domData=new (function (){
var uniqueId=0;
var dataStoreKeyExpandoPropertyName="__ko__" + (new Date).getTime();
var dataStore={};
function getAll(node, createIfNotFound){
var dataStoreKey=node[dataStoreKeyExpandoPropertyName];
var hasExistingDataStore=dataStoreKey&&(dataStoreKey!=="null")&&dataStore[dataStoreKey];
if(!hasExistingDataStore){
if(!createIfNotFound)
return undefined;
dataStoreKey=node[dataStoreKeyExpandoPropertyName]="ko" + uniqueId++;
dataStore[dataStoreKey]={};}
return dataStore[dataStoreKey];
}
return {
get: function (node, key){
var allDataForNode=getAll(node, false);
return allDataForNode===undefined ? undefined:allDataForNode[key];
},
set: function (node, key, value){
if(value===undefined){
if(getAll(node, false)===undefined)
return;
}
var allDataForNode=getAll(node, true);
allDataForNode[key]=value;
},
clear: function (node){
var dataStoreKey=node[dataStoreKeyExpandoPropertyName];
if(dataStoreKey){
delete dataStore[dataStoreKey];
node[dataStoreKeyExpandoPropertyName]=null;
return true;
}
return false;
},
nextKey: function (){
return (uniqueId++) + dataStoreKeyExpandoPropertyName;
}};})();
ko.exportSymbol('utils.domData', ko.utils.domData);
ko.exportSymbol('utils.domData.clear', ko.utils.domData.clear);
ko.utils.domNodeDisposal=new (function (){
var domDataKey=ko.utils.domData.nextKey();
var cleanableNodeTypes={ 1: true, 8: true, 9: true };
var cleanableNodeTypesWithDescendants={ 1: true, 9: true };
function getDisposeCallbacksCollection(node, createIfNotFound){
var allDisposeCallbacks=ko.utils.domData.get(node, domDataKey);
if((allDisposeCallbacks===undefined)&&createIfNotFound){
allDisposeCallbacks=[];
ko.utils.domData.set(node, domDataKey, allDisposeCallbacks);
}
return allDisposeCallbacks;
}
function destroyCallbacksCollection(node){
ko.utils.domData.set(node, domDataKey, undefined);
}
function cleanSingleNode(node){
var callbacks=getDisposeCallbacksCollection(node, false);
if(callbacks){
callbacks=callbacks.slice(0);
for (var i=0; i < callbacks.length; i++)
callbacks[i](node);
}
ko.utils.domData.clear(node);
ko.utils.domNodeDisposal["cleanExternalData"](node);
if(cleanableNodeTypesWithDescendants[node.nodeType])
cleanImmediateCommentTypeChildren(node);
}
function cleanImmediateCommentTypeChildren(nodeWithChildren){
var child, nextChild=nodeWithChildren.firstChild;
while (child=nextChild){
nextChild=child.nextSibling;
if(child.nodeType===8)
cleanSingleNode(child);
}}
return {
addDisposeCallback:function(node, callback){
if(typeof callback!="function")
throw new Error("Callback must be a function");
getDisposeCallbacksCollection(node, true).push(callback);
},
removeDisposeCallback:function(node, callback){
var callbacksCollection=getDisposeCallbacksCollection(node, false);
if(callbacksCollection){
ko.utils.arrayRemoveItem(callbacksCollection, callback);
if(callbacksCollection.length==0)
destroyCallbacksCollection(node);
}},
cleanNode:function(node){
if(cleanableNodeTypes[node.nodeType]){
cleanSingleNode(node);
if(cleanableNodeTypesWithDescendants[node.nodeType]){
var descendants=[];
ko.utils.arrayPushAll(descendants, node.getElementsByTagName("*"));
for (var i=0, j=descendants.length; i < j; i++)
cleanSingleNode(descendants[i]);
}}
return node;
},
removeNode:function(node){
ko.cleanNode(node);
if(node.parentNode)
node.parentNode.removeChild(node);
},
"cleanExternalData":function (node){
if(jQueryInstance&&(typeof jQueryInstance['cleanData']=="function"))
jQueryInstance['cleanData']([node]);
}};})();
ko.cleanNode=ko.utils.domNodeDisposal.cleanNode;
ko.removeNode=ko.utils.domNodeDisposal.removeNode;
ko.exportSymbol('cleanNode', ko.cleanNode);
ko.exportSymbol('removeNode', ko.removeNode);
ko.exportSymbol('utils.domNodeDisposal', ko.utils.domNodeDisposal);
ko.exportSymbol('utils.domNodeDisposal.addDisposeCallback', ko.utils.domNodeDisposal.addDisposeCallback);
ko.exportSymbol('utils.domNodeDisposal.removeDisposeCallback', ko.utils.domNodeDisposal.removeDisposeCallback);
(function (){
var none=[0, "", ""],
table=[1, "<table>", "</table>"],
tbody=[2, "<table><tbody>", "</tbody></table>"],
tr=[3, "<table><tbody><tr>", "</tr></tbody></table>"],
select=[1, "<select multiple='multiple'>", "</select>"],
lookup={
'thead': table,
'tbody': table,
'tfoot': table,
'tr': tbody,
'td': tr,
'th': tr,
'option': select,
'optgroup': select
},
mayRequireCreateElementHack=ko.utils.ieVersion <=8;
function getWrap(tags){
var m=tags.match(/^<([a-z]+)[ >]/);
return (m&&lookup[m[1]])||none;
}
function simpleHtmlParse(html, documentContext){
documentContext||(documentContext=document);
var windowContext=documentContext['parentWindow']||documentContext['defaultView']||window;
var tags=ko.utils.stringTrim(html).toLowerCase(), div=documentContext.createElement("div"),
wrap=getWrap(tags),
depth=wrap[0];
var markup="ignored<div>" + wrap[1] + html + wrap[2] + "</div>";
if(typeof windowContext['innerShiv']=="function"){
div.appendChild(windowContext['innerShiv'](markup));
}else{
if(mayRequireCreateElementHack){
documentContext.appendChild(div);
}
div.innerHTML=markup;
if(mayRequireCreateElementHack){
div.parentNode.removeChild(div);
}}
while (depth--)
div=div.lastChild;
return ko.utils.makeArray(div.lastChild.childNodes);
}
function jQueryHtmlParse(html, documentContext){
if(jQueryInstance['parseHTML']){
return jQueryInstance['parseHTML'](html, documentContext)||[];
}else{
var elems=jQueryInstance['clean']([html], documentContext);
if(elems&&elems[0]){
var elem=elems[0];
while (elem.parentNode&&elem.parentNode.nodeType!==11 )
elem=elem.parentNode;
if(elem.parentNode)
elem.parentNode.removeChild(elem);
}
return elems;
}}
ko.utils.parseHtmlFragment=function(html, documentContext){
return jQueryInstance ?
jQueryHtmlParse(html, documentContext) :
simpleHtmlParse(html, documentContext);
};
ko.utils.setHtml=function(node, html){
ko.utils.emptyDomNode(node);
html=ko.utils.unwrapObservable(html);
if((html!==null)&&(html!==undefined)){
if(typeof html!='string')
html=html.toString();
if(jQueryInstance){
jQueryInstance(node)['html'](html);
}else{
var parsedNodes=ko.utils.parseHtmlFragment(html, node.ownerDocument);
for (var i=0; i < parsedNodes.length; i++)
node.appendChild(parsedNodes[i]);
}}
};})();
ko.exportSymbol('utils.parseHtmlFragment', ko.utils.parseHtmlFragment);
ko.exportSymbol('utils.setHtml', ko.utils.setHtml);
ko.memoization=(function (){
var memos={};
function randomMax8HexChars(){
return (((1 + Math.random()) * 0x100000000) | 0).toString(16).substring(1);
}
function generateRandomId(){
return randomMax8HexChars() + randomMax8HexChars();
}
function findMemoNodes(rootNode, appendToArray){
if(!rootNode)
return;
if(rootNode.nodeType==8){
var memoId=ko.memoization.parseMemoText(rootNode.nodeValue);
if(memoId!=null)
appendToArray.push({ domNode: rootNode, memoId: memoId });
}else if(rootNode.nodeType==1){
for (var i=0, childNodes=rootNode.childNodes, j=childNodes.length; i < j; i++)
findMemoNodes(childNodes[i], appendToArray);
}}
return {
memoize: function (callback){
if(typeof callback!="function")
throw new Error("You can only pass a function to ko.memoization.memoize()");
var memoId=generateRandomId();
memos[memoId]=callback;
return "<!--[ko_memo:" + memoId + "]-->";
},
unmemoize: function (memoId, callbackParams){
var callback=memos[memoId];
if(callback===undefined)
throw new Error("Couldn't find any memo with ID " + memoId + ". Perhaps it's already been unmemoized.");
try {
callback.apply(null, callbackParams||[]);
return true;
}
finally { delete memos[memoId]; }},
unmemoizeDomNodeAndDescendants: function (domNode, extraCallbackParamsArray){
var memos=[];
findMemoNodes(domNode, memos);
for (var i=0, j=memos.length; i < j; i++){
var node=memos[i].domNode;
var combinedParams=[node];
if(extraCallbackParamsArray)
ko.utils.arrayPushAll(combinedParams, extraCallbackParamsArray);
ko.memoization.unmemoize(memos[i].memoId, combinedParams);
node.nodeValue="";
if(node.parentNode)
node.parentNode.removeChild(node);
}},
parseMemoText: function (memoText){
var match=memoText.match(/^\[ko_memo\:(.*?)\]$/);
return match ? match[1]:null;
}};})();
ko.exportSymbol('memoization', ko.memoization);
ko.exportSymbol('memoization.memoize', ko.memoization.memoize);
ko.exportSymbol('memoization.unmemoize', ko.memoization.unmemoize);
ko.exportSymbol('memoization.parseMemoText', ko.memoization.parseMemoText);
ko.exportSymbol('memoization.unmemoizeDomNodeAndDescendants', ko.memoization.unmemoizeDomNodeAndDescendants);
ko.tasks=(function (){
var scheduler,
taskQueue=[],
taskQueueLength=0,
nextHandle=1,
nextIndexToProcess=0;
if(window['MutationObserver']){
scheduler=(function (callback){
var div=document.createElement("div");
new MutationObserver(callback).observe(div, {attributes: true});
return function (){ div.classList.toggle("foo"); };})(scheduledProcess);
}else if(document&&"onreadystatechange" in document.createElement("script")){
scheduler=function (callback){
var script=document.createElement("script");
script.onreadystatechange=function (){
script.onreadystatechange=null;
document.documentElement.removeChild(script);
script=null;
callback();
};
document.documentElement.appendChild(script);
};}else{
scheduler=function (callback){
setTimeout(callback, 0);
};}
function processTasks(){
if(taskQueueLength){
var mark=taskQueueLength, countMarks=0;
for (var task; nextIndexToProcess < taskQueueLength;){
if(task=taskQueue[nextIndexToProcess++]){
if(nextIndexToProcess > mark){
if(++countMarks >=5000){
nextIndexToProcess=taskQueueLength;
ko.utils.deferError(Error("'Too much recursion' after processing " + countMarks + " task groups."));
break;
}
mark=taskQueueLength;
}
try {
task();
} catch (ex){
ko.utils.deferError(ex);
}}
}}
}
function scheduledProcess(){
processTasks();
nextIndexToProcess=taskQueueLength=taskQueue.length=0;
}
function scheduleTaskProcessing(){
ko.tasks['scheduler'](scheduledProcess);
}
var tasks={
'scheduler': scheduler,
schedule: function (func){
if(!taskQueueLength){
scheduleTaskProcessing();
}
taskQueue[taskQueueLength++]=func;
return nextHandle++;
},
cancel: function (handle){
var index=handle - (nextHandle - taskQueueLength);
if(index >=nextIndexToProcess&&index < taskQueueLength){
taskQueue[index]=null;
}},
'resetForTesting': function (){
var length=taskQueueLength - nextIndexToProcess;
nextIndexToProcess=taskQueueLength=taskQueue.length=0;
return length;
},
runEarly: processTasks
};
return tasks;
})();
ko.exportSymbol('tasks', ko.tasks);
ko.exportSymbol('tasks.schedule', ko.tasks.schedule);
ko.exportSymbol('tasks.runEarly', ko.tasks.runEarly);
ko.extenders={
'throttle': function(target, timeout){
target['throttleEvaluation']=timeout;
var writeTimeoutInstance=null;
return ko.dependentObservable({
'read': target,
'write': function(value){
clearTimeout(writeTimeoutInstance);
writeTimeoutInstance=ko.utils.setTimeout(function(){
target(value);
}, timeout);
}});
},
'rateLimit': function(target, options){
var timeout, method, limitFunction;
if(typeof options=='number'){
timeout=options;
}else{
timeout=options['timeout'];
method=options['method'];
}
target._deferUpdates=false;
limitFunction=method=='notifyWhenChangesStop' ?  debounce:throttle;
target.limit(function(callback){
return limitFunction(callback, timeout);
});
},
'deferred': function(target, options){
if(options!==true){
throw new Error('The \'deferred\' extender only accepts the value \'true\', because it is not supported to turn deferral off once enabled.')
}
if(!target._deferUpdates){
target._deferUpdates=true;
target.limit(function (callback){
var handle,
ignoreUpdates=false;
return function (){
if(!ignoreUpdates){
ko.tasks.cancel(handle);
handle=ko.tasks.schedule(callback);
try {
ignoreUpdates=true;
target['notifySubscribers'](undefined, 'dirty');
} finally {
ignoreUpdates=false;
}}
};});
}},
'notify': function(target, notifyWhen){
target["equalityComparer"]=notifyWhen=="always" ?
null :
valuesArePrimitiveAndEqual;
}};
var primitiveTypes={ 'undefined':1, 'boolean':1, 'number':1, 'string':1 };
function valuesArePrimitiveAndEqual(a, b){
var oldValueIsPrimitive=(a===null)||(typeof(a) in primitiveTypes);
return oldValueIsPrimitive ? (a===b):false;
}
function throttle(callback, timeout){
var timeoutInstance;
return function (){
if(!timeoutInstance){
timeoutInstance=ko.utils.setTimeout(function (){
timeoutInstance=undefined;
callback();
}, timeout);
}};}
function debounce(callback, timeout){
var timeoutInstance;
return function (){
clearTimeout(timeoutInstance);
timeoutInstance=ko.utils.setTimeout(callback, timeout);
};}
function applyExtenders(requestedExtenders){
var target=this;
if(requestedExtenders){
ko.utils.objectForEach(requestedExtenders, function(key, value){
var extenderHandler=ko.extenders[key];
if(typeof extenderHandler=='function'){
target=extenderHandler(target, value)||target;
}});
}
return target;
}
ko.exportSymbol('extenders', ko.extenders);
ko.subscription=function (target, callback, disposeCallback){
this._target=target;
this.callback=callback;
this.disposeCallback=disposeCallback;
this.isDisposed=false;
ko.exportProperty(this, 'dispose', this.dispose);
};
ko.subscription.prototype.dispose=function (){
this.isDisposed=true;
this.disposeCallback();
};
ko.subscribable=function (){
ko.utils.setPrototypeOfOrExtend(this, ko_subscribable_fn);
ko_subscribable_fn.init(this);
}
var defaultEvent="change";
function limitNotifySubscribers(value, event){
if(!event||event===defaultEvent){
this._limitChange(value);
}else if(event==='beforeChange'){
this._limitBeforeChange(value);
}else{
this._origNotifySubscribers(value, event);
}}
var ko_subscribable_fn={
init: function(instance){
instance._subscriptions={ "change": [] };
instance._versionNumber=1;
},
subscribe: function (callback, callbackTarget, event){
var self=this;
event=event||defaultEvent;
var boundCallback=callbackTarget ? callback.bind(callbackTarget):callback;
var subscription=new ko.subscription(self, boundCallback, function (){
ko.utils.arrayRemoveItem(self._subscriptions[event], subscription);
if(self.afterSubscriptionRemove)
self.afterSubscriptionRemove(event);
});
if(self.beforeSubscriptionAdd)
self.beforeSubscriptionAdd(event);
if(!self._subscriptions[event])
self._subscriptions[event]=[];
self._subscriptions[event].push(subscription);
return subscription;
},
"notifySubscribers": function (valueToNotify, event){
event=event||defaultEvent;
if(event===defaultEvent){
this.updateVersion();
}
if(this.hasSubscriptionsForEvent(event)){
var subs=event===defaultEvent&&this._changeSubscriptions||this._subscriptions[event].slice(0);
try {
ko.dependencyDetection.begin();
for (var i=0, subscription; subscription=subs[i]; ++i){
if(!subscription.isDisposed)
subscription.callback(valueToNotify);
}} finally {
ko.dependencyDetection.end();
}}
},
getVersion: function (){
return this._versionNumber;
},
hasChanged: function (versionToCheck){
return this.getVersion()!==versionToCheck;
},
updateVersion: function (){
++this._versionNumber;
},
limit: function(limitFunction){
var self=this, selfIsObservable=ko.isObservable(self),
ignoreBeforeChange, notifyNextChange, previousValue, pendingValue, beforeChange='beforeChange';
if(!self._origNotifySubscribers){
self._origNotifySubscribers=self["notifySubscribers"];
self["notifySubscribers"]=limitNotifySubscribers;
}
var finish=limitFunction(function(){
self._notificationIsPending=false;
if(selfIsObservable&&pendingValue===self){
pendingValue=self._evalIfChanged ? self._evalIfChanged():self();
}
var shouldNotify=notifyNextChange||self.isDifferent(previousValue, pendingValue);
notifyNextChange=ignoreBeforeChange=false;
if(shouldNotify){
self._origNotifySubscribers(previousValue=pendingValue);
}});
self._limitChange=function(value){
self._changeSubscriptions=self._subscriptions[defaultEvent].slice(0);
self._notificationIsPending=ignoreBeforeChange=true;
pendingValue=value;
finish();
};
self._limitBeforeChange=function(value){
if(!ignoreBeforeChange){
previousValue=value;
self._origNotifySubscribers(value, beforeChange);
}};
self._notifyNextChangeIfValueIsDifferent=function(){
if(self.isDifferent(previousValue, self.peek(true ))){
notifyNextChange=true;
}};},
hasSubscriptionsForEvent: function(event){
return this._subscriptions[event]&&this._subscriptions[event].length;
},
getSubscriptionsCount: function (event){
if(event){
return this._subscriptions[event]&&this._subscriptions[event].length||0;
}else{
var total=0;
ko.utils.objectForEach(this._subscriptions, function(eventName, subscriptions){
if(eventName!=='dirty')
total +=subscriptions.length;
});
return total;
}},
isDifferent: function(oldValue, newValue){
return !this['equalityComparer']||!this['equalityComparer'](oldValue, newValue);
},
extend: applyExtenders
};
ko.exportProperty(ko_subscribable_fn, 'subscribe', ko_subscribable_fn.subscribe);
ko.exportProperty(ko_subscribable_fn, 'extend', ko_subscribable_fn.extend);
ko.exportProperty(ko_subscribable_fn, 'getSubscriptionsCount', ko_subscribable_fn.getSubscriptionsCount);
if(ko.utils.canSetPrototype){
ko.utils.setPrototypeOf(ko_subscribable_fn, Function.prototype);
}
ko.subscribable['fn']=ko_subscribable_fn;
ko.isSubscribable=function (instance){
return instance!=null&&typeof instance.subscribe=="function"&&typeof instance["notifySubscribers"]=="function";
};
ko.exportSymbol('subscribable', ko.subscribable);
ko.exportSymbol('isSubscribable', ko.isSubscribable);
ko.computedContext=ko.dependencyDetection=(function (){
var outerFrames=[],
currentFrame,
lastId=0;
function getId(){
return ++lastId;
}
function begin(options){
outerFrames.push(currentFrame);
currentFrame=options;
}
function end(){
currentFrame=outerFrames.pop();
}
return {
begin: begin,
end: end,
registerDependency: function (subscribable){
if(currentFrame){
if(!ko.isSubscribable(subscribable))
throw new Error("Only subscribable things can act as dependencies");
currentFrame.callback.call(currentFrame.callbackTarget, subscribable, subscribable._id||(subscribable._id=getId()));
}},
ignore: function (callback, callbackTarget, callbackArgs){
try {
begin();
return callback.apply(callbackTarget, callbackArgs||[]);
} finally {
end();
}},
getDependenciesCount: function (){
if(currentFrame)
return currentFrame.computed.getDependenciesCount();
},
isInitial: function(){
if(currentFrame)
return currentFrame.isInitial;
}};})();
ko.exportSymbol('computedContext', ko.computedContext);
ko.exportSymbol('computedContext.getDependenciesCount', ko.computedContext.getDependenciesCount);
ko.exportSymbol('computedContext.isInitial', ko.computedContext.isInitial);
ko.exportSymbol('ignoreDependencies', ko.ignoreDependencies=ko.dependencyDetection.ignore);
var observableLatestValue=ko.utils.createSymbolOrString('_latestValue');
ko.observable=function (initialValue){
function observable(){
if(arguments.length > 0){
if(observable.isDifferent(observable[observableLatestValue], arguments[0])){
observable.valueWillMutate();
observable[observableLatestValue]=arguments[0];
observable.valueHasMutated();
}
return this;
}else{
ko.dependencyDetection.registerDependency(observable);
return observable[observableLatestValue];
}}
observable[observableLatestValue]=initialValue;
if(!ko.utils.canSetPrototype){
ko.utils.extend(observable, ko.subscribable['fn']);
}
ko.subscribable['fn'].init(observable);
ko.utils.setPrototypeOfOrExtend(observable, observableFn);
if(ko.options['deferUpdates']){
ko.extenders['deferred'](observable, true);
}
return observable;
}
var observableFn={
'equalityComparer': valuesArePrimitiveAndEqual,
peek: function(){ return this[observableLatestValue]; },
valueHasMutated: function (){ this['notifySubscribers'](this[observableLatestValue]); },
valueWillMutate: function (){ this['notifySubscribers'](this[observableLatestValue], 'beforeChange'); }};
if(ko.utils.canSetPrototype){
ko.utils.setPrototypeOf(observableFn, ko.subscribable['fn']);
}
var protoProperty=ko.observable.protoProperty='__ko_proto__';
observableFn[protoProperty]=ko.observable;
ko.hasPrototype=function(instance, prototype){
if((instance===null)||(instance===undefined)||(instance[protoProperty]===undefined)) return false;
if(instance[protoProperty]===prototype) return true;
return ko.hasPrototype(instance[protoProperty], prototype);
};
ko.isObservable=function (instance){
return ko.hasPrototype(instance, ko.observable);
}
ko.isWriteableObservable=function (instance){
if((typeof instance=='function')&&instance[protoProperty]===ko.observable)
return true;
if((typeof instance=='function')&&(instance[protoProperty]===ko.dependentObservable)&&(instance.hasWriteFunction))
return true;
return false;
}
ko.exportSymbol('observable', ko.observable);
ko.exportSymbol('isObservable', ko.isObservable);
ko.exportSymbol('isWriteableObservable', ko.isWriteableObservable);
ko.exportSymbol('isWritableObservable', ko.isWriteableObservable);
ko.exportSymbol('observable.fn', observableFn);
ko.exportProperty(observableFn, 'peek', observableFn.peek);
ko.exportProperty(observableFn, 'valueHasMutated', observableFn.valueHasMutated);
ko.exportProperty(observableFn, 'valueWillMutate', observableFn.valueWillMutate);
ko.observableArray=function (initialValues){
initialValues=initialValues||[];
if(typeof initialValues!='object'||!('length' in initialValues))
throw new Error("The argument passed when initializing an observable array must be an array, or null, or undefined.");
var result=ko.observable(initialValues);
ko.utils.setPrototypeOfOrExtend(result, ko.observableArray['fn']);
return result.extend({'trackArrayChanges':true});
};
ko.observableArray['fn']={
'remove': function (valueOrPredicate){
var underlyingArray=this.peek();
var removedValues=[];
var predicate=typeof valueOrPredicate=="function"&&!ko.isObservable(valueOrPredicate) ? valueOrPredicate:function (value){ return value===valueOrPredicate; };
for (var i=0; i < underlyingArray.length; i++){
var value=underlyingArray[i];
if(predicate(value)){
if(removedValues.length===0){
this.valueWillMutate();
}
removedValues.push(value);
underlyingArray.splice(i, 1);
i--;
}}
if(removedValues.length){
this.valueHasMutated();
}
return removedValues;
},
'removeAll': function (arrayOfValues){
if(arrayOfValues===undefined){
var underlyingArray=this.peek();
var allValues=underlyingArray.slice(0);
this.valueWillMutate();
underlyingArray.splice(0, underlyingArray.length);
this.valueHasMutated();
return allValues;
}
if(!arrayOfValues)
return [];
return this['remove'](function (value){
return ko.utils.arrayIndexOf(arrayOfValues, value) >=0;
});
},
'destroy': function (valueOrPredicate){
var underlyingArray=this.peek();
var predicate=typeof valueOrPredicate=="function"&&!ko.isObservable(valueOrPredicate) ? valueOrPredicate:function (value){ return value===valueOrPredicate; };
this.valueWillMutate();
for (var i=underlyingArray.length - 1; i >=0; i--){
var value=underlyingArray[i];
if(predicate(value))
underlyingArray[i]["_destroy"]=true;
}
this.valueHasMutated();
},
'destroyAll': function (arrayOfValues){
if(arrayOfValues===undefined)
return this['destroy'](function(){ return true });
if(!arrayOfValues)
return [];
return this['destroy'](function (value){
return ko.utils.arrayIndexOf(arrayOfValues, value) >=0;
});
},
'indexOf': function (item){
var underlyingArray=this();
return ko.utils.arrayIndexOf(underlyingArray, item);
},
'replace': function(oldItem, newItem){
var index=this['indexOf'](oldItem);
if(index >=0){
this.valueWillMutate();
this.peek()[index]=newItem;
this.valueHasMutated();
}}
};
if(ko.utils.canSetPrototype){
ko.utils.setPrototypeOf(ko.observableArray['fn'], ko.observable['fn']);
}
ko.utils.arrayForEach(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function (methodName){
ko.observableArray['fn'][methodName]=function (){
var underlyingArray=this.peek();
this.valueWillMutate();
this.cacheDiffForKnownOperation(underlyingArray, methodName, arguments);
var methodCallResult=underlyingArray[methodName].apply(underlyingArray, arguments);
this.valueHasMutated();
return methodCallResult===underlyingArray ? this:methodCallResult;
};});
ko.utils.arrayForEach(["slice"], function (methodName){
ko.observableArray['fn'][methodName]=function (){
var underlyingArray=this();
return underlyingArray[methodName].apply(underlyingArray, arguments);
};});
ko.exportSymbol('observableArray', ko.observableArray);
var arrayChangeEventName='arrayChange';
ko.extenders['trackArrayChanges']=function(target, options){
target.compareArrayOptions={};
if(options&&typeof options=="object"){
ko.utils.extend(target.compareArrayOptions, options);
}
target.compareArrayOptions['sparse']=true;
if(target.cacheDiffForKnownOperation){
return;
}
var trackingChanges=false,
cachedDiff=null,
arrayChangeSubscription,
pendingNotifications=0,
underlyingNotifySubscribersFunction,
underlyingBeforeSubscriptionAddFunction=target.beforeSubscriptionAdd,
underlyingAfterSubscriptionRemoveFunction=target.afterSubscriptionRemove;
target.beforeSubscriptionAdd=function (event){
if(underlyingBeforeSubscriptionAddFunction)
underlyingBeforeSubscriptionAddFunction.call(target, event);
if(event===arrayChangeEventName){
trackChanges();
}};
target.afterSubscriptionRemove=function (event){
if(underlyingAfterSubscriptionRemoveFunction)
underlyingAfterSubscriptionRemoveFunction.call(target, event);
if(event===arrayChangeEventName&&!target.hasSubscriptionsForEvent(arrayChangeEventName)){
if(underlyingNotifySubscribersFunction){
target['notifySubscribers']=underlyingNotifySubscribersFunction;
underlyingNotifySubscribersFunction=undefined;
}
arrayChangeSubscription.dispose();
trackingChanges=false;
}};
function trackChanges(){
if(trackingChanges){
return;
}
trackingChanges=true;
underlyingNotifySubscribersFunction=target['notifySubscribers'];
target['notifySubscribers']=function(valueToNotify, event){
if(!event||event===defaultEvent){
++pendingNotifications;
}
return underlyingNotifySubscribersFunction.apply(this, arguments);
};
var previousContents=[].concat(target.peek()||[]);
cachedDiff=null;
arrayChangeSubscription=target.subscribe(function(currentContents){
currentContents=[].concat(currentContents||[]);
if(target.hasSubscriptionsForEvent(arrayChangeEventName)){
var changes=getChanges(previousContents, currentContents);
}
previousContents=currentContents;
cachedDiff=null;
pendingNotifications=0;
if(changes&&changes.length){
target['notifySubscribers'](changes, arrayChangeEventName);
}});
}
function getChanges(previousContents, currentContents){
if(!cachedDiff||pendingNotifications > 1){
cachedDiff=ko.utils.compareArrays(previousContents, currentContents, target.compareArrayOptions);
}
return cachedDiff;
}
target.cacheDiffForKnownOperation=function(rawArray, operationName, args){
if(!trackingChanges||pendingNotifications){
return;
}
var diff=[],
arrayLength=rawArray.length,
argsLength=args.length,
offset=0;
function pushDiff(status, value, index){
return diff[diff.length]={ 'status': status, 'value': value, 'index': index };}
switch (operationName){
case 'push':
offset=arrayLength;
case 'unshift':
for (var index=0; index < argsLength; index++){
pushDiff('added', args[index], offset + index);
}
break;
case 'pop':
offset=arrayLength - 1;
case 'shift':
if(arrayLength){
pushDiff('deleted', rawArray[offset], offset);
}
break;
case 'splice':
var startIndex=Math.min(Math.max(0, args[0] < 0 ? arrayLength + args[0]:args[0]), arrayLength),
endDeleteIndex=argsLength===1 ? arrayLength:Math.min(startIndex + (args[1]||0), arrayLength),
endAddIndex=startIndex + argsLength - 2,
endIndex=Math.max(endDeleteIndex, endAddIndex),
additions=[], deletions=[];
for (var index=startIndex, argsIndex=2; index < endIndex; ++index, ++argsIndex){
if(index < endDeleteIndex)
deletions.push(pushDiff('deleted', rawArray[index], index));
if(index < endAddIndex)
additions.push(pushDiff('added', args[argsIndex], index));
}
ko.utils.findMovesInArrayComparison(deletions, additions);
break;
default:
return;
}
cachedDiff=diff;
};};
var computedState=ko.utils.createSymbolOrString('_state');
ko.computed=ko.dependentObservable=function (evaluatorFunctionOrOptions, evaluatorFunctionTarget, options){
if(typeof evaluatorFunctionOrOptions==="object"){
options=evaluatorFunctionOrOptions;
}else{
options=options||{};
if(evaluatorFunctionOrOptions){
options["read"]=evaluatorFunctionOrOptions;
}}
if(typeof options["read"]!="function")
throw Error("Pass a function that returns the value of the ko.computed");
var writeFunction=options["write"];
var state={
latestValue: undefined,
isStale: true,
isDirty: true,
isBeingEvaluated: false,
suppressDisposalUntilDisposeWhenReturnsFalse: false,
isDisposed: false,
pure: false,
isSleeping: false,
readFunction: options["read"],
evaluatorFunctionTarget: evaluatorFunctionTarget||options["owner"],
disposeWhenNodeIsRemoved: options["disposeWhenNodeIsRemoved"]||options.disposeWhenNodeIsRemoved||null,
disposeWhen: options["disposeWhen"]||options.disposeWhen,
domNodeDisposalCallback: null,
dependencyTracking: {},
dependenciesCount: 0,
evaluationTimeoutInstance: null
};
function computedObservable(){
if(arguments.length > 0){
if(typeof writeFunction==="function"){
writeFunction.apply(state.evaluatorFunctionTarget, arguments);
}else{
throw new Error("Cannot write a value to a ko.computed unless you specify a 'write' option. If you wish to read the current value, don't pass any parameters.");
}
return this;
}else{
ko.dependencyDetection.registerDependency(computedObservable);
if(state.isDirty||(state.isSleeping&&computedObservable.haveDependenciesChanged())){
computedObservable.evaluateImmediate();
}
return state.latestValue;
}}
computedObservable[computedState]=state;
computedObservable.hasWriteFunction=typeof writeFunction==="function";
if(!ko.utils.canSetPrototype){
ko.utils.extend(computedObservable, ko.subscribable['fn']);
}
ko.subscribable['fn'].init(computedObservable);
ko.utils.setPrototypeOfOrExtend(computedObservable, computedFn);
if(options['pure']){
state.pure=true;
state.isSleeping=true;
ko.utils.extend(computedObservable, pureComputedOverrides);
}else if(options['deferEvaluation']){
ko.utils.extend(computedObservable, deferEvaluationOverrides);
}
if(ko.options['deferUpdates']){
ko.extenders['deferred'](computedObservable, true);
}
if(DEBUG){
computedObservable["_options"]=options;
}
if(state.disposeWhenNodeIsRemoved){
state.suppressDisposalUntilDisposeWhenReturnsFalse=true;
if(!state.disposeWhenNodeIsRemoved.nodeType){
state.disposeWhenNodeIsRemoved=null;
}}
if(!state.isSleeping&&!options['deferEvaluation']){
computedObservable.evaluateImmediate();
}
if(state.disposeWhenNodeIsRemoved&&computedObservable.isActive()){
ko.utils.domNodeDisposal.addDisposeCallback(state.disposeWhenNodeIsRemoved, state.domNodeDisposalCallback=function (){
computedObservable.dispose();
});
}
return computedObservable;
};
function computedDisposeDependencyCallback(id, entryToDispose){
if(entryToDispose!==null&&entryToDispose.dispose){
entryToDispose.dispose();
}}
function computedBeginDependencyDetectionCallback(subscribable, id){
var computedObservable=this.computedObservable,
state=computedObservable[computedState];
if(!state.isDisposed){
if(this.disposalCount&&this.disposalCandidates[id]){
computedObservable.addDependencyTracking(id, subscribable, this.disposalCandidates[id]);
this.disposalCandidates[id]=null;
--this.disposalCount;
}else if(!state.dependencyTracking[id]){
computedObservable.addDependencyTracking(id, subscribable, state.isSleeping ? { _target: subscribable }:computedObservable.subscribeToDependency(subscribable));
}
if(subscribable._notificationIsPending){
subscribable._notifyNextChangeIfValueIsDifferent();
}}
}
var computedFn={
"equalityComparer": valuesArePrimitiveAndEqual,
getDependenciesCount: function (){
return this[computedState].dependenciesCount;
},
addDependencyTracking: function (id, target, trackingObj){
if(this[computedState].pure&&target===this){
throw Error("A 'pure' computed must not be called recursively");
}
this[computedState].dependencyTracking[id]=trackingObj;
trackingObj._order=this[computedState].dependenciesCount++;
trackingObj._version=target.getVersion();
},
haveDependenciesChanged: function (){
var id, dependency, dependencyTracking=this[computedState].dependencyTracking;
for (id in dependencyTracking){
if(dependencyTracking.hasOwnProperty(id)){
dependency=dependencyTracking[id];
if((this._evalDelayed&&dependency._target._notificationIsPending)||dependency._target.hasChanged(dependency._version)){
return true;
}}
}},
markDirty: function (){
if(this._evalDelayed&&!this[computedState].isBeingEvaluated){
this._evalDelayed(false );
}},
isActive: function (){
var state=this[computedState];
return state.isDirty||state.dependenciesCount > 0;
},
respondToChange: function (){
if(!this._notificationIsPending){
this.evaluatePossiblyAsync();
}else if(this[computedState].isDirty){
this[computedState].isStale=true;
}},
subscribeToDependency: function (target){
if(target._deferUpdates&&!this[computedState].disposeWhenNodeIsRemoved){
var dirtySub=target.subscribe(this.markDirty, this, 'dirty'),
changeSub=target.subscribe(this.respondToChange, this);
return {
_target: target,
dispose: function (){
dirtySub.dispose();
changeSub.dispose();
}};}else{
return target.subscribe(this.evaluatePossiblyAsync, this);
}},
evaluatePossiblyAsync: function (){
var computedObservable=this,
throttleEvaluationTimeout=computedObservable['throttleEvaluation'];
if(throttleEvaluationTimeout&&throttleEvaluationTimeout >=0){
clearTimeout(this[computedState].evaluationTimeoutInstance);
this[computedState].evaluationTimeoutInstance=ko.utils.setTimeout(function (){
computedObservable.evaluateImmediate(true );
}, throttleEvaluationTimeout);
}else if(computedObservable._evalDelayed){
computedObservable._evalDelayed(true );
}else{
computedObservable.evaluateImmediate(true );
}},
evaluateImmediate: function (notifyChange){
var computedObservable=this,
state=computedObservable[computedState],
disposeWhen=state.disposeWhen,
changed=false;
if(state.isBeingEvaluated){
return;
}
if(state.isDisposed){
return;
}
if(state.disposeWhenNodeIsRemoved&&!ko.utils.domNodeIsAttachedToDocument(state.disposeWhenNodeIsRemoved)||disposeWhen&&disposeWhen()){
if(!state.suppressDisposalUntilDisposeWhenReturnsFalse){
computedObservable.dispose();
return;
}}else{
state.suppressDisposalUntilDisposeWhenReturnsFalse=false;
}
state.isBeingEvaluated=true;
try {
changed=this.evaluateImmediate_CallReadWithDependencyDetection(notifyChange);
} finally {
state.isBeingEvaluated=false;
}
if(!state.dependenciesCount){
computedObservable.dispose();
}
return changed;
},
evaluateImmediate_CallReadWithDependencyDetection: function (notifyChange){
var computedObservable=this,
state=computedObservable[computedState],
changed=false;
var isInitial=state.pure ? undefined:!state.dependenciesCount,
dependencyDetectionContext={
computedObservable: computedObservable,
disposalCandidates: state.dependencyTracking,
disposalCount: state.dependenciesCount
};
ko.dependencyDetection.begin({
callbackTarget: dependencyDetectionContext,
callback: computedBeginDependencyDetectionCallback,
computed: computedObservable,
isInitial: isInitial
});
state.dependencyTracking={};
state.dependenciesCount=0;
var newValue=this.evaluateImmediate_CallReadThenEndDependencyDetection(state, dependencyDetectionContext);
if(computedObservable.isDifferent(state.latestValue, newValue)){
if(!state.isSleeping){
computedObservable["notifySubscribers"](state.latestValue, "beforeChange");
}
state.latestValue=newValue;
if(DEBUG) computedObservable._latestValue=newValue;
if(state.isSleeping){
computedObservable.updateVersion();
}else if(notifyChange){
computedObservable["notifySubscribers"](state.latestValue);
}
changed=true;
}
if(isInitial){
computedObservable["notifySubscribers"](state.latestValue, "awake");
}
return changed;
},
evaluateImmediate_CallReadThenEndDependencyDetection: function (state, dependencyDetectionContext){
try {
var readFunction=state.readFunction;
return state.evaluatorFunctionTarget ? readFunction.call(state.evaluatorFunctionTarget):readFunction();
} finally {
ko.dependencyDetection.end();
if(dependencyDetectionContext.disposalCount&&!state.isSleeping){
ko.utils.objectForEach(dependencyDetectionContext.disposalCandidates, computedDisposeDependencyCallback);
}
state.isStale=state.isDirty=false;
}},
peek: function (evaluate){
var state=this[computedState];
if((state.isDirty&&(evaluate||!state.dependenciesCount))||(state.isSleeping&&this.haveDependenciesChanged())){
this.evaluateImmediate();
}
return state.latestValue;
},
limit: function (limitFunction){
ko.subscribable['fn'].limit.call(this, limitFunction);
this._evalIfChanged=function (){
if(this[computedState].isStale){
this.evaluateImmediate();
}else{
this[computedState].isDirty=false;
}
return this[computedState].latestValue;
};
this._evalDelayed=function (isChange){
this._limitBeforeChange(this[computedState].latestValue);
this[computedState].isDirty=true;
if(isChange){
this[computedState].isStale=true;
}
this._limitChange(this);
};},
dispose: function (){
var state=this[computedState];
if(!state.isSleeping&&state.dependencyTracking){
ko.utils.objectForEach(state.dependencyTracking, function (id, dependency){
if(dependency.dispose)
dependency.dispose();
});
}
if(state.disposeWhenNodeIsRemoved&&state.domNodeDisposalCallback){
ko.utils.domNodeDisposal.removeDisposeCallback(state.disposeWhenNodeIsRemoved, state.domNodeDisposalCallback);
}
state.dependencyTracking=null;
state.dependenciesCount=0;
state.isDisposed=true;
state.isStale=false;
state.isDirty=false;
state.isSleeping=false;
state.disposeWhenNodeIsRemoved=null;
}};
var pureComputedOverrides={
beforeSubscriptionAdd: function (event){
var computedObservable=this,
state=computedObservable[computedState];
if(!state.isDisposed&&state.isSleeping&&event=='change'){
state.isSleeping=false;
if(state.isStale||computedObservable.haveDependenciesChanged()){
state.dependencyTracking=null;
state.dependenciesCount=0;
if(computedObservable.evaluateImmediate()){
computedObservable.updateVersion();
}}else{
var dependeciesOrder=[];
ko.utils.objectForEach(state.dependencyTracking, function (id, dependency){
dependeciesOrder[dependency._order]=id;
});
ko.utils.arrayForEach(dependeciesOrder, function (id, order){
var dependency=state.dependencyTracking[id],
subscription=computedObservable.subscribeToDependency(dependency._target);
subscription._order=order;
subscription._version=dependency._version;
state.dependencyTracking[id]=subscription;
});
}
if(!state.isDisposed){
computedObservable["notifySubscribers"](state.latestValue, "awake");
}}
},
afterSubscriptionRemove: function (event){
var state=this[computedState];
if(!state.isDisposed&&event=='change'&&!this.hasSubscriptionsForEvent('change')){
ko.utils.objectForEach(state.dependencyTracking, function (id, dependency){
if(dependency.dispose){
state.dependencyTracking[id]={
_target: dependency._target,
_order: dependency._order,
_version: dependency._version
};
dependency.dispose();
}});
state.isSleeping=true;
this["notifySubscribers"](undefined, "asleep");
}},
getVersion: function (){
var state=this[computedState];
if(state.isSleeping&&(state.isStale||this.haveDependenciesChanged())){
this.evaluateImmediate();
}
return ko.subscribable['fn'].getVersion.call(this);
}};
var deferEvaluationOverrides={
beforeSubscriptionAdd: function (event){
if(event=='change'||event=='beforeChange'){
this.peek();
}}
};
if(ko.utils.canSetPrototype){
ko.utils.setPrototypeOf(computedFn, ko.subscribable['fn']);
}
var protoProp=ko.observable.protoProperty;
ko.computed[protoProp]=ko.observable;
computedFn[protoProp]=ko.computed;
ko.isComputed=function (instance){
return ko.hasPrototype(instance, ko.computed);
};
ko.isPureComputed=function (instance){
return ko.hasPrototype(instance, ko.computed)
&& instance[computedState]&&instance[computedState].pure;
};
ko.exportSymbol('computed', ko.computed);
ko.exportSymbol('dependentObservable', ko.computed);
ko.exportSymbol('isComputed', ko.isComputed);
ko.exportSymbol('isPureComputed', ko.isPureComputed);
ko.exportSymbol('computed.fn', computedFn);
ko.exportProperty(computedFn, 'peek', computedFn.peek);
ko.exportProperty(computedFn, 'dispose', computedFn.dispose);
ko.exportProperty(computedFn, 'isActive', computedFn.isActive);
ko.exportProperty(computedFn, 'getDependenciesCount', computedFn.getDependenciesCount);
ko.pureComputed=function (evaluatorFunctionOrOptions, evaluatorFunctionTarget){
if(typeof evaluatorFunctionOrOptions==='function'){
return ko.computed(evaluatorFunctionOrOptions, evaluatorFunctionTarget, {'pure':true});
}else{
evaluatorFunctionOrOptions=ko.utils.extend({}, evaluatorFunctionOrOptions);
evaluatorFunctionOrOptions['pure']=true;
return ko.computed(evaluatorFunctionOrOptions, evaluatorFunctionTarget);
}}
ko.exportSymbol('pureComputed', ko.pureComputed);
(function(){
var maxNestedObservableDepth=10;
ko.toJS=function(rootObject){
if(arguments.length==0)
throw new Error("When calling ko.toJS, pass the object you want to convert.");
return mapJsObjectGraph(rootObject, function(valueToMap){
for (var i=0; ko.isObservable(valueToMap)&&(i < maxNestedObservableDepth); i++)
valueToMap=valueToMap();
return valueToMap;
});
};
ko.toJSON=function(rootObject, replacer, space){
var plainJavaScriptObject=ko.toJS(rootObject);
return ko.utils.stringifyJson(plainJavaScriptObject, replacer, space);
};
function mapJsObjectGraph(rootObject, mapInputCallback, visitedObjects){
visitedObjects=visitedObjects||new objectLookup();
rootObject=mapInputCallback(rootObject);
var canHaveProperties=(typeof rootObject=="object")&&(rootObject!==null)&&(rootObject!==undefined)&&(!(rootObject instanceof RegExp))&&(!(rootObject instanceof Date))&&(!(rootObject instanceof String))&&(!(rootObject instanceof Number))&&(!(rootObject instanceof Boolean));
if(!canHaveProperties)
return rootObject;
var outputProperties=rootObject instanceof Array ? []:{};
visitedObjects.save(rootObject, outputProperties);
visitPropertiesOrArrayEntries(rootObject, function(indexer){
var propertyValue=mapInputCallback(rootObject[indexer]);
switch (typeof propertyValue){
case "boolean":
case "number":
case "string":
case "function":
outputProperties[indexer]=propertyValue;
break;
case "object":
case "undefined":
var previouslyMappedValue=visitedObjects.get(propertyValue);
outputProperties[indexer]=(previouslyMappedValue!==undefined)
? previouslyMappedValue
: mapJsObjectGraph(propertyValue, mapInputCallback, visitedObjects);
break;
}});
return outputProperties;
}
function visitPropertiesOrArrayEntries(rootObject, visitorCallback){
if(rootObject instanceof Array){
for (var i=0; i < rootObject.length; i++)
visitorCallback(i);
if(typeof rootObject['toJSON']=='function')
visitorCallback('toJSON');
}else{
for (var propertyName in rootObject){
visitorCallback(propertyName);
}}
};
function objectLookup(){
this.keys=[];
this.values=[];
};
objectLookup.prototype={
constructor: objectLookup,
save: function(key, value){
var existingIndex=ko.utils.arrayIndexOf(this.keys, key);
if(existingIndex >=0)
this.values[existingIndex]=value;
else {
this.keys.push(key);
this.values.push(value);
}},
get: function(key){
var existingIndex=ko.utils.arrayIndexOf(this.keys, key);
return (existingIndex >=0) ? this.values[existingIndex]:undefined;
}};})();
ko.exportSymbol('toJS', ko.toJS);
ko.exportSymbol('toJSON', ko.toJSON);
(function (){
var hasDomDataExpandoProperty='__ko__hasDomDataOptionValue__';
ko.selectExtensions={
readValue:function(element){
switch (ko.utils.tagNameLower(element)){
case 'option':
if(element[hasDomDataExpandoProperty]===true)
return ko.utils.domData.get(element, ko.bindingHandlers.options.optionValueDomDataKey);
return ko.utils.ieVersion <=7
? (element.getAttributeNode('value')&&element.getAttributeNode('value').specified ? element.value:element.text)
: element.value;
case 'select':
return element.selectedIndex >=0 ? ko.selectExtensions.readValue(element.options[element.selectedIndex]):undefined;
default:
return element.value;
}},
writeValue: function(element, value, allowUnset){
switch (ko.utils.tagNameLower(element)){
case 'option':
switch(typeof value){
case "string":
ko.utils.domData.set(element, ko.bindingHandlers.options.optionValueDomDataKey, undefined);
if(hasDomDataExpandoProperty in element){
delete element[hasDomDataExpandoProperty];
}
element.value=value;
break;
default:
ko.utils.domData.set(element, ko.bindingHandlers.options.optionValueDomDataKey, value);
element[hasDomDataExpandoProperty]=true;
element.value=typeof value==="number" ? value:"";
break;
}
break;
case 'select':
if(value===""||value===null)
value=undefined;
var selection=-1;
for (var i=0, n=element.options.length, optionValue; i < n; ++i){
optionValue=ko.selectExtensions.readValue(element.options[i]);
if(optionValue==value||(optionValue==""&&value===undefined)){
selection=i;
break;
}}
if(allowUnset||selection >=0||(value===undefined&&element.size > 1)){
element.selectedIndex=selection;
}
break;
default:
if((value===null)||(value===undefined))
value="";
element.value=value;
break;
}}
};})();
ko.exportSymbol('selectExtensions', ko.selectExtensions);
ko.exportSymbol('selectExtensions.readValue', ko.selectExtensions.readValue);
ko.exportSymbol('selectExtensions.writeValue', ko.selectExtensions.writeValue);
ko.expressionRewriting=(function (){
var javaScriptReservedWords=["true", "false", "null", "undefined"];
var javaScriptAssignmentTarget=/^(?:[$_a-z][$\w]*|(.+)(\.\s*[$_a-z][$\w]*|\[.+\]))$/i;
function getWriteableValue(expression){
if(ko.utils.arrayIndexOf(javaScriptReservedWords, expression) >=0)
return false;
var match=expression.match(javaScriptAssignmentTarget);
return match===null ? false:match[1] ? ('Object(' + match[1] + ')' + match[2]):expression;
}
var stringDouble='"(?:[^"\\\\]|\\\\.)*"',
stringSingle="'(?:[^'\\\\]|\\\\.)*'",
stringRegexp='/(?:[^/\\\\]|\\\\.)*/\w*',
specials=',"\'{}()/:[\\]',
everyThingElse='[^\\s:,/][^' + specials + ']*[^\\s' + specials + ']',
oneNotSpace='[^\\s]',
bindingToken=RegExp(stringDouble + '|' + stringSingle + '|' + stringRegexp + '|' + everyThingElse + '|' + oneNotSpace, 'g'),
divisionLookBehind=/[\])"'A-Za-z0-9_$]+$/,
keywordRegexLookBehind={'in':1,'return':1,'typeof':1};
function parseObjectLiteral(objectLiteralString){
var str=ko.utils.stringTrim(objectLiteralString);
if(str.charCodeAt(0)===123) str=str.slice(1, -1);
var result=[], toks=str.match(bindingToken), key, values=[], depth=0;
if(toks){
toks.push(',');
for (var i=0, tok; tok=toks[i]; ++i){
var c=tok.charCodeAt(0);
if(c===44){ // ","
if(depth <=0){
result.push((key&&values.length) ? {key: key, value: values.join('')}:{'unknown': key||values.join('')});
key=depth=0;
values=[];
continue;
}}else if(c===58){ // ":"
if(!depth&&!key&&values.length===1){
key=values.pop();
continue;
}}else if(c===47&&i && tok.length > 1){  // "/"
var match=toks[i-1].match(divisionLookBehind);
if(match&&!keywordRegexLookBehind[match[0]]){
str=str.substr(str.indexOf(tok) + 1);
toks=str.match(bindingToken);
toks.push(',');
i=-1;
tok='/';
}}else if(c===40||c===123||c===91){ // '(', '{', '['
++depth;
}else if(c===41||c===125||c===93){ // ')', '}', ']'
--depth;
}else if(!key&&!values.length&&(c===34||c===39)){ // '"', "'"
tok=tok.slice(1, -1);
}
values.push(tok);
}}
return result;
}
var twoWayBindings={};
function preProcessBindings(bindingsStringOrKeyValueArray, bindingOptions){
bindingOptions=bindingOptions||{};
function processKeyValue(key, val){
var writableVal;
function callPreprocessHook(obj){
return (obj&&obj['preprocess']) ? (val=obj['preprocess'](val, key, processKeyValue)):true;
}
if(!bindingParams){
if(!callPreprocessHook(ko['getBindingHandler'](key)))
return;
if(twoWayBindings[key]&&(writableVal=getWriteableValue(val))){
propertyAccessorResultStrings.push("'" + key + "':function(_z){" + writableVal + "=_z}");
}}
if(makeValueAccessors){
val='function(){return ' + val + ' }';
}
resultStrings.push("'" + key + "':" + val);
}
var resultStrings=[],
propertyAccessorResultStrings=[],
makeValueAccessors=bindingOptions['valueAccessors'],
bindingParams=bindingOptions['bindingParams'],
keyValueArray=typeof bindingsStringOrKeyValueArray==="string" ?
parseObjectLiteral(bindingsStringOrKeyValueArray):bindingsStringOrKeyValueArray;
ko.utils.arrayForEach(keyValueArray, function(keyValue){
processKeyValue(keyValue.key||keyValue['unknown'], keyValue.value);
});
if(propertyAccessorResultStrings.length)
processKeyValue('_ko_property_writers', "{" + propertyAccessorResultStrings.join(",") + " }");
return resultStrings.join(",");
}
return {
bindingRewriteValidators: [],
twoWayBindings: twoWayBindings,
parseObjectLiteral: parseObjectLiteral,
preProcessBindings: preProcessBindings,
keyValueArrayContainsKey: function(keyValueArray, key){
for (var i=0; i < keyValueArray.length; i++)
if(keyValueArray[i]['key']==key)
return true;
return false;
},
writeValueToProperty: function(property, allBindings, key, value, checkIfDifferent){
if(!property||!ko.isObservable(property)){
var propWriters=allBindings.get('_ko_property_writers');
if(propWriters&&propWriters[key])
propWriters[key](value);
}else if(ko.isWriteableObservable(property)&&(!checkIfDifferent||property.peek()!==value)){
property(value);
}}
};})();
ko.exportSymbol('expressionRewriting', ko.expressionRewriting);
ko.exportSymbol('expressionRewriting.bindingRewriteValidators', ko.expressionRewriting.bindingRewriteValidators);
ko.exportSymbol('expressionRewriting.parseObjectLiteral', ko.expressionRewriting.parseObjectLiteral);
ko.exportSymbol('expressionRewriting.preProcessBindings', ko.expressionRewriting.preProcessBindings);
ko.exportSymbol('expressionRewriting._twoWayBindings', ko.expressionRewriting.twoWayBindings);
ko.exportSymbol('jsonExpressionRewriting', ko.expressionRewriting);
ko.exportSymbol('jsonExpressionRewriting.insertPropertyAccessorsIntoJson', ko.expressionRewriting.preProcessBindings);
(function(){
var commentNodesHaveTextProperty=document&&document.createComment("test").text==="<!--test-->";
var startCommentRegex=commentNodesHaveTextProperty ? /^<!--\s*ko(?:\s+([\s\S]+))?\s*-->$/:/^\s*ko(?:\s+([\s\S]+))?\s*$/;
var endCommentRegex=commentNodesHaveTextProperty ? /^<!--\s*\/ko\s*-->$/:/^\s*\/ko\s*$/;
var htmlTagsWithOptionallyClosingChildren={ 'ul': true, 'ol': true };
function isStartComment(node){
return (node.nodeType==8)&&startCommentRegex.test(commentNodesHaveTextProperty ? node.text:node.nodeValue);
}
function isEndComment(node){
return (node.nodeType==8)&&endCommentRegex.test(commentNodesHaveTextProperty ? node.text:node.nodeValue);
}
function getVirtualChildren(startComment, allowUnbalanced){
var currentNode=startComment;
var depth=1;
var children=[];
while (currentNode=currentNode.nextSibling){
if(isEndComment(currentNode)){
depth--;
if(depth===0)
return children;
}
children.push(currentNode);
if(isStartComment(currentNode))
depth++;
}
if(!allowUnbalanced)
throw new Error("Cannot find closing comment tag to match: " + startComment.nodeValue);
return null;
}
function getMatchingEndComment(startComment, allowUnbalanced){
var allVirtualChildren=getVirtualChildren(startComment, allowUnbalanced);
if(allVirtualChildren){
if(allVirtualChildren.length > 0)
return allVirtualChildren[allVirtualChildren.length - 1].nextSibling;
return startComment.nextSibling;
} else
return null;
}
function getUnbalancedChildTags(node){
var childNode=node.firstChild, captureRemaining=null;
if(childNode){
do {
if(captureRemaining)
captureRemaining.push(childNode);
else if(isStartComment(childNode)){
var matchingEndComment=getMatchingEndComment(childNode,  true);
if(matchingEndComment)
childNode=matchingEndComment;
else
captureRemaining=[childNode];
}else if(isEndComment(childNode)){
captureRemaining=[childNode];
}} while (childNode=childNode.nextSibling);
}
return captureRemaining;
}
ko.virtualElements={
allowedBindings: {},
childNodes: function(node){
return isStartComment(node) ? getVirtualChildren(node):node.childNodes;
},
emptyNode: function(node){
if(!isStartComment(node))
ko.utils.emptyDomNode(node);
else {
var virtualChildren=ko.virtualElements.childNodes(node);
for (var i=0, j=virtualChildren.length; i < j; i++)
ko.removeNode(virtualChildren[i]);
}},
setDomNodeChildren: function(node, childNodes){
if(!isStartComment(node))
ko.utils.setDomNodeChildren(node, childNodes);
else {
ko.virtualElements.emptyNode(node);
var endCommentNode=node.nextSibling;
for (var i=0, j=childNodes.length; i < j; i++)
endCommentNode.parentNode.insertBefore(childNodes[i], endCommentNode);
}},
prepend: function(containerNode, nodeToPrepend){
if(!isStartComment(containerNode)){
if(containerNode.firstChild)
containerNode.insertBefore(nodeToPrepend, containerNode.firstChild);
else
containerNode.appendChild(nodeToPrepend);
}else{
containerNode.parentNode.insertBefore(nodeToPrepend, containerNode.nextSibling);
}},
insertAfter: function(containerNode, nodeToInsert, insertAfterNode){
if(!insertAfterNode){
ko.virtualElements.prepend(containerNode, nodeToInsert);
}else if(!isStartComment(containerNode)){
if(insertAfterNode.nextSibling)
containerNode.insertBefore(nodeToInsert, insertAfterNode.nextSibling);
else
containerNode.appendChild(nodeToInsert);
}else{
containerNode.parentNode.insertBefore(nodeToInsert, insertAfterNode.nextSibling);
}},
firstChild: function(node){
if(!isStartComment(node))
return node.firstChild;
if(!node.nextSibling||isEndComment(node.nextSibling))
return null;
return node.nextSibling;
},
nextSibling: function(node){
if(isStartComment(node))
node=getMatchingEndComment(node);
if(node.nextSibling&&isEndComment(node.nextSibling))
return null;
return node.nextSibling;
},
hasBindingValue: isStartComment,
virtualNodeBindingValue: function(node){
var regexMatch=(commentNodesHaveTextProperty ? node.text:node.nodeValue).match(startCommentRegex);
return regexMatch ? regexMatch[1]:null;
},
normaliseVirtualElementDomStructure: function(elementVerified){
if(!htmlTagsWithOptionallyClosingChildren[ko.utils.tagNameLower(elementVerified)])
return;
var childNode=elementVerified.firstChild;
if(childNode){
do {
if(childNode.nodeType===1){
var unbalancedTags=getUnbalancedChildTags(childNode);
if(unbalancedTags){
var nodeToInsertBefore=childNode.nextSibling;
for (var i=0; i < unbalancedTags.length; i++){
if(nodeToInsertBefore)
elementVerified.insertBefore(unbalancedTags[i], nodeToInsertBefore);
else
elementVerified.appendChild(unbalancedTags[i]);
}}
}} while (childNode=childNode.nextSibling);
}}
};})();
ko.exportSymbol('virtualElements', ko.virtualElements);
ko.exportSymbol('virtualElements.allowedBindings', ko.virtualElements.allowedBindings);
ko.exportSymbol('virtualElements.emptyNode', ko.virtualElements.emptyNode);
ko.exportSymbol('virtualElements.insertAfter', ko.virtualElements.insertAfter);
ko.exportSymbol('virtualElements.prepend', ko.virtualElements.prepend);
ko.exportSymbol('virtualElements.setDomNodeChildren', ko.virtualElements.setDomNodeChildren);
(function(){
var defaultBindingAttributeName="data-bind";
ko.bindingProvider=function(){
this.bindingCache={};};
ko.utils.extend(ko.bindingProvider.prototype, {
'nodeHasBindings': function(node){
switch (node.nodeType){
case 1:
return node.getAttribute(defaultBindingAttributeName)!=null
|| ko.components['getComponentNameForNode'](node);
case 8:
return ko.virtualElements.hasBindingValue(node);
default: return false;
}},
'getBindings': function(node, bindingContext){
var bindingsString=this['getBindingsString'](node, bindingContext),
parsedBindings=bindingsString ? this['parseBindingsString'](bindingsString, bindingContext, node):null;
return ko.components.addBindingsForCustomElement(parsedBindings, node, bindingContext,  false);
},
'getBindingAccessors': function(node, bindingContext){
var bindingsString=this['getBindingsString'](node, bindingContext),
parsedBindings=bindingsString ? this['parseBindingsString'](bindingsString, bindingContext, node, { 'valueAccessors': true }):null;
return ko.components.addBindingsForCustomElement(parsedBindings, node, bindingContext,  true);
},
'getBindingsString': function(node, bindingContext){
switch (node.nodeType){
case 1: return node.getAttribute(defaultBindingAttributeName);
case 8: return ko.virtualElements.virtualNodeBindingValue(node);
default: return null;
}},
'parseBindingsString': function(bindingsString, bindingContext, node, options){
try {
var bindingFunction=createBindingsStringEvaluatorViaCache(bindingsString, this.bindingCache, options);
return bindingFunction(bindingContext, node);
} catch (ex){
ex.message="Unable to parse bindings.\nBindings value: " + bindingsString + "\nMessage: " + ex.message;
throw ex;
}}
});
ko.bindingProvider['instance']=new ko.bindingProvider();
function createBindingsStringEvaluatorViaCache(bindingsString, cache, options){
var cacheKey=bindingsString + (options&&options['valueAccessors']||'');
return cache[cacheKey]
|| (cache[cacheKey]=createBindingsStringEvaluator(bindingsString, options));
}
function createBindingsStringEvaluator(bindingsString, options){
var rewrittenBindings=ko.expressionRewriting.preProcessBindings(bindingsString, options),
functionBody="with($context){with($data||{}){return{" + rewrittenBindings + "}}}";
return new Function("$context", "$element", functionBody);
}})();
ko.exportSymbol('bindingProvider', ko.bindingProvider);
(function (){
ko.bindingHandlers={};
var bindingDoesNotRecurseIntoElementTypes={
'script': true,
'textarea': true,
'template': true
};
ko['getBindingHandler']=function(bindingKey){
return ko.bindingHandlers[bindingKey];
};
ko.bindingContext=function(dataItemOrAccessor, parentContext, dataItemAlias, extendCallback, options){
function updateContext(){
var dataItemOrObservable=isFunc ? dataItemOrAccessor():dataItemOrAccessor,
dataItem=ko.utils.unwrapObservable(dataItemOrObservable);
if(parentContext){
if(parentContext._subscribable)
parentContext._subscribable();
ko.utils.extend(self, parentContext);
self._subscribable=subscribable;
}else{
self['$parents']=[];
self['$root']=dataItem;
self['ko']=ko;
}
self['$rawData']=dataItemOrObservable;
self['$data']=dataItem;
if(dataItemAlias)
self[dataItemAlias]=dataItem;
if(extendCallback)
extendCallback(self, parentContext, dataItem);
return self['$data'];
}
function disposeWhen(){
return nodes&&!ko.utils.anyDomNodeIsAttachedToDocument(nodes);
}
var self=this,
isFunc=typeof(dataItemOrAccessor)=="function"&&!ko.isObservable(dataItemOrAccessor),
nodes,
subscribable;
if(options&&options['exportDependencies']){
updateContext();
}else{
subscribable=ko.dependentObservable(updateContext, null, { disposeWhen: disposeWhen, disposeWhenNodeIsRemoved: true });
if(subscribable.isActive()){
self._subscribable=subscribable;
subscribable['equalityComparer']=null;
nodes=[];
subscribable._addNode=function(node){
nodes.push(node);
ko.utils.domNodeDisposal.addDisposeCallback(node, function(node){
ko.utils.arrayRemoveItem(nodes, node);
if(!nodes.length){
subscribable.dispose();
self._subscribable=subscribable=undefined;
}});
};}}
}
ko.bindingContext.prototype['createChildContext']=function (dataItemOrAccessor, dataItemAlias, extendCallback, options){
return new ko.bindingContext(dataItemOrAccessor, this, dataItemAlias, function(self, parentContext){
self['$parentContext']=parentContext;
self['$parent']=parentContext['$data'];
self['$parents']=(parentContext['$parents']||[]).slice(0);
self['$parents'].unshift(self['$parent']);
if(extendCallback)
extendCallback(self);
}, options);
};
ko.bindingContext.prototype['extend']=function(properties){
return new ko.bindingContext(this._subscribable||this['$data'], this, null, function(self, parentContext){
self['$rawData']=parentContext['$rawData'];
ko.utils.extend(self, typeof(properties)=="function" ? properties():properties);
});
};
ko.bindingContext.prototype.createStaticChildContext=function (dataItemOrAccessor, dataItemAlias){
return this['createChildContext'](dataItemOrAccessor, dataItemAlias, null, { "exportDependencies": true });
};
function makeValueAccessor(value){
return function(){
return value;
};}
function evaluateValueAccessor(valueAccessor){
return valueAccessor();
}
function makeAccessorsFromFunction(callback){
return ko.utils.objectMap(ko.dependencyDetection.ignore(callback), function(value, key){
return function(){
return callback()[key];
};});
}
function makeBindingAccessors(bindings, context, node){
if(typeof bindings==='function'){
return makeAccessorsFromFunction(bindings.bind(null, context, node));
}else{
return ko.utils.objectMap(bindings, makeValueAccessor);
}}
function getBindingsAndMakeAccessors(node, context){
return makeAccessorsFromFunction(this['getBindings'].bind(this, node, context));
}
function validateThatBindingIsAllowedForVirtualElements(bindingName){
var validator=ko.virtualElements.allowedBindings[bindingName];
if(!validator)
throw new Error("The binding '" + bindingName + "' cannot be used with virtual elements")
}
function applyBindingsToDescendantsInternal (bindingContext, elementOrVirtualElement, bindingContextsMayDifferFromDomParentElement){
var currentChild,
nextInQueue=ko.virtualElements.firstChild(elementOrVirtualElement),
provider=ko.bindingProvider['instance'],
preprocessNode=provider['preprocessNode'];
if(preprocessNode){
while (currentChild=nextInQueue){
nextInQueue=ko.virtualElements.nextSibling(currentChild);
preprocessNode.call(provider, currentChild);
}
nextInQueue=ko.virtualElements.firstChild(elementOrVirtualElement);
}
while (currentChild=nextInQueue){
nextInQueue=ko.virtualElements.nextSibling(currentChild);
applyBindingsToNodeAndDescendantsInternal(bindingContext, currentChild, bindingContextsMayDifferFromDomParentElement);
}}
function applyBindingsToNodeAndDescendantsInternal (bindingContext, nodeVerified, bindingContextMayDifferFromDomParentElement){
var shouldBindDescendants=true;
var isElement=(nodeVerified.nodeType===1);
if(isElement)
ko.virtualElements.normaliseVirtualElementDomStructure(nodeVerified);
var shouldApplyBindings=(isElement&&bindingContextMayDifferFromDomParentElement)
|| ko.bindingProvider['instance']['nodeHasBindings'](nodeVerified);
if(shouldApplyBindings)
shouldBindDescendants=applyBindingsToNodeInternal(nodeVerified, null, bindingContext, bindingContextMayDifferFromDomParentElement)['shouldBindDescendants'];
if(shouldBindDescendants&&!bindingDoesNotRecurseIntoElementTypes[ko.utils.tagNameLower(nodeVerified)]){
applyBindingsToDescendantsInternal(bindingContext, nodeVerified,  !isElement);
}}
var boundElementDomDataKey=ko.utils.domData.nextKey();
function topologicalSortBindings(bindings){
var result=[],
bindingsConsidered={},
cyclicDependencyStack=[];
ko.utils.objectForEach(bindings, function pushBinding(bindingKey){
if(!bindingsConsidered[bindingKey]){
var binding=ko['getBindingHandler'](bindingKey);
if(binding){
if(binding['after']){
cyclicDependencyStack.push(bindingKey);
ko.utils.arrayForEach(binding['after'], function(bindingDependencyKey){
if(bindings[bindingDependencyKey]){
if(ko.utils.arrayIndexOf(cyclicDependencyStack, bindingDependencyKey)!==-1){
throw Error("Cannot combine the following bindings, because they have a cyclic dependency: " + cyclicDependencyStack.join(", "));
}else{
pushBinding(bindingDependencyKey);
}}
});
cyclicDependencyStack.length--;
}
result.push({ key: bindingKey, handler: binding });
}
bindingsConsidered[bindingKey]=true;
}});
return result;
}
function applyBindingsToNodeInternal(node, sourceBindings, bindingContext, bindingContextMayDifferFromDomParentElement){
var alreadyBound=ko.utils.domData.get(node, boundElementDomDataKey);
if(!sourceBindings){
if(alreadyBound){
throw Error("You cannot apply bindings multiple times to the same element.");
}
ko.utils.domData.set(node, boundElementDomDataKey, true);
}
if(!alreadyBound&&bindingContextMayDifferFromDomParentElement)
ko.storedBindingContextForNode(node, bindingContext);
var bindings;
if(sourceBindings&&typeof sourceBindings!=='function'){
bindings=sourceBindings;
}else{
var provider=ko.bindingProvider['instance'],
getBindings=provider['getBindingAccessors']||getBindingsAndMakeAccessors;
var bindingsUpdater=ko.dependentObservable(function(){
bindings=sourceBindings ? sourceBindings(bindingContext, node):getBindings.call(provider, node, bindingContext);
if(bindings&&bindingContext._subscribable)
bindingContext._subscribable();
return bindings;
},
null, { disposeWhenNodeIsRemoved: node }
);
if(!bindings||!bindingsUpdater.isActive())
bindingsUpdater=null;
}
var bindingHandlerThatControlsDescendantBindings;
if(bindings){
var getValueAccessor=bindingsUpdater
? function(bindingKey){
return function(){
return evaluateValueAccessor(bindingsUpdater()[bindingKey]);
};}:function(bindingKey){
return bindings[bindingKey];
};
function allBindings(){
return ko.utils.objectMap(bindingsUpdater ? bindingsUpdater():bindings, evaluateValueAccessor);
}
allBindings['get']=function(key){
return bindings[key]&&evaluateValueAccessor(getValueAccessor(key));
};
allBindings['has']=function(key){
return key in bindings;
};
var orderedBindings=topologicalSortBindings(bindings);
ko.utils.arrayForEach(orderedBindings, function(bindingKeyAndHandler){
var handlerInitFn=bindingKeyAndHandler.handler["init"],
handlerUpdateFn=bindingKeyAndHandler.handler["update"],
bindingKey=bindingKeyAndHandler.key;
if(node.nodeType===8){
validateThatBindingIsAllowedForVirtualElements(bindingKey);
}
try {
if(typeof handlerInitFn=="function"){
ko.dependencyDetection.ignore(function(){
var initResult=handlerInitFn(node, getValueAccessor(bindingKey), allBindings, bindingContext['$data'], bindingContext);
if(initResult&&initResult['controlsDescendantBindings']){
if(bindingHandlerThatControlsDescendantBindings!==undefined)
throw new Error("Multiple bindings (" + bindingHandlerThatControlsDescendantBindings + " and " + bindingKey + ") are trying to control descendant bindings of the same element. You cannot use these bindings together on the same element.");
bindingHandlerThatControlsDescendantBindings=bindingKey;
}});
}
if(typeof handlerUpdateFn=="function"){
ko.dependentObservable(function(){
handlerUpdateFn(node, getValueAccessor(bindingKey), allBindings, bindingContext['$data'], bindingContext);
},
null,
{ disposeWhenNodeIsRemoved: node }
);
}} catch (ex){
ex.message="Unable to process binding \"" + bindingKey + ": " + bindings[bindingKey] + "\"\nMessage: " + ex.message;
throw ex;
}});
}
return {
'shouldBindDescendants': bindingHandlerThatControlsDescendantBindings===undefined
};};
var storedBindingContextDomDataKey=ko.utils.domData.nextKey();
ko.storedBindingContextForNode=function (node, bindingContext){
if(arguments.length==2){
ko.utils.domData.set(node, storedBindingContextDomDataKey, bindingContext);
if(bindingContext._subscribable)
bindingContext._subscribable._addNode(node);
}else{
return ko.utils.domData.get(node, storedBindingContextDomDataKey);
}}
function getBindingContext(viewModelOrBindingContext){
return viewModelOrBindingContext&&(viewModelOrBindingContext instanceof ko.bindingContext)
? viewModelOrBindingContext
: new ko.bindingContext(viewModelOrBindingContext);
}
ko.applyBindingAccessorsToNode=function (node, bindings, viewModelOrBindingContext){
if(node.nodeType===1)
ko.virtualElements.normaliseVirtualElementDomStructure(node);
return applyBindingsToNodeInternal(node, bindings, getBindingContext(viewModelOrBindingContext), true);
};
ko.applyBindingsToNode=function (node, bindings, viewModelOrBindingContext){
var context=getBindingContext(viewModelOrBindingContext);
return ko.applyBindingAccessorsToNode(node, makeBindingAccessors(bindings, context, node), context);
};
ko.applyBindingsToDescendants=function(viewModelOrBindingContext, rootNode){
if(rootNode.nodeType===1||rootNode.nodeType===8)
applyBindingsToDescendantsInternal(getBindingContext(viewModelOrBindingContext), rootNode, true);
};
ko.applyBindings=function (viewModelOrBindingContext, rootNode){
if(!jQueryInstance&&window['jQuery']){
jQueryInstance=window['jQuery'];
}
if(rootNode&&(rootNode.nodeType!==1)&&(rootNode.nodeType!==8))
throw new Error("ko.applyBindings: first parameter should be your view model; second parameter should be a DOM node");
rootNode=rootNode||window.document.body;
applyBindingsToNodeAndDescendantsInternal(getBindingContext(viewModelOrBindingContext), rootNode, true);
};
ko.contextFor=function(node){
switch (node.nodeType){
case 1:
case 8:
var context=ko.storedBindingContextForNode(node);
if(context) return context;
if(node.parentNode) return ko.contextFor(node.parentNode);
break;
}
return undefined;
};
ko.dataFor=function(node){
var context=ko.contextFor(node);
return context ? context['$data']:undefined;
};
ko.exportSymbol('bindingHandlers', ko.bindingHandlers);
ko.exportSymbol('applyBindings', ko.applyBindings);
ko.exportSymbol('applyBindingsToDescendants', ko.applyBindingsToDescendants);
ko.exportSymbol('applyBindingAccessorsToNode', ko.applyBindingAccessorsToNode);
ko.exportSymbol('applyBindingsToNode', ko.applyBindingsToNode);
ko.exportSymbol('contextFor', ko.contextFor);
ko.exportSymbol('dataFor', ko.dataFor);
})();
(function(undefined){
var loadingSubscribablesCache={},
loadedDefinitionsCache={};
ko.components={
get: function(componentName, callback){
var cachedDefinition=getObjectOwnProperty(loadedDefinitionsCache, componentName);
if(cachedDefinition){
if(cachedDefinition.isSynchronousComponent){
ko.dependencyDetection.ignore(function(){
callback(cachedDefinition.definition);
});
}else{
ko.tasks.schedule(function(){ callback(cachedDefinition.definition); });
}}else{
loadComponentAndNotify(componentName, callback);
}},
clearCachedDefinition: function(componentName){
delete loadedDefinitionsCache[componentName];
},
_getFirstResultFromLoaders: getFirstResultFromLoaders
};
function getObjectOwnProperty(obj, propName){
return obj.hasOwnProperty(propName) ? obj[propName]:undefined;
}
function loadComponentAndNotify(componentName, callback){
var subscribable=getObjectOwnProperty(loadingSubscribablesCache, componentName),
completedAsync;
if(!subscribable){
subscribable=loadingSubscribablesCache[componentName]=new ko.subscribable();
subscribable.subscribe(callback);
beginLoadingComponent(componentName, function(definition, config){
var isSynchronousComponent = !!(config&&config['synchronous']);
loadedDefinitionsCache[componentName]={ definition: definition, isSynchronousComponent: isSynchronousComponent };
delete loadingSubscribablesCache[componentName];
if(completedAsync||isSynchronousComponent){
subscribable['notifySubscribers'](definition);
}else{
ko.tasks.schedule(function(){
subscribable['notifySubscribers'](definition);
});
}});
completedAsync=true;
}else{
subscribable.subscribe(callback);
}}
function beginLoadingComponent(componentName, callback){
getFirstResultFromLoaders('getConfig', [componentName], function(config){
if(config){
getFirstResultFromLoaders('loadComponent', [componentName, config], function(definition){
callback(definition, config);
});
}else{
callback(null, null);
}});
}
function getFirstResultFromLoaders(methodName, argsExceptCallback, callback, candidateLoaders){
if(!candidateLoaders){
candidateLoaders=ko.components['loaders'].slice(0);
}
var currentCandidateLoader=candidateLoaders.shift();
if(currentCandidateLoader){
var methodInstance=currentCandidateLoader[methodName];
if(methodInstance){
var wasAborted=false,
synchronousReturnValue=methodInstance.apply(currentCandidateLoader, argsExceptCallback.concat(function(result){
if(wasAborted){
callback(null);
}else if(result!==null){
callback(result);
}else{
getFirstResultFromLoaders(methodName, argsExceptCallback, callback, candidateLoaders);
}}));
if(synchronousReturnValue!==undefined){
wasAborted=true;
if(!currentCandidateLoader['suppressLoaderExceptions']){
throw new Error('Component loaders must supply values by invoking the callback, not by returning values synchronously.');
}}
}else{
getFirstResultFromLoaders(methodName, argsExceptCallback, callback, candidateLoaders);
}}else{
callback(null);
}}
ko.components['loaders']=[];
ko.exportSymbol('components', ko.components);
ko.exportSymbol('components.get', ko.components.get);
ko.exportSymbol('components.clearCachedDefinition', ko.components.clearCachedDefinition);
})();
(function(undefined){
var defaultConfigRegistry={};
ko.components.register=function(componentName, config){
if(!config){
throw new Error('Invalid configuration for ' + componentName);
}
if(ko.components.isRegistered(componentName)){
throw new Error('Component ' + componentName + ' is already registered');
}
defaultConfigRegistry[componentName]=config;
};
ko.components.isRegistered=function(componentName){
return defaultConfigRegistry.hasOwnProperty(componentName);
};
ko.components.unregister=function(componentName){
delete defaultConfigRegistry[componentName];
ko.components.clearCachedDefinition(componentName);
};
ko.components.defaultLoader={
'getConfig': function(componentName, callback){
var result=defaultConfigRegistry.hasOwnProperty(componentName)
? defaultConfigRegistry[componentName]
: null;
callback(result);
},
'loadComponent': function(componentName, config, callback){
var errorCallback=makeErrorCallback(componentName);
possiblyGetConfigFromAmd(errorCallback, config, function(loadedConfig){
resolveConfig(componentName, errorCallback, loadedConfig, callback);
});
},
'loadTemplate': function(componentName, templateConfig, callback){
resolveTemplate(makeErrorCallback(componentName), templateConfig, callback);
},
'loadViewModel': function(componentName, viewModelConfig, callback){
resolveViewModel(makeErrorCallback(componentName), viewModelConfig, callback);
}};
var createViewModelKey='createViewModel';
function resolveConfig(componentName, errorCallback, config, callback){
var result={},
makeCallBackWhenZero=2,
tryIssueCallback=function(){
if(--makeCallBackWhenZero===0){
callback(result);
}},
templateConfig=config['template'],
viewModelConfig=config['viewModel'];
if(templateConfig){
possiblyGetConfigFromAmd(errorCallback, templateConfig, function(loadedConfig){
ko.components._getFirstResultFromLoaders('loadTemplate', [componentName, loadedConfig], function(resolvedTemplate){
result['template']=resolvedTemplate;
tryIssueCallback();
});
});
}else{
tryIssueCallback();
}
if(viewModelConfig){
possiblyGetConfigFromAmd(errorCallback, viewModelConfig, function(loadedConfig){
ko.components._getFirstResultFromLoaders('loadViewModel', [componentName, loadedConfig], function(resolvedViewModel){
result[createViewModelKey]=resolvedViewModel;
tryIssueCallback();
});
});
}else{
tryIssueCallback();
}}
function resolveTemplate(errorCallback, templateConfig, callback){
if(typeof templateConfig==='string'){
callback(ko.utils.parseHtmlFragment(templateConfig));
}else if(templateConfig instanceof Array){
callback(templateConfig);
}else if(isDocumentFragment(templateConfig)){
callback(ko.utils.makeArray(templateConfig.childNodes));
}else if(templateConfig['element']){
var element=templateConfig['element'];
if(isDomElement(element)){
callback(cloneNodesFromTemplateSourceElement(element));
}else if(typeof element==='string'){
var elemInstance=document.getElementById(element);
if(elemInstance){
callback(cloneNodesFromTemplateSourceElement(elemInstance));
}else{
errorCallback('Cannot find element with ID ' + element);
}}else{
errorCallback('Unknown element type: ' + element);
}}else{
errorCallback('Unknown template value: ' + templateConfig);
}}
function resolveViewModel(errorCallback, viewModelConfig, callback){
if(typeof viewModelConfig==='function'){
callback(function (params ){
return new viewModelConfig(params);
});
}else if(typeof viewModelConfig[createViewModelKey]==='function'){
callback(viewModelConfig[createViewModelKey]);
}else if('instance' in viewModelConfig){
var fixedInstance=viewModelConfig['instance'];
callback(function (params, componentInfo){
return fixedInstance;
});
}else if('viewModel' in viewModelConfig){
resolveViewModel(errorCallback, viewModelConfig['viewModel'], callback);
}else{
errorCallback('Unknown viewModel value: ' + viewModelConfig);
}}
function cloneNodesFromTemplateSourceElement(elemInstance){
switch (ko.utils.tagNameLower(elemInstance)){
case 'script':
return ko.utils.parseHtmlFragment(elemInstance.text);
case 'textarea':
return ko.utils.parseHtmlFragment(elemInstance.value);
case 'template':
if(isDocumentFragment(elemInstance.content)){
return ko.utils.cloneNodes(elemInstance.content.childNodes);
}}
return ko.utils.cloneNodes(elemInstance.childNodes);
}
function isDomElement(obj){
if(window['HTMLElement']){
return obj instanceof HTMLElement;
}else{
return obj&&obj.tagName&&obj.nodeType===1;
}}
function isDocumentFragment(obj){
if(window['DocumentFragment']){
return obj instanceof DocumentFragment;
}else{
return obj&&obj.nodeType===11;
}}
function possiblyGetConfigFromAmd(errorCallback, config, callback){
if(typeof config['require']==='string'){
if(amdRequire||window['require']){
(amdRequire||window['require'])([config['require']], callback);
}else{
errorCallback('Uses require, but no AMD loader is present');
}}else{
callback(config);
}}
function makeErrorCallback(componentName){
return function (message){
throw new Error('Component \'' + componentName + '\': ' + message);
};}
ko.exportSymbol('components.register', ko.components.register);
ko.exportSymbol('components.isRegistered', ko.components.isRegistered);
ko.exportSymbol('components.unregister', ko.components.unregister);
ko.exportSymbol('components.defaultLoader', ko.components.defaultLoader);
ko.components['loaders'].push(ko.components.defaultLoader);
ko.components._allRegisteredComponents=defaultConfigRegistry;
})();
(function (undefined){
ko.components['getComponentNameForNode']=function(node){
var tagNameLower=ko.utils.tagNameLower(node);
if(ko.components.isRegistered(tagNameLower)){
if(tagNameLower.indexOf('-')!=-1||('' + node)=="[object HTMLUnknownElement]"||(ko.utils.ieVersion <=8&&node.tagName===tagNameLower)){
return tagNameLower;
}}
};
ko.components.addBindingsForCustomElement=function(allBindings, node, bindingContext, valueAccessors){
if(node.nodeType===1){
var componentName=ko.components['getComponentNameForNode'](node);
if(componentName){
allBindings=allBindings||{};
if(allBindings['component']){
throw new Error('Cannot use the "component" binding on a custom element matching a component');
}
var componentBindingValue={ 'name': componentName, 'params': getComponentParamsFromCustomElement(node, bindingContext) };
allBindings['component']=valueAccessors
? function(){ return componentBindingValue; }
: componentBindingValue;
}}
return allBindings;
}
var nativeBindingProviderInstance=new ko.bindingProvider();
function getComponentParamsFromCustomElement(elem, bindingContext){
var paramsAttribute=elem.getAttribute('params');
if(paramsAttribute){
var params=nativeBindingProviderInstance['parseBindingsString'](paramsAttribute, bindingContext, elem, { 'valueAccessors': true, 'bindingParams': true }),
rawParamComputedValues=ko.utils.objectMap(params, function(paramValue, paramName){
return ko.computed(paramValue, null, { disposeWhenNodeIsRemoved: elem });
}),
result=ko.utils.objectMap(rawParamComputedValues, function(paramValueComputed, paramName){
var paramValue=paramValueComputed.peek();
if(!paramValueComputed.isActive()){
return paramValue;
}else{
return ko.computed({
'read': function(){
return ko.utils.unwrapObservable(paramValueComputed());
},
'write': ko.isWriteableObservable(paramValue)&&function(value){
paramValueComputed()(value);
},
disposeWhenNodeIsRemoved: elem
});
}});
if(!result.hasOwnProperty('$raw')){
result['$raw']=rawParamComputedValues;
}
return result;
}else{
return { '$raw': {}};}}
if(ko.utils.ieVersion < 9){
ko.components['register']=(function(originalFunction){
return function(componentName){
document.createElement(componentName);
return originalFunction.apply(this, arguments);
}})(ko.components['register']);
document.createDocumentFragment=(function(originalFunction){
return function(){
var newDocFrag=originalFunction(),
allComponents=ko.components._allRegisteredComponents;
for (var componentName in allComponents){
if(allComponents.hasOwnProperty(componentName)){
newDocFrag.createElement(componentName);
}}
return newDocFrag;
};})(document.createDocumentFragment);
}})();(function(undefined){
var componentLoadingOperationUniqueId=0;
ko.bindingHandlers['component']={
'init': function(element, valueAccessor, ignored1, ignored2, bindingContext){
var currentViewModel,
currentLoadingOperationId,
disposeAssociatedComponentViewModel=function (){
var currentViewModelDispose=currentViewModel&&currentViewModel['dispose'];
if(typeof currentViewModelDispose==='function'){
currentViewModelDispose.call(currentViewModel);
}
currentViewModel=null;
currentLoadingOperationId=null;
},
originalChildNodes=ko.utils.makeArray(ko.virtualElements.childNodes(element));
ko.utils.domNodeDisposal.addDisposeCallback(element, disposeAssociatedComponentViewModel);
ko.computed(function (){
var value=ko.utils.unwrapObservable(valueAccessor()),
componentName, componentParams;
if(typeof value==='string'){
componentName=value;
}else{
componentName=ko.utils.unwrapObservable(value['name']);
componentParams=ko.utils.unwrapObservable(value['params']);
}
if(!componentName){
throw new Error('No component name specified');
}
var loadingOperationId=currentLoadingOperationId=++componentLoadingOperationUniqueId;
ko.components.get(componentName, function(componentDefinition){
if(currentLoadingOperationId!==loadingOperationId){
return;
}
disposeAssociatedComponentViewModel();
if(!componentDefinition){
throw new Error('Unknown component \'' + componentName + '\'');
}
cloneTemplateIntoElement(componentName, componentDefinition, element);
var componentViewModel=createViewModel(componentDefinition, element, originalChildNodes, componentParams),
childBindingContext=bindingContext['createChildContext'](componentViewModel,  undefined, function(ctx){
ctx['$component']=componentViewModel;
ctx['$componentTemplateNodes']=originalChildNodes;
});
currentViewModel=componentViewModel;
ko.applyBindingsToDescendants(childBindingContext, element);
});
}, null, { disposeWhenNodeIsRemoved: element });
return { 'controlsDescendantBindings': true };}};
ko.virtualElements.allowedBindings['component']=true;
function cloneTemplateIntoElement(componentName, componentDefinition, element){
var template=componentDefinition['template'];
if(!template){
throw new Error('Component \'' + componentName + '\' has no template');
}
var clonedNodesArray=ko.utils.cloneNodes(template);
ko.virtualElements.setDomNodeChildren(element, clonedNodesArray);
}
function createViewModel(componentDefinition, element, originalChildNodes, componentParams){
var componentViewModelFactory=componentDefinition['createViewModel'];
return componentViewModelFactory
? componentViewModelFactory.call(componentDefinition, componentParams, { 'element': element, 'templateNodes': originalChildNodes })
: componentParams;
}})();
var attrHtmlToJavascriptMap={ 'class': 'className', 'for': 'htmlFor' };
ko.bindingHandlers['attr']={
'update': function(element, valueAccessor, allBindings){
var value=ko.utils.unwrapObservable(valueAccessor())||{};
ko.utils.objectForEach(value, function(attrName, attrValue){
attrValue=ko.utils.unwrapObservable(attrValue);
var toRemove=(attrValue===false)||(attrValue===null)||(attrValue===undefined);
if(toRemove)
element.removeAttribute(attrName);
if(ko.utils.ieVersion <=8&&attrName in attrHtmlToJavascriptMap){
attrName=attrHtmlToJavascriptMap[attrName];
if(toRemove)
element.removeAttribute(attrName);
else
element[attrName]=attrValue;
}else if(!toRemove){
element.setAttribute(attrName, attrValue.toString());
}
if(attrName==="name"){
ko.utils.setElementName(element, toRemove ? "":attrValue.toString());
}});
}};
(function(){
ko.bindingHandlers['checked']={
'after': ['value', 'attr'],
'init': function (element, valueAccessor, allBindings){
var checkedValue=ko.pureComputed(function(){
if(allBindings['has']('checkedValue')){
return ko.utils.unwrapObservable(allBindings.get('checkedValue'));
}else if(allBindings['has']('value')){
return ko.utils.unwrapObservable(allBindings.get('value'));
}
return element.value;
});
function updateModel(){
var isChecked=element.checked,
elemValue=useCheckedValue ? checkedValue():isChecked;
if(ko.computedContext.isInitial()){
return;
}
if(isRadio&&!isChecked){
return;
}
var modelValue=ko.dependencyDetection.ignore(valueAccessor);
if(valueIsArray){
var writableValue=rawValueIsNonArrayObservable ? modelValue.peek():modelValue;
if(oldElemValue!==elemValue){
if(isChecked){
ko.utils.addOrRemoveItem(writableValue, elemValue, true);
ko.utils.addOrRemoveItem(writableValue, oldElemValue, false);
}
oldElemValue=elemValue;
}else{
ko.utils.addOrRemoveItem(writableValue, elemValue, isChecked);
}
if(rawValueIsNonArrayObservable&&ko.isWriteableObservable(modelValue)){
modelValue(writableValue);
}}else{
ko.expressionRewriting.writeValueToProperty(modelValue, allBindings, 'checked', elemValue, true);
}};
function updateView(){
var modelValue=ko.utils.unwrapObservable(valueAccessor());
if(valueIsArray){
element.checked=ko.utils.arrayIndexOf(modelValue, checkedValue()) >=0;
}else if(isCheckbox){
element.checked=modelValue;
}else{
element.checked=(checkedValue()===modelValue);
}};
var isCheckbox=element.type=="checkbox",
isRadio=element.type=="radio";
if(!isCheckbox&&!isRadio){
return;
}
var rawValue=valueAccessor(),
valueIsArray=isCheckbox&&(ko.utils.unwrapObservable(rawValue) instanceof Array),
rawValueIsNonArrayObservable = !(valueIsArray&&rawValue.push&&rawValue.splice),
oldElemValue=valueIsArray ? checkedValue():undefined,
useCheckedValue=isRadio||valueIsArray;
if(isRadio&&!element.name)
ko.bindingHandlers['uniqueName']['init'](element, function(){ return true });
ko.computed(updateModel, null, { disposeWhenNodeIsRemoved: element });
ko.utils.registerEventHandler(element, "click", updateModel);
ko.computed(updateView, null, { disposeWhenNodeIsRemoved: element });
rawValue=undefined;
}};
ko.expressionRewriting.twoWayBindings['checked']=true;
ko.bindingHandlers['checkedValue']={
'update': function (element, valueAccessor){
element.value=ko.utils.unwrapObservable(valueAccessor());
}};})();var classesWrittenByBindingKey='__ko__cssValue';
ko.bindingHandlers['css']={
'update': function (element, valueAccessor){
var value=ko.utils.unwrapObservable(valueAccessor());
if(value!==null&&typeof value=="object"){
ko.utils.objectForEach(value, function(className, shouldHaveClass){
shouldHaveClass=ko.utils.unwrapObservable(shouldHaveClass);
ko.utils.toggleDomNodeCssClass(element, className, shouldHaveClass);
});
}else{
value=ko.utils.stringTrim(String(value||''));
ko.utils.toggleDomNodeCssClass(element, element[classesWrittenByBindingKey], false);
element[classesWrittenByBindingKey]=value;
ko.utils.toggleDomNodeCssClass(element, value, true);
}}
};
ko.bindingHandlers['enable']={
'update': function (element, valueAccessor){
var value=ko.utils.unwrapObservable(valueAccessor());
if(value&&element.disabled)
element.removeAttribute("disabled");
else if((!value)&&(!element.disabled))
element.disabled=true;
}};
ko.bindingHandlers['disable']={
'update': function (element, valueAccessor){
ko.bindingHandlers['enable']['update'](element, function(){ return !ko.utils.unwrapObservable(valueAccessor()) });
}};
function makeEventHandlerShortcut(eventName){
ko.bindingHandlers[eventName]={
'init': function(element, valueAccessor, allBindings, viewModel, bindingContext){
var newValueAccessor=function (){
var result={};
result[eventName]=valueAccessor();
return result;
};
return ko.bindingHandlers['event']['init'].call(this, element, newValueAccessor, allBindings, viewModel, bindingContext);
}}
}
ko.bindingHandlers['event']={
'init':function (element, valueAccessor, allBindings, viewModel, bindingContext){
var eventsToHandle=valueAccessor()||{};
ko.utils.objectForEach(eventsToHandle, function(eventName){
if(typeof eventName=="string"){
ko.utils.registerEventHandler(element, eventName, function (event){
var handlerReturnValue;
var handlerFunction=valueAccessor()[eventName];
if(!handlerFunction)
return;
try {
var argsForHandler=ko.utils.makeArray(arguments);
viewModel=bindingContext['$data'];
argsForHandler.unshift(viewModel);
handlerReturnValue=handlerFunction.apply(viewModel, argsForHandler);
} finally {
if(handlerReturnValue!==true){
if(event.preventDefault)
event.preventDefault();
else
event.returnValue=false;
}}
var bubble=allBindings.get(eventName + 'Bubble')!==false;
if(!bubble){
event.cancelBubble=true;
if(event.stopPropagation)
event.stopPropagation();
}});
}});
}};
ko.bindingHandlers['foreach']={
makeTemplateValueAccessor: function(valueAccessor){
return function(){
var modelValue=valueAccessor(),
unwrappedValue=ko.utils.peekObservable(modelValue);
if((!unwrappedValue)||typeof unwrappedValue.length=="number")
return { 'foreach': modelValue, 'templateEngine': ko.nativeTemplateEngine.instance };
ko.utils.unwrapObservable(modelValue);
return {
'foreach': unwrappedValue['data'],
'as': unwrappedValue['as'],
'includeDestroyed': unwrappedValue['includeDestroyed'],
'afterAdd': unwrappedValue['afterAdd'],
'beforeRemove': unwrappedValue['beforeRemove'],
'afterRender': unwrappedValue['afterRender'],
'beforeMove': unwrappedValue['beforeMove'],
'afterMove': unwrappedValue['afterMove'],
'templateEngine': ko.nativeTemplateEngine.instance
};};
},
'init': function(element, valueAccessor, allBindings, viewModel, bindingContext){
return ko.bindingHandlers['template']['init'](element, ko.bindingHandlers['foreach'].makeTemplateValueAccessor(valueAccessor));
},
'update': function(element, valueAccessor, allBindings, viewModel, bindingContext){
return ko.bindingHandlers['template']['update'](element, ko.bindingHandlers['foreach'].makeTemplateValueAccessor(valueAccessor), allBindings, viewModel, bindingContext);
}};
ko.expressionRewriting.bindingRewriteValidators['foreach']=false;
ko.virtualElements.allowedBindings['foreach']=true;
var hasfocusUpdatingProperty='__ko_hasfocusUpdating';
var hasfocusLastValue='__ko_hasfocusLastValue';
ko.bindingHandlers['hasfocus']={
'init': function(element, valueAccessor, allBindings){
var handleElementFocusChange=function(isFocused){
element[hasfocusUpdatingProperty]=true;
var ownerDoc=element.ownerDocument;
if("activeElement" in ownerDoc){
var active;
try {
active=ownerDoc.activeElement;
} catch(e){
active=ownerDoc.body;
}
isFocused=(active===element);
}
var modelValue=valueAccessor();
ko.expressionRewriting.writeValueToProperty(modelValue, allBindings, 'hasfocus', isFocused, true);
element[hasfocusLastValue]=isFocused;
element[hasfocusUpdatingProperty]=false;
};
var handleElementFocusIn=handleElementFocusChange.bind(null, true);
var handleElementFocusOut=handleElementFocusChange.bind(null, false);
ko.utils.registerEventHandler(element, "focus", handleElementFocusIn);
ko.utils.registerEventHandler(element, "focusin", handleElementFocusIn);
ko.utils.registerEventHandler(element, "blur",  handleElementFocusOut);
ko.utils.registerEventHandler(element, "focusout",  handleElementFocusOut);
},
'update': function(element, valueAccessor){
var value = !!ko.utils.unwrapObservable(valueAccessor());
if(!element[hasfocusUpdatingProperty]&&element[hasfocusLastValue]!==value){
value ? element.focus():element.blur();
if(!value&&element[hasfocusLastValue]){
element.ownerDocument.body.focus();
}
ko.dependencyDetection.ignore(ko.utils.triggerEvent, null, [element, value ? "focusin":"focusout"]);
}}
};
ko.expressionRewriting.twoWayBindings['hasfocus']=true;
ko.bindingHandlers['hasFocus']=ko.bindingHandlers['hasfocus'];
ko.expressionRewriting.twoWayBindings['hasFocus']=true;
ko.bindingHandlers['html']={
'init': function(){
return { 'controlsDescendantBindings': true };},
'update': function (element, valueAccessor){
ko.utils.setHtml(element, valueAccessor());
}};
function makeWithIfBinding(bindingKey, isWith, isNot, makeContextCallback){
ko.bindingHandlers[bindingKey]={
'init': function(element, valueAccessor, allBindings, viewModel, bindingContext){
var didDisplayOnLastUpdate,
savedNodes;
ko.computed(function(){
var rawValue=valueAccessor(),
dataValue=ko.utils.unwrapObservable(rawValue),
shouldDisplay = !isNot!==!dataValue,
isFirstRender = !savedNodes,
needsRefresh=isFirstRender||isWith||(shouldDisplay!==didDisplayOnLastUpdate);
if(needsRefresh){
if(isFirstRender&&ko.computedContext.getDependenciesCount()){
savedNodes=ko.utils.cloneNodes(ko.virtualElements.childNodes(element), true );
}
if(shouldDisplay){
if(!isFirstRender){
ko.virtualElements.setDomNodeChildren(element, ko.utils.cloneNodes(savedNodes));
}
ko.applyBindingsToDescendants(makeContextCallback ? makeContextCallback(bindingContext, rawValue):bindingContext, element);
}else{
ko.virtualElements.emptyNode(element);
}
didDisplayOnLastUpdate=shouldDisplay;
}}, null, { disposeWhenNodeIsRemoved: element });
return { 'controlsDescendantBindings': true };}};
ko.expressionRewriting.bindingRewriteValidators[bindingKey]=false;
ko.virtualElements.allowedBindings[bindingKey]=true;
}
makeWithIfBinding('if');
makeWithIfBinding('ifnot', false , true );
makeWithIfBinding('with', true , false ,
function(bindingContext, dataValue){
return bindingContext.createStaticChildContext(dataValue);
}
);
var captionPlaceholder={};
ko.bindingHandlers['options']={
'init': function(element){
if(ko.utils.tagNameLower(element)!=="select")
throw new Error("options binding applies only to SELECT elements");
while (element.length > 0){
element.remove(0);
}
return { 'controlsDescendantBindings': true };},
'update': function (element, valueAccessor, allBindings){
function selectedOptions(){
return ko.utils.arrayFilter(element.options, function (node){ return node.selected; });
}
var selectWasPreviouslyEmpty=element.length==0,
multiple=element.multiple,
previousScrollTop=(!selectWasPreviouslyEmpty&&multiple) ? element.scrollTop:null,
unwrappedArray=ko.utils.unwrapObservable(valueAccessor()),
valueAllowUnset=allBindings.get('valueAllowUnset')&&allBindings['has']('value'),
includeDestroyed=allBindings.get('optionsIncludeDestroyed'),
arrayToDomNodeChildrenOptions={},
captionValue,
filteredArray,
previousSelectedValues=[];
if(!valueAllowUnset){
if(multiple){
previousSelectedValues=ko.utils.arrayMap(selectedOptions(), ko.selectExtensions.readValue);
}else if(element.selectedIndex >=0){
previousSelectedValues.push(ko.selectExtensions.readValue(element.options[element.selectedIndex]));
}}
if(unwrappedArray){
if(typeof unwrappedArray.length=="undefined")
unwrappedArray=[unwrappedArray];
filteredArray=ko.utils.arrayFilter(unwrappedArray, function(item){
return includeDestroyed||item===undefined||item===null||!ko.utils.unwrapObservable(item['_destroy']);
});
if(allBindings['has']('optionsCaption')){
captionValue=ko.utils.unwrapObservable(allBindings.get('optionsCaption'));
if(captionValue!==null&&captionValue!==undefined){
filteredArray.unshift(captionPlaceholder);
}}
}else{
}
function applyToObject(object, predicate, defaultValue){
var predicateType=typeof predicate;
if(predicateType=="function")
return predicate(object);
else if(predicateType=="string")
return object[predicate];
else
return defaultValue;
}
var itemUpdate=false;
function optionForArrayItem(arrayEntry, index, oldOptions){
if(oldOptions.length){
previousSelectedValues = !valueAllowUnset&&oldOptions[0].selected ? [ ko.selectExtensions.readValue(oldOptions[0]) ]:[];
itemUpdate=true;
}
var option=element.ownerDocument.createElement("option");
if(arrayEntry===captionPlaceholder){
ko.utils.setTextContent(option, allBindings.get('optionsCaption'));
ko.selectExtensions.writeValue(option, undefined);
}else{
var optionValue=applyToObject(arrayEntry, allBindings.get('optionsValue'), arrayEntry);
ko.selectExtensions.writeValue(option, ko.utils.unwrapObservable(optionValue));
var optionText=applyToObject(arrayEntry, allBindings.get('optionsText'), optionValue);
ko.utils.setTextContent(option, optionText);
}
return [option];
}
arrayToDomNodeChildrenOptions['beforeRemove'] =
function (option){
element.removeChild(option);
};
function setSelectionCallback(arrayEntry, newOptions){
if(itemUpdate&&valueAllowUnset){
ko.selectExtensions.writeValue(element, ko.utils.unwrapObservable(allBindings.get('value')), true );
}else if(previousSelectedValues.length){
var isSelected=ko.utils.arrayIndexOf(previousSelectedValues, ko.selectExtensions.readValue(newOptions[0])) >=0;
ko.utils.setOptionNodeSelectionState(newOptions[0], isSelected);
if(itemUpdate&&!isSelected){
ko.dependencyDetection.ignore(ko.utils.triggerEvent, null, [element, "change"]);
}}
}
var callback=setSelectionCallback;
if(allBindings['has']('optionsAfterRender')&&typeof allBindings.get('optionsAfterRender')=="function"){
callback=function(arrayEntry, newOptions){
setSelectionCallback(arrayEntry, newOptions);
ko.dependencyDetection.ignore(allBindings.get('optionsAfterRender'), null, [newOptions[0], arrayEntry!==captionPlaceholder ? arrayEntry:undefined]);
}}
ko.utils.setDomNodeChildrenFromArrayMapping(element, filteredArray, optionForArrayItem, arrayToDomNodeChildrenOptions, callback);
ko.dependencyDetection.ignore(function (){
if(valueAllowUnset){
ko.selectExtensions.writeValue(element, ko.utils.unwrapObservable(allBindings.get('value')), true );
}else{
var selectionChanged;
if(multiple){
selectionChanged=previousSelectedValues.length&&selectedOptions().length < previousSelectedValues.length;
}else{
selectionChanged=(previousSelectedValues.length&&element.selectedIndex >=0)
? (ko.selectExtensions.readValue(element.options[element.selectedIndex])!==previousSelectedValues[0])
: (previousSelectedValues.length||element.selectedIndex >=0);
}
if(selectionChanged){
ko.utils.triggerEvent(element, "change");
}}
});
ko.utils.ensureSelectElementIsRenderedCorrectly(element);
if(previousScrollTop&&Math.abs(previousScrollTop - element.scrollTop) > 20)
element.scrollTop=previousScrollTop;
}};
ko.bindingHandlers['options'].optionValueDomDataKey=ko.utils.domData.nextKey();
ko.bindingHandlers['selectedOptions']={
'after': ['options', 'foreach'],
'init': function (element, valueAccessor, allBindings){
ko.utils.registerEventHandler(element, "change", function (){
var value=valueAccessor(), valueToWrite=[];
ko.utils.arrayForEach(element.getElementsByTagName("option"), function(node){
if(node.selected)
valueToWrite.push(ko.selectExtensions.readValue(node));
});
ko.expressionRewriting.writeValueToProperty(value, allBindings, 'selectedOptions', valueToWrite);
});
},
'update': function (element, valueAccessor){
if(ko.utils.tagNameLower(element)!="select")
throw new Error("values binding applies only to SELECT elements");
var newValue=ko.utils.unwrapObservable(valueAccessor()),
previousScrollTop=element.scrollTop;
if(newValue&&typeof newValue.length=="number"){
ko.utils.arrayForEach(element.getElementsByTagName("option"), function(node){
var isSelected=ko.utils.arrayIndexOf(newValue, ko.selectExtensions.readValue(node)) >=0;
if(node.selected!=isSelected){
ko.utils.setOptionNodeSelectionState(node, isSelected);
}});
}
element.scrollTop=previousScrollTop;
}};
ko.expressionRewriting.twoWayBindings['selectedOptions']=true;
ko.bindingHandlers['style']={
'update': function (element, valueAccessor){
var value=ko.utils.unwrapObservable(valueAccessor()||{});
ko.utils.objectForEach(value, function(styleName, styleValue){
styleValue=ko.utils.unwrapObservable(styleValue);
if(styleValue===null||styleValue===undefined||styleValue===false){
styleValue="";
}
element.style[styleName]=styleValue;
});
}};
ko.bindingHandlers['submit']={
'init': function (element, valueAccessor, allBindings, viewModel, bindingContext){
if(typeof valueAccessor()!="function")
throw new Error("The value for a submit binding must be a function");
ko.utils.registerEventHandler(element, "submit", function (event){
var handlerReturnValue;
var value=valueAccessor();
try { handlerReturnValue=value.call(bindingContext['$data'], element); }
finally {
if(handlerReturnValue!==true){
if(event.preventDefault)
event.preventDefault();
else
event.returnValue=false;
}}
});
}};
ko.bindingHandlers['text']={
'init': function(){
return { 'controlsDescendantBindings': true };},
'update': function (element, valueAccessor){
ko.utils.setTextContent(element, valueAccessor());
}};
ko.virtualElements.allowedBindings['text']=true;
(function (){
if(window&&window.navigator){
var parseVersion=function (matches){
if(matches){
return parseFloat(matches[1]);
}};
var operaVersion=window.opera&&window.opera.version&&parseInt(window.opera.version()),
userAgent=window.navigator.userAgent,
safariVersion=parseVersion(userAgent.match(/^(?:(?!chrome).)*version\/([^ ]*) safari/i)),
firefoxVersion=parseVersion(userAgent.match(/Firefox\/([^ ]*)/));
}
if(ko.utils.ieVersion < 10){
var selectionChangeRegisteredName=ko.utils.domData.nextKey(),
selectionChangeHandlerName=ko.utils.domData.nextKey();
var selectionChangeHandler=function(event){
var target=this.activeElement,
handler=target&&ko.utils.domData.get(target, selectionChangeHandlerName);
if(handler){
handler(event);
}};
var registerForSelectionChangeEvent=function (element, handler){
var ownerDoc=element.ownerDocument;
if(!ko.utils.domData.get(ownerDoc, selectionChangeRegisteredName)){
ko.utils.domData.set(ownerDoc, selectionChangeRegisteredName, true);
ko.utils.registerEventHandler(ownerDoc, 'selectionchange', selectionChangeHandler);
}
ko.utils.domData.set(element, selectionChangeHandlerName, handler);
};}
ko.bindingHandlers['textInput']={
'init': function (element, valueAccessor, allBindings){
var previousElementValue=element.value,
timeoutHandle,
elementValueBeforeEvent;
var updateModel=function (event){
clearTimeout(timeoutHandle);
elementValueBeforeEvent=timeoutHandle=undefined;
var elementValue=element.value;
if(previousElementValue!==elementValue){
if(DEBUG&&event) element['_ko_textInputProcessedEvent']=event.type;
previousElementValue=elementValue;
ko.expressionRewriting.writeValueToProperty(valueAccessor(), allBindings, 'textInput', elementValue);
}};
var deferUpdateModel=function (event){
if(!timeoutHandle){
elementValueBeforeEvent=element.value;
var handler=DEBUG ? updateModel.bind(element, {type: event.type}):updateModel;
timeoutHandle=ko.utils.setTimeout(handler, 4);
}};
var ieUpdateModel=ko.utils.ieVersion==9 ? deferUpdateModel:updateModel;
var updateView=function (){
var modelValue=ko.utils.unwrapObservable(valueAccessor());
if(modelValue===null||modelValue===undefined){
modelValue='';
}
if(elementValueBeforeEvent!==undefined&&modelValue===elementValueBeforeEvent){
ko.utils.setTimeout(updateView, 4);
return;
}
if(element.value!==modelValue){
previousElementValue=modelValue;
element.value=modelValue;
}};
var onEvent=function (event, handler){
ko.utils.registerEventHandler(element, event, handler);
};
if(DEBUG&&ko.bindingHandlers['textInput']['_forceUpdateOn']){
ko.utils.arrayForEach(ko.bindingHandlers['textInput']['_forceUpdateOn'], function(eventName){
if(eventName.slice(0,5)=='after'){
onEvent(eventName.slice(5), deferUpdateModel);
}else{
onEvent(eventName, updateModel);
}});
}else{
if(ko.utils.ieVersion < 10){
onEvent('propertychange', function(event){
if(event.propertyName==='value'){
ieUpdateModel(event);
}});
if(ko.utils.ieVersion==8){
onEvent('keyup', updateModel);
onEvent('keydown', updateModel);
}
if(ko.utils.ieVersion >=8){
registerForSelectionChangeEvent(element, ieUpdateModel);  // 'selectionchange' covers cut, paste, drop, delete, etc.
onEvent('dragend', deferUpdateModel);
}}else{
onEvent('input', updateModel);
if(safariVersion < 5&&ko.utils.tagNameLower(element)==="textarea"){
onEvent('keydown', deferUpdateModel);
onEvent('paste', deferUpdateModel);
onEvent('cut', deferUpdateModel);
}else if(operaVersion < 11){
onEvent('keydown', deferUpdateModel);
}else if(firefoxVersion < 4.0){
onEvent('DOMAutoComplete', updateModel);
onEvent('dragdrop', updateModel);
onEvent('drop', updateModel);
}}
}
onEvent('change', updateModel);
ko.computed(updateView, null, { disposeWhenNodeIsRemoved: element });
}};
ko.expressionRewriting.twoWayBindings['textInput']=true;
ko.bindingHandlers['textinput']={
'preprocess': function (value, name, addBinding){
addBinding('textInput', value);
}};})();ko.bindingHandlers['uniqueName']={
'init': function (element, valueAccessor){
if(valueAccessor()){
var name="ko_unique_" + (++ko.bindingHandlers['uniqueName'].currentIndex);
ko.utils.setElementName(element, name);
}}
};
ko.bindingHandlers['uniqueName'].currentIndex=0;
ko.bindingHandlers['value']={
'after': ['options', 'foreach'],
'init': function (element, valueAccessor, allBindings){
if(element.tagName.toLowerCase()=="input"&&(element.type=="checkbox"||element.type=="radio")){
ko.applyBindingAccessorsToNode(element, { 'checkedValue': valueAccessor });
return;
}
var eventsToCatch=["change"];
var requestedEventsToCatch=allBindings.get("valueUpdate");
var propertyChangedFired=false;
var elementValueBeforeEvent=null;
if(requestedEventsToCatch){
if(typeof requestedEventsToCatch=="string")
requestedEventsToCatch=[requestedEventsToCatch];
ko.utils.arrayPushAll(eventsToCatch, requestedEventsToCatch);
eventsToCatch=ko.utils.arrayGetDistinctValues(eventsToCatch);
}
var valueUpdateHandler=function(){
elementValueBeforeEvent=null;
propertyChangedFired=false;
var modelValue=valueAccessor();
var elementValue=ko.selectExtensions.readValue(element);
ko.expressionRewriting.writeValueToProperty(modelValue, allBindings, 'value', elementValue);
}
var ieAutoCompleteHackNeeded=ko.utils.ieVersion&&element.tagName.toLowerCase()=="input"&&element.type=="text"
&& element.autocomplete!="off"&&(!element.form||element.form.autocomplete!="off");
if(ieAutoCompleteHackNeeded&&ko.utils.arrayIndexOf(eventsToCatch, "propertychange")==-1){
ko.utils.registerEventHandler(element, "propertychange", function (){ propertyChangedFired=true });
ko.utils.registerEventHandler(element, "focus", function (){ propertyChangedFired=false });
ko.utils.registerEventHandler(element, "blur", function(){
if(propertyChangedFired){
valueUpdateHandler();
}});
}
ko.utils.arrayForEach(eventsToCatch, function(eventName){
var handler=valueUpdateHandler;
if(ko.utils.stringStartsWith(eventName, "after")){
handler=function(){
elementValueBeforeEvent=ko.selectExtensions.readValue(element);
ko.utils.setTimeout(valueUpdateHandler, 0);
};
eventName=eventName.substring("after".length);
}
ko.utils.registerEventHandler(element, eventName, handler);
});
var updateFromModel=function (){
var newValue=ko.utils.unwrapObservable(valueAccessor());
var elementValue=ko.selectExtensions.readValue(element);
if(elementValueBeforeEvent!==null&&newValue===elementValueBeforeEvent){
ko.utils.setTimeout(updateFromModel, 0);
return;
}
var valueHasChanged=(newValue!==elementValue);
if(valueHasChanged){
if(ko.utils.tagNameLower(element)==="select"){
var allowUnset=allBindings.get('valueAllowUnset');
var applyValueAction=function (){
ko.selectExtensions.writeValue(element, newValue, allowUnset);
};
applyValueAction();
if(!allowUnset&&newValue!==ko.selectExtensions.readValue(element)){
ko.dependencyDetection.ignore(ko.utils.triggerEvent, null, [element, "change"]);
}else{
ko.utils.setTimeout(applyValueAction, 0);
}}else{
ko.selectExtensions.writeValue(element, newValue);
}}
};
ko.computed(updateFromModel, null, { disposeWhenNodeIsRemoved: element });
},
'update': function(){}};
ko.expressionRewriting.twoWayBindings['value']=true;
ko.bindingHandlers['visible']={
'update': function (element, valueAccessor){
var value=ko.utils.unwrapObservable(valueAccessor());
var isCurrentlyVisible = !(element.style.display=="none");
if(value&&!isCurrentlyVisible)
element.style.display="";
else if((!value)&&isCurrentlyVisible)
element.style.display="none";
}};
makeEventHandlerShortcut('click');
ko.templateEngine=function (){ };
ko.templateEngine.prototype['renderTemplateSource']=function (templateSource, bindingContext, options, templateDocument){
throw new Error("Override renderTemplateSource");
};
ko.templateEngine.prototype['createJavaScriptEvaluatorBlock']=function (script){
throw new Error("Override createJavaScriptEvaluatorBlock");
};
ko.templateEngine.prototype['makeTemplateSource']=function(template, templateDocument){
if(typeof template=="string"){
templateDocument=templateDocument||document;
var elem=templateDocument.getElementById(template);
if(!elem)
throw new Error("Cannot find template with ID " + template);
return new ko.templateSources.domElement(elem);
}else if((template.nodeType==1)||(template.nodeType==8)){
return new ko.templateSources.anonymousTemplate(template);
} else
throw new Error("Unknown template type: " + template);
};
ko.templateEngine.prototype['renderTemplate']=function (template, bindingContext, options, templateDocument){
var templateSource=this['makeTemplateSource'](template, templateDocument);
return this['renderTemplateSource'](templateSource, bindingContext, options, templateDocument);
};
ko.templateEngine.prototype['isTemplateRewritten']=function (template, templateDocument){
if(this['allowTemplateRewriting']===false)
return true;
return this['makeTemplateSource'](template, templateDocument)['data']("isRewritten");
};
ko.templateEngine.prototype['rewriteTemplate']=function (template, rewriterCallback, templateDocument){
var templateSource=this['makeTemplateSource'](template, templateDocument);
var rewritten=rewriterCallback(templateSource['text']());
templateSource['text'](rewritten);
templateSource['data']("isRewritten", true);
};
ko.exportSymbol('templateEngine', ko.templateEngine);
ko.templateRewriting=(function (){
var memoizeDataBindingAttributeSyntaxRegex=/(<([a-z]+\d*)(?:\s+(?!data-bind\s*=\s*)[a-z0-9\-]+(?:=(?:\"[^\"]*\"|\'[^\']*\'|[^>]*))?)*\s+)data-bind\s*=\s*(["'])([\s\S]*?)\3/gi;
var memoizeVirtualContainerBindingSyntaxRegex=/<!--\s*ko\b\s*([\s\S]*?)\s*-->/g;
function validateDataBindValuesForRewriting(keyValueArray){
var allValidators=ko.expressionRewriting.bindingRewriteValidators;
for (var i=0; i < keyValueArray.length; i++){
var key=keyValueArray[i]['key'];
if(allValidators.hasOwnProperty(key)){
var validator=allValidators[key];
if(typeof validator==="function"){
var possibleErrorMessage=validator(keyValueArray[i]['value']);
if(possibleErrorMessage)
throw new Error(possibleErrorMessage);
}else if(!validator){
throw new Error("This template engine does not support the '" + key + "' binding within its templates");
}}
}}
function constructMemoizedTagReplacement(dataBindAttributeValue, tagToRetain, nodeName, templateEngine){
var dataBindKeyValueArray=ko.expressionRewriting.parseObjectLiteral(dataBindAttributeValue);
validateDataBindValuesForRewriting(dataBindKeyValueArray);
var rewrittenDataBindAttributeValue=ko.expressionRewriting.preProcessBindings(dataBindKeyValueArray, {'valueAccessors':true});
var applyBindingsToNextSiblingScript =
"ko.__tr_ambtns(function($context,$element){return(function(){return{ " + rewrittenDataBindAttributeValue + " }})()},'" + nodeName.toLowerCase() + "')";
return templateEngine['createJavaScriptEvaluatorBlock'](applyBindingsToNextSiblingScript) + tagToRetain;
}
return {
ensureTemplateIsRewritten: function (template, templateEngine, templateDocument){
if(!templateEngine['isTemplateRewritten'](template, templateDocument))
templateEngine['rewriteTemplate'](template, function (htmlString){
return ko.templateRewriting.memoizeBindingAttributeSyntax(htmlString, templateEngine);
}, templateDocument);
},
memoizeBindingAttributeSyntax: function (htmlString, templateEngine){
return htmlString.replace(memoizeDataBindingAttributeSyntaxRegex, function (){
return constructMemoizedTagReplacement( arguments[4],  arguments[1],  arguments[2], templateEngine);
}).replace(memoizeVirtualContainerBindingSyntaxRegex, function(){
return constructMemoizedTagReplacement( arguments[1],  "<!-- ko -->",  "#comment", templateEngine);
});
},
applyMemoizedBindingsToNextSibling: function (bindings, nodeName){
return ko.memoization.memoize(function (domNode, bindingContext){
var nodeToBind=domNode.nextSibling;
if(nodeToBind&&nodeToBind.nodeName.toLowerCase()===nodeName){
ko.applyBindingAccessorsToNode(nodeToBind, bindings, bindingContext);
}});
}}
})();
ko.exportSymbol('__tr_ambtns', ko.templateRewriting.applyMemoizedBindingsToNextSibling);
(function(){
ko.templateSources={};
var templateScript=1,
templateTextArea=2,
templateTemplate=3,
templateElement=4;
ko.templateSources.domElement=function(element){
this.domElement=element;
if(element){
var tagNameLower=ko.utils.tagNameLower(element);
this.templateType =
tagNameLower==="script" ? templateScript :
tagNameLower==="textarea" ? templateTextArea :
tagNameLower=="template"&&element.content&&element.content.nodeType===11 ? templateTemplate :
templateElement;
}}
ko.templateSources.domElement.prototype['text']=function(){
var elemContentsProperty=this.templateType===templateScript ? "text"
: this.templateType===templateTextArea ? "value"
: "innerHTML";
if(arguments.length==0){
return this.domElement[elemContentsProperty];
}else{
var valueToWrite=arguments[0];
if(elemContentsProperty==="innerHTML")
ko.utils.setHtml(this.domElement, valueToWrite);
else
this.domElement[elemContentsProperty]=valueToWrite;
}};
var dataDomDataPrefix=ko.utils.domData.nextKey() + "_";
ko.templateSources.domElement.prototype['data']=function(key ){
if(arguments.length===1){
return ko.utils.domData.get(this.domElement, dataDomDataPrefix + key);
}else{
ko.utils.domData.set(this.domElement, dataDomDataPrefix + key, arguments[1]);
}};
var templatesDomDataKey=ko.utils.domData.nextKey();
function getTemplateDomData(element){
return ko.utils.domData.get(element, templatesDomDataKey)||{};}
function setTemplateDomData(element, data){
ko.utils.domData.set(element, templatesDomDataKey, data);
}
ko.templateSources.domElement.prototype['nodes']=function(){
var element=this.domElement;
if(arguments.length==0){
var templateData=getTemplateDomData(element),
containerData=templateData.containerData;
return containerData||(
this.templateType===templateTemplate ? element.content :
this.templateType===templateElement ? element :
undefined);
}else{
var valueToWrite=arguments[0];
setTemplateDomData(element, {containerData: valueToWrite});
}};
ko.templateSources.anonymousTemplate=function(element){
this.domElement=element;
}
ko.templateSources.anonymousTemplate.prototype=new ko.templateSources.domElement();
ko.templateSources.anonymousTemplate.prototype.constructor=ko.templateSources.anonymousTemplate;
ko.templateSources.anonymousTemplate.prototype['text']=function(){
if(arguments.length==0){
var templateData=getTemplateDomData(this.domElement);
if(templateData.textData===undefined&&templateData.containerData)
templateData.textData=templateData.containerData.innerHTML;
return templateData.textData;
}else{
var valueToWrite=arguments[0];
setTemplateDomData(this.domElement, {textData: valueToWrite});
}};
ko.exportSymbol('templateSources', ko.templateSources);
ko.exportSymbol('templateSources.domElement', ko.templateSources.domElement);
ko.exportSymbol('templateSources.anonymousTemplate', ko.templateSources.anonymousTemplate);
})();
(function (){
var _templateEngine;
ko.setTemplateEngine=function (templateEngine){
if((templateEngine!=undefined)&&!(templateEngine instanceof ko.templateEngine))
throw new Error("templateEngine must inherit from ko.templateEngine");
_templateEngine=templateEngine;
}
function invokeForEachNodeInContinuousRange(firstNode, lastNode, action){
var node, nextInQueue=firstNode, firstOutOfRangeNode=ko.virtualElements.nextSibling(lastNode);
while (nextInQueue&&((node=nextInQueue)!==firstOutOfRangeNode)){
nextInQueue=ko.virtualElements.nextSibling(node);
action(node, nextInQueue);
}}
function activateBindingsOnContinuousNodeArray(continuousNodeArray, bindingContext){
if(continuousNodeArray.length){
var firstNode=continuousNodeArray[0],
lastNode=continuousNodeArray[continuousNodeArray.length - 1],
parentNode=firstNode.parentNode,
provider=ko.bindingProvider['instance'],
preprocessNode=provider['preprocessNode'];
if(preprocessNode){
invokeForEachNodeInContinuousRange(firstNode, lastNode, function(node, nextNodeInRange){
var nodePreviousSibling=node.previousSibling;
var newNodes=preprocessNode.call(provider, node);
if(newNodes){
if(node===firstNode)
firstNode=newNodes[0]||nextNodeInRange;
if(node===lastNode)
lastNode=newNodes[newNodes.length - 1]||nodePreviousSibling;
}});
continuousNodeArray.length=0;
if(!firstNode){
return;
}
if(firstNode===lastNode){
continuousNodeArray.push(firstNode);
}else{
continuousNodeArray.push(firstNode, lastNode);
ko.utils.fixUpContinuousNodeArray(continuousNodeArray, parentNode);
}}
invokeForEachNodeInContinuousRange(firstNode, lastNode, function(node){
if(node.nodeType===1||node.nodeType===8)
ko.applyBindings(bindingContext, node);
});
invokeForEachNodeInContinuousRange(firstNode, lastNode, function(node){
if(node.nodeType===1||node.nodeType===8)
ko.memoization.unmemoizeDomNodeAndDescendants(node, [bindingContext]);
});
ko.utils.fixUpContinuousNodeArray(continuousNodeArray, parentNode);
}}
function getFirstNodeFromPossibleArray(nodeOrNodeArray){
return nodeOrNodeArray.nodeType ? nodeOrNodeArray
: nodeOrNodeArray.length > 0 ? nodeOrNodeArray[0]
: null;
}
function executeTemplate(targetNodeOrNodeArray, renderMode, template, bindingContext, options){
options=options||{};
var firstTargetNode=targetNodeOrNodeArray&&getFirstNodeFromPossibleArray(targetNodeOrNodeArray);
var templateDocument=(firstTargetNode||template||{}).ownerDocument;
var templateEngineToUse=(options['templateEngine']||_templateEngine);
ko.templateRewriting.ensureTemplateIsRewritten(template, templateEngineToUse, templateDocument);
var renderedNodesArray=templateEngineToUse['renderTemplate'](template, bindingContext, options, templateDocument);
if((typeof renderedNodesArray.length!="number")||(renderedNodesArray.length > 0&&typeof renderedNodesArray[0].nodeType!="number"))
throw new Error("Template engine must return an array of DOM nodes");
var haveAddedNodesToParent=false;
switch (renderMode){
case "replaceChildren":
ko.virtualElements.setDomNodeChildren(targetNodeOrNodeArray, renderedNodesArray);
haveAddedNodesToParent=true;
break;
case "replaceNode":
ko.utils.replaceDomNodes(targetNodeOrNodeArray, renderedNodesArray);
haveAddedNodesToParent=true;
break;
case "ignoreTargetNode": break;
default:
throw new Error("Unknown renderMode: " + renderMode);
}
if(haveAddedNodesToParent){
activateBindingsOnContinuousNodeArray(renderedNodesArray, bindingContext);
if(options['afterRender'])
ko.dependencyDetection.ignore(options['afterRender'], null, [renderedNodesArray, bindingContext['$data']]);
}
return renderedNodesArray;
}
function resolveTemplateName(template, data, context){
if(ko.isObservable(template)){
return template();
}else if(typeof template==='function'){
return template(data, context);
}else{
return template;
}}
ko.renderTemplate=function (template, dataOrBindingContext, options, targetNodeOrNodeArray, renderMode){
options=options||{};
if((options['templateEngine']||_templateEngine)==undefined)
throw new Error("Set a template engine before calling renderTemplate");
renderMode=renderMode||"replaceChildren";
if(targetNodeOrNodeArray){
var firstTargetNode=getFirstNodeFromPossibleArray(targetNodeOrNodeArray);
var whenToDispose=function (){ return (!firstTargetNode)||!ko.utils.domNodeIsAttachedToDocument(firstTargetNode); };
var activelyDisposeWhenNodeIsRemoved=(firstTargetNode&&renderMode=="replaceNode") ? firstTargetNode.parentNode:firstTargetNode;
return ko.dependentObservable(function (){
var bindingContext=(dataOrBindingContext&&(dataOrBindingContext instanceof ko.bindingContext))
? dataOrBindingContext
: new ko.bindingContext(dataOrBindingContext, null, null, null, { "exportDependencies": true });
var templateName=resolveTemplateName(template, bindingContext['$data'], bindingContext),
renderedNodesArray=executeTemplate(targetNodeOrNodeArray, renderMode, templateName, bindingContext, options);
if(renderMode=="replaceNode"){
targetNodeOrNodeArray=renderedNodesArray;
firstTargetNode=getFirstNodeFromPossibleArray(targetNodeOrNodeArray);
}},
null,
{ disposeWhen: whenToDispose, disposeWhenNodeIsRemoved: activelyDisposeWhenNodeIsRemoved }
);
}else{
return ko.memoization.memoize(function (domNode){
ko.renderTemplate(template, dataOrBindingContext, options, domNode, "replaceNode");
});
}};
ko.renderTemplateForEach=function (template, arrayOrObservableArray, options, targetNode, parentBindingContext){
var arrayItemContext;
var executeTemplateForArrayItem=function (arrayValue, index){
arrayItemContext=parentBindingContext['createChildContext'](arrayValue, options['as'], function(context){
context['$index']=index;
});
var templateName=resolveTemplateName(template, arrayValue, arrayItemContext);
return executeTemplate(null, "ignoreTargetNode", templateName, arrayItemContext, options);
}
var activateBindingsCallback=function(arrayValue, addedNodesArray, index){
activateBindingsOnContinuousNodeArray(addedNodesArray, arrayItemContext);
if(options['afterRender'])
options['afterRender'](addedNodesArray, arrayValue);
arrayItemContext=null;
};
return ko.dependentObservable(function (){
var unwrappedArray=ko.utils.unwrapObservable(arrayOrObservableArray)||[];
if(typeof unwrappedArray.length=="undefined")
unwrappedArray=[unwrappedArray];
var filteredArray=ko.utils.arrayFilter(unwrappedArray, function(item){
return options['includeDestroyed']||item===undefined||item===null||!ko.utils.unwrapObservable(item['_destroy']);
});
ko.dependencyDetection.ignore(ko.utils.setDomNodeChildrenFromArrayMapping, null, [targetNode, filteredArray, executeTemplateForArrayItem, options, activateBindingsCallback]);
}, null, { disposeWhenNodeIsRemoved: targetNode });
};
var templateComputedDomDataKey=ko.utils.domData.nextKey();
function disposeOldComputedAndStoreNewOne(element, newComputed){
var oldComputed=ko.utils.domData.get(element, templateComputedDomDataKey);
if(oldComputed&&(typeof(oldComputed.dispose)=='function'))
oldComputed.dispose();
ko.utils.domData.set(element, templateComputedDomDataKey, (newComputed&&newComputed.isActive()) ? newComputed:undefined);
}
ko.bindingHandlers['template']={
'init': function(element, valueAccessor){
var bindingValue=ko.utils.unwrapObservable(valueAccessor());
if(typeof bindingValue=="string"||bindingValue['name']){
ko.virtualElements.emptyNode(element);
}else if('nodes' in bindingValue){
var nodes=bindingValue['nodes']||[];
if(ko.isObservable(nodes)){
throw new Error('The "nodes" option must be a plain, non-observable array.');
}
var container=ko.utils.moveCleanedNodesToContainerElement(nodes);
new ko.templateSources.anonymousTemplate(element)['nodes'](container);
}else{
var templateNodes=ko.virtualElements.childNodes(element),
container=ko.utils.moveCleanedNodesToContainerElement(templateNodes);
new ko.templateSources.anonymousTemplate(element)['nodes'](container);
}
return { 'controlsDescendantBindings': true };},
'update': function (element, valueAccessor, allBindings, viewModel, bindingContext){
var value=valueAccessor(),
options=ko.utils.unwrapObservable(value),
shouldDisplay=true,
templateComputed=null,
templateName;
if(typeof options=="string"){
templateName=value;
options={};}else{
templateName=options['name'];
if('if' in options)
shouldDisplay=ko.utils.unwrapObservable(options['if']);
if(shouldDisplay&&'ifnot' in options)
shouldDisplay = !ko.utils.unwrapObservable(options['ifnot']);
}
if('foreach' in options){
var dataArray=(shouldDisplay&&options['foreach'])||[];
templateComputed=ko.renderTemplateForEach(templateName||element, dataArray, options, element, bindingContext);
}else if(!shouldDisplay){
ko.virtualElements.emptyNode(element);
}else{
var innerBindingContext=('data' in options) ?
bindingContext.createStaticChildContext(options['data'], options['as']) :
bindingContext;
templateComputed=ko.renderTemplate(templateName||element, innerBindingContext, options, element);
}
disposeOldComputedAndStoreNewOne(element, templateComputed);
}};
ko.expressionRewriting.bindingRewriteValidators['template']=function(bindingValue){
var parsedBindingValue=ko.expressionRewriting.parseObjectLiteral(bindingValue);
if((parsedBindingValue.length==1)&&parsedBindingValue[0]['unknown'])
return null;
if(ko.expressionRewriting.keyValueArrayContainsKey(parsedBindingValue, "name"))
return null;
return "This template engine does not support anonymous templates nested within its templates";
};
ko.virtualElements.allowedBindings['template']=true;
})();
ko.exportSymbol('setTemplateEngine', ko.setTemplateEngine);
ko.exportSymbol('renderTemplate', ko.renderTemplate);
ko.utils.findMovesInArrayComparison=function (left, right, limitFailedCompares){
if(left.length&&right.length){
var failedCompares, l, r, leftItem, rightItem;
for (failedCompares=l = 0; (!limitFailedCompares||failedCompares < limitFailedCompares)&&(leftItem=left[l]); ++l){
for (r=0; rightItem=right[r]; ++r){
if(leftItem['value']===rightItem['value']){
leftItem['moved']=rightItem['index'];
rightItem['moved']=leftItem['index'];
right.splice(r, 1);
failedCompares=r = 0;
break;
}}
failedCompares +=r;
}}
};
ko.utils.compareArrays=(function (){
var statusNotInOld='added', statusNotInNew='deleted';
function compareArrays(oldArray, newArray, options){
options=(typeof options==='boolean') ? { 'dontLimitMoves': options }:(options||{});
oldArray=oldArray||[];
newArray=newArray||[];
if(oldArray.length < newArray.length)
return compareSmallArrayToBigArray(oldArray, newArray, statusNotInOld, statusNotInNew, options);
else
return compareSmallArrayToBigArray(newArray, oldArray, statusNotInNew, statusNotInOld, options);
}
function compareSmallArrayToBigArray(smlArray, bigArray, statusNotInSml, statusNotInBig, options){
var myMin=Math.min,
myMax=Math.max,
editDistanceMatrix=[],
smlIndex, smlIndexMax=smlArray.length,
bigIndex, bigIndexMax=bigArray.length,
compareRange=(bigIndexMax - smlIndexMax)||1,
maxDistance=smlIndexMax + bigIndexMax + 1,
thisRow, lastRow,
bigIndexMaxForRow, bigIndexMinForRow;
for (smlIndex=0; smlIndex <=smlIndexMax; smlIndex++){
lastRow=thisRow;
editDistanceMatrix.push(thisRow=[]);
bigIndexMaxForRow=myMin(bigIndexMax, smlIndex + compareRange);
bigIndexMinForRow=myMax(0, smlIndex - 1);
for (bigIndex=bigIndexMinForRow; bigIndex <=bigIndexMaxForRow; bigIndex++){
if(!bigIndex)
thisRow[bigIndex]=smlIndex + 1;
else if(!smlIndex)
thisRow[bigIndex]=bigIndex + 1;
else if(smlArray[smlIndex - 1]===bigArray[bigIndex - 1])
thisRow[bigIndex]=lastRow[bigIndex - 1];
else {
var northDistance=lastRow[bigIndex]||maxDistance;
var westDistance=thisRow[bigIndex - 1]||maxDistance;
thisRow[bigIndex]=myMin(northDistance, westDistance) + 1;
}}
}
var editScript=[], meMinusOne, notInSml=[], notInBig=[];
for (smlIndex=smlIndexMax, bigIndex=bigIndexMax; smlIndex||bigIndex;){
meMinusOne=editDistanceMatrix[smlIndex][bigIndex] - 1;
if(bigIndex&&meMinusOne===editDistanceMatrix[smlIndex][bigIndex-1]){
notInSml.push(editScript[editScript.length]={
'status': statusNotInSml,
'value': bigArray[--bigIndex],
'index': bigIndex });
}else if(smlIndex&&meMinusOne===editDistanceMatrix[smlIndex - 1][bigIndex]){
notInBig.push(editScript[editScript.length]={
'status': statusNotInBig,
'value': smlArray[--smlIndex],
'index': smlIndex });
}else{
--bigIndex;
--smlIndex;
if(!options['sparse']){
editScript.push({
'status': "retained",
'value': bigArray[bigIndex] });
}}
}
ko.utils.findMovesInArrayComparison(notInBig, notInSml, !options['dontLimitMoves']&&smlIndexMax * 10);
return editScript.reverse();
}
return compareArrays;
})();
ko.exportSymbol('utils.compareArrays', ko.utils.compareArrays);
(function (){
function mapNodeAndRefreshWhenChanged(containerNode, mapping, valueToMap, callbackAfterAddingNodes, index){
var mappedNodes=[];
var dependentObservable=ko.dependentObservable(function(){
var newMappedNodes=mapping(valueToMap, index, ko.utils.fixUpContinuousNodeArray(mappedNodes, containerNode))||[];
if(mappedNodes.length > 0){
ko.utils.replaceDomNodes(mappedNodes, newMappedNodes);
if(callbackAfterAddingNodes)
ko.dependencyDetection.ignore(callbackAfterAddingNodes, null, [valueToMap, newMappedNodes, index]);
}
mappedNodes.length=0;
ko.utils.arrayPushAll(mappedNodes, newMappedNodes);
}, null, { disposeWhenNodeIsRemoved: containerNode, disposeWhen: function(){ return !ko.utils.anyDomNodeIsAttachedToDocument(mappedNodes); }});
return { mappedNodes:mappedNodes, dependentObservable:(dependentObservable.isActive() ? dependentObservable:undefined) };}
var lastMappingResultDomDataKey=ko.utils.domData.nextKey(),
deletedItemDummyValue=ko.utils.domData.nextKey();
ko.utils.setDomNodeChildrenFromArrayMapping=function (domNode, array, mapping, options, callbackAfterAddingNodes){
array=array||[];
options=options||{};
var isFirstExecution=ko.utils.domData.get(domNode, lastMappingResultDomDataKey)===undefined;
var lastMappingResult=ko.utils.domData.get(domNode, lastMappingResultDomDataKey)||[];
var lastArray=ko.utils.arrayMap(lastMappingResult, function (x){ return x.arrayEntry; });
var editScript=ko.utils.compareArrays(lastArray, array, options['dontLimitMoves']);
var newMappingResult=[];
var lastMappingResultIndex=0;
var newMappingResultIndex=0;
var nodesToDelete=[];
var itemsToProcess=[];
var itemsForBeforeRemoveCallbacks=[];
var itemsForMoveCallbacks=[];
var itemsForAfterAddCallbacks=[];
var mapData;
function itemMovedOrRetained(editScriptIndex, oldPosition){
mapData=lastMappingResult[oldPosition];
if(newMappingResultIndex!==oldPosition)
itemsForMoveCallbacks[editScriptIndex]=mapData;
mapData.indexObservable(newMappingResultIndex++);
ko.utils.fixUpContinuousNodeArray(mapData.mappedNodes, domNode);
newMappingResult.push(mapData);
itemsToProcess.push(mapData);
}
function callCallback(callback, items){
if(callback){
for (var i=0, n=items.length; i < n; i++){
if(items[i]){
ko.utils.arrayForEach(items[i].mappedNodes, function(node){
callback(node, i, items[i].arrayEntry);
});
}}
}}
for (var i=0, editScriptItem, movedIndex; editScriptItem=editScript[i]; i++){
movedIndex=editScriptItem['moved'];
switch (editScriptItem['status']){
case "deleted":
if(movedIndex===undefined){
mapData=lastMappingResult[lastMappingResultIndex];
if(mapData.dependentObservable){
mapData.dependentObservable.dispose();
mapData.dependentObservable=undefined;
}
if(ko.utils.fixUpContinuousNodeArray(mapData.mappedNodes, domNode).length){
if(options['beforeRemove']){
newMappingResult.push(mapData);
itemsToProcess.push(mapData);
if(mapData.arrayEntry===deletedItemDummyValue){
mapData=null;
}else{
itemsForBeforeRemoveCallbacks[i]=mapData;
}}
if(mapData){
nodesToDelete.push.apply(nodesToDelete, mapData.mappedNodes);
}}
}
lastMappingResultIndex++;
break;
case "retained":
itemMovedOrRetained(i, lastMappingResultIndex++);
break;
case "added":
if(movedIndex!==undefined){
itemMovedOrRetained(i, movedIndex);
}else{
mapData={ arrayEntry: editScriptItem['value'], indexObservable: ko.observable(newMappingResultIndex++) };
newMappingResult.push(mapData);
itemsToProcess.push(mapData);
if(!isFirstExecution)
itemsForAfterAddCallbacks[i]=mapData;
}
break;
}}
ko.utils.domData.set(domNode, lastMappingResultDomDataKey, newMappingResult);
callCallback(options['beforeMove'], itemsForMoveCallbacks);
ko.utils.arrayForEach(nodesToDelete, options['beforeRemove'] ? ko.cleanNode:ko.removeNode);
for (var i=0, nextNode=ko.virtualElements.firstChild(domNode), lastNode, node; mapData=itemsToProcess[i]; i++){
if(!mapData.mappedNodes)
ko.utils.extend(mapData, mapNodeAndRefreshWhenChanged(domNode, mapping, mapData.arrayEntry, callbackAfterAddingNodes, mapData.indexObservable));
for (var j=0; node=mapData.mappedNodes[j]; nextNode=node.nextSibling, lastNode=node, j++){
if(node!==nextNode)
ko.virtualElements.insertAfter(domNode, node, lastNode);
}
if(!mapData.initialized&&callbackAfterAddingNodes){
callbackAfterAddingNodes(mapData.arrayEntry, mapData.mappedNodes, mapData.indexObservable);
mapData.initialized=true;
}}
callCallback(options['beforeRemove'], itemsForBeforeRemoveCallbacks);
for (i=0; i < itemsForBeforeRemoveCallbacks.length; ++i){
if(itemsForBeforeRemoveCallbacks[i]){
itemsForBeforeRemoveCallbacks[i].arrayEntry=deletedItemDummyValue;
}}
callCallback(options['afterMove'], itemsForMoveCallbacks);
callCallback(options['afterAdd'], itemsForAfterAddCallbacks);
}})();
ko.exportSymbol('utils.setDomNodeChildrenFromArrayMapping', ko.utils.setDomNodeChildrenFromArrayMapping);
ko.nativeTemplateEngine=function (){
this['allowTemplateRewriting']=false;
}
ko.nativeTemplateEngine.prototype=new ko.templateEngine();
ko.nativeTemplateEngine.prototype.constructor=ko.nativeTemplateEngine;
ko.nativeTemplateEngine.prototype['renderTemplateSource']=function (templateSource, bindingContext, options, templateDocument){
var useNodesIfAvailable = !(ko.utils.ieVersion < 9),
templateNodesFunc=useNodesIfAvailable ? templateSource['nodes']:null,
templateNodes=templateNodesFunc ? templateSource['nodes']():null;
if(templateNodes){
return ko.utils.makeArray(templateNodes.cloneNode(true).childNodes);
}else{
var templateText=templateSource['text']();
return ko.utils.parseHtmlFragment(templateText, templateDocument);
}};
ko.nativeTemplateEngine.instance=new ko.nativeTemplateEngine();
ko.setTemplateEngine(ko.nativeTemplateEngine.instance);
ko.exportSymbol('nativeTemplateEngine', ko.nativeTemplateEngine);
(function(){
ko.jqueryTmplTemplateEngine=function (){
var jQueryTmplVersion=this.jQueryTmplVersion=(function(){
if(!jQueryInstance||!(jQueryInstance['tmpl']))
return 0;
try {
if(jQueryInstance['tmpl']['tag']['tmpl']['open'].toString().indexOf('__') >=0){
return 2;
}} catch(ex){  }
return 1;
})();
function ensureHasReferencedJQueryTemplates(){
if(jQueryTmplVersion < 2)
throw new Error("Your version of jQuery.tmpl is too old. Please upgrade to jQuery.tmpl 1.0.0pre or later.");
}
function executeTemplate(compiledTemplate, data, jQueryTemplateOptions){
return jQueryInstance['tmpl'](compiledTemplate, data, jQueryTemplateOptions);
}
this['renderTemplateSource']=function(templateSource, bindingContext, options, templateDocument){
templateDocument=templateDocument||document;
options=options||{};
ensureHasReferencedJQueryTemplates();
var precompiled=templateSource['data']('precompiled');
if(!precompiled){
var templateText=templateSource['text']()||"";
templateText="{{ko_with $item.koBindingContext}}" + templateText + "{{/ko_with}}";
precompiled=jQueryInstance['template'](null, templateText);
templateSource['data']('precompiled', precompiled);
}
var data=[bindingContext['$data']];
var jQueryTemplateOptions=jQueryInstance['extend']({ 'koBindingContext': bindingContext }, options['templateOptions']);
var resultNodes=executeTemplate(precompiled, data, jQueryTemplateOptions);
resultNodes['appendTo'](templateDocument.createElement("div"));
jQueryInstance['fragments']={};
return resultNodes;
};
this['createJavaScriptEvaluatorBlock']=function(script){
return "{{ko_code ((function(){ return " + script + " })()) }}";
};
this['addTemplate']=function(templateName, templateMarkup){
document.write("<script type='text/html' id='" + templateName + "'>" + templateMarkup + "<" + "/script>");
};
if(jQueryTmplVersion > 0){
jQueryInstance['tmpl']['tag']['ko_code']={
open: "__.push($1||'');"
};
jQueryInstance['tmpl']['tag']['ko_with']={
open: "with($1){",
close: "} "
};}};
ko.jqueryTmplTemplateEngine.prototype=new ko.templateEngine();
ko.jqueryTmplTemplateEngine.prototype.constructor=ko.jqueryTmplTemplateEngine;
var jqueryTmplTemplateEngineInstance=new ko.jqueryTmplTemplateEngine();
if(jqueryTmplTemplateEngineInstance.jQueryTmplVersion > 0)
ko.setTemplateEngine(jqueryTmplTemplateEngineInstance);
ko.exportSymbol('jqueryTmplTemplateEngine', ko.jqueryTmplTemplateEngine);
})();
}));
}());
})();
},{}],3:[function(require,module,exports){
;(function(factory){
'use strict';
if(typeof define==='function'&&define.amd){
define(['jquery'], factory);
}else if(typeof exports!=='undefined'){
module.exports=factory(require('jquery'));
}else{
factory(jQuery);
}}(function($){
'use strict';
var Slick=window.Slick||{};
Slick=(function(){
var instanceUid=0;
function Slick(element, settings){
var _=this, dataSettings;
_.defaults={
accessibility: true,
adaptiveHeight: false,
appendArrows: $(element),
appendDots: $(element),
arrows: true,
asNavFor: null,
prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
autoplay: false,
autoplaySpeed: 3000,
centerMode: false,
centerPadding: '50px',
cssEase: 'ease',
customPaging: function(slider, i){
return $('<button type="button" />').text(i + 1);
},
dots: false,
dotsClass: 'slick-dots',
draggable: true,
easing: 'linear',
edgeFriction: 0.35,
fade: false,
focusOnSelect: false,
focusOnChange: false,
infinite: true,
initialSlide: 0,
lazyLoad: 'ondemand',
mobileFirst: false,
pauseOnHover: true,
pauseOnFocus: true,
pauseOnDotsHover: false,
respondTo: 'window',
responsive: null,
rows: 1,
rtl: false,
slide: '',
slidesPerRow: 1,
slidesToShow: 1,
slidesToScroll: 1,
speed: 500,
swipe: true,
swipeToSlide: false,
touchMove: true,
touchThreshold: 5,
useCSS: true,
useTransform: true,
variableWidth: false,
vertical: false,
verticalSwiping: false,
waitForAnimate: true,
zIndex: 1000
};
_.initials={
animating: false,
dragging: false,
autoPlayTimer: null,
currentDirection: 0,
currentLeft: null,
currentSlide: 0,
direction: 1,
$dots: null,
listWidth: null,
listHeight: null,
loadIndex: 0,
$nextArrow: null,
$prevArrow: null,
scrolling: false,
slideCount: null,
slideWidth: null,
$slideTrack: null,
$slides: null,
sliding: false,
slideOffset: 0,
swipeLeft: null,
swiping: false,
$list: null,
touchObject: {},
transformsEnabled: false,
unslicked: false
};
$.extend(_, _.initials);
_.activeBreakpoint=null;
_.animType=null;
_.animProp=null;
_.breakpoints=[];
_.breakpointSettings=[];
_.cssTransitions=false;
_.focussed=false;
_.interrupted=false;
_.hidden='hidden';
_.paused=true;
_.positionProp=null;
_.respondTo=null;
_.rowCount=1;
_.shouldClick=true;
_.$slider=$(element);
_.$slidesCache=null;
_.transformType=null;
_.transitionType=null;
_.visibilityChange='visibilitychange';
_.windowWidth=0;
_.windowTimer=null;
dataSettings=$(element).data('slick')||{};
_.options=$.extend({}, _.defaults, settings, dataSettings);
_.currentSlide=_.options.initialSlide;
_.originalSettings=_.options;
if(typeof document.mozHidden!=='undefined'){
_.hidden='mozHidden';
_.visibilityChange='mozvisibilitychange';
}else if(typeof document.webkitHidden!=='undefined'){
_.hidden='webkitHidden';
_.visibilityChange='webkitvisibilitychange';
}
_.autoPlay=$.proxy(_.autoPlay, _);
_.autoPlayClear=$.proxy(_.autoPlayClear, _);
_.autoPlayIterator=$.proxy(_.autoPlayIterator, _);
_.changeSlide=$.proxy(_.changeSlide, _);
_.clickHandler=$.proxy(_.clickHandler, _);
_.selectHandler=$.proxy(_.selectHandler, _);
_.setPosition=$.proxy(_.setPosition, _);
_.swipeHandler=$.proxy(_.swipeHandler, _);
_.dragHandler=$.proxy(_.dragHandler, _);
_.keyHandler=$.proxy(_.keyHandler, _);
_.instanceUid=instanceUid++;
_.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/;
_.registerBreakpoints();
_.init(true);
}
return Slick;
}());
Slick.prototype.activateADA=function(){
var _=this;
_.$slideTrack.find('.slick-active').attr({
'aria-hidden': 'false'
}).find('a, input, button, select').attr({
'tabindex': '0'
});
};
Slick.prototype.addSlide=Slick.prototype.slickAdd=function(markup, index, addBefore){
var _=this;
if(typeof(index)==='boolean'){
addBefore=index;
index=null;
}else if(index < 0||(index >=_.slideCount)){
return false;
}
_.unload();
if(typeof(index)==='number'){
if(index===0&&_.$slides.length===0){
$(markup).appendTo(_.$slideTrack);
}else if(addBefore){
$(markup).insertBefore(_.$slides.eq(index));
}else{
$(markup).insertAfter(_.$slides.eq(index));
}}else{
if(addBefore===true){
$(markup).prependTo(_.$slideTrack);
}else{
$(markup).appendTo(_.$slideTrack);
}}
_.$slides=_.$slideTrack.children(this.options.slide);
_.$slideTrack.children(this.options.slide).detach();
_.$slideTrack.append(_.$slides);
_.$slides.each(function(index, element){
$(element).attr('data-slick-index', index);
});
_.$slidesCache=_.$slides;
_.reinit();
};
Slick.prototype.animateHeight=function(){
var _=this;
if(_.options.slidesToShow===1&&_.options.adaptiveHeight===true&&_.options.vertical===false){
var targetHeight=_.$slides.eq(_.currentSlide).outerHeight(true);
_.$list.animate({
height: targetHeight
}, _.options.speed);
}};
Slick.prototype.animateSlide=function(targetLeft, callback){
var animProps={},
_=this;
_.animateHeight();
if(_.options.rtl===true&&_.options.vertical===false){
targetLeft=-targetLeft;
}
if(_.transformsEnabled===false){
if(_.options.vertical===false){
_.$slideTrack.animate({
left: targetLeft
}, _.options.speed, _.options.easing, callback);
}else{
_.$slideTrack.animate({
top: targetLeft
}, _.options.speed, _.options.easing, callback);
}}else{
if(_.cssTransitions===false){
if(_.options.rtl===true){
_.currentLeft=-(_.currentLeft);
}
$({
animStart: _.currentLeft
}).animate({
animStart: targetLeft
}, {
duration: _.options.speed,
easing: _.options.easing,
step: function(now){
now=Math.ceil(now);
if(_.options.vertical===false){
animProps[_.animType]='translate(' +
now + 'px, 0px)';
_.$slideTrack.css(animProps);
}else{
animProps[_.animType]='translate(0px,' +
now + 'px)';
_.$slideTrack.css(animProps);
}},
complete: function(){
if(callback){
callback.call();
}}
});
}else{
_.applyTransition();
targetLeft=Math.ceil(targetLeft);
if(_.options.vertical===false){
animProps[_.animType]='translate3d(' + targetLeft + 'px, 0px, 0px)';
}else{
animProps[_.animType]='translate3d(0px,' + targetLeft + 'px, 0px)';
}
_.$slideTrack.css(animProps);
if(callback){
setTimeout(function(){
_.disableTransition();
callback.call();
}, _.options.speed);
}}
}};
Slick.prototype.getNavTarget=function(){
var _=this,
asNavFor=_.options.asNavFor;
if(asNavFor&&asNavFor!==null){
asNavFor=$(asNavFor).not(_.$slider);
}
return asNavFor;
};
Slick.prototype.asNavFor=function(index){
var _=this,
asNavFor=_.getNavTarget();
if(asNavFor!==null&&typeof asNavFor==='object'){
asNavFor.each(function(){
var target=$(this).slick('getSlick');
if(!target.unslicked){
target.slideHandler(index, true);
}});
}};
Slick.prototype.applyTransition=function(slide){
var _=this,
transition={};
if(_.options.fade===false){
transition[_.transitionType]=_.transformType + ' ' + _.options.speed + 'ms ' + _.options.cssEase;
}else{
transition[_.transitionType]='opacity ' + _.options.speed + 'ms ' + _.options.cssEase;
}
if(_.options.fade===false){
_.$slideTrack.css(transition);
}else{
_.$slides.eq(slide).css(transition);
}};
Slick.prototype.autoPlay=function(){
var _=this;
_.autoPlayClear();
if(_.slideCount > _.options.slidesToShow){
_.autoPlayTimer=setInterval(_.autoPlayIterator, _.options.autoplaySpeed);
}};
Slick.prototype.autoPlayClear=function(){
var _=this;
if(_.autoPlayTimer){
clearInterval(_.autoPlayTimer);
}};
Slick.prototype.autoPlayIterator=function(){
var _=this,
slideTo=_.currentSlide + _.options.slidesToScroll;
if(!_.paused&&!_.interrupted&&!_.focussed){
if(_.options.infinite===false){
if(_.direction===1&&(_.currentSlide + 1)===(_.slideCount - 1)){
_.direction=0;
}
else if(_.direction===0){
slideTo=_.currentSlide - _.options.slidesToScroll;
if(_.currentSlide - 1===0){
_.direction=1;
}}
}
_.slideHandler(slideTo);
}};
Slick.prototype.buildArrows=function(){
var _=this;
if(_.options.arrows===true){
_.$prevArrow=$(_.options.prevArrow).addClass('slick-arrow');
_.$nextArrow=$(_.options.nextArrow).addClass('slick-arrow');
if(_.slideCount > _.options.slidesToShow){
_.$prevArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');
_.$nextArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');
if(_.htmlExpr.test(_.options.prevArrow)){
_.$prevArrow.prependTo(_.options.appendArrows);
}
if(_.htmlExpr.test(_.options.nextArrow)){
_.$nextArrow.appendTo(_.options.appendArrows);
}
if(_.options.infinite!==true){
_.$prevArrow
.addClass('slick-disabled')
.attr('aria-disabled', 'true');
}}else{
_.$prevArrow.add(_.$nextArrow)
.addClass('slick-hidden')
.attr({
'aria-disabled': 'true',
'tabindex': '-1'
});
}}
};
Slick.prototype.buildDots=function(){
var _=this,
i, dot;
if(_.options.dots===true&&_.slideCount > _.options.slidesToShow){
_.$slider.addClass('slick-dotted');
dot=$('<ul />').addClass(_.options.dotsClass);
for (i=0; i <=_.getDotCount(); i +=1){
dot.append($('<li />').append(_.options.customPaging.call(this, _, i)));
}
_.$dots=dot.appendTo(_.options.appendDots);
_.$dots.find('li').first().addClass('slick-active');
}};
Slick.prototype.buildOut=function(){
var _=this;
_.$slides =
_.$slider
.children(_.options.slide + ':not(.slick-cloned)')
.addClass('slick-slide');
_.slideCount=_.$slides.length;
_.$slides.each(function(index, element){
$(element)
.attr('data-slick-index', index)
.data('originalStyling', $(element).attr('style')||'');
});
_.$slider.addClass('slick-slider');
_.$slideTrack=(_.slideCount===0) ?
$('<div class="slick-track"/>').appendTo(_.$slider) :
_.$slides.wrapAll('<div class="slick-track"/>').parent();
_.$list=_.$slideTrack.wrap('<div class="slick-list"/>').parent();
_.$slideTrack.css('opacity', 0);
if(_.options.centerMode===true||_.options.swipeToSlide===true){
_.options.slidesToScroll=1;
}
$('img[data-lazy]', _.$slider).not('[src]').addClass('slick-loading');
_.setupInfinite();
_.buildArrows();
_.buildDots();
_.updateDots();
_.setSlideClasses(typeof _.currentSlide==='number' ? _.currentSlide:0);
if(_.options.draggable===true){
_.$list.addClass('draggable');
}};
Slick.prototype.buildRows=function(){
var _=this, a, b, c, newSlides, numOfSlides, originalSlides,slidesPerSection;
newSlides=document.createDocumentFragment();
originalSlides=_.$slider.children();
if(_.options.rows > 0){
slidesPerSection=_.options.slidesPerRow * _.options.rows;
numOfSlides=Math.ceil(originalSlides.length / slidesPerSection
);
for(a=0; a < numOfSlides; a++){
var slide=document.createElement('div');
for(b=0; b < _.options.rows; b++){
var row=document.createElement('div');
for(c=0; c < _.options.slidesPerRow; c++){
var target=(a * slidesPerSection + ((b * _.options.slidesPerRow) + c));
if(originalSlides.get(target)){
row.appendChild(originalSlides.get(target));
}}
slide.appendChild(row);
}
newSlides.appendChild(slide);
}
_.$slider.empty().append(newSlides);
_.$slider.children().children().children()
.css({
'width':(100 / _.options.slidesPerRow) + '%',
'display': 'inline-block'
});
}};
Slick.prototype.checkResponsive=function(initial, forceUpdate){
var _=this,
breakpoint, targetBreakpoint, respondToWidth, triggerBreakpoint=false;
var sliderWidth=_.$slider.width();
var windowWidth=window.innerWidth||$(window).width();
if(_.respondTo==='window'){
respondToWidth=windowWidth;
}else if(_.respondTo==='slider'){
respondToWidth=sliderWidth;
}else if(_.respondTo==='min'){
respondToWidth=Math.min(windowWidth, sliderWidth);
}
if(_.options.responsive &&
_.options.responsive.length &&
_.options.responsive!==null){
targetBreakpoint=null;
for (breakpoint in _.breakpoints){
if(_.breakpoints.hasOwnProperty(breakpoint)){
if(_.originalSettings.mobileFirst===false){
if(respondToWidth < _.breakpoints[breakpoint]){
targetBreakpoint=_.breakpoints[breakpoint];
}}else{
if(respondToWidth > _.breakpoints[breakpoint]){
targetBreakpoint=_.breakpoints[breakpoint];
}}
}}
if(targetBreakpoint!==null){
if(_.activeBreakpoint!==null){
if(targetBreakpoint!==_.activeBreakpoint||forceUpdate){
_.activeBreakpoint =
targetBreakpoint;
if(_.breakpointSettings[targetBreakpoint]==='unslick'){
_.unslick(targetBreakpoint);
}else{
_.options=$.extend({}, _.originalSettings,
_.breakpointSettings[
targetBreakpoint]);
if(initial===true){
_.currentSlide=_.options.initialSlide;
}
_.refresh(initial);
}
triggerBreakpoint=targetBreakpoint;
}}else{
_.activeBreakpoint=targetBreakpoint;
if(_.breakpointSettings[targetBreakpoint]==='unslick'){
_.unslick(targetBreakpoint);
}else{
_.options=$.extend({}, _.originalSettings,
_.breakpointSettings[
targetBreakpoint]);
if(initial===true){
_.currentSlide=_.options.initialSlide;
}
_.refresh(initial);
}
triggerBreakpoint=targetBreakpoint;
}}else{
if(_.activeBreakpoint!==null){
_.activeBreakpoint=null;
_.options=_.originalSettings;
if(initial===true){
_.currentSlide=_.options.initialSlide;
}
_.refresh(initial);
triggerBreakpoint=targetBreakpoint;
}}
if(!initial&&triggerBreakpoint!==false){
_.$slider.trigger('breakpoint', [_, triggerBreakpoint]);
}}
};
Slick.prototype.changeSlide=function(event, dontAnimate){
var _=this,
$target=$(event.currentTarget),
indexOffset, slideOffset, unevenOffset;
if($target.is('a')){
event.preventDefault();
}
if(!$target.is('li')){
$target=$target.closest('li');
}
unevenOffset=(_.slideCount % _.options.slidesToScroll!==0);
indexOffset=unevenOffset ? 0:(_.slideCount - _.currentSlide) % _.options.slidesToScroll;
switch (event.data.message){
case 'previous':
slideOffset=indexOffset===0 ? _.options.slidesToScroll:_.options.slidesToShow - indexOffset;
if(_.slideCount > _.options.slidesToShow){
_.slideHandler(_.currentSlide - slideOffset, false, dontAnimate);
}
break;
case 'next':
slideOffset=indexOffset===0 ? _.options.slidesToScroll:indexOffset;
if(_.slideCount > _.options.slidesToShow){
_.slideHandler(_.currentSlide + slideOffset, false, dontAnimate);
}
break;
case 'index':
var index=event.data.index===0 ? 0 :
event.data.index||$target.index() * _.options.slidesToScroll;
_.slideHandler(_.checkNavigable(index), false, dontAnimate);
$target.children().trigger('focus');
break;
default:
return;
}};
Slick.prototype.checkNavigable=function(index){
var _=this,
navigables, prevNavigable;
navigables=_.getNavigableIndexes();
prevNavigable=0;
if(index > navigables[navigables.length - 1]){
index=navigables[navigables.length - 1];
}else{
for (var n in navigables){
if(index < navigables[n]){
index=prevNavigable;
break;
}
prevNavigable=navigables[n];
}}
return index;
};
Slick.prototype.cleanUpEvents=function(){
var _=this;
if(_.options.dots&&_.$dots!==null){
$('li', _.$dots)
.off('click.slick', _.changeSlide)
.off('mouseenter.slick', $.proxy(_.interrupt, _, true))
.off('mouseleave.slick', $.proxy(_.interrupt, _, false));
if(_.options.accessibility===true){
_.$dots.off('keydown.slick', _.keyHandler);
}}
_.$slider.off('focus.slick blur.slick');
if(_.options.arrows===true&&_.slideCount > _.options.slidesToShow){
_.$prevArrow&&_.$prevArrow.off('click.slick', _.changeSlide);
_.$nextArrow&&_.$nextArrow.off('click.slick', _.changeSlide);
if(_.options.accessibility===true){
_.$prevArrow&&_.$prevArrow.off('keydown.slick', _.keyHandler);
_.$nextArrow&&_.$nextArrow.off('keydown.slick', _.keyHandler);
}}
_.$list.off('touchstart.slick mousedown.slick', _.swipeHandler);
_.$list.off('touchmove.slick mousemove.slick', _.swipeHandler);
_.$list.off('touchend.slick mouseup.slick', _.swipeHandler);
_.$list.off('touchcancel.slick mouseleave.slick', _.swipeHandler);
_.$list.off('click.slick', _.clickHandler);
$(document).off(_.visibilityChange, _.visibility);
_.cleanUpSlideEvents();
if(_.options.accessibility===true){
_.$list.off('keydown.slick', _.keyHandler);
}
if(_.options.focusOnSelect===true){
$(_.$slideTrack).children().off('click.slick', _.selectHandler);
}
$(window).off('orientationchange.slick.slick-' + _.instanceUid, _.orientationChange);
$(window).off('resize.slick.slick-' + _.instanceUid, _.resize);
$('[draggable!=true]', _.$slideTrack).off('dragstart', _.preventDefault);
$(window).off('load.slick.slick-' + _.instanceUid, _.setPosition);
};
Slick.prototype.cleanUpSlideEvents=function(){
var _=this;
_.$list.off('mouseenter.slick', $.proxy(_.interrupt, _, true));
_.$list.off('mouseleave.slick', $.proxy(_.interrupt, _, false));
};
Slick.prototype.cleanUpRows=function(){
var _=this, originalSlides;
if(_.options.rows > 0){
originalSlides=_.$slides.children().children();
originalSlides.removeAttr('style');
_.$slider.empty().append(originalSlides);
}};
Slick.prototype.clickHandler=function(event){
var _=this;
if(_.shouldClick===false){
event.stopImmediatePropagation();
event.stopPropagation();
event.preventDefault();
}};
Slick.prototype.destroy=function(refresh){
var _=this;
_.autoPlayClear();
_.touchObject={};
_.cleanUpEvents();
$('.slick-cloned', _.$slider).detach();
if(_.$dots){
_.$dots.remove();
}
if(_.$prevArrow&&_.$prevArrow.length){
_.$prevArrow
.removeClass('slick-disabled slick-arrow slick-hidden')
.removeAttr('aria-hidden aria-disabled tabindex')
.css('display','');
if(_.htmlExpr.test(_.options.prevArrow)){
_.$prevArrow.remove();
}}
if(_.$nextArrow&&_.$nextArrow.length){
_.$nextArrow
.removeClass('slick-disabled slick-arrow slick-hidden')
.removeAttr('aria-hidden aria-disabled tabindex')
.css('display','');
if(_.htmlExpr.test(_.options.nextArrow)){
_.$nextArrow.remove();
}}
if(_.$slides){
_.$slides
.removeClass('slick-slide slick-active slick-center slick-visible slick-current')
.removeAttr('aria-hidden')
.removeAttr('data-slick-index')
.each(function(){
$(this).attr('style', $(this).data('originalStyling'));
});
_.$slideTrack.children(this.options.slide).detach();
_.$slideTrack.detach();
_.$list.detach();
_.$slider.append(_.$slides);
}
_.cleanUpRows();
_.$slider.removeClass('slick-slider');
_.$slider.removeClass('slick-initialized');
_.$slider.removeClass('slick-dotted');
_.unslicked=true;
if(!refresh){
_.$slider.trigger('destroy', [_]);
}};
Slick.prototype.disableTransition=function(slide){
var _=this,
transition={};
transition[_.transitionType]='';
if(_.options.fade===false){
_.$slideTrack.css(transition);
}else{
_.$slides.eq(slide).css(transition);
}};
Slick.prototype.fadeSlide=function(slideIndex, callback){
var _=this;
if(_.cssTransitions===false){
_.$slides.eq(slideIndex).css({
zIndex: _.options.zIndex
});
_.$slides.eq(slideIndex).animate({
opacity: 1
}, _.options.speed, _.options.easing, callback);
}else{
_.applyTransition(slideIndex);
_.$slides.eq(slideIndex).css({
opacity: 1,
zIndex: _.options.zIndex
});
if(callback){
setTimeout(function(){
_.disableTransition(slideIndex);
callback.call();
}, _.options.speed);
}}
};
Slick.prototype.fadeSlideOut=function(slideIndex){
var _=this;
if(_.cssTransitions===false){
_.$slides.eq(slideIndex).animate({
opacity: 0,
zIndex: _.options.zIndex - 2
}, _.options.speed, _.options.easing);
}else{
_.applyTransition(slideIndex);
_.$slides.eq(slideIndex).css({
opacity: 0,
zIndex: _.options.zIndex - 2
});
}};
Slick.prototype.filterSlides=Slick.prototype.slickFilter=function(filter){
var _=this;
if(filter!==null){
_.$slidesCache=_.$slides;
_.unload();
_.$slideTrack.children(this.options.slide).detach();
_.$slidesCache.filter(filter).appendTo(_.$slideTrack);
_.reinit();
}};
Slick.prototype.focusHandler=function(){
var _=this;
_.$slider
.off('focus.slick blur.slick')
.on('focus.slick blur.slick', '*', function(event){
event.stopImmediatePropagation();
var $sf=$(this);
setTimeout(function(){
if(_.options.pauseOnFocus){
_.focussed=$sf.is(':focus');
_.autoPlay();
}}, 0);
});
};
Slick.prototype.getCurrent=Slick.prototype.slickCurrentSlide=function(){
var _=this;
return _.currentSlide;
};
Slick.prototype.getDotCount=function(){
var _=this;
var breakPoint=0;
var counter=0;
var pagerQty=0;
if(_.options.infinite===true){
if(_.slideCount <=_.options.slidesToShow){
++pagerQty;
}else{
while (breakPoint < _.slideCount){
++pagerQty;
breakPoint=counter + _.options.slidesToScroll;
counter +=_.options.slidesToScroll <=_.options.slidesToShow ? _.options.slidesToScroll:_.options.slidesToShow;
}}
}else if(_.options.centerMode===true){
pagerQty=_.slideCount;
}else if(!_.options.asNavFor){
pagerQty=1 + Math.ceil((_.slideCount - _.options.slidesToShow) / _.options.slidesToScroll);
}else{
while (breakPoint < _.slideCount){
++pagerQty;
breakPoint=counter + _.options.slidesToScroll;
counter +=_.options.slidesToScroll <=_.options.slidesToShow ? _.options.slidesToScroll:_.options.slidesToShow;
}}
return pagerQty - 1;
};
Slick.prototype.getLeft=function(slideIndex){
var _=this,
targetLeft,
verticalHeight,
verticalOffset=0,
targetSlide,
coef;
_.slideOffset=0;
verticalHeight=_.$slides.first().outerHeight(true);
if(_.options.infinite===true){
if(_.slideCount > _.options.slidesToShow){
_.slideOffset=(_.slideWidth * _.options.slidesToShow) * -1;
coef=-1
if(_.options.vertical===true&&_.options.centerMode===true){
if(_.options.slidesToShow===2){
coef=-1.5;
}else if(_.options.slidesToShow===1){
coef=-2
}}
verticalOffset=(verticalHeight * _.options.slidesToShow) * coef;
}
if(_.slideCount % _.options.slidesToScroll!==0){
if(slideIndex + _.options.slidesToScroll > _.slideCount&&_.slideCount > _.options.slidesToShow){
if(slideIndex > _.slideCount){
_.slideOffset=((_.options.slidesToShow - (slideIndex - _.slideCount)) * _.slideWidth) * -1;
verticalOffset=((_.options.slidesToShow - (slideIndex - _.slideCount)) * verticalHeight) * -1;
}else{
_.slideOffset=((_.slideCount % _.options.slidesToScroll) * _.slideWidth) * -1;
verticalOffset=((_.slideCount % _.options.slidesToScroll) * verticalHeight) * -1;
}}
}}else{
if(slideIndex + _.options.slidesToShow > _.slideCount){
_.slideOffset=((slideIndex + _.options.slidesToShow) - _.slideCount) * _.slideWidth;
verticalOffset=((slideIndex + _.options.slidesToShow) - _.slideCount) * verticalHeight;
}}
if(_.slideCount <=_.options.slidesToShow){
_.slideOffset=0;
verticalOffset=0;
}
if(_.options.centerMode===true&&_.slideCount <=_.options.slidesToShow){
_.slideOffset=((_.slideWidth * Math.floor(_.options.slidesToShow)) / 2) - ((_.slideWidth * _.slideCount) / 2);
}else if(_.options.centerMode===true&&_.options.infinite===true){
_.slideOffset +=_.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
}else if(_.options.centerMode===true){
_.slideOffset=0;
_.slideOffset +=_.slideWidth * Math.floor(_.options.slidesToShow / 2);
}
if(_.options.vertical===false){
targetLeft=((slideIndex * _.slideWidth) * -1) + _.slideOffset;
}else{
targetLeft=((slideIndex * verticalHeight) * -1) + verticalOffset;
}
if(_.options.variableWidth===true){
if(_.slideCount <=_.options.slidesToShow||_.options.infinite===false){
targetSlide=_.$slideTrack.children('.slick-slide').eq(slideIndex);
}else{
targetSlide=_.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow);
}
if(_.options.rtl===true){
if(targetSlide[0]){
targetLeft=(_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
}else{
targetLeft=0;
}}else{
targetLeft=targetSlide[0] ? targetSlide[0].offsetLeft * -1:0;
}
if(_.options.centerMode===true){
if(_.slideCount <=_.options.slidesToShow||_.options.infinite===false){
targetSlide=_.$slideTrack.children('.slick-slide').eq(slideIndex);
}else{
targetSlide=_.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow + 1);
}
if(_.options.rtl===true){
if(targetSlide[0]){
targetLeft=(_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
}else{
targetLeft=0;
}}else{
targetLeft=targetSlide[0] ? targetSlide[0].offsetLeft * -1:0;
}
targetLeft +=(_.$list.width() - targetSlide.outerWidth()) / 2;
}}
return targetLeft;
};
Slick.prototype.getOption=Slick.prototype.slickGetOption=function(option){
var _=this;
return _.options[option];
};
Slick.prototype.getNavigableIndexes=function(){
var _=this,
breakPoint=0,
counter=0,
indexes=[],
max;
if(_.options.infinite===false){
max=_.slideCount;
}else{
breakPoint=_.options.slidesToScroll * -1;
counter=_.options.slidesToScroll * -1;
max=_.slideCount * 2;
}
while (breakPoint < max){
indexes.push(breakPoint);
breakPoint=counter + _.options.slidesToScroll;
counter +=_.options.slidesToScroll <=_.options.slidesToShow ? _.options.slidesToScroll:_.options.slidesToShow;
}
return indexes;
};
Slick.prototype.getSlick=function(){
return this;
};
Slick.prototype.getSlideCount=function(){
var _=this,
slidesTraversed, swipedSlide, centerOffset;
centerOffset=_.options.centerMode===true ? _.slideWidth * Math.floor(_.options.slidesToShow / 2):0;
if(_.options.swipeToSlide===true){
_.$slideTrack.find('.slick-slide').each(function(index, slide){
if(slide.offsetLeft - centerOffset + ($(slide).outerWidth() / 2) > (_.swipeLeft * -1)){
swipedSlide=slide;
return false;
}});
slidesTraversed=Math.abs($(swipedSlide).attr('data-slick-index') - _.currentSlide)||1;
return slidesTraversed;
}else{
return _.options.slidesToScroll;
}};
Slick.prototype.goTo=Slick.prototype.slickGoTo=function(slide, dontAnimate){
var _=this;
_.changeSlide({
data: {
message: 'index',
index: parseInt(slide)
}}, dontAnimate);
};
Slick.prototype.init=function(creation){
var _=this;
if(!$(_.$slider).hasClass('slick-initialized')){
$(_.$slider).addClass('slick-initialized');
_.buildRows();
_.buildOut();
_.setProps();
_.startLoad();
_.loadSlider();
_.initializeEvents();
_.updateArrows();
_.updateDots();
_.checkResponsive(true);
_.focusHandler();
}
if(creation){
_.$slider.trigger('init', [_]);
}
if(_.options.accessibility===true){
_.initADA();
}
if(_.options.autoplay){
_.paused=false;
_.autoPlay();
}};
Slick.prototype.initADA=function(){
var _=this,
numDotGroups=Math.ceil(_.slideCount / _.options.slidesToShow),
tabControlIndexes=_.getNavigableIndexes().filter(function(val){
return (val >=0)&&(val < _.slideCount);
});
_.$slides.add(_.$slideTrack.find('.slick-cloned')).attr({
'aria-hidden': 'true',
'tabindex': '-1'
}).find('a, input, button, select').attr({
'tabindex': '-1'
});
if(_.$dots!==null){
_.$slides.not(_.$slideTrack.find('.slick-cloned')).each(function(i){
var slideControlIndex=tabControlIndexes.indexOf(i);
$(this).attr({
'role': 'tabpanel',
'id': 'slick-slide' + _.instanceUid + i,
'tabindex': -1
});
if(slideControlIndex!==-1){
var ariaButtonControl='slick-slide-control' + _.instanceUid + slideControlIndex
if($('#' + ariaButtonControl).length){
$(this).attr({
'aria-describedby': ariaButtonControl
});
}}
});
_.$dots.attr('role', 'tablist').find('li').each(function(i){
var mappedSlideIndex=tabControlIndexes[i];
$(this).attr({
'role': 'presentation'
});
$(this).find('button').first().attr({
'role': 'tab',
'id': 'slick-slide-control' + _.instanceUid + i,
'aria-controls': 'slick-slide' + _.instanceUid + mappedSlideIndex,
'aria-label': (i + 1) + ' of ' + numDotGroups,
'aria-selected': null,
'tabindex': '-1'
});
}).eq(_.currentSlide).find('button').attr({
'aria-selected': 'true',
'tabindex': '0'
}).end();
}
for (var i=_.currentSlide, max=i+_.options.slidesToShow; i < max; i++){
if(_.options.focusOnChange){
_.$slides.eq(i).attr({'tabindex': '0'});
}else{
_.$slides.eq(i).removeAttr('tabindex');
}}
_.activateADA();
};
Slick.prototype.initArrowEvents=function(){
var _=this;
if(_.options.arrows===true&&_.slideCount > _.options.slidesToShow){
_.$prevArrow
.off('click.slick')
.on('click.slick', {
message: 'previous'
}, _.changeSlide);
_.$nextArrow
.off('click.slick')
.on('click.slick', {
message: 'next'
}, _.changeSlide);
if(_.options.accessibility===true){
_.$prevArrow.on('keydown.slick', _.keyHandler);
_.$nextArrow.on('keydown.slick', _.keyHandler);
}}
};
Slick.prototype.initDotEvents=function(){
var _=this;
if(_.options.dots===true&&_.slideCount > _.options.slidesToShow){
$('li', _.$dots).on('click.slick', {
message: 'index'
}, _.changeSlide);
if(_.options.accessibility===true){
_.$dots.on('keydown.slick', _.keyHandler);
}}
if(_.options.dots===true&&_.options.pauseOnDotsHover===true&&_.slideCount > _.options.slidesToShow){
$('li', _.$dots)
.on('mouseenter.slick', $.proxy(_.interrupt, _, true))
.on('mouseleave.slick', $.proxy(_.interrupt, _, false));
}};
Slick.prototype.initSlideEvents=function(){
var _=this;
if(_.options.pauseOnHover){
_.$list.on('mouseenter.slick', $.proxy(_.interrupt, _, true));
_.$list.on('mouseleave.slick', $.proxy(_.interrupt, _, false));
}};
Slick.prototype.initializeEvents=function(){
var _=this;
_.initArrowEvents();
_.initDotEvents();
_.initSlideEvents();
_.$list.on('touchstart.slick mousedown.slick', {
action: 'start'
}, _.swipeHandler);
_.$list.on('touchmove.slick mousemove.slick', {
action: 'move'
}, _.swipeHandler);
_.$list.on('touchend.slick mouseup.slick', {
action: 'end'
}, _.swipeHandler);
_.$list.on('touchcancel.slick mouseleave.slick', {
action: 'end'
}, _.swipeHandler);
_.$list.on('click.slick', _.clickHandler);
$(document).on(_.visibilityChange, $.proxy(_.visibility, _));
if(_.options.accessibility===true){
_.$list.on('keydown.slick', _.keyHandler);
}
if(_.options.focusOnSelect===true){
$(_.$slideTrack).children().on('click.slick', _.selectHandler);
}
$(window).on('orientationchange.slick.slick-' + _.instanceUid, $.proxy(_.orientationChange, _));
$(window).on('resize.slick.slick-' + _.instanceUid, $.proxy(_.resize, _));
$('[draggable!=true]', _.$slideTrack).on('dragstart', _.preventDefault);
$(window).on('load.slick.slick-' + _.instanceUid, _.setPosition);
$(_.setPosition);
};
Slick.prototype.initUI=function(){
var _=this;
if(_.options.arrows===true&&_.slideCount > _.options.slidesToShow){
_.$prevArrow.show();
_.$nextArrow.show();
}
if(_.options.dots===true&&_.slideCount > _.options.slidesToShow){
_.$dots.show();
}};
Slick.prototype.keyHandler=function(event){
var _=this;
if(!event.target.tagName.match('TEXTAREA|INPUT|SELECT')){
if(event.keyCode===37&&_.options.accessibility===true){
_.changeSlide({
data: {
message: _.options.rtl===true ? 'next':'previous'
}});
}else if(event.keyCode===39&&_.options.accessibility===true){
_.changeSlide({
data: {
message: _.options.rtl===true ? 'previous':'next'
}});
}}
};
Slick.prototype.lazyLoad=function(){
var _=this,
loadRange, cloneRange, rangeStart, rangeEnd;
function loadImages(imagesScope){
$('img[data-lazy]', imagesScope).each(function(){
var image=$(this),
imageSource=$(this).attr('data-lazy'),
imageSrcSet=$(this).attr('data-srcset'),
imageSizes=$(this).attr('data-sizes')||_.$slider.attr('data-sizes'),
imageToLoad=document.createElement('img');
imageToLoad.onload=function(){
image
.animate({ opacity: 0 }, 100, function(){
if(imageSrcSet){
image
.attr('srcset', imageSrcSet);
if(imageSizes){
image
.attr('sizes', imageSizes);
}}
image
.attr('src', imageSource)
.animate({ opacity: 1 }, 200, function(){
image
.removeAttr('data-lazy data-srcset data-sizes')
.removeClass('slick-loading');
});
_.$slider.trigger('lazyLoaded', [_, image, imageSource]);
});
};
imageToLoad.onerror=function(){
image
.removeAttr('data-lazy')
.removeClass('slick-loading')
.addClass('slick-lazyload-error');
_.$slider.trigger('lazyLoadError', [ _, image, imageSource ]);
};
imageToLoad.src=imageSource;
});
}
if(_.options.centerMode===true){
if(_.options.infinite===true){
rangeStart=_.currentSlide + (_.options.slidesToShow / 2 + 1);
rangeEnd=rangeStart + _.options.slidesToShow + 2;
}else{
rangeStart=Math.max(0, _.currentSlide - (_.options.slidesToShow / 2 + 1));
rangeEnd=2 + (_.options.slidesToShow / 2 + 1) + _.currentSlide;
}}else{
rangeStart=_.options.infinite ? _.options.slidesToShow + _.currentSlide:_.currentSlide;
rangeEnd=Math.ceil(rangeStart + _.options.slidesToShow);
if(_.options.fade===true){
if(rangeStart > 0) rangeStart--;
if(rangeEnd <=_.slideCount) rangeEnd++;
}}
loadRange=_.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);
if(_.options.lazyLoad==='anticipated'){
var prevSlide=rangeStart - 1,
nextSlide=rangeEnd,
$slides=_.$slider.find('.slick-slide');
for (var i=0; i < _.options.slidesToScroll; i++){
if(prevSlide < 0) prevSlide=_.slideCount - 1;
loadRange=loadRange.add($slides.eq(prevSlide));
loadRange=loadRange.add($slides.eq(nextSlide));
prevSlide--;
nextSlide++;
}}
loadImages(loadRange);
if(_.slideCount <=_.options.slidesToShow){
cloneRange=_.$slider.find('.slick-slide');
loadImages(cloneRange);
} else
if(_.currentSlide >=_.slideCount - _.options.slidesToShow){
cloneRange=_.$slider.find('.slick-cloned').slice(0, _.options.slidesToShow);
loadImages(cloneRange);
}else if(_.currentSlide===0){
cloneRange=_.$slider.find('.slick-cloned').slice(_.options.slidesToShow * -1);
loadImages(cloneRange);
}};
Slick.prototype.loadSlider=function(){
var _=this;
_.setPosition();
_.$slideTrack.css({
opacity: 1
});
_.$slider.removeClass('slick-loading');
_.initUI();
if(_.options.lazyLoad==='progressive'){
_.progressiveLazyLoad();
}};
Slick.prototype.next=Slick.prototype.slickNext=function(){
var _=this;
_.changeSlide({
data: {
message: 'next'
}});
};
Slick.prototype.orientationChange=function(){
var _=this;
_.checkResponsive();
_.setPosition();
};
Slick.prototype.pause=Slick.prototype.slickPause=function(){
var _=this;
_.autoPlayClear();
_.paused=true;
};
Slick.prototype.play=Slick.prototype.slickPlay=function(){
var _=this;
_.autoPlay();
_.options.autoplay=true;
_.paused=false;
_.focussed=false;
_.interrupted=false;
};
Slick.prototype.postSlide=function(index){
var _=this;
if(!_.unslicked){
_.$slider.trigger('afterChange', [_, index]);
_.animating=false;
if(_.slideCount > _.options.slidesToShow){
_.setPosition();
}
_.swipeLeft=null;
if(_.options.autoplay){
_.autoPlay();
}
if(_.options.accessibility===true){
_.initADA();
if(_.options.focusOnChange){
var $currentSlide=$(_.$slides.get(_.currentSlide));
$currentSlide.attr('tabindex', 0).focus();
}}
}};
Slick.prototype.prev=Slick.prototype.slickPrev=function(){
var _=this;
_.changeSlide({
data: {
message: 'previous'
}});
};
Slick.prototype.preventDefault=function(event){
event.preventDefault();
};
Slick.prototype.progressiveLazyLoad=function(tryCount){
tryCount=tryCount||1;
var _=this,
$imgsToLoad=$('img[data-lazy]', _.$slider),
image,
imageSource,
imageSrcSet,
imageSizes,
imageToLoad;
if($imgsToLoad.length){
image=$imgsToLoad.first();
imageSource=image.attr('data-lazy');
imageSrcSet=image.attr('data-srcset');
imageSizes=image.attr('data-sizes')||_.$slider.attr('data-sizes');
imageToLoad=document.createElement('img');
imageToLoad.onload=function(){
if(imageSrcSet){
image
.attr('srcset', imageSrcSet);
if(imageSizes){
image
.attr('sizes', imageSizes);
}}
image
.attr('src', imageSource)
.removeAttr('data-lazy data-srcset data-sizes')
.removeClass('slick-loading');
if(_.options.adaptiveHeight===true){
_.setPosition();
}
_.$slider.trigger('lazyLoaded', [ _, image, imageSource ]);
_.progressiveLazyLoad();
};
imageToLoad.onerror=function(){
if(tryCount < 3){
setTimeout(function(){
_.progressiveLazyLoad(tryCount + 1);
}, 500);
}else{
image
.removeAttr('data-lazy')
.removeClass('slick-loading')
.addClass('slick-lazyload-error');
_.$slider.trigger('lazyLoadError', [ _, image, imageSource ]);
_.progressiveLazyLoad();
}};
imageToLoad.src=imageSource;
}else{
_.$slider.trigger('allImagesLoaded', [ _ ]);
}};
Slick.prototype.refresh=function(initializing){
var _=this, currentSlide, lastVisibleIndex;
lastVisibleIndex=_.slideCount - _.options.slidesToShow;
if(!_.options.infinite&&(_.currentSlide > lastVisibleIndex)){
_.currentSlide=lastVisibleIndex;
}
if(_.slideCount <=_.options.slidesToShow){
_.currentSlide=0;
}
currentSlide=_.currentSlide;
_.destroy(true);
$.extend(_, _.initials, { currentSlide: currentSlide });
_.init();
if(!initializing){
_.changeSlide({
data: {
message: 'index',
index: currentSlide
}}, false);
}};
Slick.prototype.registerBreakpoints=function(){
var _=this, breakpoint, currentBreakpoint, l,
responsiveSettings=_.options.responsive||null;
if($.type(responsiveSettings)==='array'&&responsiveSettings.length){
_.respondTo=_.options.respondTo||'window';
for(breakpoint in responsiveSettings){
l=_.breakpoints.length-1;
if(responsiveSettings.hasOwnProperty(breakpoint)){
currentBreakpoint=responsiveSettings[breakpoint].breakpoint;
while(l >=0){
if(_.breakpoints[l]&&_.breakpoints[l]===currentBreakpoint){
_.breakpoints.splice(l,1);
}
l--;
}
_.breakpoints.push(currentBreakpoint);
_.breakpointSettings[currentBreakpoint]=responsiveSettings[breakpoint].settings;
}}
_.breakpoints.sort(function(a, b){
return(_.options.mobileFirst) ? a-b:b-a;
});
}};
Slick.prototype.reinit=function(){
var _=this;
_.$slides =
_.$slideTrack
.children(_.options.slide)
.addClass('slick-slide');
_.slideCount=_.$slides.length;
if(_.currentSlide >=_.slideCount&&_.currentSlide!==0){
_.currentSlide=_.currentSlide - _.options.slidesToScroll;
}
if(_.slideCount <=_.options.slidesToShow){
_.currentSlide=0;
}
_.registerBreakpoints();
_.setProps();
_.setupInfinite();
_.buildArrows();
_.updateArrows();
_.initArrowEvents();
_.buildDots();
_.updateDots();
_.initDotEvents();
_.cleanUpSlideEvents();
_.initSlideEvents();
_.checkResponsive(false, true);
if(_.options.focusOnSelect===true){
$(_.$slideTrack).children().on('click.slick', _.selectHandler);
}
_.setSlideClasses(typeof _.currentSlide==='number' ? _.currentSlide:0);
_.setPosition();
_.focusHandler();
_.paused = !_.options.autoplay;
_.autoPlay();
_.$slider.trigger('reInit', [_]);
};
Slick.prototype.resize=function(){
var _=this;
if($(window).width()!==_.windowWidth){
clearTimeout(_.windowDelay);
_.windowDelay=window.setTimeout(function(){
_.windowWidth=$(window).width();
_.checkResponsive();
if(!_.unslicked){ _.setPosition(); }}, 50);
}};
Slick.prototype.removeSlide=Slick.prototype.slickRemove=function(index, removeBefore, removeAll){
var _=this;
if(typeof(index)==='boolean'){
removeBefore=index;
index=removeBefore===true ? 0:_.slideCount - 1;
}else{
index=removeBefore===true ? --index:index;
}
if(_.slideCount < 1||index < 0||index > _.slideCount - 1){
return false;
}
_.unload();
if(removeAll===true){
_.$slideTrack.children().remove();
}else{
_.$slideTrack.children(this.options.slide).eq(index).remove();
}
_.$slides=_.$slideTrack.children(this.options.slide);
_.$slideTrack.children(this.options.slide).detach();
_.$slideTrack.append(_.$slides);
_.$slidesCache=_.$slides;
_.reinit();
};
Slick.prototype.setCSS=function(position){
var _=this,
positionProps={},
x, y;
if(_.options.rtl===true){
position=-position;
}
x=_.positionProp=='left' ? Math.ceil(position) + 'px':'0px';
y=_.positionProp=='top' ? Math.ceil(position) + 'px':'0px';
positionProps[_.positionProp]=position;
if(_.transformsEnabled===false){
_.$slideTrack.css(positionProps);
}else{
positionProps={};
if(_.cssTransitions===false){
positionProps[_.animType]='translate(' + x + ', ' + y + ')';
_.$slideTrack.css(positionProps);
}else{
positionProps[_.animType]='translate3d(' + x + ', ' + y + ', 0px)';
_.$slideTrack.css(positionProps);
}}
};
Slick.prototype.setDimensions=function(){
var _=this;
if(_.options.vertical===false){
if(_.options.centerMode===true){
_.$list.css({
padding: ('0px ' + _.options.centerPadding)
});
}}else{
_.$list.height(_.$slides.first().outerHeight(true) * _.options.slidesToShow);
if(_.options.centerMode===true){
_.$list.css({
padding: (_.options.centerPadding + ' 0px')
});
}}
_.listWidth=_.$list.width();
_.listHeight=_.$list.height();
if(_.options.vertical===false&&_.options.variableWidth===false){
_.slideWidth=Math.ceil(_.listWidth / _.options.slidesToShow);
_.$slideTrack.width(Math.ceil((_.slideWidth * _.$slideTrack.children('.slick-slide').length)));
}else if(_.options.variableWidth===true){
_.$slideTrack.width(5000 * _.slideCount);
}else{
_.slideWidth=Math.ceil(_.listWidth);
_.$slideTrack.height(Math.ceil((_.$slides.first().outerHeight(true) * _.$slideTrack.children('.slick-slide').length)));
}
var offset=_.$slides.first().outerWidth(true) - _.$slides.first().width();
if(_.options.variableWidth===false) _.$slideTrack.children('.slick-slide').width(_.slideWidth - offset);
};
Slick.prototype.setFade=function(){
var _=this,
targetLeft;
_.$slides.each(function(index, element){
targetLeft=(_.slideWidth * index) * -1;
if(_.options.rtl===true){
$(element).css({
position: 'relative',
right: targetLeft,
top: 0,
zIndex: _.options.zIndex - 2,
opacity: 0
});
}else{
$(element).css({
position: 'relative',
left: targetLeft,
top: 0,
zIndex: _.options.zIndex - 2,
opacity: 0
});
}});
_.$slides.eq(_.currentSlide).css({
zIndex: _.options.zIndex - 1,
opacity: 1
});
};
Slick.prototype.setHeight=function(){
var _=this;
if(_.options.slidesToShow===1&&_.options.adaptiveHeight===true&&_.options.vertical===false){
var targetHeight=_.$slides.eq(_.currentSlide).outerHeight(true);
_.$list.css('height', targetHeight);
}};
Slick.prototype.setOption =
Slick.prototype.slickSetOption=function(){
var _=this, l, item, option, value, refresh=false, type;
if($.type(arguments[0])==='object'){
option=arguments[0];
refresh=arguments[1];
type='multiple';
}else if($.type(arguments[0])==='string'){
option=arguments[0];
value=arguments[1];
refresh=arguments[2];
if(arguments[0]==='responsive'&&$.type(arguments[1])==='array'){
type='responsive';
}else if(typeof arguments[1]!=='undefined'){
type='single';
}}
if(type==='single'){
_.options[option]=value;
}else if(type==='multiple'){
$.each(option , function(opt, val){
_.options[opt]=val;
});
}else if(type==='responsive'){
for(item in value){
if($.type(_.options.responsive)!=='array'){
_.options.responsive=[ value[item] ];
}else{
l=_.options.responsive.length-1;
while(l >=0){
if(_.options.responsive[l].breakpoint===value[item].breakpoint){
_.options.responsive.splice(l,1);
}
l--;
}
_.options.responsive.push(value[item]);
}}
}
if(refresh){
_.unload();
_.reinit();
}};
Slick.prototype.setPosition=function(){
var _=this;
_.setDimensions();
_.setHeight();
if(_.options.fade===false){
_.setCSS(_.getLeft(_.currentSlide));
}else{
_.setFade();
}
_.$slider.trigger('setPosition', [_]);
};
Slick.prototype.setProps=function(){
var _=this,
bodyStyle=document.body.style;
_.positionProp=_.options.vertical===true ? 'top':'left';
if(_.positionProp==='top'){
_.$slider.addClass('slick-vertical');
}else{
_.$slider.removeClass('slick-vertical');
}
if(bodyStyle.WebkitTransition!==undefined ||
bodyStyle.MozTransition!==undefined ||
bodyStyle.msTransition!==undefined){
if(_.options.useCSS===true){
_.cssTransitions=true;
}}
if(_.options.fade){
if(typeof _.options.zIndex==='number'){
if(_.options.zIndex < 3){
_.options.zIndex=3;
}}else{
_.options.zIndex=_.defaults.zIndex;
}}
if(bodyStyle.OTransform!==undefined){
_.animType='OTransform';
_.transformType='-o-transform';
_.transitionType='OTransition';
if(bodyStyle.perspectiveProperty===undefined&&bodyStyle.webkitPerspective===undefined) _.animType=false;
}
if(bodyStyle.MozTransform!==undefined){
_.animType='MozTransform';
_.transformType='-moz-transform';
_.transitionType='MozTransition';
if(bodyStyle.perspectiveProperty===undefined&&bodyStyle.MozPerspective===undefined) _.animType=false;
}
if(bodyStyle.webkitTransform!==undefined){
_.animType='webkitTransform';
_.transformType='-webkit-transform';
_.transitionType='webkitTransition';
if(bodyStyle.perspectiveProperty===undefined&&bodyStyle.webkitPerspective===undefined) _.animType=false;
}
if(bodyStyle.msTransform!==undefined){
_.animType='msTransform';
_.transformType='-ms-transform';
_.transitionType='msTransition';
if(bodyStyle.msTransform===undefined) _.animType=false;
}
if(bodyStyle.transform!==undefined&&_.animType!==false){
_.animType='transform';
_.transformType='transform';
_.transitionType='transition';
}
_.transformsEnabled=_.options.useTransform&&(_.animType!==null&&_.animType!==false);
};
Slick.prototype.setSlideClasses=function(index){
var _=this,
centerOffset, allSlides, indexOffset, remainder;
allSlides=_.$slider
.find('.slick-slide')
.removeClass('slick-active slick-center slick-current')
.attr('aria-hidden', 'true');
_.$slides
.eq(index)
.addClass('slick-current');
if(_.options.centerMode===true){
var evenCoef=_.options.slidesToShow % 2===0 ? 1:0;
centerOffset=Math.floor(_.options.slidesToShow / 2);
if(_.options.infinite===true){
if(index >=centerOffset&&index <=(_.slideCount - 1) - centerOffset){
_.$slides
.slice(index - centerOffset + evenCoef, index + centerOffset + 1)
.addClass('slick-active')
.attr('aria-hidden', 'false');
}else{
indexOffset=_.options.slidesToShow + index;
allSlides
.slice(indexOffset - centerOffset + 1 + evenCoef, indexOffset + centerOffset + 2)
.addClass('slick-active')
.attr('aria-hidden', 'false');
}
if(index===0){
allSlides
.eq(allSlides.length - 1 - _.options.slidesToShow)
.addClass('slick-center');
}else if(index===_.slideCount - 1){
allSlides
.eq(_.options.slidesToShow)
.addClass('slick-center');
}}
_.$slides
.eq(index)
.addClass('slick-center');
}else{
if(index >=0&&index <=(_.slideCount - _.options.slidesToShow)){
_.$slides
.slice(index, index + _.options.slidesToShow)
.addClass('slick-active')
.attr('aria-hidden', 'false');
}else if(allSlides.length <=_.options.slidesToShow){
allSlides
.addClass('slick-active')
.attr('aria-hidden', 'false');
}else{
remainder=_.slideCount % _.options.slidesToShow;
indexOffset=_.options.infinite===true ? _.options.slidesToShow + index:index;
if(_.options.slidesToShow==_.options.slidesToScroll&&(_.slideCount - index) < _.options.slidesToShow){
allSlides
.slice(indexOffset - (_.options.slidesToShow - remainder), indexOffset + remainder)
.addClass('slick-active')
.attr('aria-hidden', 'false');
}else{
allSlides
.slice(indexOffset, indexOffset + _.options.slidesToShow)
.addClass('slick-active')
.attr('aria-hidden', 'false');
}}
}
if(_.options.lazyLoad==='ondemand'||_.options.lazyLoad==='anticipated'){
_.lazyLoad();
}};
Slick.prototype.setupInfinite=function(){
var _=this,
i, slideIndex, infiniteCount;
if(_.options.fade===true){
_.options.centerMode=false;
}
if(_.options.infinite===true&&_.options.fade===false){
slideIndex=null;
if(_.slideCount > _.options.slidesToShow){
if(_.options.centerMode===true){
infiniteCount=_.options.slidesToShow + 1;
}else{
infiniteCount=_.options.slidesToShow;
}
for (i=_.slideCount; i > (_.slideCount -
infiniteCount); i -=1){
slideIndex=i - 1;
$(_.$slides[slideIndex]).clone(true).attr('id', '')
.attr('data-slick-index', slideIndex - _.slideCount)
.prependTo(_.$slideTrack).addClass('slick-cloned');
}
for (i=0; i < infiniteCount  + _.slideCount; i +=1){
slideIndex=i;
$(_.$slides[slideIndex]).clone(true).attr('id', '')
.attr('data-slick-index', slideIndex + _.slideCount)
.appendTo(_.$slideTrack).addClass('slick-cloned');
}
_.$slideTrack.find('.slick-cloned').find('[id]').each(function(){
$(this).attr('id', '');
});
}}
};
Slick.prototype.interrupt=function(toggle){
var _=this;
if(!toggle){
_.autoPlay();
}
_.interrupted=toggle;
};
Slick.prototype.selectHandler=function(event){
var _=this;
var targetElement =
$(event.target).is('.slick-slide') ?
$(event.target) :
$(event.target).parents('.slick-slide');
var index=parseInt(targetElement.attr('data-slick-index'));
if(!index) index=0;
if(_.slideCount <=_.options.slidesToShow){
_.slideHandler(index, false, true);
return;
}
_.slideHandler(index);
};
Slick.prototype.slideHandler=function(index, sync, dontAnimate){
var targetSlide, animSlide, oldSlide, slideLeft, targetLeft=null,
_=this, navTarget;
sync=sync||false;
if(_.animating===true&&_.options.waitForAnimate===true){
return;
}
if(_.options.fade===true&&_.currentSlide===index){
return;
}
if(sync===false){
_.asNavFor(index);
}
targetSlide=index;
targetLeft=_.getLeft(targetSlide);
slideLeft=_.getLeft(_.currentSlide);
_.currentLeft=_.swipeLeft===null ? slideLeft:_.swipeLeft;
if(_.options.infinite===false&&_.options.centerMode===false&&(index < 0||index > _.getDotCount() * _.options.slidesToScroll)){
if(_.options.fade===false){
targetSlide=_.currentSlide;
if(dontAnimate!==true&&_.slideCount > _.options.slidesToShow){
_.animateSlide(slideLeft, function(){
_.postSlide(targetSlide);
});
}else{
_.postSlide(targetSlide);
}}
return;
}else if(_.options.infinite===false&&_.options.centerMode===true&&(index < 0||index > (_.slideCount - _.options.slidesToScroll))){
if(_.options.fade===false){
targetSlide=_.currentSlide;
if(dontAnimate!==true&&_.slideCount > _.options.slidesToShow){
_.animateSlide(slideLeft, function(){
_.postSlide(targetSlide);
});
}else{
_.postSlide(targetSlide);
}}
return;
}
if(_.options.autoplay){
clearInterval(_.autoPlayTimer);
}
if(targetSlide < 0){
if(_.slideCount % _.options.slidesToScroll!==0){
animSlide=_.slideCount - (_.slideCount % _.options.slidesToScroll);
}else{
animSlide=_.slideCount + targetSlide;
}}else if(targetSlide >=_.slideCount){
if(_.slideCount % _.options.slidesToScroll!==0){
animSlide=0;
}else{
animSlide=targetSlide - _.slideCount;
}}else{
animSlide=targetSlide;
}
_.animating=true;
_.$slider.trigger('beforeChange', [_, _.currentSlide, animSlide]);
oldSlide=_.currentSlide;
_.currentSlide=animSlide;
_.setSlideClasses(_.currentSlide);
if(_.options.asNavFor){
navTarget=_.getNavTarget();
navTarget=navTarget.slick('getSlick');
if(navTarget.slideCount <=navTarget.options.slidesToShow){
navTarget.setSlideClasses(_.currentSlide);
}}
_.updateDots();
_.updateArrows();
if(_.options.fade===true){
if(dontAnimate!==true){
_.fadeSlideOut(oldSlide);
_.fadeSlide(animSlide, function(){
_.postSlide(animSlide);
});
}else{
_.postSlide(animSlide);
}
_.animateHeight();
return;
}
if(dontAnimate!==true&&_.slideCount > _.options.slidesToShow){
_.animateSlide(targetLeft, function(){
_.postSlide(animSlide);
});
}else{
_.postSlide(animSlide);
}};
Slick.prototype.startLoad=function(){
var _=this;
if(_.options.arrows===true&&_.slideCount > _.options.slidesToShow){
_.$prevArrow.hide();
_.$nextArrow.hide();
}
if(_.options.dots===true&&_.slideCount > _.options.slidesToShow){
_.$dots.hide();
}
_.$slider.addClass('slick-loading');
};
Slick.prototype.swipeDirection=function(){
var xDist, yDist, r, swipeAngle, _=this;
xDist=_.touchObject.startX - _.touchObject.curX;
yDist=_.touchObject.startY - _.touchObject.curY;
r=Math.atan2(yDist, xDist);
swipeAngle=Math.round(r * 180 / Math.PI);
if(swipeAngle < 0){
swipeAngle=360 - Math.abs(swipeAngle);
}
if((swipeAngle <=45)&&(swipeAngle >=0)){
return (_.options.rtl===false ? 'left':'right');
}
if((swipeAngle <=360)&&(swipeAngle >=315)){
return (_.options.rtl===false ? 'left':'right');
}
if((swipeAngle >=135)&&(swipeAngle <=225)){
return (_.options.rtl===false ? 'right':'left');
}
if(_.options.verticalSwiping===true){
if((swipeAngle >=35)&&(swipeAngle <=135)){
return 'down';
}else{
return 'up';
}}
return 'vertical';
};
Slick.prototype.swipeEnd=function(event){
var _=this,
slideCount,
direction;
_.dragging=false;
_.swiping=false;
if(_.scrolling){
_.scrolling=false;
return false;
}
_.interrupted=false;
_.shouldClick=(_.touchObject.swipeLength > 10) ? false:true;
if(_.touchObject.curX===undefined){
return false;
}
if(_.touchObject.edgeHit===true){
_.$slider.trigger('edge', [_, _.swipeDirection() ]);
}
if(_.touchObject.swipeLength >=_.touchObject.minSwipe){
direction=_.swipeDirection();
switch(direction){
case 'left':
case 'down':
slideCount =
_.options.swipeToSlide ?
_.checkNavigable(_.currentSlide + _.getSlideCount()) :
_.currentSlide + _.getSlideCount();
_.currentDirection=0;
break;
case 'right':
case 'up':
slideCount =
_.options.swipeToSlide ?
_.checkNavigable(_.currentSlide - _.getSlideCount()) :
_.currentSlide - _.getSlideCount();
_.currentDirection=1;
break;
default:
}
if(direction!='vertical'){
_.slideHandler(slideCount);
_.touchObject={};
_.$slider.trigger('swipe', [_, direction ]);
}}else{
if(_.touchObject.startX!==_.touchObject.curX){
_.slideHandler(_.currentSlide);
_.touchObject={};}}
};
Slick.prototype.swipeHandler=function(event){
var _=this;
if((_.options.swipe===false)||('ontouchend' in document&&_.options.swipe===false)){
return;
}else if(_.options.draggable===false&&event.type.indexOf('mouse')!==-1){
return;
}
_.touchObject.fingerCount=event.originalEvent&&event.originalEvent.touches!==undefined ?
event.originalEvent.touches.length:1;
_.touchObject.minSwipe=_.listWidth / _.options
.touchThreshold;
if(_.options.verticalSwiping===true){
_.touchObject.minSwipe=_.listHeight / _.options
.touchThreshold;
}
switch (event.data.action){
case 'start':
_.swipeStart(event);
break;
case 'move':
_.swipeMove(event);
break;
case 'end':
_.swipeEnd(event);
break;
}};
Slick.prototype.swipeMove=function(event){
var _=this,
edgeWasHit=false,
curLeft, swipeDirection, swipeLength, positionOffset, touches, verticalSwipeLength;
touches=event.originalEvent!==undefined ? event.originalEvent.touches:null;
if(!_.dragging||_.scrolling||touches&&touches.length!==1){
return false;
}
curLeft=_.getLeft(_.currentSlide);
_.touchObject.curX=touches!==undefined ? touches[0].pageX:event.clientX;
_.touchObject.curY=touches!==undefined ? touches[0].pageY:event.clientY;
_.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(_.touchObject.curX - _.touchObject.startX, 2)));
verticalSwipeLength=Math.round(Math.sqrt(Math.pow(_.touchObject.curY - _.touchObject.startY, 2)));
if(!_.options.verticalSwiping&&!_.swiping&&verticalSwipeLength > 4){
_.scrolling=true;
return false;
}
if(_.options.verticalSwiping===true){
_.touchObject.swipeLength=verticalSwipeLength;
}
swipeDirection=_.swipeDirection();
if(event.originalEvent!==undefined&&_.touchObject.swipeLength > 4){
_.swiping=true;
event.preventDefault();
}
positionOffset=(_.options.rtl===false ? 1:-1) * (_.touchObject.curX > _.touchObject.startX ? 1:-1);
if(_.options.verticalSwiping===true){
positionOffset=_.touchObject.curY > _.touchObject.startY ? 1:-1;
}
swipeLength=_.touchObject.swipeLength;
_.touchObject.edgeHit=false;
if(_.options.infinite===false){
if((_.currentSlide===0&&swipeDirection==='right')||(_.currentSlide >=_.getDotCount()&&swipeDirection==='left')){
swipeLength=_.touchObject.swipeLength * _.options.edgeFriction;
_.touchObject.edgeHit=true;
}}
if(_.options.vertical===false){
_.swipeLeft=curLeft + swipeLength * positionOffset;
}else{
_.swipeLeft=curLeft + (swipeLength * (_.$list.height() / _.listWidth)) * positionOffset;
}
if(_.options.verticalSwiping===true){
_.swipeLeft=curLeft + swipeLength * positionOffset;
}
if(_.options.fade===true||_.options.touchMove===false){
return false;
}
if(_.animating===true){
_.swipeLeft=null;
return false;
}
_.setCSS(_.swipeLeft);
};
Slick.prototype.swipeStart=function(event){
var _=this,
touches;
_.interrupted=true;
if(_.touchObject.fingerCount!==1||_.slideCount <=_.options.slidesToShow){
_.touchObject={};
return false;
}
if(event.originalEvent!==undefined&&event.originalEvent.touches!==undefined){
touches=event.originalEvent.touches[0];
}
_.touchObject.startX=_.touchObject.curX=touches!==undefined ? touches.pageX:event.clientX;
_.touchObject.startY=_.touchObject.curY=touches!==undefined ? touches.pageY:event.clientY;
_.dragging=true;
};
Slick.prototype.unfilterSlides=Slick.prototype.slickUnfilter=function(){
var _=this;
if(_.$slidesCache!==null){
_.unload();
_.$slideTrack.children(this.options.slide).detach();
_.$slidesCache.appendTo(_.$slideTrack);
_.reinit();
}};
Slick.prototype.unload=function(){
var _=this;
$('.slick-cloned', _.$slider).remove();
if(_.$dots){
_.$dots.remove();
}
if(_.$prevArrow&&_.htmlExpr.test(_.options.prevArrow)){
_.$prevArrow.remove();
}
if(_.$nextArrow&&_.htmlExpr.test(_.options.nextArrow)){
_.$nextArrow.remove();
}
_.$slides
.removeClass('slick-slide slick-active slick-visible slick-current')
.attr('aria-hidden', 'true')
.css('width', '');
};
Slick.prototype.unslick=function(fromBreakpoint){
var _=this;
_.$slider.trigger('unslick', [_, fromBreakpoint]);
_.destroy();
};
Slick.prototype.updateArrows=function(){
var _=this,
centerOffset;
centerOffset=Math.floor(_.options.slidesToShow / 2);
if(_.options.arrows===true &&
_.slideCount > _.options.slidesToShow &&
!_.options.infinite){
_.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
_.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
if(_.currentSlide===0){
_.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
_.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
}else if(_.currentSlide >=_.slideCount - _.options.slidesToShow&&_.options.centerMode===false){
_.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
_.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
}else if(_.currentSlide >=_.slideCount - 1&&_.options.centerMode===true){
_.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
_.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
}}
};
Slick.prototype.updateDots=function(){
var _=this;
if(_.$dots!==null){
_.$dots
.find('li')
.removeClass('slick-active')
.end();
_.$dots
.find('li')
.eq(Math.floor(_.currentSlide / _.options.slidesToScroll))
.addClass('slick-active');
}};
Slick.prototype.visibility=function(){
var _=this;
if(_.options.autoplay){
if(document[_.hidden]){
_.interrupted=true;
}else{
_.interrupted=false;
}}
};
$.fn.slick=function(){
var _=this,
opt=arguments[0],
args=Array.prototype.slice.call(arguments, 1),
l=_.length,
i,
ret;
for (i=0; i < l; i++){
if(typeof opt=='object'||typeof opt=='undefined')
_[i].slick=new Slick(_[i], opt);
else
ret=_[i].slick[opt].apply(_[i].slick, args);
if(typeof ret!='undefined') return ret;
}
return _;
};}));
},{"jquery":1}],4:[function(require,module,exports){
"use strict";
var _jquery=_interopRequireDefault(require("jquery"));
function _interopRequireDefault(obj){ return obj&&obj.__esModule ? obj:{ default: obj };}
window.jQuery=_jquery.default;
window.$=_jquery.default;
},{"jquery":1}],5:[function(require,module,exports){
"use strict";
require("./_jquery");
var ko=_interopRequireWildcard(require("knockout"));
require("slick-carousel");
function _interopRequireWildcard(obj){ if(obj&&obj.__esModule){ return obj; }else{ var newObj={}; if(obj!=null){ for (var key in obj){ if(Object.prototype.hasOwnProperty.call(obj, key)){ var desc=Object.defineProperty&&Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key):{}; if(desc.get||desc.set){ Object.defineProperty(newObj, key, desc); }else{ newObj[key]=obj[key]; }} }} newObj.default=obj; return newObj; }}
function _classCallCheck(instance, Constructor){ if(!(instance instanceof Constructor)){ throw new TypeError("Cannot call a class as a function"); }}
function _defineProperties(target, props){ for (var i=0; i < props.length; i++){ var descriptor=props[i]; descriptor.enumerable=descriptor.enumerable||false; descriptor.configurable=true; if("value" in descriptor) descriptor.writable=true; Object.defineProperty(target, descriptor.key, descriptor); }}
function _createClass(Constructor, protoProps, staticProps){ if(protoProps) _defineProperties(Constructor.prototype, protoProps); if(staticProps) _defineProperties(Constructor, staticProps); return Constructor; }
var App =
function (){
function App(){
var _this=this;
_classCallCheck(this, App);
this.scrolledTop=ko.observable(0);
this.menuOpen=ko.observable(false);
this.membershiptype=ko.observable('monthly');
this.loading=ko.observable(false);
this.firstname=ko.observable("");
this.lastname=ko.observable("");
this.email=ko.observable("");
this.phone=ko.observable("");
this.jobtitle=ko.observable("");
this.company=ko.observable("");
this.message=ko.observable("");
this.toc=ko.observable(false);
this.membership=ko.observable("");
this.formSubmitted=ko.observable(false);
this.formError=ko.observable(false);
this.formErrorMessage=ko.observable("");
$(window).on('scroll', function (){
return _this.scrolledTop($(window).scrollTop());
});
this.scrolledTop($(window).scrollTop());
$(".js-membership-elite .elementor-button-text").on("click", function (){
return _this.membership("membership-elite");
});
$(".js-membership-pro .elementor-button-text").on("click", function (){
return _this.membership("membership-pro");
});
$(".js-membership-starter .elementor-button-text").on("click", function (){
return _this.membership("membership-starter");
});
$(window).on('submit', function (){
_this.loading(true);
});
$(window).on('wpcf7mailsent', function (){
$('.form__wrapper').remove();
});
$(window).on('wpcf7invalid', function (){
_this.loading(false);
});
this.formSubmitted.subscribe(function (){
if($('.modal.fade.in')){
$('.modal .hide-on-form-submit').hide();
$('.modal .show-on-form-submit').show();
}else{
$('.hide-on-form-submit').hide();
$('.show-on-form-submit').show();
}});
}
_createClass(App, [{
key: "setLoading",
value: function setLoading(){
this.loading(true);
}}, {
key: "toggleMenu",
value: function toggleMenu(){
this.menuOpen(!this.menuOpen());
}}, {
key: "submit",
value: function submit(url, listid){
var _this2=this;
this.loading(true);
var info={
listid: listid,
firstname: this.firstname(),
lastname: this.lastname(),
email: this.email(),
phone: this.phone(),
jobtitle: this.jobtitle(),
company: this.company()
};
if(listid==2){
info['tags']=this.membership() + "," + this.membershiptype();
}
form(url, info).then(function (){
_this2.formSubmitted(true);
}).catch(function (response){
_this2.formErrorMessage(response);
_this2.formError(true);
}).finally(function (){
_this2.loading(false);
});
}}]);
return App;
}();
var form=function form(url, info){
return new Promise(function (resolve, reject){
var newXHR=new XMLHttpRequest();
newXHR.addEventListener('load', function (e){
if(e.currentTarget.response==="SUCCESS"){
resolve();
}else{
reject(e.currentTarget.response);
}});
newXHR.open('POST', url);
var data=new FormData();
Object.keys(info).forEach(function (obj){
data.append(obj, info[obj]);
});
newXHR.send(data);
});
};
ko.applyBindings(new App());
},{"./_jquery":4,"knockout":2,"slick-carousel":3}]},{},[5]);
jQuery(document).ready(function ($){
$('.zoom-social_icons-list__link').on({
'mouseenter': function (e){
e.preventDefault();
var $this=$(this).find('.zoom-social_icons-list-span');
var $rule=$this.data('hover-rule');
var $color=$this.data('hover-color');
if($color!==undefined){
$this.attr('data-old-color', $this.css($rule));
$this.css($rule, $color);
}},
'mouseleave': function (e){
e.preventDefault();
var $this=$(this).find('.zoom-social_icons-list-span');
var $rule=$this.data('hover-rule');
var $oldColor=$this.data('old-color');
if($oldColor!==undefined){
$this.css($rule, $oldColor);
}}
});
});
!function(a,b){"use strict";function c(){if(!e){e=!0;var a,c,d,f,g=-1!==navigator.appVersion.indexOf("MSIE 10"),h=!!navigator.userAgent.match(/Trident.*rv:11\./),i=b.querySelectorAll("iframe.wp-embedded-content");for(c=0;c<i.length;c++){if(d=i[c],!d.getAttribute("data-secret"))f=Math.random().toString(36).substr(2,10),d.src+="#?secret="+f,d.setAttribute("data-secret",f);if(g||h)a=d.cloneNode(!0),a.removeAttribute("security"),d.parentNode.replaceChild(a,d)}}}var d=!1,e=!1;if(b.querySelector)if(a.addEventListener)d=!0;if(a.wp=a.wp||{},!a.wp.receiveEmbedMessage)if(a.wp.receiveEmbedMessage=function(c){var d=c.data;if(d)if(d.secret||d.message||d.value)if(!/[^a-zA-Z0-9]/.test(d.secret)){var e,f,g,h,i,j=b.querySelectorAll('iframe[data-secret="'+d.secret+'"]'),k=b.querySelectorAll('blockquote[data-secret="'+d.secret+'"]');for(e=0;e<k.length;e++)k[e].style.display="none";for(e=0;e<j.length;e++)if(f=j[e],c.source===f.contentWindow){if(f.removeAttribute("style"),"height"===d.message){if(g=parseInt(d.value,10),g>1e3)g=1e3;else if(~~g<200)g=200;f.height=g}if("link"===d.message)if(h=b.createElement("a"),i=b.createElement("a"),h.href=f.getAttribute("src"),i.href=d.value,i.host===h.host)if(b.activeElement===f)a.top.location.href=d.value}else;}},d)a.addEventListener("message",a.wp.receiveEmbedMessage,!1),b.addEventListener("DOMContentLoaded",c,!1),a.addEventListener("load",c,!1)}(window,document);
(function($){var Sticky=function(element,userSettings){var $element,isSticky=false,isFollowingParent=false,isReachedEffectsPoint=false,elements={},settings;var defaultSettings={to:"top",offset:0,effectsOffset:0,parent:false,classes:{sticky:"sticky",stickyActive:"sticky-active",stickyEffects:"sticky-effects",spacer:"sticky-spacer"}};var initElements=function(){$element=$(element).addClass(settings.classes.sticky);elements.$window=$(window);if(settings.parent){if("parent"===settings.parent){elements.$parent=$element.parent()}else{elements.$parent=$element.closest(settings.parent)}}};var initSettings=function(){settings=jQuery.extend(true,defaultSettings,userSettings)};var bindEvents=function(){elements.$window.on({scroll:onWindowScroll,resize:onWindowResize})};var unbindEvents=function(){elements.$window.off("scroll",onWindowScroll).off("resize",onWindowResize)};var init=function(){initSettings();initElements();bindEvents();checkPosition()};var backupCSS=function($element,backupState,properties){var css={},elementStyle=$element[0].style;properties.forEach(function(property){css[property]=undefined!==elementStyle[property]?elementStyle[property]:""});$element.data("css-backup-"+backupState,css)};var getCSSBackup=function($element,backupState){return $element.data("css-backup-"+backupState)};var addSpacer=function(){elements.$spacer=$element.clone().addClass(settings.classes.spacer).css({visibility:"hidden",transition:"none",animation:"none"});$element.after(elements.$spacer)};var removeSpacer=function(){elements.$spacer.remove()};var stickElement=function(){backupCSS($element,"unsticky",["position","width","margin-top","margin-bottom","top","bottom"]);var css={position:"fixed",width:getElementOuterSize($element,"width"),marginTop:0,marginBottom:0};css[settings.to]=settings.offset;css["top"===settings.to?"bottom":"top"]="";$element.css(css).addClass(settings.classes.stickyActive)};var unstickElement=function(){$element.css(getCSSBackup($element,"unsticky")).removeClass(settings.classes.stickyActive)};var followParent=function(){backupCSS(elements.$parent,"childNotFollowing",["position"]);elements.$parent.css("position","relative");backupCSS($element,"notFollowing",["position","top","bottom"]);var css={position:"absolute"};css[settings.to]="";css["top"===settings.to?"bottom":"top"]=0;$element.css(css);isFollowingParent=true};var unfollowParent=function(){elements.$parent.css(getCSSBackup(elements.$parent,"childNotFollowing"));$element.css(getCSSBackup($element,"notFollowing"));isFollowingParent=false};var getElementOuterSize=function($element,dimension,includeMargins){var computedStyle=getComputedStyle($element[0]),elementSize=parseFloat(computedStyle[dimension]),sides="height"===dimension?["top","bottom"]:["left","right"],propertiesToAdd=[];if("border-box"!==computedStyle.boxSizing){propertiesToAdd.push("border","padding")}if(includeMargins){propertiesToAdd.push("margin")}propertiesToAdd.forEach(function(property){sides.forEach(function(side){elementSize+=parseFloat(computedStyle[property+"-"+side])})});return elementSize};var getElementViewportOffset=function($element){var windowScrollTop=elements.$window.scrollTop(),elementHeight=getElementOuterSize($element,"height"),viewportHeight=innerHeight,elementOffsetFromTop=$element.offset().top,distanceFromTop=elementOffsetFromTop-windowScrollTop,topFromBottom=distanceFromTop-viewportHeight;return{top:{fromTop:distanceFromTop,fromBottom:topFromBottom},bottom:{fromTop:distanceFromTop+elementHeight,fromBottom:topFromBottom+elementHeight}}};var stick=function(){addSpacer();stickElement();isSticky=true;$element.trigger("sticky:stick")};var unstick=function(){unstickElement();removeSpacer();isSticky=false;$element.trigger("sticky:unstick")};var checkParent=function(){var elementOffset=getElementViewportOffset($element),isTop="top"===settings.to;if(isFollowingParent){var isNeedUnfollowing=isTop?elementOffset.top.fromTop>settings.offset:elementOffset.bottom.fromBottom<-settings.offset;if(isNeedUnfollowing){unfollowParent()}}else{var parentOffset=getElementViewportOffset(elements.$parent),parentStyle=getComputedStyle(elements.$parent[0]),borderWidthToDecrease=parseFloat(parentStyle[isTop?"borderBottomWidth":"borderTopWidth"]),parentViewportDistance=isTop?parentOffset.bottom.fromTop-borderWidthToDecrease:parentOffset.top.fromBottom+borderWidthToDecrease,isNeedFollowing=isTop?parentViewportDistance<=elementOffset.bottom.fromTop:parentViewportDistance>=elementOffset.top.fromBottom;if(isNeedFollowing){followParent()}}};var checkEffectsPoint=function(distanceFromTriggerPoint){if(isReachedEffectsPoint&&-distanceFromTriggerPoint<settings.effectsOffset){$element.removeClass(settings.classes.stickyEffects);isReachedEffectsPoint=false}else if(!isReachedEffectsPoint&&-distanceFromTriggerPoint>=settings.effectsOffset){$element.addClass(settings.classes.stickyEffects);isReachedEffectsPoint=true}};var checkPosition=function(){var offset=settings.offset,distanceFromTriggerPoint;if(isSticky){var spacerViewportOffset=getElementViewportOffset(elements.$spacer);distanceFromTriggerPoint="top"===settings.to?spacerViewportOffset.top.fromTop-offset:-spacerViewportOffset.bottom.fromBottom-offset;if(settings.parent){checkParent()}if(distanceFromTriggerPoint>0){unstick()}}else{var elementViewportOffset=getElementViewportOffset($element);distanceFromTriggerPoint="top"===settings.to?elementViewportOffset.top.fromTop-offset:-elementViewportOffset.bottom.fromBottom-offset;if(distanceFromTriggerPoint<=0){stick();if(settings.parent){checkParent()}}}checkEffectsPoint(distanceFromTriggerPoint)};var onWindowScroll=function(){checkPosition()};var onWindowResize=function(){if(!isSticky){return}unstickElement();stickElement()};this.destroy=function(){if(isSticky){unstick()}unbindEvents();$element.removeClass(settings.classes.sticky)};init()};$.fn.sticky=function(settings){var isCommand="string"===typeof settings;this.each(function(){var $this=$(this);if(!isCommand){$this.data("sticky",new Sticky(this,settings));return}var instance=$this.data("sticky");if(!instance){throw Error("Trying to perform the `"+settings+"` method prior to initialization")}if(!instance[settings]){throw ReferenceError("Method `"+settings+"` not found in sticky instance")}instance[settings].apply(instance,Array.prototype.slice.call(arguments,1));if("destroy"===settings){$this.removeData("sticky")}});return this};window.Sticky=Sticky})(jQuery);
!function(e){var t={};function n(i){if(t[i])return t[i].exports;var s=t[i]={i:i,l:!1,exports:{}};return e[i].call(s.exports,s,s.exports,n),s.l=!0,s.exports}n.m=e,n.c=t,n.d=function(e,t,i){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:i})},n.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var i=Object.create(null);if(n.r(i),Object.defineProperty(i,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var s in e)n.d(i,s,function(t){return e[t]}.bind(null,s));return i},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,"a",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p="",n(n.s=65)}([,,,function(e,t,n){"use strict";e.exports=elementorFrontend.Module.extend({getElementName:function(){return"posts"},getSkinPrefix:function(){return"classic_"},bindEvents:function(){var e=this.getModelCID();elementorFrontend.addListenerOnce(e,"resize",this.onWindowResize)},getClosureMethodsNames:function(){return elementorFrontend.Module.prototype.getClosureMethodsNames.apply(this,arguments).concat(["fitImages","onWindowResize","runMasonry"])},getDefaultSettings:function(){return{classes:{fitHeight:"elementor-fit-height",hasItemRatio:"elementor-has-item-ratio"},selectors:{postsContainer:".elementor-posts-container",post:".elementor-post",postThumbnail:".elementor-post__thumbnail",postThumbnailImage:".elementor-post__thumbnail img"}}},getDefaultElements:function(){var e=this.getSettings("selectors");return{$postsContainer:this.$element.find(e.postsContainer),$posts:this.$element.find(e.post)}},fitImage:function(e){var t=this.getSettings(),n=e.find(t.selectors.postThumbnail),i=n.find("img")[0];if(i){var s=n.outerHeight()/n.outerWidth(),o=i.naturalHeight/i.naturalWidth;n.toggleClass(t.classes.fitHeight,o<s)}},fitImages:function(){var e=jQuery,t=this,n=getComputedStyle(this.$element[0],":after").content,i=this.getSettings();this.elements.$postsContainer.toggleClass(i.classes.hasItemRatio,!!n.match(/\d/)),t.isMasonryEnabled()||this.elements.$posts.each(function(){var n=e(this),s=n.find(i.selectors.postThumbnailImage);t.fitImage(n),s.on("load",function(){t.fitImage(n)})})},setColsCountSettings:function(){var e,t=elementorFrontend.getCurrentDeviceMode(),n=this.getElementSettings(),i=this.getSkinPrefix();switch(t){case"mobile":e=n[i+"columns_mobile"];break;case"tablet":e=n[i+"columns_tablet"];break;default:e=n[i+"columns"]}this.setSettings("colsCount",e)},isMasonryEnabled:function(){return!!this.getElementSettings(this.getSkinPrefix()+"masonry")},initMasonry:function(){imagesLoaded(this.elements.$posts,this.runMasonry)},runMasonry:function(){var e=this.elements;e.$posts.css({marginTop:"",transitionDuration:""}),this.setColsCountSettings();var t=this.getSettings("colsCount"),n=this.isMasonryEnabled()&&t>=2;if(e.$postsContainer.toggleClass("elementor-posts-masonry",n),n){var i=this.getElementSettings(this.getSkinPrefix()+"row_gap.size");""===this.getSkinPrefix()&&""===i&&(i=this.getElementSettings(this.getSkinPrefix()+"item_gap.size")),new elementorFrontend.modules.Masonry({container:e.$postsContainer,items:e.$posts.filter(":visible"),columnsCount:this.getSettings("colsCount"),verticalSpaceBetween:i}).run()}else e.$postsContainer.height("")},run:function(){setTimeout(this.fitImages,0),this.initMasonry()},onInit:function(){elementorFrontend.Module.prototype.onInit.apply(this,arguments),this.bindEvents(),this.run()},onWindowResize:function(){this.fitImages(),this.runMasonry()},onElementChange:function(){this.fitImages(),setTimeout(this.runMasonry)}})},function(e,t,n){"use strict";var i=n(67).extend(),s=n(68);e.exports=function(e){new i({$element:e}),new s({$element:e})}},function(e,t,n){"use strict";var i=n(3);e.exports=i.extend({getSkinPrefix:function(){return"cards_"}})},function(e,t,n){"use strict";e.exports=elementorFrontend.Module.extend({getDefaultSettings:function(){return{selectors:{mainSwiper:".elementor-main-swiper",swiperSlide:".swiper-slide"},slidesPerView:{desktop:3,tablet:2,mobile:1}}},getDefaultElements:function(){var e=this.getSettings("selectors"),t={$mainSwiper:this.$element.find(e.mainSwiper)};return t.$mainSwiperSlides=t.$mainSwiper.find(e.swiperSlide),t},getSlidesCount:function(){return this.elements.$mainSwiperSlides.length},getInitialSlide:function(){var e=this.getEditSettings();return e.activeItemIndex?e.activeItemIndex-1:0},getEffect:function(){return this.getElementSettings("effect")},getDeviceSlidesPerView:function(e){var t="slides_per_view"+("desktop"===e?"":"_"+e);return Math.min(this.getSlidesCount(),+this.getElementSettings(t)||this.getSettings("slidesPerView")[e])},getSlidesPerView:function(e){return"slide"===this.getEffect()?this.getDeviceSlidesPerView(e):1},getDesktopSlidesPerView:function(){return this.getSlidesPerView("desktop")},getTabletSlidesPerView:function(){return this.getSlidesPerView("tablet")},getMobileSlidesPerView:function(){return this.getSlidesPerView("mobile")},getDeviceSlidesToScroll:function(e){var t="slides_to_scroll"+("desktop"===e?"":"_"+e);return Math.min(this.getSlidesCount(),+this.getElementSettings(t)||1)},getSlidesToScroll:function(e){return"slide"===this.getEffect()?this.getDeviceSlidesToScroll(e):1},getDesktopSlidesToScroll:function(){return this.getSlidesToScroll("desktop")},getTabletSlidesToScroll:function(){return this.getSlidesToScroll("tablet")},getMobileSlidesToScroll:function(){return this.getSlidesToScroll("mobile")},getSpaceBetween:function(e){var t="space_between";return e&&"desktop"!==e&&(t+="_"+e),this.getElementSettings(t).size||0},getSwiperOptions:function(){var e=this.getElementSettings();"progress"===e.pagination&&(e.pagination="progressbar");var t={grabCursor:!0,initialSlide:this.getInitialSlide(),slidesPerView:this.getDesktopSlidesPerView(),slidesPerGroup:this.getDesktopSlidesToScroll(),spaceBetween:this.getSpaceBetween(),loop:"yes"===e.loop,speed:e.speed,effect:this.getEffect()};if(e.show_arrows&&(t.navigation={prevEl:".elementor-swiper-button-prev",nextEl:".elementor-swiper-button-next"}),e.pagination&&(t.pagination={el:".swiper-pagination",type:e.pagination,clickable:!0}),"cube"!==this.getEffect()){var n={},i=elementorFrontend.config.breakpoints;n[i.lg-1]={slidesPerView:this.getTabletSlidesPerView(),slidesPerGroup:this.getTabletSlidesToScroll(),spaceBetween:this.getSpaceBetween("tablet")},n[i.md-1]={slidesPerView:this.getMobileSlidesPerView(),slidesPerGroup:this.getMobileSlidesToScroll(),spaceBetween:this.getSpaceBetween("mobile")},t.breakpoints=n}return!this.isEdit&&e.autoplay&&(t.autoplay={delay:e.autoplay_speed,disableOnInteraction:!!e.pause_on_interaction}),t},updateSpaceBetween:function(e,t){var n=t.match("space_between_(.*)"),i=n?n[1]:"desktop",s=this.getSpaceBetween(i),o=elementorFrontend.config.breakpoints;if("desktop"!==i){var r={tablet:o.lg-1,mobile:o.md-1};e.params.breakpoints[r[i]].spaceBetween=s}else e.originalParams.spaceBetween=s;e.params.spaceBetween=s,e.update()},onInit:function(){elementorFrontend.Module.prototype.onInit.apply(this,arguments),this.swipers={},1>=this.getSlidesCount()||(this.swipers.main=new Swiper(this.elements.$mainSwiper,this.getSwiperOptions()))},onElementChange:function(e){1>=this.getSlidesCount()||(0===e.indexOf("width")&&this.swipers.main.update(),0===e.indexOf("space_between")&&this.updateSpaceBetween(this.swipers.main,e))},onEditSettingsChange:function(e){1>=this.getSlidesCount()||"activeItemIndex"===e&&this.swipers.main.slideToLoop(this.getEditSettings("activeItemIndex")-1)}})},function(e,t,n){"use strict";var i,s=n(6);i=s.extend({getDefaultSettings:function(){var e=s.prototype.getDefaultSettings.apply(this,arguments);return e.slidesPerView={desktop:1,tablet:1,mobile:1},e},getEffect:function(){return"slide"}}),e.exports=function(e){new i({$element:e})}},function(e,t,n){"use strict";var i=elementorFrontend.Module.extend({bindEvents:function(){elementorFrontend.addListenerOnce(this.getUniqueHandlerID()+"sticky","resize",this.run)},unbindEvents:function(){elementorFrontend.removeListeners(this.getUniqueHandlerID()+"sticky","resize",this.run)},isActive:function(){return void 0!==this.$element.data("sticky")},activate:function(){var e=this.getElementSettings(),t={to:e.sticky,offset:e.sticky_offset,effectsOffset:e.sticky_effects_offset,classes:{sticky:"elementor-sticky",stickyActive:"elementor-sticky--active elementor-section--handles-inside",stickyEffects:"elementor-sticky--effects",spacer:"elementor-sticky__spacer"}},n=elementorFrontend.getElements("$wpAdminBar");e.sticky_parent&&(t.parent=".elementor-widget-wrap"),n.length&&"top"===e.sticky&&"fixed"===n.css("position")&&(t.offset+=n.height()),this.$element.sticky(t)},deactivate:function(){this.isActive()&&this.$element.sticky("destroy")},run:function(e){if(this.getElementSettings("sticky")){var t=elementorFrontend.getCurrentDeviceMode();-1!==this.getElementSettings("sticky_on").indexOf(t)?!0===e?this.reactivate():this.isActive()||this.activate():this.deactivate()}else this.deactivate()},reactivate:function(){this.deactivate(),this.activate()},onElementChange:function(e){-1!==["sticky","sticky_on"].indexOf(e)&&this.run(!0),-1!==["sticky_offset","sticky_effects_offset","sticky_parent"].indexOf(e)&&this.reactivate()},onInit:function(){elementorFrontend.Module.prototype.onInit.apply(this,arguments),this.run()},onDestroy:function(){elementorFrontend.Module.prototype.onDestroy.apply(this,arguments),this.deactivate()}});e.exports=function(e){new i({$element:e})}},,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,function(e,t,n){"use strict";window.elementorProFrontend=new function(e){var t=this;this.config=ElementorProFrontendConfig,this.modules={};var i={form:n(66),countdown:n(72),posts:n(74),slides:n(76),share_buttons:n(78),nav_menu:n(80),animatedText:n(82),carousel:n(84),social:n(86),themeElements:n(88),themeBuilder:n(90),sticky:n(93),woocommerce:n(94),lightbox:n(96)},s=function(){t.modules={},e.each(i,function(n){t.modules[n]=new this(e)})};this.init=function(){!function(){if(jQuery("body").hasClass("elementor-msie")){var e=jQuery("#elementor-pro-css"),t=e[0].outerHTML.replace("css/frontend","css/frontend-msie").replace("elementor-pro-css","elementor-pro-msie-css");e.after(t)}}(),e(window).on("elementor/frontend/init",s)},this.init()}(jQuery)},function(e,t,n){"use strict";e.exports=function(){elementorFrontend.hooks.addAction("frontend/element_ready/form.default",n(4)),elementorFrontend.hooks.addAction("frontend/element_ready/subscribe.default",n(4)),elementorFrontend.hooks.addAction("frontend/element_ready/form.default",n(69)),elementorFrontend.hooks.addAction("frontend/element_ready/form.default",n(70)),elementorFrontend.hooks.addAction("frontend/element_ready/form.default",n(71))}},function(e,t,n){"use strict";e.exports=elementorFrontend.Module.extend({getDefaultSettings:function(){return{selectors:{form:".elementor-form",submitButton:'[type="submit"]'},action:"elementor_pro_forms_send_form",ajaxUrl:elementorProFrontend.config.ajaxurl}},getDefaultElements:function(){var e=this.getSettings("selectors"),t={};return t.$form=this.$element.find(e.form),t.$submitButton=t.$form.find(e.submitButton),t},bindEvents:function(){this.elements.$form.on("submit",this.handleSubmit);var e=this.elements.$form.find("input[type=file]");e.length&&e.on("change",this.validateFileSize)},validateFileSize:function(e){var t=this,n=jQuery(e.currentTarget),i=n[0].files;if(i.length){var s=1024*parseInt(n.attr("data-maxsize"))*1024,o=n.attr("data-maxsize-message");Array.prototype.slice.call(i).forEach(function(e){s<e.size&&(n.parent().addClass("elementor-error").append('<span class="elementor-message elementor-message-danger elementor-help-inline elementor-form-help-inline" role="alert">'+o+"</span>").find(":input").attr("aria-invalid","true"),t.elements.$form.trigger("error"))})}},beforeSend:function(){var e=this.elements.$form;e.animate({opacity:"0.45"},500).addClass("elementor-form-waiting"),e.find(".elementor-message").remove(),e.find(".elementor-error").removeClass("elementor-error"),e.find("div.elementor-field-group").removeClass("error").find("span.elementor-form-help-inline").remove().end().find(":input").attr("aria-invalid","false"),this.elements.$submitButton.attr("disabled","disabled").find("> span").prepend('<span class="elementor-button-text elementor-form-spinner"><i class="fa fa-spinner fa-spin"></i>&nbsp;</span>')},getFormData:function(){var e=new FormData(this.elements.$form[0]);return e.append("action",this.getSettings("action")),e.append("referrer",location.toString()),e},onSuccess:function(e){var t=this.elements.$form;this.elements.$submitButton.removeAttr("disabled").find(".elementor-form-spinner").remove(),t.animate({opacity:"1"},100).removeClass("elementor-form-waiting"),e.success?(t.trigger("submit_success",e.data),t.trigger("form_destruct",e.data),t.trigger("reset"),void 0!==e.data.message&&""!==e.data.message&&t.append('<div class="elementor-message elementor-message-success" role="alert">'+e.data.message+"</div>")):(e.data.errors&&(jQuery.each(e.data.errors,function(e,n){t.find("#form-field-"+e).parent().addClass("elementor-error").append('<span class="elementor-message elementor-message-danger elementor-help-inline elementor-form-help-inline" role="alert">'+n+"</span>").find(":input").attr("aria-invalid","true")}),t.trigger("error")),t.append('<div class="elementor-message elementor-message-danger" role="alert">'+e.data.message+"</div>"))},onError:function(e,t){var n=this.elements.$form;n.append('<div class="elementor-message elementor-message-danger" role="alert">'+t+"</div>"),this.elements.$submitButton.html(this.elements.$submitButton.text()).removeAttr("disabled"),n.animate({opacity:"1"},100).removeClass("elementor-form-waiting"),n.trigger("error")},handleSubmit:function(e){var t=this.elements.$form;if(e.preventDefault(),t.hasClass("elementor-form-waiting"))return!1;this.beforeSend(),jQuery.ajax({url:this.getSettings("ajaxUrl"),type:"POST",dataType:"json",data:this.getFormData(),processData:!1,contentType:!1,success:this.onSuccess,error:this.onError})}})},function(e,t,n){"use strict";e.exports=elementorFrontend.Module.extend({getDefaultSettings:function(){return{selectors:{form:".elementor-form"}}},getDefaultElements:function(){var e=this.getSettings("selectors"),t={};return t.$form=this.$element.find(e.form),t},bindEvents:function(){this.elements.$form.on("form_destruct",this.handleSubmit)},handleSubmit:function(e,t){void 0!==t.data.redirect_url&&(location.href=t.data.redirect_url)}})},function(e,t,n){"use strict";e.exports=function(e){var t=e.find(".elementor-g-recaptcha:last");if(t.length){!function e(t){window.grecaptcha&&window.grecaptcha.render?t():setTimeout(function(){e(t)},350)}(function(){!function(e){var t=grecaptcha.render(e[0],e.data()),n=e.parents("form");e.data("widgetId",t),n.on("reset error",function(){grecaptcha.reset(e.data("widgetId"))})}(t)})}}},function(e,t,n){"use strict";e.exports=function(e,t){var n=e.find(".elementor-date-field");if(n.length){t.each(n,function(e,n){!function(e){if(!t(e).hasClass("elementor-use-native")){var n={minDate:t(e).attr("min")||null,maxDate:t(e).attr("max")||null,allowInput:!0};e.flatpickr(n)}}(n)})}}},function(e,t,n){"use strict";e.exports=function(e,t){var n=e.find(".elementor-time-field");if(n.length){t.each(n,function(e,n){!function(e){t(e).hasClass("elementor-use-native")||e.flatpickr({noCalendar:!0,enableTime:!0,allowInput:!0})}(n)})}}},function(e,t,n){"use strict";e.exports=function(){elementorFrontend.hooks.addAction("frontend/element_ready/countdown.default",n(73))}},function(e,t,n){"use strict";var i=elementorFrontend.Module.extend({cache:null,cacheElements:function(){var e=this.$element.find(".elementor-countdown-wrapper");this.cache={$countDown:e,timeInterval:null,elements:{$countdown:e.find(".elementor-countdown-wrapper"),$daysSpan:e.find(".elementor-countdown-days"),$hoursSpan:e.find(".elementor-countdown-hours"),$minutesSpan:e.find(".elementor-countdown-minutes"),$secondsSpan:e.find(".elementor-countdown-seconds"),$expireMessage:e.parent().find(".elementor-countdown-expire--message")},data:{id:this.$element.data("id"),endTime:new Date(1e3*e.data("date")),actions:e.data("expire-actions"),evergreenInterval:e.data("evergreen-interval")}}},onInit:function(){elementorFrontend.Module.prototype.onInit.apply(this,arguments),this.cacheElements(),0<this.cache.data.evergreenInterval&&(this.cache.data.endTime=this.getEvergreenDate()),this.initializeClock()},updateClock:function(){var e=this,t=this.getTimeRemaining(this.cache.data.endTime);jQuery.each(t.parts,function(t){var n=e.cache.elements["$"+t+"Span"],i=this.toString();1===i.length&&(i=0+i),n.length&&n.text(i)}),t.total<=0&&(clearInterval(this.cache.timeInterval),this.runActions())},initializeClock:function(){var e=this;this.updateClock(),this.cache.timeInterval=setInterval(function(){e.updateClock()},1e3)},runActions:function(){var e=this;e.$element.trigger("countdown_expire",e.$element),this.cache.data.actions&&this.cache.data.actions.forEach(function(t){switch(t.type){case"hide":e.cache.$countDown.hide();break;case"redirect":t.redirect_url&&(window.location.href=t.redirect_url);break;case"message":e.cache.elements.$expireMessage.show()}})},getTimeRemaining:function(e){var t=e-new Date,n=Math.floor(t/1e3%60),i=Math.floor(t/1e3/60%60),s=Math.floor(t/36e5%24),o=Math.floor(t/864e5);return(o<0||s<0||i<0)&&(n=i=s=o=0),{total:t,parts:{days:o,hours:s,minutes:i,seconds:n}}},getEvergreenDate:function(){var e=this,t=this.cache.data.id,n=this.cache.data.evergreenInterval,i=t+"-evergreen_due_date",s=t+"-evergreen_interval",o={dueDate:localStorage.getItem(i),interval:localStorage.getItem(s)},r=function(){var t=new Date;return e.cache.data.endTime=t.setSeconds(t.getSeconds()+n),localStorage.setItem(i,e.cache.data.endTime),localStorage.setItem(s,n),e.cache.data.endTime};return null===o.dueDate&&null===o.interval?r():null!==o.dueDate&&n!==parseInt(o.interval,10)?r():o.dueDate>0&&parseInt(o.interval,10)===n?o.dueDate:void 0}});e.exports=function(e){new i({$element:e})}},function(e,t,n){"use strict";e.exports=function(){var e=n(3),t=n(5),i=n(75);elementorFrontend.hooks.addAction("frontend/element_ready/posts.classic",function(t){new e({$element:t})}),elementorFrontend.hooks.addAction("frontend/element_ready/posts.cards",function(e){new t({$element:e})}),elementorFrontend.hooks.addAction("frontend/element_ready/portfolio.default",function(e){e.find(".elementor-portfolio").length&&new i({$element:e})})}},function(e,t,n){"use strict";var i=n(3);e.exports=i.extend({getElementName:function(){return"portfolio"},getSkinPrefix:function(){return""},getDefaultSettings:function(){var e=i.prototype.getDefaultSettings.apply(this,arguments);return e.transitionDuration=450,jQuery.extend(e.classes,{active:"elementor-active",item:"elementor-portfolio-item",ghostItem:"elementor-portfolio-ghost-item"}),e},getDefaultElements:function(){var e=i.prototype.getDefaultElements.apply(this,arguments);return e.$filterButtons=this.$element.find(".elementor-portfolio__filter"),e},getOffset:function(e,t,n){var i=this.getSettings(),s=this.elements.$postsContainer.width()/i.colsCount-t;return{start:(t+(s+=s/(i.colsCount-1)))*(e%i.colsCount),top:(n+s)*Math.floor(e/i.colsCount)}},getClosureMethodsNames:function(){return i.prototype.getClosureMethodsNames.apply(this,arguments).concat(["onFilterButtonClick"])},filterItems:function(e){var t=this.elements.$posts,n=this.getSettings("classes.active"),i=".elementor-filter-"+e;"__all"!==e?(t.not(i).removeClass(n),t.filter(i).addClass(n)):t.addClass(n)},removeExtraGhostItems:function(){var e=this.getSettings(),t=this.elements.$posts.filter(":visible"),n=(e.colsCount-t.length%e.colsCount)%e.colsCount;this.elements.$postsContainer.find("."+e.classes.ghostItem).slice(n).remove()},handleEmptyColumns:function(){this.removeExtraGhostItems();for(var e=this.getSettings(),t=this.elements.$posts.filter(":visible"),n=this.elements.$postsContainer.find("."+e.classes.ghostItem),i=(e.colsCount-(t.length+n.length)%e.colsCount)%e.colsCount,s=0;s<i;s++)this.elements.$postsContainer.append(jQuery("<div>",{class:e.classes.item+" "+e.classes.ghostItem}))},showItems:function(e){e.show(),setTimeout(function(){e.css({opacity:1})})},hideItems:function(e){e.hide()},arrangeGrid:function(){var e=jQuery,t=this,n=t.getSettings(),i=t.elements.$posts.filter("."+n.classes.active),s=t.elements.$posts.not("."+n.classes.active),o=t.elements.$posts.filter(":visible"),r=i.add(o),a=i.filter(":visible"),l=i.filter(":hidden"),c=s.filter(":visible"),d=o.outerWidth(),u=o.outerHeight();if(t.elements.$posts.css("transition-duration",n.transitionDuration+"ms"),t.showItems(l),t.isEdit&&t.fitImages(),t.handleEmptyColumns(),t.isMasonryEnabled())return t.hideItems(c),t.showItems(l),t.handleEmptyColumns(),void t.runMasonry();c.css({opacity:0,transform:"scale3d(0.2, 0.2, 1)"}),a.each(function(){var n=e(this),i=t.getOffset(r.index(n),d,u),s=t.getOffset(o.index(n),d,u);i.start===s.start&&i.top===s.top||(s.start-=i.start,s.top-=i.top,elementorFrontend.config.is_rtl&&(s.start*=-1),n.css({transitionDuration:"",transform:"translate3d("+s.start+"px, "+s.top+"px, 0)"}))}),setTimeout(function(){i.each(function(){var s=e(this),o=t.getOffset(r.index(s),d,u),a=t.getOffset(i.index(s),d,u);s.css({transitionDuration:n.transitionDuration+"ms"}),a.start-=o.start,a.top-=o.top,elementorFrontend.config.is_rtl&&(a.start*=-1),setTimeout(function(){s.css("transform","translate3d("+a.start+"px, "+a.top+"px, 0)")})})}),setTimeout(function(){t.hideItems(c),i.css({transitionDuration:"",transform:"translate3d(0px, 0px, 0px)"}),t.handleEmptyColumns()},n.transitionDuration)},activeFilterButton:function(e){var t=this.getSettings("classes.active"),n=this.elements.$filterButtons,i=n.filter('[data-filter="'+e+'"]');n.removeClass(t),i.addClass(t)},setFilter:function(e){this.activeFilterButton(e),this.filterItems(e),this.arrangeGrid()},refreshGrid:function(){this.setColsCountSettings(),this.arrangeGrid()},bindEvents:function(){i.prototype.bindEvents.apply(this,arguments),this.elements.$filterButtons.on("click",this.onFilterButtonClick)},isMasonryEnabled:function(){return!!this.getElementSettings("masonry")},run:function(){i.prototype.run.apply(this,arguments),this.setColsCountSettings(),this.setFilter("__all"),this.handleEmptyColumns()},onFilterButtonClick:function(e){this.setFilter(jQuery(e.currentTarget).data("filter"))},onWindowResize:function(){i.prototype.onWindowResize.apply(this,arguments),this.refreshGrid()},onElementChange:function(e){i.prototype.onElementChange.apply(this,arguments),"classic_item_ratio"===e&&this.refreshGrid()}})},function(e,t,n){"use strict";e.exports=function(){elementorFrontend.hooks.addAction("frontend/element_ready/slides.default",n(77))}},function(e,t,n){"use strict";var i=elementorFrontend.Module.extend({getDefaultSettings:function(){return{selectors:{slider:".elementor-slides",slideContent:".elementor-slide-content"},classes:{animated:"animated"},attributes:{dataSliderOptions:"slider_options",dataAnimation:"animation"}}},getDefaultElements:function(){var e=this.getSettings("selectors");return{$slider:this.$element.find(e.slider)}},initSlider:function(){var e=this.elements.$slider;e.length&&e.slick(e.data(this.getSettings("attributes.dataSliderOptions")))},goToActiveSlide:function(){this.elements.$slider.slick("slickGoTo",this.getEditSettings("activeItemIndex")-1)},onPanelShow:function(){var e=this.elements.$slider;e.slick("slickPause"),e.on("afterChange",function(){e.slick("slickPause")})},bindEvents:function(){var e=this.elements.$slider,t=this.getSettings(),n=e.data(t.attributes.dataAnimation);n&&(elementorFrontend.isEditMode()&&elementor.hooks.addAction("panel/open_editor/widget/slides",this.onPanelShow),e.on({beforeChange:function(){e.find(t.selectors.slideContent).removeClass(t.classes.animated+" "+n).hide()},afterChange:function(e,i,s){jQuery(i.$slides.get(s)).find(t.selectors.slideContent).show().addClass(t.classes.animated+" "+n)}}))},onInit:function(){elementorFrontend.Module.prototype.onInit.apply(this,arguments),this.initSlider(),this.isEdit&&this.goToActiveSlide()},onEditSettingsChange:function(e){"activeItemIndex"===e&&this.goToActiveSlide()}});e.exports=function(e){new i({$element:e})}},function(e,t,n){"use strict";e.exports=function(){elementorFrontend.isEditMode()||elementorFrontend.hooks.addAction("frontend/element_ready/share-buttons.default",n(79))}},function(e,t,n){"use strict";var i,s=elementorFrontend.Module;i=s.extend({onInit:function(){s.prototype.onInit.apply(this,arguments);var e=this.getElementSettings(),t=this.getSettings("classes"),n=e.share_url&&e.share_url.url,i={classPrefix:t.shareLinkPrefix};if(n?i.url=e.share_url.url:(i.url=location.href,i.title=elementorFrontend.config.post.title,i.text=elementorFrontend.config.post.excerpt),this.elements.$shareButton.shareLink){this.elements.$shareButton.shareLink(i);var o=jQuery.map(elementorProFrontend.config.shareButtonsNetworks,function(e,t){return e.has_counter?t:null});ElementorProFrontendConfig.hasOwnProperty("donreach")&&this.elements.$shareCounter.shareCounter({url:n?e.share_url.url:location.href,providers:o,classPrefix:t.shareCounterPrefix,formatCount:!0})}},getDefaultSettings:function(){return{selectors:{shareButton:".elementor-share-btn",shareCounter:".elementor-share-btn__counter"},classes:{shareLinkPrefix:"elementor-share-btn_",shareCounterPrefix:"elementor-share-btn__counter_"}}},getDefaultElements:function(){var e=this.getSettings("selectors");return{$shareButton:this.$element.find(e.shareButton),$shareCounter:this.$element.find(e.shareCounter)}}}),e.exports=function(e){new i({$element:e})}},function(e,t,n){"use strict";e.exports=function(){jQuery.fn.smartmenus&&(jQuery.SmartMenus.prototype.isCSSOn=function(){return!0},elementorFrontend.config.is_rtl&&(jQuery.fn.smartmenus.defaults.rightToLeftSubMenus=!0)),elementorFrontend.hooks.addAction("frontend/element_ready/nav-menu.default",n(81))}},function(e,t,n){"use strict";var i=elementorFrontend.Module.extend({stretchElement:null,getDefaultSettings:function(){return{selectors:{menu:".elementor-nav-menu",anchorLink:".elementor-nav-menu--main .elementor-item-anchor",dropdownMenu:".elementor-nav-menu__container.elementor-nav-menu--dropdown",menuToggle:".elementor-menu-toggle"}}},getDefaultElements:function(){var e=this.getSettings("selectors"),t={};return t.$menu=this.$element.find(e.menu),t.$anchorLink=this.$element.find(e.anchorLink),t.$dropdownMenu=this.$element.find(e.dropdownMenu),t.$dropdownMenuFinalItems=t.$dropdownMenu.find(".menu-item:not(.menu-item-has-children) > a"),t.$menuToggle=this.$element.find(e.menuToggle),t},bindEvents:function(){this.elements.$menu.length&&(this.elements.$menuToggle.on("click",this.toggleMenu.bind(this)),this.getElementSettings("full_width")&&this.elements.$dropdownMenuFinalItems.on("click",this.toggleMenu.bind(this,!1)),elementorFrontend.addListenerOnce(this.$element.data("model-cid"),"resize",this.stretchMenu))},initStretchElement:function(){this.stretchElement=new elementorFrontend.modules.StretchElement({element:this.elements.$dropdownMenu})},toggleMenu:function(e){var t=this.elements.$menuToggle.hasClass("elementor-active");"boolean"!=typeof e&&(e=!t),this.elements.$menuToggle.toggleClass("elementor-active",e),e&&this.getElementSettings("full_width")&&this.stretchElement.stretch()},followMenuAnchors:function(){var e=this;e.elements.$anchorLink.each(function(){location.pathname===this.pathname&&""!==this.hash&&e.followMenuAnchor(jQuery(this))})},followMenuAnchor:function(e){var t=e[0].hash,n=jQuery(decodeURIComponent(t)),i=-300;if(n.length){if(!n.hasClass("elementor-menu-anchor")){var s=jQuery(window).height()/2;i=-n.outerHeight()+s}elementorFrontend.waypoint(n,function(t){"down"===t?e.addClass("elementor-item-active"):e.removeClass("elementor-item-active")},{offset:"50%",triggerOnce:!1}),elementorFrontend.waypoint(n,function(t){"down"===t?e.removeClass("elementor-item-active"):e.addClass("elementor-item-active")},{offset:i,triggerOnce:!1})}},stretchMenu:function(){this.getElementSettings("full_width")?(this.stretchElement.stretch(),this.elements.$dropdownMenu.css("top",this.elements.$menuToggle.outerHeight())):this.stretchElement.reset()},onInit:function(){elementorFrontend.Module.prototype.onInit.apply(this,arguments),this.elements.$menu.length&&(this.elements.$menu.smartmenus({subIndicatorsText:'<i class="fa"></i>',subIndicatorsPos:"append",subMenusMaxWidth:"1000px"}),this.initStretchElement(),this.stretchMenu(),elementorFrontend.isEditMode()||this.followMenuAnchors())},onElementChange:function(e){"full_width"===e&&this.stretchMenu()}});e.exports=function(e){new i({$element:e})}},function(e,t,n){"use strict";e.exports=function(){elementorFrontend.hooks.addAction("frontend/element_ready/animated-headline.default",n(83))}},function(e,t,n){"use strict";var i=elementorFrontend.Module.extend({svgPaths:{circle:["M325,18C228.7-8.3,118.5,8.3,78,21C22.4,38.4,4.6,54.6,5.6,77.6c1.4,32.4,52.2,54,142.6,63.7 c66.2,7.1,212.2,7.5,273.5-8.3c64.4-16.6,104.3-57.6,33.8-98.2C386.7-4.9,179.4-1.4,126.3,20.7"],underline_zigzag:["M9.3,127.3c49.3-3,150.7-7.6,199.7-7.4c121.9,0.4,189.9,0.4,282.3,7.2C380.1,129.6,181.2,130.6,70,139 c82.6-2.9,254.2-1,335.9,1.3c-56,1.4-137.2-0.3-197.1,9"],x:["M497.4,23.9C301.6,40,155.9,80.6,4,144.4","M14.1,27.6c204.5,20.3,393.8,74,467.3,111.7"],strikethrough:["M3,75h493.5"],curly:["M3,146.1c17.1-8.8,33.5-17.8,51.4-17.8c15.6,0,17.1,18.1,30.2,18.1c22.9,0,36-18.6,53.9-18.6 c17.1,0,21.3,18.5,37.5,18.5c21.3,0,31.8-18.6,49-18.6c22.1,0,18.8,18.8,36.8,18.8c18.8,0,37.5-18.6,49-18.6c20.4,0,17.1,19,36.8,19 c22.9,0,36.8-20.6,54.7-18.6c17.7,1.4,7.1,19.5,33.5,18.8c17.1,0,47.2-6.5,61.1-15.6"],diagonal:["M13.5,15.5c131,13.7,289.3,55.5,475,125.5"],double:["M8.4,143.1c14.2-8,97.6-8.8,200.6-9.2c122.3-0.4,287.5,7.2,287.5,7.2","M8,19.4c72.3-5.3,162-7.8,216-7.8c54,0,136.2,0,267,7.8"],double_underline:["M5,125.4c30.5-3.8,137.9-7.6,177.3-7.6c117.2,0,252.2,4.7,312.7,7.6","M26.9,143.8c55.1-6.1,126-6.3,162.2-6.1c46.5,0.2,203.9,3.2,268.9,6.4"],underline:["M7.7,145.6C109,125,299.9,116.2,401,121.3c42.1,2.2,87.6,11.8,87.3,25.7"]},getDefaultSettings:function(){var e={animationDelay:2500,lettersDelay:50,typeLettersDelay:150,selectionDuration:500,revealDuration:600,revealAnimationDelay:1500};return e.typeAnimationDelay=e.selectionDuration+800,e.selectors={headline:".elementor-headline",dynamicWrapper:".elementor-headline-dynamic-wrapper"},e.classes={dynamicText:"elementor-headline-dynamic-text",dynamicLetter:"elementor-headline-dynamic-letter",textActive:"elementor-headline-text-active",textInactive:"elementor-headline-text-inactive",letters:"elementor-headline-letters",animationIn:"elementor-headline-animation-in",typeSelected:"elementor-headline-typing-selected"},e},getDefaultElements:function(){var e=this.getSettings("selectors");return{$headline:this.$element.find(e.headline),$dynamicWrapper:this.$element.find(e.dynamicWrapper)}},getNextWord:function(e){return e.is(":last-child")?e.parent().children().eq(0):e.next()},switchWord:function(e,t){e.removeClass("elementor-headline-text-active").addClass("elementor-headline-text-inactive"),t.removeClass("elementor-headline-text-inactive").addClass("elementor-headline-text-active")},singleLetters:function(){var e=this.getSettings("classes");this.elements.$dynamicText.each(function(){var t=jQuery(this),n=t.text().split(""),i=t.hasClass(e.textActive);t.empty(),n.forEach(function(n){var s=jQuery("<span>",{class:e.dynamicLetter}).text(n);i&&s.addClass(e.animationIn),t.append(s)}),t.css("opacity",1)})},showLetter:function(e,t,n,i){var s=this,o=this.getSettings("classes");e.addClass(o.animationIn),e.is(":last-child")?n||setTimeout(function(){s.hideWord(t)},s.getSettings("animationDelay")):setTimeout(function(){s.showLetter(e.next(),t,n,i)},i)},hideLetter:function(e,t,n,i){var s=this,o=this.getSettings();e.removeClass(o.classes.animationIn),e.is(":last-child")?n&&setTimeout(function(){s.hideWord(s.getNextWord(t))},s.getSettings("animationDelay")):setTimeout(function(){s.hideLetter(e.next(),t,n,i)},i)},showWord:function(e,t){var n=this,i=n.getSettings(),s=n.getElementSettings("animation_type");"typing"===s?(n.showLetter(e.find("."+i.classes.dynamicLetter).eq(0),e,!1,t),e.addClass(i.classes.textActive).removeClass(i.classes.textInactive)):"clip"===s&&n.elements.$dynamicWrapper.animate({width:e.width()+10},i.revealDuration,function(){setTimeout(function(){n.hideWord(e)},i.revealAnimationDelay)})},hideWord:function(e){var t=this,n=t.getSettings(),i=n.classes,s="."+i.dynamicLetter,o=t.getElementSettings("animation_type"),r=t.getNextWord(e);if("typing"===o)t.elements.$dynamicWrapper.addClass(i.typeSelected),setTimeout(function(){t.elements.$dynamicWrapper.removeClass(i.typeSelected),e.addClass(n.classes.textInactive).removeClass(i.textActive).children(s).removeClass(i.animationIn)},n.selectionDuration),setTimeout(function(){t.showWord(r,n.typeLettersDelay)},n.typeAnimationDelay);else if(t.elements.$headline.hasClass(i.letters)){var a=e.children(s).length>=r.children(s).length;t.hideLetter(e.find(s).eq(0),e,a,n.lettersDelay),t.showLetter(r.find(s).eq(0),r,a,n.lettersDelay)}else"clip"===o?t.elements.$dynamicWrapper.animate({width:"2px"},n.revealDuration,function(){t.switchWord(e,r),t.showWord(r)}):(t.switchWord(e,r),setTimeout(function(){t.hideWord(r)},n.animationDelay))},animateHeadline:function(){var e=this,t=e.getElementSettings("animation_type"),n=e.elements.$dynamicWrapper;if("clip"===t)n.width(n.width()+10);else if("typing"!==t){var i=0;e.elements.$dynamicText.each(function(){var e=jQuery(this).width();e>i&&(i=e)}),n.css("width",i)}setTimeout(function(){e.hideWord(e.elements.$dynamicText.eq(0))},e.getSettings("animationDelay"))},getSvgPaths:function(e){var t=this.svgPaths[e],n=jQuery();return t.forEach(function(e){n=n.add(jQuery("<path>",{d:e}))}),n},fillWords:function(){var e=this.getElementSettings(),t=this.getSettings("classes"),n=this.elements.$dynamicWrapper;if("rotate"===e.headline_style){(e.rotating_text||"").split("\n").forEach(function(e,i){var s=jQuery("<span>",{class:t.dynamicText}).html(e.replace(/ /g,"&nbsp;"));i||s.addClass(t.textActive),n.append(s)})}else{var i=jQuery("<span>",{class:t.dynamicText+" "+t.textActive}).text(e.highlighted_text),s=jQuery("<svg>",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 500 150",preserveAspectRatio:"none"}).html(this.getSvgPaths(e.marker));n.append(i,s[0].outerHTML)}this.elements.$dynamicText=n.children("."+t.dynamicText)},rotateHeadline:function(){var e=this.getSettings();this.elements.$headline.hasClass(e.classes.letters)&&this.singleLetters(),this.animateHeadline()},initHeadline:function(){"rotate"===this.getElementSettings("headline_style")&&this.rotateHeadline()},onInit:function(){elementorFrontend.Module.prototype.onInit.apply(this,arguments),this.fillWords(),this.initHeadline()}});e.exports=function(e){new i({$element:e})}},function(e,t,n){"use strict";e.exports=function(){elementorFrontend.hooks.addAction("frontend/element_ready/media-carousel.default",n(85)),elementorFrontend.hooks.addAction("frontend/element_ready/testimonial-carousel.default",n(7)),elementorFrontend.hooks.addAction("frontend/element_ready/reviews.default",n(7))}},function(e,t,n){"use strict";var i,s=n(6);i=s.extend({slideshowSpecialElementSettings:["slides_per_view","slides_per_view_tablet","slides_per_view_mobile"],isSlideshow:function(){return"slideshow"===this.getElementSettings("skin")},getDefaultSettings:function(){var e=s.prototype.getDefaultSettings.apply(this,arguments);return this.isSlideshow()&&(e.selectors.thumbsSwiper=".elementor-thumbnails-swiper",e.slidesPerView={desktop:5,tablet:4,mobile:3}),e},getElementSettings:function(e){return-1!==this.slideshowSpecialElementSettings.indexOf(e)&&this.isSlideshow()&&(e="slideshow_"+e),s.prototype.getElementSettings.call(this,e)},getDefaultElements:function(){var e=this.getSettings("selectors"),t=s.prototype.getDefaultElements.apply(this,arguments);return this.isSlideshow()&&(t.$thumbsSwiper=this.$element.find(e.thumbsSwiper)),t},getEffect:function(){return"coverflow"===this.getElementSettings("skin")?"coverflow":s.prototype.getEffect.apply(this,arguments)},getSlidesPerView:function(e){return this.isSlideshow()?1:"coverflow"===this.getElementSettings("skin")?this.getDeviceSlidesPerView(e):s.prototype.getSlidesPerView.apply(this,arguments)},getSwiperOptions:function(){var e=s.prototype.getSwiperOptions.apply(this,arguments);return this.isSlideshow()&&(e.loopedSlides=this.getSlidesCount(),delete e.pagination,delete e.breakpoints),e},onInit:function(){s.prototype.onInit.apply(this,arguments);var e=this.getSlidesCount();if(this.isSlideshow()&&!(1>=e)){var t=this.getElementSettings(),n="yes"===t.loop,i={},o=elementorFrontend.config.breakpoints,r=this.getDeviceSlidesPerView("desktop");i[o.lg-1]={slidesPerView:this.getDeviceSlidesPerView("tablet"),spaceBetween:this.getSpaceBetween("tablet")},i[o.md-1]={slidesPerView:this.getDeviceSlidesPerView("mobile"),spaceBetween:this.getSpaceBetween("mobile")};var a={slidesPerView:r,initialSlide:this.getInitialSlide(),centeredSlides:t.centered_slides,slideToClickedSlide:!0,spaceBetween:this.getSpaceBetween(),loopedSlides:e,loop:n,onSlideChangeEnd:function(e){n&&e.fixLoop()},breakpoints:i};this.swipers.main.controller.control=this.swipers.thumbs=new Swiper(this.elements.$thumbsSwiper,a),this.swipers.thumbs.controller.control=this.swipers.main}},onElementChange:function(e){1>=this.getSlidesCount()||(this.isSlideshow()?(0===e.indexOf("width")&&(this.swipers.main.update(),this.swipers.thumbs.update()),0===e.indexOf("space_between")&&this.updateSpaceBetween(this.swipers.thumbs,e)):s.prototype.onElementChange.apply(this,arguments))}}),e.exports=function(e){new i({$element:e})}},function(e,t,n){"use strict";var i=n(87);e.exports=function(){elementorFrontend.hooks.addAction("frontend/element_ready/facebook-button.default",i),elementorFrontend.hooks.addAction("frontend/element_ready/facebook-comments.default",i),elementorFrontend.hooks.addAction("frontend/element_ready/facebook-embed.default",i),elementorFrontend.hooks.addAction("frontend/element_ready/facebook-page.default",i)}},function(e,t,n){"use strict";var i=ElementorProFrontendConfig.facebook_sdk;e.exports=function(e){i.isLoading||i.isLoaded||(i.isLoading=!0,jQuery.ajax({url:"https://connect.facebook.net/"+i.lang+"/sdk.js",dataType:"script",cache:!0,success:function(){FB.init({appId:i.app_id,version:"v2.10",xfbml:!1}),i.isLoaded=!0,i.isLoading=!1,jQuery(document).trigger("fb:sdk:loaded")}}));var t=function(){e.find(".elementor-widget-container div").attr("data-width",e.width()+"px"),FB.XFBML.parse(e[0])};i.isLoaded?t():jQuery(document).on("fb:sdk:loaded",t)}},function(e,t,n){"use strict";e.exports=function(){elementorFrontend.hooks.addAction("frontend/element_ready/search-form.default",n(89))}},function(e,t,n){"use strict";var i=elementorFrontend.Module.extend({getDefaultSettings:function(){return{selectors:{wrapper:".elementor-search-form",container:".elementor-search-form__container",icon:".elementor-search-form__icon",input:".elementor-search-form__input",toggle:".elementor-search-form__toggle",submit:".elementor-search-form__submit",closeButton:".dialog-close-button"},classes:{isFocus:"elementor-search-form--focus",isFullScreen:"elementor-search-form--full-screen",lightbox:"elementor-lightbox"}}},getDefaultElements:function(){var e=this.getSettings("selectors"),t={};return t.$wrapper=this.$element.find(e.wrapper),t.$container=this.$element.find(e.container),t.$input=this.$element.find(e.input),t.$icon=this.$element.find(e.icon),t.$toggle=this.$element.find(e.toggle),t.$submit=this.$element.find(e.submit),t.$closeButton=this.$element.find(e.closeButton),t},bindEvents:function(){var e=this.elements.$container,t=this.elements.$closeButton,n=this.elements.$input,i=this.elements.$wrapper,s=this.elements.$icon,o=this.getElementSettings("skin"),r=this.getSettings("classes");"full_screen"===o?(this.elements.$toggle.on("click",function(){e.toggleClass(r.isFullScreen).toggleClass(r.lightbox),n.focus()}),e.on("click",function(t){e.hasClass(r.isFullScreen)&&e[0]===t.target&&e.removeClass(r.isFullScreen).removeClass(r.lightbox)}),t.on("click",function(){e.removeClass(r.isFullScreen).removeClass(r.lightbox)}),elementorFrontend.getElements("$document").keyup(function(t){27===t.keyCode&&e.hasClass(r.isFullScreen)&&e.click()})):n.on({focus:function(){i.addClass(r.isFocus)},blur:function(){i.removeClass(r.isFocus)}}),"minimal"===o&&s.on("click",function(){i.addClass(r.isFocus),n.focus()})}});e.exports=function(e){new i({$element:e})}},function(e,t,n){"use strict";e.exports=function(){var e=n(91),t=n(92);elementorFrontend.hooks.addAction("frontend/element_ready/archive-posts.archive_classic",function(t){new e({$element:t})}),elementorFrontend.hooks.addAction("frontend/element_ready/archive-posts.archive_cards",function(e){new t({$element:e})}),jQuery(function(){var e=location.search.match(/theme_template_id=(\d*)/),t=e?jQuery(".elementor-"+e[1]):[];t.length&&jQuery("html, body").animate({scrollTop:t.offset().top-window.innerHeight/2})})}},function(e,t,n){"use strict";var i=n(3);e.exports=i.extend({getElementName:function(){return"archive-posts"},getSkinPrefix:function(){return"archive_classic_"}})},function(e,t,n){"use strict";var i=n(5);e.exports=i.extend({getElementName:function(){return"archive-posts"},getSkinPrefix:function(){return"archive_cards_"}})},function(e,t,n){"use strict";e.exports=function(){elementorFrontend.hooks.addAction("frontend/element_ready/section",n(8)),elementorFrontend.hooks.addAction("frontend/element_ready/widget",n(8))}},function(e,t,n){"use strict";e.exports=function(){elementorFrontend.hooks.addAction("frontend/element_ready/woocommerce-menu-cart.default",n(95)),elementorFrontend.isEditMode()||jQuery(document.body).on("wc_fragments_loaded wc_fragments_refreshed",function(){jQuery("div.elementor-widget-woocommerce-menu-cart").each(function(){elementorFrontend.elementsHandler.runReadyTrigger(jQuery(this))})})}},function(e,t,n){"use strict";var i=elementorFrontend.Module.extend({getDefaultSettings:function(){return{selectors:{container:".elementor-menu-cart__container",toggle:".elementor-menu-cart__toggle .elementor-button",closeButton:".elementor-menu-cart__close-button"},classes:{isShown:"elementor-menu-cart--shown",lightbox:"elementor-lightbox"}}},getDefaultElements:function(){var e=this.getSettings("selectors"),t={};return t.$container=this.$element.find(e.container),t.$toggle=this.$element.find(e.toggle),t.$closeButton=this.$element.find(e.closeButton),t},bindEvents:function(){var e=this.elements.$container,t=this.elements.$closeButton,n=this.getSettings("classes");this.elements.$toggle.on("click",function(t){t.preventDefault(),e.toggleClass(n.isShown)}),e.on("click",function(t){e.hasClass(n.isShown)&&e[0]===t.target&&e.removeClass(n.isShown)}),t.on("click",function(){e.removeClass(n.isShown)}),elementorFrontend.getElements("$document").keyup(function(t){27===t.keyCode&&e.hasClass(n.isShown)&&e.click()})}});e.exports=function(e){new i({$element:e})}},function(e,t,n){"use strict";e.exports=function(){var e=n(97);this.dynamicTags={lightbox:new e}}},function(e,t,n){"use strict";var i=elementorFrontend.Module.__super__.constructor.extend({selectors:{lightbox:'a[href^="!#elementor-lightbox|"]'},bindEvents:function(){elementorFrontend.getElements("$document").on("click",this.selectors.lightbox,this.triggerLightbox)},triggerLightbox:function(e){e.preventDefault();var t=jQuery(e.currentTarget).attr("href");t=t.replace("!#elementor-lightbox|","");var n=JSON.parse(t);"video"===n.type&&""===n.url||elementorFrontend.utils.lightbox.showModal(n)}});e.exports=i}]);
(function(modules){
var installedModules={};
function __webpack_require__(moduleId){
if(installedModules[moduleId]){
return installedModules[moduleId].exports;
}
var module=installedModules[moduleId]={
i: moduleId,
l: false,
exports: {}
};
modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
module.l=true;
return module.exports;
}
__webpack_require__.m=modules;
__webpack_require__.c=installedModules;
__webpack_require__.d=function(exports, name, getter){
if(!__webpack_require__.o(exports, name)){
Object.defineProperty(exports, name, { enumerable: true, get: getter });
}
};
__webpack_require__.r=function(exports){
if(typeof Symbol!=='undefined'&&Symbol.toStringTag){
Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
}
Object.defineProperty(exports, '__esModule', { value: true });
};
__webpack_require__.t=function(value, mode){
if(mode & 1) value=__webpack_require__(value);
if(mode & 8) return value;
if((mode & 4)&&typeof value==='object'&&value&&value.__esModule) return value;
var ns=Object.create(null);
__webpack_require__.r(ns);
Object.defineProperty(ns, 'default', { enumerable: true, value: value });
if(mode & 2&&typeof value!='string') for(var key in value) __webpack_require__.d(ns, key, function(key){ return value[key]; }.bind(null, key));
return ns;
};
__webpack_require__.n=function(module){
var getter=module&&module.__esModule ?
function getDefault(){ return module['default']; } :
function getModuleExports(){ return module; };
__webpack_require__.d(getter, 'a', getter);
return getter;
};
__webpack_require__.o=function(object, property){ return Object.prototype.hasOwnProperty.call(object, property); };
__webpack_require__.p="";
return __webpack_require__(__webpack_require__.s=207);
})
({
18:
(function(module, exports, __webpack_require__){
"use strict";
Object.defineProperty(exports, "__esModule", {
value: true
});
var _createClass=function (){ function defineProperties(target, props){ for (var i=0; i < props.length; i++){ var descriptor=props[i]; descriptor.enumerable=descriptor.enumerable||false; descriptor.configurable=true; if("value" in descriptor) descriptor.writable=true; Object.defineProperty(target, descriptor.key, descriptor); }} return function (Constructor, protoProps, staticProps){ if(protoProps) defineProperties(Constructor.prototype, protoProps); if(staticProps) defineProperties(Constructor, staticProps); return Constructor; };}();
var _get=function get(object, property, receiver){ if(object===null) object=Function.prototype; var desc=Object.getOwnPropertyDescriptor(object, property); if(desc===undefined){ var parent=Object.getPrototypeOf(object); if(parent===null){ return undefined; }else{ return get(parent, property, receiver); }}else if("value" in desc){ return desc.value; }else{ var getter=desc.get; if(getter===undefined){ return undefined; } return getter.call(receiver); }};
function _classCallCheck(instance, Constructor){ if(!(instance instanceof Constructor)){ throw new TypeError("Cannot call a class as a function"); }}
function _possibleConstructorReturn(self, call){ if(!self){ throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call&&(typeof call==="object"||typeof call==="function") ? call:self; }
function _inherits(subClass, superClass){ if(typeof superClass!=="function"&&superClass!==null){ throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype=Object.create(superClass&&superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true }});if(superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass):subClass.__proto__=superClass; }
var _class=function (_elementorModules$Vie){
_inherits(_class, _elementorModules$Vie);
function _class(){
_classCallCheck(this, _class);
return _possibleConstructorReturn(this, (_class.__proto__||Object.getPrototypeOf(_class)).apply(this, arguments));
}
_createClass(_class, [{
key: 'getDefaultSettings',
value: function getDefaultSettings(){
return {
selectors: {
elements: '.elementor-element',
nestedDocumentElements: '.elementor .elementor-element'
},
classes: {
editMode: 'elementor-edit-mode'
}};}}, {
key: 'getDefaultElements',
value: function getDefaultElements(){
var selectors=this.getSettings('selectors');
return {
$elements: this.$element.find(selectors.elements).not(this.$element.find(selectors.nestedDocumentElements))
};}}, {
key: 'getDocumentSettings',
value: function getDocumentSettings(setting){
var elementSettings=void 0;
if(this.isEdit){
elementSettings={};
var settings=elementor.settings.page.model;
jQuery.each(settings.getActiveControls(), function (controlKey){
elementSettings[controlKey]=settings.attributes[controlKey];
});
}else{
elementSettings=this.$element.data('elementor-settings')||{};}
return this.getItems(elementSettings, setting);
}}, {
key: 'runElementsHandlers',
value: function runElementsHandlers(){
this.elements.$elements.each(function (index, element){
return elementorFrontend.elementsHandler.runReadyTrigger(jQuery(element));
});
}}, {
key: 'onInit',
value: function onInit(){
this.$element=this.getSettings('$element');
_get(_class.prototype.__proto__||Object.getPrototypeOf(_class.prototype), 'onInit', this).call(this);
this.isEdit=this.$element.hasClass(this.getSettings('classes.editMode'));
if(this.isEdit){
elementor.settings.page.model.on('change', this.onSettingsChange.bind(this));
}else{
this.runElementsHandlers();
}}
}, {
key: 'onSettingsChange',
value: function onSettingsChange(){}}]);
return _class;
}(elementorModules.ViewModule);
exports.default=_class;
}),
20:
(function(module, exports, __webpack_require__){
"use strict";
Object.defineProperty(exports, "__esModule", {
value: true
});
var _module=__webpack_require__(6);
var _module2=_interopRequireDefault(_module);
var _viewModule=__webpack_require__(7);
var _viewModule2=_interopRequireDefault(_viewModule);
var _masonry=__webpack_require__(21);
var _masonry2=_interopRequireDefault(_masonry);
function _interopRequireDefault(obj){ return obj&&obj.__esModule ? obj:{ default: obj };}
exports.default=window.elementorModules={
Module: _module2.default,
ViewModule: _viewModule2.default,
utils: {
Masonry: _masonry2.default
}};
}),
207:
(function(module, exports, __webpack_require__){
"use strict";
var _modules=__webpack_require__(20);
var _modules2=_interopRequireDefault(_modules);
var _document=__webpack_require__(18);
var _document2=_interopRequireDefault(_document);
function _interopRequireDefault(obj){ return obj&&obj.__esModule ? obj:{ default: obj };}
_modules2.default.frontend={
Document: _document2.default,
tools: {
StretchElement: __webpack_require__(208)
}};
}),
208:
(function(module, exports, __webpack_require__){
"use strict";
module.exports=elementorModules.ViewModule.extend({
getDefaultSettings: function getDefaultSettings(){
return {
element: null,
direction: elementorFrontend.config.is_rtl ? 'right':'left',
selectors: {
container: window
}};},
getDefaultElements: function getDefaultElements(){
return {
$element: jQuery(this.getSettings('element'))
};},
stretch: function stretch(){
var containerSelector=this.getSettings('selectors.container'),
$container;
try {
$container=jQuery(containerSelector);
} catch (e){}
if(!$container||!$container.length){
$container=jQuery(this.getDefaultSettings().selectors.container);
}
this.reset();
var $element=this.elements.$element,
containerWidth=$container.outerWidth(),
elementOffset=$element.offset().left,
isFixed='fixed'===$element.css('position'),
correctOffset=isFixed ? 0:elementOffset;
if(window!==$container[0]){
var containerOffset=$container.offset().left;
if(isFixed){
correctOffset=containerOffset;
}
if(elementOffset > containerOffset){
correctOffset=elementOffset - containerOffset;
}}
if(!isFixed){
if(elementorFrontend.config.is_rtl){
correctOffset=containerWidth - ($element.outerWidth() + correctOffset);
}
correctOffset=-correctOffset;
}
var css={};
css.width=containerWidth + 'px';
css[this.getSettings('direction')]=correctOffset + 'px';
$element.css(css);
},
reset: function reset(){
var css={};
css.width='';
css[this.getSettings('direction')]='';
this.elements.$element.css(css);
}});
}),
21:
(function(module, exports, __webpack_require__){
"use strict";
var _viewModule=__webpack_require__(7);
var _viewModule2=_interopRequireDefault(_viewModule);
function _interopRequireDefault(obj){ return obj&&obj.__esModule ? obj:{ default: obj };}
module.exports=_viewModule2.default.extend({
getDefaultSettings: function getDefaultSettings(){
return {
container: null,
items: null,
columnsCount: 3,
verticalSpaceBetween: 30
};},
getDefaultElements: function getDefaultElements(){
return {
$container: jQuery(this.getSettings('container')),
$items: jQuery(this.getSettings('items'))
};},
run: function run(){
var heights=[],
distanceFromTop=this.elements.$container.position().top,
settings=this.getSettings(),
columnsCount=settings.columnsCount;
distanceFromTop +=parseInt(this.elements.$container.css('margin-top'), 10);
this.elements.$items.each(function (index){
var row=Math.floor(index / columnsCount),
$item=jQuery(this),
itemHeight=$item[0].getBoundingClientRect().height + settings.verticalSpaceBetween;
if(row){
var itemPosition=$item.position(),
indexAtRow=index % columnsCount,
pullHeight=itemPosition.top - distanceFromTop - heights[indexAtRow];
pullHeight -=parseInt($item.css('margin-top'), 10);
pullHeight *=-1;
$item.css('margin-top', pullHeight + 'px');
heights[indexAtRow] +=itemHeight;
}else{
heights.push(itemHeight);
}});
}});
}),
6:
(function(module, exports, __webpack_require__){
"use strict";
var _typeof=typeof Symbol==="function"&&typeof Symbol.iterator==="symbol" ? function (obj){ return typeof obj; }:function (obj){ return obj&&typeof Symbol==="function"&&obj.constructor===Symbol&&obj!==Symbol.prototype ? "symbol":typeof obj; };
var Module=function Module(){
var $=jQuery,
instanceParams=arguments,
self=this,
events={};
var settings=void 0;
var ensureClosureMethods=function ensureClosureMethods(){
$.each(self, function (methodName){
var oldMethod=self[methodName];
if('function'!==typeof oldMethod){
return;
}
self[methodName]=function (){
return oldMethod.apply(self, arguments);
};});
};
var initSettings=function initSettings(){
settings=self.getDefaultSettings();
var instanceSettings=instanceParams[0];
if(instanceSettings){
$.extend(settings, instanceSettings);
}};
var init=function init(){
self.__construct.apply(self, instanceParams);
ensureClosureMethods();
initSettings();
self.trigger('init');
};
this.getItems=function (items, itemKey){
if(itemKey){
var keyStack=itemKey.split('.'),
currentKey=keyStack.splice(0, 1);
if(!keyStack.length){
return items[currentKey];
}
if(!items[currentKey]){
return;
}
return this.getItems(items[currentKey], keyStack.join('.'));
}
return items;
};
this.getSettings=function (setting){
return this.getItems(settings, setting);
};
this.setSettings=function (settingKey, value, settingsContainer){
if(!settingsContainer){
settingsContainer=settings;
}
if('object'===(typeof settingKey==='undefined' ? 'undefined':_typeof(settingKey))){
$.extend(settingsContainer, settingKey);
return self;
}
var keyStack=settingKey.split('.'),
currentKey=keyStack.splice(0, 1);
if(!keyStack.length){
settingsContainer[currentKey]=value;
return self;
}
if(!settingsContainer[currentKey]){
settingsContainer[currentKey]={};}
return self.setSettings(keyStack.join('.'), value, settingsContainer[currentKey]);
};
this.forceMethodImplementation=function (methodArguments){
var functionName=methodArguments.callee.name;
throw new ReferenceError('The method ' + functionName + ' must to be implemented in the inheritor child.');
};
this.on=function (eventName, callback){
if('object'===(typeof eventName==='undefined' ? 'undefined':_typeof(eventName))){
$.each(eventName, function (singleEventName){
self.on(singleEventName, this);
});
return self;
}
var eventNames=eventName.split(' ');
eventNames.forEach(function (singleEventName){
if(!events[singleEventName]){
events[singleEventName]=[];
}
events[singleEventName].push(callback);
});
return self;
};
this.off=function (eventName, callback){
if(!events[eventName]){
return self;
}
if(!callback){
delete events[eventName];
return self;
}
var callbackIndex=events[eventName].indexOf(callback);
if(-1!==callbackIndex){
delete events[eventName][callbackIndex];
}
return self;
};
this.trigger=function (eventName){
var methodName='on' + eventName[0].toUpperCase() + eventName.slice(1),
params=Array.prototype.slice.call(arguments, 1);
if(self[methodName]){
self[methodName].apply(self, params);
}
var callbacks=events[eventName];
if(!callbacks){
return self;
}
$.each(callbacks, function (index, callback){
callback.apply(self, params);
});
return self;
};
init();
};
Module.prototype.__construct=function (){};
Module.prototype.getDefaultSettings=function (){
return {};};
Module.extendsCount=0;
Module.extend=function (properties){
var $=jQuery,
parent=this;
var child=function child(){
return parent.apply(this, arguments);
};
$.extend(child, parent);
child.prototype=Object.create($.extend({}, parent.prototype, properties));
child.prototype.constructor=child;
var constructorID=++Module.extendsCount;
child.prototype.getConstructorID=function (){
return constructorID;
};
child.__super__=parent.prototype;
return child;
};
module.exports=Module;
}),
7:
(function(module, exports, __webpack_require__){
"use strict";
var _module=__webpack_require__(6);
var _module2=_interopRequireDefault(_module);
function _interopRequireDefault(obj){ return obj&&obj.__esModule ? obj:{ default: obj };}
module.exports=_module2.default.extend({
elements: null,
getDefaultElements: function getDefaultElements(){
return {};},
bindEvents: function bindEvents(){},
onInit: function onInit(){
this.initElements();
this.bindEvents();
},
initElements: function initElements(){
this.elements=this.getDefaultElements();
}});
})
});
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){return function(){function b(a,b,c){return[parseFloat(a[0])*(n.test(a[0])?b/100:1),parseFloat(a[1])*(n.test(a[1])?c/100:1)]}function c(b,c){return parseInt(a.css(b,c),10)||0}function d(b){var c=b[0];return 9===c.nodeType?{width:b.width(),height:b.height(),offset:{top:0,left:0}}:a.isWindow(c)?{width:b.width(),height:b.height(),offset:{top:b.scrollTop(),left:b.scrollLeft()}}:c.preventDefault?{width:0,height:0,offset:{top:c.pageY,left:c.pageX}}:{width:b.outerWidth(),height:b.outerHeight(),offset:b.offset()}}a.ui=a.ui||{};var e,f,g=Math.max,h=Math.abs,i=Math.round,j=/left|center|right/,k=/top|center|bottom/,l=/[\+\-]\d+(\.[\d]+)?%?/,m=/^\w+/,n=/%$/,o=a.fn.position;a.position={scrollbarWidth:function(){if(void 0!==e)return e;var b,c,d=a("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),f=d.children()[0];return a("body").append(d),b=f.offsetWidth,d.css("overflow","scroll"),c=f.offsetWidth,b===c&&(c=d[0].clientWidth),d.remove(),e=b-c},getScrollInfo:function(b){var c=b.isWindow||b.isDocument?"":b.element.css("overflow-x"),d=b.isWindow||b.isDocument?"":b.element.css("overflow-y"),e="scroll"===c||"auto"===c&&b.width<b.element[0].scrollWidth,f="scroll"===d||"auto"===d&&b.height<b.element[0].scrollHeight;return{width:f?a.position.scrollbarWidth():0,height:e?a.position.scrollbarWidth():0}},getWithinInfo:function(b){var c=a(b||window),d=a.isWindow(c[0]),e=!!c[0]&&9===c[0].nodeType;return{element:c,isWindow:d,isDocument:e,offset:c.offset()||{left:0,top:0},scrollLeft:c.scrollLeft(),scrollTop:c.scrollTop(),width:d||e?c.width():c.outerWidth(),height:d||e?c.height():c.outerHeight()}}},a.fn.position=function(e){if(!e||!e.of)return o.apply(this,arguments);e=a.extend({},e);var n,p,q,r,s,t,u=a(e.of),v=a.position.getWithinInfo(e.within),w=a.position.getScrollInfo(v),x=(e.collision||"flip").split(" "),y={};return t=d(u),u[0].preventDefault&&(e.at="left top"),p=t.width,q=t.height,r=t.offset,s=a.extend({},r),a.each(["my","at"],function(){var a,b,c=(e[this]||"").split(" ");1===c.length&&(c=j.test(c[0])?c.concat(["center"]):k.test(c[0])?["center"].concat(c):["center","center"]),c[0]=j.test(c[0])?c[0]:"center",c[1]=k.test(c[1])?c[1]:"center",a=l.exec(c[0]),b=l.exec(c[1]),y[this]=[a?a[0]:0,b?b[0]:0],e[this]=[m.exec(c[0])[0],m.exec(c[1])[0]]}),1===x.length&&(x[1]=x[0]),"right"===e.at[0]?s.left+=p:"center"===e.at[0]&&(s.left+=p/2),"bottom"===e.at[1]?s.top+=q:"center"===e.at[1]&&(s.top+=q/2),n=b(y.at,p,q),s.left+=n[0],s.top+=n[1],this.each(function(){var d,j,k=a(this),l=k.outerWidth(),m=k.outerHeight(),o=c(this,"marginLeft"),t=c(this,"marginTop"),z=l+o+c(this,"marginRight")+w.width,A=m+t+c(this,"marginBottom")+w.height,B=a.extend({},s),C=b(y.my,k.outerWidth(),k.outerHeight());"right"===e.my[0]?B.left-=l:"center"===e.my[0]&&(B.left-=l/2),"bottom"===e.my[1]?B.top-=m:"center"===e.my[1]&&(B.top-=m/2),B.left+=C[0],B.top+=C[1],f||(B.left=i(B.left),B.top=i(B.top)),d={marginLeft:o,marginTop:t},a.each(["left","top"],function(b,c){a.ui.position[x[b]]&&a.ui.position[x[b]][c](B,{targetWidth:p,targetHeight:q,elemWidth:l,elemHeight:m,collisionPosition:d,collisionWidth:z,collisionHeight:A,offset:[n[0]+C[0],n[1]+C[1]],my:e.my,at:e.at,within:v,elem:k})}),e.using&&(j=function(a){var b=r.left-B.left,c=b+p-l,d=r.top-B.top,f=d+q-m,i={target:{element:u,left:r.left,top:r.top,width:p,height:q},element:{element:k,left:B.left,top:B.top,width:l,height:m},horizontal:c<0?"left":b>0?"right":"center",vertical:f<0?"top":d>0?"bottom":"middle"};p<l&&h(b+c)<p&&(i.horizontal="center"),q<m&&h(d+f)<q&&(i.vertical="middle"),g(h(b),h(c))>g(h(d),h(f))?i.important="horizontal":i.important="vertical",e.using.call(this,a,i)}),k.offset(a.extend(B,{using:j}))})},a.ui.position={fit:{left:function(a,b){var c,d=b.within,e=d.isWindow?d.scrollLeft:d.offset.left,f=d.width,h=a.left-b.collisionPosition.marginLeft,i=e-h,j=h+b.collisionWidth-f-e;b.collisionWidth>f?i>0&&j<=0?(c=a.left+i+b.collisionWidth-f-e,a.left+=i-c):j>0&&i<=0?a.left=e:i>j?a.left=e+f-b.collisionWidth:a.left=e:i>0?a.left+=i:j>0?a.left-=j:a.left=g(a.left-h,a.left)},top:function(a,b){var c,d=b.within,e=d.isWindow?d.scrollTop:d.offset.top,f=b.within.height,h=a.top-b.collisionPosition.marginTop,i=e-h,j=h+b.collisionHeight-f-e;b.collisionHeight>f?i>0&&j<=0?(c=a.top+i+b.collisionHeight-f-e,a.top+=i-c):j>0&&i<=0?a.top=e:i>j?a.top=e+f-b.collisionHeight:a.top=e:i>0?a.top+=i:j>0?a.top-=j:a.top=g(a.top-h,a.top)}},flip:{left:function(a,b){var c,d,e=b.within,f=e.offset.left+e.scrollLeft,g=e.width,i=e.isWindow?e.scrollLeft:e.offset.left,j=a.left-b.collisionPosition.marginLeft,k=j-i,l=j+b.collisionWidth-g-i,m="left"===b.my[0]?-b.elemWidth:"right"===b.my[0]?b.elemWidth:0,n="left"===b.at[0]?b.targetWidth:"right"===b.at[0]?-b.targetWidth:0,o=-2*b.offset[0];k<0?(c=a.left+m+n+o+b.collisionWidth-g-f,(c<0||c<h(k))&&(a.left+=m+n+o)):l>0&&(d=a.left-b.collisionPosition.marginLeft+m+n+o-i,(d>0||h(d)<l)&&(a.left+=m+n+o))},top:function(a,b){var c,d,e=b.within,f=e.offset.top+e.scrollTop,g=e.height,i=e.isWindow?e.scrollTop:e.offset.top,j=a.top-b.collisionPosition.marginTop,k=j-i,l=j+b.collisionHeight-g-i,m="top"===b.my[1],n=m?-b.elemHeight:"bottom"===b.my[1]?b.elemHeight:0,o="top"===b.at[1]?b.targetHeight:"bottom"===b.at[1]?-b.targetHeight:0,p=-2*b.offset[1];k<0?(d=a.top+n+o+p+b.collisionHeight-g-f,(d<0||d<h(k))&&(a.top+=n+o+p)):l>0&&(c=a.top-b.collisionPosition.marginTop+n+o+p-i,(c>0||h(c)<l)&&(a.top+=n+o+p))}},flipfit:{left:function(){a.ui.position.flip.left.apply(this,arguments),a.ui.position.fit.left.apply(this,arguments)},top:function(){a.ui.position.flip.top.apply(this,arguments),a.ui.position.fit.top.apply(this,arguments)}}},function(){var b,c,d,e,g,h=document.getElementsByTagName("body")[0],i=document.createElement("div");b=document.createElement(h?"div":"body"),d={visibility:"hidden",width:0,height:0,border:0,margin:0,background:"none"},h&&a.extend(d,{position:"absolute",left:"-1000px",top:"-1000px"});for(g in d)b.style[g]=d[g];b.appendChild(i),c=h||document.documentElement,c.insertBefore(b,c.firstChild),i.style.cssText="position: absolute; left: 10.7432222px;",e=a(i).offset().left,f=e>10&&e<11,b.innerHTML="",c.removeChild(b)}()}(),a.ui.position});
!function(a,b){"use strict";var c={widgetsTypes:{},createWidgetType:function(b,d,e){e||(e=this.Widget);var f=function(){e.apply(this,arguments)},g=f.prototype=new e(b);return g.types=g.types.concat([b]),a.extend(g,d),g.constructor=f,f.extend=function(a,b){return c.createWidgetType(a,b,f)},f},addWidgetType:function(a,b,c){return b&&b.prototype instanceof this.Widget?this.widgetsTypes[a]=b:this.widgetsTypes[a]=this.createWidgetType(a,b,c)},getWidgetType:function(a){return this.widgetsTypes[a]}};c.Instance=function(){var b=this,d={},e={},f=function(){d.body=a("body")},g=function(b){var c={classPrefix:"dialog",effects:{show:"fadeIn",hide:"fadeOut"}};a.extend(e,c,b)};this.createWidget=function(a,d){var e=c.getWidgetType(a),f=new e(a);return d=d||{},f.init(b,d),f},this.getSettings=function(a){return a?e[a]:Object.create(e)},this.init=function(a){return g(a),f(),b},b.init()},c.Widget=function(b){var d=this,e={},f={},g={},h=0,i=["refreshPosition"],j=function(){var a=[g.window];g.iframe&&a.push(jQuery(g.iframe[0].contentWindow)),a.forEach(function(a){e.hide.onEscKeyPress&&a.on("keyup",u),e.hide.onOutsideClick&&a[0].addEventListener("click",o,!0),e.hide.onOutsideContextMenu&&a[0].addEventListener("contextmenu",o,!0),e.position.autoRefresh&&a.on("resize",d.refreshPosition)}),(e.hide.onClick||e.hide.onBackgroundClick)&&g.widget.on("click",n)},k=function(b,c){var d=e.effects[b],f=g.widget;if(a.isFunction(d))d.apply(f,c);else{if(!f[d])throw"Reference Error: The effect "+d+" not found";f[d].apply(f,c)}},l=function(){var b=i.concat(d.getClosureMethods());a.each(b,function(){var a=this,b=d[a];d[a]=function(){b.apply(d,arguments)}})},m=function(a){if(a.my){var b=/left|right/,c=/([+-]\d+)?$/,d=g.iframe.offset(),e=g.iframe[0].contentWindow,f=a.my.split(" "),h=[];1===f.length&&(b.test(f[0])?f.push("center"):f.unshift("center")),f.forEach(function(a,b){var f=a.replace(c,function(a){return a=+a||0,a+=b?d.top-e.scrollY:d.left-e.scrollX,a>=0&&(a="+"+a),a});h.push(f)}),a.my=h.join(" ")}},n=function(b){if(!s(b)){if(e.hide.onClick){if(a(b.target).closest(e.selectors.preventClose).length)return}else if(b.target!==this)return;d.hide()}},o=function(b){s(b)||a(b.target).closest(g.widget).length||d.hide()},p=function(){d.addElement("widget"),d.addElement("header"),d.addElement("message"),d.addElement("window",window),d.addElement("body",document.body),d.addElement("container",e.container),e.iframe&&d.addElement("iframe",e.iframe),e.closeButton&&d.addElement("closeButton",'<div><i class="'+e.closeButtonClass+'"></i></div>');var b=d.getSettings("id");b&&d.setID(b);var c=[];a.each(d.types,function(){c.push(e.classes.globalPrefix+"-type-"+this)}),c.push(d.getSettings("className")),g.widget.addClass(c.join(" "))},q=function(c,f){var g=a.extend(!0,{},c.getSettings());e={headerMessage:"",message:"",effects:g.effects,classes:{globalPrefix:g.classPrefix,prefix:g.classPrefix+"-"+b,preventScroll:g.classPrefix+"-prevent-scroll"},selectors:{preventClose:"."+g.classPrefix+"-prevent-close"},container:"body",preventScroll:!1,iframe:null,closeButton:!1,closeButtonClass:g.classPrefix+"-close-button-icon",position:{element:"widget",my:"center",at:"center",enable:!0,autoRefresh:!1},hide:{auto:!1,autoDelay:5e3,onClick:!1,onOutsideClick:!0,onOutsideContextMenu:!1,onBackgroundClick:!0,onEscKeyPress:!0}},a.extend(!0,e,d.getDefaultSettings(),f),r()},r=function(){a.each(e,function(a){var b=a.match(/^on([A-Z].*)/);b&&(b=b[1].charAt(0).toLowerCase()+b[1].slice(1),d.on(b,this))})},s=function(a){return"click"===a.type&&2===a.button},t=function(a){return a.replace(/([a-z])([A-Z])/g,function(){return arguments[1]+"-"+arguments[2].toLowerCase()})},u=function(a){var b=27,c=a.which;b===c&&d.hide()},v=function(){var a=[g.window];g.iframe&&a.push(jQuery(g.iframe[0].contentWindow)),a.forEach(function(a){e.hide.onEscKeyPress&&a.off("keyup",u),e.hide.onOutsideClick&&a[0].removeEventListener("click",o,!0),e.hide.onOutsideContextMenu&&a[0].removeEventListener("contextmenu",o,!0),e.position.autoRefresh&&a.off("resize",d.refreshPosition)}),(e.hide.onClick||e.hide.onBackgroundClick)&&g.widget.off("click",n)};this.addElement=function(b,c,d){var f=g[b]=a(c||"<div>"),h=t(b),i=[];return d&&i.push(e.classes.globalPrefix+"-"+d),i.push(e.classes.globalPrefix+"-"+h),i.push(e.classes.prefix+"-"+h),f.addClass(i.join(" ")),f},this.destroy=function(){return v(),g.widget.remove(),d.trigger("destroy"),d},this.getElements=function(a){return a?g[a]:g},this.getSettings=function(a){var b=Object.create(e);return a?b[a]:b},this.hide=function(){return clearTimeout(h),k("hide",arguments),v(),e.preventScroll&&d.getElements("body").removeClass(e.classes.preventScroll),d.trigger("hide"),d},this.init=function(a,b){if(!(a instanceof c.Instance))throw"The "+d.widgetName+" must to be initialized from an instance of DialogsManager.Instance";return l(),d.trigger("init",b),q(a,b),p(),d.buildWidget(),d.attachEvents(),d.trigger("ready"),d},this.isVisible=function(){return g.widget.is(":visible")},this.on=function(b,c){if("object"==typeof b)return a.each(b,function(a){d.on(a,this)}),d;var e=b.split(" ");return e.forEach(function(a){f[a]||(f[a]=[]),f[a].push(c)}),d},this.off=function(a,b){if(!f[a])return d;if(!b)return delete f[a],d;var c=f[a].indexOf(b);return-1!==c&&f[a].splice(c,1),d},this.refreshPosition=function(){if(e.position.enable){var b=a.extend({},e.position);g[b.of]&&(b.of=g[b.of]),b.of||(b.of=window),e.iframe&&m(b),g[b.element].position(b)}},this.setID=function(a){return g.widget.attr("id",a),d},this.setHeaderMessage=function(a){return d.getElements("header").html(a),this},this.setMessage=function(a){return g.message.html(a),d},this.setSettings=function(b,c){return jQuery.isPlainObject(c)?a.extend(!0,e[b],c):e[b]=c,d},this.show=function(){return clearTimeout(h),g.widget.appendTo(g.container).hide(),k("show",arguments),d.refreshPosition(),e.hide.auto&&(h=setTimeout(d.hide,e.hide.autoDelay)),j(),e.preventScroll&&d.getElements("body").addClass(e.classes.preventScroll),d.trigger("show"),d},this.trigger=function(b,c){var e="on"+b[0].toUpperCase()+b.slice(1);d[e]&&d[e](c);var g=f[b];if(g)return a.each(g,function(a,b){b.call(d,c)}),d}},c.Widget.prototype.types=[],c.Widget.prototype.buildWidget=function(){var a=this.getElements(),b=this.getSettings();a.widget.append(a.header,a.message),this.setHeaderMessage(b.headerMessage),this.setMessage(b.message),this.getSettings("closeButton")&&a.widget.prepend(a.closeButton)},c.Widget.prototype.attachEvents=function(){var a=this;a.getSettings("closeButton")&&a.getElements("closeButton").on("click",function(){a.hide()})},c.Widget.prototype.getDefaultSettings=function(){return{}},c.Widget.prototype.getClosureMethods=function(){return[]},c.Widget.prototype.onHide=function(){},c.Widget.prototype.onShow=function(){},c.Widget.prototype.onInit=function(){},c.Widget.prototype.onReady=function(){},c.widgetsTypes.simple=c.Widget,c.addWidgetType("buttons",{activeKeyUp:function(a){var b=9;a.which===b&&a.preventDefault(),this.hotKeys[a.which]&&this.hotKeys[a.which](this)},activeKeyDown:function(a){if(this.focusedButton){var b=9;if(a.which===b){a.preventDefault();var c,d=this.focusedButton.index();a.shiftKey?(c=d-1,c<0&&(c=this.buttons.length-1)):(c=d+1,c>=this.buttons.length&&(c=0)),this.focusedButton=this.buttons[c].focus()}}},addButton:function(b){var c=this,d=c.getSettings(),e=jQuery.extend(d.button,b),f=c.addElement(b.name,a("<"+e.tag+">").text(b.text),"button");c.buttons.push(f);var g=function(){d.hide.onButtonClick&&c.hide(),a.isFunction(b.callback)&&b.callback.call(this,c)};return f.on("click",g),b.hotKey&&(this.hotKeys[b.hotKey]=g),this.getElements("buttonsWrapper").append(f),b.focus&&(this.focusedButton=f),c},bindHotKeys:function(){this.getElements("window").on({keyup:this.activeKeyUp,keydown:this.activeKeyDown})},buildWidget:function(){c.Widget.prototype.buildWidget.apply(this,arguments);var a=this.addElement("buttonsWrapper");this.getElements("widget").append(a)},getClosureMethods:function(){return["activeKeyUp","activeKeyDown"]},getDefaultSettings:function(){return{hide:{onButtonClick:!0},button:{tag:"button"}}},onHide:function(){this.unbindHotKeys()},onInit:function(){this.buttons=[],this.hotKeys={},this.focusedButton=null},onShow:function(){this.bindHotKeys(),this.focusedButton||(this.focusedButton=this.buttons[0]),this.focusedButton&&this.focusedButton.focus()},unbindHotKeys:function(){this.getElements("window").off({keyup:this.activeKeyUp,keydown:this.activeKeyDown})}}),c.addWidgetType("lightbox",c.getWidgetType("buttons").extend("lightbox",{getDefaultSettings:function(){var b=c.getWidgetType("buttons").prototype.getDefaultSettings.apply(this,arguments);return a.extend(!0,b,{contentWidth:"auto",contentHeight:"auto",position:{element:"widgetContent",of:"widget",autoRefresh:!0}})},buildWidget:function(){c.getWidgetType("buttons").prototype.buildWidget.apply(this,arguments);var a=this.addElement("widgetContent"),b=this.getElements();a.append(b.header,b.message,b.buttonsWrapper),b.widget.html(a),b.closeButton&&a.prepend(b.closeButton)},onReady:function(){var a=this.getElements(),b=this.getSettings();"auto"!==b.contentWidth&&a.message.width(b.contentWidth),"auto"!==b.contentHeight&&a.message.height(b.contentHeight)}})),c.addWidgetType("confirm",c.getWidgetType("lightbox").extend("confirm",{onReady:function(){c.getWidgetType("lightbox").prototype.onReady.apply(this,arguments);var a=this.getSettings("strings"),b="cancel"===this.getSettings("defaultOption");this.addButton({name:"cancel",text:a.cancel,callback:function(a){a.trigger("cancel")},focus:b}),this.addButton({name:"ok",text:a.confirm,callback:function(a){a.trigger("confirm")},focus:!b})},getDefaultSettings:function(){var a=c.getWidgetType("lightbox").prototype.getDefaultSettings.apply(this,arguments);return a.strings={confirm:"OK",cancel:"Cancel"},a.defaultOption="cancel",a}})),c.addWidgetType("alert",c.getWidgetType("lightbox").extend("alert",{onReady:function(){c.getWidgetType("lightbox").prototype.onReady.apply(this,arguments);var a=this.getSettings("strings");this.addButton({name:"ok",text:a.confirm,callback:function(a){a.trigger("confirm")}})},getDefaultSettings:function(){var a=c.getWidgetType("lightbox").prototype.getDefaultSettings.apply(this,arguments);return a.strings={confirm:"OK"},a}})),b.DialogsManager=c}("undefined"!=typeof jQuery?jQuery:"function"==typeof require&&require("jquery"),"undefined"!=typeof module?module.exports:window);
!function(){"use strict";function Waypoint(options){if(!options)throw new Error("No options passed to Waypoint constructor");if(!options.element)throw new Error("No element option passed to Waypoint constructor");if(!options.handler)throw new Error("No handler option passed to Waypoint constructor");this.key="waypoint-"+keyCounter,this.options=Waypoint.Adapter.extend({},Waypoint.defaults,options),this.element=this.options.element,this.adapter=new Waypoint.Adapter(this.element),this.callback=options.handler,this.axis=this.options.horizontal?"horizontal":"vertical",this.enabled=this.options.enabled,this.triggerPoint=null,this.group=Waypoint.Group.findOrCreate({name:this.options.group,axis:this.axis}),this.context=Waypoint.Context.findOrCreateByElement(this.options.context),Waypoint.offsetAliases[this.options.offset]&&(this.options.offset=Waypoint.offsetAliases[this.options.offset]),this.group.add(this),this.context.add(this),allWaypoints[this.key]=this,keyCounter+=1}var keyCounter=0,allWaypoints={};Waypoint.prototype.queueTrigger=function(direction){this.group.queueTrigger(this,direction)},Waypoint.prototype.trigger=function(args){this.enabled&&this.callback&&this.callback.apply(this,args)},Waypoint.prototype.destroy=function(){this.context.remove(this),this.group.remove(this),delete allWaypoints[this.key]},Waypoint.prototype.disable=function(){return this.enabled=!1,this},Waypoint.prototype.enable=function(){return this.context.refresh(),this.enabled=!0,this},Waypoint.prototype.next=function(){return this.group.next(this)},Waypoint.prototype.previous=function(){return this.group.previous(this)},Waypoint.invokeAll=function(method){var allWaypointsArray=[];for(var waypointKey in allWaypoints)allWaypointsArray.push(allWaypoints[waypointKey]);for(var i=0,end=allWaypointsArray.length;i<end;i++)allWaypointsArray[i][method]()},Waypoint.destroyAll=function(){Waypoint.invokeAll("destroy")},Waypoint.disableAll=function(){Waypoint.invokeAll("disable")},Waypoint.enableAll=function(){Waypoint.Context.refreshAll();for(var waypointKey in allWaypoints)allWaypoints[waypointKey].enabled=!0;return this},Waypoint.refreshAll=function(){Waypoint.Context.refreshAll()},Waypoint.viewportHeight=function(){return window.innerHeight||document.documentElement.clientHeight},Waypoint.viewportWidth=function(){return document.documentElement.clientWidth},Waypoint.adapters=[],Waypoint.defaults={context:window,continuous:!0,enabled:!0,group:"default",horizontal:!1,offset:0},Waypoint.offsetAliases={"bottom-in-view":function(){return this.context.innerHeight()-this.adapter.outerHeight()},"right-in-view":function(){return this.context.innerWidth()-this.adapter.outerWidth()}},window.Waypoint=Waypoint}(),function(){"use strict";function requestAnimationFrameShim(callback){window.setTimeout(callback,1e3/60)}function Context(element){this.element=element,this.Adapter=Waypoint.Adapter,this.adapter=new this.Adapter(element),this.key="waypoint-context-"+keyCounter,this.didScroll=!1,this.didResize=!1,this.oldScroll={x:this.adapter.scrollLeft(),y:this.adapter.scrollTop()},this.waypoints={vertical:{},horizontal:{}},element.waypointContextKey=this.key,contexts[element.waypointContextKey]=this,keyCounter+=1,Waypoint.windowContext||(Waypoint.windowContext=!0,Waypoint.windowContext=new Context(window)),this.createThrottledScrollHandler(),this.createThrottledResizeHandler()}var keyCounter=0,contexts={},Waypoint=window.Waypoint,oldWindowLoad=window.onload;Context.prototype.add=function(waypoint){var axis=waypoint.options.horizontal?"horizontal":"vertical";this.waypoints[axis][waypoint.key]=waypoint,this.refresh()},Context.prototype.checkEmpty=function(){var horizontalEmpty=this.Adapter.isEmptyObject(this.waypoints.horizontal),verticalEmpty=this.Adapter.isEmptyObject(this.waypoints.vertical),isWindow=this.element==this.element.window;horizontalEmpty&&verticalEmpty&&!isWindow&&(this.adapter.off(".waypoints"),delete contexts[this.key])},Context.prototype.createThrottledResizeHandler=function(){function resizeHandler(){self.handleResize(),self.didResize=!1}var self=this;this.adapter.on("resize.waypoints",function(){self.didResize||(self.didResize=!0,Waypoint.requestAnimationFrame(resizeHandler))})},Context.prototype.createThrottledScrollHandler=function(){function scrollHandler(){self.handleScroll(),self.didScroll=!1}var self=this;this.adapter.on("scroll.waypoints",function(){self.didScroll&&!Waypoint.isTouch||(self.didScroll=!0,Waypoint.requestAnimationFrame(scrollHandler))})},Context.prototype.handleResize=function(){Waypoint.Context.refreshAll()},Context.prototype.handleScroll=function(){var triggeredGroups={},axes={horizontal:{newScroll:this.adapter.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.adapter.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};for(var axisKey in axes){var axis=axes[axisKey],isForward=axis.newScroll>axis.oldScroll,direction=isForward?axis.forward:axis.backward;for(var waypointKey in this.waypoints[axisKey]){var waypoint=this.waypoints[axisKey][waypointKey];if(null!==waypoint.triggerPoint){var wasBeforeTriggerPoint=axis.oldScroll<waypoint.triggerPoint,nowAfterTriggerPoint=axis.newScroll>=waypoint.triggerPoint,crossedForward=wasBeforeTriggerPoint&&nowAfterTriggerPoint,crossedBackward=!wasBeforeTriggerPoint&&!nowAfterTriggerPoint;(crossedForward||crossedBackward)&&(waypoint.queueTrigger(direction),triggeredGroups[waypoint.group.id]=waypoint.group)}}}for(var groupKey in triggeredGroups)triggeredGroups[groupKey].flushTriggers();this.oldScroll={x:axes.horizontal.newScroll,y:axes.vertical.newScroll}},Context.prototype.innerHeight=function(){return this.element==this.element.window?Waypoint.viewportHeight():this.adapter.innerHeight()},Context.prototype.remove=function(waypoint){delete this.waypoints[waypoint.axis][waypoint.key],this.checkEmpty()},Context.prototype.innerWidth=function(){return this.element==this.element.window?Waypoint.viewportWidth():this.adapter.innerWidth()},Context.prototype.destroy=function(){var allWaypoints=[];for(var axis in this.waypoints)for(var waypointKey in this.waypoints[axis])allWaypoints.push(this.waypoints[axis][waypointKey]);for(var i=0,end=allWaypoints.length;i<end;i++)allWaypoints[i].destroy()},Context.prototype.refresh=function(){var axes,isWindow=this.element==this.element.window,contextOffset=isWindow?void 0:this.adapter.offset(),triggeredGroups={};this.handleScroll(),axes={horizontal:{contextOffset:isWindow?0:contextOffset.left,contextScroll:isWindow?0:this.oldScroll.x,contextDimension:this.innerWidth(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:isWindow?0:contextOffset.top,contextScroll:isWindow?0:this.oldScroll.y,contextDimension:this.innerHeight(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};for(var axisKey in axes){var axis=axes[axisKey];for(var waypointKey in this.waypoints[axisKey]){var contextModifier,wasBeforeScroll,nowAfterScroll,triggeredBackward,triggeredForward,waypoint=this.waypoints[axisKey][waypointKey],adjustment=waypoint.options.offset,oldTriggerPoint=waypoint.triggerPoint,elementOffset=0,freshWaypoint=null==oldTriggerPoint;waypoint.element!==waypoint.element.window&&(elementOffset=waypoint.adapter.offset()[axis.offsetProp]),"function"==typeof adjustment?adjustment=adjustment.apply(waypoint):"string"==typeof adjustment&&(adjustment=parseFloat(adjustment),waypoint.options.offset.indexOf("%")>-1&&(adjustment=Math.ceil(axis.contextDimension*adjustment/100))),contextModifier=axis.contextScroll-axis.contextOffset,waypoint.triggerPoint=Math.floor(elementOffset+contextModifier-adjustment),wasBeforeScroll=oldTriggerPoint<axis.oldScroll,nowAfterScroll=waypoint.triggerPoint>=axis.oldScroll,triggeredBackward=wasBeforeScroll&&nowAfterScroll,triggeredForward=!wasBeforeScroll&&!nowAfterScroll,!freshWaypoint&&triggeredBackward?(waypoint.queueTrigger(axis.backward),triggeredGroups[waypoint.group.id]=waypoint.group):!freshWaypoint&&triggeredForward?(waypoint.queueTrigger(axis.forward),triggeredGroups[waypoint.group.id]=waypoint.group):freshWaypoint&&axis.oldScroll>=waypoint.triggerPoint&&(waypoint.queueTrigger(axis.forward),triggeredGroups[waypoint.group.id]=waypoint.group)}}return Waypoint.requestAnimationFrame(function(){for(var groupKey in triggeredGroups)triggeredGroups[groupKey].flushTriggers()}),this},Context.findOrCreateByElement=function(element){return Context.findByElement(element)||new Context(element)},Context.refreshAll=function(){for(var contextId in contexts)contexts[contextId].refresh()},Context.findByElement=function(element){return contexts[element.waypointContextKey]},window.onload=function(){oldWindowLoad&&oldWindowLoad(),Context.refreshAll()},Waypoint.requestAnimationFrame=function(callback){var requestFn=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||requestAnimationFrameShim;requestFn.call(window,callback)},Waypoint.Context=Context}(),function(){"use strict";function byTriggerPoint(a,b){return a.triggerPoint-b.triggerPoint}function byReverseTriggerPoint(a,b){return b.triggerPoint-a.triggerPoint}function Group(options){this.name=options.name,this.axis=options.axis,this.id=this.name+"-"+this.axis,this.waypoints=[],this.clearTriggerQueues(),groups[this.axis][this.name]=this}var groups={vertical:{},horizontal:{}},Waypoint=window.Waypoint;Group.prototype.add=function(waypoint){this.waypoints.push(waypoint)},Group.prototype.clearTriggerQueues=function(){this.triggerQueues={up:[],down:[],left:[],right:[]}},Group.prototype.flushTriggers=function(){for(var direction in this.triggerQueues){var waypoints=this.triggerQueues[direction],reverse="up"===direction||"left"===direction;waypoints.sort(reverse?byReverseTriggerPoint:byTriggerPoint);for(var i=0,end=waypoints.length;i<end;i+=1){var waypoint=waypoints[i];(waypoint.options.continuous||i===waypoints.length-1)&&waypoint.trigger([direction])}}this.clearTriggerQueues()},Group.prototype.next=function(waypoint){this.waypoints.sort(byTriggerPoint);var index=Waypoint.Adapter.inArray(waypoint,this.waypoints),isLast=index===this.waypoints.length-1;return isLast?null:this.waypoints[index+1]},Group.prototype.previous=function(waypoint){this.waypoints.sort(byTriggerPoint);var index=Waypoint.Adapter.inArray(waypoint,this.waypoints);return index?this.waypoints[index-1]:null},Group.prototype.queueTrigger=function(waypoint,direction){this.triggerQueues[direction].push(waypoint)},Group.prototype.remove=function(waypoint){var index=Waypoint.Adapter.inArray(waypoint,this.waypoints);index>-1&&this.waypoints.splice(index,1)},Group.prototype.first=function(){return this.waypoints[0]},Group.prototype.last=function(){return this.waypoints[this.waypoints.length-1]},Group.findOrCreate=function(options){return groups[options.axis][options.name]||new Group(options)},Waypoint.Group=Group}(),function(){"use strict";function JQueryAdapter(element){this.$element=$(element)}var $=window.jQuery,Waypoint=window.Waypoint;$.each(["innerHeight","innerWidth","off","offset","on","outerHeight","outerWidth","scrollLeft","scrollTop"],function(i,method){JQueryAdapter.prototype[method]=function(){var args=Array.prototype.slice.call(arguments);return this.$element[method].apply(this.$element,args)}}),$.each(["extend","inArray","isEmptyObject"],function(i,method){JQueryAdapter[method]=$[method]}),Waypoint.adapters.push({name:"jquery",Adapter:JQueryAdapter}),Waypoint.Adapter=JQueryAdapter}(),function(){"use strict";function createExtension(framework){return function(){var waypoints=[],overrides=arguments[0];return framework.isFunction(arguments[0])&&(overrides=framework.extend({},arguments[1]),overrides.handler=arguments[0]),this.each(function(){var options=framework.extend({},overrides,{element:this});"string"==typeof options.context&&(options.context=framework(this).closest(options.context)[0]),waypoints.push(new Waypoint(options))}),waypoints}}var Waypoint=window.Waypoint;window.jQuery&&(window.jQuery.fn.elementorWaypoint=createExtension(window.jQuery)),window.Zepto&&(window.Zepto.fn.elementorWaypoint=createExtension(window.Zepto))}();
!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):e.Swiper=t()}(this,function(){"use strict";var e="undefined"==typeof document?{body:{},addEventListener:function(){},removeEventListener:function(){},activeElement:{blur:function(){},nodeName:""},querySelector:function(){return null},querySelectorAll:function(){return[]},getElementById:function(){return null},createEvent:function(){return{initEvent:function(){}}},createElement:function(){return{children:[],childNodes:[],style:{},setAttribute:function(){},getElementsByTagName:function(){return[]}}},location:{hash:""}}:document,t="undefined"==typeof window?{document:e,navigator:{userAgent:""},location:{},history:{},CustomEvent:function(){return this},addEventListener:function(){},removeEventListener:function(){},getComputedStyle:function(){return{getPropertyValue:function(){return""}}},Image:function(){},Date:function(){},screen:{},setTimeout:function(){},clearTimeout:function(){}}:window,i=function(e){for(var t=0;t<e.length;t+=1)this[t]=e[t];return this.length=e.length,this};function s(s,a){var r=[],n=0;if(s&&!a&&s instanceof i)return s;if(s)if("string"==typeof s){var o,l,d=s.trim();if(d.indexOf("<")>=0&&d.indexOf(">")>=0){var h="div";for(0===d.indexOf("<li")&&(h="ul"),0===d.indexOf("<tr")&&(h="tbody"),0!==d.indexOf("<td")&&0!==d.indexOf("<th")||(h="tr"),0===d.indexOf("<tbody")&&(h="table"),0===d.indexOf("<option")&&(h="select"),(l=e.createElement(h)).innerHTML=d,n=0;n<l.childNodes.length;n+=1)r.push(l.childNodes[n])}else for(o=a||"#"!==s[0]||s.match(/[ .<>:~]/)?(a||e).querySelectorAll(s.trim()):[e.getElementById(s.trim().split("#")[1])],n=0;n<o.length;n+=1)o[n]&&r.push(o[n])}else if(s.nodeType||s===t||s===e)r.push(s);else if(s.length>0&&s[0].nodeType)for(n=0;n<s.length;n+=1)r.push(s[n]);return new i(r)}function a(e){for(var t=[],i=0;i<e.length;i+=1)-1===t.indexOf(e[i])&&t.push(e[i]);return t}s.fn=i.prototype,s.Class=i,s.Dom7=i;var r={addClass:function(e){if(void 0===e)return this;for(var t=e.split(" "),i=0;i<t.length;i+=1)for(var s=0;s<this.length;s+=1)void 0!==this[s].classList&&this[s].classList.add(t[i]);return this},removeClass:function(e){for(var t=e.split(" "),i=0;i<t.length;i+=1)for(var s=0;s<this.length;s+=1)void 0!==this[s].classList&&this[s].classList.remove(t[i]);return this},hasClass:function(e){return!!this[0]&&this[0].classList.contains(e)},toggleClass:function(e){for(var t=e.split(" "),i=0;i<t.length;i+=1)for(var s=0;s<this.length;s+=1)void 0!==this[s].classList&&this[s].classList.toggle(t[i]);return this},attr:function(e,t){var i=arguments;if(1===arguments.length&&"string"==typeof e)return this[0]?this[0].getAttribute(e):void 0;for(var s=0;s<this.length;s+=1)if(2===i.length)this[s].setAttribute(e,t);else for(var a in e)this[s][a]=e[a],this[s].setAttribute(a,e[a]);return this},removeAttr:function(e){for(var t=0;t<this.length;t+=1)this[t].removeAttribute(e);return this},data:function(e,t){var i;if(void 0!==t){for(var s=0;s<this.length;s+=1)(i=this[s]).dom7ElementDataStorage||(i.dom7ElementDataStorage={}),i.dom7ElementDataStorage[e]=t;return this}if(i=this[0]){if(i.dom7ElementDataStorage&&e in i.dom7ElementDataStorage)return i.dom7ElementDataStorage[e];var a=i.getAttribute("data-"+e);return a||void 0}},transform:function(e){for(var t=0;t<this.length;t+=1){var i=this[t].style;i.webkitTransform=e,i.transform=e}return this},transition:function(e){"string"!=typeof e&&(e+="ms");for(var t=0;t<this.length;t+=1){var i=this[t].style;i.webkitTransitionDuration=e,i.transitionDuration=e}return this},on:function(){for(var e,t=[],i=arguments.length;i--;)t[i]=arguments[i];var a=t[0],r=t[1],n=t[2],o=t[3];function l(e){var t=e.target;if(t){var i=e.target.dom7EventData||[];if(i.indexOf(e)<0&&i.unshift(e),s(t).is(r))n.apply(t,i);else for(var a=s(t).parents(),o=0;o<a.length;o+=1)s(a[o]).is(r)&&n.apply(a[o],i)}}function d(e){var t=e&&e.target?e.target.dom7EventData||[]:[];t.indexOf(e)<0&&t.unshift(e),n.apply(this,t)}"function"==typeof t[1]&&(a=(e=t)[0],n=e[1],o=e[2],r=void 0),o||(o=!1);for(var h,p=a.split(" "),c=0;c<this.length;c+=1){var u=this[c];if(r)for(h=0;h<p.length;h+=1){var v=p[h];u.dom7LiveListeners||(u.dom7LiveListeners={}),u.dom7LiveListeners[v]||(u.dom7LiveListeners[v]=[]),u.dom7LiveListeners[v].push({listener:n,proxyListener:l}),u.addEventListener(v,l,o)}else for(h=0;h<p.length;h+=1){var f=p[h];u.dom7Listeners||(u.dom7Listeners={}),u.dom7Listeners[f]||(u.dom7Listeners[f]=[]),u.dom7Listeners[f].push({listener:n,proxyListener:d}),u.addEventListener(f,d,o)}}return this},off:function(){for(var e,t=[],i=arguments.length;i--;)t[i]=arguments[i];var s=t[0],a=t[1],r=t[2],n=t[3];"function"==typeof t[1]&&(s=(e=t)[0],r=e[1],n=e[2],a=void 0),n||(n=!1);for(var o=s.split(" "),l=0;l<o.length;l+=1)for(var d=o[l],h=0;h<this.length;h+=1){var p=this[h],c=void 0;if(!a&&p.dom7Listeners?c=p.dom7Listeners[d]:a&&p.dom7LiveListeners&&(c=p.dom7LiveListeners[d]),c&&c.length)for(var u=c.length-1;u>=0;u-=1){var v=c[u];r&&v.listener===r?(p.removeEventListener(d,v.proxyListener,n),c.splice(u,1)):r||(p.removeEventListener(d,v.proxyListener,n),c.splice(u,1))}}return this},trigger:function(){for(var i=[],s=arguments.length;s--;)i[s]=arguments[s];for(var a=i[0].split(" "),r=i[1],n=0;n<a.length;n+=1)for(var o=a[n],l=0;l<this.length;l+=1){var d=this[l],h=void 0;try{h=new t.CustomEvent(o,{detail:r,bubbles:!0,cancelable:!0})}catch(t){(h=e.createEvent("Event")).initEvent(o,!0,!0),h.detail=r}d.dom7EventData=i.filter(function(e,t){return t>0}),d.dispatchEvent(h),d.dom7EventData=[],delete d.dom7EventData}return this},transitionEnd:function(e){var t,i=["webkitTransitionEnd","transitionend"],s=this;function a(r){if(r.target===this)for(e.call(this,r),t=0;t<i.length;t+=1)s.off(i[t],a)}if(e)for(t=0;t<i.length;t+=1)s.on(i[t],a);return this},outerWidth:function(e){if(this.length>0){if(e){var t=this.styles();return this[0].offsetWidth+parseFloat(t.getPropertyValue("margin-right"))+parseFloat(t.getPropertyValue("margin-left"))}return this[0].offsetWidth}return null},outerHeight:function(e){if(this.length>0){if(e){var t=this.styles();return this[0].offsetHeight+parseFloat(t.getPropertyValue("margin-top"))+parseFloat(t.getPropertyValue("margin-bottom"))}return this[0].offsetHeight}return null},offset:function(){if(this.length>0){var i=this[0],s=i.getBoundingClientRect(),a=e.body,r=i.clientTop||a.clientTop||0,n=i.clientLeft||a.clientLeft||0,o=i===t?t.scrollY:i.scrollTop,l=i===t?t.scrollX:i.scrollLeft;return{top:s.top+o-r,left:s.left+l-n}}return null},css:function(e,i){var s;if(1===arguments.length){if("string"!=typeof e){for(s=0;s<this.length;s+=1)for(var a in e)this[s].style[a]=e[a];return this}if(this[0])return t.getComputedStyle(this[0],null).getPropertyValue(e)}if(2===arguments.length&&"string"==typeof e){for(s=0;s<this.length;s+=1)this[s].style[e]=i;return this}return this},each:function(e){if(!e)return this;for(var t=0;t<this.length;t+=1)if(!1===e.call(this[t],t,this[t]))return this;return this},html:function(e){if(void 0===e)return this[0]?this[0].innerHTML:void 0;for(var t=0;t<this.length;t+=1)this[t].innerHTML=e;return this},text:function(e){if(void 0===e)return this[0]?this[0].textContent.trim():null;for(var t=0;t<this.length;t+=1)this[t].textContent=e;return this},is:function(a){var r,n,o=this[0];if(!o||void 0===a)return!1;if("string"==typeof a){if(o.matches)return o.matches(a);if(o.webkitMatchesSelector)return o.webkitMatchesSelector(a);if(o.msMatchesSelector)return o.msMatchesSelector(a);for(r=s(a),n=0;n<r.length;n+=1)if(r[n]===o)return!0;return!1}if(a===e)return o===e;if(a===t)return o===t;if(a.nodeType||a instanceof i){for(r=a.nodeType?[a]:a,n=0;n<r.length;n+=1)if(r[n]===o)return!0;return!1}return!1},index:function(){var e,t=this[0];if(t){for(e=0;null!==(t=t.previousSibling);)1===t.nodeType&&(e+=1);return e}},eq:function(e){if(void 0===e)return this;var t,s=this.length;return new i(e>s-1?[]:e<0?(t=s+e)<0?[]:[this[t]]:[this[e]])},append:function(){for(var t,s=[],a=arguments.length;a--;)s[a]=arguments[a];for(var r=0;r<s.length;r+=1){t=s[r];for(var n=0;n<this.length;n+=1)if("string"==typeof t){var o=e.createElement("div");for(o.innerHTML=t;o.firstChild;)this[n].appendChild(o.firstChild)}else if(t instanceof i)for(var l=0;l<t.length;l+=1)this[n].appendChild(t[l]);else this[n].appendChild(t)}return this},prepend:function(t){var s,a;for(s=0;s<this.length;s+=1)if("string"==typeof t){var r=e.createElement("div");for(r.innerHTML=t,a=r.childNodes.length-1;a>=0;a-=1)this[s].insertBefore(r.childNodes[a],this[s].childNodes[0])}else if(t instanceof i)for(a=0;a<t.length;a+=1)this[s].insertBefore(t[a],this[s].childNodes[0]);else this[s].insertBefore(t,this[s].childNodes[0]);return this},next:function(e){return this.length>0?e?this[0].nextElementSibling&&s(this[0].nextElementSibling).is(e)?new i([this[0].nextElementSibling]):new i([]):this[0].nextElementSibling?new i([this[0].nextElementSibling]):new i([]):new i([])},nextAll:function(e){var t=[],a=this[0];if(!a)return new i([]);for(;a.nextElementSibling;){var r=a.nextElementSibling;e?s(r).is(e)&&t.push(r):t.push(r),a=r}return new i(t)},prev:function(e){if(this.length>0){var t=this[0];return e?t.previousElementSibling&&s(t.previousElementSibling).is(e)?new i([t.previousElementSibling]):new i([]):t.previousElementSibling?new i([t.previousElementSibling]):new i([])}return new i([])},prevAll:function(e){var t=[],a=this[0];if(!a)return new i([]);for(;a.previousElementSibling;){var r=a.previousElementSibling;e?s(r).is(e)&&t.push(r):t.push(r),a=r}return new i(t)},parent:function(e){for(var t=[],i=0;i<this.length;i+=1)null!==this[i].parentNode&&(e?s(this[i].parentNode).is(e)&&t.push(this[i].parentNode):t.push(this[i].parentNode));return s(a(t))},parents:function(e){for(var t=[],i=0;i<this.length;i+=1)for(var r=this[i].parentNode;r;)e?s(r).is(e)&&t.push(r):t.push(r),r=r.parentNode;return s(a(t))},closest:function(e){var t=this;return void 0===e?new i([]):(t.is(e)||(t=t.parents(e).eq(0)),t)},find:function(e){for(var t=[],s=0;s<this.length;s+=1)for(var a=this[s].querySelectorAll(e),r=0;r<a.length;r+=1)t.push(a[r]);return new i(t)},children:function(e){for(var t=[],r=0;r<this.length;r+=1)for(var n=this[r].childNodes,o=0;o<n.length;o+=1)e?1===n[o].nodeType&&s(n[o]).is(e)&&t.push(n[o]):1===n[o].nodeType&&t.push(n[o]);return new i(a(t))},remove:function(){for(var e=0;e<this.length;e+=1)this[e].parentNode&&this[e].parentNode.removeChild(this[e]);return this},add:function(){for(var e=[],t=arguments.length;t--;)e[t]=arguments[t];var i,a;for(i=0;i<e.length;i+=1){var r=s(e[i]);for(a=0;a<r.length;a+=1)this[this.length]=r[a],this.length+=1}return this},styles:function(){return this[0]?t.getComputedStyle(this[0],null):{}}};Object.keys(r).forEach(function(e){s.fn[e]=r[e]});var n,o,l,d={deleteProps:function(e){var t=e;Object.keys(t).forEach(function(e){try{t[e]=null}catch(e){}try{delete t[e]}catch(e){}})},nextTick:function(e,t){return void 0===t&&(t=0),setTimeout(e,t)},now:function(){return Date.now()},getTranslate:function(e,i){var s,a,r;void 0===i&&(i="x");var n=t.getComputedStyle(e,null);return t.WebKitCSSMatrix?((a=n.transform||n.webkitTransform).split(",").length>6&&(a=a.split(", ").map(function(e){return e.replace(",",".")}).join(", ")),r=new t.WebKitCSSMatrix("none"===a?"":a)):s=(r=n.MozTransform||n.OTransform||n.MsTransform||n.msTransform||n.transform||n.getPropertyValue("transform").replace("translate(","matrix(1, 0, 0, 1,")).toString().split(","),"x"===i&&(a=t.WebKitCSSMatrix?r.m41:16===s.length?parseFloat(s[12]):parseFloat(s[4])),"y"===i&&(a=t.WebKitCSSMatrix?r.m42:16===s.length?parseFloat(s[13]):parseFloat(s[5])),a||0},parseUrlQuery:function(e){var i,s,a,r,n={},o=e||t.location.href;if("string"==typeof o&&o.length)for(r=(s=(o=o.indexOf("?")>-1?o.replace(/\S*\?/,""):"").split("&").filter(function(e){return""!==e})).length,i=0;i<r;i+=1)a=s[i].replace(/#\S+/g,"").split("="),n[decodeURIComponent(a[0])]=void 0===a[1]?void 0:decodeURIComponent(a[1])||"";return n},isObject:function(e){return"object"==typeof e&&null!==e&&e.constructor&&e.constructor===Object},extend:function(){for(var e=[],t=arguments.length;t--;)e[t]=arguments[t];for(var i=Object(e[0]),s=1;s<e.length;s+=1){var a=e[s];if(void 0!==a&&null!==a)for(var r=Object.keys(Object(a)),n=0,o=r.length;n<o;n+=1){var l=r[n],h=Object.getOwnPropertyDescriptor(a,l);void 0!==h&&h.enumerable&&(d.isObject(i[l])&&d.isObject(a[l])?d.extend(i[l],a[l]):!d.isObject(i[l])&&d.isObject(a[l])?(i[l]={},d.extend(i[l],a[l])):i[l]=a[l])}}return i}},h=(l=e.createElement("div"),{touch:t.Modernizr&&!0===t.Modernizr.touch||!!("ontouchstart"in t||t.DocumentTouch&&e instanceof t.DocumentTouch),pointerEvents:!(!t.navigator.pointerEnabled&&!t.PointerEvent),prefixedPointerEvents:!!t.navigator.msPointerEnabled,transition:(o=l.style,"transition"in o||"webkitTransition"in o||"MozTransition"in o),transforms3d:t.Modernizr&&!0===t.Modernizr.csstransforms3d||(n=l.style,"webkitPerspective"in n||"MozPerspective"in n||"OPerspective"in n||"MsPerspective"in n||"perspective"in n),flexbox:function(){for(var e=l.style,t="alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "),i=0;i<t.length;i+=1)if(t[i]in e)return!0;return!1}(),observer:"MutationObserver"in t||"WebkitMutationObserver"in t,passiveListener:function(){var e=!1;try{var i=Object.defineProperty({},"passive",{get:function(){e=!0}});t.addEventListener("testPassiveListener",null,i)}catch(e){}return e}(),gestures:"ongesturestart"in t}),p=function(e){void 0===e&&(e={});var t=this;t.params=e,t.eventsListeners={},t.params&&t.params.on&&Object.keys(t.params.on).forEach(function(e){t.on(e,t.params.on[e])})},c={components:{configurable:!0}};p.prototype.on=function(e,t,i){var s=this;if("function"!=typeof t)return s;var a=i?"unshift":"push";return e.split(" ").forEach(function(e){s.eventsListeners[e]||(s.eventsListeners[e]=[]),s.eventsListeners[e][a](t)}),s},p.prototype.once=function(e,t,i){var s=this;if("function"!=typeof t)return s;return s.on(e,function i(){for(var a=[],r=arguments.length;r--;)a[r]=arguments[r];t.apply(s,a),s.off(e,i)},i)},p.prototype.off=function(e,t){var i=this;return i.eventsListeners?(e.split(" ").forEach(function(e){void 0===t?i.eventsListeners[e]=[]:i.eventsListeners[e].forEach(function(s,a){s===t&&i.eventsListeners[e].splice(a,1)})}),i):i},p.prototype.emit=function(){for(var e=[],t=arguments.length;t--;)e[t]=arguments[t];var i,s,a,r=this;return r.eventsListeners?("string"==typeof e[0]||Array.isArray(e[0])?(i=e[0],s=e.slice(1,e.length),a=r):(i=e[0].events,s=e[0].data,a=e[0].context||r),(Array.isArray(i)?i:i.split(" ")).forEach(function(e){if(r.eventsListeners&&r.eventsListeners[e]){var t=[];r.eventsListeners[e].forEach(function(e){t.push(e)}),t.forEach(function(e){e.apply(a,s)})}}),r):r},p.prototype.useModulesParams=function(e){var t=this;t.modules&&Object.keys(t.modules).forEach(function(i){var s=t.modules[i];s.params&&d.extend(e,s.params)})},p.prototype.useModules=function(e){void 0===e&&(e={});var t=this;t.modules&&Object.keys(t.modules).forEach(function(i){var s=t.modules[i],a=e[i]||{};s.instance&&Object.keys(s.instance).forEach(function(e){var i=s.instance[e];t[e]="function"==typeof i?i.bind(t):i}),s.on&&t.on&&Object.keys(s.on).forEach(function(e){t.on(e,s.on[e])}),s.create&&s.create.bind(t)(a)})},c.components.set=function(e){this.use&&this.use(e)},p.installModule=function(e){for(var t=[],i=arguments.length-1;i-- >0;)t[i]=arguments[i+1];var s=this;s.prototype.modules||(s.prototype.modules={});var a=e.name||Object.keys(s.prototype.modules).length+"_"+d.now();return s.prototype.modules[a]=e,e.proto&&Object.keys(e.proto).forEach(function(t){s.prototype[t]=e.proto[t]}),e.static&&Object.keys(e.static).forEach(function(t){s[t]=e.static[t]}),e.install&&e.install.apply(s,t),s},p.use=function(e){for(var t=[],i=arguments.length-1;i-- >0;)t[i]=arguments[i+1];var s=this;return Array.isArray(e)?(e.forEach(function(e){return s.installModule(e)}),s):s.installModule.apply(s,[e].concat(t))},Object.defineProperties(p,c);var u={updateSize:function(){var e,t,i=this.$el;e=void 0!==this.params.width?this.params.width:i[0].clientWidth,t=void 0!==this.params.height?this.params.height:i[0].clientHeight,0===e&&this.isHorizontal()||0===t&&this.isVertical()||(e=e-parseInt(i.css("padding-left"),10)-parseInt(i.css("padding-right"),10),t=t-parseInt(i.css("padding-top"),10)-parseInt(i.css("padding-bottom"),10),d.extend(this,{width:e,height:t,size:this.isHorizontal()?e:t}))},updateSlides:function(){var e=this.params,i=this.$wrapperEl,s=this.size,a=this.rtlTranslate,r=this.wrongRTL,n=this.virtual&&e.virtual.enabled,o=n?this.virtual.slides.length:this.slides.length,l=i.children("."+this.params.slideClass),p=n?this.virtual.slides.length:l.length,c=[],u=[],v=[],f=e.slidesOffsetBefore;"function"==typeof f&&(f=e.slidesOffsetBefore.call(this));var m=e.slidesOffsetAfter;"function"==typeof m&&(m=e.slidesOffsetAfter.call(this));var g=this.snapGrid.length,b=this.snapGrid.length,w=e.spaceBetween,y=-f,x=0,E=0;if(void 0!==s){var T,S;"string"==typeof w&&w.indexOf("%")>=0&&(w=parseFloat(w.replace("%",""))/100*s),this.virtualSize=-w,a?l.css({marginLeft:"",marginTop:""}):l.css({marginRight:"",marginBottom:""}),e.slidesPerColumn>1&&(T=Math.floor(p/e.slidesPerColumn)===p/this.params.slidesPerColumn?p:Math.ceil(p/e.slidesPerColumn)*e.slidesPerColumn,"auto"!==e.slidesPerView&&"row"===e.slidesPerColumnFill&&(T=Math.max(T,e.slidesPerView*e.slidesPerColumn)));for(var C,M=e.slidesPerColumn,z=T/M,k=z-(e.slidesPerColumn*z-p),P=0;P<p;P+=1){S=0;var $=l.eq(P);if(e.slidesPerColumn>1){var L=void 0,I=void 0,D=void 0;"column"===e.slidesPerColumnFill?(D=P-(I=Math.floor(P/M))*M,(I>k||I===k&&D===M-1)&&(D+=1)>=M&&(D=0,I+=1),L=I+D*T/M,$.css({"-webkit-box-ordinal-group":L,"-moz-box-ordinal-group":L,"-ms-flex-order":L,"-webkit-order":L,order:L})):I=P-(D=Math.floor(P/z))*z,$.css("margin-"+(this.isHorizontal()?"top":"left"),0!==D&&e.spaceBetween&&e.spaceBetween+"px").attr("data-swiper-column",I).attr("data-swiper-row",D)}if("none"!==$.css("display")){if("auto"===e.slidesPerView){var O=t.getComputedStyle($[0],null),A=$[0].style.transform,G=$[0].style.webkitTransform;A&&($[0].style.transform="none"),G&&($[0].style.webkitTransform="none"),S=this.isHorizontal()?$[0].getBoundingClientRect().width+parseFloat(O.getPropertyValue("margin-left"))+parseFloat(O.getPropertyValue("margin-right")):$[0].getBoundingClientRect().height+parseFloat(O.getPropertyValue("margin-top"))+parseFloat(O.getPropertyValue("margin-bottom")),A&&($[0].style.transform=A),G&&($[0].style.webkitTransform=G),e.roundLengths&&(S=Math.floor(S))}else S=(s-(e.slidesPerView-1)*w)/e.slidesPerView,e.roundLengths&&(S=Math.floor(S)),l[P]&&(this.isHorizontal()?l[P].style.width=S+"px":l[P].style.height=S+"px");l[P]&&(l[P].swiperSlideSize=S),v.push(S),e.centeredSlides?(y=y+S/2+x/2+w,0===x&&0!==P&&(y=y-s/2-w),0===P&&(y=y-s/2-w),Math.abs(y)<.001&&(y=0),e.roundLengths&&(y=Math.floor(y)),E%e.slidesPerGroup==0&&c.push(y),u.push(y)):(e.roundLengths&&(y=Math.floor(y)),E%e.slidesPerGroup==0&&c.push(y),u.push(y),y=y+S+w),this.virtualSize+=S+w,x=S,E+=1}}if(this.virtualSize=Math.max(this.virtualSize,s)+m,a&&r&&("slide"===e.effect||"coverflow"===e.effect)&&i.css({width:this.virtualSize+e.spaceBetween+"px"}),h.flexbox&&!e.setWrapperSize||(this.isHorizontal()?i.css({width:this.virtualSize+e.spaceBetween+"px"}):i.css({height:this.virtualSize+e.spaceBetween+"px"})),e.slidesPerColumn>1&&(this.virtualSize=(S+e.spaceBetween)*T,this.virtualSize=Math.ceil(this.virtualSize/e.slidesPerColumn)-e.spaceBetween,this.isHorizontal()?i.css({width:this.virtualSize+e.spaceBetween+"px"}):i.css({height:this.virtualSize+e.spaceBetween+"px"}),e.centeredSlides)){C=[];for(var H=0;H<c.length;H+=1){var N=c[H];e.roundLengths&&(N=Math.floor(N)),c[H]<this.virtualSize+c[0]&&C.push(N)}c=C}if(!e.centeredSlides){C=[];for(var B=0;B<c.length;B+=1){var X=c[B];e.roundLengths&&(X=Math.floor(X)),c[B]<=this.virtualSize-s&&C.push(X)}c=C,Math.floor(this.virtualSize-s)-Math.floor(c[c.length-1])>1&&c.push(this.virtualSize-s)}0===c.length&&(c=[0]),0!==e.spaceBetween&&(this.isHorizontal()?a?l.css({marginLeft:w+"px"}):l.css({marginRight:w+"px"}):l.css({marginBottom:w+"px"})),d.extend(this,{slides:l,snapGrid:c,slidesGrid:u,slidesSizesGrid:v}),p!==o&&this.emit("slidesLengthChange"),c.length!==g&&(this.params.watchOverflow&&this.checkOverflow(),this.emit("snapGridLengthChange")),u.length!==b&&this.emit("slidesGridLengthChange"),(e.watchSlidesProgress||e.watchSlidesVisibility)&&this.updateSlidesOffset()}},updateAutoHeight:function(e){var t,i=[],s=0;if("number"==typeof e?this.setTransition(e):!0===e&&this.setTransition(this.params.speed),"auto"!==this.params.slidesPerView&&this.params.slidesPerView>1)for(t=0;t<Math.ceil(this.params.slidesPerView);t+=1){var a=this.activeIndex+t;if(a>this.slides.length)break;i.push(this.slides.eq(a)[0])}else i.push(this.slides.eq(this.activeIndex)[0]);for(t=0;t<i.length;t+=1)if(void 0!==i[t]){var r=i[t].offsetHeight;s=r>s?r:s}s&&this.$wrapperEl.css("height",s+"px")},updateSlidesOffset:function(){for(var e=this.slides,t=0;t<e.length;t+=1)e[t].swiperSlideOffset=this.isHorizontal()?e[t].offsetLeft:e[t].offsetTop},updateSlidesProgress:function(e){void 0===e&&(e=this&&this.translate||0);var t=this.params,i=this.slides,s=this.rtlTranslate;if(0!==i.length){void 0===i[0].swiperSlideOffset&&this.updateSlidesOffset();var a=-e;s&&(a=e),i.removeClass(t.slideVisibleClass);for(var r=0;r<i.length;r+=1){var n=i[r],o=(a+(t.centeredSlides?this.minTranslate():0)-n.swiperSlideOffset)/(n.swiperSlideSize+t.spaceBetween);if(t.watchSlidesVisibility){var l=-(a-n.swiperSlideOffset),d=l+this.slidesSizesGrid[r];(l>=0&&l<this.size||d>0&&d<=this.size||l<=0&&d>=this.size)&&i.eq(r).addClass(t.slideVisibleClass)}n.progress=s?-o:o}}},updateProgress:function(e){void 0===e&&(e=this&&this.translate||0);var t=this.params,i=this.maxTranslate()-this.minTranslate(),s=this.progress,a=this.isBeginning,r=this.isEnd,n=a,o=r;0===i?(s=0,a=!0,r=!0):(a=(s=(e-this.minTranslate())/i)<=0,r=s>=1),d.extend(this,{progress:s,isBeginning:a,isEnd:r}),(t.watchSlidesProgress||t.watchSlidesVisibility)&&this.updateSlidesProgress(e),a&&!n&&this.emit("reachBeginning toEdge"),r&&!o&&this.emit("reachEnd toEdge"),(n&&!a||o&&!r)&&this.emit("fromEdge"),this.emit("progress",s)},updateSlidesClasses:function(){var e,t=this.slides,i=this.params,s=this.$wrapperEl,a=this.activeIndex,r=this.realIndex,n=this.virtual&&i.virtual.enabled;t.removeClass(i.slideActiveClass+" "+i.slideNextClass+" "+i.slidePrevClass+" "+i.slideDuplicateActiveClass+" "+i.slideDuplicateNextClass+" "+i.slideDuplicatePrevClass),(e=n?this.$wrapperEl.find("."+i.slideClass+'[data-swiper-slide-index="'+a+'"]'):t.eq(a)).addClass(i.slideActiveClass),i.loop&&(e.hasClass(i.slideDuplicateClass)?s.children("."+i.slideClass+":not(."+i.slideDuplicateClass+')[data-swiper-slide-index="'+r+'"]').addClass(i.slideDuplicateActiveClass):s.children("."+i.slideClass+"."+i.slideDuplicateClass+'[data-swiper-slide-index="'+r+'"]').addClass(i.slideDuplicateActiveClass));var o=e.nextAll("."+i.slideClass).eq(0).addClass(i.slideNextClass);i.loop&&0===o.length&&(o=t.eq(0)).addClass(i.slideNextClass);var l=e.prevAll("."+i.slideClass).eq(0).addClass(i.slidePrevClass);i.loop&&0===l.length&&(l=t.eq(-1)).addClass(i.slidePrevClass),i.loop&&(o.hasClass(i.slideDuplicateClass)?s.children("."+i.slideClass+":not(."+i.slideDuplicateClass+')[data-swiper-slide-index="'+o.attr("data-swiper-slide-index")+'"]').addClass(i.slideDuplicateNextClass):s.children("."+i.slideClass+"."+i.slideDuplicateClass+'[data-swiper-slide-index="'+o.attr("data-swiper-slide-index")+'"]').addClass(i.slideDuplicateNextClass),l.hasClass(i.slideDuplicateClass)?s.children("."+i.slideClass+":not(."+i.slideDuplicateClass+')[data-swiper-slide-index="'+l.attr("data-swiper-slide-index")+'"]').addClass(i.slideDuplicatePrevClass):s.children("."+i.slideClass+"."+i.slideDuplicateClass+'[data-swiper-slide-index="'+l.attr("data-swiper-slide-index")+'"]').addClass(i.slideDuplicatePrevClass))},updateActiveIndex:function(e){var t,i=this.rtlTranslate?this.translate:-this.translate,s=this.slidesGrid,a=this.snapGrid,r=this.params,n=this.activeIndex,o=this.realIndex,l=this.snapIndex,h=e;if(void 0===h){for(var p=0;p<s.length;p+=1)void 0!==s[p+1]?i>=s[p]&&i<s[p+1]-(s[p+1]-s[p])/2?h=p:i>=s[p]&&i<s[p+1]&&(h=p+1):i>=s[p]&&(h=p);r.normalizeSlideIndex&&(h<0||void 0===h)&&(h=0)}if((t=a.indexOf(i)>=0?a.indexOf(i):Math.floor(h/r.slidesPerGroup))>=a.length&&(t=a.length-1),h!==n){var c=parseInt(this.slides.eq(h).attr("data-swiper-slide-index")||h,10);d.extend(this,{snapIndex:t,realIndex:c,previousIndex:n,activeIndex:h}),this.emit("activeIndexChange"),this.emit("snapIndexChange"),o!==c&&this.emit("realIndexChange"),this.emit("slideChange")}else t!==l&&(this.snapIndex=t,this.emit("snapIndexChange"))},updateClickedSlide:function(e){var t=this.params,i=s(e.target).closest("."+t.slideClass)[0],a=!1;if(i)for(var r=0;r<this.slides.length;r+=1)this.slides[r]===i&&(a=!0);if(!i||!a)return this.clickedSlide=void 0,void(this.clickedIndex=void 0);this.clickedSlide=i,this.virtual&&this.params.virtual.enabled?this.clickedIndex=parseInt(s(i).attr("data-swiper-slide-index"),10):this.clickedIndex=s(i).index(),t.slideToClickedSlide&&void 0!==this.clickedIndex&&this.clickedIndex!==this.activeIndex&&this.slideToClickedSlide()}};var v={getTranslate:function(e){void 0===e&&(e=this.isHorizontal()?"x":"y");var t=this.params,i=this.rtlTranslate,s=this.translate,a=this.$wrapperEl;if(t.virtualTranslate)return i?-s:s;var r=d.getTranslate(a[0],e);return i&&(r=-r),r||0},setTranslate:function(e,t){var i=this.rtlTranslate,s=this.params,a=this.$wrapperEl,r=this.progress,n=0,o=0;this.isHorizontal()?n=i?-e:e:o=e,s.roundLengths&&(n=Math.floor(n),o=Math.floor(o)),s.virtualTranslate||(h.transforms3d?a.transform("translate3d("+n+"px, "+o+"px, 0px)"):a.transform("translate("+n+"px, "+o+"px)")),this.previousTranslate=this.translate,this.translate=this.isHorizontal()?n:o;var l=this.maxTranslate()-this.minTranslate();(0===l?0:(e-this.minTranslate())/l)!==r&&this.updateProgress(e),this.emit("setTranslate",this.translate,t)},minTranslate:function(){return-this.snapGrid[0]},maxTranslate:function(){return-this.snapGrid[this.snapGrid.length-1]}};var f={setTransition:function(e,t){this.$wrapperEl.transition(e),this.emit("setTransition",e,t)},transitionStart:function(e,t){void 0===e&&(e=!0);var i=this.activeIndex,s=this.params,a=this.previousIndex;s.autoHeight&&this.updateAutoHeight();var r=t;if(r||(r=i>a?"next":i<a?"prev":"reset"),this.emit("transitionStart"),e&&i!==a){if("reset"===r)return void this.emit("slideResetTransitionStart");this.emit("slideChangeTransitionStart"),"next"===r?this.emit("slideNextTransitionStart"):this.emit("slidePrevTransitionStart")}},transitionEnd:function(e,t){void 0===e&&(e=!0);var i=this.activeIndex,s=this.previousIndex;this.animating=!1,this.setTransition(0);var a=t;if(a||(a=i>s?"next":i<s?"prev":"reset"),this.emit("transitionEnd"),e&&i!==s){if("reset"===a)return void this.emit("slideResetTransitionEnd");this.emit("slideChangeTransitionEnd"),"next"===a?this.emit("slideNextTransitionEnd"):this.emit("slidePrevTransitionEnd")}}};var m={slideTo:function(e,t,i,s){void 0===e&&(e=0),void 0===t&&(t=this.params.speed),void 0===i&&(i=!0);var a=this,r=e;r<0&&(r=0);var n=a.params,o=a.snapGrid,l=a.slidesGrid,d=a.previousIndex,p=a.activeIndex,c=a.rtlTranslate;if(a.animating&&n.preventIntercationOnTransition)return!1;var u=Math.floor(r/n.slidesPerGroup);u>=o.length&&(u=o.length-1),(p||n.initialSlide||0)===(d||0)&&i&&a.emit("beforeSlideChangeStart");var v,f=-o[u];if(a.updateProgress(f),n.normalizeSlideIndex)for(var m=0;m<l.length;m+=1)-Math.floor(100*f)>=Math.floor(100*l[m])&&(r=m);if(a.initialized&&r!==p){if(!a.allowSlideNext&&f<a.translate&&f<a.minTranslate())return!1;if(!a.allowSlidePrev&&f>a.translate&&f>a.maxTranslate()&&(p||0)!==r)return!1}return v=r>p?"next":r<p?"prev":"reset",c&&-f===a.translate||!c&&f===a.translate?(a.updateActiveIndex(r),n.autoHeight&&a.updateAutoHeight(),a.updateSlidesClasses(),"slide"!==n.effect&&a.setTranslate(f),"reset"!==v&&(a.transitionStart(i,v),a.transitionEnd(i,v)),!1):(0!==t&&h.transition?(a.setTransition(t),a.setTranslate(f),a.updateActiveIndex(r),a.updateSlidesClasses(),a.emit("beforeTransitionStart",t,s),a.transitionStart(i,v),a.animating||(a.animating=!0,a.onSlideToWrapperTransitionEnd||(a.onSlideToWrapperTransitionEnd=function(e){a&&!a.destroyed&&e.target===this&&(a.$wrapperEl[0].removeEventListener("transitionend",a.onSlideToWrapperTransitionEnd),a.$wrapperEl[0].removeEventListener("webkitTransitionEnd",a.onSlideToWrapperTransitionEnd),a.transitionEnd(i,v))}),a.$wrapperEl[0].addEventListener("transitionend",a.onSlideToWrapperTransitionEnd),a.$wrapperEl[0].addEventListener("webkitTransitionEnd",a.onSlideToWrapperTransitionEnd))):(a.setTransition(0),a.setTranslate(f),a.updateActiveIndex(r),a.updateSlidesClasses(),a.emit("beforeTransitionStart",t,s),a.transitionStart(i,v),a.transitionEnd(i,v)),!0)},slideToLoop:function(e,t,i,s){void 0===e&&(e=0),void 0===t&&(t=this.params.speed),void 0===i&&(i=!0);var a=e;return this.params.loop&&(a+=this.loopedSlides),this.slideTo(a,t,i,s)},slideNext:function(e,t,i){void 0===e&&(e=this.params.speed),void 0===t&&(t=!0);var s=this.params,a=this.animating;return s.loop?!a&&(this.loopFix(),this._clientLeft=this.$wrapperEl[0].clientLeft,this.slideTo(this.activeIndex+s.slidesPerGroup,e,t,i)):this.slideTo(this.activeIndex+s.slidesPerGroup,e,t,i)},slidePrev:function(e,t,i){void 0===e&&(e=this.params.speed),void 0===t&&(t=!0);var s=this.params,a=this.animating,r=this.snapGrid,n=this.slidesGrid,o=this.rtlTranslate;if(s.loop){if(a)return!1;this.loopFix(),this._clientLeft=this.$wrapperEl[0].clientLeft}function l(e){return e<0?-Math.floor(Math.abs(e)):Math.floor(e)}var d,h=l(o?this.translate:-this.translate),p=r.map(function(e){return l(e)}),c=(n.map(function(e){return l(e)}),r[p.indexOf(h)],r[p.indexOf(h)-1]);return void 0!==c&&(d=n.indexOf(c))<0&&(d=this.activeIndex-1),this.slideTo(d,e,t,i)},slideReset:function(e,t,i){return void 0===e&&(e=this.params.speed),void 0===t&&(t=!0),this.slideTo(this.activeIndex,e,t,i)},slideToClosest:function(e,t,i){void 0===e&&(e=this.params.speed),void 0===t&&(t=!0);var s=this.activeIndex,a=Math.floor(s/this.params.slidesPerGroup);if(a<this.snapGrid.length-1){var r=this.rtlTranslate?this.translate:-this.translate,n=this.snapGrid[a];r-n>(this.snapGrid[a+1]-n)/2&&(s=this.params.slidesPerGroup)}return this.slideTo(s,e,t,i)},slideToClickedSlide:function(){var e,t=this,i=t.params,a=t.$wrapperEl,r="auto"===i.slidesPerView?t.slidesPerViewDynamic():i.slidesPerView,n=t.clickedIndex;if(i.loop){if(t.animating)return;e=parseInt(s(t.clickedSlide).attr("data-swiper-slide-index"),10),i.centeredSlides?n<t.loopedSlides-r/2||n>t.slides.length-t.loopedSlides+r/2?(t.loopFix(),n=a.children("."+i.slideClass+'[data-swiper-slide-index="'+e+'"]:not(.'+i.slideDuplicateClass+")").eq(0).index(),d.nextTick(function(){t.slideTo(n)})):t.slideTo(n):n>t.slides.length-r?(t.loopFix(),n=a.children("."+i.slideClass+'[data-swiper-slide-index="'+e+'"]:not(.'+i.slideDuplicateClass+")").eq(0).index(),d.nextTick(function(){t.slideTo(n)})):t.slideTo(n)}else t.slideTo(n)}};var g={loopCreate:function(){var t=this,i=t.params,a=t.$wrapperEl;a.children("."+i.slideClass+"."+i.slideDuplicateClass).remove();var r=a.children("."+i.slideClass);if(i.loopFillGroupWithBlank){var n=i.slidesPerGroup-r.length%i.slidesPerGroup;if(n!==i.slidesPerGroup){for(var o=0;o<n;o+=1){var l=s(e.createElement("div")).addClass(i.slideClass+" "+i.slideBlankClass);a.append(l)}r=a.children("."+i.slideClass)}}"auto"!==i.slidesPerView||i.loopedSlides||(i.loopedSlides=r.length),t.loopedSlides=parseInt(i.loopedSlides||i.slidesPerView,10),t.loopedSlides+=i.loopAdditionalSlides,t.loopedSlides>r.length&&(t.loopedSlides=r.length);var d=[],h=[];r.each(function(e,i){var a=s(i);e<t.loopedSlides&&h.push(i),e<r.length&&e>=r.length-t.loopedSlides&&d.push(i),a.attr("data-swiper-slide-index",e)});for(var p=0;p<h.length;p+=1)a.append(s(h[p].cloneNode(!0)).addClass(i.slideDuplicateClass));for(var c=d.length-1;c>=0;c-=1)a.prepend(s(d[c].cloneNode(!0)).addClass(i.slideDuplicateClass))},loopFix:function(){var e,t=this.params,i=this.activeIndex,s=this.slides,a=this.loopedSlides,r=this.allowSlidePrev,n=this.allowSlideNext,o=this.snapGrid,l=this.rtlTranslate;this.allowSlidePrev=!0,this.allowSlideNext=!0;var d=-o[i]-this.getTranslate();i<a?(e=s.length-3*a+i,e+=a,this.slideTo(e,0,!1,!0)&&0!==d&&this.setTranslate((l?-this.translate:this.translate)-d)):("auto"===t.slidesPerView&&i>=2*a||i>=s.length-a)&&(e=-s.length+i+a,e+=a,this.slideTo(e,0,!1,!0)&&0!==d&&this.setTranslate((l?-this.translate:this.translate)-d));this.allowSlidePrev=r,this.allowSlideNext=n},loopDestroy:function(){var e=this.$wrapperEl,t=this.params,i=this.slides;e.children("."+t.slideClass+"."+t.slideDuplicateClass).remove(),i.removeAttr("data-swiper-slide-index")}};var b={setGrabCursor:function(e){if(!(h.touch||!this.params.simulateTouch||this.params.watchOverflow&&this.isLocked)){var t=this.el;t.style.cursor="move",t.style.cursor=e?"-webkit-grabbing":"-webkit-grab",t.style.cursor=e?"-moz-grabbin":"-moz-grab",t.style.cursor=e?"grabbing":"grab"}},unsetGrabCursor:function(){h.touch||this.params.watchOverflow&&this.isLocked||(this.el.style.cursor="")}};var w={appendSlide:function(e){var t=this.$wrapperEl,i=this.params;if(i.loop&&this.loopDestroy(),"object"==typeof e&&"length"in e)for(var s=0;s<e.length;s+=1)e[s]&&t.append(e[s]);else t.append(e);i.loop&&this.loopCreate(),i.observer&&h.observer||this.update()},prependSlide:function(e){var t=this.params,i=this.$wrapperEl,s=this.activeIndex;t.loop&&this.loopDestroy();var a=s+1;if("object"==typeof e&&"length"in e){for(var r=0;r<e.length;r+=1)e[r]&&i.prepend(e[r]);a=s+e.length}else i.prepend(e);t.loop&&this.loopCreate(),t.observer&&h.observer||this.update(),this.slideTo(a,0,!1)},addSlide:function(e,t){var i=this.$wrapperEl,s=this.params,a=this.activeIndex;s.loop&&(a-=this.loopedSlides,this.loopDestroy(),this.slides=i.children("."+s.slideClass));var r=this.slides.length;if(e<=0)this.prependSlide(t);else if(e>=r)this.appendSlide(t);else{for(var n=a>e?a+1:a,o=[],l=r-1;l>=e;l-=1){var d=this.slides.eq(l);d.remove(),o.unshift(d)}if("object"==typeof t&&"length"in t){for(var p=0;p<t.length;p+=1)t[p]&&i.append(t[p]);n=a>e?a+t.length:a}else i.append(t);for(var c=0;c<o.length;c+=1)i.append(o[c]);s.loop&&this.loopCreate(),s.observer&&h.observer||this.update(),s.loop?this.slideTo(n+this.loopedSlides,0,!1):this.slideTo(n,0,!1)}},removeSlide:function(e){var t=this.params,i=this.$wrapperEl,s=this.activeIndex;t.loop&&(s-=this.loopedSlides,this.loopDestroy(),this.slides=i.children("."+t.slideClass));var a,r=s;if("object"==typeof e&&"length"in e){for(var n=0;n<e.length;n+=1)a=e[n],this.slides[a]&&this.slides.eq(a).remove(),a<r&&(r-=1);r=Math.max(r,0)}else a=e,this.slides[a]&&this.slides.eq(a).remove(),a<r&&(r-=1),r=Math.max(r,0);t.loop&&this.loopCreate(),t.observer&&h.observer||this.update(),t.loop?this.slideTo(r+this.loopedSlides,0,!1):this.slideTo(r,0,!1)},removeAllSlides:function(){for(var e=[],t=0;t<this.slides.length;t+=1)e.push(t);this.removeSlide(e)}},y=function(){var i=t.navigator.userAgent,s={ios:!1,android:!1,androidChrome:!1,desktop:!1,windows:!1,iphone:!1,ipod:!1,ipad:!1,cordova:t.cordova||t.phonegap,phonegap:t.cordova||t.phonegap},a=i.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),r=i.match(/(Android);?[\s\/]+([\d.]+)?/),n=i.match(/(iPad).*OS\s([\d_]+)/),o=i.match(/(iPod)(.*OS\s([\d_]+))?/),l=!n&&i.match(/(iPhone\sOS|iOS)\s([\d_]+)/);if(a&&(s.os="windows",s.osVersion=a[2],s.windows=!0),r&&!a&&(s.os="android",s.osVersion=r[2],s.android=!0,s.androidChrome=i.toLowerCase().indexOf("chrome")>=0),(n||l||o)&&(s.os="ios",s.ios=!0),l&&!o&&(s.osVersion=l[2].replace(/_/g,"."),s.iphone=!0),n&&(s.osVersion=n[2].replace(/_/g,"."),s.ipad=!0),o&&(s.osVersion=o[3]?o[3].replace(/_/g,"."):null,s.iphone=!0),s.ios&&s.osVersion&&i.indexOf("Version/")>=0&&"10"===s.osVersion.split(".")[0]&&(s.osVersion=i.toLowerCase().split("version/")[1].split(" ")[0]),s.desktop=!(s.os||s.android||s.webView),s.webView=(l||n||o)&&i.match(/.*AppleWebKit(?!.*Safari)/i),s.os&&"ios"===s.os){var d=s.osVersion.split("."),h=e.querySelector('meta[name="viewport"]');s.minimalUi=!s.webView&&(o||l)&&(1*d[0]==7?1*d[1]>=1:1*d[0]>7)&&h&&h.getAttribute("content").indexOf("minimal-ui")>=0}return s.pixelRatio=t.devicePixelRatio||1,s}();function x(){var e=this.params,t=this.el;if(!t||0!==t.offsetWidth){e.breakpoints&&this.setBreakpoint();var i=this.allowSlideNext,s=this.allowSlidePrev,a=this.snapGrid;if(this.allowSlideNext=!0,this.allowSlidePrev=!0,this.updateSize(),this.updateSlides(),e.freeMode){var r=Math.min(Math.max(this.translate,this.maxTranslate()),this.minTranslate());this.setTranslate(r),this.updateActiveIndex(),this.updateSlidesClasses(),e.autoHeight&&this.updateAutoHeight()}else this.updateSlidesClasses(),("auto"===e.slidesPerView||e.slidesPerView>1)&&this.isEnd&&!this.params.centeredSlides?this.slideTo(this.slides.length-1,0,!1,!0):this.slideTo(this.activeIndex,0,!1,!0);this.allowSlidePrev=s,this.allowSlideNext=i,this.params.watchOverflow&&a!==this.snapGrid&&this.checkOverflow()}}var E={attachEvents:function(){var i=this.params,a=this.touchEvents,r=this.el,n=this.wrapperEl;this.onTouchStart=function(i){var a=this.touchEventsData,r=this.params,n=this.touches;if(!this.animating||!r.preventIntercationOnTransition){var o=i;if(o.originalEvent&&(o=o.originalEvent),a.isTouchEvent="touchstart"===o.type,(a.isTouchEvent||!("which"in o)||3!==o.which)&&(!a.isTouched||!a.isMoved))if(r.noSwiping&&s(o.target).closest(r.noSwipingSelector?r.noSwipingSelector:"."+r.noSwipingClass)[0])this.allowClick=!0;else if(!r.swipeHandler||s(o).closest(r.swipeHandler)[0]){n.currentX="touchstart"===o.type?o.targetTouches[0].pageX:o.pageX,n.currentY="touchstart"===o.type?o.targetTouches[0].pageY:o.pageY;var l=n.currentX,h=n.currentY;if(!y.ios||y.cordova||!r.iOSEdgeSwipeDetection||!(l<=r.iOSEdgeSwipeThreshold||l>=t.screen.width-r.iOSEdgeSwipeThreshold)){if(d.extend(a,{isTouched:!0,isMoved:!1,allowTouchCallbacks:!0,isScrolling:void 0,startMoving:void 0}),n.startX=l,n.startY=h,a.touchStartTime=d.now(),this.allowClick=!0,this.updateSize(),this.swipeDirection=void 0,r.threshold>0&&(a.allowThresholdMove=!1),"touchstart"!==o.type){var p=!0;s(o.target).is(a.formElements)&&(p=!1),e.activeElement&&s(e.activeElement).is(a.formElements)&&e.activeElement!==o.target&&e.activeElement.blur(),p&&this.allowTouchMove&&o.preventDefault()}this.emit("touchStart",o)}}}}.bind(this),this.onTouchMove=function(t){var i=this.touchEventsData,a=this.params,r=this.touches,n=this.rtlTranslate,o=t;if(o.originalEvent&&(o=o.originalEvent),i.isTouched){if(!i.isTouchEvent||"mousemove"!==o.type){var l="touchmove"===o.type?o.targetTouches[0].pageX:o.pageX,h="touchmove"===o.type?o.targetTouches[0].pageY:o.pageY;if(o.preventedByNestedSwiper)return r.startX=l,void(r.startY=h);if(!this.allowTouchMove)return this.allowClick=!1,void(i.isTouched&&(d.extend(r,{startX:l,startY:h,currentX:l,currentY:h}),i.touchStartTime=d.now()));if(i.isTouchEvent&&a.touchReleaseOnEdges&&!a.loop)if(this.isVertical()){if(h<r.startY&&this.translate<=this.maxTranslate()||h>r.startY&&this.translate>=this.minTranslate())return i.isTouched=!1,void(i.isMoved=!1)}else if(l<r.startX&&this.translate<=this.maxTranslate()||l>r.startX&&this.translate>=this.minTranslate())return;if(i.isTouchEvent&&e.activeElement&&o.target===e.activeElement&&s(o.target).is(i.formElements))return i.isMoved=!0,void(this.allowClick=!1);if(i.allowTouchCallbacks&&this.emit("touchMove",o),!(o.targetTouches&&o.targetTouches.length>1)){r.currentX=l,r.currentY=h;var p,c=r.currentX-r.startX,u=r.currentY-r.startY;if(void 0===i.isScrolling&&(this.isHorizontal()&&r.currentY===r.startY||this.isVertical()&&r.currentX===r.startX?i.isScrolling=!1:c*c+u*u>=25&&(p=180*Math.atan2(Math.abs(u),Math.abs(c))/Math.PI,i.isScrolling=this.isHorizontal()?p>a.touchAngle:90-p>a.touchAngle)),i.isScrolling&&this.emit("touchMoveOpposite",o),"undefined"==typeof startMoving&&(r.currentX===r.startX&&r.currentY===r.startY||(i.startMoving=!0)),i.isScrolling)i.isTouched=!1;else if(i.startMoving){this.allowClick=!1,o.preventDefault(),a.touchMoveStopPropagation&&!a.nested&&o.stopPropagation(),i.isMoved||(a.loop&&this.loopFix(),i.startTranslate=this.getTranslate(),this.setTransition(0),this.animating&&this.$wrapperEl.trigger("webkitTransitionEnd transitionend"),i.allowMomentumBounce=!1,!a.grabCursor||!0!==this.allowSlideNext&&!0!==this.allowSlidePrev||this.setGrabCursor(!0),this.emit("sliderFirstMove",o)),this.emit("sliderMove",o),i.isMoved=!0;var v=this.isHorizontal()?c:u;r.diff=v,v*=a.touchRatio,n&&(v=-v),this.swipeDirection=v>0?"prev":"next",i.currentTranslate=v+i.startTranslate;var f=!0,m=a.resistanceRatio;if(a.touchReleaseOnEdges&&(m=0),v>0&&i.currentTranslate>this.minTranslate()?(f=!1,a.resistance&&(i.currentTranslate=this.minTranslate()-1+Math.pow(-this.minTranslate()+i.startTranslate+v,m))):v<0&&i.currentTranslate<this.maxTranslate()&&(f=!1,a.resistance&&(i.currentTranslate=this.maxTranslate()+1-Math.pow(this.maxTranslate()-i.startTranslate-v,m))),f&&(o.preventedByNestedSwiper=!0),!this.allowSlideNext&&"next"===this.swipeDirection&&i.currentTranslate<i.startTranslate&&(i.currentTranslate=i.startTranslate),!this.allowSlidePrev&&"prev"===this.swipeDirection&&i.currentTranslate>i.startTranslate&&(i.currentTranslate=i.startTranslate),a.threshold>0){if(!(Math.abs(v)>a.threshold||i.allowThresholdMove))return void(i.currentTranslate=i.startTranslate);if(!i.allowThresholdMove)return i.allowThresholdMove=!0,r.startX=r.currentX,r.startY=r.currentY,i.currentTranslate=i.startTranslate,void(r.diff=this.isHorizontal()?r.currentX-r.startX:r.currentY-r.startY)}a.followFinger&&((a.freeMode||a.watchSlidesProgress||a.watchSlidesVisibility)&&(this.updateActiveIndex(),this.updateSlidesClasses()),a.freeMode&&(0===i.velocities.length&&i.velocities.push({position:r[this.isHorizontal()?"startX":"startY"],time:i.touchStartTime}),i.velocities.push({position:r[this.isHorizontal()?"currentX":"currentY"],time:d.now()})),this.updateProgress(i.currentTranslate),this.setTranslate(i.currentTranslate))}}}}else i.startMoving&&i.isScrolling&&this.emit("touchMoveOpposite",o)}.bind(this),this.onTouchEnd=function(e){var t=this,i=t.touchEventsData,s=t.params,a=t.touches,r=t.rtlTranslate,n=t.$wrapperEl,o=t.slidesGrid,l=t.snapGrid,h=e;if(h.originalEvent&&(h=h.originalEvent),i.allowTouchCallbacks&&t.emit("touchEnd",h),i.allowTouchCallbacks=!1,!i.isTouched)return i.isMoved&&s.grabCursor&&t.setGrabCursor(!1),i.isMoved=!1,void(i.startMoving=!1);s.grabCursor&&i.isMoved&&i.isTouched&&(!0===t.allowSlideNext||!0===t.allowSlidePrev)&&t.setGrabCursor(!1);var p,c=d.now(),u=c-i.touchStartTime;if(t.allowClick&&(t.updateClickedSlide(h),t.emit("tap",h),u<300&&c-i.lastClickTime>300&&(i.clickTimeout&&clearTimeout(i.clickTimeout),i.clickTimeout=d.nextTick(function(){t&&!t.destroyed&&t.emit("click",h)},300)),u<300&&c-i.lastClickTime<300&&(i.clickTimeout&&clearTimeout(i.clickTimeout),t.emit("doubleTap",h))),i.lastClickTime=d.now(),d.nextTick(function(){t.destroyed||(t.allowClick=!0)}),!i.isTouched||!i.isMoved||!t.swipeDirection||0===a.diff||i.currentTranslate===i.startTranslate)return i.isTouched=!1,i.isMoved=!1,void(i.startMoving=!1);if(i.isTouched=!1,i.isMoved=!1,i.startMoving=!1,p=s.followFinger?r?t.translate:-t.translate:-i.currentTranslate,s.freeMode){if(p<-t.minTranslate())return void t.slideTo(t.activeIndex);if(p>-t.maxTranslate())return void(t.slides.length<l.length?t.slideTo(l.length-1):t.slideTo(t.slides.length-1));if(s.freeModeMomentum){if(i.velocities.length>1){var v=i.velocities.pop(),f=i.velocities.pop(),m=v.position-f.position,g=v.time-f.time;t.velocity=m/g,t.velocity/=2,Math.abs(t.velocity)<s.freeModeMinimumVelocity&&(t.velocity=0),(g>150||d.now()-v.time>300)&&(t.velocity=0)}else t.velocity=0;t.velocity*=s.freeModeMomentumVelocityRatio,i.velocities.length=0;var b=1e3*s.freeModeMomentumRatio,w=t.velocity*b,y=t.translate+w;r&&(y=-y);var x,E,T=!1,S=20*Math.abs(t.velocity)*s.freeModeMomentumBounceRatio;if(y<t.maxTranslate())s.freeModeMomentumBounce?(y+t.maxTranslate()<-S&&(y=t.maxTranslate()-S),x=t.maxTranslate(),T=!0,i.allowMomentumBounce=!0):y=t.maxTranslate(),s.loop&&s.centeredSlides&&(E=!0);else if(y>t.minTranslate())s.freeModeMomentumBounce?(y-t.minTranslate()>S&&(y=t.minTranslate()+S),x=t.minTranslate(),T=!0,i.allowMomentumBounce=!0):y=t.minTranslate(),s.loop&&s.centeredSlides&&(E=!0);else if(s.freeModeSticky){for(var C,M=0;M<l.length;M+=1)if(l[M]>-y){C=M;break}y=-(y=Math.abs(l[C]-y)<Math.abs(l[C-1]-y)||"next"===t.swipeDirection?l[C]:l[C-1])}if(E&&t.once("transitionEnd",function(){t.loopFix()}),0!==t.velocity)b=r?Math.abs((-y-t.translate)/t.velocity):Math.abs((y-t.translate)/t.velocity);else if(s.freeModeSticky)return void t.slideToClosest();s.freeModeMomentumBounce&&T?(t.updateProgress(x),t.setTransition(b),t.setTranslate(y),t.transitionStart(!0,t.swipeDirection),t.animating=!0,n.transitionEnd(function(){t&&!t.destroyed&&i.allowMomentumBounce&&(t.emit("momentumBounce"),t.setTransition(s.speed),t.setTranslate(x),n.transitionEnd(function(){t&&!t.destroyed&&t.transitionEnd()}))})):t.velocity?(t.updateProgress(y),t.setTransition(b),t.setTranslate(y),t.transitionStart(!0,t.swipeDirection),t.animating||(t.animating=!0,n.transitionEnd(function(){t&&!t.destroyed&&t.transitionEnd()}))):t.updateProgress(y),t.updateActiveIndex(),t.updateSlidesClasses()}else if(s.freeModeSticky)return void t.slideToClosest();(!s.freeModeMomentum||u>=s.longSwipesMs)&&(t.updateProgress(),t.updateActiveIndex(),t.updateSlidesClasses())}else{for(var z=0,k=t.slidesSizesGrid[0],P=0;P<o.length;P+=s.slidesPerGroup)void 0!==o[P+s.slidesPerGroup]?p>=o[P]&&p<o[P+s.slidesPerGroup]&&(z=P,k=o[P+s.slidesPerGroup]-o[P]):p>=o[P]&&(z=P,k=o[o.length-1]-o[o.length-2]);var $=(p-o[z])/k;if(u>s.longSwipesMs){if(!s.longSwipes)return void t.slideTo(t.activeIndex);"next"===t.swipeDirection&&($>=s.longSwipesRatio?t.slideTo(z+s.slidesPerGroup):t.slideTo(z)),"prev"===t.swipeDirection&&($>1-s.longSwipesRatio?t.slideTo(z+s.slidesPerGroup):t.slideTo(z))}else{if(!s.shortSwipes)return void t.slideTo(t.activeIndex);"next"===t.swipeDirection&&t.slideTo(z+s.slidesPerGroup),"prev"===t.swipeDirection&&t.slideTo(z)}}}.bind(this),this.onClick=function(e){this.allowClick||(this.params.preventClicks&&e.preventDefault(),this.params.preventClicksPropagation&&this.animating&&(e.stopPropagation(),e.stopImmediatePropagation()))}.bind(this);var o="container"===i.touchEventsTarget?r:n,l=!!i.nested;if(h.touch||!h.pointerEvents&&!h.prefixedPointerEvents){if(h.touch){var p=!("touchstart"!==a.start||!h.passiveListener||!i.passiveListeners)&&{passive:!0,capture:!1};o.addEventListener(a.start,this.onTouchStart,p),o.addEventListener(a.move,this.onTouchMove,h.passiveListener?{passive:!1,capture:l}:l),o.addEventListener(a.end,this.onTouchEnd,p)}(i.simulateTouch&&!y.ios&&!y.android||i.simulateTouch&&!h.touch&&y.ios)&&(o.addEventListener("mousedown",this.onTouchStart,!1),e.addEventListener("mousemove",this.onTouchMove,l),e.addEventListener("mouseup",this.onTouchEnd,!1))}else o.addEventListener(a.start,this.onTouchStart,!1),e.addEventListener(a.move,this.onTouchMove,l),e.addEventListener(a.end,this.onTouchEnd,!1);(i.preventClicks||i.preventClicksPropagation)&&o.addEventListener("click",this.onClick,!0),this.on(y.ios||y.android?"resize orientationchange observerUpdate":"resize observerUpdate",x,!0)},detachEvents:function(){var t=this.params,i=this.touchEvents,s=this.el,a=this.wrapperEl,r="container"===t.touchEventsTarget?s:a,n=!!t.nested;if(h.touch||!h.pointerEvents&&!h.prefixedPointerEvents){if(h.touch){var o=!("onTouchStart"!==i.start||!h.passiveListener||!t.passiveListeners)&&{passive:!0,capture:!1};r.removeEventListener(i.start,this.onTouchStart,o),r.removeEventListener(i.move,this.onTouchMove,n),r.removeEventListener(i.end,this.onTouchEnd,o)}(t.simulateTouch&&!y.ios&&!y.android||t.simulateTouch&&!h.touch&&y.ios)&&(r.removeEventListener("mousedown",this.onTouchStart,!1),e.removeEventListener("mousemove",this.onTouchMove,n),e.removeEventListener("mouseup",this.onTouchEnd,!1))}else r.removeEventListener(i.start,this.onTouchStart,!1),e.removeEventListener(i.move,this.onTouchMove,n),e.removeEventListener(i.end,this.onTouchEnd,!1);(t.preventClicks||t.preventClicksPropagation)&&r.removeEventListener("click",this.onClick,!0),this.off(y.ios||y.android?"resize orientationchange observerUpdate":"resize observerUpdate",x)}};var T={setBreakpoint:function(){var e=this.activeIndex,t=this.initialized,i=this.loopedSlides;void 0===i&&(i=0);var s=this.params,a=s.breakpoints;if(a&&(!a||0!==Object.keys(a).length)){var r=this.getBreakpoint(a);if(r&&this.currentBreakpoint!==r){var n=r in a?a[r]:this.originalParams,o=s.loop&&n.slidesPerView!==s.slidesPerView;d.extend(this.params,n),d.extend(this,{allowTouchMove:this.params.allowTouchMove,allowSlideNext:this.params.allowSlideNext,allowSlidePrev:this.params.allowSlidePrev}),this.currentBreakpoint=r,o&&t&&(this.loopDestroy(),this.loopCreate(),this.updateSlides(),this.slideTo(e-i+this.loopedSlides,0,!1)),this.emit("breakpoint",n)}}},getBreakpoint:function(e){if(e){var i=!1,s=[];Object.keys(e).forEach(function(e){s.push(e)}),s.sort(function(e,t){return parseInt(e,10)-parseInt(t,10)});for(var a=0;a<s.length;a+=1){var r=s[a];r>=t.innerWidth&&!i&&(i=r)}return i||"max"}}},S=function(){return{isIE:!!t.navigator.userAgent.match(/Trident/g)||!!t.navigator.userAgent.match(/MSIE/g),isSafari:(e=t.navigator.userAgent.toLowerCase(),e.indexOf("safari")>=0&&e.indexOf("chrome")<0&&e.indexOf("android")<0),isUiWebView:/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(t.navigator.userAgent)};var e}();var C={init:!0,direction:"horizontal",touchEventsTarget:"container",initialSlide:0,speed:300,preventIntercationOnTransition:!1,iOSEdgeSwipeDetection:!1,iOSEdgeSwipeThreshold:20,freeMode:!1,freeModeMomentum:!0,freeModeMomentumRatio:1,freeModeMomentumBounce:!0,freeModeMomentumBounceRatio:1,freeModeMomentumVelocityRatio:1,freeModeSticky:!1,freeModeMinimumVelocity:.02,autoHeight:!1,setWrapperSize:!1,virtualTranslate:!1,effect:"slide",breakpoints:void 0,spaceBetween:0,slidesPerView:1,slidesPerColumn:1,slidesPerColumnFill:"column",slidesPerGroup:1,centeredSlides:!1,slidesOffsetBefore:0,slidesOffsetAfter:0,normalizeSlideIndex:!0,watchOverflow:!1,roundLengths:!1,touchRatio:1,touchAngle:45,simulateTouch:!0,shortSwipes:!0,longSwipes:!0,longSwipesRatio:.5,longSwipesMs:300,followFinger:!0,allowTouchMove:!0,threshold:0,touchMoveStopPropagation:!0,touchReleaseOnEdges:!1,uniqueNavElements:!0,resistance:!0,resistanceRatio:.85,watchSlidesProgress:!1,watchSlidesVisibility:!1,grabCursor:!1,preventClicks:!0,preventClicksPropagation:!0,slideToClickedSlide:!1,preloadImages:!0,updateOnImagesReady:!0,loop:!1,loopAdditionalSlides:0,loopedSlides:null,loopFillGroupWithBlank:!1,allowSlidePrev:!0,allowSlideNext:!0,swipeHandler:null,noSwiping:!0,noSwipingClass:"swiper-no-swiping",noSwipingSelector:null,passiveListeners:!0,containerModifierClass:"swiper-container-",slideClass:"swiper-slide",slideBlankClass:"swiper-slide-invisible-blank",slideActiveClass:"swiper-slide-active",slideDuplicateActiveClass:"swiper-slide-duplicate-active",slideVisibleClass:"swiper-slide-visible",slideDuplicateClass:"swiper-slide-duplicate",slideNextClass:"swiper-slide-next",slideDuplicateNextClass:"swiper-slide-duplicate-next",slidePrevClass:"swiper-slide-prev",slideDuplicatePrevClass:"swiper-slide-duplicate-prev",wrapperClass:"swiper-wrapper",runCallbacksOnInit:!0},M={update:u,translate:v,transition:f,slide:m,loop:g,grabCursor:b,manipulation:w,events:E,breakpoints:T,checkOverflow:{checkOverflow:function(){var e=this.isLocked;this.isLocked=1===this.snapGrid.length,this.allowSlideNext=!this.isLocked,this.allowSlidePrev=!this.isLocked,e!==this.isLocked&&this.emit(this.isLocked?"lock":"unlock"),e&&e!==this.isLocked&&(this.isEnd=!1,this.navigation.update())}},classes:{addClasses:function(){var e=this.classNames,t=this.params,i=this.rtl,s=this.$el,a=[];a.push(t.direction),t.freeMode&&a.push("free-mode"),h.flexbox||a.push("no-flexbox"),t.autoHeight&&a.push("autoheight"),i&&a.push("rtl"),t.slidesPerColumn>1&&a.push("multirow"),y.android&&a.push("android"),y.ios&&a.push("ios"),S.isIE&&(h.pointerEvents||h.prefixedPointerEvents)&&a.push("wp8-"+t.direction),a.forEach(function(i){e.push(t.containerModifierClass+i)}),s.addClass(e.join(" "))},removeClasses:function(){var e=this.$el,t=this.classNames;e.removeClass(t.join(" "))}},images:{loadImage:function(e,i,s,a,r,n){var o;function l(){n&&n()}e.complete&&r?l():i?((o=new t.Image).onload=l,o.onerror=l,a&&(o.sizes=a),s&&(o.srcset=s),i&&(o.src=i)):l()},preloadImages:function(){var e=this;function t(){void 0!==e&&null!==e&&e&&!e.destroyed&&(void 0!==e.imagesLoaded&&(e.imagesLoaded+=1),e.imagesLoaded===e.imagesToLoad.length&&(e.params.updateOnImagesReady&&e.update(),e.emit("imagesReady")))}e.imagesToLoad=e.$el.find("img");for(var i=0;i<e.imagesToLoad.length;i+=1){var s=e.imagesToLoad[i];e.loadImage(s,s.currentSrc||s.getAttribute("src"),s.srcset||s.getAttribute("srcset"),s.sizes||s.getAttribute("sizes"),!0,t)}}}},z={},k=function(e){function t(){for(var i,a,r,n=[],o=arguments.length;o--;)n[o]=arguments[o];1===n.length&&n[0].constructor&&n[0].constructor===Object?r=n[0]:(a=(i=n)[0],r=i[1]),r||(r={}),r=d.extend({},r),a&&!r.el&&(r.el=a),e.call(this,r),Object.keys(M).forEach(function(e){Object.keys(M[e]).forEach(function(i){t.prototype[i]||(t.prototype[i]=M[e][i])})});var l=this;void 0===l.modules&&(l.modules={}),Object.keys(l.modules).forEach(function(e){var t=l.modules[e];if(t.params){var i=Object.keys(t.params)[0],s=t.params[i];if("object"!=typeof s)return;if(!(i in r&&"enabled"in s))return;!0===r[i]&&(r[i]={enabled:!0}),"object"!=typeof r[i]||"enabled"in r[i]||(r[i].enabled=!0),r[i]||(r[i]={enabled:!1})}});var p=d.extend({},C);l.useModulesParams(p),l.params=d.extend({},p,z,r),l.originalParams=d.extend({},l.params),l.passedParams=d.extend({},r),l.$=s;var c=s(l.params.el);if(a=c[0]){if(c.length>1){var u=[];return c.each(function(e,i){var s=d.extend({},r,{el:i});u.push(new t(s))}),u}a.swiper=l,c.data("swiper",l);var v,f,m=c.children("."+l.params.wrapperClass);return d.extend(l,{$el:c,el:a,$wrapperEl:m,wrapperEl:m[0],classNames:[],slides:s(),slidesGrid:[],snapGrid:[],slidesSizesGrid:[],isHorizontal:function(){return"horizontal"===l.params.direction},isVertical:function(){return"vertical"===l.params.direction},rtl:"rtl"===a.dir.toLowerCase()||"rtl"===c.css("direction"),rtlTranslate:"horizontal"===l.params.direction&&("rtl"===a.dir.toLowerCase()||"rtl"===c.css("direction")),wrongRTL:"-webkit-box"===m.css("display"),activeIndex:0,realIndex:0,isBeginning:!0,isEnd:!1,translate:0,previousTranslate:0,progress:0,velocity:0,animating:!1,allowSlideNext:l.params.allowSlideNext,allowSlidePrev:l.params.allowSlidePrev,touchEvents:(v=["touchstart","touchmove","touchend"],f=["mousedown","mousemove","mouseup"],h.pointerEvents?f=["pointerdown","pointermove","pointerup"]:h.prefixedPointerEvents&&(f=["MSPointerDown","MSPointerMove","MSPointerUp"]),l.touchEventsTouch={start:v[0],move:v[1],end:v[2]},l.touchEventsDesktop={start:f[0],move:f[1],end:f[2]},h.touch||!l.params.simulateTouch?l.touchEventsTouch:l.touchEventsDesktop),touchEventsData:{isTouched:void 0,isMoved:void 0,allowTouchCallbacks:void 0,touchStartTime:void 0,isScrolling:void 0,currentTranslate:void 0,startTranslate:void 0,allowThresholdMove:void 0,formElements:"input, select, option, textarea, button, video",lastClickTime:d.now(),clickTimeout:void 0,velocities:[],allowMomentumBounce:void 0,isTouchEvent:void 0,startMoving:void 0},allowClick:!0,allowTouchMove:l.params.allowTouchMove,touches:{startX:0,startY:0,currentX:0,currentY:0,diff:0},imagesToLoad:[],imagesLoaded:0}),l.useModules(),l.params.init&&l.init(),l}}e&&(t.__proto__=e),t.prototype=Object.create(e&&e.prototype),t.prototype.constructor=t;var i={extendedDefaults:{configurable:!0},defaults:{configurable:!0},Class:{configurable:!0},$:{configurable:!0}};return t.prototype.slidesPerViewDynamic=function(){var e=this.params,t=this.slides,i=this.slidesGrid,s=this.size,a=this.activeIndex,r=1;if(e.centeredSlides){for(var n,o=t[a].swiperSlideSize,l=a+1;l<t.length;l+=1)t[l]&&!n&&(r+=1,(o+=t[l].swiperSlideSize)>s&&(n=!0));for(var d=a-1;d>=0;d-=1)t[d]&&!n&&(r+=1,(o+=t[d].swiperSlideSize)>s&&(n=!0))}else for(var h=a+1;h<t.length;h+=1)i[h]-i[a]<s&&(r+=1);return r},t.prototype.update=function(){var e=this;if(e&&!e.destroyed){var t=e.snapGrid,i=e.params;i.breakpoints&&e.setBreakpoint(),e.updateSize(),e.updateSlides(),e.updateProgress(),e.updateSlidesClasses(),e.params.freeMode?(s(),e.params.autoHeight&&e.updateAutoHeight()):(("auto"===e.params.slidesPerView||e.params.slidesPerView>1)&&e.isEnd&&!e.params.centeredSlides?e.slideTo(e.slides.length-1,0,!1,!0):e.slideTo(e.activeIndex,0,!1,!0))||s(),i.watchOverflow&&t!==e.snapGrid&&e.checkOverflow(),e.emit("update")}function s(){var t=e.rtlTranslate?-1*e.translate:e.translate,i=Math.min(Math.max(t,e.maxTranslate()),e.minTranslate());e.setTranslate(i),e.updateActiveIndex(),e.updateSlidesClasses()}},t.prototype.init=function(){this.initialized||(this.emit("beforeInit"),this.params.breakpoints&&this.setBreakpoint(),this.addClasses(),this.params.loop&&this.loopCreate(),this.updateSize(),this.updateSlides(),this.params.watchOverflow&&this.checkOverflow(),this.params.grabCursor&&this.setGrabCursor(),this.params.preloadImages&&this.preloadImages(),this.params.loop?this.slideTo(this.params.initialSlide+this.loopedSlides,0,this.params.runCallbacksOnInit):this.slideTo(this.params.initialSlide,0,this.params.runCallbacksOnInit),this.attachEvents(),this.initialized=!0,this.emit("init"))},t.prototype.destroy=function(e,t){void 0===e&&(e=!0),void 0===t&&(t=!0);var i=this,s=i.params,a=i.$el,r=i.$wrapperEl,n=i.slides;return void 0===i.params||i.destroyed?null:(i.emit("beforeDestroy"),i.initialized=!1,i.detachEvents(),s.loop&&i.loopDestroy(),t&&(i.removeClasses(),a.removeAttr("style"),r.removeAttr("style"),n&&n.length&&n.removeClass([s.slideVisibleClass,s.slideActiveClass,s.slideNextClass,s.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")),i.emit("destroy"),Object.keys(i.eventsListeners).forEach(function(e){i.off(e)}),!1!==e&&(i.$el[0].swiper=null,i.$el.data("swiper",null),d.deleteProps(i)),i.destroyed=!0,null)},t.extendDefaults=function(e){d.extend(z,e)},i.extendedDefaults.get=function(){return z},i.defaults.get=function(){return C},i.Class.get=function(){return e},i.$.get=function(){return s},Object.defineProperties(t,i),t}(p),P={name:"device",proto:{device:y},static:{device:y}},$={name:"support",proto:{support:h},static:{support:h}},L={name:"browser",proto:{browser:S},static:{browser:S}},I={name:"resize",create:function(){var e=this;d.extend(e,{resize:{resizeHandler:function(){e&&!e.destroyed&&e.initialized&&(e.emit("beforeResize"),e.emit("resize"))},orientationChangeHandler:function(){e&&!e.destroyed&&e.initialized&&e.emit("orientationchange")}}})},on:{init:function(){t.addEventListener("resize",this.resize.resizeHandler),t.addEventListener("orientationchange",this.resize.orientationChangeHandler)},destroy:function(){t.removeEventListener("resize",this.resize.resizeHandler),t.removeEventListener("orientationchange",this.resize.orientationChangeHandler)}}},D={func:t.MutationObserver||t.WebkitMutationObserver,attach:function(e,t){void 0===t&&(t={});var i=this,s=new(0,D.func)(function(e){e.forEach(function(e){i.emit("observerUpdate",e)})});s.observe(e,{attributes:void 0===t.attributes||t.attributes,childList:void 0===t.childList||t.childList,characterData:void 0===t.characterData||t.characterData}),i.observer.observers.push(s)},init:function(){if(h.observer&&this.params.observer){if(this.params.observeParents)for(var e=this.$el.parents(),t=0;t<e.length;t+=1)this.observer.attach(e[t]);this.observer.attach(this.$el[0],{childList:!1}),this.observer.attach(this.$wrapperEl[0],{attributes:!1})}},destroy:function(){this.observer.observers.forEach(function(e){e.disconnect()}),this.observer.observers=[]}},O={name:"observer",params:{observer:!1,observeParents:!1},create:function(){d.extend(this,{observer:{init:D.init.bind(this),attach:D.attach.bind(this),destroy:D.destroy.bind(this),observers:[]}})},on:{init:function(){this.observer.init()},destroy:function(){this.observer.destroy()}}},A={update:function(e){var t=this,i=t.params,s=i.slidesPerView,a=i.slidesPerGroup,r=i.centeredSlides,n=t.virtual,o=n.from,l=n.to,h=n.slides,p=n.slidesGrid,c=n.renderSlide,u=n.offset;t.updateActiveIndex();var v,f,m,g=t.activeIndex||0;v=t.rtlTranslate?"right":t.isHorizontal()?"left":"top",r?(f=Math.floor(s/2)+a,m=Math.floor(s/2)+a):(f=s+(a-1),m=a);var b=Math.max((g||0)-m,0),w=Math.min((g||0)+f,h.length-1),y=(t.slidesGrid[b]||0)-(t.slidesGrid[0]||0);function x(){t.updateSlides(),t.updateProgress(),t.updateSlidesClasses(),t.lazy&&t.params.lazy.enabled&&t.lazy.load()}if(d.extend(t.virtual,{from:b,to:w,offset:y,slidesGrid:t.slidesGrid}),o===b&&l===w&&!e)return t.slidesGrid!==p&&y!==u&&t.slides.css(v,y+"px"),void t.updateProgress();if(t.params.virtual.renderExternal)return t.params.virtual.renderExternal.call(t,{offset:y,from:b,to:w,slides:function(){for(var e=[],t=b;t<=w;t+=1)e.push(h[t]);return e}()}),void x();var E=[],T=[];if(e)t.$wrapperEl.find("."+t.params.slideClass).remove();else for(var S=o;S<=l;S+=1)(S<b||S>w)&&t.$wrapperEl.find("."+t.params.slideClass+'[data-swiper-slide-index="'+S+'"]').remove();for(var C=0;C<h.length;C+=1)C>=b&&C<=w&&(void 0===l||e?T.push(C):(C>l&&T.push(C),C<o&&E.push(C)));T.forEach(function(e){t.$wrapperEl.append(c(h[e],e))}),E.sort(function(e,t){return e<t}).forEach(function(e){t.$wrapperEl.prepend(c(h[e],e))}),t.$wrapperEl.children(".swiper-slide").css(v,y+"px"),x()},renderSlide:function(e,t){var i=this.params.virtual;if(i.cache&&this.virtual.cache[t])return this.virtual.cache[t];var a=i.renderSlide?s(i.renderSlide.call(this,e,t)):s('<div class="'+this.params.slideClass+'" data-swiper-slide-index="'+t+'">'+e+"</div>");return a.attr("data-swiper-slide-index")||a.attr("data-swiper-slide-index",t),i.cache&&(this.virtual.cache[t]=a),a},appendSlide:function(e){this.virtual.slides.push(e),this.virtual.update(!0)},prependSlide:function(e){if(this.virtual.slides.unshift(e),this.params.virtual.cache){var t=this.virtual.cache,i={};Object.keys(t).forEach(function(e){i[e+1]=t[e]}),this.virtual.cache=i}this.virtual.update(!0),this.slideNext(0)}},G={name:"virtual",params:{virtual:{enabled:!1,slides:[],cache:!0,renderSlide:null,renderExternal:null}},create:function(){d.extend(this,{virtual:{update:A.update.bind(this),appendSlide:A.appendSlide.bind(this),prependSlide:A.prependSlide.bind(this),renderSlide:A.renderSlide.bind(this),slides:this.params.virtual.slides,cache:{}}})},on:{beforeInit:function(){if(this.params.virtual.enabled){this.classNames.push(this.params.containerModifierClass+"virtual");var e={watchSlidesProgress:!0};d.extend(this.params,e),d.extend(this.originalParams,e),this.virtual.update()}},setTranslate:function(){this.params.virtual.enabled&&this.virtual.update()}}},H={handle:function(i){var s=this.rtlTranslate,a=i;a.originalEvent&&(a=a.originalEvent);var r=a.keyCode||a.charCode;if(!this.allowSlideNext&&(this.isHorizontal()&&39===r||this.isVertical()&&40===r))return!1;if(!this.allowSlidePrev&&(this.isHorizontal()&&37===r||this.isVertical()&&38===r))return!1;if(!(a.shiftKey||a.altKey||a.ctrlKey||a.metaKey||e.activeElement&&e.activeElement.nodeName&&("input"===e.activeElement.nodeName.toLowerCase()||"textarea"===e.activeElement.nodeName.toLowerCase()))){if(this.params.keyboard.onlyInViewport&&(37===r||39===r||38===r||40===r)){var n=!1;if(this.$el.parents("."+this.params.slideClass).length>0&&0===this.$el.parents("."+this.params.slideActiveClass).length)return;var o=t.innerWidth,l=t.innerHeight,d=this.$el.offset();s&&(d.left-=this.$el[0].scrollLeft);for(var h=[[d.left,d.top],[d.left+this.width,d.top],[d.left,d.top+this.height],[d.left+this.width,d.top+this.height]],p=0;p<h.length;p+=1){var c=h[p];c[0]>=0&&c[0]<=o&&c[1]>=0&&c[1]<=l&&(n=!0)}if(!n)return}this.isHorizontal()?(37!==r&&39!==r||(a.preventDefault?a.preventDefault():a.returnValue=!1),(39===r&&!s||37===r&&s)&&this.slideNext(),(37===r&&!s||39===r&&s)&&this.slidePrev()):(38!==r&&40!==r||(a.preventDefault?a.preventDefault():a.returnValue=!1),40===r&&this.slideNext(),38===r&&this.slidePrev()),this.emit("keyPress",r)}},enable:function(){this.keyboard.enabled||(s(e).on("keydown",this.keyboard.handle),this.keyboard.enabled=!0)},disable:function(){this.keyboard.enabled&&(s(e).off("keydown",this.keyboard.handle),this.keyboard.enabled=!1)}},N={name:"keyboard",params:{keyboard:{enabled:!1,onlyInViewport:!0}},create:function(){d.extend(this,{keyboard:{enabled:!1,enable:H.enable.bind(this),disable:H.disable.bind(this),handle:H.handle.bind(this)}})},on:{init:function(){this.params.keyboard.enabled&&this.keyboard.enable()},destroy:function(){this.keyboard.enabled&&this.keyboard.disable()}}};var B={lastScrollTime:d.now(),event:t.navigator.userAgent.indexOf("firefox")>-1?"DOMMouseScroll":function(){var t="onwheel"in e;if(!t){var i=e.createElement("div");i.setAttribute("onwheel","return;"),t="function"==typeof i.onwheel}return!t&&e.implementation&&e.implementation.hasFeature&&!0!==e.implementation.hasFeature("","")&&(t=e.implementation.hasFeature("Events.wheel","3.0")),t}()?"wheel":"mousewheel",normalize:function(e){var t=0,i=0,s=0,a=0;return"detail"in e&&(i=e.detail),"wheelDelta"in e&&(i=-e.wheelDelta/120),"wheelDeltaY"in e&&(i=-e.wheelDeltaY/120),"wheelDeltaX"in e&&(t=-e.wheelDeltaX/120),"axis"in e&&e.axis===e.HORIZONTAL_AXIS&&(t=i,i=0),s=10*t,a=10*i,"deltaY"in e&&(a=e.deltaY),"deltaX"in e&&(s=e.deltaX),(s||a)&&e.deltaMode&&(1===e.deltaMode?(s*=40,a*=40):(s*=800,a*=800)),s&&!t&&(t=s<1?-1:1),a&&!i&&(i=a<1?-1:1),{spinX:t,spinY:i,pixelX:s,pixelY:a}},handleMouseEnter:function(){this.mouseEntered=!0},handleMouseLeave:function(){this.mouseEntered=!1},handle:function(e){var i=e,s=this,a=s.params.mousewheel;if(!s.mouseEntered&&!a.releaseOnEdges)return!0;i.originalEvent&&(i=i.originalEvent);var r=0,n=s.rtlTranslate?-1:1,o=B.normalize(i);if(a.forceToAxis)if(s.isHorizontal()){if(!(Math.abs(o.pixelX)>Math.abs(o.pixelY)))return!0;r=o.pixelX*n}else{if(!(Math.abs(o.pixelY)>Math.abs(o.pixelX)))return!0;r=o.pixelY}else r=Math.abs(o.pixelX)>Math.abs(o.pixelY)?-o.pixelX*n:-o.pixelY;if(0===r)return!0;if(a.invert&&(r=-r),s.params.freeMode){s.params.loop&&s.loopFix();var l=s.getTranslate()+r*a.sensitivity,h=s.isBeginning,p=s.isEnd;if(l>=s.minTranslate()&&(l=s.minTranslate()),l<=s.maxTranslate()&&(l=s.maxTranslate()),s.setTransition(0),s.setTranslate(l),s.updateProgress(),s.updateActiveIndex(),s.updateSlidesClasses(),(!h&&s.isBeginning||!p&&s.isEnd)&&s.updateSlidesClasses(),s.params.freeModeSticky&&(clearTimeout(s.mousewheel.timeout),s.mousewheel.timeout=d.nextTick(function(){s.slideToClosest()},300)),s.emit("scroll",i),s.params.autoplay&&s.params.autoplayDisableOnInteraction&&s.autoplay.stop(),l===s.minTranslate()||l===s.maxTranslate())return!0}else{if(d.now()-s.mousewheel.lastScrollTime>60)if(r<0)if(s.isEnd&&!s.params.loop||s.animating){if(a.releaseOnEdges)return!0}else s.slideNext(),s.emit("scroll",i);else if(s.isBeginning&&!s.params.loop||s.animating){if(a.releaseOnEdges)return!0}else s.slidePrev(),s.emit("scroll",i);s.mousewheel.lastScrollTime=(new t.Date).getTime()}return i.preventDefault?i.preventDefault():i.returnValue=!1,!1},enable:function(){if(!B.event)return!1;if(this.mousewheel.enabled)return!1;var e=this.$el;return"container"!==this.params.mousewheel.eventsTarged&&(e=s(this.params.mousewheel.eventsTarged)),e.on("mouseenter",this.mousewheel.handleMouseEnter),e.on("mouseleave",this.mousewheel.handleMouseLeave),e.on(B.event,this.mousewheel.handle),this.mousewheel.enabled=!0,!0},disable:function(){if(!B.event)return!1;if(!this.mousewheel.enabled)return!1;var e=this.$el;return"container"!==this.params.mousewheel.eventsTarged&&(e=s(this.params.mousewheel.eventsTarged)),e.off(B.event,this.mousewheel.handle),this.mousewheel.enabled=!1,!0}},X={update:function(){var e=this.params.navigation;if(!this.params.loop){var t=this.navigation,i=t.$nextEl,s=t.$prevEl;s&&s.length>0&&(this.isBeginning?s.addClass(e.disabledClass):s.removeClass(e.disabledClass),s[this.params.watchOverflow&&this.isLocked?"addClass":"removeClass"](e.lockClass)),i&&i.length>0&&(this.isEnd?i.addClass(e.disabledClass):i.removeClass(e.disabledClass),i[this.params.watchOverflow&&this.isLocked?"addClass":"removeClass"](e.lockClass))}},init:function(){var e,t,i=this,a=i.params.navigation;(a.nextEl||a.prevEl)&&(a.nextEl&&(e=s(a.nextEl),i.params.uniqueNavElements&&"string"==typeof a.nextEl&&e.length>1&&1===i.$el.find(a.nextEl).length&&(e=i.$el.find(a.nextEl))),a.prevEl&&(t=s(a.prevEl),i.params.uniqueNavElements&&"string"==typeof a.prevEl&&t.length>1&&1===i.$el.find(a.prevEl).length&&(t=i.$el.find(a.prevEl))),e&&e.length>0&&e.on("click",function(e){e.preventDefault(),i.isEnd&&!i.params.loop||i.slideNext()}),t&&t.length>0&&t.on("click",function(e){e.preventDefault(),i.isBeginning&&!i.params.loop||i.slidePrev()}),d.extend(i.navigation,{$nextEl:e,nextEl:e&&e[0],$prevEl:t,prevEl:t&&t[0]}))},destroy:function(){var e=this.navigation,t=e.$nextEl,i=e.$prevEl;t&&t.length&&(t.off("click"),t.removeClass(this.params.navigation.disabledClass)),i&&i.length&&(i.off("click"),i.removeClass(this.params.navigation.disabledClass))}},Y={update:function(){var e=this.rtl,t=this.params.pagination;if(t.el&&this.pagination.el&&this.pagination.$el&&0!==this.pagination.$el.length){var i,a=this.virtual&&this.params.virtual.enabled?this.virtual.slides.length:this.slides.length,r=this.pagination.$el,n=this.params.loop?Math.ceil((a-2*this.loopedSlides)/this.params.slidesPerGroup):this.snapGrid.length;if(this.params.loop?((i=Math.ceil((this.activeIndex-this.loopedSlides)/this.params.slidesPerGroup))>a-1-2*this.loopedSlides&&(i-=a-2*this.loopedSlides),i>n-1&&(i-=n),i<0&&"bullets"!==this.params.paginationType&&(i=n+i)):i=void 0!==this.snapIndex?this.snapIndex:this.activeIndex||0,"bullets"===t.type&&this.pagination.bullets&&this.pagination.bullets.length>0){var o,l,d,h=this.pagination.bullets;if(t.dynamicBullets&&(this.pagination.bulletSize=h.eq(0)[this.isHorizontal()?"outerWidth":"outerHeight"](!0),r.css(this.isHorizontal()?"width":"height",this.pagination.bulletSize*(t.dynamicMainBullets+4)+"px"),t.dynamicMainBullets>1&&void 0!==this.previousIndex&&(this.pagination.dynamicBulletIndex+=i-this.previousIndex,this.pagination.dynamicBulletIndex>t.dynamicMainBullets-1?this.pagination.dynamicBulletIndex=t.dynamicMainBullets-1:this.pagination.dynamicBulletIndex<0&&(this.pagination.dynamicBulletIndex=0)),o=i-this.pagination.dynamicBulletIndex,d=((l=o+(Math.min(h.length,t.dynamicMainBullets)-1))+o)/2),h.removeClass(t.bulletActiveClass+" "+t.bulletActiveClass+"-next "+t.bulletActiveClass+"-next-next "+t.bulletActiveClass+"-prev "+t.bulletActiveClass+"-prev-prev "+t.bulletActiveClass+"-main"),r.length>1)h.each(function(e,a){var r=s(a),n=r.index();n===i&&r.addClass(t.bulletActiveClass),t.dynamicBullets&&(n>=o&&n<=l&&r.addClass(t.bulletActiveClass+"-main"),n===o&&r.prev().addClass(t.bulletActiveClass+"-prev").prev().addClass(t.bulletActiveClass+"-prev-prev"),n===l&&r.next().addClass(t.bulletActiveClass+"-next").next().addClass(t.bulletActiveClass+"-next-next"))});else if(h.eq(i).addClass(t.bulletActiveClass),t.dynamicBullets){for(var p=h.eq(o),c=h.eq(l),u=o;u<=l;u+=1)h.eq(u).addClass(t.bulletActiveClass+"-main");p.prev().addClass(t.bulletActiveClass+"-prev").prev().addClass(t.bulletActiveClass+"-prev-prev"),c.next().addClass(t.bulletActiveClass+"-next").next().addClass(t.bulletActiveClass+"-next-next")}if(t.dynamicBullets){var v=Math.min(h.length,t.dynamicMainBullets+4),f=(this.pagination.bulletSize*v-this.pagination.bulletSize)/2-d*this.pagination.bulletSize,m=e?"right":"left";h.css(this.isHorizontal()?m:"top",f+"px")}}if("fraction"===t.type&&(r.find("."+t.currentClass).text(t.formatFractionCurrent(i+1)),r.find("."+t.totalClass).text(t.formatFractionTotal(n))),"progressbar"===t.type){var g;g=t.progressbarOpposite?this.isHorizontal()?"vertical":"horizontal":this.isHorizontal()?"horizontal":"vertical";var b=(i+1)/n,w=1,y=1;"horizontal"===g?w=b:y=b,r.find("."+t.progressbarFillClass).transform("translate3d(0,0,0) scaleX("+w+") scaleY("+y+")").transition(this.params.speed)}"custom"===t.type&&t.renderCustom?(r.html(t.renderCustom(this,i+1,n)),this.emit("paginationRender",this,r[0])):this.emit("paginationUpdate",this,r[0]),r[this.params.watchOverflow&&this.isLocked?"addClass":"removeClass"](t.lockClass)}},render:function(){var e=this.params.pagination;if(e.el&&this.pagination.el&&this.pagination.$el&&0!==this.pagination.$el.length){var t=this.virtual&&this.params.virtual.enabled?this.virtual.slides.length:this.slides.length,i=this.pagination.$el,s="";if("bullets"===e.type){for(var a=this.params.loop?Math.ceil((t-2*this.loopedSlides)/this.params.slidesPerGroup):this.snapGrid.length,r=0;r<a;r+=1)e.renderBullet?s+=e.renderBullet.call(this,r,e.bulletClass):s+="<"+e.bulletElement+' class="'+e.bulletClass+'"></'+e.bulletElement+">";i.html(s),this.pagination.bullets=i.find("."+e.bulletClass)}"fraction"===e.type&&(s=e.renderFraction?e.renderFraction.call(this,e.currentClass,e.totalClass):'<span class="'+e.currentClass+'"></span> / <span class="'+e.totalClass+'"></span>',i.html(s)),"progressbar"===e.type&&(s=e.renderProgressbar?e.renderProgressbar.call(this,e.progressbarFillClass):'<span class="'+e.progressbarFillClass+'"></span>',i.html(s)),"custom"!==e.type&&this.emit("paginationRender",this.pagination.$el[0])}},init:function(){var e=this,t=e.params.pagination;if(t.el){var i=s(t.el);0!==i.length&&(e.params.uniqueNavElements&&"string"==typeof t.el&&i.length>1&&1===e.$el.find(t.el).length&&(i=e.$el.find(t.el)),"bullets"===t.type&&t.clickable&&i.addClass(t.clickableClass),i.addClass(t.modifierClass+t.type),"bullets"===t.type&&t.dynamicBullets&&(i.addClass(""+t.modifierClass+t.type+"-dynamic"),e.pagination.dynamicBulletIndex=0,t.dynamicMainBullets<1&&(t.dynamicMainBullets=1)),"progressbar"===t.type&&t.progressbarOpposite&&i.addClass(t.progressbarOppositeClass),t.clickable&&i.on("click","."+t.bulletClass,function(t){t.preventDefault();var i=s(this).index()*e.params.slidesPerGroup;e.params.loop&&(i+=e.loopedSlides),e.slideTo(i)}),d.extend(e.pagination,{$el:i,el:i[0]}))}},destroy:function(){var e=this.params.pagination;if(e.el&&this.pagination.el&&this.pagination.$el&&0!==this.pagination.$el.length){var t=this.pagination.$el;t.removeClass(e.hiddenClass),t.removeClass(e.modifierClass+e.type),this.pagination.bullets&&this.pagination.bullets.removeClass(e.bulletActiveClass),e.clickable&&t.off("click","."+e.bulletClass)}}},V={setTranslate:function(){if(this.params.scrollbar.el&&this.scrollbar.el){var e=this.scrollbar,t=this.rtlTranslate,i=this.progress,s=e.dragSize,a=e.trackSize,r=e.$dragEl,n=e.$el,o=this.params.scrollbar,l=s,d=(a-s)*i;t?(d=-d)>0?(l=s-d,d=0):-d+s>a&&(l=a+d):d<0?(l=s+d,d=0):d+s>a&&(l=a-d),this.isHorizontal()?(h.transforms3d?r.transform("translate3d("+d+"px, 0, 0)"):r.transform("translateX("+d+"px)"),r[0].style.width=l+"px"):(h.transforms3d?r.transform("translate3d(0px, "+d+"px, 0)"):r.transform("translateY("+d+"px)"),r[0].style.height=l+"px"),o.hide&&(clearTimeout(this.scrollbar.timeout),n[0].style.opacity=1,this.scrollbar.timeout=setTimeout(function(){n[0].style.opacity=0,n.transition(400)},1e3))}},setTransition:function(e){this.params.scrollbar.el&&this.scrollbar.el&&this.scrollbar.$dragEl.transition(e)},updateSize:function(){if(this.params.scrollbar.el&&this.scrollbar.el){var e=this.scrollbar,t=e.$dragEl,i=e.$el;t[0].style.width="",t[0].style.height="";var s,a=this.isHorizontal()?i[0].offsetWidth:i[0].offsetHeight,r=this.size/this.virtualSize,n=r*(a/this.size);s="auto"===this.params.scrollbar.dragSize?a*r:parseInt(this.params.scrollbar.dragSize,10),this.isHorizontal()?t[0].style.width=s+"px":t[0].style.height=s+"px",i[0].style.display=r>=1?"none":"",this.params.scrollbarHide&&(i[0].style.opacity=0),d.extend(e,{trackSize:a,divider:r,moveDivider:n,dragSize:s}),e.$el[this.params.watchOverflow&&this.isLocked?"addClass":"removeClass"](this.params.scrollbar.lockClass)}},setDragPosition:function(e){var t,i=this.scrollbar,s=this.rtlTranslate,a=i.$el,r=i.dragSize,n=i.trackSize;t=((this.isHorizontal()?"touchstart"===e.type||"touchmove"===e.type?e.targetTouches[0].pageX:e.pageX||e.clientX:"touchstart"===e.type||"touchmove"===e.type?e.targetTouches[0].pageY:e.pageY||e.clientY)-a.offset()[this.isHorizontal()?"left":"top"]-r/2)/(n-r),t=Math.max(Math.min(t,1),0),s&&(t=1-t);var o=this.minTranslate()+(this.maxTranslate()-this.minTranslate())*t;this.updateProgress(o),this.setTranslate(o),this.updateActiveIndex(),this.updateSlidesClasses()},onDragStart:function(e){var t=this.params.scrollbar,i=this.scrollbar,s=this.$wrapperEl,a=i.$el,r=i.$dragEl;this.scrollbar.isTouched=!0,e.preventDefault(),e.stopPropagation(),s.transition(100),r.transition(100),i.setDragPosition(e),clearTimeout(this.scrollbar.dragTimeout),a.transition(0),t.hide&&a.css("opacity",1),this.emit("scrollbarDragStart",e)},onDragMove:function(e){var t=this.scrollbar,i=this.$wrapperEl,s=t.$el,a=t.$dragEl;this.scrollbar.isTouched&&(e.preventDefault?e.preventDefault():e.returnValue=!1,t.setDragPosition(e),i.transition(0),s.transition(0),a.transition(0),this.emit("scrollbarDragMove",e))},onDragEnd:function(e){var t=this.params.scrollbar,i=this.scrollbar.$el;this.scrollbar.isTouched&&(this.scrollbar.isTouched=!1,t.hide&&(clearTimeout(this.scrollbar.dragTimeout),this.scrollbar.dragTimeout=d.nextTick(function(){i.css("opacity",0),i.transition(400)},1e3)),this.emit("scrollbarDragEnd",e),t.snapOnRelease&&this.slideToClosest())},enableDraggable:function(){if(this.params.scrollbar.el){var t=this.scrollbar,i=this.touchEvents,s=this.touchEventsDesktop,a=this.params,r=t.$el[0],n=!(!h.passiveListener||!a.passiveListener)&&{passive:!1,capture:!1},o=!(!h.passiveListener||!a.passiveListener)&&{passive:!0,capture:!1};h.touch||!h.pointerEvents&&!h.prefixedPointerEvents?(h.touch&&(r.addEventListener(i.start,this.scrollbar.onDragStart,n),r.addEventListener(i.move,this.scrollbar.onDragMove,n),r.addEventListener(i.end,this.scrollbar.onDragEnd,o)),(a.simulateTouch&&!y.ios&&!y.android||a.simulateTouch&&!h.touch&&y.ios)&&(r.addEventListener("mousedown",this.scrollbar.onDragStart,n),e.addEventListener("mousemove",this.scrollbar.onDragMove,n),e.addEventListener("mouseup",this.scrollbar.onDragEnd,o))):(r.addEventListener(s.start,this.scrollbar.onDragStart,n),e.addEventListener(s.move,this.scrollbar.onDragMove,n),e.addEventListener(s.end,this.scrollbar.onDragEnd,o))}},disableDraggable:function(){if(this.params.scrollbar.el){var t=this.scrollbar,i=this.touchEvents,s=this.touchEventsDesktop,a=this.params,r=t.$el[0],n=!(!h.passiveListener||!a.passiveListener)&&{passive:!1,capture:!1},o=!(!h.passiveListener||!a.passiveListener)&&{passive:!0,capture:!1};h.touch||!h.pointerEvents&&!h.prefixedPointerEvents?(h.touch&&(r.removeEventListener(i.start,this.scrollbar.onDragStart,n),r.removeEventListener(i.move,this.scrollbar.onDragMove,n),r.removeEventListener(i.end,this.scrollbar.onDragEnd,o)),(a.simulateTouch&&!y.ios&&!y.android||a.simulateTouch&&!h.touch&&y.ios)&&(r.removeEventListener("mousedown",this.scrollbar.onDragStart,n),e.removeEventListener("mousemove",this.scrollbar.onDragMove,n),e.removeEventListener("mouseup",this.scrollbar.onDragEnd,o))):(r.removeEventListener(s.start,this.scrollbar.onDragStart,n),e.removeEventListener(s.move,this.scrollbar.onDragMove,n),e.removeEventListener(s.end,this.scrollbar.onDragEnd,o))}},init:function(){if(this.params.scrollbar.el){var e=this.scrollbar,t=this.$el,i=this.params.scrollbar,a=s(i.el);this.params.uniqueNavElements&&"string"==typeof i.el&&a.length>1&&1===t.find(i.el).length&&(a=t.find(i.el));var r=a.find("."+this.params.scrollbar.dragClass);0===r.length&&(r=s('<div class="'+this.params.scrollbar.dragClass+'"></div>'),a.append(r)),d.extend(e,{$el:a,el:a[0],$dragEl:r,dragEl:r[0]}),i.draggable&&e.enableDraggable()}},destroy:function(){this.scrollbar.disableDraggable()}},R={setTransform:function(e,t){var i=this.rtl,a=s(e),r=i?-1:1,n=a.attr("data-swiper-parallax")||"0",o=a.attr("data-swiper-parallax-x"),l=a.attr("data-swiper-parallax-y"),d=a.attr("data-swiper-parallax-scale"),h=a.attr("data-swiper-parallax-opacity");if(o||l?(o=o||"0",l=l||"0"):this.isHorizontal()?(o=n,l="0"):(l=n,o="0"),o=o.indexOf("%")>=0?parseInt(o,10)*t*r+"%":o*t*r+"px",l=l.indexOf("%")>=0?parseInt(l,10)*t+"%":l*t+"px",void 0!==h&&null!==h){var p=h-(h-1)*(1-Math.abs(t));a[0].style.opacity=p}if(void 0===d||null===d)a.transform("translate3d("+o+", "+l+", 0px)");else{var c=d-(d-1)*(1-Math.abs(t));a.transform("translate3d("+o+", "+l+", 0px) scale("+c+")")}},setTranslate:function(){var e=this,t=e.$el,i=e.slides,a=e.progress,r=e.snapGrid;t.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(t,i){e.parallax.setTransform(i,a)}),i.each(function(t,i){var n=i.progress;e.params.slidesPerGroup>1&&"auto"!==e.params.slidesPerView&&(n+=Math.ceil(t/2)-a*(r.length-1)),n=Math.min(Math.max(n,-1),1),s(i).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(t,i){e.parallax.setTransform(i,n)})})},setTransition:function(e){void 0===e&&(e=this.params.speed);this.$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(t,i){var a=s(i),r=parseInt(a.attr("data-swiper-parallax-duration"),10)||e;0===e&&(r=0),a.transition(r)})}},F={getDistanceBetweenTouches:function(e){if(e.targetTouches.length<2)return 1;var t=e.targetTouches[0].pageX,i=e.targetTouches[0].pageY,s=e.targetTouches[1].pageX,a=e.targetTouches[1].pageY;return Math.sqrt(Math.pow(s-t,2)+Math.pow(a-i,2))},onGestureStart:function(e){var t=this.params.zoom,i=this.zoom,a=i.gesture;if(i.fakeGestureTouched=!1,i.fakeGestureMoved=!1,!h.gestures){if("touchstart"!==e.type||"touchstart"===e.type&&e.targetTouches.length<2)return;i.fakeGestureTouched=!0,a.scaleStart=F.getDistanceBetweenTouches(e)}a.$slideEl&&a.$slideEl.length||(a.$slideEl=s(e.target).closest(".swiper-slide"),0===a.$slideEl.length&&(a.$slideEl=this.slides.eq(this.activeIndex)),a.$imageEl=a.$slideEl.find("img, svg, canvas"),a.$imageWrapEl=a.$imageEl.parent("."+t.containerClass),a.maxRatio=a.$imageWrapEl.attr("data-swiper-zoom")||t.maxRatio,0!==a.$imageWrapEl.length)?(a.$imageEl.transition(0),this.zoom.isScaling=!0):a.$imageEl=void 0},onGestureChange:function(e){var t=this.params.zoom,i=this.zoom,s=i.gesture;if(!h.gestures){if("touchmove"!==e.type||"touchmove"===e.type&&e.targetTouches.length<2)return;i.fakeGestureMoved=!0,s.scaleMove=F.getDistanceBetweenTouches(e)}s.$imageEl&&0!==s.$imageEl.length&&(h.gestures?this.zoom.scale=e.scale*i.currentScale:i.scale=s.scaleMove/s.scaleStart*i.currentScale,i.scale>s.maxRatio&&(i.scale=s.maxRatio-1+Math.pow(i.scale-s.maxRatio+1,.5)),i.scale<t.minRatio&&(i.scale=t.minRatio+1-Math.pow(t.minRatio-i.scale+1,.5)),s.$imageEl.transform("translate3d(0,0,0) scale("+i.scale+")"))},onGestureEnd:function(e){var t=this.params.zoom,i=this.zoom,s=i.gesture;if(!h.gestures){if(!i.fakeGestureTouched||!i.fakeGestureMoved)return;if("touchend"!==e.type||"touchend"===e.type&&e.changedTouches.length<2&&!y.android)return;i.fakeGestureTouched=!1,i.fakeGestureMoved=!1}s.$imageEl&&0!==s.$imageEl.length&&(i.scale=Math.max(Math.min(i.scale,s.maxRatio),t.minRatio),s.$imageEl.transition(this.params.speed).transform("translate3d(0,0,0) scale("+i.scale+")"),i.currentScale=i.scale,i.isScaling=!1,1===i.scale&&(s.$slideEl=void 0))},onTouchStart:function(e){var t=this.zoom,i=t.gesture,s=t.image;i.$imageEl&&0!==i.$imageEl.length&&(s.isTouched||(y.android&&e.preventDefault(),s.isTouched=!0,s.touchesStart.x="touchstart"===e.type?e.targetTouches[0].pageX:e.pageX,s.touchesStart.y="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY))},onTouchMove:function(e){var t=this.zoom,i=t.gesture,s=t.image,a=t.velocity;if(i.$imageEl&&0!==i.$imageEl.length&&(this.allowClick=!1,s.isTouched&&i.$slideEl)){s.isMoved||(s.width=i.$imageEl[0].offsetWidth,s.height=i.$imageEl[0].offsetHeight,s.startX=d.getTranslate(i.$imageWrapEl[0],"x")||0,s.startY=d.getTranslate(i.$imageWrapEl[0],"y")||0,i.slideWidth=i.$slideEl[0].offsetWidth,i.slideHeight=i.$slideEl[0].offsetHeight,i.$imageWrapEl.transition(0),this.rtl&&(s.startX=-s.startX,s.startY=-s.startY));var r=s.width*t.scale,n=s.height*t.scale;if(!(r<i.slideWidth&&n<i.slideHeight)){if(s.minX=Math.min(i.slideWidth/2-r/2,0),s.maxX=-s.minX,s.minY=Math.min(i.slideHeight/2-n/2,0),s.maxY=-s.minY,s.touchesCurrent.x="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,s.touchesCurrent.y="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY,!s.isMoved&&!t.isScaling){if(this.isHorizontal()&&(Math.floor(s.minX)===Math.floor(s.startX)&&s.touchesCurrent.x<s.touchesStart.x||Math.floor(s.maxX)===Math.floor(s.startX)&&s.touchesCurrent.x>s.touchesStart.x))return void(s.isTouched=!1);if(!this.isHorizontal()&&(Math.floor(s.minY)===Math.floor(s.startY)&&s.touchesCurrent.y<s.touchesStart.y||Math.floor(s.maxY)===Math.floor(s.startY)&&s.touchesCurrent.y>s.touchesStart.y))return void(s.isTouched=!1)}e.preventDefault(),e.stopPropagation(),s.isMoved=!0,s.currentX=s.touchesCurrent.x-s.touchesStart.x+s.startX,s.currentY=s.touchesCurrent.y-s.touchesStart.y+s.startY,s.currentX<s.minX&&(s.currentX=s.minX+1-Math.pow(s.minX-s.currentX+1,.8)),s.currentX>s.maxX&&(s.currentX=s.maxX-1+Math.pow(s.currentX-s.maxX+1,.8)),s.currentY<s.minY&&(s.currentY=s.minY+1-Math.pow(s.minY-s.currentY+1,.8)),s.currentY>s.maxY&&(s.currentY=s.maxY-1+Math.pow(s.currentY-s.maxY+1,.8)),a.prevPositionX||(a.prevPositionX=s.touchesCurrent.x),a.prevPositionY||(a.prevPositionY=s.touchesCurrent.y),a.prevTime||(a.prevTime=Date.now()),a.x=(s.touchesCurrent.x-a.prevPositionX)/(Date.now()-a.prevTime)/2,a.y=(s.touchesCurrent.y-a.prevPositionY)/(Date.now()-a.prevTime)/2,Math.abs(s.touchesCurrent.x-a.prevPositionX)<2&&(a.x=0),Math.abs(s.touchesCurrent.y-a.prevPositionY)<2&&(a.y=0),a.prevPositionX=s.touchesCurrent.x,a.prevPositionY=s.touchesCurrent.y,a.prevTime=Date.now(),i.$imageWrapEl.transform("translate3d("+s.currentX+"px, "+s.currentY+"px,0)")}}},onTouchEnd:function(){var e=this.zoom,t=e.gesture,i=e.image,s=e.velocity;if(t.$imageEl&&0!==t.$imageEl.length){if(!i.isTouched||!i.isMoved)return i.isTouched=!1,void(i.isMoved=!1);i.isTouched=!1,i.isMoved=!1;var a=300,r=300,n=s.x*a,o=i.currentX+n,l=s.y*r,d=i.currentY+l;0!==s.x&&(a=Math.abs((o-i.currentX)/s.x)),0!==s.y&&(r=Math.abs((d-i.currentY)/s.y));var h=Math.max(a,r);i.currentX=o,i.currentY=d;var p=i.width*e.scale,c=i.height*e.scale;i.minX=Math.min(t.slideWidth/2-p/2,0),i.maxX=-i.minX,i.minY=Math.min(t.slideHeight/2-c/2,0),i.maxY=-i.minY,i.currentX=Math.max(Math.min(i.currentX,i.maxX),i.minX),i.currentY=Math.max(Math.min(i.currentY,i.maxY),i.minY),t.$imageWrapEl.transition(h).transform("translate3d("+i.currentX+"px, "+i.currentY+"px,0)")}},onTransitionEnd:function(){var e=this.zoom,t=e.gesture;t.$slideEl&&this.previousIndex!==this.activeIndex&&(t.$imageEl.transform("translate3d(0,0,0) scale(1)"),t.$imageWrapEl.transform("translate3d(0,0,0)"),t.$slideEl=void 0,t.$imageEl=void 0,t.$imageWrapEl=void 0,e.scale=1,e.currentScale=1)},toggle:function(e){var t=this.zoom;t.scale&&1!==t.scale?t.out():t.in(e)},in:function(e){var t,i,a,r,n,o,l,d,h,p,c,u,v,f,m,g,b=this.zoom,w=this.params.zoom,y=b.gesture,x=b.image;(y.$slideEl||(y.$slideEl=this.clickedSlide?s(this.clickedSlide):this.slides.eq(this.activeIndex),y.$imageEl=y.$slideEl.find("img, svg, canvas"),y.$imageWrapEl=y.$imageEl.parent("."+w.containerClass)),y.$imageEl&&0!==y.$imageEl.length)&&(y.$slideEl.addClass(""+w.zoomedSlideClass),void 0===x.touchesStart.x&&e?(t="touchend"===e.type?e.changedTouches[0].pageX:e.pageX,i="touchend"===e.type?e.changedTouches[0].pageY:e.pageY):(t=x.touchesStart.x,i=x.touchesStart.y),b.scale=y.$imageWrapEl.attr("data-swiper-zoom")||w.maxRatio,b.currentScale=y.$imageWrapEl.attr("data-swiper-zoom")||w.maxRatio,e?(m=y.$slideEl[0].offsetWidth,g=y.$slideEl[0].offsetHeight,a=y.$slideEl.offset().left+m/2-t,r=y.$slideEl.offset().top+g/2-i,l=y.$imageEl[0].offsetWidth,d=y.$imageEl[0].offsetHeight,h=l*b.scale,p=d*b.scale,v=-(c=Math.min(m/2-h/2,0)),f=-(u=Math.min(g/2-p/2,0)),n=a*b.scale,o=r*b.scale,n<c&&(n=c),n>v&&(n=v),o<u&&(o=u),o>f&&(o=f)):(n=0,o=0),y.$imageWrapEl.transition(300).transform("translate3d("+n+"px, "+o+"px,0)"),y.$imageEl.transition(300).transform("translate3d(0,0,0) scale("+b.scale+")"))},out:function(){var e=this.zoom,t=this.params.zoom,i=e.gesture;i.$slideEl||(i.$slideEl=this.clickedSlide?s(this.clickedSlide):this.slides.eq(this.activeIndex),i.$imageEl=i.$slideEl.find("img, svg, canvas"),i.$imageWrapEl=i.$imageEl.parent("."+t.containerClass)),i.$imageEl&&0!==i.$imageEl.length&&(e.scale=1,e.currentScale=1,i.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"),i.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"),i.$slideEl.removeClass(""+t.zoomedSlideClass),i.$slideEl=void 0)},enable:function(){var e=this.zoom;if(!e.enabled){e.enabled=!0;var t=!("touchstart"!==this.touchEvents.start||!h.passiveListener||!this.params.passiveListeners)&&{passive:!0,capture:!1};h.gestures?(this.$wrapperEl.on("gesturestart",".swiper-slide",e.onGestureStart,t),this.$wrapperEl.on("gesturechange",".swiper-slide",e.onGestureChange,t),this.$wrapperEl.on("gestureend",".swiper-slide",e.onGestureEnd,t)):"touchstart"===this.touchEvents.start&&(this.$wrapperEl.on(this.touchEvents.start,".swiper-slide",e.onGestureStart,t),this.$wrapperEl.on(this.touchEvents.move,".swiper-slide",e.onGestureChange,t),this.$wrapperEl.on(this.touchEvents.end,".swiper-slide",e.onGestureEnd,t)),this.$wrapperEl.on(this.touchEvents.move,"."+this.params.zoom.containerClass,e.onTouchMove)}},disable:function(){var e=this.zoom;if(e.enabled){this.zoom.enabled=!1;var t=!("touchstart"!==this.touchEvents.start||!h.passiveListener||!this.params.passiveListeners)&&{passive:!0,capture:!1};h.gestures?(this.$wrapperEl.off("gesturestart",".swiper-slide",e.onGestureStart,t),this.$wrapperEl.off("gesturechange",".swiper-slide",e.onGestureChange,t),this.$wrapperEl.off("gestureend",".swiper-slide",e.onGestureEnd,t)):"touchstart"===this.touchEvents.start&&(this.$wrapperEl.off(this.touchEvents.start,".swiper-slide",e.onGestureStart,t),this.$wrapperEl.off(this.touchEvents.move,".swiper-slide",e.onGestureChange,t),this.$wrapperEl.off(this.touchEvents.end,".swiper-slide",e.onGestureEnd,t)),this.$wrapperEl.off(this.touchEvents.move,"."+this.params.zoom.containerClass,e.onTouchMove)}}},W={loadInSlide:function(e,t){void 0===t&&(t=!0);var i=this,a=i.params.lazy;if(void 0!==e&&0!==i.slides.length){var r=i.virtual&&i.params.virtual.enabled?i.$wrapperEl.children("."+i.params.slideClass+'[data-swiper-slide-index="'+e+'"]'):i.slides.eq(e),n=r.find("."+a.elementClass+":not(."+a.loadedClass+"):not(."+a.loadingClass+")");!r.hasClass(a.elementClass)||r.hasClass(a.loadedClass)||r.hasClass(a.loadingClass)||(n=n.add(r[0])),0!==n.length&&n.each(function(e,n){var o=s(n);o.addClass(a.loadingClass);var l=o.attr("data-background"),d=o.attr("data-src"),h=o.attr("data-srcset"),p=o.attr("data-sizes");i.loadImage(o[0],d||l,h,p,!1,function(){if(void 0!==i&&null!==i&&i&&(!i||i.params)&&!i.destroyed){if(l?(o.css("background-image",'url("'+l+'")'),o.removeAttr("data-background")):(h&&(o.attr("srcset",h),o.removeAttr("data-srcset")),p&&(o.attr("sizes",p),o.removeAttr("data-sizes")),d&&(o.attr("src",d),o.removeAttr("data-src"))),o.addClass(a.loadedClass).removeClass(a.loadingClass),r.find("."+a.preloaderClass).remove(),i.params.loop&&t){var e=r.attr("data-swiper-slide-index");if(r.hasClass(i.params.slideDuplicateClass)){var s=i.$wrapperEl.children('[data-swiper-slide-index="'+e+'"]:not(.'+i.params.slideDuplicateClass+")");i.lazy.loadInSlide(s.index(),!1)}else{var n=i.$wrapperEl.children("."+i.params.slideDuplicateClass+'[data-swiper-slide-index="'+e+'"]');i.lazy.loadInSlide(n.index(),!1)}}i.emit("lazyImageReady",r[0],o[0])}}),i.emit("lazyImageLoad",r[0],o[0])})}},load:function(){var e=this,t=e.$wrapperEl,i=e.params,a=e.slides,r=e.activeIndex,n=e.virtual&&i.virtual.enabled,o=i.lazy,l=i.slidesPerView;function d(e){if(n){if(t.children("."+i.slideClass+'[data-swiper-slide-index="'+e+'"]').length)return!0}else if(a[e])return!0;return!1}function h(e){return n?s(e).attr("data-swiper-slide-index"):s(e).index()}if("auto"===l&&(l=0),e.lazy.initialImageLoaded||(e.lazy.initialImageLoaded=!0),e.params.watchSlidesVisibility)t.children("."+i.slideVisibleClass).each(function(t,i){var a=n?s(i).attr("data-swiper-slide-index"):s(i).index();e.lazy.loadInSlide(a)});else if(l>1)for(var p=r;p<r+l;p+=1)d(p)&&e.lazy.loadInSlide(p);else e.lazy.loadInSlide(r);if(o.loadPrevNext)if(l>1||o.loadPrevNextAmount&&o.loadPrevNextAmount>1){for(var c=o.loadPrevNextAmount,u=l,v=Math.min(r+u+Math.max(c,u),a.length),f=Math.max(r-Math.max(u,c),0),m=r+l;m<v;m+=1)d(m)&&e.lazy.loadInSlide(m);for(var g=f;g<r;g+=1)d(g)&&e.lazy.loadInSlide(g)}else{var b=t.children("."+i.slideNextClass);b.length>0&&e.lazy.loadInSlide(h(b));var w=t.children("."+i.slidePrevClass);w.length>0&&e.lazy.loadInSlide(h(w))}}},q={LinearSpline:function(e,t){var i,s,a,r,n,o=function(e,t){for(s=-1,i=e.length;i-s>1;)e[a=i+s>>1]<=t?s=a:i=a;return i};return this.x=e,this.y=t,this.lastIndex=e.length-1,this.interpolate=function(e){return e?(n=o(this.x,e),r=n-1,(e-this.x[r])*(this.y[n]-this.y[r])/(this.x[n]-this.x[r])+this.y[r]):0},this},getInterpolateFunction:function(e){this.controller.spline||(this.controller.spline=this.params.loop?new q.LinearSpline(this.slidesGrid,e.slidesGrid):new q.LinearSpline(this.snapGrid,e.snapGrid))},setTranslate:function(e,t){var i,s,a=this,r=a.controller.control;function n(e){var t=a.rtlTranslate?-a.translate:a.translate;"slide"===a.params.controller.by&&(a.controller.getInterpolateFunction(e),s=-a.controller.spline.interpolate(-t)),s&&"container"!==a.params.controller.by||(i=(e.maxTranslate()-e.minTranslate())/(a.maxTranslate()-a.minTranslate()),s=(t-a.minTranslate())*i+e.minTranslate()),a.params.controller.inverse&&(s=e.maxTranslate()-s),e.updateProgress(s),e.setTranslate(s,a),e.updateActiveIndex(),e.updateSlidesClasses()}if(Array.isArray(r))for(var o=0;o<r.length;o+=1)r[o]!==t&&r[o]instanceof k&&n(r[o]);else r instanceof k&&t!==r&&n(r)},setTransition:function(e,t){var i,s=this,a=s.controller.control;function r(t){t.setTransition(e,s),0!==e&&(t.transitionStart(),t.$wrapperEl.transitionEnd(function(){a&&(t.params.loop&&"slide"===s.params.controller.by&&t.loopFix(),t.transitionEnd())}))}if(Array.isArray(a))for(i=0;i<a.length;i+=1)a[i]!==t&&a[i]instanceof k&&r(a[i]);else a instanceof k&&t!==a&&r(a)}},j={makeElFocusable:function(e){return e.attr("tabIndex","0"),e},addElRole:function(e,t){return e.attr("role",t),e},addElLabel:function(e,t){return e.attr("aria-label",t),e},disableEl:function(e){return e.attr("aria-disabled",!0),e},enableEl:function(e){return e.attr("aria-disabled",!1),e},onEnterKey:function(e){var t=this.params.a11y;if(13===e.keyCode){var i=s(e.target);this.navigation&&this.navigation.$nextEl&&i.is(this.navigation.$nextEl)&&(this.isEnd&&!this.params.loop||this.slideNext(),this.isEnd?this.a11y.notify(t.lastSlideMessage):this.a11y.notify(t.nextSlideMessage)),this.navigation&&this.navigation.$prevEl&&i.is(this.navigation.$prevEl)&&(this.isBeginning&&!this.params.loop||this.slidePrev(),this.isBeginning?this.a11y.notify(t.firstSlideMessage):this.a11y.notify(t.prevSlideMessage)),this.pagination&&i.is("."+this.params.pagination.bulletClass)&&i[0].click()}},notify:function(e){var t=this.a11y.liveRegion;0!==t.length&&(t.html(""),t.html(e))},updateNavigation:function(){if(!this.params.loop){var e=this.navigation,t=e.$nextEl,i=e.$prevEl;i&&i.length>0&&(this.isBeginning?this.a11y.disableEl(i):this.a11y.enableEl(i)),t&&t.length>0&&(this.isEnd?this.a11y.disableEl(t):this.a11y.enableEl(t))}},updatePagination:function(){var e=this,t=e.params.a11y;e.pagination&&e.params.pagination.clickable&&e.pagination.bullets&&e.pagination.bullets.length&&e.pagination.bullets.each(function(i,a){var r=s(a);e.a11y.makeElFocusable(r),e.a11y.addElRole(r,"button"),e.a11y.addElLabel(r,t.paginationBulletMessage.replace(/{{index}}/,r.index()+1))})},init:function(){this.$el.append(this.a11y.liveRegion);var e,t,i=this.params.a11y;this.navigation&&this.navigation.$nextEl&&(e=this.navigation.$nextEl),this.navigation&&this.navigation.$prevEl&&(t=this.navigation.$prevEl),e&&(this.a11y.makeElFocusable(e),this.a11y.addElRole(e,"button"),this.a11y.addElLabel(e,i.nextSlideMessage),e.on("keydown",this.a11y.onEnterKey)),t&&(this.a11y.makeElFocusable(t),this.a11y.addElRole(t,"button"),this.a11y.addElLabel(t,i.prevSlideMessage),t.on("keydown",this.a11y.onEnterKey)),this.pagination&&this.params.pagination.clickable&&this.pagination.bullets&&this.pagination.bullets.length&&this.pagination.$el.on("keydown","."+this.params.pagination.bulletClass,this.a11y.onEnterKey)},destroy:function(){var e,t;this.a11y.liveRegion&&this.a11y.liveRegion.length>0&&this.a11y.liveRegion.remove(),this.navigation&&this.navigation.$nextEl&&(e=this.navigation.$nextEl),this.navigation&&this.navigation.$prevEl&&(t=this.navigation.$prevEl),e&&e.off("keydown",this.a11y.onEnterKey),t&&t.off("keydown",this.a11y.onEnterKey),this.pagination&&this.params.pagination.clickable&&this.pagination.bullets&&this.pagination.bullets.length&&this.pagination.$el.off("keydown","."+this.params.pagination.bulletClass,this.a11y.onEnterKey)}},K={init:function(){if(this.params.history){if(!t.history||!t.history.pushState)return this.params.history.enabled=!1,void(this.params.hashNavigation.enabled=!0);var e=this.history;e.initialized=!0,e.paths=K.getPathValues(),(e.paths.key||e.paths.value)&&(e.scrollToSlide(0,e.paths.value,this.params.runCallbacksOnInit),this.params.history.replaceState||t.addEventListener("popstate",this.history.setHistoryPopState))}},destroy:function(){this.params.history.replaceState||t.removeEventListener("popstate",this.history.setHistoryPopState)},setHistoryPopState:function(){this.history.paths=K.getPathValues(),this.history.scrollToSlide(this.params.speed,this.history.paths.value,!1)},getPathValues:function(){var e=t.location.pathname.slice(1).split("/").filter(function(e){return""!==e}),i=e.length;return{key:e[i-2],value:e[i-1]}},setHistory:function(e,i){if(this.history.initialized&&this.params.history.enabled){var s=this.slides.eq(i),a=K.slugify(s.attr("data-history"));t.location.pathname.includes(e)||(a=e+"/"+a);var r=t.history.state;r&&r.value===a||(this.params.history.replaceState?t.history.replaceState({value:a},null,a):t.history.pushState({value:a},null,a))}},slugify:function(e){return e.toString().toLowerCase().replace(/\s+/g,"-").replace(/[^\w-]+/g,"").replace(/--+/g,"-").replace(/^-+/,"").replace(/-+$/,"")},scrollToSlide:function(e,t,i){if(t)for(var s=0,a=this.slides.length;s<a;s+=1){var r=this.slides.eq(s);if(K.slugify(r.attr("data-history"))===t&&!r.hasClass(this.params.slideDuplicateClass)){var n=r.index();this.slideTo(n,e,i)}}else this.slideTo(0,e,i)}},U={onHashCange:function(){var t=e.location.hash.replace("#","");t!==this.slides.eq(this.activeIndex).attr("data-hash")&&this.slideTo(this.$wrapperEl.children("."+this.params.slideClass+'[data-hash="'+t+'"]').index())},setHash:function(){if(this.hashNavigation.initialized&&this.params.hashNavigation.enabled)if(this.params.hashNavigation.replaceState&&t.history&&t.history.replaceState)t.history.replaceState(null,null,"#"+this.slides.eq(this.activeIndex).attr("data-hash")||"");else{var i=this.slides.eq(this.activeIndex),s=i.attr("data-hash")||i.attr("data-history");e.location.hash=s||""}},init:function(){if(!(!this.params.hashNavigation.enabled||this.params.history&&this.params.history.enabled)){this.hashNavigation.initialized=!0;var i=e.location.hash.replace("#","");if(i)for(var a=0,r=this.slides.length;a<r;a+=1){var n=this.slides.eq(a);if((n.attr("data-hash")||n.attr("data-history"))===i&&!n.hasClass(this.params.slideDuplicateClass)){var o=n.index();this.slideTo(o,0,this.params.runCallbacksOnInit,!0)}}this.params.hashNavigation.watchState&&s(t).on("hashchange",this.hashNavigation.onHashCange)}},destroy:function(){this.params.hashNavigation.watchState&&s(t).off("hashchange",this.hashNavigation.onHashCange)}},_={run:function(){var e=this,t=e.slides.eq(e.activeIndex),i=e.params.autoplay.delay;t.attr("data-swiper-autoplay")&&(i=t.attr("data-swiper-autoplay")||e.params.autoplay.delay),e.autoplay.timeout=d.nextTick(function(){e.params.autoplay.reverseDirection?e.params.loop?(e.loopFix(),e.slidePrev(e.params.speed,!0,!0),e.emit("autoplay")):e.isBeginning?e.params.autoplay.stopOnLastSlide?e.autoplay.stop():(e.slideTo(e.slides.length-1,e.params.speed,!0,!0),e.emit("autoplay")):(e.slidePrev(e.params.speed,!0,!0),e.emit("autoplay")):e.params.loop?(e.loopFix(),e.slideNext(e.params.speed,!0,!0),e.emit("autoplay")):e.isEnd?e.params.autoplay.stopOnLastSlide?e.autoplay.stop():(e.slideTo(0,e.params.speed,!0,!0),e.emit("autoplay")):(e.slideNext(e.params.speed,!0,!0),e.emit("autoplay"))},i)},start:function(){return void 0===this.autoplay.timeout&&(!this.autoplay.running&&(this.autoplay.running=!0,this.emit("autoplayStart"),this.autoplay.run(),!0))},stop:function(){return!!this.autoplay.running&&(void 0!==this.autoplay.timeout&&(this.autoplay.timeout&&(clearTimeout(this.autoplay.timeout),this.autoplay.timeout=void 0),this.autoplay.running=!1,this.emit("autoplayStop"),!0))},pause:function(e){this.autoplay.running&&(this.autoplay.paused||(this.autoplay.timeout&&clearTimeout(this.autoplay.timeout),this.autoplay.paused=!0,0!==e&&this.params.autoplay.waitForTransition?(this.$wrapperEl[0].addEventListener("transitionend",this.autoplay.onTransitionEnd),this.$wrapperEl[0].addEventListener("webkitTransitionEnd",this.autoplay.onTransitionEnd)):(this.autoplay.paused=!1,this.autoplay.run())))}},Z={setTranslate:function(){for(var e=this.slides,t=0;t<e.length;t+=1){var i=this.slides.eq(t),s=-i[0].swiperSlideOffset;this.params.virtualTranslate||(s-=this.translate);var a=0;this.isHorizontal()||(a=s,s=0);var r=this.params.fadeEffect.crossFade?Math.max(1-Math.abs(i[0].progress),0):1+Math.min(Math.max(i[0].progress,-1),0);i.css({opacity:r}).transform("translate3d("+s+"px, "+a+"px, 0px)")}},setTransition:function(e){var t=this,i=t.slides,s=t.$wrapperEl;if(i.transition(e),t.params.virtualTranslate&&0!==e){var a=!1;i.transitionEnd(function(){if(!a&&t&&!t.destroyed){a=!0,t.animating=!1;for(var e=["webkitTransitionEnd","transitionend"],i=0;i<e.length;i+=1)s.trigger(e[i])}})}}},Q={setTranslate:function(){var e,t=this.$el,i=this.$wrapperEl,a=this.slides,r=this.width,n=this.height,o=this.rtlTranslate,l=this.size,d=this.params.cubeEffect,h=this.isHorizontal(),p=this.virtual&&this.params.virtual.enabled,c=0;d.shadow&&(h?(0===(e=i.find(".swiper-cube-shadow")).length&&(e=s('<div class="swiper-cube-shadow"></div>'),i.append(e)),e.css({height:r+"px"})):0===(e=t.find(".swiper-cube-shadow")).length&&(e=s('<div class="swiper-cube-shadow"></div>'),t.append(e)));for(var u=0;u<a.length;u+=1){var v=a.eq(u),f=u;p&&(f=parseInt(v.attr("data-swiper-slide-index"),10));var m=90*f,g=Math.floor(m/360);o&&(m=-m,g=Math.floor(-m/360));var b=Math.max(Math.min(v[0].progress,1),-1),w=0,y=0,x=0;f%4==0?(w=4*-g*l,x=0):(f-1)%4==0?(w=0,x=4*-g*l):(f-2)%4==0?(w=l+4*g*l,x=l):(f-3)%4==0&&(w=-l,x=3*l+4*l*g),o&&(w=-w),h||(y=w,w=0);var E="rotateX("+(h?0:-m)+"deg) rotateY("+(h?m:0)+"deg) translate3d("+w+"px, "+y+"px, "+x+"px)";if(b<=1&&b>-1&&(c=90*f+90*b,o&&(c=90*-f-90*b)),v.transform(E),d.slideShadows){var T=h?v.find(".swiper-slide-shadow-left"):v.find(".swiper-slide-shadow-top"),C=h?v.find(".swiper-slide-shadow-right"):v.find(".swiper-slide-shadow-bottom");0===T.length&&(T=s('<div class="swiper-slide-shadow-'+(h?"left":"top")+'"></div>'),v.append(T)),0===C.length&&(C=s('<div class="swiper-slide-shadow-'+(h?"right":"bottom")+'"></div>'),v.append(C)),T.length&&(T[0].style.opacity=Math.max(-b,0)),C.length&&(C[0].style.opacity=Math.max(b,0))}}if(i.css({"-webkit-transform-origin":"50% 50% -"+l/2+"px","-moz-transform-origin":"50% 50% -"+l/2+"px","-ms-transform-origin":"50% 50% -"+l/2+"px","transform-origin":"50% 50% -"+l/2+"px"}),d.shadow)if(h)e.transform("translate3d(0px, "+(r/2+d.shadowOffset)+"px, "+-r/2+"px) rotateX(90deg) rotateZ(0deg) scale("+d.shadowScale+")");else{var M=Math.abs(c)-90*Math.floor(Math.abs(c)/90),z=1.5-(Math.sin(2*M*Math.PI/360)/2+Math.cos(2*M*Math.PI/360)/2),k=d.shadowScale,P=d.shadowScale/z,$=d.shadowOffset;e.transform("scale3d("+k+", 1, "+P+") translate3d(0px, "+(n/2+$)+"px, "+-n/2/P+"px) rotateX(-90deg)")}var L=S.isSafari||S.isUiWebView?-l/2:0;i.transform("translate3d(0px,0,"+L+"px) rotateX("+(this.isHorizontal()?0:c)+"deg) rotateY("+(this.isHorizontal()?-c:0)+"deg)")},setTransition:function(e){var t=this.$el;this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),this.params.cubeEffect.shadow&&!this.isHorizontal()&&t.find(".swiper-cube-shadow").transition(e)}},J={setTranslate:function(){for(var e=this.slides,t=this.rtlTranslate,i=0;i<e.length;i+=1){var a=e.eq(i),r=a[0].progress;this.params.flipEffect.limitRotation&&(r=Math.max(Math.min(a[0].progress,1),-1));var n=-180*r,o=0,l=-a[0].swiperSlideOffset,d=0;if(this.isHorizontal()?t&&(n=-n):(d=l,l=0,o=-n,n=0),a[0].style.zIndex=-Math.abs(Math.round(r))+e.length,this.params.flipEffect.slideShadows){var h=this.isHorizontal()?a.find(".swiper-slide-shadow-left"):a.find(".swiper-slide-shadow-top"),p=this.isHorizontal()?a.find(".swiper-slide-shadow-right"):a.find(".swiper-slide-shadow-bottom");0===h.length&&(h=s('<div class="swiper-slide-shadow-'+(this.isHorizontal()?"left":"top")+'"></div>'),a.append(h)),0===p.length&&(p=s('<div class="swiper-slide-shadow-'+(this.isHorizontal()?"right":"bottom")+'"></div>'),a.append(p)),h.length&&(h[0].style.opacity=Math.max(-r,0)),p.length&&(p[0].style.opacity=Math.max(r,0))}a.transform("translate3d("+l+"px, "+d+"px, 0px) rotateX("+o+"deg) rotateY("+n+"deg)")}},setTransition:function(e){var t=this,i=t.slides,s=t.activeIndex,a=t.$wrapperEl;if(i.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),t.params.virtualTranslate&&0!==e){var r=!1;i.eq(s).transitionEnd(function(){if(!r&&t&&!t.destroyed){r=!0,t.animating=!1;for(var e=["webkitTransitionEnd","transitionend"],i=0;i<e.length;i+=1)a.trigger(e[i])}})}}},ee={setTranslate:function(){for(var e=this.width,t=this.height,i=this.slides,a=this.$wrapperEl,r=this.slidesSizesGrid,n=this.params.coverflowEffect,o=this.isHorizontal(),l=this.translate,d=o?e/2-l:t/2-l,p=o?n.rotate:-n.rotate,c=n.depth,u=0,v=i.length;u<v;u+=1){var f=i.eq(u),m=r[u],g=(d-f[0].swiperSlideOffset-m/2)/m*n.modifier,b=o?p*g:0,w=o?0:p*g,y=-c*Math.abs(g),x=o?0:n.stretch*g,E=o?n.stretch*g:0;Math.abs(E)<.001&&(E=0),Math.abs(x)<.001&&(x=0),Math.abs(y)<.001&&(y=0),Math.abs(b)<.001&&(b=0),Math.abs(w)<.001&&(w=0);var T="translate3d("+E+"px,"+x+"px,"+y+"px)  rotateX("+w+"deg) rotateY("+b+"deg)";if(f.transform(T),f[0].style.zIndex=1-Math.abs(Math.round(g)),n.slideShadows){var S=o?f.find(".swiper-slide-shadow-left"):f.find(".swiper-slide-shadow-top"),C=o?f.find(".swiper-slide-shadow-right"):f.find(".swiper-slide-shadow-bottom");0===S.length&&(S=s('<div class="swiper-slide-shadow-'+(o?"left":"top")+'"></div>'),f.append(S)),0===C.length&&(C=s('<div class="swiper-slide-shadow-'+(o?"right":"bottom")+'"></div>'),f.append(C)),S.length&&(S[0].style.opacity=g>0?g:0),C.length&&(C[0].style.opacity=-g>0?-g:0)}}(h.pointerEvents||h.prefixedPointerEvents)&&(a[0].style.perspectiveOrigin=d+"px 50%")},setTransition:function(e){this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)}},te=[P,$,L,I,O,G,N,{name:"mousewheel",params:{mousewheel:{enabled:!1,releaseOnEdges:!1,invert:!1,forceToAxis:!1,sensitivity:1,eventsTarged:"container"}},create:function(){d.extend(this,{mousewheel:{enabled:!1,enable:B.enable.bind(this),disable:B.disable.bind(this),handle:B.handle.bind(this),handleMouseEnter:B.handleMouseEnter.bind(this),handleMouseLeave:B.handleMouseLeave.bind(this),lastScrollTime:d.now()}})},on:{init:function(){this.params.mousewheel.enabled&&this.mousewheel.enable()},destroy:function(){this.mousewheel.enabled&&this.mousewheel.disable()}}},{name:"navigation",params:{navigation:{nextEl:null,prevEl:null,hideOnClick:!1,disabledClass:"swiper-button-disabled",hiddenClass:"swiper-button-hidden",lockClass:"swiper-button-lock"}},create:function(){d.extend(this,{navigation:{init:X.init.bind(this),update:X.update.bind(this),destroy:X.destroy.bind(this)}})},on:{init:function(){this.navigation.init(),this.navigation.update()},toEdge:function(){this.navigation.update()},fromEdge:function(){this.navigation.update()},destroy:function(){this.navigation.destroy()},click:function(e){var t=this.navigation,i=t.$nextEl,a=t.$prevEl;!this.params.navigation.hideOnClick||s(e.target).is(a)||s(e.target).is(i)||(i&&i.toggleClass(this.params.navigation.hiddenClass),a&&a.toggleClass(this.params.navigation.hiddenClass))}}},{name:"pagination",params:{pagination:{el:null,bulletElement:"span",clickable:!1,hideOnClick:!1,renderBullet:null,renderProgressbar:null,renderFraction:null,renderCustom:null,progressbarOpposite:!1,type:"bullets",dynamicBullets:!1,dynamicMainBullets:1,formatFractionCurrent:function(e){return e},formatFractionTotal:function(e){return e},bulletClass:"swiper-pagination-bullet",bulletActiveClass:"swiper-pagination-bullet-active",modifierClass:"swiper-pagination-",currentClass:"swiper-pagination-current",totalClass:"swiper-pagination-total",hiddenClass:"swiper-pagination-hidden",progressbarFillClass:"swiper-pagination-progressbar-fill",progressbarOppositeClass:"swiper-pagination-progressbar-opposite",clickableClass:"swiper-pagination-clickable",lockClass:"swiper-pagination-lock"}},create:function(){d.extend(this,{pagination:{init:Y.init.bind(this),render:Y.render.bind(this),update:Y.update.bind(this),destroy:Y.destroy.bind(this),dynamicBulletIndex:0}})},on:{init:function(){this.pagination.init(),this.pagination.render(),this.pagination.update()},activeIndexChange:function(){this.params.loop?this.pagination.update():void 0===this.snapIndex&&this.pagination.update()},snapIndexChange:function(){this.params.loop||this.pagination.update()},slidesLengthChange:function(){this.params.loop&&(this.pagination.render(),this.pagination.update())},snapGridLengthChange:function(){this.params.loop||(this.pagination.render(),this.pagination.update())},destroy:function(){this.pagination.destroy()},click:function(e){this.params.pagination.el&&this.params.pagination.hideOnClick&&this.pagination.$el.length>0&&!s(e.target).hasClass(this.params.pagination.bulletClass)&&this.pagination.$el.toggleClass(this.params.pagination.hiddenClass)}}},{name:"scrollbar",params:{scrollbar:{el:null,dragSize:"auto",hide:!1,draggable:!1,snapOnRelease:!0,lockClass:"swiper-scrollbar-lock",dragClass:"swiper-scrollbar-drag"}},create:function(){d.extend(this,{scrollbar:{init:V.init.bind(this),destroy:V.destroy.bind(this),updateSize:V.updateSize.bind(this),setTranslate:V.setTranslate.bind(this),setTransition:V.setTransition.bind(this),enableDraggable:V.enableDraggable.bind(this),disableDraggable:V.disableDraggable.bind(this),setDragPosition:V.setDragPosition.bind(this),onDragStart:V.onDragStart.bind(this),onDragMove:V.onDragMove.bind(this),onDragEnd:V.onDragEnd.bind(this),isTouched:!1,timeout:null,dragTimeout:null}})},on:{init:function(){this.scrollbar.init(),this.scrollbar.updateSize(),this.scrollbar.setTranslate()},update:function(){this.scrollbar.updateSize()},resize:function(){this.scrollbar.updateSize()},observerUpdate:function(){this.scrollbar.updateSize()},setTranslate:function(){this.scrollbar.setTranslate()},setTransition:function(e){this.scrollbar.setTransition(e)},destroy:function(){this.scrollbar.destroy()}}},{name:"parallax",params:{parallax:{enabled:!1}},create:function(){d.extend(this,{parallax:{setTransform:R.setTransform.bind(this),setTranslate:R.setTranslate.bind(this),setTransition:R.setTransition.bind(this)}})},on:{beforeInit:function(){this.params.parallax.enabled&&(this.params.watchSlidesProgress=!0)},init:function(){this.params.parallax&&this.parallax.setTranslate()},setTranslate:function(){this.params.parallax&&this.parallax.setTranslate()},setTransition:function(e){this.params.parallax&&this.parallax.setTransition(e)}}},{name:"zoom",params:{zoom:{enabled:!1,maxRatio:3,minRatio:1,toggle:!0,containerClass:"swiper-zoom-container",zoomedSlideClass:"swiper-slide-zoomed"}},create:function(){var e=this,t={enabled:!1,scale:1,currentScale:1,isScaling:!1,gesture:{$slideEl:void 0,slideWidth:void 0,slideHeight:void 0,$imageEl:void 0,$imageWrapEl:void 0,maxRatio:3},image:{isTouched:void 0,isMoved:void 0,currentX:void 0,currentY:void 0,minX:void 0,minY:void 0,maxX:void 0,maxY:void 0,width:void 0,height:void 0,startX:void 0,startY:void 0,touchesStart:{},touchesCurrent:{}},velocity:{x:void 0,y:void 0,prevPositionX:void 0,prevPositionY:void 0,prevTime:void 0}};"onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function(i){t[i]=F[i].bind(e)}),d.extend(e,{zoom:t})},on:{init:function(){this.params.zoom.enabled&&this.zoom.enable()},destroy:function(){this.zoom.disable()},touchStart:function(e){this.zoom.enabled&&this.zoom.onTouchStart(e)},touchEnd:function(e){this.zoom.enabled&&this.zoom.onTouchEnd(e)},doubleTap:function(e){this.params.zoom.enabled&&this.zoom.enabled&&this.params.zoom.toggle&&this.zoom.toggle(e)},transitionEnd:function(){this.zoom.enabled&&this.params.zoom.enabled&&this.zoom.onTransitionEnd()}}},{name:"lazy",params:{lazy:{enabled:!1,loadPrevNext:!1,loadPrevNextAmount:1,loadOnTransitionStart:!1,elementClass:"swiper-lazy",loadingClass:"swiper-lazy-loading",loadedClass:"swiper-lazy-loaded",preloaderClass:"swiper-lazy-preloader"}},create:function(){d.extend(this,{lazy:{initialImageLoaded:!1,load:W.load.bind(this),loadInSlide:W.loadInSlide.bind(this)}})},on:{beforeInit:function(){this.params.lazy.enabled&&this.params.preloadImages&&(this.params.preloadImages=!1)},init:function(){this.params.lazy.enabled&&!this.params.loop&&0===this.params.initialSlide&&this.lazy.load()},scroll:function(){this.params.freeMode&&!this.params.freeModeSticky&&this.lazy.load()},resize:function(){this.params.lazy.enabled&&this.lazy.load()},scrollbarDragMove:function(){this.params.lazy.enabled&&this.lazy.load()},transitionStart:function(){this.params.lazy.enabled&&(this.params.lazy.loadOnTransitionStart||!this.params.lazy.loadOnTransitionStart&&!this.lazy.initialImageLoaded)&&this.lazy.load()},transitionEnd:function(){this.params.lazy.enabled&&!this.params.lazy.loadOnTransitionStart&&this.lazy.load()}}},{name:"controller",params:{controller:{control:void 0,inverse:!1,by:"slide"}},create:function(){d.extend(this,{controller:{control:this.params.controller.control,getInterpolateFunction:q.getInterpolateFunction.bind(this),setTranslate:q.setTranslate.bind(this),setTransition:q.setTransition.bind(this)}})},on:{update:function(){this.controller.control&&this.controller.spline&&(this.controller.spline=void 0,delete this.controller.spline)},resize:function(){this.controller.control&&this.controller.spline&&(this.controller.spline=void 0,delete this.controller.spline)},observerUpdate:function(){this.controller.control&&this.controller.spline&&(this.controller.spline=void 0,delete this.controller.spline)},setTranslate:function(e,t){this.controller.control&&this.controller.setTranslate(e,t)},setTransition:function(e,t){this.controller.control&&this.controller.setTransition(e,t)}}},{name:"a11y",params:{a11y:{enabled:!0,notificationClass:"swiper-notification",prevSlideMessage:"Previous slide",nextSlideMessage:"Next slide",firstSlideMessage:"This is the first slide",lastSlideMessage:"This is the last slide",paginationBulletMessage:"Go to slide {{index}}"}},create:function(){var e=this;d.extend(e,{a11y:{liveRegion:s('<span class="'+e.params.a11y.notificationClass+'" aria-live="assertive" aria-atomic="true"></span>')}}),Object.keys(j).forEach(function(t){e.a11y[t]=j[t].bind(e)})},on:{init:function(){this.params.a11y.enabled&&(this.a11y.init(),this.a11y.updateNavigation())},toEdge:function(){this.params.a11y.enabled&&this.a11y.updateNavigation()},fromEdge:function(){this.params.a11y.enabled&&this.a11y.updateNavigation()},paginationUpdate:function(){this.params.a11y.enabled&&this.a11y.updatePagination()},destroy:function(){this.params.a11y.enabled&&this.a11y.destroy()}}},{name:"history",params:{history:{enabled:!1,replaceState:!1,key:"slides"}},create:function(){d.extend(this,{history:{init:K.init.bind(this),setHistory:K.setHistory.bind(this),setHistoryPopState:K.setHistoryPopState.bind(this),scrollToSlide:K.scrollToSlide.bind(this),destroy:K.destroy.bind(this)}})},on:{init:function(){this.params.history.enabled&&this.history.init()},destroy:function(){this.params.history.enabled&&this.history.destroy()},transitionEnd:function(){this.history.initialized&&this.history.setHistory(this.params.history.key,this.activeIndex)}}},{name:"hash-navigation",params:{hashNavigation:{enabled:!1,replaceState:!1,watchState:!1}},create:function(){d.extend(this,{hashNavigation:{initialized:!1,init:U.init.bind(this),destroy:U.destroy.bind(this),setHash:U.setHash.bind(this),onHashCange:U.onHashCange.bind(this)}})},on:{init:function(){this.params.hashNavigation.enabled&&this.hashNavigation.init()},destroy:function(){this.params.hashNavigation.enabled&&this.hashNavigation.destroy()},transitionEnd:function(){this.hashNavigation.initialized&&this.hashNavigation.setHash()}}},{name:"autoplay",params:{autoplay:{enabled:!1,delay:3e3,waitForTransition:!0,disableOnInteraction:!0,stopOnLastSlide:!1,reverseDirection:!1}},create:function(){var e=this;d.extend(e,{autoplay:{running:!1,paused:!1,run:_.run.bind(e),start:_.start.bind(e),stop:_.stop.bind(e),pause:_.pause.bind(e),onTransitionEnd:function(t){e&&!e.destroyed&&e.$wrapperEl&&t.target===this&&(e.$wrapperEl[0].removeEventListener("transitionend",e.autoplay.onTransitionEnd),e.$wrapperEl[0].removeEventListener("webkitTransitionEnd",e.autoplay.onTransitionEnd),e.autoplay.paused=!1,e.autoplay.running?e.autoplay.run():e.autoplay.stop())}}})},on:{init:function(){this.params.autoplay.enabled&&this.autoplay.start()},beforeTransitionStart:function(e,t){this.autoplay.running&&(t||!this.params.autoplay.disableOnInteraction?this.autoplay.pause(e):this.autoplay.stop())},sliderFirstMove:function(){this.autoplay.running&&(this.params.autoplay.disableOnInteraction?this.autoplay.stop():this.autoplay.pause())},destroy:function(){this.autoplay.running&&this.autoplay.stop()}}},{name:"effect-fade",params:{fadeEffect:{crossFade:!1}},create:function(){d.extend(this,{fadeEffect:{setTranslate:Z.setTranslate.bind(this),setTransition:Z.setTransition.bind(this)}})},on:{beforeInit:function(){if("fade"===this.params.effect){this.classNames.push(this.params.containerModifierClass+"fade");var e={slidesPerView:1,slidesPerColumn:1,slidesPerGroup:1,watchSlidesProgress:!0,spaceBetween:0,virtualTranslate:!0};d.extend(this.params,e),d.extend(this.originalParams,e)}},setTranslate:function(){"fade"===this.params.effect&&this.fadeEffect.setTranslate()},setTransition:function(e){"fade"===this.params.effect&&this.fadeEffect.setTransition(e)}}},{name:"effect-cube",params:{cubeEffect:{slideShadows:!0,shadow:!0,shadowOffset:20,shadowScale:.94}},create:function(){d.extend(this,{cubeEffect:{setTranslate:Q.setTranslate.bind(this),setTransition:Q.setTransition.bind(this)}})},on:{beforeInit:function(){if("cube"===this.params.effect){this.classNames.push(this.params.containerModifierClass+"cube"),this.classNames.push(this.params.containerModifierClass+"3d");var e={slidesPerView:1,slidesPerColumn:1,slidesPerGroup:1,watchSlidesProgress:!0,resistanceRatio:0,spaceBetween:0,centeredSlides:!1,virtualTranslate:!0};d.extend(this.params,e),d.extend(this.originalParams,e)}},setTranslate:function(){"cube"===this.params.effect&&this.cubeEffect.setTranslate()},setTransition:function(e){"cube"===this.params.effect&&this.cubeEffect.setTransition(e)}}},{name:"effect-flip",params:{flipEffect:{slideShadows:!0,limitRotation:!0}},create:function(){d.extend(this,{flipEffect:{setTranslate:J.setTranslate.bind(this),setTransition:J.setTransition.bind(this)}})},on:{beforeInit:function(){if("flip"===this.params.effect){this.classNames.push(this.params.containerModifierClass+"flip"),this.classNames.push(this.params.containerModifierClass+"3d");var e={slidesPerView:1,slidesPerColumn:1,slidesPerGroup:1,watchSlidesProgress:!0,spaceBetween:0,virtualTranslate:!0};d.extend(this.params,e),d.extend(this.originalParams,e)}},setTranslate:function(){"flip"===this.params.effect&&this.flipEffect.setTranslate()},setTransition:function(e){"flip"===this.params.effect&&this.flipEffect.setTransition(e)}}},{name:"effect-coverflow",params:{coverflowEffect:{rotate:50,stretch:0,depth:100,modifier:1,slideShadows:!0}},create:function(){d.extend(this,{coverflowEffect:{setTranslate:ee.setTranslate.bind(this),setTransition:ee.setTransition.bind(this)}})},on:{beforeInit:function(){"coverflow"===this.params.effect&&(this.classNames.push(this.params.containerModifierClass+"coverflow"),this.classNames.push(this.params.containerModifierClass+"3d"),this.params.watchSlidesProgress=!0,this.originalParams.watchSlidesProgress=!0)},setTranslate:function(){"coverflow"===this.params.effect&&this.coverflowEffect.setTranslate()},setTransition:function(e){"coverflow"===this.params.effect&&this.coverflowEffect.setTransition(e)}}}];return void 0===k.use&&(k.use=k.Class.use,k.installModule=k.Class.installModule),k.use(te),k});
!function(e){var t={};function n(i){if(t[i])return t[i].exports;var o=t[i]={i:i,l:!1,exports:{}};return e[i].call(o.exports,o,o.exports,n),o.l=!0,o.exports}n.m=e,n.c=t,n.d=function(e,t,i){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:i})},n.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var i=Object.create(null);if(n.r(i),Object.defineProperty(i,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var o in e)n.d(i,o,function(t){return e[t]}.bind(null,o));return i},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,"a",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p="",n(n.s=180)}({1:function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var i=navigator.userAgent;t.default={webkit:-1!==i.indexOf("AppleWebKit"),firefox:-1!==i.indexOf("Firefox"),ie:/Trident|MSIE/.test(i),edge:-1!==i.indexOf("Edge"),mac:-1!==i.indexOf("Macintosh")}},14:function(e,t,n){"use strict";e.exports=function(){var e,t=Array.prototype.slice,n={actions:{},filters:{}};function i(e,t,i,o){var s,r,a;if(n[e][t])if(i)if(s=n[e][t],o)for(a=s.length;a--;)(r=s[a]).callback===i&&r.context===o&&s.splice(a,1);else for(a=s.length;a--;)s[a].callback===i&&s.splice(a,1);else n[e][t]=[]}function o(e,t,i,o,s){var r={callback:i,priority:o,context:s},a=n[e][t];if(a){var l=!1;if(jQuery.each(a,function(){if(this.callback===i)return l=!0,!1}),l)return;a.push(r),a=function(e){for(var t,n,i,o=1,s=e.length;o<s;o++){for(t=e[o],n=o;(i=e[n-1])&&i.priority>t.priority;)e[n]=e[n-1],--n;e[n]=t}return e}(a)}else a=[r];n[e][t]=a}function s(e,t,i){var o,s,r=n[e][t];if(!r)return"filters"===e&&i[0];if(s=r.length,"filters"===e)for(o=0;o<s;o++)i[0]=r[o].callback.apply(r[o].context,i);else for(o=0;o<s;o++)r[o].callback.apply(r[o].context,i);return"filters"!==e||i[0]}return e={removeFilter:function(t,n){return"string"==typeof t&&i("filters",t,n),e},applyFilters:function(){var n=t.call(arguments),i=n.shift();return"string"==typeof i?s("filters",i,n):e},addFilter:function(t,n,i,s){return"string"==typeof t&&"function"==typeof n&&o("filters",t,n,i=parseInt(i||10,10),s),e},removeAction:function(t,n){return"string"==typeof t&&i("actions",t,n),e},doAction:function(){var n=t.call(arguments),i=n.shift();return"string"==typeof i&&s("actions",i,n),e},addAction:function(t,n,i,s){return"string"==typeof t&&"function"==typeof n&&o("actions",t,n,i=parseInt(i||10,10),s),e}}}},16:function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var i=function(){function e(e,t){for(var n=0;n<t.length;n++){var i=t[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(e,i.key,i)}}return function(t,n,i){return n&&e(t.prototype,n),i&&e(t,i),t}}();var o=function(e){function t(){return function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}(this,t),function(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}(this,(t.__proto__||Object.getPrototypeOf(t)).apply(this,arguments))}return function(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}(t,elementorModules.Module),i(t,[{key:"get",value:function(e,t){var n=((t=t||{}).session?sessionStorage:localStorage).getItem("elementor");(n=n?JSON.parse(n):{}).__expiration||(n.__expiration={});var i=n.__expiration,o=[];e?i[e]&&(o=[e]):o=Object.keys(i);var s=!1;return o.forEach(function(e){new Date(i[e])<new Date&&(delete n[e],delete i[e],s=!0)}),s&&this.save(n,t.session),e?n[e]:n}},{key:"set",value:function(e,t,n){n=n||{};var i=this.get(null,n);if(i[e]=t,n.lifetimeInSeconds){var o=new Date;o.setTime(o.getTime()+1e3*n.lifetimeInSeconds),i.__expiration[e]=o.getTime()}this.save(i,n.session)}},{key:"save",value:function(e,t){(t?sessionStorage:localStorage).setItem("elementor",JSON.stringify(e))}}]),t}();t.default=o},17:function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var i=function(){function e(e,t){for(var n=0;n<t.length;n++){var i=t[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(e,i.key,i)}}return function(t,n,i){return n&&e(t.prototype,n),i&&e(t,i),t}}(),o=function(e){return e&&e.__esModule?e:{default:e}}(n(1));var s=function(){function e(){!function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}(this,e),this.hotKeysHandlers={}}return i(e,[{key:"applyHotKey",value:function(e){var t=this.hotKeysHandlers[e.which];t&&jQuery.each(t,function(t,n){n.isWorthHandling&&!n.isWorthHandling(e)||!n.allowAltKey&&e.altKey||(e.preventDefault(),n.handle(e))})}},{key:"isControlEvent",value:function(e){return e[o.default.mac?"metaKey":"ctrlKey"]}},{key:"addHotKeyHandler",value:function(e,t,n){this.hotKeysHandlers[e]||(this.hotKeysHandlers[e]={}),this.hotKeysHandlers[e][t]=n}},{key:"bindListener",value:function(e){e.on("keydown",this.applyHotKey.bind(this))}}]),e}();t.default=s},18:function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var i=function(){function e(e,t){for(var n=0;n<t.length;n++){var i=t[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(e,i.key,i)}}return function(t,n,i){return n&&e(t.prototype,n),i&&e(t,i),t}}();var o=function(e){function t(){return function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}(this,t),function(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}(this,(t.__proto__||Object.getPrototypeOf(t)).apply(this,arguments))}return function(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}(t,elementorModules.ViewModule),i(t,[{key:"getDefaultSettings",value:function(){return{selectors:{elements:".elementor-element",nestedDocumentElements:".elementor .elementor-element"},classes:{editMode:"elementor-edit-mode"}}}},{key:"getDefaultElements",value:function(){var e=this.getSettings("selectors");return{$elements:this.$element.find(e.elements).not(this.$element.find(e.nestedDocumentElements))}}},{key:"getDocumentSettings",value:function(e){var t=void 0;if(this.isEdit){t={};var n=elementor.settings.page.model;jQuery.each(n.getActiveControls(),function(e){t[e]=n.attributes[e]})}else t=this.$element.data("elementor-settings")||{};return this.getItems(t,e)}},{key:"runElementsHandlers",value:function(){this.elements.$elements.each(function(e,t){return elementorFrontend.elementsHandler.runReadyTrigger(jQuery(t))})}},{key:"onInit",value:function(){this.$element=this.getSettings("$element"),function e(t,n,i){null===t&&(t=Function.prototype);var o=Object.getOwnPropertyDescriptor(t,n);if(void 0===o){var s=Object.getPrototypeOf(t);return null===s?void 0:e(s,n,i)}if("value"in o)return o.value;var r=o.get;return void 0!==r?r.call(i):void 0}(t.prototype.__proto__||Object.getPrototypeOf(t.prototype),"onInit",this).call(this),this.isEdit=this.$element.hasClass(this.getSettings("classes.editMode")),this.isEdit?elementor.settings.page.model.on("change",this.onSettingsChange.bind(this)):this.runElementsHandlers()}},{key:"onSettingsChange",value:function(){}}]),t}();t.default=o},180:function(e,t,n){"use strict";var i=function(){function e(e,t){for(var n=0;n<t.length;n++){var i=t[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(e,i.key,i)}}return function(t,n,i){return n&&e(t.prototype,n),i&&e(t,i),t}}(),o=l(n(181)),s=l(n(17)),r=l(n(16)),a=l(n(1));function l(e){return e&&e.__esModule?e:{default:e}}var c=n(14),u=n(182),d=n(194),h=n(195),f=n(196),m=function(e){function t(){var e;!function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}(this,t);for(var i=arguments.length,o=Array(i),s=0;s<i;s++)o[s]=arguments[s];var r=function(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(o)));return r.config=elementorFrontendConfig,r.Module=n(3),r}return function(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}(t,elementorModules.ViewModule),i(t,[{key:"getDefaultSettings",value:function(){return{selectors:{elementor:".elementor",adminBar:"#wpadminbar"},classes:{ie:"elementor-msie"}}}},{key:"getDefaultElements",value:function(){var e=this.getSettings("selectors"),t={window:window,$window:jQuery(window),$document:jQuery(document),$body:jQuery(document.body)};return t.$elementor=t.$document.find(e.elementor),t.$wpAdminBar=t.$document.find(e.adminBar),t}},{key:"bindEvents",value:function(){var e=this;this.elements.$window.on("resize",function(){return e.setDeviceModeData()})}},{key:"getElements",value:function(e){return this.getItems(this.elements,e)}},{key:"getPageSettings",value:function(e){var t=this.isEditMode()?elementor.settings.page.model.attributes:this.config.settings.page;return this.getItems(t,e)}},{key:"getGeneralSettings",value:function(e){var t=this.isEditMode()?elementor.settings.general.model.attributes:this.config.settings.general;return this.getItems(t,e)}},{key:"getCurrentDeviceMode",value:function(){return getComputedStyle(this.elements.$elementor[0],":after").content.replace(/"/g,"")}},{key:"isEditMode",value:function(){return this.config.environmentMode.edit}},{key:"isWPPreviewMode",value:function(){return this.config.environmentMode.wpPreview}},{key:"initDialogsManager",value:function(){var e=void 0;this.getDialogsManager=function(){return e||(e=new DialogsManager.Instance),e}}},{key:"initHotKeys",value:function(){this.hotKeys=new s.default,this.hotKeys.bindListener(this.elements.$window)}},{key:"initOnReadyComponents",value:function(){this.utils={youtube:new d,anchors:new h,lightbox:new f},this.modules={StretchElement:elementorModules.frontend.tools.StretchElement,Masonry:elementorModules.utils.Masonry},this.elementsHandler=new u(jQuery),this.documentsManager=new o.default,this.trigger("components:init")}},{key:"addIeCompatibility",value:function(){var e="string"==typeof document.createElement("div").style.grid;if(a.default.ie||!e){this.elements.$body.addClass(this.getSettings("classes.ie"));var t='<link rel="stylesheet" id="elementor-frontend-css-msie" href="'+this.config.urls.assets+"css/frontend-msie.min.css?"+this.config.version+'" type="text/css" />';this.elements.$body.append(t)}}},{key:"setDeviceModeData",value:function(){this.elements.$body.attr("data-elementor-device-mode",this.getCurrentDeviceMode())}},{key:"addListenerOnce",value:function(e,t,n,i){if(i||(i=this.elements.$window),this.isEditMode())if(this.removeListeners(e,t,i),i instanceof jQuery){var o=t+"."+e;i.on(o,n)}else i.on(t,n,e);else i.on(t,n)}},{key:"removeListeners",value:function(e,t,n,i){if(i||(i=this.elements.$window),i instanceof jQuery){var o=t+"."+e;i.off(o,n)}else i.off(t,n,e)}},{key:"throttle",value:function(e,t){var n=void 0,i=void 0,o=void 0,s=void 0,r=0,a=function(){r=Date.now(),n=null,s=e.apply(i,o),n||(i=o=null)};return function(){var l=Date.now(),c=t-(l-r);return i=this,o=arguments,c<=0||c>t?(n&&(clearTimeout(n),n=null),r=l,s=e.apply(i,o),n||(i=o=null)):n||(n=setTimeout(a,c)),s}}},{key:"waypoint",value:function(e,t,n){n=jQuery.extend({offset:"100%",triggerOnce:!0},n);return e.elementorWaypoint(function(){var e=this.element||this,i=t.apply(e,arguments);return n.triggerOnce&&this.destroy&&this.destroy(),i},n)}},{key:"muteMigrationTraces",value:function(){jQuery.migrateMute=!0,jQuery.migrateTrace=!1}},{key:"init",value:function(){this.hooks=new c,this.storage=new r.default,this.addIeCompatibility(),this.setDeviceModeData(),this.initDialogsManager(),this.isEditMode()&&this.muteMigrationTraces(),this.elements.$window.trigger("elementor/frontend/init"),this.isEditMode()||this.initHotKeys(),this.initOnReadyComponents()}}]),t}();window.elementorFrontend=new m,elementorFrontend.isEditMode()||jQuery(function(){return elementorFrontend.init()})},181:function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var i=function(){function e(e,t){for(var n=0;n<t.length;n++){var i=t[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(e,i.key,i)}}return function(t,n,i){return n&&e(t.prototype,n),i&&e(t,i),t}}(),o=function(e){return e&&e.__esModule?e:{default:e}}(n(18));var s=function(e){function t(){var e;!function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}(this,t);for(var n=arguments.length,i=Array(n),o=0;o<n;o++)i[o]=arguments[o];var s=function(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(i)));return s.documents={},s.initDocumentClasses(),s.attachDocumentsClasses(),s}return function(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}(t,elementorModules.ViewModule),i(t,[{key:"getDefaultSettings",value:function(){return{selectors:{document:".elementor"}}}},{key:"getDefaultElements",value:function(){var e=this.getSettings("selectors");return{$documents:jQuery(e.document)}}},{key:"initDocumentClasses",value:function(){this.documentClasses={base:o.default},elementorFrontend.hooks.doAction("elementor/frontend/documents-manager/init-classes",this)}},{key:"addDocumentClass",value:function(e,t){this.documentClasses[e]=t}},{key:"attachDocumentsClasses",value:function(){var e=this;this.elements.$documents.each(function(t,n){return e.attachDocumentClass(jQuery(n))})}},{key:"attachDocumentClass",value:function(e){var t=e.data(),n=t.elementorId,i=t.elementorType,o=this.documentClasses[i]||this.documentClasses.base;this.documents[n]=new o({$element:e,id:n})}}]),t}();t.default=s},182:function(e,t,n){"use strict";var i;i=function(e){var t=this,i={section:n(183),"accordion.default":n(184),"alert.default":n(185),"counter.default":n(186),"progress.default":n(187),"tabs.default":n(188),"toggle.default":n(189),"video.default":n(190),"image-carousel.default":n(191),"text-editor.default":n(192)};this.initHandlers=function(){elementorFrontend.hooks.addAction("frontend/element_ready/global",n(193)),e.each(i,function(e,t){elementorFrontend.hooks.addAction("frontend/element_ready/"+e,t)})},this.getHandlers=function(e){return e?i[e]:i},this.runReadyTrigger=function(t){var n=t.attr("data-element_type");n&&(t=jQuery(t),elementorFrontend.hooks.doAction("frontend/element_ready/global",t,e),-1===["section","column"].indexOf(n)&&elementorFrontend.hooks.doAction("frontend/element_ready/widget",t,e),elementorFrontend.hooks.doAction("frontend/element_ready/"+n,t,e))},t.initHandlers()},e.exports=i},183:function(e,t,n){"use strict";var i=n(3),o=i.extend({player:null,isYTVideo:null,getDefaultSettings:function(){return{selectors:{backgroundVideoContainer:".elementor-background-video-container",backgroundVideoEmbed:".elementor-background-video-embed",backgroundVideoHosted:".elementor-background-video-hosted"}}},getDefaultElements:function(){var e=this.getSettings("selectors"),t={$backgroundVideoContainer:this.$element.find(e.backgroundVideoContainer)};return t.$backgroundVideoEmbed=t.$backgroundVideoContainer.children(e.backgroundVideoEmbed),t.$backgroundVideoHosted=t.$backgroundVideoContainer.children(e.backgroundVideoHosted),t},calcVideosSize:function(){var e=this.elements.$backgroundVideoContainer.outerWidth(),t=this.elements.$backgroundVideoContainer.outerHeight(),n="16:9".split(":"),i=n[0]/n[1],o=e/t>i;return{width:o?e:t*i,height:o?e/i:t}},changeVideoSize:function(){var e=this.isYTVideo?jQuery(this.player.getIframe()):this.elements.$backgroundVideoHosted,t=this.calcVideosSize();e.width(t.width).height(t.height)},startVideoLoop:function(){var e=this;if(e.player.getIframe().contentWindow){var t=e.getElementSettings(),n=t.background_video_start||0,i=t.background_video_end;if(e.player.seekTo(n),i)setTimeout(function(){e.startVideoLoop()},1e3*(i-n+1))}},prepareYTVideo:function(e,t){var n=this,i=n.elements.$backgroundVideoContainer,o=n.getElementSettings(),s=e.PlayerState.PLAYING;window.chrome&&(s=e.PlayerState.UNSTARTED),i.addClass("elementor-loading elementor-invisible"),n.player=new e.Player(n.elements.$backgroundVideoEmbed[0],{videoId:t,events:{onReady:function(){n.player.mute(),n.changeVideoSize(),n.startVideoLoop(),n.player.playVideo()},onStateChange:function(t){switch(t.data){case s:i.removeClass("elementor-invisible elementor-loading");break;case e.PlayerState.ENDED:n.player.seekTo(o.background_video_start||0)}}},playerVars:{controls:0,rel:0}}),elementorFrontend.elements.$window.on("resize",n.changeVideoSize)},activate:function(){var e=this,t=e.getElementSettings("background_video_link"),n=elementorFrontend.utils.youtube.getYoutubeIDFromURL(t);e.isYTVideo=!!n,n?elementorFrontend.utils.youtube.onYoutubeApiReady(function(t){setTimeout(function(){e.prepareYTVideo(t,n)},1)}):e.elements.$backgroundVideoHosted.attr("src",t).one("canplay",e.changeVideoSize)},deactivate:function(){this.isYTVideo&&this.player.getIframe()?this.player.destroy():this.elements.$backgroundVideoHosted.removeAttr("src")},run:function(){var e=this.getElementSettings();"video"===e.background_background&&e.background_video_link?this.activate():this.deactivate()},onInit:function(){i.prototype.onInit.apply(this,arguments),this.run()},onElementChange:function(e){"background_background"===e&&this.run()}}),s=i.extend({stretchElement:null,bindEvents:function(){var e=this.getUniqueHandlerID();elementorFrontend.addListenerOnce(e,"resize",this.stretch),elementorFrontend.addListenerOnce(e,"sticky:stick",this.stretch,this.$element),elementorFrontend.addListenerOnce(e,"sticky:unstick",this.stretch,this.$element)},unbindEvents:function(){elementorFrontend.removeListeners(this.getUniqueHandlerID(),"resize",this.stretch)},initStretch:function(){this.stretchElement=new elementorModules.frontend.tools.StretchElement({element:this.$element,selectors:{container:this.getStretchContainer()}})},getStretchContainer:function(){return elementorFrontend.getGeneralSettings("elementor_stretched_section_container")||window},stretch:function(){this.getElementSettings("stretch_section")&&this.stretchElement.stretch()},onInit:function(){i.prototype.onInit.apply(this,arguments),this.initStretch(),this.stretch()},onElementChange:function(e){"stretch_section"===e&&(this.getElementSettings("stretch_section")?this.stretch():this.stretchElement.reset())},onGeneralSettingsChange:function(e){"elementor_stretched_section_container"in e&&(this.stretchElement.setSettings("selectors.container",this.getStretchContainer()),this.stretch())}}),r=i.extend({getDefaultSettings:function(){return{selectors:{container:"> .elementor-shape-%s"},svgURL:elementorFrontend.config.urls.assets+"shapes/"}},getDefaultElements:function(){var e={},t=this.getSettings("selectors");return e.$topContainer=this.$element.find(t.container.replace("%s","top")),e.$bottomContainer=this.$element.find(t.container.replace("%s","bottom")),e},buildSVG:function(e){var t="shape_divider_"+e,n=this.getElementSettings(t),i=this.elements["$"+e+"Container"];if(i.empty().attr("data-shape",n),n){var o=n;this.getElementSettings(t+"_negative")&&(o+="-negative");var s=this.getSettings("svgURL")+o+".svg";jQuery.get(s,function(e){i.append(e.childNodes[0])}),this.setNegative(e)}},setNegative:function(e){this.elements["$"+e+"Container"].attr("data-negative",!!this.getElementSettings("shape_divider_"+e+"_negative"))},onInit:function(){var e=this;i.prototype.onInit.apply(e,arguments),["top","bottom"].forEach(function(t){e.getElementSettings("shape_divider_"+t)&&e.buildSVG(t)})},onElementChange:function(e){var t=e.match(/^shape_divider_(top|bottom)$/);if(t)this.buildSVG(t[1]);else{var n=e.match(/^shape_divider_(top|bottom)_negative$/);n&&(this.buildSVG(n[1]),this.setNegative(n[1]))}}}),a=i.extend({isFirst:function(){return this.$element.is(".elementor-edit-mode .elementor-top-section:first")},getOffset:function(){if("body"===elementor.config.document.container)return this.$element.offset().top;var e=jQuery(elementor.config.document.container);return this.$element.offset().top-e.offset().top},setHandlesPosition:function(){if(this.isFirst()){var e=this.getOffset(),t=this.$element.find("> .elementor-element-overlay > .elementor-editor-section-settings");e<25?(this.$element.addClass("elementor-section--handles-inside"),e<-5?t.css("top",-e):t.css("top","")):this.$element.removeClass("elementor-section--handles-inside")}},onInit:function(){this.setHandlesPosition(),this.$element.on("mouseenter",this.setHandlesPosition)}});e.exports=function(e){(elementorFrontend.isEditMode()||e.hasClass("elementor-section-stretched"))&&new s({$element:e}),elementorFrontend.isEditMode()&&(new r({$element:e}),new a({$element:e})),new o({$element:e})}},184:function(e,t,n){"use strict";var i=n(19);e.exports=function(e){new i({$element:e,showTabFn:"slideDown",hideTabFn:"slideUp"})}},185:function(e,t,n){"use strict";e.exports=function(e,t){e.find(".elementor-alert-dismiss").on("click",function(){t(this).parent().fadeOut()})}},186:function(e,t,n){"use strict";e.exports=function(e,t){elementorFrontend.waypoint(e.find(".elementor-counter-number"),function(){var e=t(this),n=e.data(),i=n.toValue.toString().match(/\.(.*)/);i&&(n.rounding=i[1].length),e.numerator(n)})}},187:function(e,t,n){"use strict";e.exports=function(e,t){elementorFrontend.waypoint(e.find(".elementor-progress-bar"),function(){var e=t(this);e.css("width",e.data("max")+"%")})}},188:function(e,t,n){"use strict";var i=n(19);e.exports=function(e){new i({$element:e,toggleSelf:!1})}},189:function(e,t,n){"use strict";var i=n(19);e.exports=function(e){new i({$element:e,showTabFn:"slideDown",hideTabFn:"slideUp",hidePrevious:!1,autoExpand:"editor"})}},19:function(e,t,n){"use strict";var i=n(3);e.exports=i.extend({$activeContent:null,getDefaultSettings:function(){return{selectors:{tabTitle:".elementor-tab-title",tabContent:".elementor-tab-content"},classes:{active:"elementor-active"},showTabFn:"show",hideTabFn:"hide",toggleSelf:!0,hidePrevious:!0,autoExpand:!0}},getDefaultElements:function(){var e=this.getSettings("selectors");return{$tabTitles:this.findElement(e.tabTitle),$tabContents:this.findElement(e.tabContent)}},activateDefaultTab:function(){var e=this.getSettings();if(e.autoExpand&&("editor"!==e.autoExpand||this.isEdit)){var t=this.getEditSettings("activeItemIndex")||1,n={showTabFn:e.showTabFn,hideTabFn:e.hideTabFn};this.setSettings({showTabFn:"show",hideTabFn:"hide"}),this.changeActiveTab(t),this.setSettings(n)}},deactivateActiveTab:function(e){var t=this.getSettings(),n=t.classes.active,i=e?'[data-tab="'+e+'"]':"."+n,o=this.elements.$tabTitles.filter(i),s=this.elements.$tabContents.filter(i);o.add(s).removeClass(n),s[t.hideTabFn]()},activateTab:function(e){var t=this.getSettings(),n=t.classes.active,i=this.elements.$tabTitles.filter('[data-tab="'+e+'"]'),o=this.elements.$tabContents.filter('[data-tab="'+e+'"]');i.add(o).addClass(n),o[t.showTabFn]()},isActiveTab:function(e){return this.elements.$tabTitles.filter('[data-tab="'+e+'"]').hasClass(this.getSettings("classes.active"))},bindEvents:function(){var e=this;this.elements.$tabTitles.on({keydown:function(t){"Enter"===t.key&&(t.preventDefault(),e.changeActiveTab(t.currentTarget.dataset.tab))},click:function(t){t.preventDefault(),e.changeActiveTab(t.currentTarget.dataset.tab)}})},onInit:function(){i.prototype.onInit.apply(this,arguments),this.activateDefaultTab()},onEditSettingsChange:function(e){"activeItemIndex"===e&&this.activateDefaultTab()},changeActiveTab:function(e){var t=this.isActiveTab(e),n=this.getSettings();!n.toggleSelf&&t||!n.hidePrevious||this.deactivateActiveTab(),!n.hidePrevious&&t&&this.deactivateActiveTab(e),t||this.activateTab(e)}})},190:function(e,t,n){"use strict";var i,o=n(3);i=o.extend({getDefaultSettings:function(){return{selectors:{imageOverlay:".elementor-custom-embed-image-overlay",video:".elementor-video",videoIframe:".elementor-video-iframe"}}},getDefaultElements:function(){var e=this.getSettings("selectors");return{$imageOverlay:this.$element.find(e.imageOverlay),$video:this.$element.find(e.video),$videoIframe:this.$element.find(e.videoIframe)}},getLightBox:function(){return elementorFrontend.utils.lightbox},handleVideo:function(){this.getElementSettings("lightbox")||(this.elements.$imageOverlay.remove(),this.playVideo())},playVideo:function(){if(this.elements.$video.length)this.elements.$video[0].play();else{var e=this.elements.$videoIframe,t=e.data("lazy-load");t&&e.attr("src",t);var n=e[0].src.replace("&autoplay=0","");e[0].src=n+"&autoplay=1"}},animateVideo:function(){this.getLightBox().setEntranceAnimation(this.getElementSettings("lightbox_content_animation"))},handleAspectRatio:function(){this.getLightBox().setVideoAspectRatio(this.getElementSettings("aspect_ratio"))},bindEvents:function(){this.elements.$imageOverlay.on("click",this.handleVideo)},onElementChange:function(e){if("lightbox_content_animation"!==e){var t=this.getElementSettings("lightbox");"lightbox"!==e||t?"aspect_ratio"===e&&t&&this.handleAspectRatio():this.getLightBox().getModal().hide()}else this.animateVideo()}}),e.exports=function(e){new i({$element:e})}},191:function(e,t,n){"use strict";var i,o=n(3);i=o.extend({getDefaultSettings:function(){return{selectors:{carousel:".elementor-image-carousel"}}},getDefaultElements:function(){var e=this.getSettings("selectors");return{$carousel:this.$element.find(e.carousel)}},onInit:function(){o.prototype.onInit.apply(this,arguments);var e=this.getElementSettings(),t=+e.slides_to_show||3,n=1===t,i=n?1:2,s=elementorFrontend.config.breakpoints,r={slidesToShow:t,autoplay:"yes"===e.autoplay,autoplaySpeed:e.autoplay_speed,infinite:"yes"===e.infinite,pauseOnHover:"yes"===e.pause_on_hover,speed:e.speed,arrows:-1!==["arrows","both"].indexOf(e.navigation),dots:-1!==["dots","both"].indexOf(e.navigation),rtl:"rtl"===e.direction,responsive:[{breakpoint:s.lg,settings:{slidesToShow:+e.slides_to_show_tablet||i,slidesToScroll:+e.slides_to_scroll_tablet||i}},{breakpoint:s.md,settings:{slidesToShow:+e.slides_to_show_mobile||1,slidesToScroll:+e.slides_to_scroll_mobile||1}}]};n?r.fade="fade"===e.effect:r.slidesToScroll=+e.slides_to_scroll||i,this.elements.$carousel.slick(r)}}),e.exports=function(e){new i({$element:e})}},192:function(e,t,n){"use strict";var i,o=n(3);i=o.extend({dropCapLetter:"",getDefaultSettings:function(){return{selectors:{paragraph:"p:first"},classes:{dropCap:"elementor-drop-cap",dropCapLetter:"elementor-drop-cap-letter"}}},getDefaultElements:function(){var e=this.getSettings("selectors"),t=this.getSettings("classes"),n=jQuery("<span>",{class:t.dropCap}),i=jQuery("<span>",{class:t.dropCapLetter});return n.append(i),{$paragraph:this.$element.find(e.paragraph),$dropCap:n,$dropCapLetter:i}},getElementName:function(){return"text-editor"},wrapDropCap:function(){if(this.getElementSettings("drop_cap")){var e=this.elements.$paragraph;if(e.length){var t=e.html().replace(/&nbsp;/g," "),n=t.match(/^ *([^ ] ?)/);if(n){var i=n[1],o=i.trim();if("<"!==o){this.dropCapLetter=i,this.elements.$dropCapLetter.text(o);var s=t.slice(i.length).replace(/^ */,function(e){return new Array(e.length+1).join("&nbsp;")});e.html(s).prepend(this.elements.$dropCap)}}}}else this.dropCapLetter&&(this.elements.$dropCap.remove(),this.elements.$paragraph.prepend(this.dropCapLetter),this.dropCapLetter="")},onInit:function(){o.prototype.onInit.apply(this,arguments),this.wrapDropCap()},onElementChange:function(e){"drop_cap"===e&&this.wrapDropCap()}}),e.exports=function(e){new i({$element:e})}},193:function(e,t,n){"use strict";var i,o=n(3);i=o.extend({getElementName:function(){return"global"},animate:function(){var e=this.$element,t=this.getAnimation(),n=this.getElementSettings(),i=n._animation_delay||n.animation_delay||0;e.removeClass(t),setTimeout(function(){e.removeClass("elementor-invisible").addClass(t)},i)},getAnimation:function(){var e=this.getElementSettings();return e.animation||e._animation},onInit:function(){o.prototype.onInit.apply(this,arguments);var e=this.getAnimation();e&&(this.$element.removeClass(e),elementorFrontend.waypoint(this.$element,this.animate.bind(this)))},onElementChange:function(e){/^_?animation/.test(e)&&this.animate()}}),e.exports=function(e){new i({$element:e})}},194:function(e,t,n){"use strict";e.exports=elementorModules.ViewModule.extend({getDefaultSettings:function(){return{isInserted:!1,APISrc:"https://www.youtube.com/iframe_api",selectors:{firstScript:"script:first"}}},getDefaultElements:function(){return{$firstScript:jQuery(this.getSettings("selectors.firstScript"))}},insertYTAPI:function(){this.setSettings("isInserted",!0),this.elements.$firstScript.before(jQuery("<script>",{src:this.getSettings("APISrc")}))},onYoutubeApiReady:function(e){var t=this;t.getSettings("IsInserted")||t.insertYTAPI(),window.YT&&YT.loaded?e(YT):setTimeout(function(){t.onYoutubeApiReady(e)},350)},getYoutubeIDFromURL:function(e){var t=e.match(/^(?:https?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?vi?=|(?:embed|v|vi|user)\/))([^?&"'>]+)/);return t&&t[1]}})},195:function(e,t,n){"use strict";e.exports=elementorModules.ViewModule.extend({getDefaultSettings:function(){return{scrollDuration:500,selectors:{links:'a[href*="#"]',targets:".elementor-element, .elementor-menu-anchor",scrollable:"html, body"}}},getDefaultElements:function(){return{$scrollable:jQuery(this.getSettings("selectors").scrollable)}},bindEvents:function(){elementorFrontend.elements.$document.on("click",this.getSettings("selectors.links"),this.handleAnchorLinks)},handleAnchorLinks:function(e){var t,n=e.currentTarget,i=location.pathname===n.pathname;if(location.hostname===n.hostname&&i&&!(n.hash.length<2)){try{t=jQuery(n.hash).filter(this.getSettings("selectors.targets"))}catch(e){return}if(t.length){var o=t.offset().top,s=elementorFrontend.elements.$wpAdminBar,r=jQuery(".elementor-sticky--active");s.length>0&&(o-=s.height()),r.length>0&&(o-=Math.max.apply(null,r.map(function(){return jQuery(this).outerHeight()}).get())),e.preventDefault(),o=elementorFrontend.hooks.applyFilters("frontend/handlers/menu_anchor/scroll_top_distance",o),this.elements.$scrollable.animate({scrollTop:o},this.getSettings("scrollDuration"),"linear")}}},onInit:function(){elementorModules.ViewModule.prototype.onInit.apply(this,arguments),this.bindEvents()}})},196:function(e,t,n){"use strict";e.exports=elementorModules.ViewModule.extend({oldAspectRatio:null,oldAnimation:null,swiper:null,getDefaultSettings:function(){return{classes:{aspectRatio:"elementor-aspect-ratio-%s",item:"elementor-lightbox-item",image:"elementor-lightbox-image",videoContainer:"elementor-video-container",videoWrapper:"elementor-fit-aspect-ratio",playButton:"elementor-custom-embed-play",playButtonIcon:"fa",playing:"elementor-playing",hidden:"elementor-hidden",invisible:"elementor-invisible",preventClose:"elementor-lightbox-prevent-close",slideshow:{container:"swiper-container",slidesWrapper:"swiper-wrapper",prevButton:"elementor-swiper-button elementor-swiper-button-prev",nextButton:"elementor-swiper-button elementor-swiper-button-next",prevButtonIcon:"eicon-chevron-left",nextButtonIcon:"eicon-chevron-right",slide:"swiper-slide"}},selectors:{links:"a, [data-elementor-lightbox]",slideshow:{activeSlide:".swiper-slide-active",prevSlide:".swiper-slide-prev",nextSlide:".swiper-slide-next"}},modalOptions:{id:"elementor-lightbox",entranceAnimation:"zoomIn",videoAspectRatio:169,position:{enable:!1}}}},getModal:function(){return e.exports.modal||this.initModal(),e.exports.modal},initModal:function(){var t=e.exports.modal=elementorFrontend.getDialogsManager().createWidget("lightbox",{className:"elementor-lightbox",closeButton:!0,closeButtonClass:"eicon-close",selectors:{preventClose:"."+this.getSettings("classes.preventClose")},hide:{onClick:!0}});t.on("hide",function(){t.setMessage("")})},showModal:function(e){var t=this,n=t.getDefaultSettings().modalOptions;t.setSettings("modalOptions",jQuery.extend(n,e.modalOptions));var i=t.getModal();switch(i.setID(t.getSettings("modalOptions.id")),i.onShow=function(){DialogsManager.getWidgetType("lightbox").prototype.onShow.apply(i,arguments),setTimeout(function(){t.setEntranceAnimation()},10)},i.onHide=function(){DialogsManager.getWidgetType("lightbox").prototype.onHide.apply(i,arguments),i.getElements("message").removeClass("animated")},e.type){case"image":t.setImageContent(e.url);break;case"video":t.setVideoContent(e);break;case"slideshow":t.setSlideshowContent(e.slideshow);break;default:t.setHTMLContent(e.html)}i.show()},setHTMLContent:function(e){this.getModal().setMessage(e)},setImageContent:function(e){var t=this.getSettings("classes"),n=jQuery("<div>",{class:t.item}),i=jQuery("<img>",{src:e,class:t.image+" "+t.preventClose});n.append(i),this.getModal().setMessage(n)},setVideoContent:function(e){var t,n=this.getSettings("classes"),i=jQuery("<div>",{class:n.videoContainer}),o=jQuery("<div>",{class:n.videoWrapper}),s=this.getModal();if("hosted"===e.videoType){var r=jQuery.extend({src:e.url,autoplay:""},e.videoParams);t=jQuery("<video>",r)}else{var a=e.url.replace("&autoplay=0","")+"&autoplay=1";t=jQuery("<iframe>",{src:a,allowfullscreen:1})}i.append(o),o.append(t),s.setMessage(i),this.setVideoAspectRatio();var l=s.onHide;s.onHide=function(){l(),s.getElements("message").removeClass("elementor-fit-aspect-ratio")}},setSlideshowContent:function(e){var t=jQuery,n=this,i=n.getSettings("classes"),o=i.slideshow,s=t("<div>",{class:o.container}),r=t("<div>",{class:o.slidesWrapper}),a=t("<div>",{class:o.prevButton+" "+i.preventClose}).html(t("<i>",{class:o.prevButtonIcon})),l=t("<div>",{class:o.nextButton+" "+i.preventClose}).html(t("<i>",{class:o.nextButtonIcon}));e.slides.forEach(function(e){var n=o.slide+" "+i.item;e.video&&(n+=" "+i.video);var s=t("<div>",{class:n});if(e.video){s.attr("data-elementor-slideshow-video",e.video);var a=t("<div>",{class:i.playButton}).html(t("<i>",{class:i.playButtonIcon}));s.append(a)}else{var l=t("<div>",{class:"swiper-zoom-container"}),c=t("<img>",{class:i.image+" "+i.preventClose,src:e.image});l.append(c),s.append(l)}r.append(s)}),s.append(r,a,l);var c=n.getModal();c.setMessage(s);var u=c.onShow;c.onShow=function(){u();var i={navigation:{prevEl:a,nextEl:l},pagination:{clickable:!0},on:{slideChangeTransitionEnd:n.onSlideChange},grabCursor:!0,runCallbacksOnInit:!1,loop:!0,keyboard:!0};e.swiper&&t.extend(i,e.swiper),n.swiper=new Swiper(s,i),n.setVideoAspectRatio(),n.playSlideVideo()}},setVideoAspectRatio:function(e){e=e||this.getSettings("modalOptions.videoAspectRatio");var t=this.getModal().getElements("widgetContent"),n=this.oldAspectRatio,i=this.getSettings("classes.aspectRatio");this.oldAspectRatio=e,n&&t.removeClass(i.replace("%s",n)),e&&t.addClass(i.replace("%s",e))},getSlide:function(e){return jQuery(this.swiper.slides).filter(this.getSettings("selectors.slideshow."+e+"Slide"))},playSlideVideo:function(){var e=this.getSlide("active"),t=e.data("elementor-slideshow-video");if(t){var n=this.getSettings("classes"),i=jQuery("<div>",{class:n.videoContainer+" "+n.invisible}),o=jQuery("<div>",{class:n.videoWrapper}),s=jQuery("<iframe>",{src:t}),r=e.children("."+n.playButton);i.append(o),o.append(s),e.append(i),r.addClass(n.playing).removeClass(n.hidden),s.on("load",function(){r.addClass(n.hidden),i.removeClass(n.invisible)})}},setEntranceAnimation:function(e){e=e||this.getSettings("modalOptions.entranceAnimation");var t=this.getModal().getElements("message");this.oldAnimation&&t.removeClass(this.oldAnimation),this.oldAnimation=e,e&&t.addClass("animated "+e)},isLightboxLink:function(e){if("A"===e.tagName&&(e.hasAttribute("download")||!/\.(png|jpe?g|gif|svg)(\?.*)?$/i.test(e.href)))return!1;var t=elementorFrontend.getGeneralSettings("elementor_global_image_lightbox"),n=e.dataset.elementorOpenLightbox;return"yes"===n||t&&"no"!==n},openLink:function(e){var t=e.currentTarget,n=jQuery(e.target),i=elementorFrontend.isEditMode(),o=!!n.closest("#elementor").length;if(this.isLightboxLink(t)){if(e.preventDefault(),!i||elementorFrontend.getGeneralSettings("elementor_enable_lightbox_in_editor")){var s={};if(t.dataset.elementorLightbox&&(s=JSON.parse(t.dataset.elementorLightbox)),s.type&&"slideshow"!==s.type)this.showModal(s);else if(t.dataset.elementorLightboxSlideshow){var r=t.dataset.elementorLightboxSlideshow,a=jQuery(this.getSettings("selectors.links")).filter(function(){return r===this.dataset.elementorLightboxSlideshow}),l=[],c={};a.each(function(){var e=this.dataset.elementorLightboxVideo,t=e||this.href;if(!c[t]){c[t]=!0;var n=this.dataset.elementorLightboxIndex;void 0===n&&(n=a.index(this));var i={image:this.href,index:n};e&&(i.video=e),l.push(i)}}),l.sort(function(e,t){return e.index-t.index});var u=t.dataset.elementorLightboxIndex;void 0===u&&(u=a.index(t)),this.showModal({type:"slideshow",modalOptions:{id:"elementor-lightbox-slideshow-"+r},slideshow:{slides:l,swiper:{initialSlide:+u}}})}else this.showModal({type:"image",url:t.href})}}else i&&o&&e.preventDefault()},bindEvents:function(){elementorFrontend.elements.$document.on("click",this.getSettings("selectors.links"),this.openLink)},onInit:function(){elementorModules.ViewModule.prototype.onInit.apply(this,arguments),elementorFrontend.isEditMode()&&elementor.settings.general.model.on("change",this.onGeneralSettingsChange)},onGeneralSettingsChange:function(e){"elementor_lightbox_content_animation"in e.changed&&(this.setSettings("modalOptions.entranceAnimation",e.changed.elementor_lightbox_content_animation),this.setEntranceAnimation())},onSlideChange:function(){this.getSlide("prev").add(this.getSlide("next")).add(this.getSlide("active")).find("."+this.getSettings("classes.videoWrapper")).remove(),this.playSlideVideo()}})},3:function(e,t,n){"use strict";e.exports=elementorModules.ViewModule.extend({$element:null,editorListeners:null,onElementChange:null,onEditSettingsChange:null,onGeneralSettingsChange:null,onPageSettingsChange:null,isEdit:null,__construct:function(e){this.$element=e.$element,this.isEdit=this.$element.hasClass("elementor-element-edit-mode"),this.isEdit&&this.addEditorListeners()},findElement:function(e){var t=this.$element;return t.find(e).filter(function(){return jQuery(this).closest(".elementor-element").is(t)})},getUniqueHandlerID:function(e,t){return e||(e=this.getModelCID()),t||(t=this.$element),e+t.attr("data-element_type")+this.getConstructorID()},initEditorListeners:function(){var e=this;if(e.editorListeners=[{event:"element:destroy",to:elementor.channels.data,callback:function(t){t.cid===e.getModelCID()&&e.onDestroy()}}],e.onElementChange){var t=e.getElementName(),n="change";"global"!==t&&(n+=":"+t),e.editorListeners.push({event:n,to:elementor.channels.editor,callback:function(t,n){e.getUniqueHandlerID(n.model.cid,n.$el)===e.getUniqueHandlerID()&&e.onElementChange(t.model.get("name"),t,n)}})}e.onEditSettingsChange&&e.editorListeners.push({event:"change:editSettings",to:elementor.channels.editor,callback:function(t,n){n.model.cid===e.getModelCID()&&e.onEditSettingsChange(Object.keys(t.changed)[0])}}),["page","general"].forEach(function(t){var n="on"+t[0].toUpperCase()+t.slice(1)+"SettingsChange";e[n]&&e.editorListeners.push({event:"change",to:elementor.settings[t].model,callback:function(t){e[n](t.changed)}})})},getEditorListeners:function(){return this.editorListeners||this.initEditorListeners(),this.editorListeners},addEditorListeners:function(){var e=this.getUniqueHandlerID();this.getEditorListeners().forEach(function(t){elementorFrontend.addListenerOnce(e,t.event,t.callback,t.to)})},removeEditorListeners:function(){var e=this.getUniqueHandlerID();this.getEditorListeners().forEach(function(t){elementorFrontend.removeListeners(e,t.event,null,t.to)})},getElementName:function(){return this.$element.data("element_type").split(".")[0]},getID:function(){return this.$element.data("id")},getModelCID:function(){return this.$element.data("model-cid")},getElementSettings:function(e){var t={},n=this.getModelCID();if(this.isEdit&&n){var i=elementorFrontend.config.elements.data[n],o=elementorFrontend.config.elements.keys[i.attributes.widgetType||i.attributes.elType];jQuery.each(i.getActiveControls(),function(e){-1!==o.indexOf(e)&&(t[e]=i.attributes[e])})}else t=this.$element.data("settings")||{};return this.getItems(t,e)},getEditSettings:function(e){var t={};return this.isEdit&&(t=elementorFrontend.config.elements.editSettings[this.getModelCID()].attributes),this.getItems(t,e)},onDestroy:function(){this.removeEditorListeners(),this.unbindEvents&&this.unbindEvents()}})}});