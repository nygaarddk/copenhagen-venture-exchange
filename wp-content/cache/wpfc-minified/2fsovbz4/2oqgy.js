if(typeof(php_data.ac_settings.site_tracking)!="undefined"&&php_data.ac_settings.site_tracking=="1"){
var trackByDefault=php_data.ac_settings.site_tracking_default;
function acEnableTracking(){
var expiration=new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30);
document.cookie="ac_enable_tracking=1; expires=" + expiration + "; path=/";
acTrackVisit();
}
function acTrackVisit(){
var trackcmp_email=php_data.user_email;
var trackcmp=document.createElement("script");
trackcmp.async=true;
trackcmp.type='text/javascript';
trackcmp.src='//trackcmp.net/visit?actid=' + php_data.ac_settings.tracking_actid + '&e=' + encodeURIComponent(trackcmp_email) + '&r=' + encodeURIComponent(document.referrer) + '&u=' + encodeURIComponent(window.location.href);
var trackcmp_s=document.getElementsByTagName("script");
if(trackcmp_s.length){
trackcmp_s[0].parentNode.appendChild(trackcmp);
}else{
var trackcmp_h=document.getElementsByTagName("head");
trackcmp_h.length&&trackcmp_h[0].appendChild(trackcmp);
}}
if(trackByDefault||/(^|;)ac_enable_tracking=([^;]+)/.test(document.cookie)){
acEnableTracking();
}};
(function($){
'use strict';
if(typeof wpcf7==='undefined'||wpcf7===null){
return;
}
wpcf7=$.extend({
cached: 0,
inputs: []
}, wpcf7);
$(function(){
wpcf7.supportHtml5=(function(){
var features={};
var input=document.createElement('input');
features.placeholder='placeholder' in input;
var inputTypes=[ 'email', 'url', 'tel', 'number', 'range', 'date' ];
$.each(inputTypes, function(index, value){
input.setAttribute('type', value);
features[ value ]=input.type!=='text';
});
return features;
})();
$('div.wpcf7 > form').each(function(){
var $form=$(this);
wpcf7.initForm($form);
if(wpcf7.cached){
wpcf7.refill($form);
}});
});
wpcf7.getId=function(form){
return parseInt($('input[name="_wpcf7"]', form).val(), 10);
};
wpcf7.initForm=function(form){
var $form=$(form);
$form.submit(function(event){
if(! wpcf7.supportHtml5.placeholder){
$('[placeholder].placeheld', $form).each(function(i, n){
$(n).val('').removeClass('placeheld');
});
}
if(typeof window.FormData==='function'){
wpcf7.submit($form);
event.preventDefault();
}});
$('.wpcf7-submit', $form).after('<span class="ajax-loader"></span>');
wpcf7.toggleSubmit($form);
$form.on('click', '.wpcf7-acceptance', function(){
wpcf7.toggleSubmit($form);
});
$('.wpcf7-exclusive-checkbox', $form).on('click', 'input:checkbox', function(){
var name=$(this).attr('name');
$form.find('input:checkbox[name="' + name + '"]').not(this).prop('checked', false);
});
$('.wpcf7-list-item.has-free-text', $form).each(function(){
var $freetext=$(':input.wpcf7-free-text', this);
var $wrap=$(this).closest('.wpcf7-form-control');
if($(':checkbox, :radio', this).is(':checked')){
$freetext.prop('disabled', false);
}else{
$freetext.prop('disabled', true);
}
$wrap.on('change', ':checkbox, :radio', function(){
var $cb=$('.has-free-text', $wrap).find(':checkbox, :radio');
if($cb.is(':checked')){
$freetext.prop('disabled', false).focus();
}else{
$freetext.prop('disabled', true);
}});
});
if(! wpcf7.supportHtml5.placeholder){
$('[placeholder]', $form).each(function(){
$(this).val($(this).attr('placeholder'));
$(this).addClass('placeheld');
$(this).focus(function(){
if($(this).hasClass('placeheld')){
$(this).val('').removeClass('placeheld');
}});
$(this).blur(function(){
if(''===$(this).val()){
$(this).val($(this).attr('placeholder'));
$(this).addClass('placeheld');
}});
});
}
if(wpcf7.jqueryUi&&! wpcf7.supportHtml5.date){
$form.find('input.wpcf7-date[type="date"]').each(function(){
$(this).datepicker({
dateFormat: 'yy-mm-dd',
minDate: new Date($(this).attr('min')),
maxDate: new Date($(this).attr('max'))
});
});
}
if(wpcf7.jqueryUi&&! wpcf7.supportHtml5.number){
$form.find('input.wpcf7-number[type="number"]').each(function(){
$(this).spinner({
min: $(this).attr('min'),
max: $(this).attr('max'),
step: $(this).attr('step')
});
});
}
$('.wpcf7-character-count', $form).each(function(){
var $count=$(this);
var name=$count.attr('data-target-name');
var down=$count.hasClass('down');
var starting=parseInt($count.attr('data-starting-value'), 10);
var maximum=parseInt($count.attr('data-maximum-value'), 10);
var minimum=parseInt($count.attr('data-minimum-value'), 10);
var updateCount=function(target){
var $target=$(target);
var length=$target.val().length;
var count=down ? starting - length:length;
$count.attr('data-current-value', count);
$count.text(count);
if(maximum&&maximum < length){
$count.addClass('too-long');
}else{
$count.removeClass('too-long');
}
if(minimum&&length < minimum){
$count.addClass('too-short');
}else{
$count.removeClass('too-short');
}};
$(':input[name="' + name + '"]', $form).each(function(){
updateCount(this);
$(this).keyup(function(){
updateCount(this);
});
});
});
$form.on('change', '.wpcf7-validates-as-url', function(){
var val=$.trim($(this).val());
if(val
&& ! val.match(/^[a-z][a-z0-9.+-]*:/i)
&& -1!==val.indexOf('.')){
val=val.replace(/^\/+/, '');
val='http://' + val;
}
$(this).val(val);
});
};
wpcf7.submit=function(form){
if(typeof window.FormData!=='function'){
return;
}
var $form=$(form);
$('.ajax-loader', $form).addClass('is-active');
wpcf7.clearResponse($form);
var formData=new FormData($form.get(0));
var detail={
id: $form.closest('div.wpcf7').attr('id'),
status: 'init',
inputs: [],
formData: formData
};
$.each($form.serializeArray(), function(i, field){
if('_wpcf7'==field.name){
detail.contactFormId=field.value;
}else if('_wpcf7_version'==field.name){
detail.pluginVersion=field.value;
}else if('_wpcf7_locale'==field.name){
detail.contactFormLocale=field.value;
}else if('_wpcf7_unit_tag'==field.name){
detail.unitTag=field.value;
}else if('_wpcf7_container_post'==field.name){
detail.containerPostId=field.value;
}else if(field.name.match(/^_wpcf7_\w+_free_text_/)){
var owner=field.name.replace(/^_wpcf7_\w+_free_text_/, '');
detail.inputs.push({
name: owner + '-free-text',
value: field.value
});
}else if(field.name.match(/^_/)){
}else{
detail.inputs.push(field);
}});
wpcf7.triggerEvent($form.closest('div.wpcf7'), 'beforesubmit', detail);
var ajaxSuccess=function(data, status, xhr, $form){
detail.id=$(data.into).attr('id');
detail.status=data.status;
detail.apiResponse=data;
var $message=$('.wpcf7-response-output', $form);
switch(data.status){
case 'validation_failed':
$.each(data.invalidFields, function(i, n){
$(n.into, $form).each(function(){
wpcf7.notValidTip(this, n.message);
$('.wpcf7-form-control', this).addClass('wpcf7-not-valid');
$('[aria-invalid]', this).attr('aria-invalid', 'true');
});
});
$message.addClass('wpcf7-validation-errors');
$form.addClass('invalid');
wpcf7.triggerEvent(data.into, 'invalid', detail);
break;
case 'acceptance_missing':
$message.addClass('wpcf7-acceptance-missing');
$form.addClass('unaccepted');
wpcf7.triggerEvent(data.into, 'unaccepted', detail);
break;
case 'spam':
$message.addClass('wpcf7-spam-blocked');
$form.addClass('spam');
wpcf7.triggerEvent(data.into, 'spam', detail);
break;
case 'aborted':
$message.addClass('wpcf7-aborted');
$form.addClass('aborted');
wpcf7.triggerEvent(data.into, 'aborted', detail);
break;
case 'mail_sent':
$message.addClass('wpcf7-mail-sent-ok');
$form.addClass('sent');
wpcf7.triggerEvent(data.into, 'mailsent', detail);
break;
case 'mail_failed':
$message.addClass('wpcf7-mail-sent-ng');
$form.addClass('failed');
wpcf7.triggerEvent(data.into, 'mailfailed', detail);
break;
default:
var customStatusClass='custom-'
+ data.status.replace(/[^0-9a-z]+/i, '-');
$message.addClass('wpcf7-' + customStatusClass);
$form.addClass(customStatusClass);
}
wpcf7.refill($form, data);
wpcf7.triggerEvent(data.into, 'submit', detail);
if('mail_sent'==data.status){
$form.each(function(){
this.reset();
});
wpcf7.toggleSubmit($form);
}
if(! wpcf7.supportHtml5.placeholder){
$form.find('[placeholder].placeheld').each(function(i, n){
$(n).val($(n).attr('placeholder'));
});
}
$message.html('').append(data.message).slideDown('fast');
$message.attr('role', 'alert');
$('.screen-reader-response', $form.closest('.wpcf7')).each(function(){
var $response=$(this);
$response.html('').attr('role', '').append(data.message);
if(data.invalidFields){
var $invalids=$('<ul></ul>');
$.each(data.invalidFields, function(i, n){
if(n.idref){
var $li=$('<li></li>').append($('<a></a>').attr('href', '#' + n.idref).append(n.message));
}else{
var $li=$('<li></li>').append(n.message);
}
$invalids.append($li);
});
$response.append($invalids);
}
$response.attr('role', 'alert').focus();
});
};
$.ajax({
type: 'POST',
url: wpcf7.apiSettings.getRoute('/contact-forms/' + wpcf7.getId($form) + '/feedback'),
data: formData,
dataType: 'json',
processData: false,
contentType: false
}).done(function(data, status, xhr){
ajaxSuccess(data, status, xhr, $form);
$('.ajax-loader', $form).removeClass('is-active');
}).fail(function(xhr, status, error){
var $e=$('<div class="ajax-error"></div>').text(error.message);
$form.after($e);
});
};
wpcf7.triggerEvent=function(target, name, detail){
var $target=$(target);
var event=new CustomEvent('wpcf7' + name, {
bubbles: true,
detail: detail
});
$target.get(0).dispatchEvent(event);
$target.trigger('wpcf7:' + name, detail);
$target.trigger(name + '.wpcf7', detail);
};
wpcf7.toggleSubmit=function(form, state){
var $form=$(form);
var $submit=$('input:submit', $form);
if(typeof state!=='undefined'){
$submit.prop('disabled', ! state);
return;
}
if($form.hasClass('wpcf7-acceptance-as-validation')){
return;
}
$submit.prop('disabled', false);
$('.wpcf7-acceptance', $form).each(function(){
var $span=$(this);
var $input=$('input:checkbox', $span);
if(! $span.hasClass('optional')){
if($span.hasClass('invert')&&$input.is(':checked')
|| ! $span.hasClass('invert')&&! $input.is(':checked')){
$submit.prop('disabled', true);
return false;
}}
});
};
wpcf7.notValidTip=function(target, message){
var $target=$(target);
$('.wpcf7-not-valid-tip', $target).remove();
$('<span role="alert" class="wpcf7-not-valid-tip"></span>')
.text(message).appendTo($target);
if($target.is('.use-floating-validation-tip *')){
var fadeOut=function(target){
$(target).not(':hidden').animate({
opacity: 0
}, 'fast', function(){
$(this).css({ 'z-index': -100 });
});
};
$target.on('mouseover', '.wpcf7-not-valid-tip', function(){
fadeOut(this);
});
$target.on('focus', ':input', function(){
fadeOut($('.wpcf7-not-valid-tip', $target));
});
}};
wpcf7.refill=function(form, data){
var $form=$(form);
var refillCaptcha=function($form, items){
$.each(items, function(i, n){
$form.find(':input[name="' + i + '"]').val('');
$form.find('img.wpcf7-captcha-' + i).attr('src', n);
var match=/([0-9]+)\.(png|gif|jpeg)$/.exec(n);
$form.find('input:hidden[name="_wpcf7_captcha_challenge_' + i + '"]').attr('value', match[ 1 ]);
});
};
var refillQuiz=function($form, items){
$.each(items, function(i, n){
$form.find(':input[name="' + i + '"]').val('');
$form.find(':input[name="' + i + '"]').siblings('span.wpcf7-quiz-label').text(n[ 0 ]);
$form.find('input:hidden[name="_wpcf7_quiz_answer_' + i + '"]').attr('value', n[ 1 ]);
});
};
if(typeof data==='undefined'){
$.ajax({
type: 'GET',
url: wpcf7.apiSettings.getRoute('/contact-forms/' + wpcf7.getId($form) + '/refill'),
beforeSend: function(xhr){
var nonce=$form.find(':input[name="_wpnonce"]').val();
if(nonce){
xhr.setRequestHeader('X-WP-Nonce', nonce);
}},
dataType: 'json'
}).done(function(data, status, xhr){
if(data.captcha){
refillCaptcha($form, data.captcha);
}
if(data.quiz){
refillQuiz($form, data.quiz);
}});
}else{
if(data.captcha){
refillCaptcha($form, data.captcha);
}
if(data.quiz){
refillQuiz($form, data.quiz);
}}
};
wpcf7.clearResponse=function(form){
var $form=$(form);
$form.removeClass('invalid spam sent failed');
$form.siblings('.screen-reader-response').html('').attr('role', '');
$('.wpcf7-not-valid-tip', $form).remove();
$('[aria-invalid]', $form).attr('aria-invalid', 'false');
$('.wpcf7-form-control', $form).removeClass('wpcf7-not-valid');
$('.wpcf7-response-output', $form)
.hide().empty().removeAttr('role')
.removeClass('wpcf7-mail-sent-ok wpcf7-mail-sent-ng wpcf7-validation-errors wpcf7-spam-blocked');
};
wpcf7.apiSettings.getRoute=function(path){
var url=wpcf7.apiSettings.root;
url=url.replace(wpcf7.apiSettings.namespace,
wpcf7.apiSettings.namespace + path);
return url;
};})(jQuery);
(function (){
if(typeof window.CustomEvent==="function") return false;
function CustomEvent(event, params){
params=params||{ bubbles: false, cancelable: false, detail: undefined };
var evt=document.createEvent('CustomEvent');
evt.initCustomEvent(event,
params.bubbles, params.cancelable, params.detail);
return evt;
}
CustomEvent.prototype=window.Event.prototype;
window.CustomEvent=CustomEvent;
})();
+function ($){
'use strict';
function transitionEnd(){
var el=document.createElement('bootstrap')
var transEndEventNames={
WebkitTransition:'webkitTransitionEnd',
MozTransition:'transitionend',
OTransition:'oTransitionEnd otransitionend',
transition:'transitionend'
}
for (var name in transEndEventNames){
if(el.style[name]!==undefined){
return { end: transEndEventNames[name] }}
}
return false
}
$.fn.emulateTransitionEnd=function (duration){
var called=false
var $el=this
$(this).one('bsTransitionEnd', function (){ called=true })
var callback=function (){ if(!called) $($el).trigger($.support.transition.end) }
setTimeout(callback, duration)
return this
}
$(function (){
$.support.transition=transitionEnd()
if(!$.support.transition) return
$.event.special.bsTransitionEnd={
bindType: $.support.transition.end,
delegateType: $.support.transition.end,
handle: function (e){
if($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
}}
})
}(jQuery);
+function ($){
'use strict';
var Modal=function (element, options){
this.options=options
this.$body=$(document.body)
this.$element=$(element)
this.$dialog=this.$element.find('.modal-dialog')
this.$backdrop=null
this.isShown=null
this.originalBodyPad=null
this.scrollbarWidth=0
this.ignoreBackdropClick=false
if(this.options.remote){
this.$element
.find('.modal-content')
.load(this.options.remote, $.proxy(function (){
this.$element.trigger('loaded.bs.modal')
}, this))
}}
Modal.VERSION='3.3.6'
Modal.TRANSITION_DURATION=300
Modal.BACKDROP_TRANSITION_DURATION=150
Modal.DEFAULTS={
backdrop: true,
keyboard: true,
show: true
}
Modal.prototype.toggle=function (_relatedTarget){
return this.isShown ? this.hide():this.show(_relatedTarget)
}
Modal.prototype.show=function (_relatedTarget){
var that=this
var e=$.Event('show.bs.modal', { relatedTarget: _relatedTarget })
this.$element.trigger(e)
if(this.isShown||e.isDefaultPrevented()) return
this.isShown=true
this.checkScrollbar()
this.setScrollbar()
this.$body.addClass('modal-open')
this.escape()
this.resize()
this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))
this.$dialog.on('mousedown.dismiss.bs.modal', function (){
that.$element.one('mouseup.dismiss.bs.modal', function (e){
if($(e.target).is(that.$element)) that.ignoreBackdropClick=true
})
})
this.backdrop(function (){
var transition=$.support.transition&&that.$element.hasClass('fade')
if(!that.$element.parent().length){
that.$element.appendTo(that.$body)
}
that.$element
.show()
.scrollTop(0)
that.adjustDialog()
if(transition){
that.$element[0].offsetWidth
}
that.$element.addClass('in')
that.enforceFocus()
var e=$.Event('shown.bs.modal', { relatedTarget: _relatedTarget })
transition ?
that.$dialog
.one('bsTransitionEnd', function (){
that.$element.trigger('focus').trigger(e)
})
.emulateTransitionEnd(Modal.TRANSITION_DURATION) :
that.$element.trigger('focus').trigger(e)
})
}
Modal.prototype.hide=function (e){
if(e) e.preventDefault()
e=$.Event('hide.bs.modal')
this.$element.trigger(e)
if(!this.isShown||e.isDefaultPrevented()) return
this.isShown=false
this.escape()
this.resize()
$(document).off('focusin.bs.modal')
this.$element
.removeClass('in')
.off('click.dismiss.bs.modal')
.off('mouseup.dismiss.bs.modal')
this.$dialog.off('mousedown.dismiss.bs.modal')
$.support.transition&&this.$element.hasClass('fade') ?
this.$element
.one('bsTransitionEnd', $.proxy(this.hideModal, this))
.emulateTransitionEnd(Modal.TRANSITION_DURATION) :
this.hideModal()
}
Modal.prototype.enforceFocus=function (){
$(document)
.off('focusin.bs.modal')
.on('focusin.bs.modal', $.proxy(function (e){
if(this.$element[0]!==e.target&&!this.$element.has(e.target).length){
this.$element.trigger('focus')
}}, this))
}
Modal.prototype.escape=function (){
if(this.isShown&&this.options.keyboard){
this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e){
e.which==27&&this.hide()
}, this))
}else if(!this.isShown){
this.$element.off('keydown.dismiss.bs.modal')
}}
Modal.prototype.resize=function (){
if(this.isShown){
$(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
}else{
$(window).off('resize.bs.modal')
}}
Modal.prototype.hideModal=function (){
var that=this
this.$element.hide()
this.backdrop(function (){
that.$body.removeClass('modal-open')
that.resetAdjustments()
that.resetScrollbar()
that.$element.trigger('hidden.bs.modal')
})
}
Modal.prototype.removeBackdrop=function (){
this.$backdrop&&this.$backdrop.remove()
this.$backdrop=null
}
Modal.prototype.backdrop=function (callback){
var that=this
var animate=this.$element.hasClass('fade') ? 'fade':''
if(this.isShown&&this.options.backdrop){
var doAnimate=$.support.transition&&animate
this.$backdrop=$(document.createElement('div'))
.addClass('modal-backdrop ' + animate)
.appendTo(this.$body)
this.$element.on('click.dismiss.bs.modal', $.proxy(function (e){
if(this.ignoreBackdropClick){
this.ignoreBackdropClick=false
return
}
if(e.target!==e.currentTarget) return
this.options.backdrop=='static'
? this.$element[0].focus()
: this.hide()
}, this))
if(doAnimate) this.$backdrop[0].offsetWidth
this.$backdrop.addClass('in')
if(!callback) return
doAnimate ?
this.$backdrop
.one('bsTransitionEnd', callback)
.emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
callback()
}else if(!this.isShown&&this.$backdrop){
this.$backdrop.removeClass('in')
var callbackRemove=function (){
that.removeBackdrop()
callback&&callback()
}
$.support.transition&&this.$element.hasClass('fade') ?
this.$backdrop
.one('bsTransitionEnd', callbackRemove)
.emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
callbackRemove()
}else if(callback){
callback()
}}
Modal.prototype.handleUpdate=function (){
this.adjustDialog()
}
Modal.prototype.adjustDialog=function (){
var lmmodalIsOverflowing=this.$element[0].scrollHeight > document.documentElement.clientHeight
this.$element.css({
paddingLeft:  !this.bodyIsOverflowing&&lmmodalIsOverflowing ? this.scrollbarWidth:'',
paddingRight: this.bodyIsOverflowing&&!lmmodalIsOverflowing ? this.scrollbarWidth:''
})
}
Modal.prototype.resetAdjustments=function (){
this.$element.css({
paddingLeft: '',
paddingRight: ''
})
}
Modal.prototype.checkScrollbar=function (){
var fullWindowWidth=window.innerWidth
if(!fullWindowWidth){
var documentElementRect=document.documentElement.getBoundingClientRect()
fullWindowWidth=documentElementRect.right - Math.abs(documentElementRect.left)
}
this.bodyIsOverflowing=document.body.clientWidth < fullWindowWidth
this.scrollbarWidth=this.measureScrollbar()
}
Modal.prototype.setScrollbar=function (){
var bodyPad=parseInt((this.$body.css('padding-right')||0), 10)
this.originalBodyPad=document.body.style.paddingRight||''
if(this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
}
Modal.prototype.resetScrollbar=function (){
this.$body.css('padding-right', this.originalBodyPad)
}
Modal.prototype.measureScrollbar=function (){
var scrollDiv=document.createElement('div')
scrollDiv.className='modal-scrollbar-measure'
this.$body.append(scrollDiv)
var scrollbarWidth=scrollDiv.offsetWidth - scrollDiv.clientWidth
this.$body[0].removeChild(scrollDiv)
return scrollbarWidth
}
function Plugin(option, _relatedTarget){
return this.each(function (){
var $this=$(this)
var data=$this.data('bs.modal')
var options=$.extend({}, Modal.DEFAULTS, $this.data(), typeof option=='object'&&option)
if(!data) $this.data('bs.modal', (data=new Modal(this, options)))
if(typeof option=='string') data[option](_relatedTarget)
else if(options.show) data.show(_relatedTarget)
})
}
var old=$.fn.modal
$.fn.modal=Plugin
$.fn.modal.Constructor=Modal
$.fn.modal.noConflict=function (){
$.fn.modal=old
return this
}
$(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e){
var $this=$(this)
var href=$this.attr('href')
var $target=$($this.attr('data-target')||(href&&href.replace(/.*(?=#[^\s]+$)/, '')))
var option=$target.data('bs.modal') ? 'toggle':$.extend({ remote: !/#/.test(href)&&href }, $target.data(), $this.data())
if($this.is('a')) e.preventDefault()
$target.one('show.bs.modal', function (showEvent){
if(showEvent.isDefaultPrevented()) return
$target.one('hidden.bs.modal', function (){
$this.is(':visible')&&$this.trigger('focus')
})
})
Plugin.call($target, option, this)
})
}(jQuery);
var isMobile={
Android: function(){
return navigator.userAgent.match(/Android/i);
},
BlackBerry: function(){
return navigator.userAgent.match(/BlackBerry/i);
},
iOS: function(){
return navigator.userAgent.match(/iPhone|iPad|iPod/i);
},
Opera: function(){
return navigator.userAgent.match(/Opera Mini/i);
},
Windows: function(){
return navigator.userAgent.match(/IEMobile/i);
},
any: function(){
return (isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows());
}};
jQuery(document).ready(function($){
$('.modal-popup').click(function(){
var popup_id=$(this).attr('data-target');
$(popup_id).modal('show');
$('.modal-backdrop').hide();
});
});
(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function(global, factory){
"use strict";
if(typeof module==="object"&&typeof module.exports==="object"){
module.exports=global.document ?
factory(global, true) :
function(w){
if(!w.document){
throw new Error("jQuery requires a window with a document");
}
return factory(w);
};}else{
factory(global);
}})(typeof window!=="undefined" ? window:this, function(window, noGlobal){
"use strict";
var arr=[];
var document=window.document;
var getProto=Object.getPrototypeOf;
var slice=arr.slice;
var concat=arr.concat;
var push=arr.push;
var indexOf=arr.indexOf;
var class2type={};
var toString=class2type.toString;
var hasOwn=class2type.hasOwnProperty;
var fnToString=hasOwn.toString;
var ObjectFunctionString=fnToString.call(Object);
var support={};
var isFunction=function isFunction(obj){
return typeof obj==="function"&&typeof obj.nodeType!=="number";
};
var isWindow=function isWindow(obj){
return obj!=null&&obj===obj.window;
};
var preservedScriptAttributes={
type: true,
src: true,
noModule: true
};
function DOMEval(code, doc, node){
doc=doc||document;
var i,
script=doc.createElement("script");
script.text=code;
if(node){
for(i in preservedScriptAttributes){
if(node[ i ]){
script[ i ]=node[ i ];
}}
}
doc.head.appendChild(script).parentNode.removeChild(script);
}
function toType(obj){
if(obj==null){
return obj + "";
}
return typeof obj==="object"||typeof obj==="function" ?
class2type[ toString.call(obj) ]||"object" :
typeof obj;
}
var
version="3.3.1",
jQuery=function(selector, context){
return new jQuery.fn.init(selector, context);
},
rtrim=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
jQuery.fn=jQuery.prototype={
jquery: version,
constructor: jQuery,
length: 0,
toArray: function(){
return slice.call(this);
},
get: function(num){
if(num==null){
return slice.call(this);
}
return num < 0 ? this[ num + this.length ]:this[ num ];
},
pushStack: function(elems){
var ret=jQuery.merge(this.constructor(), elems);
ret.prevObject=this;
return ret;
},
each: function(callback){
return jQuery.each(this, callback);
},
map: function(callback){
return this.pushStack(jQuery.map(this, function(elem, i){
return callback.call(elem, i, elem);
}));
},
slice: function(){
return this.pushStack(slice.apply(this, arguments));
},
first: function(){
return this.eq(0);
},
last: function(){
return this.eq(-1);
},
eq: function(i){
var len=this.length,
j=+i +(i < 0 ? len:0);
return this.pushStack(j >=0&&j < len ? [ this[ j ] ]:[]);
},
end: function(){
return this.prevObject||this.constructor();
},
push: push,
sort: arr.sort,
splice: arr.splice
};
jQuery.extend=jQuery.fn.extend=function(){
var options, name, src, copy, copyIsArray, clone,
target=arguments[ 0 ]||{},
i=1,
length=arguments.length,
deep=false;
if(typeof target==="boolean"){
deep=target;
target=arguments[ i ]||{};
i++;
}
if(typeof target!=="object"&&!isFunction(target)){
target={};}
if(i===length){
target=this;
i--;
}
for(; i < length; i++){
if(( options=arguments[ i ])!=null){
for(name in options){
src=target[ name ];
copy=options[ name ];
if(target===copy){
continue;
}
if(deep&&copy&&(jQuery.isPlainObject(copy) ||
(copyIsArray=Array.isArray(copy)))){
if(copyIsArray){
copyIsArray=false;
clone=src&&Array.isArray(src) ? src:[];
}else{
clone=src&&jQuery.isPlainObject(src) ? src:{};}
target[ name ]=jQuery.extend(deep, clone, copy);
}else if(copy!==undefined){
target[ name ]=copy;
}}
}}
return target;
};
jQuery.extend({
expando: "jQuery" +(version + Math.random()).replace(/\D/g, ""),
isReady: true,
error: function(msg){
throw new Error(msg);
},
noop: function(){},
isPlainObject: function(obj){
var proto, Ctor;
if(!obj||toString.call(obj)!=="[object Object]"){
return false;
}
proto=getProto(obj);
if(!proto){
return true;
}
Ctor=hasOwn.call(proto, "constructor")&&proto.constructor;
return typeof Ctor==="function"&&fnToString.call(Ctor)===ObjectFunctionString;
},
isEmptyObject: function(obj){
var name;
for(name in obj){
return false;
}
return true;
},
globalEval: function(code){
DOMEval(code);
},
each: function(obj, callback){
var length, i=0;
if(isArrayLike(obj)){
length=obj.length;
for(; i < length; i++){
if(callback.call(obj[ i ], i, obj[ i ])===false){
break;
}}
}else{
for(i in obj){
if(callback.call(obj[ i ], i, obj[ i ])===false){
break;
}}
}
return obj;
},
trim: function(text){
return text==null ?
"" :
(text + "").replace(rtrim, "");
},
makeArray: function(arr, results){
var ret=results||[];
if(arr!=null){
if(isArrayLike(Object(arr))){
jQuery.merge(ret,
typeof arr==="string" ?
[ arr ]:arr
);
}else{
push.call(ret, arr);
}}
return ret;
},
inArray: function(elem, arr, i){
return arr==null ? -1:indexOf.call(arr, elem, i);
},
merge: function(first, second){
var len=+second.length,
j=0,
i=first.length;
for(; j < len; j++){
first[ i++ ]=second[ j ];
}
first.length=i;
return first;
},
grep: function(elems, callback, invert){
var callbackInverse,
matches=[],
i=0,
length=elems.length,
callbackExpect = !invert;
for(; i < length; i++){
callbackInverse = !callback(elems[ i ], i);
if(callbackInverse!==callbackExpect){
matches.push(elems[ i ]);
}}
return matches;
},
map: function(elems, callback, arg){
var length, value,
i=0,
ret=[];
if(isArrayLike(elems)){
length=elems.length;
for(; i < length; i++){
value=callback(elems[ i ], i, arg);
if(value!=null){
ret.push(value);
}}
}else{
for(i in elems){
value=callback(elems[ i ], i, arg);
if(value!=null){
ret.push(value);
}}
}
return concat.apply([], ret);
},
guid: 1,
support: support
});
if(typeof Symbol==="function"){
jQuery.fn[ Symbol.iterator ]=arr[ Symbol.iterator ];
}
jQuery.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),
function(i, name){
class2type[ "[object " + name + "]" ]=name.toLowerCase();
});
function isArrayLike(obj){
var length = !!obj&&"length" in obj&&obj.length,
type=toType(obj);
if(isFunction(obj)||isWindow(obj)){
return false;
}
return type==="array"||length===0 ||
typeof length==="number"&&length > 0&&(length - 1) in obj;
}
var Sizzle =
(function(window){
var i,
support,
Expr,
getText,
isXML,
tokenize,
compile,
select,
outermostContext,
sortInput,
hasDuplicate,
setDocument,
document,
docElem,
documentIsHTML,
rbuggyQSA,
rbuggyMatches,
matches,
contains,
expando="sizzle" + 1 * new Date(),
preferredDoc=window.document,
dirruns=0,
done=0,
classCache=createCache(),
tokenCache=createCache(),
compilerCache=createCache(),
sortOrder=function(a, b){
if(a===b){
hasDuplicate=true;
}
return 0;
},
hasOwn=({}).hasOwnProperty,
arr=[],
pop=arr.pop,
push_native=arr.push,
push=arr.push,
slice=arr.slice,
indexOf=function(list, elem){
var i=0,
len=list.length;
for(; i < len; i++){
if(list[i]===elem){
return i;
}}
return -1;
},
booleans="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
whitespace="[\\x20\\t\\r\\n\\f]",
identifier="(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
attributes="\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
"*([*^$|!~]?=)" + whitespace +
"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
"*\\]",
pseudos=":(" + identifier + ")(?:\\((" +
"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
".*" +
")\\)|)",
rwhitespace=new RegExp(whitespace + "+", "g"),
rtrim=new RegExp("^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g"),
rcomma=new RegExp("^" + whitespace + "*," + whitespace + "*"),
rcombinators=new RegExp("^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*"),
rattributeQuotes=new RegExp("=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g"),
rpseudo=new RegExp(pseudos),
ridentifier=new RegExp("^" + identifier + "$"),
matchExpr={
"ID": new RegExp("^#(" + identifier + ")"),
"CLASS": new RegExp("^\\.(" + identifier + ")"),
"TAG": new RegExp("^(" + identifier + "|[*])"),
"ATTR": new RegExp("^" + attributes),
"PSEUDO": new RegExp("^" + pseudos),
"CHILD": new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
"*(\\d+)|))" + whitespace + "*\\)|)", "i"),
"bool": new RegExp("^(?:" + booleans + ")$", "i"),
"needsContext": new RegExp("^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i")
},
rinputs=/^(?:input|select|textarea|button)$/i,
rheader=/^h\d$/i,
rnative=/^[^{]+\{\s*\[native \w/,
rquickExpr=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
rsibling=/[+~]/,
runescape=new RegExp("\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig"),
funescape=function(_, escaped, escapedWhitespace){
var high="0x" + escaped - 0x10000;
return high!==high||escapedWhitespace ?
escaped :
high < 0 ?
String.fromCharCode(high + 0x10000) :
String.fromCharCode(high >> 10 | 0xD800, high & 0x3FF | 0xDC00);
},
rcssescape=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
fcssescape=function(ch, asCodePoint){
if(asCodePoint){
if(ch==="\0"){
return "\uFFFD";
}
return ch.slice(0, -1) + "\\" + ch.charCodeAt(ch.length - 1).toString(16) + " ";
}
return "\\" + ch;
},
unloadHandler=function(){
setDocument();
},
disabledAncestor=addCombinator(
function(elem){
return elem.disabled===true&&("form" in elem||"label" in elem);
},
{ dir: "parentNode", next: "legend" }
);
try {
push.apply((arr=slice.call(preferredDoc.childNodes)),
preferredDoc.childNodes
);
arr[ preferredDoc.childNodes.length ].nodeType;
} catch(e){
push={ apply: arr.length ?
function(target, els){
push_native.apply(target, slice.call(els));
} :
function(target, els){
var j=target.length,
i=0;
while((target[j++]=els[i++])){}
target.length=j - 1;
}};}
function Sizzle(selector, context, results, seed){
var m, i, elem, nid, match, groups, newSelector,
newContext=context&&context.ownerDocument,
nodeType=context ? context.nodeType:9;
results=results||[];
if(typeof selector!=="string"||!selector ||
nodeType!==1&&nodeType!==9&&nodeType!==11){
return results;
}
if(!seed){
if(( context ? context.ownerDocument||context:preferredDoc)!==document){
setDocument(context);
}
context=context||document;
if(documentIsHTML){
if(nodeType!==11&&(match=rquickExpr.exec(selector))){
if((m=match[1])){
if(nodeType===9){
if((elem=context.getElementById(m))){
if(elem.id===m){
results.push(elem);
return results;
}}else{
return results;
}}else{
if(newContext&&(elem=newContext.getElementById(m)) &&
contains(context, elem) &&
elem.id===m){
results.push(elem);
return results;
}}
}else if(match[2]){
push.apply(results, context.getElementsByTagName(selector));
return results;
}else if((m=match[3])&&support.getElementsByClassName &&
context.getElementsByClassName){
push.apply(results, context.getElementsByClassName(m));
return results;
}}
if(support.qsa &&
!compilerCache[ selector + " " ] &&
(!rbuggyQSA||!rbuggyQSA.test(selector))){
if(nodeType!==1){
newContext=context;
newSelector=selector;
}else if(context.nodeName.toLowerCase()!=="object"){
if((nid=context.getAttribute("id"))){
nid=nid.replace(rcssescape, fcssescape);
}else{
context.setAttribute("id", (nid=expando));
}
groups=tokenize(selector);
i=groups.length;
while(i--){
groups[i]="#" + nid + " " + toSelector(groups[i]);
}
newSelector=groups.join(",");
newContext=rsibling.test(selector)&&testContext(context.parentNode) ||
context;
}
if(newSelector){
try {
push.apply(results,
newContext.querySelectorAll(newSelector)
);
return results;
} catch(qsaError){
} finally {
if(nid===expando){
context.removeAttribute("id");
}}
}}
}}
return select(selector.replace(rtrim, "$1"), context, results, seed);
}
/**
* Create key-value caches of limited size
* @returns {function(string, object)} Returns the Object data after storing it on itself with
*	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
*	deleting the oldest entry
*/
function createCache(){
var keys=[];
function cache(key, value){
if(keys.push(key + " ") > Expr.cacheLength){
delete cache[ keys.shift() ];
}
return (cache[ key + " " ]=value);
}
return cache;
}
function markFunction(fn){
fn[ expando ]=true;
return fn;
}
function assert(fn){
var el=document.createElement("fieldset");
try {
return !!fn(el);
} catch (e){
return false;
} finally {
if(el.parentNode){
el.parentNode.removeChild(el);
}
el=null;
}}
function addHandle(attrs, handler){
var arr=attrs.split("|"),
i=arr.length;
while(i--){
Expr.attrHandle[ arr[i] ]=handler;
}}
function siblingCheck(a, b){
var cur=b&&a,
diff=cur&&a.nodeType===1&&b.nodeType===1 &&
a.sourceIndex - b.sourceIndex;
if(diff){
return diff;
}
if(cur){
while((cur=cur.nextSibling)){
if(cur===b){
return -1;
}}
}
return a ? 1:-1;
}
function createInputPseudo(type){
return function(elem){
var name=elem.nodeName.toLowerCase();
return name==="input"&&elem.type===type;
};}
function createButtonPseudo(type){
return function(elem){
var name=elem.nodeName.toLowerCase();
return (name==="input"||name==="button")&&elem.type===type;
};}
function createDisabledPseudo(disabled){
return function(elem){
if("form" in elem){
if(elem.parentNode&&elem.disabled===false){
if("label" in elem){
if("label" in elem.parentNode){
return elem.parentNode.disabled===disabled;
}else{
return elem.disabled===disabled;
}}
return elem.isDisabled===disabled ||
elem.isDisabled!==!disabled &&
disabledAncestor(elem)===disabled;
}
return elem.disabled===disabled;
}else if("label" in elem){
return elem.disabled===disabled;
}
return false;
};}
function createPositionalPseudo(fn){
return markFunction(function(argument){
argument=+argument;
return markFunction(function(seed, matches){
var j,
matchIndexes=fn([], seed.length, argument),
i=matchIndexes.length;
while(i--){
if(seed[ (j=matchIndexes[i]) ]){
seed[j] = !(matches[j]=seed[j]);
}}
});
});
}
function testContext(context){
return context&&typeof context.getElementsByTagName!=="undefined"&&context;
}
support=Sizzle.support={};
isXML=Sizzle.isXML=function(elem){
var documentElement=elem&&(elem.ownerDocument||elem).documentElement;
return documentElement ? documentElement.nodeName!=="HTML":false;
};
setDocument=Sizzle.setDocument=function(node){
var hasCompare, subWindow,
doc=node ? node.ownerDocument||node:preferredDoc;
if(doc===document||doc.nodeType!==9||!doc.documentElement){
return document;
}
document=doc;
docElem=document.documentElement;
documentIsHTML = !isXML(document);
if(preferredDoc!==document &&
(subWindow=document.defaultView)&&subWindow.top!==subWindow){
if(subWindow.addEventListener){
subWindow.addEventListener("unload", unloadHandler, false);
}else if(subWindow.attachEvent){
subWindow.attachEvent("onunload", unloadHandler);
}}
support.attributes=assert(function(el){
el.className="i";
return !el.getAttribute("className");
});
support.getElementsByTagName=assert(function(el){
el.appendChild(document.createComment(""));
return !el.getElementsByTagName("*").length;
});
support.getElementsByClassName=rnative.test(document.getElementsByClassName);
support.getById=assert(function(el){
docElem.appendChild(el).id=expando;
return !document.getElementsByName||!document.getElementsByName(expando).length;
});
if(support.getById){
Expr.filter["ID"]=function(id){
var attrId=id.replace(runescape, funescape);
return function(elem){
return elem.getAttribute("id")===attrId;
};};
Expr.find["ID"]=function(id, context){
if(typeof context.getElementById!=="undefined"&&documentIsHTML){
var elem=context.getElementById(id);
return elem ? [ elem ]:[];
}};}else{
Expr.filter["ID"]=function(id){
var attrId=id.replace(runescape, funescape);
return function(elem){
var node=typeof elem.getAttributeNode!=="undefined" &&
elem.getAttributeNode("id");
return node&&node.value===attrId;
};};
Expr.find["ID"]=function(id, context){
if(typeof context.getElementById!=="undefined"&&documentIsHTML){
var node, i, elems,
elem=context.getElementById(id);
if(elem){
node=elem.getAttributeNode("id");
if(node&&node.value===id){
return [ elem ];
}
elems=context.getElementsByName(id);
i=0;
while((elem=elems[i++])){
node=elem.getAttributeNode("id");
if(node&&node.value===id){
return [ elem ];
}}
}
return [];
}};}
Expr.find["TAG"]=support.getElementsByTagName ?
function(tag, context){
if(typeof context.getElementsByTagName!=="undefined"){
return context.getElementsByTagName(tag);
}else if(support.qsa){
return context.querySelectorAll(tag);
}} :
function(tag, context){
var elem,
tmp=[],
i=0,
results=context.getElementsByTagName(tag);
if(tag==="*"){
while((elem=results[i++])){
if(elem.nodeType===1){
tmp.push(elem);
}}
return tmp;
}
return results;
};
Expr.find["CLASS"]=support.getElementsByClassName&&function(className, context){
if(typeof context.getElementsByClassName!=="undefined"&&documentIsHTML){
return context.getElementsByClassName(className);
}};
rbuggyMatches=[];
rbuggyQSA=[];
if((support.qsa=rnative.test(document.querySelectorAll))){
assert(function(el){
docElem.appendChild(el).innerHTML="<a id='" + expando + "'></a>" +
"<select id='" + expando + "-\r\\' msallowcapture=''>" +
"<option selected=''></option></select>";
if(el.querySelectorAll("[msallowcapture^='']").length){
rbuggyQSA.push("[*^$]=" + whitespace + "*(?:''|\"\")");
}
if(!el.querySelectorAll("[selected]").length){
rbuggyQSA.push("\\[" + whitespace + "*(?:value|" + booleans + ")");
}
if(!el.querySelectorAll("[id~=" + expando + "-]").length){
rbuggyQSA.push("~=");
}
if(!el.querySelectorAll(":checked").length){
rbuggyQSA.push(":checked");
}
if(!el.querySelectorAll("a#" + expando + "+*").length){
rbuggyQSA.push(".#.+[+~]");
}});
assert(function(el){
el.innerHTML="<a href='' disabled='disabled'></a>" +
"<select disabled='disabled'><option/></select>";
var input=document.createElement("input");
input.setAttribute("type", "hidden");
el.appendChild(input).setAttribute("name", "D");
if(el.querySelectorAll("[name=d]").length){
rbuggyQSA.push("name" + whitespace + "*[*^$|!~]?=");
}
if(el.querySelectorAll(":enabled").length!==2){
rbuggyQSA.push(":enabled", ":disabled");
}
docElem.appendChild(el).disabled=true;
if(el.querySelectorAll(":disabled").length!==2){
rbuggyQSA.push(":enabled", ":disabled");
}
el.querySelectorAll("*,:x");
rbuggyQSA.push(",.*:");
});
}
if((support.matchesSelector=rnative.test((matches=docElem.matches ||
docElem.webkitMatchesSelector ||
docElem.mozMatchesSelector ||
docElem.oMatchesSelector ||
docElem.msMatchesSelector)))){
assert(function(el){
support.disconnectedMatch=matches.call(el, "*");
matches.call(el, "[s!='']:x");
rbuggyMatches.push("!=", pseudos);
});
}
rbuggyQSA=rbuggyQSA.length&&new RegExp(rbuggyQSA.join("|"));
rbuggyMatches=rbuggyMatches.length&&new RegExp(rbuggyMatches.join("|"));
hasCompare=rnative.test(docElem.compareDocumentPosition);
contains=hasCompare||rnative.test(docElem.contains) ?
function(a, b){
var adown=a.nodeType===9 ? a.documentElement:a,
bup=b&&b.parentNode;
return a===bup||!!(bup&&bup.nodeType===1&&(
adown.contains ?
adown.contains(bup) :
a.compareDocumentPosition&&a.compareDocumentPosition(bup) & 16
));
} :
function(a, b){
if(b){
while((b=b.parentNode)){
if(b===a){
return true;
}}
}
return false;
};
sortOrder=hasCompare ?
function(a, b){
if(a===b){
hasDuplicate=true;
return 0;
}
var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
if(compare){
return compare;
}
compare=(a.ownerDocument||a)===(b.ownerDocument||b) ?
a.compareDocumentPosition(b) :
1;
if(compare & 1 ||
(!support.sortDetached&&b.compareDocumentPosition(a)===compare)){
if(a===document||a.ownerDocument===preferredDoc&&contains(preferredDoc, a)){
return -1;
}
if(b===document||b.ownerDocument===preferredDoc&&contains(preferredDoc, b)){
return 1;
}
return sortInput ?
(indexOf(sortInput, a) - indexOf(sortInput, b)) :
0;
}
return compare & 4 ? -1:1;
} :
function(a, b){
if(a===b){
hasDuplicate=true;
return 0;
}
var cur,
i=0,
aup=a.parentNode,
bup=b.parentNode,
ap=[ a ],
bp=[ b ];
if(!aup||!bup){
return a===document ? -1 :
b===document ? 1 :
aup ? -1 :
bup ? 1 :
sortInput ?
(indexOf(sortInput, a) - indexOf(sortInput, b)) :
0;
}else if(aup===bup){
return siblingCheck(a, b);
}
cur=a;
while((cur=cur.parentNode)){
ap.unshift(cur);
}
cur=b;
while((cur=cur.parentNode)){
bp.unshift(cur);
}
while(ap[i]===bp[i]){
i++;
}
return i ?
siblingCheck(ap[i], bp[i]) :
ap[i]===preferredDoc ? -1 :
bp[i]===preferredDoc ? 1 :
0;
};
return document;
};
Sizzle.matches=function(expr, elements){
return Sizzle(expr, null, null, elements);
};
Sizzle.matchesSelector=function(elem, expr){
if(( elem.ownerDocument||elem)!==document){
setDocument(elem);
}
expr=expr.replace(rattributeQuotes, "='$1']");
if(support.matchesSelector&&documentIsHTML &&
!compilerCache[ expr + " " ] &&
(!rbuggyMatches||!rbuggyMatches.test(expr)) &&
(!rbuggyQSA||!rbuggyQSA.test(expr))){
try {
var ret=matches.call(elem, expr);
if(ret||support.disconnectedMatch ||
elem.document&&elem.document.nodeType!==11){
return ret;
}} catch (e){}}
return Sizzle(expr, document, null, [ elem ]).length > 0;
};
Sizzle.contains=function(context, elem){
if(( context.ownerDocument||context)!==document){
setDocument(context);
}
return contains(context, elem);
};
Sizzle.attr=function(elem, name){
if(( elem.ownerDocument||elem)!==document){
setDocument(elem);
}
var fn=Expr.attrHandle[ name.toLowerCase() ],
val=fn&&hasOwn.call(Expr.attrHandle, name.toLowerCase()) ?
fn(elem, name, !documentIsHTML) :
undefined;
return val!==undefined ?
val :
support.attributes||!documentIsHTML ?
elem.getAttribute(name) :
(val=elem.getAttributeNode(name))&&val.specified ?
val.value :
null;
};
Sizzle.escape=function(sel){
return (sel + "").replace(rcssescape, fcssescape);
};
Sizzle.error=function(msg){
throw new Error("Syntax error, unrecognized expression: " + msg);
};
Sizzle.uniqueSort=function(results){
var elem,
duplicates=[],
j=0,
i=0;
hasDuplicate = !support.detectDuplicates;
sortInput = !support.sortStable&&results.slice(0);
results.sort(sortOrder);
if(hasDuplicate){
while((elem=results[i++])){
if(elem===results[ i ]){
j=duplicates.push(i);
}}
while(j--){
results.splice(duplicates[ j ], 1);
}}
sortInput=null;
return results;
};
getText=Sizzle.getText=function(elem){
var node,
ret="",
i=0,
nodeType=elem.nodeType;
if(!nodeType){
while((node=elem[i++])){
ret +=getText(node);
}}else if(nodeType===1||nodeType===9||nodeType===11){
if(typeof elem.textContent==="string"){
return elem.textContent;
}else{
for(elem=elem.firstChild; elem; elem=elem.nextSibling){
ret +=getText(elem);
}}
}else if(nodeType===3||nodeType===4){
return elem.nodeValue;
}
return ret;
};
Expr=Sizzle.selectors={
cacheLength: 50,
createPseudo: markFunction,
match: matchExpr,
attrHandle: {},
find: {},
relative: {
">": { dir: "parentNode", first: true },
" ": { dir: "parentNode" },
"+": { dir: "previousSibling", first: true },
"~": { dir: "previousSibling" }},
preFilter: {
"ATTR": function(match){
match[1]=match[1].replace(runescape, funescape);
match[3]=(match[3]||match[4]||match[5]||"").replace(runescape, funescape);
if(match[2]==="~="){
match[3]=" " + match[3] + " ";
}
return match.slice(0, 4);
},
"CHILD": function(match){
match[1]=match[1].toLowerCase();
if(match[1].slice(0, 3)==="nth"){
if(!match[3]){
Sizzle.error(match[0]);
}
match[4]=+(match[4] ? match[5] + (match[6]||1):2 *(match[3]==="even"||match[3]==="odd"));
match[5]=+(( match[7] + match[8])||match[3]==="odd");
}else if(match[3]){
Sizzle.error(match[0]);
}
return match;
},
"PSEUDO": function(match){
var excess,
unquoted = !match[6]&&match[2];
if(matchExpr["CHILD"].test(match[0])){
return null;
}
if(match[3]){
match[2]=match[4]||match[5]||"";
}else if(unquoted&&rpseudo.test(unquoted) &&
(excess=tokenize(unquoted, true)) &&
(excess=unquoted.indexOf(")", unquoted.length - excess) - unquoted.length)){
match[0]=match[0].slice(0, excess);
match[2]=unquoted.slice(0, excess);
}
return match.slice(0, 3);
}},
filter: {
"TAG": function(nodeNameSelector){
var nodeName=nodeNameSelector.replace(runescape, funescape).toLowerCase();
return nodeNameSelector==="*" ?
function(){ return true; } :
function(elem){
return elem.nodeName&&elem.nodeName.toLowerCase()===nodeName;
};},
"CLASS": function(className){
var pattern=classCache[ className + " " ];
return pattern ||
(pattern=new RegExp("(^|" + whitespace + ")" + className + "(" + whitespace + "|$)")) &&
classCache(className, function(elem){
return pattern.test(typeof elem.className==="string"&&elem.className||typeof elem.getAttribute!=="undefined"&&elem.getAttribute("class")||"");
});
},
"ATTR": function(name, operator, check){
return function(elem){
var result=Sizzle.attr(elem, name);
if(result==null){
return operator==="!=";
}
if(!operator){
return true;
}
result +="";
return operator==="=" ? result===check :
operator==="!=" ? result!==check :
operator==="^=" ? check&&result.indexOf(check)===0 :
operator==="*=" ? check&&result.indexOf(check) > -1 :
operator==="$=" ? check&&result.slice(-check.length)===check :
operator==="~=" ?(" " + result.replace(rwhitespace, " ") + " ").indexOf(check) > -1 :
operator==="|=" ? result===check||result.slice(0, check.length + 1)===check + "-" :
false;
};},
"CHILD": function(type, what, argument, first, last){
var simple=type.slice(0, 3)!=="nth",
forward=type.slice(-4)!=="last",
ofType=what==="of-type";
return first===1&&last===0 ?
function(elem){
return !!elem.parentNode;
} :
function(elem, context, xml){
var cache, uniqueCache, outerCache, node, nodeIndex, start,
dir=simple!==forward ? "nextSibling":"previousSibling",
parent=elem.parentNode,
name=ofType&&elem.nodeName.toLowerCase(),
useCache = !xml&&!ofType,
diff=false;
if(parent){
if(simple){
while(dir){
node=elem;
while((node=node[ dir ])){
if(ofType ?
node.nodeName.toLowerCase()===name :
node.nodeType===1){
return false;
}}
start=dir=type==="only"&&!start&&"nextSibling";
}
return true;
}
start=[ forward ? parent.firstChild:parent.lastChild ];
if(forward&&useCache){
node=parent;
outerCache=node[ expando ]||(node[ expando ]={});
uniqueCache=outerCache[ node.uniqueID ] ||
(outerCache[ node.uniqueID ]={});
cache=uniqueCache[ type ]||[];
nodeIndex=cache[ 0 ]===dirruns&&cache[ 1 ];
diff=nodeIndex&&cache[ 2 ];
node=nodeIndex&&parent.childNodes[ nodeIndex ];
while((node=++nodeIndex&&node&&node[ dir ] ||
(diff=nodeIndex=0)||start.pop())){
if(node.nodeType===1&&++diff&&node===elem){
uniqueCache[ type ]=[ dirruns, nodeIndex, diff ];
break;
}}
}else{
if(useCache){
node=elem;
outerCache=node[ expando ]||(node[ expando ]={});
uniqueCache=outerCache[ node.uniqueID ] ||
(outerCache[ node.uniqueID ]={});
cache=uniqueCache[ type ]||[];
nodeIndex=cache[ 0 ]===dirruns&&cache[ 1 ];
diff=nodeIndex;
}
if(diff===false){
while((node=++nodeIndex&&node&&node[ dir ] ||
(diff=nodeIndex=0)||start.pop())){
if(( ofType ?
node.nodeName.toLowerCase()===name :
node.nodeType===1) &&
++diff){
if(useCache){
outerCache=node[ expando ]||(node[ expando ]={});
uniqueCache=outerCache[ node.uniqueID ] ||
(outerCache[ node.uniqueID ]={});
uniqueCache[ type ]=[ dirruns, diff ];
}
if(node===elem){
break;
}}
}}
}
diff -=last;
return diff===first||(diff % first===0&&diff / first >=0);
}};},
"PSEUDO": function(pseudo, argument){
var args,
fn=Expr.pseudos[ pseudo ]||Expr.setFilters[ pseudo.toLowerCase() ] ||
Sizzle.error("unsupported pseudo: " + pseudo);
if(fn[ expando ]){
return fn(argument);
}
if(fn.length > 1){
args=[ pseudo, pseudo, "", argument ];
return Expr.setFilters.hasOwnProperty(pseudo.toLowerCase()) ?
markFunction(function(seed, matches){
var idx,
matched=fn(seed, argument),
i=matched.length;
while(i--){
idx=indexOf(seed, matched[i]);
seed[ idx ] = !(matches[ idx ]=matched[i]);
}}) :
function(elem){
return fn(elem, 0, args);
};}
return fn;
}},
pseudos: {
"not": markFunction(function(selector){
var input=[],
results=[],
matcher=compile(selector.replace(rtrim, "$1"));
return matcher[ expando ] ?
markFunction(function(seed, matches, context, xml){
var elem,
unmatched=matcher(seed, null, xml, []),
i=seed.length;
while(i--){
if((elem=unmatched[i])){
seed[i] = !(matches[i]=elem);
}}
}) :
function(elem, context, xml){
input[0]=elem;
matcher(input, null, xml, results);
input[0]=null;
return !results.pop();
};}),
"has": markFunction(function(selector){
return function(elem){
return Sizzle(selector, elem).length > 0;
};}),
"contains": markFunction(function(text){
text=text.replace(runescape, funescape);
return function(elem){
return(elem.textContent||elem.innerText||getText(elem)).indexOf(text) > -1;
};}),
"lang": markFunction(function(lang){
if(!ridentifier.test(lang||"")){
Sizzle.error("unsupported lang: " + lang);
}
lang=lang.replace(runescape, funescape).toLowerCase();
return function(elem){
var elemLang;
do {
if((elemLang=documentIsHTML ?
elem.lang :
elem.getAttribute("xml:lang")||elem.getAttribute("lang"))){
elemLang=elemLang.toLowerCase();
return elemLang===lang||elemLang.indexOf(lang + "-")===0;
}} while((elem=elem.parentNode)&&elem.nodeType===1);
return false;
};}),
"target": function(elem){
var hash=window.location&&window.location.hash;
return hash&&hash.slice(1)===elem.id;
},
"root": function(elem){
return elem===docElem;
},
"focus": function(elem){
return elem===document.activeElement&&(!document.hasFocus||document.hasFocus())&&!!(elem.type||elem.href||~elem.tabIndex);
},
"enabled": createDisabledPseudo(false),
"disabled": createDisabledPseudo(true),
"checked": function(elem){
var nodeName=elem.nodeName.toLowerCase();
return (nodeName==="input"&&!!elem.checked)||(nodeName==="option"&&!!elem.selected);
},
"selected": function(elem){
if(elem.parentNode){
elem.parentNode.selectedIndex;
}
return elem.selected===true;
},
"empty": function(elem){
for(elem=elem.firstChild; elem; elem=elem.nextSibling){
if(elem.nodeType < 6){
return false;
}}
return true;
},
"parent": function(elem){
return !Expr.pseudos["empty"](elem);
},
"header": function(elem){
return rheader.test(elem.nodeName);
},
"input": function(elem){
return rinputs.test(elem.nodeName);
},
"button": function(elem){
var name=elem.nodeName.toLowerCase();
return name==="input"&&elem.type==="button"||name==="button";
},
"text": function(elem){
var attr;
return elem.nodeName.toLowerCase()==="input" &&
elem.type==="text" &&
((attr=elem.getAttribute("type"))==null||attr.toLowerCase()==="text");
},
"first": createPositionalPseudo(function(){
return [ 0 ];
}),
"last": createPositionalPseudo(function(matchIndexes, length){
return [ length - 1 ];
}),
"eq": createPositionalPseudo(function(matchIndexes, length, argument){
return [ argument < 0 ? argument + length:argument ];
}),
"even": createPositionalPseudo(function(matchIndexes, length){
var i=0;
for(; i < length; i +=2){
matchIndexes.push(i);
}
return matchIndexes;
}),
"odd": createPositionalPseudo(function(matchIndexes, length){
var i=1;
for(; i < length; i +=2){
matchIndexes.push(i);
}
return matchIndexes;
}),
"lt": createPositionalPseudo(function(matchIndexes, length, argument){
var i=argument < 0 ? argument + length:argument;
for(; --i >=0;){
matchIndexes.push(i);
}
return matchIndexes;
}),
"gt": createPositionalPseudo(function(matchIndexes, length, argument){
var i=argument < 0 ? argument + length:argument;
for(; ++i < length;){
matchIndexes.push(i);
}
return matchIndexes;
})
}};
Expr.pseudos["nth"]=Expr.pseudos["eq"];
for(i in { radio: true, checkbox: true, file: true, password: true, image: true }){
Expr.pseudos[ i ]=createInputPseudo(i);
}
for(i in { submit: true, reset: true }){
Expr.pseudos[ i ]=createButtonPseudo(i);
}
function setFilters(){}
setFilters.prototype=Expr.filters=Expr.pseudos;
Expr.setFilters=new setFilters();
tokenize=Sizzle.tokenize=function(selector, parseOnly){
var matched, match, tokens, type,
soFar, groups, preFilters,
cached=tokenCache[ selector + " " ];
if(cached){
return parseOnly ? 0:cached.slice(0);
}
soFar=selector;
groups=[];
preFilters=Expr.preFilter;
while(soFar){
if(!matched||(match=rcomma.exec(soFar))){
if(match){
soFar=soFar.slice(match[0].length)||soFar;
}
groups.push((tokens=[]));
}
matched=false;
if((match=rcombinators.exec(soFar))){
matched=match.shift();
tokens.push({
value: matched,
type: match[0].replace(rtrim, " ")
});
soFar=soFar.slice(matched.length);
}
for(type in Expr.filter){
if((match=matchExpr[ type ].exec(soFar))&&(!preFilters[ type ] ||
(match=preFilters[ type ](match)))){
matched=match.shift();
tokens.push({
value: matched,
type: type,
matches: match
});
soFar=soFar.slice(matched.length);
}}
if(!matched){
break;
}}
return parseOnly ?
soFar.length :
soFar ?
Sizzle.error(selector) :
tokenCache(selector, groups).slice(0);
};
function toSelector(tokens){
var i=0,
len=tokens.length,
selector="";
for(; i < len; i++){
selector +=tokens[i].value;
}
return selector;
}
function addCombinator(matcher, combinator, base){
var dir=combinator.dir,
skip=combinator.next,
key=skip||dir,
checkNonElements=base&&key==="parentNode",
doneName=done++;
return combinator.first ?
function(elem, context, xml){
while((elem=elem[ dir ])){
if(elem.nodeType===1||checkNonElements){
return matcher(elem, context, xml);
}}
return false;
} :
function(elem, context, xml){
var oldCache, uniqueCache, outerCache,
newCache=[ dirruns, doneName ];
if(xml){
while((elem=elem[ dir ])){
if(elem.nodeType===1||checkNonElements){
if(matcher(elem, context, xml)){
return true;
}}
}}else{
while((elem=elem[ dir ])){
if(elem.nodeType===1||checkNonElements){
outerCache=elem[ expando ]||(elem[ expando ]={});
uniqueCache=outerCache[ elem.uniqueID ]||(outerCache[ elem.uniqueID ]={});
if(skip&&skip===elem.nodeName.toLowerCase()){
elem=elem[ dir ]||elem;
}else if((oldCache=uniqueCache[ key ]) &&
oldCache[ 0 ]===dirruns&&oldCache[ 1 ]===doneName){
return (newCache[ 2 ]=oldCache[ 2 ]);
}else{
uniqueCache[ key ]=newCache;
if((newCache[ 2 ]=matcher(elem, context, xml))){
return true;
}}
}}
}
return false;
};}
function elementMatcher(matchers){
return matchers.length > 1 ?
function(elem, context, xml){
var i=matchers.length;
while(i--){
if(!matchers[i](elem, context, xml)){
return false;
}}
return true;
} :
matchers[0];
}
function multipleContexts(selector, contexts, results){
var i=0,
len=contexts.length;
for(; i < len; i++){
Sizzle(selector, contexts[i], results);
}
return results;
}
function condense(unmatched, map, filter, context, xml){
var elem,
newUnmatched=[],
i=0,
len=unmatched.length,
mapped=map!=null;
for(; i < len; i++){
if((elem=unmatched[i])){
if(!filter||filter(elem, context, xml)){
newUnmatched.push(elem);
if(mapped){
map.push(i);
}}
}}
return newUnmatched;
}
function setMatcher(preFilter, selector, matcher, postFilter, postFinder, postSelector){
if(postFilter&&!postFilter[ expando ]){
postFilter=setMatcher(postFilter);
}
if(postFinder&&!postFinder[ expando ]){
postFinder=setMatcher(postFinder, postSelector);
}
return markFunction(function(seed, results, context, xml){
var temp, i, elem,
preMap=[],
postMap=[],
preexisting=results.length,
elems=seed||multipleContexts(selector||"*", context.nodeType ? [ context ]:context, []),
matcherIn=preFilter&&(seed||!selector) ?
condense(elems, preMap, preFilter, context, xml) :
elems,
matcherOut=matcher ?
postFinder||(seed ? preFilter:preexisting||postFilter) ?
[] :
results :
matcherIn;
if(matcher){
matcher(matcherIn, matcherOut, context, xml);
}
if(postFilter){
temp=condense(matcherOut, postMap);
postFilter(temp, [], context, xml);
i=temp.length;
while(i--){
if((elem=temp[i])){
matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ]=elem);
}}
}
if(seed){
if(postFinder||preFilter){
if(postFinder){
temp=[];
i=matcherOut.length;
while(i--){
if((elem=matcherOut[i])){
temp.push((matcherIn[i]=elem));
}}
postFinder(null, (matcherOut=[]), temp, xml);
}
i=matcherOut.length;
while(i--){
if((elem=matcherOut[i]) &&
(temp=postFinder ? indexOf(seed, elem):preMap[i]) > -1){
seed[temp] = !(results[temp]=elem);
}}
}}else{
matcherOut=condense(
matcherOut===results ?
matcherOut.splice(preexisting, matcherOut.length) :
matcherOut
);
if(postFinder){
postFinder(null, results, matcherOut, xml);
}else{
push.apply(results, matcherOut);
}}
});
}
function matcherFromTokens(tokens){
var checkContext, matcher, j,
len=tokens.length,
leadingRelative=Expr.relative[ tokens[0].type ],
implicitRelative=leadingRelative||Expr.relative[" "],
i=leadingRelative ? 1:0,
matchContext=addCombinator(function(elem){
return elem===checkContext;
}, implicitRelative, true),
matchAnyContext=addCombinator(function(elem){
return indexOf(checkContext, elem) > -1;
}, implicitRelative, true),
matchers=[ function(elem, context, xml){
var ret=(!leadingRelative&&(xml||context!==outermostContext))||(
(checkContext=context).nodeType ?
matchContext(elem, context, xml) :
matchAnyContext(elem, context, xml));
checkContext=null;
return ret;
} ];
for(; i < len; i++){
if((matcher=Expr.relative[ tokens[i].type ])){
matchers=[ addCombinator(elementMatcher(matchers), matcher) ];
}else{
matcher=Expr.filter[ tokens[i].type ].apply(null, tokens[i].matches);
if(matcher[ expando ]){
j=++i;
for(; j < len; j++){
if(Expr.relative[ tokens[j].type ]){
break;
}}
return setMatcher(
i > 1&&elementMatcher(matchers),
i > 1&&toSelector(
tokens.slice(0, i - 1).concat({ value: tokens[ i - 2 ].type===" " ? "*":"" })
).replace(rtrim, "$1"),
matcher,
i < j&&matcherFromTokens(tokens.slice(i, j)),
j < len&&matcherFromTokens((tokens=tokens.slice(j))),
j < len&&toSelector(tokens)
);
}
matchers.push(matcher);
}}
return elementMatcher(matchers);
}
function matcherFromGroupMatchers(elementMatchers, setMatchers){
var bySet=setMatchers.length > 0,
byElement=elementMatchers.length > 0,
superMatcher=function(seed, context, xml, results, outermost){
var elem, j, matcher,
matchedCount=0,
i="0",
unmatched=seed&&[],
setMatched=[],
contextBackup=outermostContext,
elems=seed||byElement&&Expr.find["TAG"]("*", outermost),
dirrunsUnique=(dirruns +=contextBackup==null ? 1:Math.random()||0.1),
len=elems.length;
if(outermost){
outermostContext=context===document||context||outermost;
}
for(; i!==len&&(elem=elems[i])!=null; i++){
if(byElement&&elem){
j=0;
if(!context&&elem.ownerDocument!==document){
setDocument(elem);
xml = !documentIsHTML;
}
while((matcher=elementMatchers[j++])){
if(matcher(elem, context||document, xml)){
results.push(elem);
break;
}}
if(outermost){
dirruns=dirrunsUnique;
}}
if(bySet){
if((elem = !matcher&&elem)){
matchedCount--;
}
if(seed){
unmatched.push(elem);
}}
}
matchedCount +=i;
if(bySet&&i!==matchedCount){
j=0;
while((matcher=setMatchers[j++])){
matcher(unmatched, setMatched, context, xml);
}
if(seed){
if(matchedCount > 0){
while(i--){
if(!(unmatched[i]||setMatched[i])){
setMatched[i]=pop.call(results);
}}
}
setMatched=condense(setMatched);
}
push.apply(results, setMatched);
if(outermost&&!seed&&setMatched.length > 0 &&
(matchedCount + setMatchers.length) > 1){
Sizzle.uniqueSort(results);
}}
if(outermost){
dirruns=dirrunsUnique;
outermostContext=contextBackup;
}
return unmatched;
};
return bySet ?
markFunction(superMatcher) :
superMatcher;
}
compile=Sizzle.compile=function(selector, match ){
var i,
setMatchers=[],
elementMatchers=[],
cached=compilerCache[ selector + " " ];
if(!cached){
if(!match){
match=tokenize(selector);
}
i=match.length;
while(i--){
cached=matcherFromTokens(match[i]);
if(cached[ expando ]){
setMatchers.push(cached);
}else{
elementMatchers.push(cached);
}}
cached=compilerCache(selector, matcherFromGroupMatchers(elementMatchers, setMatchers));
cached.selector=selector;
}
return cached;
};
select=Sizzle.select=function(selector, context, results, seed){
var i, tokens, token, type, find,
compiled=typeof selector==="function"&&selector,
match = !seed&&tokenize((selector=compiled.selector||selector));
results=results||[];
if(match.length===1){
tokens=match[0]=match[0].slice(0);
if(tokens.length > 2&&(token=tokens[0]).type==="ID" &&
context.nodeType===9&&documentIsHTML&&Expr.relative[ tokens[1].type ]){
context=(Expr.find["ID"](token.matches[0].replace(runescape, funescape), context)||[])[0];
if(!context){
return results;
}else if(compiled){
context=context.parentNode;
}
selector=selector.slice(tokens.shift().value.length);
}
i=matchExpr["needsContext"].test(selector) ? 0:tokens.length;
while(i--){
token=tokens[i];
if(Expr.relative[ (type=token.type) ]){
break;
}
if((find=Expr.find[ type ])){
if((seed=find(
token.matches[0].replace(runescape, funescape),
rsibling.test(tokens[0].type)&&testContext(context.parentNode)||context
))){
tokens.splice(i, 1);
selector=seed.length&&toSelector(tokens);
if(!selector){
push.apply(results, seed);
return results;
}
break;
}}
}}
(compiled||compile(selector, match))(
seed,
context,
!documentIsHTML,
results,
!context||rsibling.test(selector)&&testContext(context.parentNode)||context
);
return results;
};
support.sortStable=expando.split("").sort(sortOrder).join("")===expando;
support.detectDuplicates = !!hasDuplicate;
setDocument();
support.sortDetached=assert(function(el){
return el.compareDocumentPosition(document.createElement("fieldset")) & 1;
});
if(!assert(function(el){
el.innerHTML="<a href='#'></a>";
return el.firstChild.getAttribute("href")==="#" ;
})){
addHandle("type|href|height|width", function(elem, name, isXML){
if(!isXML){
return elem.getAttribute(name, name.toLowerCase()==="type" ? 1:2);
}});
}
if(!support.attributes||!assert(function(el){
el.innerHTML="<input/>";
el.firstChild.setAttribute("value", "");
return el.firstChild.getAttribute("value")==="";
})){
addHandle("value", function(elem, name, isXML){
if(!isXML&&elem.nodeName.toLowerCase()==="input"){
return elem.defaultValue;
}});
}
if(!assert(function(el){
return el.getAttribute("disabled")==null;
})){
addHandle(booleans, function(elem, name, isXML){
var val;
if(!isXML){
return elem[ name ]===true ? name.toLowerCase() :
(val=elem.getAttributeNode(name))&&val.specified ?
val.value :
null;
}});
}
return Sizzle;
})(window);
jQuery.find=Sizzle;
jQuery.expr=Sizzle.selectors;
jQuery.expr[ ":" ]=jQuery.expr.pseudos;
jQuery.uniqueSort=jQuery.unique=Sizzle.uniqueSort;
jQuery.text=Sizzle.getText;
jQuery.isXMLDoc=Sizzle.isXML;
jQuery.contains=Sizzle.contains;
jQuery.escapeSelector=Sizzle.escape;
var dir=function(elem, dir, until){
var matched=[],
truncate=until!==undefined;
while(( elem=elem[ dir ])&&elem.nodeType!==9){
if(elem.nodeType===1){
if(truncate&&jQuery(elem).is(until)){
break;
}
matched.push(elem);
}}
return matched;
};
var siblings=function(n, elem){
var matched=[];
for(; n; n=n.nextSibling){
if(n.nodeType===1&&n!==elem){
matched.push(n);
}}
return matched;
};
var rneedsContext=jQuery.expr.match.needsContext;
function nodeName(elem, name){
return elem.nodeName&&elem.nodeName.toLowerCase()===name.toLowerCase();
};
var rsingleTag=(/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i);
function winnow(elements, qualifier, not){
if(isFunction(qualifier)){
return jQuery.grep(elements, function(elem, i){
return !!qualifier.call(elem, i, elem)!==not;
});
}
if(qualifier.nodeType){
return jQuery.grep(elements, function(elem){
return(elem===qualifier)!==not;
});
}
if(typeof qualifier!=="string"){
return jQuery.grep(elements, function(elem){
return(indexOf.call(qualifier, elem) > -1)!==not;
});
}
return jQuery.filter(qualifier, elements, not);
}
jQuery.filter=function(expr, elems, not){
var elem=elems[ 0 ];
if(not){
expr=":not(" + expr + ")";
}
if(elems.length===1&&elem.nodeType===1){
return jQuery.find.matchesSelector(elem, expr) ? [ elem ]:[];
}
return jQuery.find.matches(expr, jQuery.grep(elems, function(elem){
return elem.nodeType===1;
}));
};
jQuery.fn.extend({
find: function(selector){
var i, ret,
len=this.length,
self=this;
if(typeof selector!=="string"){
return this.pushStack(jQuery(selector).filter(function(){
for(i=0; i < len; i++){
if(jQuery.contains(self[ i ], this)){
return true;
}}
}));
}
ret=this.pushStack([]);
for(i=0; i < len; i++){
jQuery.find(selector, self[ i ], ret);
}
return len > 1 ? jQuery.uniqueSort(ret):ret;
},
filter: function(selector){
return this.pushStack(winnow(this, selector||[], false));
},
not: function(selector){
return this.pushStack(winnow(this, selector||[], true));
},
is: function(selector){
return !!winnow(
this,
typeof selector==="string"&&rneedsContext.test(selector) ?
jQuery(selector) :
selector||[],
false
).length;
}});
var rootjQuery,
rquickExpr=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
init=jQuery.fn.init=function(selector, context, root){
var match, elem;
if(!selector){
return this;
}
root=root||rootjQuery;
if(typeof selector==="string"){
if(selector[ 0 ]==="<" &&
selector[ selector.length - 1 ]===">" &&
selector.length >=3){
match=[ null, selector, null ];
}else{
match=rquickExpr.exec(selector);
}
if(match&&(match[ 1 ]||!context)){
if(match[ 1 ]){
context=context instanceof jQuery ? context[ 0 ]:context;
jQuery.merge(this, jQuery.parseHTML(match[ 1 ],
context&&context.nodeType ? context.ownerDocument||context:document,
true
));
if(rsingleTag.test(match[ 1 ])&&jQuery.isPlainObject(context)){
for(match in context){
if(isFunction(this[ match ])){
this[ match ](context[ match ]);
}else{
this.attr(match, context[ match ]);
}}
}
return this;
}else{
elem=document.getElementById(match[ 2 ]);
if(elem){
this[ 0 ]=elem;
this.length=1;
}
return this;
}}else if(!context||context.jquery){
return(context||root).find(selector);
}else{
return this.constructor(context).find(selector);
}}else if(selector.nodeType){
this[ 0 ]=selector;
this.length=1;
return this;
}else if(isFunction(selector)){
return root.ready!==undefined ?
root.ready(selector) :
selector(jQuery);
}
return jQuery.makeArray(selector, this);
};
init.prototype=jQuery.fn;
rootjQuery=jQuery(document);
var rparentsprev=/^(?:parents|prev(?:Until|All))/,
guaranteedUnique={
children: true,
contents: true,
next: true,
prev: true
};
jQuery.fn.extend({
has: function(target){
var targets=jQuery(target, this),
l=targets.length;
return this.filter(function(){
var i=0;
for(; i < l; i++){
if(jQuery.contains(this, targets[ i ])){
return true;
}}
});
},
closest: function(selectors, context){
var cur,
i=0,
l=this.length,
matched=[],
targets=typeof selectors!=="string"&&jQuery(selectors);
if(!rneedsContext.test(selectors)){
for(; i < l; i++){
for(cur=this[ i ]; cur&&cur!==context; cur=cur.parentNode){
if(cur.nodeType < 11&&(targets ?
targets.index(cur) > -1 :
cur.nodeType===1 &&
jQuery.find.matchesSelector(cur, selectors))){
matched.push(cur);
break;
}}
}}
return this.pushStack(matched.length > 1 ? jQuery.uniqueSort(matched):matched);
},
index: function(elem){
if(!elem){
return(this[ 0 ]&&this[ 0 ].parentNode) ? this.first().prevAll().length:-1;
}
if(typeof elem==="string"){
return indexOf.call(jQuery(elem), this[ 0 ]);
}
return indexOf.call(this,
elem.jquery ? elem[ 0 ]:elem
);
},
add: function(selector, context){
return this.pushStack(jQuery.uniqueSort(jQuery.merge(this.get(), jQuery(selector, context))
)
);
},
addBack: function(selector){
return this.add(selector==null ?
this.prevObject:this.prevObject.filter(selector)
);
}});
function sibling(cur, dir){
while(( cur=cur[ dir ])&&cur.nodeType!==1){}
return cur;
}
jQuery.each({
parent: function(elem){
var parent=elem.parentNode;
return parent&&parent.nodeType!==11 ? parent:null;
},
parents: function(elem){
return dir(elem, "parentNode");
},
parentsUntil: function(elem, i, until){
return dir(elem, "parentNode", until);
},
next: function(elem){
return sibling(elem, "nextSibling");
},
prev: function(elem){
return sibling(elem, "previousSibling");
},
nextAll: function(elem){
return dir(elem, "nextSibling");
},
prevAll: function(elem){
return dir(elem, "previousSibling");
},
nextUntil: function(elem, i, until){
return dir(elem, "nextSibling", until);
},
prevUntil: function(elem, i, until){
return dir(elem, "previousSibling", until);
},
siblings: function(elem){
return siblings(( elem.parentNode||{}).firstChild, elem);
},
children: function(elem){
return siblings(elem.firstChild);
},
contents: function(elem){
if(nodeName(elem, "iframe")){
return elem.contentDocument;
}
if(nodeName(elem, "template")){
elem=elem.content||elem;
}
return jQuery.merge([], elem.childNodes);
}}, function(name, fn){
jQuery.fn[ name ]=function(until, selector){
var matched=jQuery.map(this, fn, until);
if(name.slice(-5)!=="Until"){
selector=until;
}
if(selector&&typeof selector==="string"){
matched=jQuery.filter(selector, matched);
}
if(this.length > 1){
if(!guaranteedUnique[ name ]){
jQuery.uniqueSort(matched);
}
if(rparentsprev.test(name)){
matched.reverse();
}}
return this.pushStack(matched);
};});
var rnothtmlwhite=(/[^\x20\t\r\n\f]+/g);
function createOptions(options){
var object={};
jQuery.each(options.match(rnothtmlwhite)||[], function(_, flag){
object[ flag ]=true;
});
return object;
}
jQuery.Callbacks=function(options){
options=typeof options==="string" ?
createOptions(options) :
jQuery.extend({}, options);
var
firing,
memory,
fired,
locked,
list=[],
queue=[],
firingIndex=-1,
fire=function(){
locked=locked||options.once;
fired=firing=true;
for(; queue.length; firingIndex=-1){
memory=queue.shift();
while ( ++firingIndex < list.length){
if(list[ firingIndex ].apply(memory[ 0 ], memory[ 1 ])===false &&
options.stopOnFalse){
firingIndex=list.length;
memory=false;
}}
}
if(!options.memory){
memory=false;
}
firing=false;
if(locked){
if(memory){
list=[];
}else{
list="";
}}
},
self={
add: function(){
if(list){
if(memory&&!firing){
firingIndex=list.length - 1;
queue.push(memory);
}
(function add(args){
jQuery.each(args, function(_, arg){
if(isFunction(arg)){
if(!options.unique||!self.has(arg)){
list.push(arg);
}}else if(arg&&arg.length&&toType(arg)!=="string"){
add(arg);
}});
})(arguments);
if(memory&&!firing){
fire();
}}
return this;
},
remove: function(){
jQuery.each(arguments, function(_, arg){
var index;
while(( index=jQuery.inArray(arg, list, index)) > -1){
list.splice(index, 1);
if(index <=firingIndex){
firingIndex--;
}}
});
return this;
},
has: function(fn){
return fn ?
jQuery.inArray(fn, list) > -1 :
list.length > 0;
},
empty: function(){
if(list){
list=[];
}
return this;
},
disable: function(){
locked=queue=[];
list=memory="";
return this;
},
disabled: function(){
return !list;
},
lock: function(){
locked=queue=[];
if(!memory&&!firing){
list=memory="";
}
return this;
},
locked: function(){
return !!locked;
},
fireWith: function(context, args){
if(!locked){
args=args||[];
args=[ context, args.slice ? args.slice():args ];
queue.push(args);
if(!firing){
fire();
}}
return this;
},
fire: function(){
self.fireWith(this, arguments);
return this;
},
fired: function(){
return !!fired;
}};
return self;
};
function Identity(v){
return v;
}
function Thrower(ex){
throw ex;
}
function adoptValue(value, resolve, reject, noValue){
var method;
try {
if(value&&isFunction(( method=value.promise))){
method.call(value).done(resolve).fail(reject);
}else if(value&&isFunction(( method=value.then))){
method.call(value, resolve, reject);
}else{
resolve.apply(undefined, [ value ].slice(noValue));
}} catch(value){
reject.apply(undefined, [ value ]);
}}
jQuery.extend({
Deferred: function(func){
var tuples=[
[ "notify", "progress", jQuery.Callbacks("memory"),
jQuery.Callbacks("memory"), 2 ],
[ "resolve", "done", jQuery.Callbacks("once memory"),
jQuery.Callbacks("once memory"), 0, "resolved" ],
[ "reject", "fail", jQuery.Callbacks("once memory"),
jQuery.Callbacks("once memory"), 1, "rejected" ]
],
state="pending",
promise={
state: function(){
return state;
},
always: function(){
deferred.done(arguments).fail(arguments);
return this;
},
"catch": function(fn){
return promise.then(null, fn);
},
pipe: function(){
var fns=arguments;
return jQuery.Deferred(function(newDefer){
jQuery.each(tuples, function(i, tuple){
var fn=isFunction(fns[ tuple[ 4 ] ])&&fns[ tuple[ 4 ] ];
deferred[ tuple[ 1 ] ](function(){
var returned=fn&&fn.apply(this, arguments);
if(returned&&isFunction(returned.promise)){
returned.promise()
.progress(newDefer.notify)
.done(newDefer.resolve)
.fail(newDefer.reject);
}else{
newDefer[ tuple[ 0 ] + "With" ](
this,
fn ? [ returned ]:arguments
);
}});
});
fns=null;
}).promise();
},
then: function(onFulfilled, onRejected, onProgress){
var maxDepth=0;
function resolve(depth, deferred, handler, special){
return function(){
var that=this,
args=arguments,
mightThrow=function(){
var returned, then;
if(depth < maxDepth){
return;
}
returned=handler.apply(that, args);
if(returned===deferred.promise()){
throw new TypeError("Thenable self-resolution");
}
then=returned &&
(typeof returned==="object" ||
typeof returned==="function") &&
returned.then;
if(isFunction(then)){
if(special){
then.call(returned,
resolve(maxDepth, deferred, Identity, special),
resolve(maxDepth, deferred, Thrower, special)
);
}else{
maxDepth++;
then.call(returned,
resolve(maxDepth, deferred, Identity, special),
resolve(maxDepth, deferred, Thrower, special),
resolve(maxDepth, deferred, Identity,
deferred.notifyWith)
);
}}else{
if(handler!==Identity){
that=undefined;
args=[ returned ];
}
(special||deferred.resolveWith)(that, args);
}},
process=special ?
mightThrow :
function(){
try {
mightThrow();
} catch(e){
if(jQuery.Deferred.exceptionHook){
jQuery.Deferred.exceptionHook(e,
process.stackTrace);
}
if(depth + 1 >=maxDepth){
if(handler!==Thrower){
that=undefined;
args=[ e ];
}
deferred.rejectWith(that, args);
}}
};
if(depth){
process();
}else{
if(jQuery.Deferred.getStackHook){
process.stackTrace=jQuery.Deferred.getStackHook();
}
window.setTimeout(process);
}};}
return jQuery.Deferred(function(newDefer){
tuples[ 0 ][ 3 ].add(resolve(
0,
newDefer,
isFunction(onProgress) ?
onProgress :
Identity,
newDefer.notifyWith
)
);
tuples[ 1 ][ 3 ].add(resolve(
0,
newDefer,
isFunction(onFulfilled) ?
onFulfilled :
Identity
)
);
tuples[ 2 ][ 3 ].add(resolve(
0,
newDefer,
isFunction(onRejected) ?
onRejected :
Thrower
)
);
}).promise();
},
promise: function(obj){
return obj!=null ? jQuery.extend(obj, promise):promise;
}},
deferred={};
jQuery.each(tuples, function(i, tuple){
var list=tuple[ 2 ],
stateString=tuple[ 5 ];
promise[ tuple[ 1 ] ]=list.add;
if(stateString){
list.add(function(){
state=stateString;
},
tuples[ 3 - i ][ 2 ].disable,
tuples[ 3 - i ][ 3 ].disable,
tuples[ 0 ][ 2 ].lock,
tuples[ 0 ][ 3 ].lock
);
}
list.add(tuple[ 3 ].fire);
deferred[ tuple[ 0 ] ]=function(){
deferred[ tuple[ 0 ] + "With" ](this===deferred ? undefined:this, arguments);
return this;
};
deferred[ tuple[ 0 ] + "With" ]=list.fireWith;
});
promise.promise(deferred);
if(func){
func.call(deferred, deferred);
}
return deferred;
},
when: function(singleValue){
var
remaining=arguments.length,
i=remaining,
resolveContexts=Array(i),
resolveValues=slice.call(arguments),
master=jQuery.Deferred(),
updateFunc=function(i){
return function(value){
resolveContexts[ i ]=this;
resolveValues[ i ]=arguments.length > 1 ? slice.call(arguments):value;
if(!(--remaining)){
master.resolveWith(resolveContexts, resolveValues);
}};};
if(remaining <=1){
adoptValue(singleValue, master.done(updateFunc(i)).resolve, master.reject,
!remaining);
if(master.state()==="pending" ||
isFunction(resolveValues[ i ]&&resolveValues[ i ].then)){
return master.then();
}}
while(i--){
adoptValue(resolveValues[ i ], updateFunc(i), master.reject);
}
return master.promise();
}});
var rerrorNames=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
jQuery.Deferred.exceptionHook=function(error, stack){
if(window.console&&window.console.warn&&error&&rerrorNames.test(error.name)){
window.console.warn("jQuery.Deferred exception: " + error.message, error.stack, stack);
}};
jQuery.readyException=function(error){
window.setTimeout(function(){
throw error;
});
};
var readyList=jQuery.Deferred();
jQuery.fn.ready=function(fn){
readyList
.then(fn)
.catch(function(error){
jQuery.readyException(error);
});
return this;
};
jQuery.extend({
isReady: false,
readyWait: 1,
ready: function(wait){
if(wait===true ? --jQuery.readyWait:jQuery.isReady){
return;
}
jQuery.isReady=true;
if(wait!==true&&--jQuery.readyWait > 0){
return;
}
readyList.resolveWith(document, [ jQuery ]);
}});
jQuery.ready.then=readyList.then;
function completed(){
document.removeEventListener("DOMContentLoaded", completed);
window.removeEventListener("load", completed);
jQuery.ready();
}
if(document.readyState==="complete" ||
(document.readyState!=="loading"&&!document.documentElement.doScroll)){
window.setTimeout(jQuery.ready);
}else{
document.addEventListener("DOMContentLoaded", completed);
window.addEventListener("load", completed);
}
var access=function(elems, fn, key, value, chainable, emptyGet, raw){
var i=0,
len=elems.length,
bulk=key==null;
if(toType(key)==="object"){
chainable=true;
for(i in key){
access(elems, fn, i, key[ i ], true, emptyGet, raw);
}}else if(value!==undefined){
chainable=true;
if(!isFunction(value)){
raw=true;
}
if(bulk){
if(raw){
fn.call(elems, value);
fn=null;
}else{
bulk=fn;
fn=function(elem, key, value){
return bulk.call(jQuery(elem), value);
};}}
if(fn){
for(; i < len; i++){
fn(
elems[ i ], key, raw ?
value :
value.call(elems[ i ], i, fn(elems[ i ], key))
);
}}
}
if(chainable){
return elems;
}
if(bulk){
return fn.call(elems);
}
return len ? fn(elems[ 0 ], key):emptyGet;
};
var rmsPrefix=/^-ms-/,
rdashAlpha=/-([a-z])/g;
function fcamelCase(all, letter){
return letter.toUpperCase();
}
function camelCase(string){
return string.replace(rmsPrefix, "ms-").replace(rdashAlpha, fcamelCase);
}
var acceptData=function(owner){
return owner.nodeType===1||owner.nodeType===9||!( +owner.nodeType);
};
function Data(){
this.expando=jQuery.expando + Data.uid++;
}
Data.uid=1;
Data.prototype={
cache: function(owner){
var value=owner[ this.expando ];
if(!value){
value={};
if(acceptData(owner)){
if(owner.nodeType){
owner[ this.expando ]=value;
}else{
Object.defineProperty(owner, this.expando, {
value: value,
configurable: true
});
}}
}
return value;
},
set: function(owner, data, value){
var prop,
cache=this.cache(owner);
if(typeof data==="string"){
cache[ camelCase(data) ]=value;
}else{
for(prop in data){
cache[ camelCase(prop) ]=data[ prop ];
}}
return cache;
},
get: function(owner, key){
return key===undefined ?
this.cache(owner) :
owner[ this.expando ]&&owner[ this.expando ][ camelCase(key) ];
},
access: function(owner, key, value){
if(key===undefined ||
(( key&&typeof key==="string")&&value===undefined)){
return this.get(owner, key);
}
this.set(owner, key, value);
return value!==undefined ? value:key;
},
remove: function(owner, key){
var i,
cache=owner[ this.expando ];
if(cache===undefined){
return;
}
if(key!==undefined){
if(Array.isArray(key)){
key=key.map(camelCase);
}else{
key=camelCase(key);
key=key in cache ?
[ key ] :
(key.match(rnothtmlwhite)||[]);
}
i=key.length;
while(i--){
delete cache[ key[ i ] ];
}}
if(key===undefined||jQuery.isEmptyObject(cache)){
if(owner.nodeType){
owner[ this.expando ]=undefined;
}else{
delete owner[ this.expando ];
}}
},
hasData: function(owner){
var cache=owner[ this.expando ];
return cache!==undefined&&!jQuery.isEmptyObject(cache);
}};
var dataPriv=new Data();
var dataUser=new Data();
var rbrace=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
rmultiDash=/[A-Z]/g;
function getData(data){
if(data==="true"){
return true;
}
if(data==="false"){
return false;
}
if(data==="null"){
return null;
}
if(data===+data + ""){
return +data;
}
if(rbrace.test(data)){
return JSON.parse(data);
}
return data;
}
function dataAttr(elem, key, data){
var name;
if(data===undefined&&elem.nodeType===1){
name="data-" + key.replace(rmultiDash, "-$&").toLowerCase();
data=elem.getAttribute(name);
if(typeof data==="string"){
try {
data=getData(data);
} catch(e){}
dataUser.set(elem, key, data);
}else{
data=undefined;
}}
return data;
}
jQuery.extend({
hasData: function(elem){
return dataUser.hasData(elem)||dataPriv.hasData(elem);
},
data: function(elem, name, data){
return dataUser.access(elem, name, data);
},
removeData: function(elem, name){
dataUser.remove(elem, name);
},
_data: function(elem, name, data){
return dataPriv.access(elem, name, data);
},
_removeData: function(elem, name){
dataPriv.remove(elem, name);
}});
jQuery.fn.extend({
data: function(key, value){
var i, name, data,
elem=this[ 0 ],
attrs=elem&&elem.attributes;
if(key===undefined){
if(this.length){
data=dataUser.get(elem);
if(elem.nodeType===1&&!dataPriv.get(elem, "hasDataAttrs")){
i=attrs.length;
while(i--){
if(attrs[ i ]){
name=attrs[ i ].name;
if(name.indexOf("data-")===0){
name=camelCase(name.slice(5));
dataAttr(elem, name, data[ name ]);
}}
}
dataPriv.set(elem, "hasDataAttrs", true);
}}
return data;
}
if(typeof key==="object"){
return this.each(function(){
dataUser.set(this, key);
});
}
return access(this, function(value){
var data;
if(elem&&value===undefined){
data=dataUser.get(elem, key);
if(data!==undefined){
return data;
}
data=dataAttr(elem, key);
if(data!==undefined){
return data;
}
return;
}
this.each(function(){
dataUser.set(this, key, value);
});
}, null, value, arguments.length > 1, null, true);
},
removeData: function(key){
return this.each(function(){
dataUser.remove(this, key);
});
}});
jQuery.extend({
queue: function(elem, type, data){
var queue;
if(elem){
type=(type||"fx") + "queue";
queue=dataPriv.get(elem, type);
if(data){
if(!queue||Array.isArray(data)){
queue=dataPriv.access(elem, type, jQuery.makeArray(data));
}else{
queue.push(data);
}}
return queue||[];
}},
dequeue: function(elem, type){
type=type||"fx";
var queue=jQuery.queue(elem, type),
startLength=queue.length,
fn=queue.shift(),
hooks=jQuery._queueHooks(elem, type),
next=function(){
jQuery.dequeue(elem, type);
};
if(fn==="inprogress"){
fn=queue.shift();
startLength--;
}
if(fn){
if(type==="fx"){
queue.unshift("inprogress");
}
delete hooks.stop;
fn.call(elem, next, hooks);
}
if(!startLength&&hooks){
hooks.empty.fire();
}},
_queueHooks: function(elem, type){
var key=type + "queueHooks";
return dataPriv.get(elem, key)||dataPriv.access(elem, key, {
empty: jQuery.Callbacks("once memory").add(function(){
dataPriv.remove(elem, [ type + "queue", key ]);
})
});
}});
jQuery.fn.extend({
queue: function(type, data){
var setter=2;
if(typeof type!=="string"){
data=type;
type="fx";
setter--;
}
if(arguments.length < setter){
return jQuery.queue(this[ 0 ], type);
}
return data===undefined ?
this :
this.each(function(){
var queue=jQuery.queue(this, type, data);
jQuery._queueHooks(this, type);
if(type==="fx"&&queue[ 0 ]!=="inprogress"){
jQuery.dequeue(this, type);
}});
},
dequeue: function(type){
return this.each(function(){
jQuery.dequeue(this, type);
});
},
clearQueue: function(type){
return this.queue(type||"fx", []);
},
promise: function(type, obj){
var tmp,
count=1,
defer=jQuery.Deferred(),
elements=this,
i=this.length,
resolve=function(){
if(!(--count)){
defer.resolveWith(elements, [ elements ]);
}};
if(typeof type!=="string"){
obj=type;
type=undefined;
}
type=type||"fx";
while(i--){
tmp=dataPriv.get(elements[ i ], type + "queueHooks");
if(tmp&&tmp.empty){
count++;
tmp.empty.add(resolve);
}}
resolve();
return defer.promise(obj);
}});
var pnum=(/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;
var rcssNum=new RegExp("^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i");
var cssExpand=[ "Top", "Right", "Bottom", "Left" ];
var isHiddenWithinTree=function(elem, el){
elem=el||elem;
return elem.style.display==="none" ||
elem.style.display==="" &&
jQuery.contains(elem.ownerDocument, elem) &&
jQuery.css(elem, "display")==="none";
};
var swap=function(elem, options, callback, args){
var ret, name,
old={};
for(name in options){
old[ name ]=elem.style[ name ];
elem.style[ name ]=options[ name ];
}
ret=callback.apply(elem, args||[]);
for(name in options){
elem.style[ name ]=old[ name ];
}
return ret;
};
function adjustCSS(elem, prop, valueParts, tween){
var adjusted, scale,
maxIterations=20,
currentValue=tween ?
function(){
return tween.cur();
} :
function(){
return jQuery.css(elem, prop, "");
},
initial=currentValue(),
unit=valueParts&&valueParts[ 3 ]||(jQuery.cssNumber[ prop ] ? "":"px"),
initialInUnit=(jQuery.cssNumber[ prop ]||unit!=="px"&&+initial) &&
rcssNum.exec(jQuery.css(elem, prop));
if(initialInUnit&&initialInUnit[ 3 ]!==unit){
initial=initial / 2;
unit=unit||initialInUnit[ 3 ];
initialInUnit=+initial||1;
while(maxIterations--){
jQuery.style(elem, prop, initialInUnit + unit);
if(( 1 - scale) *(1 -(scale=currentValue() / initial||0.5)) <=0){
maxIterations=0;
}
initialInUnit=initialInUnit / scale;
}
initialInUnit=initialInUnit * 2;
jQuery.style(elem, prop, initialInUnit + unit);
valueParts=valueParts||[];
}
if(valueParts){
initialInUnit=+initialInUnit||+initial||0;
adjusted=valueParts[ 1 ] ?
initialInUnit +(valueParts[ 1 ] + 1) * valueParts[ 2 ] :
+valueParts[ 2 ];
if(tween){
tween.unit=unit;
tween.start=initialInUnit;
tween.end=adjusted;
}}
return adjusted;
}
var defaultDisplayMap={};
function getDefaultDisplay(elem){
var temp,
doc=elem.ownerDocument,
nodeName=elem.nodeName,
display=defaultDisplayMap[ nodeName ];
if(display){
return display;
}
temp=doc.body.appendChild(doc.createElement(nodeName));
display=jQuery.css(temp, "display");
temp.parentNode.removeChild(temp);
if(display==="none"){
display="block";
}
defaultDisplayMap[ nodeName ]=display;
return display;
}
function showHide(elements, show){
var display, elem,
values=[],
index=0,
length=elements.length;
for(; index < length; index++){
elem=elements[ index ];
if(!elem.style){
continue;
}
display=elem.style.display;
if(show){
if(display==="none"){
values[ index ]=dataPriv.get(elem, "display")||null;
if(!values[ index ]){
elem.style.display="";
}}
if(elem.style.display===""&&isHiddenWithinTree(elem)){
values[ index ]=getDefaultDisplay(elem);
}}else{
if(display!=="none"){
values[ index ]="none";
dataPriv.set(elem, "display", display);
}}
}
for(index=0; index < length; index++){
if(values[ index ]!=null){
elements[ index ].style.display=values[ index ];
}}
return elements;
}
jQuery.fn.extend({
show: function(){
return showHide(this, true);
},
hide: function(){
return showHide(this);
},
toggle: function(state){
if(typeof state==="boolean"){
return state ? this.show():this.hide();
}
return this.each(function(){
if(isHiddenWithinTree(this)){
jQuery(this).show();
}else{
jQuery(this).hide();
}});
}});
var rcheckableType=(/^(?:checkbox|radio)$/i);
var rtagName=(/<([a-z][^\/\0>\x20\t\r\n\f]+)/i);
var rscriptType=(/^$|^module$|\/(?:java|ecma)script/i);
var wrapMap={
option: [ 1, "<select multiple='multiple'>", "</select>" ],
thead: [ 1, "<table>", "</table>" ],
col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
tr: [ 2, "<table><tbody>", "</tbody></table>" ],
td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
_default: [ 0, "", "" ]
};
wrapMap.optgroup=wrapMap.option;
wrapMap.tbody=wrapMap.tfoot=wrapMap.colgroup=wrapMap.caption=wrapMap.thead;
wrapMap.th=wrapMap.td;
function getAll(context, tag){
var ret;
if(typeof context.getElementsByTagName!=="undefined"){
ret=context.getElementsByTagName(tag||"*");
}else if(typeof context.querySelectorAll!=="undefined"){
ret=context.querySelectorAll(tag||"*");
}else{
ret=[];
}
if(tag===undefined||tag&&nodeName(context, tag)){
return jQuery.merge([ context ], ret);
}
return ret;
}
function setGlobalEval(elems, refElements){
var i=0,
l=elems.length;
for(; i < l; i++){
dataPriv.set(elems[ i ],
"globalEval",
!refElements||dataPriv.get(refElements[ i ], "globalEval")
);
}}
var rhtml=/<|&#?\w+;/;
function buildFragment(elems, context, scripts, selection, ignored){
var elem, tmp, tag, wrap, contains, j,
fragment=context.createDocumentFragment(),
nodes=[],
i=0,
l=elems.length;
for(; i < l; i++){
elem=elems[ i ];
if(elem||elem===0){
if(toType(elem)==="object"){
jQuery.merge(nodes, elem.nodeType ? [ elem ]:elem);
}else if(!rhtml.test(elem)){
nodes.push(context.createTextNode(elem));
}else{
tmp=tmp||fragment.appendChild(context.createElement("div"));
tag=(rtagName.exec(elem)||[ "", "" ])[ 1 ].toLowerCase();
wrap=wrapMap[ tag ]||wrapMap._default;
tmp.innerHTML=wrap[ 1 ] + jQuery.htmlPrefilter(elem) + wrap[ 2 ];
j=wrap[ 0 ];
while(j--){
tmp=tmp.lastChild;
}
jQuery.merge(nodes, tmp.childNodes);
tmp=fragment.firstChild;
tmp.textContent="";
}}
}
fragment.textContent="";
i=0;
while(( elem=nodes[ i++ ])){
if(selection&&jQuery.inArray(elem, selection) > -1){
if(ignored){
ignored.push(elem);
}
continue;
}
contains=jQuery.contains(elem.ownerDocument, elem);
tmp=getAll(fragment.appendChild(elem), "script");
if(contains){
setGlobalEval(tmp);
}
if(scripts){
j=0;
while(( elem=tmp[ j++ ])){
if(rscriptType.test(elem.type||"")){
scripts.push(elem);
}}
}}
return fragment;
}
(function(){
var fragment=document.createDocumentFragment(),
div=fragment.appendChild(document.createElement("div")),
input=document.createElement("input");
input.setAttribute("type", "radio");
input.setAttribute("checked", "checked");
input.setAttribute("name", "t");
div.appendChild(input);
support.checkClone=div.cloneNode(true).cloneNode(true).lastChild.checked;
div.innerHTML="<textarea>x</textarea>";
support.noCloneChecked = !!div.cloneNode(true).lastChild.defaultValue;
})();
var documentElement=document.documentElement;
var
rkeyEvent=/^key/,
rmouseEvent=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,
rtypenamespace=/^([^.]*)(?:\.(.+)|)/;
function returnTrue(){
return true;
}
function returnFalse(){
return false;
}
function safeActiveElement(){
try {
return document.activeElement;
} catch(err){ }}
function on(elem, types, selector, data, fn, one){
var origFn, type;
if(typeof types==="object"){
if(typeof selector!=="string"){
data=data||selector;
selector=undefined;
}
for(type in types){
on(elem, type, selector, data, types[ type ], one);
}
return elem;
}
if(data==null&&fn==null){
fn=selector;
data=selector=undefined;
}else if(fn==null){
if(typeof selector==="string"){
fn=data;
data=undefined;
}else{
fn=data;
data=selector;
selector=undefined;
}}
if(fn===false){
fn=returnFalse;
}else if(!fn){
return elem;
}
if(one===1){
origFn=fn;
fn=function(event){
jQuery().off(event);
return origFn.apply(this, arguments);
};
fn.guid=origFn.guid||(origFn.guid=jQuery.guid++);
}
return elem.each(function(){
jQuery.event.add(this, types, fn, data, selector);
});
}
jQuery.event={
global: {},
add: function(elem, types, handler, data, selector){
var handleObjIn, eventHandle, tmp,
events, t, handleObj,
special, handlers, type, namespaces, origType,
elemData=dataPriv.get(elem);
if(!elemData){
return;
}
if(handler.handler){
handleObjIn=handler;
handler=handleObjIn.handler;
selector=handleObjIn.selector;
}
if(selector){
jQuery.find.matchesSelector(documentElement, selector);
}
if(!handler.guid){
handler.guid=jQuery.guid++;
}
if(!(events=elemData.events)){
events=elemData.events={};}
if(!(eventHandle=elemData.handle)){
eventHandle=elemData.handle=function(e){
return typeof jQuery!=="undefined"&&jQuery.event.triggered!==e.type ?
jQuery.event.dispatch.apply(elem, arguments):undefined;
};}
types=(types||"").match(rnothtmlwhite)||[ "" ];
t=types.length;
while(t--){
tmp=rtypenamespace.exec(types[ t ])||[];
type=origType=tmp[ 1 ];
namespaces=(tmp[ 2 ]||"").split(".").sort();
if(!type){
continue;
}
special=jQuery.event.special[ type ]||{};
type=(selector ? special.delegateType:special.bindType)||type;
special=jQuery.event.special[ type ]||{};
handleObj=jQuery.extend({
type: type,
origType: origType,
data: data,
handler: handler,
guid: handler.guid,
selector: selector,
needsContext: selector&&jQuery.expr.match.needsContext.test(selector),
namespace: namespaces.join(".")
}, handleObjIn);
if(!(handlers=events[ type ])){
handlers=events[ type ]=[];
handlers.delegateCount=0;
if(!special.setup ||
special.setup.call(elem, data, namespaces, eventHandle)===false){
if(elem.addEventListener){
elem.addEventListener(type, eventHandle);
}}
}
if(special.add){
special.add.call(elem, handleObj);
if(!handleObj.handler.guid){
handleObj.handler.guid=handler.guid;
}}
if(selector){
handlers.splice(handlers.delegateCount++, 0, handleObj);
}else{
handlers.push(handleObj);
}
jQuery.event.global[ type ]=true;
}},
remove: function(elem, types, handler, selector, mappedTypes){
var j, origCount, tmp,
events, t, handleObj,
special, handlers, type, namespaces, origType,
elemData=dataPriv.hasData(elem)&&dataPriv.get(elem);
if(!elemData||!(events=elemData.events)){
return;
}
types=(types||"").match(rnothtmlwhite)||[ "" ];
t=types.length;
while(t--){
tmp=rtypenamespace.exec(types[ t ])||[];
type=origType=tmp[ 1 ];
namespaces=(tmp[ 2 ]||"").split(".").sort();
if(!type){
for(type in events){
jQuery.event.remove(elem, type + types[ t ], handler, selector, true);
}
continue;
}
special=jQuery.event.special[ type ]||{};
type=(selector ? special.delegateType:special.bindType)||type;
handlers=events[ type ]||[];
tmp=tmp[ 2 ] &&
new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)");
origCount=j = handlers.length;
while(j--){
handleObj=handlers[ j ];
if(( mappedTypes||origType===handleObj.origType) &&
(!handler||handler.guid===handleObj.guid) &&
(!tmp||tmp.test(handleObj.namespace)) &&
(!selector||selector===handleObj.selector ||
selector==="**"&&handleObj.selector)){
handlers.splice(j, 1);
if(handleObj.selector){
handlers.delegateCount--;
}
if(special.remove){
special.remove.call(elem, handleObj);
}}
}
if(origCount&&!handlers.length){
if(!special.teardown ||
special.teardown.call(elem, namespaces, elemData.handle)===false){
jQuery.removeEvent(elem, type, elemData.handle);
}
delete events[ type ];
}}
if(jQuery.isEmptyObject(events)){
dataPriv.remove(elem, "handle events");
}},
dispatch: function(nativeEvent){
var event=jQuery.event.fix(nativeEvent);
var i, j, ret, matched, handleObj, handlerQueue,
args=new Array(arguments.length),
handlers=(dataPriv.get(this, "events")||{})[ event.type ]||[],
special=jQuery.event.special[ event.type ]||{};
args[ 0 ]=event;
for(i=1; i < arguments.length; i++){
args[ i ]=arguments[ i ];
}
event.delegateTarget=this;
if(special.preDispatch&&special.preDispatch.call(this, event)===false){
return;
}
handlerQueue=jQuery.event.handlers.call(this, event, handlers);
i=0;
while(( matched=handlerQueue[ i++ ])&&!event.isPropagationStopped()){
event.currentTarget=matched.elem;
j=0;
while(( handleObj=matched.handlers[ j++ ]) &&
!event.isImmediatePropagationStopped()){
if(!event.rnamespace||event.rnamespace.test(handleObj.namespace)){
event.handleObj=handleObj;
event.data=handleObj.data;
ret=(( jQuery.event.special[ handleObj.origType ]||{}).handle ||
handleObj.handler).apply(matched.elem, args);
if(ret!==undefined){
if(( event.result=ret)===false){
event.preventDefault();
event.stopPropagation();
}}
}}
}
if(special.postDispatch){
special.postDispatch.call(this, event);
}
return event.result;
},
handlers: function(event, handlers){
var i, handleObj, sel, matchedHandlers, matchedSelectors,
handlerQueue=[],
delegateCount=handlers.delegateCount,
cur=event.target;
if(delegateCount &&
cur.nodeType &&
!(event.type==="click"&&event.button >=1)){
for(; cur!==this; cur=cur.parentNode||this){
if(cur.nodeType===1&&!(event.type==="click"&&cur.disabled===true)){
matchedHandlers=[];
matchedSelectors={};
for(i=0; i < delegateCount; i++){
handleObj=handlers[ i ];
sel=handleObj.selector + " ";
if(matchedSelectors[ sel ]===undefined){
matchedSelectors[ sel ]=handleObj.needsContext ?
jQuery(sel, this).index(cur) > -1 :
jQuery.find(sel, this, null, [ cur ]).length;
}
if(matchedSelectors[ sel ]){
matchedHandlers.push(handleObj);
}}
if(matchedHandlers.length){
handlerQueue.push({ elem: cur, handlers: matchedHandlers });
}}
}}
cur=this;
if(delegateCount < handlers.length){
handlerQueue.push({ elem: cur, handlers: handlers.slice(delegateCount) });
}
return handlerQueue;
},
addProp: function(name, hook){
Object.defineProperty(jQuery.Event.prototype, name, {
enumerable: true,
configurable: true,
get: isFunction(hook) ?
function(){
if(this.originalEvent){
return hook(this.originalEvent);
}} :
function(){
if(this.originalEvent){
return this.originalEvent[ name ];
}},
set: function(value){
Object.defineProperty(this, name, {
enumerable: true,
configurable: true,
writable: true,
value: value
});
}});
},
fix: function(originalEvent){
return originalEvent[ jQuery.expando ] ?
originalEvent :
new jQuery.Event(originalEvent);
},
special: {
load: {
noBubble: true
},
focus: {
trigger: function(){
if(this!==safeActiveElement()&&this.focus){
this.focus();
return false;
}},
delegateType: "focusin"
},
blur: {
trigger: function(){
if(this===safeActiveElement()&&this.blur){
this.blur();
return false;
}},
delegateType: "focusout"
},
click: {
trigger: function(){
if(this.type==="checkbox"&&this.click&&nodeName(this, "input")){
this.click();
return false;
}},
_default: function(event){
return nodeName(event.target, "a");
}},
beforeunload: {
postDispatch: function(event){
if(event.result!==undefined&&event.originalEvent){
event.originalEvent.returnValue=event.result;
}}
}}
};
jQuery.removeEvent=function(elem, type, handle){
if(elem.removeEventListener){
elem.removeEventListener(type, handle);
}};
jQuery.Event=function(src, props){
if(!(this instanceof jQuery.Event)){
return new jQuery.Event(src, props);
}
if(src&&src.type){
this.originalEvent=src;
this.type=src.type;
this.isDefaultPrevented=src.defaultPrevented ||
src.defaultPrevented===undefined &&
src.returnValue===false ?
returnTrue :
returnFalse;
this.target=(src.target&&src.target.nodeType===3) ?
src.target.parentNode :
src.target;
this.currentTarget=src.currentTarget;
this.relatedTarget=src.relatedTarget;
}else{
this.type=src;
}
if(props){
jQuery.extend(this, props);
}
this.timeStamp=src&&src.timeStamp||Date.now();
this[ jQuery.expando ]=true;
};
jQuery.Event.prototype={
constructor: jQuery.Event,
isDefaultPrevented: returnFalse,
isPropagationStopped: returnFalse,
isImmediatePropagationStopped: returnFalse,
isSimulated: false,
preventDefault: function(){
var e=this.originalEvent;
this.isDefaultPrevented=returnTrue;
if(e&&!this.isSimulated){
e.preventDefault();
}},
stopPropagation: function(){
var e=this.originalEvent;
this.isPropagationStopped=returnTrue;
if(e&&!this.isSimulated){
e.stopPropagation();
}},
stopImmediatePropagation: function(){
var e=this.originalEvent;
this.isImmediatePropagationStopped=returnTrue;
if(e&&!this.isSimulated){
e.stopImmediatePropagation();
}
this.stopPropagation();
}};
jQuery.each({
altKey: true,
bubbles: true,
cancelable: true,
changedTouches: true,
ctrlKey: true,
detail: true,
eventPhase: true,
metaKey: true,
pageX: true,
pageY: true,
shiftKey: true,
view: true,
"char": true,
charCode: true,
key: true,
keyCode: true,
button: true,
buttons: true,
clientX: true,
clientY: true,
offsetX: true,
offsetY: true,
pointerId: true,
pointerType: true,
screenX: true,
screenY: true,
targetTouches: true,
toElement: true,
touches: true,
which: function(event){
var button=event.button;
if(event.which==null&&rkeyEvent.test(event.type)){
return event.charCode!=null ? event.charCode:event.keyCode;
}
if(!event.which&&button!==undefined&&rmouseEvent.test(event.type)){
if(button & 1){
return 1;
}
if(button & 2){
return 3;
}
if(button & 4){
return 2;
}
return 0;
}
return event.which;
}}, jQuery.event.addProp);
jQuery.each({
mouseenter: "mouseover",
mouseleave: "mouseout",
pointerenter: "pointerover",
pointerleave: "pointerout"
}, function(orig, fix){
jQuery.event.special[ orig ]={
delegateType: fix,
bindType: fix,
handle: function(event){
var ret,
target=this,
related=event.relatedTarget,
handleObj=event.handleObj;
if(!related||(related!==target&&!jQuery.contains(target, related))){
event.type=handleObj.origType;
ret=handleObj.handler.apply(this, arguments);
event.type=fix;
}
return ret;
}};});
jQuery.fn.extend({
on: function(types, selector, data, fn){
return on(this, types, selector, data, fn);
},
one: function(types, selector, data, fn){
return on(this, types, selector, data, fn, 1);
},
off: function(types, selector, fn){
var handleObj, type;
if(types&&types.preventDefault&&types.handleObj){
handleObj=types.handleObj;
jQuery(types.delegateTarget).off(handleObj.namespace ?
handleObj.origType + "." + handleObj.namespace :
handleObj.origType,
handleObj.selector,
handleObj.handler
);
return this;
}
if(typeof types==="object"){
for(type in types){
this.off(type, selector, types[ type ]);
}
return this;
}
if(selector===false||typeof selector==="function"){
fn=selector;
selector=undefined;
}
if(fn===false){
fn=returnFalse;
}
return this.each(function(){
jQuery.event.remove(this, types, fn, selector);
});
}});
var
rxhtmlTag=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
rnoInnerhtml=/<script|<style|<link/i,
rchecked=/checked\s*(?:[^=]|=\s*.checked.)/i,
rcleanScript=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
function manipulationTarget(elem, content){
if(nodeName(elem, "table") &&
nodeName(content.nodeType!==11 ? content:content.firstChild, "tr")){
return jQuery(elem).children("tbody")[ 0 ]||elem;
}
return elem;
}
function disableScript(elem){
elem.type=(elem.getAttribute("type")!==null) + "/" + elem.type;
return elem;
}
function restoreScript(elem){
if(( elem.type||"").slice(0, 5)==="true/"){
elem.type=elem.type.slice(5);
}else{
elem.removeAttribute("type");
}
return elem;
}
function cloneCopyEvent(src, dest){
var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;
if(dest.nodeType!==1){
return;
}
if(dataPriv.hasData(src)){
pdataOld=dataPriv.access(src);
pdataCur=dataPriv.set(dest, pdataOld);
events=pdataOld.events;
if(events){
delete pdataCur.handle;
pdataCur.events={};
for(type in events){
for(i=0, l=events[ type ].length; i < l; i++){
jQuery.event.add(dest, type, events[ type ][ i ]);
}}
}}
if(dataUser.hasData(src)){
udataOld=dataUser.access(src);
udataCur=jQuery.extend({}, udataOld);
dataUser.set(dest, udataCur);
}}
function fixInput(src, dest){
var nodeName=dest.nodeName.toLowerCase();
if(nodeName==="input"&&rcheckableType.test(src.type)){
dest.checked=src.checked;
}else if(nodeName==="input"||nodeName==="textarea"){
dest.defaultValue=src.defaultValue;
}}
function domManip(collection, args, callback, ignored){
args=concat.apply([], args);
var fragment, first, scripts, hasScripts, node, doc,
i=0,
l=collection.length,
iNoClone=l - 1,
value=args[ 0 ],
valueIsFunction=isFunction(value);
if(valueIsFunction ||
(l > 1&&typeof value==="string" &&
!support.checkClone&&rchecked.test(value))){
return collection.each(function(index){
var self=collection.eq(index);
if(valueIsFunction){
args[ 0 ]=value.call(this, index, self.html());
}
domManip(self, args, callback, ignored);
});
}
if(l){
fragment=buildFragment(args, collection[ 0 ].ownerDocument, false, collection, ignored);
first=fragment.firstChild;
if(fragment.childNodes.length===1){
fragment=first;
}
if(first||ignored){
scripts=jQuery.map(getAll(fragment, "script"), disableScript);
hasScripts=scripts.length;
for(; i < l; i++){
node=fragment;
if(i!==iNoClone){
node=jQuery.clone(node, true, true);
if(hasScripts){
jQuery.merge(scripts, getAll(node, "script"));
}}
callback.call(collection[ i ], node, i);
}
if(hasScripts){
doc=scripts[ scripts.length - 1 ].ownerDocument;
jQuery.map(scripts, restoreScript);
for(i=0; i < hasScripts; i++){
node=scripts[ i ];
if(rscriptType.test(node.type||"") &&
!dataPriv.access(node, "globalEval") &&
jQuery.contains(doc, node)){
if(node.src&&(node.type||"").toLowerCase()!=="module"){
if(jQuery._evalUrl){
jQuery._evalUrl(node.src);
}}else{
DOMEval(node.textContent.replace(rcleanScript, ""), doc, node);
}}
}}
}}
return collection;
}
function remove(elem, selector, keepData){
var node,
nodes=selector ? jQuery.filter(selector, elem):elem,
i=0;
for(;(node=nodes[ i ])!=null; i++){
if(!keepData&&node.nodeType===1){
jQuery.cleanData(getAll(node));
}
if(node.parentNode){
if(keepData&&jQuery.contains(node.ownerDocument, node)){
setGlobalEval(getAll(node, "script"));
}
node.parentNode.removeChild(node);
}}
return elem;
}
jQuery.extend({
htmlPrefilter: function(html){
return html.replace(rxhtmlTag, "<$1></$2>");
},
clone: function(elem, dataAndEvents, deepDataAndEvents){
var i, l, srcElements, destElements,
clone=elem.cloneNode(true),
inPage=jQuery.contains(elem.ownerDocument, elem);
if(!support.noCloneChecked&&(elem.nodeType===1||elem.nodeType===11) &&
!jQuery.isXMLDoc(elem)){
destElements=getAll(clone);
srcElements=getAll(elem);
for(i=0, l=srcElements.length; i < l; i++){
fixInput(srcElements[ i ], destElements[ i ]);
}}
if(dataAndEvents){
if(deepDataAndEvents){
srcElements=srcElements||getAll(elem);
destElements=destElements||getAll(clone);
for(i=0, l=srcElements.length; i < l; i++){
cloneCopyEvent(srcElements[ i ], destElements[ i ]);
}}else{
cloneCopyEvent(elem, clone);
}}
destElements=getAll(clone, "script");
if(destElements.length > 0){
setGlobalEval(destElements, !inPage&&getAll(elem, "script"));
}
return clone;
},
cleanData: function(elems){
var data, elem, type,
special=jQuery.event.special,
i=0;
for(;(elem=elems[ i ])!==undefined; i++){
if(acceptData(elem)){
if(( data=elem[ dataPriv.expando ])){
if(data.events){
for(type in data.events){
if(special[ type ]){
jQuery.event.remove(elem, type);
}else{
jQuery.removeEvent(elem, type, data.handle);
}}
}
elem[ dataPriv.expando ]=undefined;
}
if(elem[ dataUser.expando ]){
elem[ dataUser.expando ]=undefined;
}}
}}
});
jQuery.fn.extend({
detach: function(selector){
return remove(this, selector, true);
},
remove: function(selector){
return remove(this, selector);
},
text: function(value){
return access(this, function(value){
return value===undefined ?
jQuery.text(this) :
this.empty().each(function(){
if(this.nodeType===1||this.nodeType===11||this.nodeType===9){
this.textContent=value;
}});
}, null, value, arguments.length);
},
append: function(){
return domManip(this, arguments, function(elem){
if(this.nodeType===1||this.nodeType===11||this.nodeType===9){
var target=manipulationTarget(this, elem);
target.appendChild(elem);
}});
},
prepend: function(){
return domManip(this, arguments, function(elem){
if(this.nodeType===1||this.nodeType===11||this.nodeType===9){
var target=manipulationTarget(this, elem);
target.insertBefore(elem, target.firstChild);
}});
},
before: function(){
return domManip(this, arguments, function(elem){
if(this.parentNode){
this.parentNode.insertBefore(elem, this);
}});
},
after: function(){
return domManip(this, arguments, function(elem){
if(this.parentNode){
this.parentNode.insertBefore(elem, this.nextSibling);
}});
},
empty: function(){
var elem,
i=0;
for(;(elem=this[ i ])!=null; i++){
if(elem.nodeType===1){
jQuery.cleanData(getAll(elem, false));
elem.textContent="";
}}
return this;
},
clone: function(dataAndEvents, deepDataAndEvents){
dataAndEvents=dataAndEvents==null ? false:dataAndEvents;
deepDataAndEvents=deepDataAndEvents==null ? dataAndEvents:deepDataAndEvents;
return this.map(function(){
return jQuery.clone(this, dataAndEvents, deepDataAndEvents);
});
},
html: function(value){
return access(this, function(value){
var elem=this[ 0 ]||{},
i=0,
l=this.length;
if(value===undefined&&elem.nodeType===1){
return elem.innerHTML;
}
if(typeof value==="string"&&!rnoInnerhtml.test(value) &&
!wrapMap[(rtagName.exec(value)||[ "", "" ])[ 1 ].toLowerCase() ]){
value=jQuery.htmlPrefilter(value);
try {
for(; i < l; i++){
elem=this[ i ]||{};
if(elem.nodeType===1){
jQuery.cleanData(getAll(elem, false));
elem.innerHTML=value;
}}
elem=0;
} catch(e){}}
if(elem){
this.empty().append(value);
}}, null, value, arguments.length);
},
replaceWith: function(){
var ignored=[];
return domManip(this, arguments, function(elem){
var parent=this.parentNode;
if(jQuery.inArray(this, ignored) < 0){
jQuery.cleanData(getAll(this));
if(parent){
parent.replaceChild(elem, this);
}}
}, ignored);
}});
jQuery.each({
appendTo: "append",
prependTo: "prepend",
insertBefore: "before",
insertAfter: "after",
replaceAll: "replaceWith"
}, function(name, original){
jQuery.fn[ name ]=function(selector){
var elems,
ret=[],
insert=jQuery(selector),
last=insert.length - 1,
i=0;
for(; i <=last; i++){
elems=i===last ? this:this.clone(true);
jQuery(insert[ i ])[ original ](elems);
push.apply(ret, elems.get());
}
return this.pushStack(ret);
};});
var rnumnonpx=new RegExp("^(" + pnum + ")(?!px)[a-z%]+$", "i");
var getStyles=function(elem){
var view=elem.ownerDocument.defaultView;
if(!view||!view.opener){
view=window;
}
return view.getComputedStyle(elem);
};
var rboxStyle=new RegExp(cssExpand.join("|"), "i");
(function(){
function computeStyleTests(){
if(!div){
return;
}
container.style.cssText="position:absolute;left:-11111px;width:60px;" +
"margin-top:1px;padding:0;border:0";
div.style.cssText =
"position:relative;display:block;box-sizing:border-box;overflow:scroll;" +
"margin:auto;border:1px;padding:1px;" +
"width:60%;top:1%";
documentElement.appendChild(container).appendChild(div);
var divStyle=window.getComputedStyle(div);
pixelPositionVal=divStyle.top!=="1%";
reliableMarginLeftVal=roundPixelMeasures(divStyle.marginLeft)===12;
div.style.right="60%";
pixelBoxStylesVal=roundPixelMeasures(divStyle.right)===36;
boxSizingReliableVal=roundPixelMeasures(divStyle.width)===36;
div.style.position="absolute";
scrollboxSizeVal=div.offsetWidth===36||"absolute";
documentElement.removeChild(container);
div=null;
}
function roundPixelMeasures(measure){
return Math.round(parseFloat(measure));
}
var pixelPositionVal, boxSizingReliableVal, scrollboxSizeVal, pixelBoxStylesVal,
reliableMarginLeftVal,
container=document.createElement("div"),
div=document.createElement("div");
if(!div.style){
return;
}
div.style.backgroundClip="content-box";
div.cloneNode(true).style.backgroundClip="";
support.clearCloneStyle=div.style.backgroundClip==="content-box";
jQuery.extend(support, {
boxSizingReliable: function(){
computeStyleTests();
return boxSizingReliableVal;
},
pixelBoxStyles: function(){
computeStyleTests();
return pixelBoxStylesVal;
},
pixelPosition: function(){
computeStyleTests();
return pixelPositionVal;
},
reliableMarginLeft: function(){
computeStyleTests();
return reliableMarginLeftVal;
},
scrollboxSize: function(){
computeStyleTests();
return scrollboxSizeVal;
}});
})();
function curCSS(elem, name, computed){
var width, minWidth, maxWidth, ret,
style=elem.style;
computed=computed||getStyles(elem);
if(computed){
ret=computed.getPropertyValue(name)||computed[ name ];
if(ret===""&&!jQuery.contains(elem.ownerDocument, elem)){
ret=jQuery.style(elem, name);
}
if(!support.pixelBoxStyles()&&rnumnonpx.test(ret)&&rboxStyle.test(name)){
width=style.width;
minWidth=style.minWidth;
maxWidth=style.maxWidth;
style.minWidth=style.maxWidth=style.width=ret;
ret=computed.width;
style.width=width;
style.minWidth=minWidth;
style.maxWidth=maxWidth;
}}
return ret!==undefined ?
ret + "" :
ret;
}
function addGetHookIf(conditionFn, hookFn){
return {
get: function(){
if(conditionFn()){
delete this.get;
return;
}
return(this.get=hookFn).apply(this, arguments);
}};}
var
rdisplayswap=/^(none|table(?!-c[ea]).+)/,
rcustomProp=/^--/,
cssShow={ position: "absolute", visibility: "hidden", display: "block" },
cssNormalTransform={
letterSpacing: "0",
fontWeight: "400"
},
cssPrefixes=[ "Webkit", "Moz", "ms" ],
emptyStyle=document.createElement("div").style;
function vendorPropName(name){
if(name in emptyStyle){
return name;
}
var capName=name[ 0 ].toUpperCase() + name.slice(1),
i=cssPrefixes.length;
while(i--){
name=cssPrefixes[ i ] + capName;
if(name in emptyStyle){
return name;
}}
}
function finalPropName(name){
var ret=jQuery.cssProps[ name ];
if(!ret){
ret=jQuery.cssProps[ name ]=vendorPropName(name)||name;
}
return ret;
}
function setPositiveNumber(elem, value, subtract){
var matches=rcssNum.exec(value);
return matches ?
Math.max(0, matches[ 2 ] -(subtract||0)) +(matches[ 3 ]||"px") :
value;
}
function boxModelAdjustment(elem, dimension, box, isBorderBox, styles, computedVal){
var i=dimension==="width" ? 1:0,
extra=0,
delta=0;
if(box===(isBorderBox ? "border":"content")){
return 0;
}
for(; i < 4; i +=2){
if(box==="margin"){
delta +=jQuery.css(elem, box + cssExpand[ i ], true, styles);
}
if(!isBorderBox){
delta +=jQuery.css(elem, "padding" + cssExpand[ i ], true, styles);
if(box!=="padding"){
delta +=jQuery.css(elem, "border" + cssExpand[ i ] + "Width", true, styles);
}else{
extra +=jQuery.css(elem, "border" + cssExpand[ i ] + "Width", true, styles);
}}else{
if(box==="content"){
delta -=jQuery.css(elem, "padding" + cssExpand[ i ], true, styles);
}
if(box!=="margin"){
delta -=jQuery.css(elem, "border" + cssExpand[ i ] + "Width", true, styles);
}}
}
if(!isBorderBox&&computedVal >=0){
delta +=Math.max(0, Math.ceil(elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice(1) ] -
computedVal -
delta -
extra -
0.5
));
}
return delta;
}
function getWidthOrHeight(elem, dimension, extra){
var styles=getStyles(elem),
val=curCSS(elem, dimension, styles),
isBorderBox=jQuery.css(elem, "boxSizing", false, styles)==="border-box",
valueIsBorderBox=isBorderBox;
if(rnumnonpx.test(val)){
if(!extra){
return val;
}
val="auto";
}
valueIsBorderBox=valueIsBorderBox &&
(support.boxSizingReliable()||val===elem.style[ dimension ]);
if(val==="auto" ||
!parseFloat(val)&&jQuery.css(elem, "display", false, styles)==="inline"){
val=elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice(1) ];
valueIsBorderBox=true;
}
val=parseFloat(val)||0;
return(val +
boxModelAdjustment(
elem,
dimension,
extra||(isBorderBox ? "border":"content"),
valueIsBorderBox,
styles,
val
)
) + "px";
}
jQuery.extend({
cssHooks: {
opacity: {
get: function(elem, computed){
if(computed){
var ret=curCSS(elem, "opacity");
return ret==="" ? "1":ret;
}}
}},
cssNumber: {
"animationIterationCount": true,
"columnCount": true,
"fillOpacity": true,
"flexGrow": true,
"flexShrink": true,
"fontWeight": true,
"lineHeight": true,
"opacity": true,
"order": true,
"orphans": true,
"widows": true,
"zIndex": true,
"zoom": true
},
cssProps: {},
style: function(elem, name, value, extra){
if(!elem||elem.nodeType===3||elem.nodeType===8||!elem.style){
return;
}
var ret, type, hooks,
origName=camelCase(name),
isCustomProp=rcustomProp.test(name),
style=elem.style;
if(!isCustomProp){
name=finalPropName(origName);
}
hooks=jQuery.cssHooks[ name ]||jQuery.cssHooks[ origName ];
if(value!==undefined){
type=typeof value;
if(type==="string"&&(ret=rcssNum.exec(value))&&ret[ 1 ]){
value=adjustCSS(elem, name, ret);
type="number";
}
if(value==null||value!==value){
return;
}
if(type==="number"){
value +=ret&&ret[ 3 ]||(jQuery.cssNumber[ origName ] ? "":"px");
}
if(!support.clearCloneStyle&&value===""&&name.indexOf("background")===0){
style[ name ]="inherit";
}
if(!hooks||!("set" in hooks) ||
(value=hooks.set(elem, value, extra))!==undefined){
if(isCustomProp){
style.setProperty(name, value);
}else{
style[ name ]=value;
}}
}else{
if(hooks&&"get" in hooks &&
(ret=hooks.get(elem, false, extra))!==undefined){
return ret;
}
return style[ name ];
}},
css: function(elem, name, extra, styles){
var val, num, hooks,
origName=camelCase(name),
isCustomProp=rcustomProp.test(name);
if(!isCustomProp){
name=finalPropName(origName);
}
hooks=jQuery.cssHooks[ name ]||jQuery.cssHooks[ origName ];
if(hooks&&"get" in hooks){
val=hooks.get(elem, true, extra);
}
if(val===undefined){
val=curCSS(elem, name, styles);
}
if(val==="normal"&&name in cssNormalTransform){
val=cssNormalTransform[ name ];
}
if(extra===""||extra){
num=parseFloat(val);
return extra===true||isFinite(num) ? num||0:val;
}
return val;
}});
jQuery.each([ "height", "width" ], function(i, dimension){
jQuery.cssHooks[ dimension ]={
get: function(elem, computed, extra){
if(computed){
return rdisplayswap.test(jQuery.css(elem, "display")) &&
(!elem.getClientRects().length||!elem.getBoundingClientRect().width) ?
swap(elem, cssShow, function(){
return getWidthOrHeight(elem, dimension, extra);
}) :
getWidthOrHeight(elem, dimension, extra);
}},
set: function(elem, value, extra){
var matches,
styles=getStyles(elem),
isBorderBox=jQuery.css(elem, "boxSizing", false, styles)==="border-box",
subtract=extra&&boxModelAdjustment(
elem,
dimension,
extra,
isBorderBox,
styles
);
if(isBorderBox&&support.scrollboxSize()===styles.position){
subtract -=Math.ceil(elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice(1) ] -
parseFloat(styles[ dimension ]) -
boxModelAdjustment(elem, dimension, "border", false, styles) -
0.5
);
}
if(subtract&&(matches=rcssNum.exec(value)) &&
(matches[ 3 ]||"px")!=="px"){
elem.style[ dimension ]=value;
value=jQuery.css(elem, dimension);
}
return setPositiveNumber(elem, value, subtract);
}};});
jQuery.cssHooks.marginLeft=addGetHookIf(support.reliableMarginLeft,
function(elem, computed){
if(computed){
return(parseFloat(curCSS(elem, "marginLeft")) ||
elem.getBoundingClientRect().left -
swap(elem, { marginLeft: 0 }, function(){
return elem.getBoundingClientRect().left;
})
) + "px";
}}
);
jQuery.each({
margin: "",
padding: "",
border: "Width"
}, function(prefix, suffix){
jQuery.cssHooks[ prefix + suffix ]={
expand: function(value){
var i=0,
expanded={},
parts=typeof value==="string" ? value.split(" "):[ value ];
for(; i < 4; i++){
expanded[ prefix + cssExpand[ i ] + suffix ] =
parts[ i ]||parts[ i - 2 ]||parts[ 0 ];
}
return expanded;
}};
if(prefix!=="margin"){
jQuery.cssHooks[ prefix + suffix ].set=setPositiveNumber;
}});
jQuery.fn.extend({
css: function(name, value){
return access(this, function(elem, name, value){
var styles, len,
map={},
i=0;
if(Array.isArray(name)){
styles=getStyles(elem);
len=name.length;
for(; i < len; i++){
map[ name[ i ] ]=jQuery.css(elem, name[ i ], false, styles);
}
return map;
}
return value!==undefined ?
jQuery.style(elem, name, value) :
jQuery.css(elem, name);
}, name, value, arguments.length > 1);
}});
function Tween(elem, options, prop, end, easing){
return new Tween.prototype.init(elem, options, prop, end, easing);
}
jQuery.Tween=Tween;
Tween.prototype={
constructor: Tween,
init: function(elem, options, prop, end, easing, unit){
this.elem=elem;
this.prop=prop;
this.easing=easing||jQuery.easing._default;
this.options=options;
this.start=this.now=this.cur();
this.end=end;
this.unit=unit||(jQuery.cssNumber[ prop ] ? "":"px");
},
cur: function(){
var hooks=Tween.propHooks[ this.prop ];
return hooks&&hooks.get ?
hooks.get(this) :
Tween.propHooks._default.get(this);
},
run: function(percent){
var eased,
hooks=Tween.propHooks[ this.prop ];
if(this.options.duration){
this.pos=eased=jQuery.easing[ this.easing ](
percent, this.options.duration * percent, 0, 1, this.options.duration
);
}else{
this.pos=eased=percent;
}
this.now=(this.end - this.start) * eased + this.start;
if(this.options.step){
this.options.step.call(this.elem, this.now, this);
}
if(hooks&&hooks.set){
hooks.set(this);
}else{
Tween.propHooks._default.set(this);
}
return this;
}};
Tween.prototype.init.prototype=Tween.prototype;
Tween.propHooks={
_default: {
get: function(tween){
var result;
if(tween.elem.nodeType!==1 ||
tween.elem[ tween.prop ]!=null&&tween.elem.style[ tween.prop ]==null){
return tween.elem[ tween.prop ];
}
result=jQuery.css(tween.elem, tween.prop, "");
return !result||result==="auto" ? 0:result;
},
set: function(tween){
if(jQuery.fx.step[ tween.prop ]){
jQuery.fx.step[ tween.prop ](tween);
}else if(tween.elem.nodeType===1 &&
(tween.elem.style[ jQuery.cssProps[ tween.prop ] ]!=null ||
jQuery.cssHooks[ tween.prop ])){
jQuery.style(tween.elem, tween.prop, tween.now + tween.unit);
}else{
tween.elem[ tween.prop ]=tween.now;
}}
}};
Tween.propHooks.scrollTop=Tween.propHooks.scrollLeft={
set: function(tween){
if(tween.elem.nodeType&&tween.elem.parentNode){
tween.elem[ tween.prop ]=tween.now;
}}
};
jQuery.easing={
linear: function(p){
return p;
},
swing: function(p){
return 0.5 - Math.cos(p * Math.PI) / 2;
},
_default: "swing"
};
jQuery.fx=Tween.prototype.init;
jQuery.fx.step={};
var
fxNow, inProgress,
rfxtypes=/^(?:toggle|show|hide)$/,
rrun=/queueHooks$/;
function schedule(){
if(inProgress){
if(document.hidden===false&&window.requestAnimationFrame){
window.requestAnimationFrame(schedule);
}else{
window.setTimeout(schedule, jQuery.fx.interval);
}
jQuery.fx.tick();
}}
function createFxNow(){
window.setTimeout(function(){
fxNow=undefined;
});
return(fxNow=Date.now());
}
function genFx(type, includeWidth){
var which,
i=0,
attrs={ height: type };
includeWidth=includeWidth ? 1:0;
for(; i < 4; i +=2 - includeWidth){
which=cssExpand[ i ];
attrs[ "margin" + which ]=attrs[ "padding" + which ]=type;
}
if(includeWidth){
attrs.opacity=attrs.width=type;
}
return attrs;
}
function createTween(value, prop, animation){
var tween,
collection=(Animation.tweeners[ prop ]||[]).concat(Animation.tweeners[ "*" ]),
index=0,
length=collection.length;
for(; index < length; index++){
if(( tween=collection[ index ].call(animation, prop, value))){
return tween;
}}
}
function defaultPrefilter(elem, props, opts){
var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
isBox="width" in props||"height" in props,
anim=this,
orig={},
style=elem.style,
hidden=elem.nodeType&&isHiddenWithinTree(elem),
dataShow=dataPriv.get(elem, "fxshow");
if(!opts.queue){
hooks=jQuery._queueHooks(elem, "fx");
if(hooks.unqueued==null){
hooks.unqueued=0;
oldfire=hooks.empty.fire;
hooks.empty.fire=function(){
if(!hooks.unqueued){
oldfire();
}};}
hooks.unqueued++;
anim.always(function(){
anim.always(function(){
hooks.unqueued--;
if(!jQuery.queue(elem, "fx").length){
hooks.empty.fire();
}});
});
}
for(prop in props){
value=props[ prop ];
if(rfxtypes.test(value)){
delete props[ prop ];
toggle=toggle||value==="toggle";
if(value===(hidden ? "hide":"show")){
if(value==="show"&&dataShow&&dataShow[ prop ]!==undefined){
hidden=true;
}else{
continue;
}}
orig[ prop ]=dataShow&&dataShow[ prop ]||jQuery.style(elem, prop);
}}
propTween = !jQuery.isEmptyObject(props);
if(!propTween&&jQuery.isEmptyObject(orig)){
return;
}
if(isBox&&elem.nodeType===1){
opts.overflow=[ style.overflow, style.overflowX, style.overflowY ];
restoreDisplay=dataShow&&dataShow.display;
if(restoreDisplay==null){
restoreDisplay=dataPriv.get(elem, "display");
}
display=jQuery.css(elem, "display");
if(display==="none"){
if(restoreDisplay){
display=restoreDisplay;
}else{
showHide([ elem ], true);
restoreDisplay=elem.style.display||restoreDisplay;
display=jQuery.css(elem, "display");
showHide([ elem ]);
}}
if(display==="inline"||display==="inline-block"&&restoreDisplay!=null){
if(jQuery.css(elem, "float")==="none"){
if(!propTween){
anim.done(function(){
style.display=restoreDisplay;
});
if(restoreDisplay==null){
display=style.display;
restoreDisplay=display==="none" ? "":display;
}}
style.display="inline-block";
}}
}
if(opts.overflow){
style.overflow="hidden";
anim.always(function(){
style.overflow=opts.overflow[ 0 ];
style.overflowX=opts.overflow[ 1 ];
style.overflowY=opts.overflow[ 2 ];
});
}
propTween=false;
for(prop in orig){
if(!propTween){
if(dataShow){
if("hidden" in dataShow){
hidden=dataShow.hidden;
}}else{
dataShow=dataPriv.access(elem, "fxshow", { display: restoreDisplay });
}
if(toggle){
dataShow.hidden = !hidden;
}
if(hidden){
showHide([ elem ], true);
}
anim.done(function(){
if(!hidden){
showHide([ elem ]);
}
dataPriv.remove(elem, "fxshow");
for(prop in orig){
jQuery.style(elem, prop, orig[ prop ]);
}});
}
propTween=createTween(hidden ? dataShow[ prop ]:0, prop, anim);
if(!(prop in dataShow)){
dataShow[ prop ]=propTween.start;
if(hidden){
propTween.end=propTween.start;
propTween.start=0;
}}
}}
function propFilter(props, specialEasing){
var index, name, easing, value, hooks;
for(index in props){
name=camelCase(index);
easing=specialEasing[ name ];
value=props[ index ];
if(Array.isArray(value)){
easing=value[ 1 ];
value=props[ index ]=value[ 0 ];
}
if(index!==name){
props[ name ]=value;
delete props[ index ];
}
hooks=jQuery.cssHooks[ name ];
if(hooks&&"expand" in hooks){
value=hooks.expand (value);
delete props[ name ];
for(index in value){
if(!(index in props)){
props[ index ]=value[ index ];
specialEasing[ index ]=easing;
}}
}else{
specialEasing[ name ]=easing;
}}
}
function Animation(elem, properties, options){
var result,
stopped,
index=0,
length=Animation.prefilters.length,
deferred=jQuery.Deferred().always(function(){
delete tick.elem;
}),
tick=function(){
if(stopped){
return false;
}
var currentTime=fxNow||createFxNow(),
remaining=Math.max(0, animation.startTime + animation.duration - currentTime),
temp=remaining / animation.duration||0,
percent=1 - temp,
index=0,
length=animation.tweens.length;
for(; index < length; index++){
animation.tweens[ index ].run(percent);
}
deferred.notifyWith(elem, [ animation, percent, remaining ]);
if(percent < 1&&length){
return remaining;
}
if(!length){
deferred.notifyWith(elem, [ animation, 1, 0 ]);
}
deferred.resolveWith(elem, [ animation ]);
return false;
},
animation=deferred.promise({
elem: elem,
props: jQuery.extend({}, properties),
opts: jQuery.extend(true, {
specialEasing: {},
easing: jQuery.easing._default
}, options),
originalProperties: properties,
originalOptions: options,
startTime: fxNow||createFxNow(),
duration: options.duration,
tweens: [],
createTween: function(prop, end){
var tween=jQuery.Tween(elem, animation.opts, prop, end,
animation.opts.specialEasing[ prop ]||animation.opts.easing);
animation.tweens.push(tween);
return tween;
},
stop: function(gotoEnd){
var index=0,
length=gotoEnd ? animation.tweens.length:0;
if(stopped){
return this;
}
stopped=true;
for(; index < length; index++){
animation.tweens[ index ].run(1);
}
if(gotoEnd){
deferred.notifyWith(elem, [ animation, 1, 0 ]);
deferred.resolveWith(elem, [ animation, gotoEnd ]);
}else{
deferred.rejectWith(elem, [ animation, gotoEnd ]);
}
return this;
}}),
props=animation.props;
propFilter(props, animation.opts.specialEasing);
for(; index < length; index++){
result=Animation.prefilters[ index ].call(animation, elem, props, animation.opts);
if(result){
if(isFunction(result.stop)){
jQuery._queueHooks(animation.elem, animation.opts.queue).stop =
result.stop.bind(result);
}
return result;
}}
jQuery.map(props, createTween, animation);
if(isFunction(animation.opts.start)){
animation.opts.start.call(elem, animation);
}
animation
.progress(animation.opts.progress)
.done(animation.opts.done, animation.opts.complete)
.fail(animation.opts.fail)
.always(animation.opts.always);
jQuery.fx.timer(jQuery.extend(tick, {
elem: elem,
anim: animation,
queue: animation.opts.queue
})
);
return animation;
}
jQuery.Animation=jQuery.extend(Animation, {
tweeners: {
"*": [ function(prop, value){
var tween=this.createTween(prop, value);
adjustCSS(tween.elem, prop, rcssNum.exec(value), tween);
return tween;
} ]
},
tweener: function(props, callback){
if(isFunction(props)){
callback=props;
props=[ "*" ];
}else{
props=props.match(rnothtmlwhite);
}
var prop,
index=0,
length=props.length;
for(; index < length; index++){
prop=props[ index ];
Animation.tweeners[ prop ]=Animation.tweeners[ prop ]||[];
Animation.tweeners[ prop ].unshift(callback);
}},
prefilters: [ defaultPrefilter ],
prefilter: function(callback, prepend){
if(prepend){
Animation.prefilters.unshift(callback);
}else{
Animation.prefilters.push(callback);
}}
});
jQuery.speed=function(speed, easing, fn){
var opt=speed&&typeof speed==="object" ? jQuery.extend({}, speed):{
complete: fn||!fn&&easing ||
isFunction(speed)&&speed,
duration: speed,
easing: fn&&easing||easing&&!isFunction(easing)&&easing
};
if(jQuery.fx.off){
opt.duration=0;
}else{
if(typeof opt.duration!=="number"){
if(opt.duration in jQuery.fx.speeds){
opt.duration=jQuery.fx.speeds[ opt.duration ];
}else{
opt.duration=jQuery.fx.speeds._default;
}}
}
if(opt.queue==null||opt.queue===true){
opt.queue="fx";
}
opt.old=opt.complete;
opt.complete=function(){
if(isFunction(opt.old)){
opt.old.call(this);
}
if(opt.queue){
jQuery.dequeue(this, opt.queue);
}};
return opt;
};
jQuery.fn.extend({
fadeTo: function(speed, to, easing, callback){
return this.filter(isHiddenWithinTree).css("opacity", 0).show()
.end().animate({ opacity: to }, speed, easing, callback);
},
animate: function(prop, speed, easing, callback){
var empty=jQuery.isEmptyObject(prop),
optall=jQuery.speed(speed, easing, callback),
doAnimation=function(){
var anim=Animation(this, jQuery.extend({}, prop), optall);
if(empty||dataPriv.get(this, "finish")){
anim.stop(true);
}};
doAnimation.finish=doAnimation;
return empty||optall.queue===false ?
this.each(doAnimation) :
this.queue(optall.queue, doAnimation);
},
stop: function(type, clearQueue, gotoEnd){
var stopQueue=function(hooks){
var stop=hooks.stop;
delete hooks.stop;
stop(gotoEnd);
};
if(typeof type!=="string"){
gotoEnd=clearQueue;
clearQueue=type;
type=undefined;
}
if(clearQueue&&type!==false){
this.queue(type||"fx", []);
}
return this.each(function(){
var dequeue=true,
index=type!=null&&type + "queueHooks",
timers=jQuery.timers,
data=dataPriv.get(this);
if(index){
if(data[ index ]&&data[ index ].stop){
stopQueue(data[ index ]);
}}else{
for(index in data){
if(data[ index ]&&data[ index ].stop&&rrun.test(index)){
stopQueue(data[ index ]);
}}
}
for(index=timers.length; index--;){
if(timers[ index ].elem===this &&
(type==null||timers[ index ].queue===type)){
timers[ index ].anim.stop(gotoEnd);
dequeue=false;
timers.splice(index, 1);
}}
if(dequeue||!gotoEnd){
jQuery.dequeue(this, type);
}});
},
finish: function(type){
if(type!==false){
type=type||"fx";
}
return this.each(function(){
var index,
data=dataPriv.get(this),
queue=data[ type + "queue" ],
hooks=data[ type + "queueHooks" ],
timers=jQuery.timers,
length=queue ? queue.length:0;
data.finish=true;
jQuery.queue(this, type, []);
if(hooks&&hooks.stop){
hooks.stop.call(this, true);
}
for(index=timers.length; index--;){
if(timers[ index ].elem===this&&timers[ index ].queue===type){
timers[ index ].anim.stop(true);
timers.splice(index, 1);
}}
for(index=0; index < length; index++){
if(queue[ index ]&&queue[ index ].finish){
queue[ index ].finish.call(this);
}}
delete data.finish;
});
}});
jQuery.each([ "toggle", "show", "hide" ], function(i, name){
var cssFn=jQuery.fn[ name ];
jQuery.fn[ name ]=function(speed, easing, callback){
return speed==null||typeof speed==="boolean" ?
cssFn.apply(this, arguments) :
this.animate(genFx(name, true), speed, easing, callback);
};});
jQuery.each({
slideDown: genFx("show"),
slideUp: genFx("hide"),
slideToggle: genFx("toggle"),
fadeIn: { opacity: "show" },
fadeOut: { opacity: "hide" },
fadeToggle: { opacity: "toggle" }}, function(name, props){
jQuery.fn[ name ]=function(speed, easing, callback){
return this.animate(props, speed, easing, callback);
};});
jQuery.timers=[];
jQuery.fx.tick=function(){
var timer,
i=0,
timers=jQuery.timers;
fxNow=Date.now();
for(; i < timers.length; i++){
timer=timers[ i ];
if(!timer()&&timers[ i ]===timer){
timers.splice(i--, 1);
}}
if(!timers.length){
jQuery.fx.stop();
}
fxNow=undefined;
};
jQuery.fx.timer=function(timer){
jQuery.timers.push(timer);
jQuery.fx.start();
};
jQuery.fx.interval=13;
jQuery.fx.start=function(){
if(inProgress){
return;
}
inProgress=true;
schedule();
};
jQuery.fx.stop=function(){
inProgress=null;
};
jQuery.fx.speeds={
slow: 600,
fast: 200,
_default: 400
};
jQuery.fn.delay=function(time, type){
time=jQuery.fx ? jQuery.fx.speeds[ time ]||time:time;
type=type||"fx";
return this.queue(type, function(next, hooks){
var timeout=window.setTimeout(next, time);
hooks.stop=function(){
window.clearTimeout(timeout);
};});
};
(function(){
var input=document.createElement("input"),
select=document.createElement("select"),
opt=select.appendChild(document.createElement("option"));
input.type="checkbox";
support.checkOn=input.value!=="";
support.optSelected=opt.selected;
input=document.createElement("input");
input.value="t";
input.type="radio";
support.radioValue=input.value==="t";
})();
var boolHook,
attrHandle=jQuery.expr.attrHandle;
jQuery.fn.extend({
attr: function(name, value){
return access(this, jQuery.attr, name, value, arguments.length > 1);
},
removeAttr: function(name){
return this.each(function(){
jQuery.removeAttr(this, name);
});
}});
jQuery.extend({
attr: function(elem, name, value){
var ret, hooks,
nType=elem.nodeType;
if(nType===3||nType===8||nType===2){
return;
}
if(typeof elem.getAttribute==="undefined"){
return jQuery.prop(elem, name, value);
}
if(nType!==1||!jQuery.isXMLDoc(elem)){
hooks=jQuery.attrHooks[ name.toLowerCase() ] ||
(jQuery.expr.match.bool.test(name) ? boolHook:undefined);
}
if(value!==undefined){
if(value===null){
jQuery.removeAttr(elem, name);
return;
}
if(hooks&&"set" in hooks &&
(ret=hooks.set(elem, value, name))!==undefined){
return ret;
}
elem.setAttribute(name, value + "");
return value;
}
if(hooks&&"get" in hooks&&(ret=hooks.get(elem, name))!==null){
return ret;
}
ret=jQuery.find.attr(elem, name);
return ret==null ? undefined:ret;
},
attrHooks: {
type: {
set: function(elem, value){
if(!support.radioValue&&value==="radio" &&
nodeName(elem, "input")){
var val=elem.value;
elem.setAttribute("type", value);
if(val){
elem.value=val;
}
return value;
}}
}},
removeAttr: function(elem, value){
var name,
i=0,
attrNames=value&&value.match(rnothtmlwhite);
if(attrNames&&elem.nodeType===1){
while(( name=attrNames[ i++ ])){
elem.removeAttribute(name);
}}
}});
boolHook={
set: function(elem, value, name){
if(value===false){
jQuery.removeAttr(elem, name);
}else{
elem.setAttribute(name, name);
}
return name;
}};
jQuery.each(jQuery.expr.match.bool.source.match(/\w+/g), function(i, name){
var getter=attrHandle[ name ]||jQuery.find.attr;
attrHandle[ name ]=function(elem, name, isXML){
var ret, handle,
lowercaseName=name.toLowerCase();
if(!isXML){
handle=attrHandle[ lowercaseName ];
attrHandle[ lowercaseName ]=ret;
ret=getter(elem, name, isXML)!=null ?
lowercaseName :
null;
attrHandle[ lowercaseName ]=handle;
}
return ret;
};});
var rfocusable=/^(?:input|select|textarea|button)$/i,
rclickable=/^(?:a|area)$/i;
jQuery.fn.extend({
prop: function(name, value){
return access(this, jQuery.prop, name, value, arguments.length > 1);
},
removeProp: function(name){
return this.each(function(){
delete this[ jQuery.propFix[ name ]||name ];
});
}});
jQuery.extend({
prop: function(elem, name, value){
var ret, hooks,
nType=elem.nodeType;
if(nType===3||nType===8||nType===2){
return;
}
if(nType!==1||!jQuery.isXMLDoc(elem)){
name=jQuery.propFix[ name ]||name;
hooks=jQuery.propHooks[ name ];
}
if(value!==undefined){
if(hooks&&"set" in hooks &&
(ret=hooks.set(elem, value, name))!==undefined){
return ret;
}
return(elem[ name ]=value);
}
if(hooks&&"get" in hooks&&(ret=hooks.get(elem, name))!==null){
return ret;
}
return elem[ name ];
},
propHooks: {
tabIndex: {
get: function(elem){
var tabindex=jQuery.find.attr(elem, "tabindex");
if(tabindex){
return parseInt(tabindex, 10);
}
if(rfocusable.test(elem.nodeName) ||
rclickable.test(elem.nodeName) &&
elem.href
){
return 0;
}
return -1;
}}
},
propFix: {
"for": "htmlFor",
"class": "className"
}});
if(!support.optSelected){
jQuery.propHooks.selected={
get: function(elem){
var parent=elem.parentNode;
if(parent&&parent.parentNode){
parent.parentNode.selectedIndex;
}
return null;
},
set: function(elem){
var parent=elem.parentNode;
if(parent){
parent.selectedIndex;
if(parent.parentNode){
parent.parentNode.selectedIndex;
}}
}};}
jQuery.each([
"tabIndex",
"readOnly",
"maxLength",
"cellSpacing",
"cellPadding",
"rowSpan",
"colSpan",
"useMap",
"frameBorder",
"contentEditable"
], function(){
jQuery.propFix[ this.toLowerCase() ]=this;
});
function stripAndCollapse(value){
var tokens=value.match(rnothtmlwhite)||[];
return tokens.join(" ");
}
function getClass(elem){
return elem.getAttribute&&elem.getAttribute("class")||"";
}
function classesToArray(value){
if(Array.isArray(value)){
return value;
}
if(typeof value==="string"){
return value.match(rnothtmlwhite)||[];
}
return [];
}
jQuery.fn.extend({
addClass: function(value){
var classes, elem, cur, curValue, clazz, j, finalValue,
i=0;
if(isFunction(value)){
return this.each(function(j){
jQuery(this).addClass(value.call(this, j, getClass(this)));
});
}
classes=classesToArray(value);
if(classes.length){
while(( elem=this[ i++ ])){
curValue=getClass(elem);
cur=elem.nodeType===1&&(" " + stripAndCollapse(curValue) + " ");
if(cur){
j=0;
while(( clazz=classes[ j++ ])){
if(cur.indexOf(" " + clazz + " ") < 0){
cur +=clazz + " ";
}}
finalValue=stripAndCollapse(cur);
if(curValue!==finalValue){
elem.setAttribute("class", finalValue);
}}
}}
return this;
},
removeClass: function(value){
var classes, elem, cur, curValue, clazz, j, finalValue,
i=0;
if(isFunction(value)){
return this.each(function(j){
jQuery(this).removeClass(value.call(this, j, getClass(this)));
});
}
if(!arguments.length){
return this.attr("class", "");
}
classes=classesToArray(value);
if(classes.length){
while(( elem=this[ i++ ])){
curValue=getClass(elem);
cur=elem.nodeType===1&&(" " + stripAndCollapse(curValue) + " ");
if(cur){
j=0;
while(( clazz=classes[ j++ ])){
while(cur.indexOf(" " + clazz + " ") > -1){
cur=cur.replace(" " + clazz + " ", " ");
}}
finalValue=stripAndCollapse(cur);
if(curValue!==finalValue){
elem.setAttribute("class", finalValue);
}}
}}
return this;
},
toggleClass: function(value, stateVal){
var type=typeof value,
isValidValue=type==="string"||Array.isArray(value);
if(typeof stateVal==="boolean"&&isValidValue){
return stateVal ? this.addClass(value):this.removeClass(value);
}
if(isFunction(value)){
return this.each(function(i){
jQuery(this).toggleClass(value.call(this, i, getClass(this), stateVal),
stateVal
);
});
}
return this.each(function(){
var className, i, self, classNames;
if(isValidValue){
i=0;
self=jQuery(this);
classNames=classesToArray(value);
while(( className=classNames[ i++ ])){
if(self.hasClass(className)){
self.removeClass(className);
}else{
self.addClass(className);
}}
}else if(value===undefined||type==="boolean"){
className=getClass(this);
if(className){
dataPriv.set(this, "__className__", className);
}
if(this.setAttribute){
this.setAttribute("class",
className||value===false ?
"" :
dataPriv.get(this, "__className__")||""
);
}}
});
},
hasClass: function(selector){
var className, elem,
i=0;
className=" " + selector + " ";
while(( elem=this[ i++ ])){
if(elem.nodeType===1 &&
(" " + stripAndCollapse(getClass(elem)) + " ").indexOf(className) > -1){
return true;
}}
return false;
}});
var rreturn=/\r/g;
jQuery.fn.extend({
val: function(value){
var hooks, ret, valueIsFunction,
elem=this[ 0 ];
if(!arguments.length){
if(elem){
hooks=jQuery.valHooks[ elem.type ] ||
jQuery.valHooks[ elem.nodeName.toLowerCase() ];
if(hooks &&
"get" in hooks &&
(ret=hooks.get(elem, "value"))!==undefined
){
return ret;
}
ret=elem.value;
if(typeof ret==="string"){
return ret.replace(rreturn, "");
}
return ret==null ? "":ret;
}
return;
}
valueIsFunction=isFunction(value);
return this.each(function(i){
var val;
if(this.nodeType!==1){
return;
}
if(valueIsFunction){
val=value.call(this, i, jQuery(this).val());
}else{
val=value;
}
if(val==null){
val="";
}else if(typeof val==="number"){
val +="";
}else if(Array.isArray(val)){
val=jQuery.map(val, function(value){
return value==null ? "":value + "";
});
}
hooks=jQuery.valHooks[ this.type ]||jQuery.valHooks[ this.nodeName.toLowerCase() ];
if(!hooks||!("set" in hooks)||hooks.set(this, val, "value")===undefined){
this.value=val;
}});
}});
jQuery.extend({
valHooks: {
option: {
get: function(elem){
var val=jQuery.find.attr(elem, "value");
return val!=null ?
val :
stripAndCollapse(jQuery.text(elem));
}},
select: {
get: function(elem){
var value, option, i,
options=elem.options,
index=elem.selectedIndex,
one=elem.type==="select-one",
values=one ? null:[],
max=one ? index + 1:options.length;
if(index < 0){
i=max;
}else{
i=one ? index:0;
}
for(; i < max; i++){
option=options[ i ];
if(( option.selected||i===index) &&
!option.disabled &&
(!option.parentNode.disabled ||
!nodeName(option.parentNode, "optgroup"))){
value=jQuery(option).val();
if(one){
return value;
}
values.push(value);
}}
return values;
},
set: function(elem, value){
var optionSet, option,
options=elem.options,
values=jQuery.makeArray(value),
i=options.length;
while(i--){
option=options[ i ];
if(option.selected =
jQuery.inArray(jQuery.valHooks.option.get(option), values) > -1
){
optionSet=true;
}
}
if(!optionSet){
elem.selectedIndex=-1;
}
return values;
}}
}});
jQuery.each([ "radio", "checkbox" ], function(){
jQuery.valHooks[ this ]={
set: function(elem, value){
if(Array.isArray(value)){
return(elem.checked=jQuery.inArray(jQuery(elem).val(), value) > -1);
}}
};
if(!support.checkOn){
jQuery.valHooks[ this ].get=function(elem){
return elem.getAttribute("value")===null ? "on":elem.value;
};}});
support.focusin="onfocusin" in window;
var rfocusMorph=/^(?:focusinfocus|focusoutblur)$/,
stopPropagationCallback=function(e){
e.stopPropagation();
};
jQuery.extend(jQuery.event, {
trigger: function(event, data, elem, onlyHandlers){
var i, cur, tmp, bubbleType, ontype, handle, special, lastElement,
eventPath=[ elem||document ],
type=hasOwn.call(event, "type") ? event.type:event,
namespaces=hasOwn.call(event, "namespace") ? event.namespace.split("."):[];
cur=lastElement=tmp=elem=elem||document;
if(elem.nodeType===3||elem.nodeType===8){
return;
}
if(rfocusMorph.test(type + jQuery.event.triggered)){
return;
}
if(type.indexOf(".") > -1){
namespaces=type.split(".");
type=namespaces.shift();
namespaces.sort();
}
ontype=type.indexOf(":") < 0&&"on" + type;
event=event[ jQuery.expando ] ?
event :
new jQuery.Event(type, typeof event==="object"&&event);
event.isTrigger=onlyHandlers ? 2:3;
event.namespace=namespaces.join(".");
event.rnamespace=event.namespace ?
new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)") :
null;
event.result=undefined;
if(!event.target){
event.target=elem;
}
data=data==null ?
[ event ] :
jQuery.makeArray(data, [ event ]);
special=jQuery.event.special[ type ]||{};
if(!onlyHandlers&&special.trigger&&special.trigger.apply(elem, data)===false){
return;
}
if(!onlyHandlers&&!special.noBubble&&!isWindow(elem)){
bubbleType=special.delegateType||type;
if(!rfocusMorph.test(bubbleType + type)){
cur=cur.parentNode;
}
for(; cur; cur=cur.parentNode){
eventPath.push(cur);
tmp=cur;
}
if(tmp===(elem.ownerDocument||document)){
eventPath.push(tmp.defaultView||tmp.parentWindow||window);
}}
i=0;
while(( cur=eventPath[ i++ ])&&!event.isPropagationStopped()){
lastElement=cur;
event.type=i > 1 ?
bubbleType :
special.bindType||type;
handle=(dataPriv.get(cur, "events")||{})[ event.type ] &&
dataPriv.get(cur, "handle");
if(handle){
handle.apply(cur, data);
}
handle=ontype&&cur[ ontype ];
if(handle&&handle.apply&&acceptData(cur)){
event.result=handle.apply(cur, data);
if(event.result===false){
event.preventDefault();
}}
}
event.type=type;
if(!onlyHandlers&&!event.isDefaultPrevented()){
if(( !special._default ||
special._default.apply(eventPath.pop(), data)===false) &&
acceptData(elem)){
if(ontype&&isFunction(elem[ type ])&&!isWindow(elem)){
tmp=elem[ ontype ];
if(tmp){
elem[ ontype ]=null;
}
jQuery.event.triggered=type;
if(event.isPropagationStopped()){
lastElement.addEventListener(type, stopPropagationCallback);
}
elem[ type ]();
if(event.isPropagationStopped()){
lastElement.removeEventListener(type, stopPropagationCallback);
}
jQuery.event.triggered=undefined;
if(tmp){
elem[ ontype ]=tmp;
}}
}}
return event.result;
},
simulate: function(type, elem, event){
var e=jQuery.extend(new jQuery.Event(),
event,
{
type: type,
isSimulated: true
}
);
jQuery.event.trigger(e, null, elem);
}});
jQuery.fn.extend({
trigger: function(type, data){
return this.each(function(){
jQuery.event.trigger(type, data, this);
});
},
triggerHandler: function(type, data){
var elem=this[ 0 ];
if(elem){
return jQuery.event.trigger(type, data, elem, true);
}}
});
if(!support.focusin){
jQuery.each({ focus: "focusin", blur: "focusout" }, function(orig, fix){
var handler=function(event){
jQuery.event.simulate(fix, event.target, jQuery.event.fix(event));
};
jQuery.event.special[ fix ]={
setup: function(){
var doc=this.ownerDocument||this,
attaches=dataPriv.access(doc, fix);
if(!attaches){
doc.addEventListener(orig, handler, true);
}
dataPriv.access(doc, fix,(attaches||0) + 1);
},
teardown: function(){
var doc=this.ownerDocument||this,
attaches=dataPriv.access(doc, fix) - 1;
if(!attaches){
doc.removeEventListener(orig, handler, true);
dataPriv.remove(doc, fix);
}else{
dataPriv.access(doc, fix, attaches);
}}
};});
}
var location=window.location;
var nonce=Date.now();
var rquery=(/\?/);
jQuery.parseXML=function(data){
var xml;
if(!data||typeof data!=="string"){
return null;
}
try {
xml=(new window.DOMParser()).parseFromString(data, "text/xml");
} catch(e){
xml=undefined;
}
if(!xml||xml.getElementsByTagName("parsererror").length){
jQuery.error("Invalid XML: " + data);
}
return xml;
};
var
rbracket=/\[\]$/,
rCRLF=/\r?\n/g,
rsubmitterTypes=/^(?:submit|button|image|reset|file)$/i,
rsubmittable=/^(?:input|select|textarea|keygen)/i;
function buildParams(prefix, obj, traditional, add){
var name;
if(Array.isArray(obj)){
jQuery.each(obj, function(i, v){
if(traditional||rbracket.test(prefix)){
add(prefix, v);
}else{
buildParams(
prefix + "[" +(typeof v==="object"&&v!=null ? i:"") + "]",
v,
traditional,
add
);
}});
}else if(!traditional&&toType(obj)==="object"){
for(name in obj){
buildParams(prefix + "[" + name + "]", obj[ name ], traditional, add);
}}else{
add(prefix, obj);
}}
jQuery.param=function(a, traditional){
var prefix,
s=[],
add=function(key, valueOrFunction){
var value=isFunction(valueOrFunction) ?
valueOrFunction() :
valueOrFunction;
s[ s.length ]=encodeURIComponent(key) + "=" +
encodeURIComponent(value==null ? "":value);
};
if(Array.isArray(a)||(a.jquery&&!jQuery.isPlainObject(a))){
jQuery.each(a, function(){
add(this.name, this.value);
});
}else{
for(prefix in a){
buildParams(prefix, a[ prefix ], traditional, add);
}}
return s.join("&");
};
jQuery.fn.extend({
serialize: function(){
return jQuery.param(this.serializeArray());
},
serializeArray: function(){
return this.map(function(){
var elements=jQuery.prop(this, "elements");
return elements ? jQuery.makeArray(elements):this;
})
.filter(function(){
var type=this.type;
return this.name&&!jQuery(this).is(":disabled") &&
rsubmittable.test(this.nodeName)&&!rsubmitterTypes.test(type) &&
(this.checked||!rcheckableType.test(type));
})
.map(function(i, elem){
var val=jQuery(this).val();
if(val==null){
return null;
}
if(Array.isArray(val)){
return jQuery.map(val, function(val){
return { name: elem.name, value: val.replace(rCRLF, "\r\n") };});
}
return { name: elem.name, value: val.replace(rCRLF, "\r\n") };}).get();
}});
var
r20=/%20/g,
rhash=/#.*$/,
rantiCache=/([?&])_=[^&]*/,
rheaders=/^(.*?):[ \t]*([^\r\n]*)$/mg,
rlocalProtocol=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
rnoContent=/^(?:GET|HEAD)$/,
rprotocol=/^\/\//,
prefilters={},
transports={},
allTypes="*/".concat("*"),
originAnchor=document.createElement("a");
originAnchor.href=location.href;
function addToPrefiltersOrTransports(structure){
return function(dataTypeExpression, func){
if(typeof dataTypeExpression!=="string"){
func=dataTypeExpression;
dataTypeExpression="*";
}
var dataType,
i=0,
dataTypes=dataTypeExpression.toLowerCase().match(rnothtmlwhite)||[];
if(isFunction(func)){
while(( dataType=dataTypes[ i++ ])){
if(dataType[ 0 ]==="+"){
dataType=dataType.slice(1)||"*";
(structure[ dataType ]=structure[ dataType ]||[]).unshift(func);
}else{
(structure[ dataType ]=structure[ dataType ]||[]).push(func);
}}
}};}
function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR){
var inspected={},
seekingTransport=(structure===transports);
function inspect(dataType){
var selected;
inspected[ dataType ]=true;
jQuery.each(structure[ dataType ]||[], function(_, prefilterOrFactory){
var dataTypeOrTransport=prefilterOrFactory(options, originalOptions, jqXHR);
if(typeof dataTypeOrTransport==="string" &&
!seekingTransport&&!inspected[ dataTypeOrTransport ]){
options.dataTypes.unshift(dataTypeOrTransport);
inspect(dataTypeOrTransport);
return false;
}else if(seekingTransport){
return !(selected=dataTypeOrTransport);
}});
return selected;
}
return inspect(options.dataTypes[ 0 ])||!inspected[ "*" ]&&inspect("*");
}
function ajaxExtend(target, src){
var key, deep,
flatOptions=jQuery.ajaxSettings.flatOptions||{};
for(key in src){
if(src[ key ]!==undefined){
(flatOptions[ key ] ? target:(deep||(deep={})))[ key ]=src[ key ];
}}
if(deep){
jQuery.extend(true, target, deep);
}
return target;
}
function ajaxHandleResponses(s, jqXHR, responses){
var ct, type, finalDataType, firstDataType,
contents=s.contents,
dataTypes=s.dataTypes;
while(dataTypes[ 0 ]==="*"){
dataTypes.shift();
if(ct===undefined){
ct=s.mimeType||jqXHR.getResponseHeader("Content-Type");
}}
if(ct){
for(type in contents){
if(contents[ type ]&&contents[ type ].test(ct)){
dataTypes.unshift(type);
break;
}}
}
if(dataTypes[ 0 ] in responses){
finalDataType=dataTypes[ 0 ];
}else{
for(type in responses){
if(!dataTypes[ 0 ]||s.converters[ type + " " + dataTypes[ 0 ] ]){
finalDataType=type;
break;
}
if(!firstDataType){
firstDataType=type;
}}
finalDataType=finalDataType||firstDataType;
}
if(finalDataType){
if(finalDataType!==dataTypes[ 0 ]){
dataTypes.unshift(finalDataType);
}
return responses[ finalDataType ];
}}
function ajaxConvert(s, response, jqXHR, isSuccess){
var conv2, current, conv, tmp, prev,
converters={},
dataTypes=s.dataTypes.slice();
if(dataTypes[ 1 ]){
for(conv in s.converters){
converters[ conv.toLowerCase() ]=s.converters[ conv ];
}}
current=dataTypes.shift();
while(current){
if(s.responseFields[ current ]){
jqXHR[ s.responseFields[ current ] ]=response;
}
if(!prev&&isSuccess&&s.dataFilter){
response=s.dataFilter(response, s.dataType);
}
prev=current;
current=dataTypes.shift();
if(current){
if(current==="*"){
current=prev;
}else if(prev!=="*"&&prev!==current){
conv=converters[ prev + " " + current ]||converters[ "* " + current ];
if(!conv){
for(conv2 in converters){
tmp=conv2.split(" ");
if(tmp[ 1 ]===current){
conv=converters[ prev + " " + tmp[ 0 ] ] ||
converters[ "* " + tmp[ 0 ] ];
if(conv){
if(conv===true){
conv=converters[ conv2 ];
}else if(converters[ conv2 ]!==true){
current=tmp[ 0 ];
dataTypes.unshift(tmp[ 1 ]);
}
break;
}}
}}
if(conv!==true){
if(conv&&s.throws){
response=conv(response);
}else{
try {
response=conv(response);
} catch(e){
return {
state: "parsererror",
error: conv ? e:"No conversion from " + prev + " to " + current
};}}
}}
}}
return { state: "success", data: response };}
jQuery.extend({
active: 0,
lastModified: {},
etag: {},
ajaxSettings: {
url: location.href,
type: "GET",
isLocal: rlocalProtocol.test(location.protocol),
global: true,
processData: true,
async: true,
contentType: "application/x-www-form-urlencoded; charset=UTF-8",
accepts: {
"*": allTypes,
text: "text/plain",
html: "text/html",
xml: "application/xml, text/xml",
json: "application/json, text/javascript"
},
contents: {
xml: /\bxml\b/,
html: /\bhtml/,
json: /\bjson\b/
},
responseFields: {
xml: "responseXML",
text: "responseText",
json: "responseJSON"
},
converters: {
"* text": String,
"text html": true,
"text json": JSON.parse,
"text xml": jQuery.parseXML
},
flatOptions: {
url: true,
context: true
}},
ajaxSetup: function(target, settings){
return settings ?
ajaxExtend(ajaxExtend(target, jQuery.ajaxSettings), settings) :
ajaxExtend(jQuery.ajaxSettings, target);
},
ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
ajaxTransport: addToPrefiltersOrTransports(transports),
ajax: function(url, options){
if(typeof url==="object"){
options=url;
url=undefined;
}
options=options||{};
var transport,
cacheURL,
responseHeadersString,
responseHeaders,
timeoutTimer,
urlAnchor,
completed,
fireGlobals,
i,
uncached,
s=jQuery.ajaxSetup({}, options),
callbackContext=s.context||s,
globalEventContext=s.context &&
(callbackContext.nodeType||callbackContext.jquery) ?
jQuery(callbackContext) :
jQuery.event,
deferred=jQuery.Deferred(),
completeDeferred=jQuery.Callbacks("once memory"),
statusCode=s.statusCode||{},
requestHeaders={},
requestHeadersNames={},
strAbort="canceled",
jqXHR={
readyState: 0,
getResponseHeader: function(key){
var match;
if(completed){
if(!responseHeaders){
responseHeaders={};
while(( match=rheaders.exec(responseHeadersString))){
responseHeaders[ match[ 1 ].toLowerCase() ]=match[ 2 ];
}}
match=responseHeaders[ key.toLowerCase() ];
}
return match==null ? null:match;
},
getAllResponseHeaders: function(){
return completed ? responseHeadersString:null;
},
setRequestHeader: function(name, value){
if(completed==null){
name=requestHeadersNames[ name.toLowerCase() ] =
requestHeadersNames[ name.toLowerCase() ]||name;
requestHeaders[ name ]=value;
}
return this;
},
overrideMimeType: function(type){
if(completed==null){
s.mimeType=type;
}
return this;
},
statusCode: function(map){
var code;
if(map){
if(completed){
jqXHR.always(map[ jqXHR.status ]);
}else{
for(code in map){
statusCode[ code ]=[ statusCode[ code ], map[ code ] ];
}}
}
return this;
},
abort: function(statusText){
var finalText=statusText||strAbort;
if(transport){
transport.abort(finalText);
}
done(0, finalText);
return this;
}};
deferred.promise(jqXHR);
s.url=(( url||s.url||location.href) + "")
.replace(rprotocol, location.protocol + "//");
s.type=options.method||options.type||s.method||s.type;
s.dataTypes=(s.dataType||"*").toLowerCase().match(rnothtmlwhite)||[ "" ];
if(s.crossDomain==null){
urlAnchor=document.createElement("a");
try {
urlAnchor.href=s.url;
urlAnchor.href=urlAnchor.href;
s.crossDomain=originAnchor.protocol + "//" + originAnchor.host!==urlAnchor.protocol + "//" + urlAnchor.host;
} catch(e){
s.crossDomain=true;
}}
if(s.data&&s.processData&&typeof s.data!=="string"){
s.data=jQuery.param(s.data, s.traditional);
}
inspectPrefiltersOrTransports(prefilters, s, options, jqXHR);
if(completed){
return jqXHR;
}
fireGlobals=jQuery.event&&s.global;
if(fireGlobals&&jQuery.active++===0){
jQuery.event.trigger("ajaxStart");
}
s.type=s.type.toUpperCase();
s.hasContent = !rnoContent.test(s.type);
cacheURL=s.url.replace(rhash, "");
if(!s.hasContent){
uncached=s.url.slice(cacheURL.length);
if(s.data&&(s.processData||typeof s.data==="string")){
cacheURL +=(rquery.test(cacheURL) ? "&":"?") + s.data;
delete s.data;
}
if(s.cache===false){
cacheURL=cacheURL.replace(rantiCache, "$1");
uncached=(rquery.test(cacheURL) ? "&":"?") + "_=" +(nonce++) + uncached;
}
s.url=cacheURL + uncached;
}else if(s.data&&s.processData &&
(s.contentType||"").indexOf("application/x-www-form-urlencoded")===0){
s.data=s.data.replace(r20, "+");
}
if(s.ifModified){
if(jQuery.lastModified[ cacheURL ]){
jqXHR.setRequestHeader("If-Modified-Since", jQuery.lastModified[ cacheURL ]);
}
if(jQuery.etag[ cacheURL ]){
jqXHR.setRequestHeader("If-None-Match", jQuery.etag[ cacheURL ]);
}}
if(s.data&&s.hasContent&&s.contentType!==false||options.contentType){
jqXHR.setRequestHeader("Content-Type", s.contentType);
}
jqXHR.setRequestHeader("Accept",
s.dataTypes[ 0 ]&&s.accepts[ s.dataTypes[ 0 ] ] ?
s.accepts[ s.dataTypes[ 0 ] ] +
(s.dataTypes[ 0 ]!=="*" ? ", " + allTypes + "; q=0.01":"") :
s.accepts[ "*" ]
);
for(i in s.headers){
jqXHR.setRequestHeader(i, s.headers[ i ]);
}
if(s.beforeSend &&
(s.beforeSend.call(callbackContext, jqXHR, s)===false||completed)){
return jqXHR.abort();
}
strAbort="abort";
completeDeferred.add(s.complete);
jqXHR.done(s.success);
jqXHR.fail(s.error);
transport=inspectPrefiltersOrTransports(transports, s, options, jqXHR);
if(!transport){
done(-1, "No Transport");
}else{
jqXHR.readyState=1;
if(fireGlobals){
globalEventContext.trigger("ajaxSend", [ jqXHR, s ]);
}
if(completed){
return jqXHR;
}
if(s.async&&s.timeout > 0){
timeoutTimer=window.setTimeout(function(){
jqXHR.abort("timeout");
}, s.timeout);
}
try {
completed=false;
transport.send(requestHeaders, done);
} catch(e){
if(completed){
throw e;
}
done(-1, e);
}}
function done(status, nativeStatusText, responses, headers){
var isSuccess, success, error, response, modified,
statusText=nativeStatusText;
if(completed){
return;
}
completed=true;
if(timeoutTimer){
window.clearTimeout(timeoutTimer);
}
transport=undefined;
responseHeadersString=headers||"";
jqXHR.readyState=status > 0 ? 4:0;
isSuccess=status >=200&&status < 300||status===304;
if(responses){
response=ajaxHandleResponses(s, jqXHR, responses);
}
response=ajaxConvert(s, response, jqXHR, isSuccess);
if(isSuccess){
if(s.ifModified){
modified=jqXHR.getResponseHeader("Last-Modified");
if(modified){
jQuery.lastModified[ cacheURL ]=modified;
}
modified=jqXHR.getResponseHeader("etag");
if(modified){
jQuery.etag[ cacheURL ]=modified;
}}
if(status===204||s.type==="HEAD"){
statusText="nocontent";
}else if(status===304){
statusText="notmodified";
}else{
statusText=response.state;
success=response.data;
error=response.error;
isSuccess = !error;
}}else{
error=statusText;
if(status||!statusText){
statusText="error";
if(status < 0){
status=0;
}}
}
jqXHR.status=status;
jqXHR.statusText=(nativeStatusText||statusText) + "";
if(isSuccess){
deferred.resolveWith(callbackContext, [ success, statusText, jqXHR ]);
}else{
deferred.rejectWith(callbackContext, [ jqXHR, statusText, error ]);
}
jqXHR.statusCode(statusCode);
statusCode=undefined;
if(fireGlobals){
globalEventContext.trigger(isSuccess ? "ajaxSuccess":"ajaxError",
[ jqXHR, s, isSuccess ? success:error ]);
}
completeDeferred.fireWith(callbackContext, [ jqXHR, statusText ]);
if(fireGlobals){
globalEventContext.trigger("ajaxComplete", [ jqXHR, s ]);
if(!(--jQuery.active)){
jQuery.event.trigger("ajaxStop");
}}
}
return jqXHR;
},
getJSON: function(url, data, callback){
return jQuery.get(url, data, callback, "json");
},
getScript: function(url, callback){
return jQuery.get(url, undefined, callback, "script");
}});
jQuery.each([ "get", "post" ], function(i, method){
jQuery[ method ]=function(url, data, callback, type){
if(isFunction(data)){
type=type||callback;
callback=data;
data=undefined;
}
return jQuery.ajax(jQuery.extend({
url: url,
type: method,
dataType: type,
data: data,
success: callback
}, jQuery.isPlainObject(url)&&url));
};});
jQuery._evalUrl=function(url){
return jQuery.ajax({
url: url,
type: "GET",
dataType: "script",
cache: true,
async: false,
global: false,
"throws": true
});
};
jQuery.fn.extend({
wrapAll: function(html){
var wrap;
if(this[ 0 ]){
if(isFunction(html)){
html=html.call(this[ 0 ]);
}
wrap=jQuery(html, this[ 0 ].ownerDocument).eq(0).clone(true);
if(this[ 0 ].parentNode){
wrap.insertBefore(this[ 0 ]);
}
wrap.map(function(){
var elem=this;
while(elem.firstElementChild){
elem=elem.firstElementChild;
}
return elem;
}).append(this);
}
return this;
},
wrapInner: function(html){
if(isFunction(html)){
return this.each(function(i){
jQuery(this).wrapInner(html.call(this, i));
});
}
return this.each(function(){
var self=jQuery(this),
contents=self.contents();
if(contents.length){
contents.wrapAll(html);
}else{
self.append(html);
}});
},
wrap: function(html){
var htmlIsFunction=isFunction(html);
return this.each(function(i){
jQuery(this).wrapAll(htmlIsFunction ? html.call(this, i):html);
});
},
unwrap: function(selector){
this.parent(selector).not("body").each(function(){
jQuery(this).replaceWith(this.childNodes);
});
return this;
}});
jQuery.expr.pseudos.hidden=function(elem){
return !jQuery.expr.pseudos.visible(elem);
};
jQuery.expr.pseudos.visible=function(elem){
return !!(elem.offsetWidth||elem.offsetHeight||elem.getClientRects().length);
};
jQuery.ajaxSettings.xhr=function(){
try {
return new window.XMLHttpRequest();
} catch(e){}};
var xhrSuccessStatus={
0: 200,
1223: 204
},
xhrSupported=jQuery.ajaxSettings.xhr();
support.cors = !!xhrSupported&&("withCredentials" in xhrSupported);
support.ajax=xhrSupported = !!xhrSupported;
jQuery.ajaxTransport(function(options){
var callback, errorCallback;
if(support.cors||xhrSupported&&!options.crossDomain){
return {
send: function(headers, complete){
var i,
xhr=options.xhr();
xhr.open(options.type,
options.url,
options.async,
options.username,
options.password
);
if(options.xhrFields){
for(i in options.xhrFields){
xhr[ i ]=options.xhrFields[ i ];
}}
if(options.mimeType&&xhr.overrideMimeType){
xhr.overrideMimeType(options.mimeType);
}
if(!options.crossDomain&&!headers[ "X-Requested-With" ]){
headers[ "X-Requested-With" ]="XMLHttpRequest";
}
for(i in headers){
xhr.setRequestHeader(i, headers[ i ]);
}
callback=function(type){
return function(){
if(callback){
callback=errorCallback=xhr.onload =
xhr.onerror=xhr.onabort=xhr.ontimeout =
xhr.onreadystatechange=null;
if(type==="abort"){
xhr.abort();
}else if(type==="error"){
if(typeof xhr.status!=="number"){
complete(0, "error");
}else{
complete(
xhr.status,
xhr.statusText
);
}}else{
complete(
xhrSuccessStatus[ xhr.status ]||xhr.status,
xhr.statusText,
(xhr.responseType||"text")!=="text"  ||
typeof xhr.responseText!=="string" ?
{ binary: xhr.response } :
{ text: xhr.responseText },
xhr.getAllResponseHeaders()
);
}}
};};
xhr.onload=callback();
errorCallback=xhr.onerror=xhr.ontimeout=callback("error");
if(xhr.onabort!==undefined){
xhr.onabort=errorCallback;
}else{
xhr.onreadystatechange=function(){
if(xhr.readyState===4){
window.setTimeout(function(){
if(callback){
errorCallback();
}});
}};}
callback=callback("abort");
try {
xhr.send(options.hasContent&&options.data||null);
} catch(e){
if(callback){
throw e;
}}
},
abort: function(){
if(callback){
callback();
}}
};}});
jQuery.ajaxPrefilter(function(s){
if(s.crossDomain){
s.contents.script=false;
}});
jQuery.ajaxSetup({
accepts: {
script: "text/javascript, application/javascript, " +
"application/ecmascript, application/x-ecmascript"
},
contents: {
script: /\b(?:java|ecma)script\b/
},
converters: {
"text script": function(text){
jQuery.globalEval(text);
return text;
}}
});
jQuery.ajaxPrefilter("script", function(s){
if(s.cache===undefined){
s.cache=false;
}
if(s.crossDomain){
s.type="GET";
}});
jQuery.ajaxTransport("script", function(s){
if(s.crossDomain){
var script, callback;
return {
send: function(_, complete){
script=jQuery("<script>").prop({
charset: s.scriptCharset,
src: s.url
}).on("load error",
callback=function(evt){
script.remove();
callback=null;
if(evt){
complete(evt.type==="error" ? 404:200, evt.type);
}}
);
document.head.appendChild(script[ 0 ]);
},
abort: function(){
if(callback){
callback();
}}
};}});
var oldCallbacks=[],
rjsonp=/(=)\?(?=&|$)|\?\?/;
jQuery.ajaxSetup({
jsonp: "callback",
jsonpCallback: function(){
var callback=oldCallbacks.pop()||(jQuery.expando + "_" +(nonce++));
this[ callback ]=true;
return callback;
}});
jQuery.ajaxPrefilter("json jsonp", function(s, originalSettings, jqXHR){
var callbackName, overwritten, responseContainer,
jsonProp=s.jsonp!==false&&(rjsonp.test(s.url) ?
"url" :
typeof s.data==="string" &&
(s.contentType||"")
.indexOf("application/x-www-form-urlencoded")===0 &&
rjsonp.test(s.data)&&"data"
);
if(jsonProp||s.dataTypes[ 0 ]==="jsonp"){
callbackName=s.jsonpCallback=isFunction(s.jsonpCallback) ?
s.jsonpCallback() :
s.jsonpCallback;
if(jsonProp){
s[ jsonProp ]=s[ jsonProp ].replace(rjsonp, "$1" + callbackName);
}else if(s.jsonp!==false){
s.url +=(rquery.test(s.url) ? "&":"?") + s.jsonp + "=" + callbackName;
}
s.converters[ "script json" ]=function(){
if(!responseContainer){
jQuery.error(callbackName + " was not called");
}
return responseContainer[ 0 ];
};
s.dataTypes[ 0 ]="json";
overwritten=window[ callbackName ];
window[ callbackName ]=function(){
responseContainer=arguments;
};
jqXHR.always(function(){
if(overwritten===undefined){
jQuery(window).removeProp(callbackName);
}else{
window[ callbackName ]=overwritten;
}
if(s[ callbackName ]){
s.jsonpCallback=originalSettings.jsonpCallback;
oldCallbacks.push(callbackName);
}
if(responseContainer&&isFunction(overwritten)){
overwritten(responseContainer[ 0 ]);
}
responseContainer=overwritten=undefined;
});
return "script";
}});
support.createHTMLDocument=(function(){
var body=document.implementation.createHTMLDocument("").body;
body.innerHTML="<form></form><form></form>";
return body.childNodes.length===2;
})();
jQuery.parseHTML=function(data, context, keepScripts){
if(typeof data!=="string"){
return [];
}
if(typeof context==="boolean"){
keepScripts=context;
context=false;
}
var base, parsed, scripts;
if(!context){
if(support.createHTMLDocument){
context=document.implementation.createHTMLDocument("");
base=context.createElement("base");
base.href=document.location.href;
context.head.appendChild(base);
}else{
context=document;
}}
parsed=rsingleTag.exec(data);
scripts = !keepScripts&&[];
if(parsed){
return [ context.createElement(parsed[ 1 ]) ];
}
parsed=buildFragment([ data ], context, scripts);
if(scripts&&scripts.length){
jQuery(scripts).remove();
}
return jQuery.merge([], parsed.childNodes);
};
jQuery.fn.load=function(url, params, callback){
var selector, type, response,
self=this,
off=url.indexOf(" ");
if(off > -1){
selector=stripAndCollapse(url.slice(off));
url=url.slice(0, off);
}
if(isFunction(params)){
callback=params;
params=undefined;
}else if(params&&typeof params==="object"){
type="POST";
}
if(self.length > 0){
jQuery.ajax({
url: url,
type: type||"GET",
dataType: "html",
data: params
}).done(function(responseText){
response=arguments;
self.html(selector ?
jQuery("<div>").append(jQuery.parseHTML(responseText)).find(selector) :
responseText);
}).always(callback&&function(jqXHR, status){
self.each(function(){
callback.apply(this, response||[ jqXHR.responseText, status, jqXHR ]);
});
});
}
return this;
};
jQuery.each([
"ajaxStart",
"ajaxStop",
"ajaxComplete",
"ajaxError",
"ajaxSuccess",
"ajaxSend"
], function(i, type){
jQuery.fn[ type ]=function(fn){
return this.on(type, fn);
};});
jQuery.expr.pseudos.animated=function(elem){
return jQuery.grep(jQuery.timers, function(fn){
return elem===fn.elem;
}).length;
};
jQuery.offset={
setOffset: function(elem, options, i){
var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
position=jQuery.css(elem, "position"),
curElem=jQuery(elem),
props={};
if(position==="static"){
elem.style.position="relative";
}
curOffset=curElem.offset();
curCSSTop=jQuery.css(elem, "top");
curCSSLeft=jQuery.css(elem, "left");
calculatePosition=(position==="absolute"||position==="fixed") &&
(curCSSTop + curCSSLeft).indexOf("auto") > -1;
if(calculatePosition){
curPosition=curElem.position();
curTop=curPosition.top;
curLeft=curPosition.left;
}else{
curTop=parseFloat(curCSSTop)||0;
curLeft=parseFloat(curCSSLeft)||0;
}
if(isFunction(options)){
options=options.call(elem, i, jQuery.extend({}, curOffset));
}
if(options.top!=null){
props.top=(options.top - curOffset.top) + curTop;
}
if(options.left!=null){
props.left=(options.left - curOffset.left) + curLeft;
}
if("using" in options){
options.using.call(elem, props);
}else{
curElem.css(props);
}}
};
jQuery.fn.extend({
offset: function(options){
if(arguments.length){
return options===undefined ?
this :
this.each(function(i){
jQuery.offset.setOffset(this, options, i);
});
}
var rect, win,
elem=this[ 0 ];
if(!elem){
return;
}
if(!elem.getClientRects().length){
return { top: 0, left: 0 };}
rect=elem.getBoundingClientRect();
win=elem.ownerDocument.defaultView;
return {
top: rect.top + win.pageYOffset,
left: rect.left + win.pageXOffset
};},
position: function(){
if(!this[ 0 ]){
return;
}
var offsetParent, offset, doc,
elem=this[ 0 ],
parentOffset={ top: 0, left: 0 };
if(jQuery.css(elem, "position")==="fixed"){
offset=elem.getBoundingClientRect();
}else{
offset=this.offset();
doc=elem.ownerDocument;
offsetParent=elem.offsetParent||doc.documentElement;
while(offsetParent &&
(offsetParent===doc.body||offsetParent===doc.documentElement) &&
jQuery.css(offsetParent, "position")==="static"){
offsetParent=offsetParent.parentNode;
}
if(offsetParent&&offsetParent!==elem&&offsetParent.nodeType===1){
parentOffset=jQuery(offsetParent).offset();
parentOffset.top +=jQuery.css(offsetParent, "borderTopWidth", true);
parentOffset.left +=jQuery.css(offsetParent, "borderLeftWidth", true);
}}
return {
top: offset.top - parentOffset.top - jQuery.css(elem, "marginTop", true),
left: offset.left - parentOffset.left - jQuery.css(elem, "marginLeft", true)
};},
offsetParent: function(){
return this.map(function(){
var offsetParent=this.offsetParent;
while(offsetParent&&jQuery.css(offsetParent, "position")==="static"){
offsetParent=offsetParent.offsetParent;
}
return offsetParent||documentElement;
});
}});
jQuery.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function(method, prop){
var top="pageYOffset"===prop;
jQuery.fn[ method ]=function(val){
return access(this, function(elem, method, val){
var win;
if(isWindow(elem)){
win=elem;
}else if(elem.nodeType===9){
win=elem.defaultView;
}
if(val===undefined){
return win ? win[ prop ]:elem[ method ];
}
if(win){
win.scrollTo(!top ? val:win.pageXOffset,
top ? val:win.pageYOffset
);
}else{
elem[ method ]=val;
}}, method, val, arguments.length);
};});
jQuery.each([ "top", "left" ], function(i, prop){
jQuery.cssHooks[ prop ]=addGetHookIf(support.pixelPosition,
function(elem, computed){
if(computed){
computed=curCSS(elem, prop);
return rnumnonpx.test(computed) ?
jQuery(elem).position()[ prop ] + "px" :
computed;
}}
);
});
jQuery.each({ Height: "height", Width: "width" }, function(name, type){
jQuery.each({ padding: "inner" + name, content: type, "": "outer" + name },
function(defaultExtra, funcName){
jQuery.fn[ funcName ]=function(margin, value){
var chainable=arguments.length&&(defaultExtra||typeof margin!=="boolean"),
extra=defaultExtra||(margin===true||value===true ? "margin":"border");
return access(this, function(elem, type, value){
var doc;
if(isWindow(elem)){
return funcName.indexOf("outer")===0 ?
elem[ "inner" + name ] :
elem.document.documentElement[ "client" + name ];
}
if(elem.nodeType===9){
doc=elem.documentElement;
return Math.max(elem.body[ "scroll" + name ], doc[ "scroll" + name ],
elem.body[ "offset" + name ], doc[ "offset" + name ],
doc[ "client" + name ]
);
}
return value===undefined ?
jQuery.css(elem, type, extra) :
jQuery.style(elem, type, value, extra);
}, type, chainable ? margin:undefined, chainable);
};});
});
jQuery.each(( "blur focus focusin focusout resize scroll click dblclick " +
"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
"change select submit keydown keypress keyup contextmenu").split(" "),
function(i, name){
jQuery.fn[ name ]=function(data, fn){
return arguments.length > 0 ?
this.on(name, null, data, fn) :
this.trigger(name);
};});
jQuery.fn.extend({
hover: function(fnOver, fnOut){
return this.mouseenter(fnOver).mouseleave(fnOut||fnOver);
}});
jQuery.fn.extend({
bind: function(types, data, fn){
return this.on(types, null, data, fn);
},
unbind: function(types, fn){
return this.off(types, null, fn);
},
delegate: function(selector, types, data, fn){
return this.on(types, selector, data, fn);
},
undelegate: function(selector, types, fn){
return arguments.length===1 ?
this.off(selector, "**") :
this.off(types, selector||"**", fn);
}});
jQuery.proxy=function(fn, context){
var tmp, args, proxy;
if(typeof context==="string"){
tmp=fn[ context ];
context=fn;
fn=tmp;
}
if(!isFunction(fn)){
return undefined;
}
args=slice.call(arguments, 2);
proxy=function(){
return fn.apply(context||this, args.concat(slice.call(arguments)));
};
proxy.guid=fn.guid=fn.guid||jQuery.guid++;
return proxy;
};
jQuery.holdReady=function(hold){
if(hold){
jQuery.readyWait++;
}else{
jQuery.ready(true);
}};
jQuery.isArray=Array.isArray;
jQuery.parseJSON=JSON.parse;
jQuery.nodeName=nodeName;
jQuery.isFunction=isFunction;
jQuery.isWindow=isWindow;
jQuery.camelCase=camelCase;
jQuery.type=toType;
jQuery.now=Date.now;
jQuery.isNumeric=function(obj){
var type=jQuery.type(obj);
return(type==="number"||type==="string") &&
!isNaN(obj - parseFloat(obj));
};
if(typeof define==="function"&&define.amd){
define("jquery", [], function(){
return jQuery;
});
}
var
_jQuery=window.jQuery,
_$=window.$;
jQuery.noConflict=function(deep){
if(window.$===jQuery){
window.$=_$;
}
if(deep&&window.jQuery===jQuery){
window.jQuery=_jQuery;
}
return jQuery;
};
if(!noGlobal){
window.jQuery=window.$=jQuery;
}
return jQuery;
});
},{}],2:[function(require,module,exports){
(function(){
var DEBUG=true;
(function(undefined){
var window=this||(0, eval)('this'),
document=window['document'],
navigator=window['navigator'],
jQueryInstance=window["jQuery"],
JSON=window["JSON"];
(function(factory){
if(typeof define==='function'&&define['amd']){
define(['exports', 'require'], factory);
}else if(typeof exports==='object'&&typeof module==='object'){
factory(module['exports']||exports);
}else{
factory(window['ko']={});
}}(function(koExports, amdRequire){
var ko=typeof koExports!=='undefined' ? koExports:{};
ko.exportSymbol=function(koPath, object){
var tokens=koPath.split(".");
var target=ko;
for (var i=0; i < tokens.length - 1; i++)
target=target[tokens[i]];
target[tokens[tokens.length - 1]]=object;
};
ko.exportProperty=function(owner, publicName, object){
owner[publicName]=object;
};
ko.version="3.4.2";
ko.exportSymbol('version', ko.version);
ko.options={
'deferUpdates': false,
'useOnlyNativeEvents': false
};
ko.utils=(function (){
function objectForEach(obj, action){
for (var prop in obj){
if(obj.hasOwnProperty(prop)){
action(prop, obj[prop]);
}}
}
function extend(target, source){
if(source){
for(var prop in source){
if(source.hasOwnProperty(prop)){
target[prop]=source[prop];
}}
}
return target;
}
function setPrototypeOf(obj, proto){
obj.__proto__=proto;
return obj;
}
var canSetPrototype=({ __proto__: [] } instanceof Array);
var canUseSymbols = !DEBUG&&typeof Symbol==='function';
var knownEvents={}, knownEventTypesByEventName={};
var keyEventTypeName=(navigator&&/Firefox\/2/i.test(navigator.userAgent)) ? 'KeyboardEvent':'UIEvents';
knownEvents[keyEventTypeName]=['keyup', 'keydown', 'keypress'];
knownEvents['MouseEvents']=['click', 'dblclick', 'mousedown', 'mouseup', 'mousemove', 'mouseover', 'mouseout', 'mouseenter', 'mouseleave'];
objectForEach(knownEvents, function(eventType, knownEventsForType){
if(knownEventsForType.length){
for (var i=0, j=knownEventsForType.length; i < j; i++)
knownEventTypesByEventName[knownEventsForType[i]]=eventType;
}});
var eventsThatMustBeRegisteredUsingAttachEvent={ 'propertychange': true };
var ieVersion=document&&(function(){
var version=3, div=document.createElement('div'), iElems=div.getElementsByTagName('i');
while (
div.innerHTML='<!--[if gt IE ' + (++version) + ']><i></i><![endif]-->',
iElems[0]
){}
return version > 4 ? version:undefined;
}());
var isIe6=ieVersion===6,
isIe7=ieVersion===7;
function isClickOnCheckableElement(element, eventType){
if((ko.utils.tagNameLower(element)!=="input")||!element.type) return false;
if(eventType.toLowerCase()!="click") return false;
var inputType=element.type;
return (inputType=="checkbox")||(inputType=="radio");
}
var cssClassNameRegex=/\S+/g;
function toggleDomNodeCssClass(node, classNames, shouldHaveClass){
var addOrRemoveFn;
if(classNames){
if(typeof node.classList==='object'){
addOrRemoveFn=node.classList[shouldHaveClass ? 'add':'remove'];
ko.utils.arrayForEach(classNames.match(cssClassNameRegex), function(className){
addOrRemoveFn.call(node.classList, className);
});
}else if(typeof node.className['baseVal']==='string'){
toggleObjectClassPropertyString(node.className, 'baseVal', classNames, shouldHaveClass);
}else{
toggleObjectClassPropertyString(node, 'className', classNames, shouldHaveClass);
}}
}
function toggleObjectClassPropertyString(obj, prop, classNames, shouldHaveClass){
var currentClassNames=obj[prop].match(cssClassNameRegex)||[];
ko.utils.arrayForEach(classNames.match(cssClassNameRegex), function(className){
ko.utils.addOrRemoveItem(currentClassNames, className, shouldHaveClass);
});
obj[prop]=currentClassNames.join(" ");
}
return {
fieldsIncludedWithJsonPost: ['authenticity_token', /^__RequestVerificationToken(_.*)?$/],
arrayForEach: function (array, action){
for (var i=0, j=array.length; i < j; i++)
action(array[i], i);
},
arrayIndexOf: function (array, item){
if(typeof Array.prototype.indexOf=="function")
return Array.prototype.indexOf.call(array, item);
for (var i=0, j=array.length; i < j; i++)
if(array[i]===item)
return i;
return -1;
},
arrayFirst: function (array, predicate, predicateOwner){
for (var i=0, j=array.length; i < j; i++)
if(predicate.call(predicateOwner, array[i], i))
return array[i];
return null;
},
arrayRemoveItem: function (array, itemToRemove){
var index=ko.utils.arrayIndexOf(array, itemToRemove);
if(index > 0){
array.splice(index, 1);
}
else if(index===0){
array.shift();
}},
arrayGetDistinctValues: function (array){
array=array||[];
var result=[];
for (var i=0, j=array.length; i < j; i++){
if(ko.utils.arrayIndexOf(result, array[i]) < 0)
result.push(array[i]);
}
return result;
},
arrayMap: function (array, mapping){
array=array||[];
var result=[];
for (var i=0, j=array.length; i < j; i++)
result.push(mapping(array[i], i));
return result;
},
arrayFilter: function (array, predicate){
array=array||[];
var result=[];
for (var i=0, j=array.length; i < j; i++)
if(predicate(array[i], i))
result.push(array[i]);
return result;
},
arrayPushAll: function (array, valuesToPush){
if(valuesToPush instanceof Array)
array.push.apply(array, valuesToPush);
else
for (var i=0, j=valuesToPush.length; i < j; i++)
array.push(valuesToPush[i]);
return array;
},
addOrRemoveItem: function(array, value, included){
var existingEntryIndex=ko.utils.arrayIndexOf(ko.utils.peekObservable(array), value);
if(existingEntryIndex < 0){
if(included)
array.push(value);
}else{
if(!included)
array.splice(existingEntryIndex, 1);
}},
canSetPrototype: canSetPrototype,
extend: extend,
setPrototypeOf: setPrototypeOf,
setPrototypeOfOrExtend: canSetPrototype ? setPrototypeOf:extend,
objectForEach: objectForEach,
objectMap: function(source, mapping){
if(!source)
return source;
var target={};
for (var prop in source){
if(source.hasOwnProperty(prop)){
target[prop]=mapping(source[prop], prop, source);
}}
return target;
},
emptyDomNode: function (domNode){
while (domNode.firstChild){
ko.removeNode(domNode.firstChild);
}},
moveCleanedNodesToContainerElement: function(nodes){
var nodesArray=ko.utils.makeArray(nodes);
var templateDocument=(nodesArray[0]&&nodesArray[0].ownerDocument)||document;
var container=templateDocument.createElement('div');
for (var i=0, j=nodesArray.length; i < j; i++){
container.appendChild(ko.cleanNode(nodesArray[i]));
}
return container;
},
cloneNodes: function (nodesArray, shouldCleanNodes){
for (var i=0, j=nodesArray.length, newNodesArray=[]; i < j; i++){
var clonedNode=nodesArray[i].cloneNode(true);
newNodesArray.push(shouldCleanNodes ? ko.cleanNode(clonedNode):clonedNode);
}
return newNodesArray;
},
setDomNodeChildren: function (domNode, childNodes){
ko.utils.emptyDomNode(domNode);
if(childNodes){
for (var i=0, j=childNodes.length; i < j; i++)
domNode.appendChild(childNodes[i]);
}},
replaceDomNodes: function (nodeToReplaceOrNodeArray, newNodesArray){
var nodesToReplaceArray=nodeToReplaceOrNodeArray.nodeType ? [nodeToReplaceOrNodeArray]:nodeToReplaceOrNodeArray;
if(nodesToReplaceArray.length > 0){
var insertionPoint=nodesToReplaceArray[0];
var parent=insertionPoint.parentNode;
for (var i=0, j=newNodesArray.length; i < j; i++)
parent.insertBefore(newNodesArray[i], insertionPoint);
for (var i=0, j=nodesToReplaceArray.length; i < j; i++){
ko.removeNode(nodesToReplaceArray[i]);
}}
},
fixUpContinuousNodeArray: function(continuousNodeArray, parentNode){
if(continuousNodeArray.length){
parentNode=(parentNode.nodeType===8&&parentNode.parentNode)||parentNode;
while (continuousNodeArray.length&&continuousNodeArray[0].parentNode!==parentNode)
continuousNodeArray.splice(0, 1);
while (continuousNodeArray.length > 1&&continuousNodeArray[continuousNodeArray.length - 1].parentNode!==parentNode)
continuousNodeArray.length--;
if(continuousNodeArray.length > 1){
var current=continuousNodeArray[0], last=continuousNodeArray[continuousNodeArray.length - 1];
continuousNodeArray.length=0;
while (current!==last){
continuousNodeArray.push(current);
current=current.nextSibling;
}
continuousNodeArray.push(last);
}}
return continuousNodeArray;
},
setOptionNodeSelectionState: function (optionNode, isSelected){
if(ieVersion < 7)
optionNode.setAttribute("selected", isSelected);
else
optionNode.selected=isSelected;
},
stringTrim: function (string){
return string===null||string===undefined ? '' :
string.trim ?
string.trim() :
string.toString().replace(/^[\s\xa0]+|[\s\xa0]+$/g, '');
},
stringStartsWith: function (string, startsWith){
string=string||"";
if(startsWith.length > string.length)
return false;
return string.substring(0, startsWith.length)===startsWith;
},
domNodeIsContainedBy: function (node, containedByNode){
if(node===containedByNode)
return true;
if(node.nodeType===11)
return false;
if(containedByNode.contains)
return containedByNode.contains(node.nodeType===3 ? node.parentNode:node);
if(containedByNode.compareDocumentPosition)
return (containedByNode.compareDocumentPosition(node) & 16)==16;
while (node&&node!=containedByNode){
node=node.parentNode;
}
return !!node;
},
domNodeIsAttachedToDocument: function (node){
return ko.utils.domNodeIsContainedBy(node, node.ownerDocument.documentElement);
},
anyDomNodeIsAttachedToDocument: function(nodes){
return !!ko.utils.arrayFirst(nodes, ko.utils.domNodeIsAttachedToDocument);
},
tagNameLower: function(element){
return element&&element.tagName&&element.tagName.toLowerCase();
},
catchFunctionErrors: function (delegate){
return ko['onError'] ? function (){
try {
return delegate.apply(this, arguments);
} catch (e){
ko['onError']&&ko['onError'](e);
throw e;
}}:delegate;
},
setTimeout: function (handler, timeout){
return setTimeout(ko.utils.catchFunctionErrors(handler), timeout);
},
deferError: function (error){
setTimeout(function (){
ko['onError']&&ko['onError'](error);
throw error;
}, 0);
},
registerEventHandler: function (element, eventType, handler){
var wrappedHandler=ko.utils.catchFunctionErrors(handler);
var mustUseAttachEvent=ieVersion&&eventsThatMustBeRegisteredUsingAttachEvent[eventType];
if(!ko.options['useOnlyNativeEvents']&&!mustUseAttachEvent&&jQueryInstance){
jQueryInstance(element)['bind'](eventType, wrappedHandler);
}else if(!mustUseAttachEvent&&typeof element.addEventListener=="function")
element.addEventListener(eventType, wrappedHandler, false);
else if(typeof element.attachEvent!="undefined"){
var attachEventHandler=function (event){ wrappedHandler.call(element, event); },
attachEventName="on" + eventType;
element.attachEvent(attachEventName, attachEventHandler);
ko.utils.domNodeDisposal.addDisposeCallback(element, function(){
element.detachEvent(attachEventName, attachEventHandler);
});
} else
throw new Error("Browser doesn't support addEventListener or attachEvent");
},
triggerEvent: function (element, eventType){
if(!(element&&element.nodeType))
throw new Error("element must be a DOM node when calling triggerEvent");
var useClickWorkaround=isClickOnCheckableElement(element, eventType);
if(!ko.options['useOnlyNativeEvents']&&jQueryInstance&&!useClickWorkaround){
jQueryInstance(element)['trigger'](eventType);
}else if(typeof document.createEvent=="function"){
if(typeof element.dispatchEvent=="function"){
var eventCategory=knownEventTypesByEventName[eventType]||"HTMLEvents";
var event=document.createEvent(eventCategory);
event.initEvent(eventType, true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, element);
element.dispatchEvent(event);
}
else
throw new Error("The supplied element doesn't support dispatchEvent");
}else if(useClickWorkaround&&element.click){
element.click();
}else if(typeof element.fireEvent!="undefined"){
element.fireEvent("on" + eventType);
}else{
throw new Error("Browser doesn't support triggering events");
}},
unwrapObservable: function (value){
return ko.isObservable(value) ? value():value;
},
peekObservable: function (value){
return ko.isObservable(value) ? value.peek():value;
},
toggleDomNodeCssClass: toggleDomNodeCssClass,
setTextContent: function(element, textContent){
var value=ko.utils.unwrapObservable(textContent);
if((value===null)||(value===undefined))
value="";
var innerTextNode=ko.virtualElements.firstChild(element);
if(!innerTextNode||innerTextNode.nodeType!=3||ko.virtualElements.nextSibling(innerTextNode)){
ko.virtualElements.setDomNodeChildren(element, [element.ownerDocument.createTextNode(value)]);
}else{
innerTextNode.data=value;
}
ko.utils.forceRefresh(element);
},
setElementName: function(element, name){
element.name=name;
if(ieVersion <=7){
try {
element.mergeAttributes(document.createElement("<input name='" + element.name + "'/>"), false);
}
catch(e){}}
},
forceRefresh: function(node){
if(ieVersion >=9){
var elem=node.nodeType==1 ? node:node.parentNode;
if(elem.style)
elem.style.zoom=elem.style.zoom;
}},
ensureSelectElementIsRenderedCorrectly: function(selectElement){
if(ieVersion){
var originalWidth=selectElement.style.width;
selectElement.style.width=0;
selectElement.style.width=originalWidth;
}},
range: function (min, max){
min=ko.utils.unwrapObservable(min);
max=ko.utils.unwrapObservable(max);
var result=[];
for (var i=min; i <=max; i++)
result.push(i);
return result;
},
makeArray: function(arrayLikeObject){
var result=[];
for (var i=0, j=arrayLikeObject.length; i < j; i++){
result.push(arrayLikeObject[i]);
};
return result;
},
createSymbolOrString: function(identifier){
return canUseSymbols ? Symbol(identifier):identifier;
},
isIe6:isIe6,
isIe7:isIe7,
ieVersion:ieVersion,
getFormFields: function(form, fieldName){
var fields=ko.utils.makeArray(form.getElementsByTagName("input")).concat(ko.utils.makeArray(form.getElementsByTagName("textarea")));
var isMatchingField=(typeof fieldName=='string')
? function(field){ return field.name===fieldName }
: function(field){ return fieldName.test(field.name) };
var matches=[];
for (var i=fields.length - 1; i >=0; i--){
if(isMatchingField(fields[i]))
matches.push(fields[i]);
};
return matches;
},
parseJson: function (jsonString){
if(typeof jsonString=="string"){
jsonString=ko.utils.stringTrim(jsonString);
if(jsonString){
if(JSON&&JSON.parse)
return JSON.parse(jsonString);
return (new Function("return " + jsonString))();
}}
return null;
},
stringifyJson: function (data, replacer, space){
if(!JSON||!JSON.stringify)
throw new Error("Cannot find JSON.stringify(). Some browsers (e.g., IE < 8) don't support it natively, but you can overcome this by adding a script reference to json2.js, downloadable from http://www.json.org/json2.js");
return JSON.stringify(ko.utils.unwrapObservable(data), replacer, space);
},
postJson: function (urlOrForm, data, options){
options=options||{};
var params=options['params']||{};
var includeFields=options['includeFields']||this.fieldsIncludedWithJsonPost;
var url=urlOrForm;
if((typeof urlOrForm=='object')&&(ko.utils.tagNameLower(urlOrForm)==="form")){
var originalForm=urlOrForm;
url=originalForm.action;
for (var i=includeFields.length - 1; i >=0; i--){
var fields=ko.utils.getFormFields(originalForm, includeFields[i]);
for (var j=fields.length - 1; j >=0; j--)
params[fields[j].name]=fields[j].value;
}}
data=ko.utils.unwrapObservable(data);
var form=document.createElement("form");
form.style.display="none";
form.action=url;
form.method="post";
for (var key in data){
var input=document.createElement("input");
input.type="hidden";
input.name=key;
input.value=ko.utils.stringifyJson(ko.utils.unwrapObservable(data[key]));
form.appendChild(input);
}
objectForEach(params, function(key, value){
var input=document.createElement("input");
input.type="hidden";
input.name=key;
input.value=value;
form.appendChild(input);
});
document.body.appendChild(form);
options['submitter'] ? options['submitter'](form):form.submit();
setTimeout(function (){ form.parentNode.removeChild(form); }, 0);
}}
}());
ko.exportSymbol('utils', ko.utils);
ko.exportSymbol('utils.arrayForEach', ko.utils.arrayForEach);
ko.exportSymbol('utils.arrayFirst', ko.utils.arrayFirst);
ko.exportSymbol('utils.arrayFilter', ko.utils.arrayFilter);
ko.exportSymbol('utils.arrayGetDistinctValues', ko.utils.arrayGetDistinctValues);
ko.exportSymbol('utils.arrayIndexOf', ko.utils.arrayIndexOf);
ko.exportSymbol('utils.arrayMap', ko.utils.arrayMap);
ko.exportSymbol('utils.arrayPushAll', ko.utils.arrayPushAll);
ko.exportSymbol('utils.arrayRemoveItem', ko.utils.arrayRemoveItem);
ko.exportSymbol('utils.extend', ko.utils.extend);
ko.exportSymbol('utils.fieldsIncludedWithJsonPost', ko.utils.fieldsIncludedWithJsonPost);
ko.exportSymbol('utils.getFormFields', ko.utils.getFormFields);
ko.exportSymbol('utils.peekObservable', ko.utils.peekObservable);
ko.exportSymbol('utils.postJson', ko.utils.postJson);
ko.exportSymbol('utils.parseJson', ko.utils.parseJson);
ko.exportSymbol('utils.registerEventHandler', ko.utils.registerEventHandler);
ko.exportSymbol('utils.stringifyJson', ko.utils.stringifyJson);
ko.exportSymbol('utils.range', ko.utils.range);
ko.exportSymbol('utils.toggleDomNodeCssClass', ko.utils.toggleDomNodeCssClass);
ko.exportSymbol('utils.triggerEvent', ko.utils.triggerEvent);
ko.exportSymbol('utils.unwrapObservable', ko.utils.unwrapObservable);
ko.exportSymbol('utils.objectForEach', ko.utils.objectForEach);
ko.exportSymbol('utils.addOrRemoveItem', ko.utils.addOrRemoveItem);
ko.exportSymbol('utils.setTextContent', ko.utils.setTextContent);
ko.exportSymbol('unwrap', ko.utils.unwrapObservable);
if(!Function.prototype['bind']){
Function.prototype['bind']=function (object){
var originalFunction=this;
if(arguments.length===1){
return function (){
return originalFunction.apply(object, arguments);
};}else{
var partialArgs=Array.prototype.slice.call(arguments, 1);
return function (){
var args=partialArgs.slice(0);
args.push.apply(args, arguments);
return originalFunction.apply(object, args);
};}};}
ko.utils.domData=new (function (){
var uniqueId=0;
var dataStoreKeyExpandoPropertyName="__ko__" + (new Date).getTime();
var dataStore={};
function getAll(node, createIfNotFound){
var dataStoreKey=node[dataStoreKeyExpandoPropertyName];
var hasExistingDataStore=dataStoreKey&&(dataStoreKey!=="null")&&dataStore[dataStoreKey];
if(!hasExistingDataStore){
if(!createIfNotFound)
return undefined;
dataStoreKey=node[dataStoreKeyExpandoPropertyName]="ko" + uniqueId++;
dataStore[dataStoreKey]={};}
return dataStore[dataStoreKey];
}
return {
get: function (node, key){
var allDataForNode=getAll(node, false);
return allDataForNode===undefined ? undefined:allDataForNode[key];
},
set: function (node, key, value){
if(value===undefined){
if(getAll(node, false)===undefined)
return;
}
var allDataForNode=getAll(node, true);
allDataForNode[key]=value;
},
clear: function (node){
var dataStoreKey=node[dataStoreKeyExpandoPropertyName];
if(dataStoreKey){
delete dataStore[dataStoreKey];
node[dataStoreKeyExpandoPropertyName]=null;
return true;
}
return false;
},
nextKey: function (){
return (uniqueId++) + dataStoreKeyExpandoPropertyName;
}};})();
ko.exportSymbol('utils.domData', ko.utils.domData);
ko.exportSymbol('utils.domData.clear', ko.utils.domData.clear);
ko.utils.domNodeDisposal=new (function (){
var domDataKey=ko.utils.domData.nextKey();
var cleanableNodeTypes={ 1: true, 8: true, 9: true };
var cleanableNodeTypesWithDescendants={ 1: true, 9: true };
function getDisposeCallbacksCollection(node, createIfNotFound){
var allDisposeCallbacks=ko.utils.domData.get(node, domDataKey);
if((allDisposeCallbacks===undefined)&&createIfNotFound){
allDisposeCallbacks=[];
ko.utils.domData.set(node, domDataKey, allDisposeCallbacks);
}
return allDisposeCallbacks;
}
function destroyCallbacksCollection(node){
ko.utils.domData.set(node, domDataKey, undefined);
}
function cleanSingleNode(node){
var callbacks=getDisposeCallbacksCollection(node, false);
if(callbacks){
callbacks=callbacks.slice(0);
for (var i=0; i < callbacks.length; i++)
callbacks[i](node);
}
ko.utils.domData.clear(node);
ko.utils.domNodeDisposal["cleanExternalData"](node);
if(cleanableNodeTypesWithDescendants[node.nodeType])
cleanImmediateCommentTypeChildren(node);
}
function cleanImmediateCommentTypeChildren(nodeWithChildren){
var child, nextChild=nodeWithChildren.firstChild;
while (child=nextChild){
nextChild=child.nextSibling;
if(child.nodeType===8)
cleanSingleNode(child);
}}
return {
addDisposeCallback:function(node, callback){
if(typeof callback!="function")
throw new Error("Callback must be a function");
getDisposeCallbacksCollection(node, true).push(callback);
},
removeDisposeCallback:function(node, callback){
var callbacksCollection=getDisposeCallbacksCollection(node, false);
if(callbacksCollection){
ko.utils.arrayRemoveItem(callbacksCollection, callback);
if(callbacksCollection.length==0)
destroyCallbacksCollection(node);
}},
cleanNode:function(node){
if(cleanableNodeTypes[node.nodeType]){
cleanSingleNode(node);
if(cleanableNodeTypesWithDescendants[node.nodeType]){
var descendants=[];
ko.utils.arrayPushAll(descendants, node.getElementsByTagName("*"));
for (var i=0, j=descendants.length; i < j; i++)
cleanSingleNode(descendants[i]);
}}
return node;
},
removeNode:function(node){
ko.cleanNode(node);
if(node.parentNode)
node.parentNode.removeChild(node);
},
"cleanExternalData":function (node){
if(jQueryInstance&&(typeof jQueryInstance['cleanData']=="function"))
jQueryInstance['cleanData']([node]);
}};})();
ko.cleanNode=ko.utils.domNodeDisposal.cleanNode;
ko.removeNode=ko.utils.domNodeDisposal.removeNode;
ko.exportSymbol('cleanNode', ko.cleanNode);
ko.exportSymbol('removeNode', ko.removeNode);
ko.exportSymbol('utils.domNodeDisposal', ko.utils.domNodeDisposal);
ko.exportSymbol('utils.domNodeDisposal.addDisposeCallback', ko.utils.domNodeDisposal.addDisposeCallback);
ko.exportSymbol('utils.domNodeDisposal.removeDisposeCallback', ko.utils.domNodeDisposal.removeDisposeCallback);
(function (){
var none=[0, "", ""],
table=[1, "<table>", "</table>"],
tbody=[2, "<table><tbody>", "</tbody></table>"],
tr=[3, "<table><tbody><tr>", "</tr></tbody></table>"],
select=[1, "<select multiple='multiple'>", "</select>"],
lookup={
'thead': table,
'tbody': table,
'tfoot': table,
'tr': tbody,
'td': tr,
'th': tr,
'option': select,
'optgroup': select
},
mayRequireCreateElementHack=ko.utils.ieVersion <=8;
function getWrap(tags){
var m=tags.match(/^<([a-z]+)[ >]/);
return (m&&lookup[m[1]])||none;
}
function simpleHtmlParse(html, documentContext){
documentContext||(documentContext=document);
var windowContext=documentContext['parentWindow']||documentContext['defaultView']||window;
var tags=ko.utils.stringTrim(html).toLowerCase(), div=documentContext.createElement("div"),
wrap=getWrap(tags),
depth=wrap[0];
var markup="ignored<div>" + wrap[1] + html + wrap[2] + "</div>";
if(typeof windowContext['innerShiv']=="function"){
div.appendChild(windowContext['innerShiv'](markup));
}else{
if(mayRequireCreateElementHack){
documentContext.appendChild(div);
}
div.innerHTML=markup;
if(mayRequireCreateElementHack){
div.parentNode.removeChild(div);
}}
while (depth--)
div=div.lastChild;
return ko.utils.makeArray(div.lastChild.childNodes);
}
function jQueryHtmlParse(html, documentContext){
if(jQueryInstance['parseHTML']){
return jQueryInstance['parseHTML'](html, documentContext)||[];
}else{
var elems=jQueryInstance['clean']([html], documentContext);
if(elems&&elems[0]){
var elem=elems[0];
while (elem.parentNode&&elem.parentNode.nodeType!==11 )
elem=elem.parentNode;
if(elem.parentNode)
elem.parentNode.removeChild(elem);
}
return elems;
}}
ko.utils.parseHtmlFragment=function(html, documentContext){
return jQueryInstance ?
jQueryHtmlParse(html, documentContext) :
simpleHtmlParse(html, documentContext);
};
ko.utils.setHtml=function(node, html){
ko.utils.emptyDomNode(node);
html=ko.utils.unwrapObservable(html);
if((html!==null)&&(html!==undefined)){
if(typeof html!='string')
html=html.toString();
if(jQueryInstance){
jQueryInstance(node)['html'](html);
}else{
var parsedNodes=ko.utils.parseHtmlFragment(html, node.ownerDocument);
for (var i=0; i < parsedNodes.length; i++)
node.appendChild(parsedNodes[i]);
}}
};})();
ko.exportSymbol('utils.parseHtmlFragment', ko.utils.parseHtmlFragment);
ko.exportSymbol('utils.setHtml', ko.utils.setHtml);
ko.memoization=(function (){
var memos={};
function randomMax8HexChars(){
return (((1 + Math.random()) * 0x100000000) | 0).toString(16).substring(1);
}
function generateRandomId(){
return randomMax8HexChars() + randomMax8HexChars();
}
function findMemoNodes(rootNode, appendToArray){
if(!rootNode)
return;
if(rootNode.nodeType==8){
var memoId=ko.memoization.parseMemoText(rootNode.nodeValue);
if(memoId!=null)
appendToArray.push({ domNode: rootNode, memoId: memoId });
}else if(rootNode.nodeType==1){
for (var i=0, childNodes=rootNode.childNodes, j=childNodes.length; i < j; i++)
findMemoNodes(childNodes[i], appendToArray);
}}
return {
memoize: function (callback){
if(typeof callback!="function")
throw new Error("You can only pass a function to ko.memoization.memoize()");
var memoId=generateRandomId();
memos[memoId]=callback;
return "<!--[ko_memo:" + memoId + "]-->";
},
unmemoize: function (memoId, callbackParams){
var callback=memos[memoId];
if(callback===undefined)
throw new Error("Couldn't find any memo with ID " + memoId + ". Perhaps it's already been unmemoized.");
try {
callback.apply(null, callbackParams||[]);
return true;
}
finally { delete memos[memoId]; }},
unmemoizeDomNodeAndDescendants: function (domNode, extraCallbackParamsArray){
var memos=[];
findMemoNodes(domNode, memos);
for (var i=0, j=memos.length; i < j; i++){
var node=memos[i].domNode;
var combinedParams=[node];
if(extraCallbackParamsArray)
ko.utils.arrayPushAll(combinedParams, extraCallbackParamsArray);
ko.memoization.unmemoize(memos[i].memoId, combinedParams);
node.nodeValue="";
if(node.parentNode)
node.parentNode.removeChild(node);
}},
parseMemoText: function (memoText){
var match=memoText.match(/^\[ko_memo\:(.*?)\]$/);
return match ? match[1]:null;
}};})();
ko.exportSymbol('memoization', ko.memoization);
ko.exportSymbol('memoization.memoize', ko.memoization.memoize);
ko.exportSymbol('memoization.unmemoize', ko.memoization.unmemoize);
ko.exportSymbol('memoization.parseMemoText', ko.memoization.parseMemoText);
ko.exportSymbol('memoization.unmemoizeDomNodeAndDescendants', ko.memoization.unmemoizeDomNodeAndDescendants);
ko.tasks=(function (){
var scheduler,
taskQueue=[],
taskQueueLength=0,
nextHandle=1,
nextIndexToProcess=0;
if(window['MutationObserver']){
scheduler=(function (callback){
var div=document.createElement("div");
new MutationObserver(callback).observe(div, {attributes: true});
return function (){ div.classList.toggle("foo"); };})(scheduledProcess);
}else if(document&&"onreadystatechange" in document.createElement("script")){
scheduler=function (callback){
var script=document.createElement("script");
script.onreadystatechange=function (){
script.onreadystatechange=null;
document.documentElement.removeChild(script);
script=null;
callback();
};
document.documentElement.appendChild(script);
};}else{
scheduler=function (callback){
setTimeout(callback, 0);
};}
function processTasks(){
if(taskQueueLength){
var mark=taskQueueLength, countMarks=0;
for (var task; nextIndexToProcess < taskQueueLength;){
if(task=taskQueue[nextIndexToProcess++]){
if(nextIndexToProcess > mark){
if(++countMarks >=5000){
nextIndexToProcess=taskQueueLength;
ko.utils.deferError(Error("'Too much recursion' after processing " + countMarks + " task groups."));
break;
}
mark=taskQueueLength;
}
try {
task();
} catch (ex){
ko.utils.deferError(ex);
}}
}}
}
function scheduledProcess(){
processTasks();
nextIndexToProcess=taskQueueLength=taskQueue.length=0;
}
function scheduleTaskProcessing(){
ko.tasks['scheduler'](scheduledProcess);
}
var tasks={
'scheduler': scheduler,
schedule: function (func){
if(!taskQueueLength){
scheduleTaskProcessing();
}
taskQueue[taskQueueLength++]=func;
return nextHandle++;
},
cancel: function (handle){
var index=handle - (nextHandle - taskQueueLength);
if(index >=nextIndexToProcess&&index < taskQueueLength){
taskQueue[index]=null;
}},
'resetForTesting': function (){
var length=taskQueueLength - nextIndexToProcess;
nextIndexToProcess=taskQueueLength=taskQueue.length=0;
return length;
},
runEarly: processTasks
};
return tasks;
})();
ko.exportSymbol('tasks', ko.tasks);
ko.exportSymbol('tasks.schedule', ko.tasks.schedule);
ko.exportSymbol('tasks.runEarly', ko.tasks.runEarly);
ko.extenders={
'throttle': function(target, timeout){
target['throttleEvaluation']=timeout;
var writeTimeoutInstance=null;
return ko.dependentObservable({
'read': target,
'write': function(value){
clearTimeout(writeTimeoutInstance);
writeTimeoutInstance=ko.utils.setTimeout(function(){
target(value);
}, timeout);
}});
},
'rateLimit': function(target, options){
var timeout, method, limitFunction;
if(typeof options=='number'){
timeout=options;
}else{
timeout=options['timeout'];
method=options['method'];
}
target._deferUpdates=false;
limitFunction=method=='notifyWhenChangesStop' ?  debounce:throttle;
target.limit(function(callback){
return limitFunction(callback, timeout);
});
},
'deferred': function(target, options){
if(options!==true){
throw new Error('The \'deferred\' extender only accepts the value \'true\', because it is not supported to turn deferral off once enabled.')
}
if(!target._deferUpdates){
target._deferUpdates=true;
target.limit(function (callback){
var handle,
ignoreUpdates=false;
return function (){
if(!ignoreUpdates){
ko.tasks.cancel(handle);
handle=ko.tasks.schedule(callback);
try {
ignoreUpdates=true;
target['notifySubscribers'](undefined, 'dirty');
} finally {
ignoreUpdates=false;
}}
};});
}},
'notify': function(target, notifyWhen){
target["equalityComparer"]=notifyWhen=="always" ?
null :
valuesArePrimitiveAndEqual;
}};
var primitiveTypes={ 'undefined':1, 'boolean':1, 'number':1, 'string':1 };
function valuesArePrimitiveAndEqual(a, b){
var oldValueIsPrimitive=(a===null)||(typeof(a) in primitiveTypes);
return oldValueIsPrimitive ? (a===b):false;
}
function throttle(callback, timeout){
var timeoutInstance;
return function (){
if(!timeoutInstance){
timeoutInstance=ko.utils.setTimeout(function (){
timeoutInstance=undefined;
callback();
}, timeout);
}};}
function debounce(callback, timeout){
var timeoutInstance;
return function (){
clearTimeout(timeoutInstance);
timeoutInstance=ko.utils.setTimeout(callback, timeout);
};}
function applyExtenders(requestedExtenders){
var target=this;
if(requestedExtenders){
ko.utils.objectForEach(requestedExtenders, function(key, value){
var extenderHandler=ko.extenders[key];
if(typeof extenderHandler=='function'){
target=extenderHandler(target, value)||target;
}});
}
return target;
}
ko.exportSymbol('extenders', ko.extenders);
ko.subscription=function (target, callback, disposeCallback){
this._target=target;
this.callback=callback;
this.disposeCallback=disposeCallback;
this.isDisposed=false;
ko.exportProperty(this, 'dispose', this.dispose);
};
ko.subscription.prototype.dispose=function (){
this.isDisposed=true;
this.disposeCallback();
};
ko.subscribable=function (){
ko.utils.setPrototypeOfOrExtend(this, ko_subscribable_fn);
ko_subscribable_fn.init(this);
}
var defaultEvent="change";
function limitNotifySubscribers(value, event){
if(!event||event===defaultEvent){
this._limitChange(value);
}else if(event==='beforeChange'){
this._limitBeforeChange(value);
}else{
this._origNotifySubscribers(value, event);
}}
var ko_subscribable_fn={
init: function(instance){
instance._subscriptions={ "change": [] };
instance._versionNumber=1;
},
subscribe: function (callback, callbackTarget, event){
var self=this;
event=event||defaultEvent;
var boundCallback=callbackTarget ? callback.bind(callbackTarget):callback;
var subscription=new ko.subscription(self, boundCallback, function (){
ko.utils.arrayRemoveItem(self._subscriptions[event], subscription);
if(self.afterSubscriptionRemove)
self.afterSubscriptionRemove(event);
});
if(self.beforeSubscriptionAdd)
self.beforeSubscriptionAdd(event);
if(!self._subscriptions[event])
self._subscriptions[event]=[];
self._subscriptions[event].push(subscription);
return subscription;
},
"notifySubscribers": function (valueToNotify, event){
event=event||defaultEvent;
if(event===defaultEvent){
this.updateVersion();
}
if(this.hasSubscriptionsForEvent(event)){
var subs=event===defaultEvent&&this._changeSubscriptions||this._subscriptions[event].slice(0);
try {
ko.dependencyDetection.begin();
for (var i=0, subscription; subscription=subs[i]; ++i){
if(!subscription.isDisposed)
subscription.callback(valueToNotify);
}} finally {
ko.dependencyDetection.end();
}}
},
getVersion: function (){
return this._versionNumber;
},
hasChanged: function (versionToCheck){
return this.getVersion()!==versionToCheck;
},
updateVersion: function (){
++this._versionNumber;
},
limit: function(limitFunction){
var self=this, selfIsObservable=ko.isObservable(self),
ignoreBeforeChange, notifyNextChange, previousValue, pendingValue, beforeChange='beforeChange';
if(!self._origNotifySubscribers){
self._origNotifySubscribers=self["notifySubscribers"];
self["notifySubscribers"]=limitNotifySubscribers;
}
var finish=limitFunction(function(){
self._notificationIsPending=false;
if(selfIsObservable&&pendingValue===self){
pendingValue=self._evalIfChanged ? self._evalIfChanged():self();
}
var shouldNotify=notifyNextChange||self.isDifferent(previousValue, pendingValue);
notifyNextChange=ignoreBeforeChange=false;
if(shouldNotify){
self._origNotifySubscribers(previousValue=pendingValue);
}});
self._limitChange=function(value){
self._changeSubscriptions=self._subscriptions[defaultEvent].slice(0);
self._notificationIsPending=ignoreBeforeChange=true;
pendingValue=value;
finish();
};
self._limitBeforeChange=function(value){
if(!ignoreBeforeChange){
previousValue=value;
self._origNotifySubscribers(value, beforeChange);
}};
self._notifyNextChangeIfValueIsDifferent=function(){
if(self.isDifferent(previousValue, self.peek(true ))){
notifyNextChange=true;
}};},
hasSubscriptionsForEvent: function(event){
return this._subscriptions[event]&&this._subscriptions[event].length;
},
getSubscriptionsCount: function (event){
if(event){
return this._subscriptions[event]&&this._subscriptions[event].length||0;
}else{
var total=0;
ko.utils.objectForEach(this._subscriptions, function(eventName, subscriptions){
if(eventName!=='dirty')
total +=subscriptions.length;
});
return total;
}},
isDifferent: function(oldValue, newValue){
return !this['equalityComparer']||!this['equalityComparer'](oldValue, newValue);
},
extend: applyExtenders
};
ko.exportProperty(ko_subscribable_fn, 'subscribe', ko_subscribable_fn.subscribe);
ko.exportProperty(ko_subscribable_fn, 'extend', ko_subscribable_fn.extend);
ko.exportProperty(ko_subscribable_fn, 'getSubscriptionsCount', ko_subscribable_fn.getSubscriptionsCount);
if(ko.utils.canSetPrototype){
ko.utils.setPrototypeOf(ko_subscribable_fn, Function.prototype);
}
ko.subscribable['fn']=ko_subscribable_fn;
ko.isSubscribable=function (instance){
return instance!=null&&typeof instance.subscribe=="function"&&typeof instance["notifySubscribers"]=="function";
};
ko.exportSymbol('subscribable', ko.subscribable);
ko.exportSymbol('isSubscribable', ko.isSubscribable);
ko.computedContext=ko.dependencyDetection=(function (){
var outerFrames=[],
currentFrame,
lastId=0;
function getId(){
return ++lastId;
}
function begin(options){
outerFrames.push(currentFrame);
currentFrame=options;
}
function end(){
currentFrame=outerFrames.pop();
}
return {
begin: begin,
end: end,
registerDependency: function (subscribable){
if(currentFrame){
if(!ko.isSubscribable(subscribable))
throw new Error("Only subscribable things can act as dependencies");
currentFrame.callback.call(currentFrame.callbackTarget, subscribable, subscribable._id||(subscribable._id=getId()));
}},
ignore: function (callback, callbackTarget, callbackArgs){
try {
begin();
return callback.apply(callbackTarget, callbackArgs||[]);
} finally {
end();
}},
getDependenciesCount: function (){
if(currentFrame)
return currentFrame.computed.getDependenciesCount();
},
isInitial: function(){
if(currentFrame)
return currentFrame.isInitial;
}};})();
ko.exportSymbol('computedContext', ko.computedContext);
ko.exportSymbol('computedContext.getDependenciesCount', ko.computedContext.getDependenciesCount);
ko.exportSymbol('computedContext.isInitial', ko.computedContext.isInitial);
ko.exportSymbol('ignoreDependencies', ko.ignoreDependencies=ko.dependencyDetection.ignore);
var observableLatestValue=ko.utils.createSymbolOrString('_latestValue');
ko.observable=function (initialValue){
function observable(){
if(arguments.length > 0){
if(observable.isDifferent(observable[observableLatestValue], arguments[0])){
observable.valueWillMutate();
observable[observableLatestValue]=arguments[0];
observable.valueHasMutated();
}
return this;
}else{
ko.dependencyDetection.registerDependency(observable);
return observable[observableLatestValue];
}}
observable[observableLatestValue]=initialValue;
if(!ko.utils.canSetPrototype){
ko.utils.extend(observable, ko.subscribable['fn']);
}
ko.subscribable['fn'].init(observable);
ko.utils.setPrototypeOfOrExtend(observable, observableFn);
if(ko.options['deferUpdates']){
ko.extenders['deferred'](observable, true);
}
return observable;
}
var observableFn={
'equalityComparer': valuesArePrimitiveAndEqual,
peek: function(){ return this[observableLatestValue]; },
valueHasMutated: function (){ this['notifySubscribers'](this[observableLatestValue]); },
valueWillMutate: function (){ this['notifySubscribers'](this[observableLatestValue], 'beforeChange'); }};
if(ko.utils.canSetPrototype){
ko.utils.setPrototypeOf(observableFn, ko.subscribable['fn']);
}
var protoProperty=ko.observable.protoProperty='__ko_proto__';
observableFn[protoProperty]=ko.observable;
ko.hasPrototype=function(instance, prototype){
if((instance===null)||(instance===undefined)||(instance[protoProperty]===undefined)) return false;
if(instance[protoProperty]===prototype) return true;
return ko.hasPrototype(instance[protoProperty], prototype);
};
ko.isObservable=function (instance){
return ko.hasPrototype(instance, ko.observable);
}
ko.isWriteableObservable=function (instance){
if((typeof instance=='function')&&instance[protoProperty]===ko.observable)
return true;
if((typeof instance=='function')&&(instance[protoProperty]===ko.dependentObservable)&&(instance.hasWriteFunction))
return true;
return false;
}
ko.exportSymbol('observable', ko.observable);
ko.exportSymbol('isObservable', ko.isObservable);
ko.exportSymbol('isWriteableObservable', ko.isWriteableObservable);
ko.exportSymbol('isWritableObservable', ko.isWriteableObservable);
ko.exportSymbol('observable.fn', observableFn);
ko.exportProperty(observableFn, 'peek', observableFn.peek);
ko.exportProperty(observableFn, 'valueHasMutated', observableFn.valueHasMutated);
ko.exportProperty(observableFn, 'valueWillMutate', observableFn.valueWillMutate);
ko.observableArray=function (initialValues){
initialValues=initialValues||[];
if(typeof initialValues!='object'||!('length' in initialValues))
throw new Error("The argument passed when initializing an observable array must be an array, or null, or undefined.");
var result=ko.observable(initialValues);
ko.utils.setPrototypeOfOrExtend(result, ko.observableArray['fn']);
return result.extend({'trackArrayChanges':true});
};
ko.observableArray['fn']={
'remove': function (valueOrPredicate){
var underlyingArray=this.peek();
var removedValues=[];
var predicate=typeof valueOrPredicate=="function"&&!ko.isObservable(valueOrPredicate) ? valueOrPredicate:function (value){ return value===valueOrPredicate; };
for (var i=0; i < underlyingArray.length; i++){
var value=underlyingArray[i];
if(predicate(value)){
if(removedValues.length===0){
this.valueWillMutate();
}
removedValues.push(value);
underlyingArray.splice(i, 1);
i--;
}}
if(removedValues.length){
this.valueHasMutated();
}
return removedValues;
},
'removeAll': function (arrayOfValues){
if(arrayOfValues===undefined){
var underlyingArray=this.peek();
var allValues=underlyingArray.slice(0);
this.valueWillMutate();
underlyingArray.splice(0, underlyingArray.length);
this.valueHasMutated();
return allValues;
}
if(!arrayOfValues)
return [];
return this['remove'](function (value){
return ko.utils.arrayIndexOf(arrayOfValues, value) >=0;
});
},
'destroy': function (valueOrPredicate){
var underlyingArray=this.peek();
var predicate=typeof valueOrPredicate=="function"&&!ko.isObservable(valueOrPredicate) ? valueOrPredicate:function (value){ return value===valueOrPredicate; };
this.valueWillMutate();
for (var i=underlyingArray.length - 1; i >=0; i--){
var value=underlyingArray[i];
if(predicate(value))
underlyingArray[i]["_destroy"]=true;
}
this.valueHasMutated();
},
'destroyAll': function (arrayOfValues){
if(arrayOfValues===undefined)
return this['destroy'](function(){ return true });
if(!arrayOfValues)
return [];
return this['destroy'](function (value){
return ko.utils.arrayIndexOf(arrayOfValues, value) >=0;
});
},
'indexOf': function (item){
var underlyingArray=this();
return ko.utils.arrayIndexOf(underlyingArray, item);
},
'replace': function(oldItem, newItem){
var index=this['indexOf'](oldItem);
if(index >=0){
this.valueWillMutate();
this.peek()[index]=newItem;
this.valueHasMutated();
}}
};
if(ko.utils.canSetPrototype){
ko.utils.setPrototypeOf(ko.observableArray['fn'], ko.observable['fn']);
}
ko.utils.arrayForEach(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function (methodName){
ko.observableArray['fn'][methodName]=function (){
var underlyingArray=this.peek();
this.valueWillMutate();
this.cacheDiffForKnownOperation(underlyingArray, methodName, arguments);
var methodCallResult=underlyingArray[methodName].apply(underlyingArray, arguments);
this.valueHasMutated();
return methodCallResult===underlyingArray ? this:methodCallResult;
};});
ko.utils.arrayForEach(["slice"], function (methodName){
ko.observableArray['fn'][methodName]=function (){
var underlyingArray=this();
return underlyingArray[methodName].apply(underlyingArray, arguments);
};});
ko.exportSymbol('observableArray', ko.observableArray);
var arrayChangeEventName='arrayChange';
ko.extenders['trackArrayChanges']=function(target, options){
target.compareArrayOptions={};
if(options&&typeof options=="object"){
ko.utils.extend(target.compareArrayOptions, options);
}
target.compareArrayOptions['sparse']=true;
if(target.cacheDiffForKnownOperation){
return;
}
var trackingChanges=false,
cachedDiff=null,
arrayChangeSubscription,
pendingNotifications=0,
underlyingNotifySubscribersFunction,
underlyingBeforeSubscriptionAddFunction=target.beforeSubscriptionAdd,
underlyingAfterSubscriptionRemoveFunction=target.afterSubscriptionRemove;
target.beforeSubscriptionAdd=function (event){
if(underlyingBeforeSubscriptionAddFunction)
underlyingBeforeSubscriptionAddFunction.call(target, event);
if(event===arrayChangeEventName){
trackChanges();
}};
target.afterSubscriptionRemove=function (event){
if(underlyingAfterSubscriptionRemoveFunction)
underlyingAfterSubscriptionRemoveFunction.call(target, event);
if(event===arrayChangeEventName&&!target.hasSubscriptionsForEvent(arrayChangeEventName)){
if(underlyingNotifySubscribersFunction){
target['notifySubscribers']=underlyingNotifySubscribersFunction;
underlyingNotifySubscribersFunction=undefined;
}
arrayChangeSubscription.dispose();
trackingChanges=false;
}};
function trackChanges(){
if(trackingChanges){
return;
}
trackingChanges=true;
underlyingNotifySubscribersFunction=target['notifySubscribers'];
target['notifySubscribers']=function(valueToNotify, event){
if(!event||event===defaultEvent){
++pendingNotifications;
}
return underlyingNotifySubscribersFunction.apply(this, arguments);
};
var previousContents=[].concat(target.peek()||[]);
cachedDiff=null;
arrayChangeSubscription=target.subscribe(function(currentContents){
currentContents=[].concat(currentContents||[]);
if(target.hasSubscriptionsForEvent(arrayChangeEventName)){
var changes=getChanges(previousContents, currentContents);
}
previousContents=currentContents;
cachedDiff=null;
pendingNotifications=0;
if(changes&&changes.length){
target['notifySubscribers'](changes, arrayChangeEventName);
}});
}
function getChanges(previousContents, currentContents){
if(!cachedDiff||pendingNotifications > 1){
cachedDiff=ko.utils.compareArrays(previousContents, currentContents, target.compareArrayOptions);
}
return cachedDiff;
}
target.cacheDiffForKnownOperation=function(rawArray, operationName, args){
if(!trackingChanges||pendingNotifications){
return;
}
var diff=[],
arrayLength=rawArray.length,
argsLength=args.length,
offset=0;
function pushDiff(status, value, index){
return diff[diff.length]={ 'status': status, 'value': value, 'index': index };}
switch (operationName){
case 'push':
offset=arrayLength;
case 'unshift':
for (var index=0; index < argsLength; index++){
pushDiff('added', args[index], offset + index);
}
break;
case 'pop':
offset=arrayLength - 1;
case 'shift':
if(arrayLength){
pushDiff('deleted', rawArray[offset], offset);
}
break;
case 'splice':
var startIndex=Math.min(Math.max(0, args[0] < 0 ? arrayLength + args[0]:args[0]), arrayLength),
endDeleteIndex=argsLength===1 ? arrayLength:Math.min(startIndex + (args[1]||0), arrayLength),
endAddIndex=startIndex + argsLength - 2,
endIndex=Math.max(endDeleteIndex, endAddIndex),
additions=[], deletions=[];
for (var index=startIndex, argsIndex=2; index < endIndex; ++index, ++argsIndex){
if(index < endDeleteIndex)
deletions.push(pushDiff('deleted', rawArray[index], index));
if(index < endAddIndex)
additions.push(pushDiff('added', args[argsIndex], index));
}
ko.utils.findMovesInArrayComparison(deletions, additions);
break;
default:
return;
}
cachedDiff=diff;
};};
var computedState=ko.utils.createSymbolOrString('_state');
ko.computed=ko.dependentObservable=function (evaluatorFunctionOrOptions, evaluatorFunctionTarget, options){
if(typeof evaluatorFunctionOrOptions==="object"){
options=evaluatorFunctionOrOptions;
}else{
options=options||{};
if(evaluatorFunctionOrOptions){
options["read"]=evaluatorFunctionOrOptions;
}}
if(typeof options["read"]!="function")
throw Error("Pass a function that returns the value of the ko.computed");
var writeFunction=options["write"];
var state={
latestValue: undefined,
isStale: true,
isDirty: true,
isBeingEvaluated: false,
suppressDisposalUntilDisposeWhenReturnsFalse: false,
isDisposed: false,
pure: false,
isSleeping: false,
readFunction: options["read"],
evaluatorFunctionTarget: evaluatorFunctionTarget||options["owner"],
disposeWhenNodeIsRemoved: options["disposeWhenNodeIsRemoved"]||options.disposeWhenNodeIsRemoved||null,
disposeWhen: options["disposeWhen"]||options.disposeWhen,
domNodeDisposalCallback: null,
dependencyTracking: {},
dependenciesCount: 0,
evaluationTimeoutInstance: null
};
function computedObservable(){
if(arguments.length > 0){
if(typeof writeFunction==="function"){
writeFunction.apply(state.evaluatorFunctionTarget, arguments);
}else{
throw new Error("Cannot write a value to a ko.computed unless you specify a 'write' option. If you wish to read the current value, don't pass any parameters.");
}
return this;
}else{
ko.dependencyDetection.registerDependency(computedObservable);
if(state.isDirty||(state.isSleeping&&computedObservable.haveDependenciesChanged())){
computedObservable.evaluateImmediate();
}
return state.latestValue;
}}
computedObservable[computedState]=state;
computedObservable.hasWriteFunction=typeof writeFunction==="function";
if(!ko.utils.canSetPrototype){
ko.utils.extend(computedObservable, ko.subscribable['fn']);
}
ko.subscribable['fn'].init(computedObservable);
ko.utils.setPrototypeOfOrExtend(computedObservable, computedFn);
if(options['pure']){
state.pure=true;
state.isSleeping=true;
ko.utils.extend(computedObservable, pureComputedOverrides);
}else if(options['deferEvaluation']){
ko.utils.extend(computedObservable, deferEvaluationOverrides);
}
if(ko.options['deferUpdates']){
ko.extenders['deferred'](computedObservable, true);
}
if(DEBUG){
computedObservable["_options"]=options;
}
if(state.disposeWhenNodeIsRemoved){
state.suppressDisposalUntilDisposeWhenReturnsFalse=true;
if(!state.disposeWhenNodeIsRemoved.nodeType){
state.disposeWhenNodeIsRemoved=null;
}}
if(!state.isSleeping&&!options['deferEvaluation']){
computedObservable.evaluateImmediate();
}
if(state.disposeWhenNodeIsRemoved&&computedObservable.isActive()){
ko.utils.domNodeDisposal.addDisposeCallback(state.disposeWhenNodeIsRemoved, state.domNodeDisposalCallback=function (){
computedObservable.dispose();
});
}
return computedObservable;
};
function computedDisposeDependencyCallback(id, entryToDispose){
if(entryToDispose!==null&&entryToDispose.dispose){
entryToDispose.dispose();
}}
function computedBeginDependencyDetectionCallback(subscribable, id){
var computedObservable=this.computedObservable,
state=computedObservable[computedState];
if(!state.isDisposed){
if(this.disposalCount&&this.disposalCandidates[id]){
computedObservable.addDependencyTracking(id, subscribable, this.disposalCandidates[id]);
this.disposalCandidates[id]=null;
--this.disposalCount;
}else if(!state.dependencyTracking[id]){
computedObservable.addDependencyTracking(id, subscribable, state.isSleeping ? { _target: subscribable }:computedObservable.subscribeToDependency(subscribable));
}
if(subscribable._notificationIsPending){
subscribable._notifyNextChangeIfValueIsDifferent();
}}
}
var computedFn={
"equalityComparer": valuesArePrimitiveAndEqual,
getDependenciesCount: function (){
return this[computedState].dependenciesCount;
},
addDependencyTracking: function (id, target, trackingObj){
if(this[computedState].pure&&target===this){
throw Error("A 'pure' computed must not be called recursively");
}
this[computedState].dependencyTracking[id]=trackingObj;
trackingObj._order=this[computedState].dependenciesCount++;
trackingObj._version=target.getVersion();
},
haveDependenciesChanged: function (){
var id, dependency, dependencyTracking=this[computedState].dependencyTracking;
for (id in dependencyTracking){
if(dependencyTracking.hasOwnProperty(id)){
dependency=dependencyTracking[id];
if((this._evalDelayed&&dependency._target._notificationIsPending)||dependency._target.hasChanged(dependency._version)){
return true;
}}
}},
markDirty: function (){
if(this._evalDelayed&&!this[computedState].isBeingEvaluated){
this._evalDelayed(false );
}},
isActive: function (){
var state=this[computedState];
return state.isDirty||state.dependenciesCount > 0;
},
respondToChange: function (){
if(!this._notificationIsPending){
this.evaluatePossiblyAsync();
}else if(this[computedState].isDirty){
this[computedState].isStale=true;
}},
subscribeToDependency: function (target){
if(target._deferUpdates&&!this[computedState].disposeWhenNodeIsRemoved){
var dirtySub=target.subscribe(this.markDirty, this, 'dirty'),
changeSub=target.subscribe(this.respondToChange, this);
return {
_target: target,
dispose: function (){
dirtySub.dispose();
changeSub.dispose();
}};}else{
return target.subscribe(this.evaluatePossiblyAsync, this);
}},
evaluatePossiblyAsync: function (){
var computedObservable=this,
throttleEvaluationTimeout=computedObservable['throttleEvaluation'];
if(throttleEvaluationTimeout&&throttleEvaluationTimeout >=0){
clearTimeout(this[computedState].evaluationTimeoutInstance);
this[computedState].evaluationTimeoutInstance=ko.utils.setTimeout(function (){
computedObservable.evaluateImmediate(true );
}, throttleEvaluationTimeout);
}else if(computedObservable._evalDelayed){
computedObservable._evalDelayed(true );
}else{
computedObservable.evaluateImmediate(true );
}},
evaluateImmediate: function (notifyChange){
var computedObservable=this,
state=computedObservable[computedState],
disposeWhen=state.disposeWhen,
changed=false;
if(state.isBeingEvaluated){
return;
}
if(state.isDisposed){
return;
}
if(state.disposeWhenNodeIsRemoved&&!ko.utils.domNodeIsAttachedToDocument(state.disposeWhenNodeIsRemoved)||disposeWhen&&disposeWhen()){
if(!state.suppressDisposalUntilDisposeWhenReturnsFalse){
computedObservable.dispose();
return;
}}else{
state.suppressDisposalUntilDisposeWhenReturnsFalse=false;
}
state.isBeingEvaluated=true;
try {
changed=this.evaluateImmediate_CallReadWithDependencyDetection(notifyChange);
} finally {
state.isBeingEvaluated=false;
}
if(!state.dependenciesCount){
computedObservable.dispose();
}
return changed;
},
evaluateImmediate_CallReadWithDependencyDetection: function (notifyChange){
var computedObservable=this,
state=computedObservable[computedState],
changed=false;
var isInitial=state.pure ? undefined:!state.dependenciesCount,
dependencyDetectionContext={
computedObservable: computedObservable,
disposalCandidates: state.dependencyTracking,
disposalCount: state.dependenciesCount
};
ko.dependencyDetection.begin({
callbackTarget: dependencyDetectionContext,
callback: computedBeginDependencyDetectionCallback,
computed: computedObservable,
isInitial: isInitial
});
state.dependencyTracking={};
state.dependenciesCount=0;
var newValue=this.evaluateImmediate_CallReadThenEndDependencyDetection(state, dependencyDetectionContext);
if(computedObservable.isDifferent(state.latestValue, newValue)){
if(!state.isSleeping){
computedObservable["notifySubscribers"](state.latestValue, "beforeChange");
}
state.latestValue=newValue;
if(DEBUG) computedObservable._latestValue=newValue;
if(state.isSleeping){
computedObservable.updateVersion();
}else if(notifyChange){
computedObservable["notifySubscribers"](state.latestValue);
}
changed=true;
}
if(isInitial){
computedObservable["notifySubscribers"](state.latestValue, "awake");
}
return changed;
},
evaluateImmediate_CallReadThenEndDependencyDetection: function (state, dependencyDetectionContext){
try {
var readFunction=state.readFunction;
return state.evaluatorFunctionTarget ? readFunction.call(state.evaluatorFunctionTarget):readFunction();
} finally {
ko.dependencyDetection.end();
if(dependencyDetectionContext.disposalCount&&!state.isSleeping){
ko.utils.objectForEach(dependencyDetectionContext.disposalCandidates, computedDisposeDependencyCallback);
}
state.isStale=state.isDirty=false;
}},
peek: function (evaluate){
var state=this[computedState];
if((state.isDirty&&(evaluate||!state.dependenciesCount))||(state.isSleeping&&this.haveDependenciesChanged())){
this.evaluateImmediate();
}
return state.latestValue;
},
limit: function (limitFunction){
ko.subscribable['fn'].limit.call(this, limitFunction);
this._evalIfChanged=function (){
if(this[computedState].isStale){
this.evaluateImmediate();
}else{
this[computedState].isDirty=false;
}
return this[computedState].latestValue;
};
this._evalDelayed=function (isChange){
this._limitBeforeChange(this[computedState].latestValue);
this[computedState].isDirty=true;
if(isChange){
this[computedState].isStale=true;
}
this._limitChange(this);
};},
dispose: function (){
var state=this[computedState];
if(!state.isSleeping&&state.dependencyTracking){
ko.utils.objectForEach(state.dependencyTracking, function (id, dependency){
if(dependency.dispose)
dependency.dispose();
});
}
if(state.disposeWhenNodeIsRemoved&&state.domNodeDisposalCallback){
ko.utils.domNodeDisposal.removeDisposeCallback(state.disposeWhenNodeIsRemoved, state.domNodeDisposalCallback);
}
state.dependencyTracking=null;
state.dependenciesCount=0;
state.isDisposed=true;
state.isStale=false;
state.isDirty=false;
state.isSleeping=false;
state.disposeWhenNodeIsRemoved=null;
}};
var pureComputedOverrides={
beforeSubscriptionAdd: function (event){
var computedObservable=this,
state=computedObservable[computedState];
if(!state.isDisposed&&state.isSleeping&&event=='change'){
state.isSleeping=false;
if(state.isStale||computedObservable.haveDependenciesChanged()){
state.dependencyTracking=null;
state.dependenciesCount=0;
if(computedObservable.evaluateImmediate()){
computedObservable.updateVersion();
}}else{
var dependeciesOrder=[];
ko.utils.objectForEach(state.dependencyTracking, function (id, dependency){
dependeciesOrder[dependency._order]=id;
});
ko.utils.arrayForEach(dependeciesOrder, function (id, order){
var dependency=state.dependencyTracking[id],
subscription=computedObservable.subscribeToDependency(dependency._target);
subscription._order=order;
subscription._version=dependency._version;
state.dependencyTracking[id]=subscription;
});
}
if(!state.isDisposed){
computedObservable["notifySubscribers"](state.latestValue, "awake");
}}
},
afterSubscriptionRemove: function (event){
var state=this[computedState];
if(!state.isDisposed&&event=='change'&&!this.hasSubscriptionsForEvent('change')){
ko.utils.objectForEach(state.dependencyTracking, function (id, dependency){
if(dependency.dispose){
state.dependencyTracking[id]={
_target: dependency._target,
_order: dependency._order,
_version: dependency._version
};
dependency.dispose();
}});
state.isSleeping=true;
this["notifySubscribers"](undefined, "asleep");
}},
getVersion: function (){
var state=this[computedState];
if(state.isSleeping&&(state.isStale||this.haveDependenciesChanged())){
this.evaluateImmediate();
}
return ko.subscribable['fn'].getVersion.call(this);
}};
var deferEvaluationOverrides={
beforeSubscriptionAdd: function (event){
if(event=='change'||event=='beforeChange'){
this.peek();
}}
};
if(ko.utils.canSetPrototype){
ko.utils.setPrototypeOf(computedFn, ko.subscribable['fn']);
}
var protoProp=ko.observable.protoProperty;
ko.computed[protoProp]=ko.observable;
computedFn[protoProp]=ko.computed;
ko.isComputed=function (instance){
return ko.hasPrototype(instance, ko.computed);
};
ko.isPureComputed=function (instance){
return ko.hasPrototype(instance, ko.computed)
&& instance[computedState]&&instance[computedState].pure;
};
ko.exportSymbol('computed', ko.computed);
ko.exportSymbol('dependentObservable', ko.computed);
ko.exportSymbol('isComputed', ko.isComputed);
ko.exportSymbol('isPureComputed', ko.isPureComputed);
ko.exportSymbol('computed.fn', computedFn);
ko.exportProperty(computedFn, 'peek', computedFn.peek);
ko.exportProperty(computedFn, 'dispose', computedFn.dispose);
ko.exportProperty(computedFn, 'isActive', computedFn.isActive);
ko.exportProperty(computedFn, 'getDependenciesCount', computedFn.getDependenciesCount);
ko.pureComputed=function (evaluatorFunctionOrOptions, evaluatorFunctionTarget){
if(typeof evaluatorFunctionOrOptions==='function'){
return ko.computed(evaluatorFunctionOrOptions, evaluatorFunctionTarget, {'pure':true});
}else{
evaluatorFunctionOrOptions=ko.utils.extend({}, evaluatorFunctionOrOptions);
evaluatorFunctionOrOptions['pure']=true;
return ko.computed(evaluatorFunctionOrOptions, evaluatorFunctionTarget);
}}
ko.exportSymbol('pureComputed', ko.pureComputed);
(function(){
var maxNestedObservableDepth=10;
ko.toJS=function(rootObject){
if(arguments.length==0)
throw new Error("When calling ko.toJS, pass the object you want to convert.");
return mapJsObjectGraph(rootObject, function(valueToMap){
for (var i=0; ko.isObservable(valueToMap)&&(i < maxNestedObservableDepth); i++)
valueToMap=valueToMap();
return valueToMap;
});
};
ko.toJSON=function(rootObject, replacer, space){
var plainJavaScriptObject=ko.toJS(rootObject);
return ko.utils.stringifyJson(plainJavaScriptObject, replacer, space);
};
function mapJsObjectGraph(rootObject, mapInputCallback, visitedObjects){
visitedObjects=visitedObjects||new objectLookup();
rootObject=mapInputCallback(rootObject);
var canHaveProperties=(typeof rootObject=="object")&&(rootObject!==null)&&(rootObject!==undefined)&&(!(rootObject instanceof RegExp))&&(!(rootObject instanceof Date))&&(!(rootObject instanceof String))&&(!(rootObject instanceof Number))&&(!(rootObject instanceof Boolean));
if(!canHaveProperties)
return rootObject;
var outputProperties=rootObject instanceof Array ? []:{};
visitedObjects.save(rootObject, outputProperties);
visitPropertiesOrArrayEntries(rootObject, function(indexer){
var propertyValue=mapInputCallback(rootObject[indexer]);
switch (typeof propertyValue){
case "boolean":
case "number":
case "string":
case "function":
outputProperties[indexer]=propertyValue;
break;
case "object":
case "undefined":
var previouslyMappedValue=visitedObjects.get(propertyValue);
outputProperties[indexer]=(previouslyMappedValue!==undefined)
? previouslyMappedValue
: mapJsObjectGraph(propertyValue, mapInputCallback, visitedObjects);
break;
}});
return outputProperties;
}
function visitPropertiesOrArrayEntries(rootObject, visitorCallback){
if(rootObject instanceof Array){
for (var i=0; i < rootObject.length; i++)
visitorCallback(i);
if(typeof rootObject['toJSON']=='function')
visitorCallback('toJSON');
}else{
for (var propertyName in rootObject){
visitorCallback(propertyName);
}}
};
function objectLookup(){
this.keys=[];
this.values=[];
};
objectLookup.prototype={
constructor: objectLookup,
save: function(key, value){
var existingIndex=ko.utils.arrayIndexOf(this.keys, key);
if(existingIndex >=0)
this.values[existingIndex]=value;
else {
this.keys.push(key);
this.values.push(value);
}},
get: function(key){
var existingIndex=ko.utils.arrayIndexOf(this.keys, key);
return (existingIndex >=0) ? this.values[existingIndex]:undefined;
}};})();
ko.exportSymbol('toJS', ko.toJS);
ko.exportSymbol('toJSON', ko.toJSON);
(function (){
var hasDomDataExpandoProperty='__ko__hasDomDataOptionValue__';
ko.selectExtensions={
readValue:function(element){
switch (ko.utils.tagNameLower(element)){
case 'option':
if(element[hasDomDataExpandoProperty]===true)
return ko.utils.domData.get(element, ko.bindingHandlers.options.optionValueDomDataKey);
return ko.utils.ieVersion <=7
? (element.getAttributeNode('value')&&element.getAttributeNode('value').specified ? element.value:element.text)
: element.value;
case 'select':
return element.selectedIndex >=0 ? ko.selectExtensions.readValue(element.options[element.selectedIndex]):undefined;
default:
return element.value;
}},
writeValue: function(element, value, allowUnset){
switch (ko.utils.tagNameLower(element)){
case 'option':
switch(typeof value){
case "string":
ko.utils.domData.set(element, ko.bindingHandlers.options.optionValueDomDataKey, undefined);
if(hasDomDataExpandoProperty in element){
delete element[hasDomDataExpandoProperty];
}
element.value=value;
break;
default:
ko.utils.domData.set(element, ko.bindingHandlers.options.optionValueDomDataKey, value);
element[hasDomDataExpandoProperty]=true;
element.value=typeof value==="number" ? value:"";
break;
}
break;
case 'select':
if(value===""||value===null)
value=undefined;
var selection=-1;
for (var i=0, n=element.options.length, optionValue; i < n; ++i){
optionValue=ko.selectExtensions.readValue(element.options[i]);
if(optionValue==value||(optionValue==""&&value===undefined)){
selection=i;
break;
}}
if(allowUnset||selection >=0||(value===undefined&&element.size > 1)){
element.selectedIndex=selection;
}
break;
default:
if((value===null)||(value===undefined))
value="";
element.value=value;
break;
}}
};})();
ko.exportSymbol('selectExtensions', ko.selectExtensions);
ko.exportSymbol('selectExtensions.readValue', ko.selectExtensions.readValue);
ko.exportSymbol('selectExtensions.writeValue', ko.selectExtensions.writeValue);
ko.expressionRewriting=(function (){
var javaScriptReservedWords=["true", "false", "null", "undefined"];
var javaScriptAssignmentTarget=/^(?:[$_a-z][$\w]*|(.+)(\.\s*[$_a-z][$\w]*|\[.+\]))$/i;
function getWriteableValue(expression){
if(ko.utils.arrayIndexOf(javaScriptReservedWords, expression) >=0)
return false;
var match=expression.match(javaScriptAssignmentTarget);
return match===null ? false:match[1] ? ('Object(' + match[1] + ')' + match[2]):expression;
}
var stringDouble='"(?:[^"\\\\]|\\\\.)*"',
stringSingle="'(?:[^'\\\\]|\\\\.)*'",
stringRegexp='/(?:[^/\\\\]|\\\\.)*/\w*',
specials=',"\'{}()/:[\\]',
everyThingElse='[^\\s:,/][^' + specials + ']*[^\\s' + specials + ']',
oneNotSpace='[^\\s]',
bindingToken=RegExp(stringDouble + '|' + stringSingle + '|' + stringRegexp + '|' + everyThingElse + '|' + oneNotSpace, 'g'),
divisionLookBehind=/[\])"'A-Za-z0-9_$]+$/,
keywordRegexLookBehind={'in':1,'return':1,'typeof':1};
function parseObjectLiteral(objectLiteralString){
var str=ko.utils.stringTrim(objectLiteralString);
if(str.charCodeAt(0)===123) str=str.slice(1, -1);
var result=[], toks=str.match(bindingToken), key, values=[], depth=0;
if(toks){
toks.push(',');
for (var i=0, tok; tok=toks[i]; ++i){
var c=tok.charCodeAt(0);
if(c===44){ // ","
if(depth <=0){
result.push((key&&values.length) ? {key: key, value: values.join('')}:{'unknown': key||values.join('')});
key=depth=0;
values=[];
continue;
}}else if(c===58){ // ":"
if(!depth&&!key&&values.length===1){
key=values.pop();
continue;
}}else if(c===47&&i && tok.length > 1){  // "/"
var match=toks[i-1].match(divisionLookBehind);
if(match&&!keywordRegexLookBehind[match[0]]){
str=str.substr(str.indexOf(tok) + 1);
toks=str.match(bindingToken);
toks.push(',');
i=-1;
tok='/';
}}else if(c===40||c===123||c===91){ // '(', '{', '['
++depth;
}else if(c===41||c===125||c===93){ // ')', '}', ']'
--depth;
}else if(!key&&!values.length&&(c===34||c===39)){ // '"', "'"
tok=tok.slice(1, -1);
}
values.push(tok);
}}
return result;
}
var twoWayBindings={};
function preProcessBindings(bindingsStringOrKeyValueArray, bindingOptions){
bindingOptions=bindingOptions||{};
function processKeyValue(key, val){
var writableVal;
function callPreprocessHook(obj){
return (obj&&obj['preprocess']) ? (val=obj['preprocess'](val, key, processKeyValue)):true;
}
if(!bindingParams){
if(!callPreprocessHook(ko['getBindingHandler'](key)))
return;
if(twoWayBindings[key]&&(writableVal=getWriteableValue(val))){
propertyAccessorResultStrings.push("'" + key + "':function(_z){" + writableVal + "=_z}");
}}
if(makeValueAccessors){
val='function(){return ' + val + ' }';
}
resultStrings.push("'" + key + "':" + val);
}
var resultStrings=[],
propertyAccessorResultStrings=[],
makeValueAccessors=bindingOptions['valueAccessors'],
bindingParams=bindingOptions['bindingParams'],
keyValueArray=typeof bindingsStringOrKeyValueArray==="string" ?
parseObjectLiteral(bindingsStringOrKeyValueArray):bindingsStringOrKeyValueArray;
ko.utils.arrayForEach(keyValueArray, function(keyValue){
processKeyValue(keyValue.key||keyValue['unknown'], keyValue.value);
});
if(propertyAccessorResultStrings.length)
processKeyValue('_ko_property_writers', "{" + propertyAccessorResultStrings.join(",") + " }");
return resultStrings.join(",");
}
return {
bindingRewriteValidators: [],
twoWayBindings: twoWayBindings,
parseObjectLiteral: parseObjectLiteral,
preProcessBindings: preProcessBindings,
keyValueArrayContainsKey: function(keyValueArray, key){
for (var i=0; i < keyValueArray.length; i++)
if(keyValueArray[i]['key']==key)
return true;
return false;
},
writeValueToProperty: function(property, allBindings, key, value, checkIfDifferent){
if(!property||!ko.isObservable(property)){
var propWriters=allBindings.get('_ko_property_writers');
if(propWriters&&propWriters[key])
propWriters[key](value);
}else if(ko.isWriteableObservable(property)&&(!checkIfDifferent||property.peek()!==value)){
property(value);
}}
};})();
ko.exportSymbol('expressionRewriting', ko.expressionRewriting);
ko.exportSymbol('expressionRewriting.bindingRewriteValidators', ko.expressionRewriting.bindingRewriteValidators);
ko.exportSymbol('expressionRewriting.parseObjectLiteral', ko.expressionRewriting.parseObjectLiteral);
ko.exportSymbol('expressionRewriting.preProcessBindings', ko.expressionRewriting.preProcessBindings);
ko.exportSymbol('expressionRewriting._twoWayBindings', ko.expressionRewriting.twoWayBindings);
ko.exportSymbol('jsonExpressionRewriting', ko.expressionRewriting);
ko.exportSymbol('jsonExpressionRewriting.insertPropertyAccessorsIntoJson', ko.expressionRewriting.preProcessBindings);
(function(){
var commentNodesHaveTextProperty=document&&document.createComment("test").text==="<!--test-->";
var startCommentRegex=commentNodesHaveTextProperty ? /^<!--\s*ko(?:\s+([\s\S]+))?\s*-->$/:/^\s*ko(?:\s+([\s\S]+))?\s*$/;
var endCommentRegex=commentNodesHaveTextProperty ? /^<!--\s*\/ko\s*-->$/:/^\s*\/ko\s*$/;
var htmlTagsWithOptionallyClosingChildren={ 'ul': true, 'ol': true };
function isStartComment(node){
return (node.nodeType==8)&&startCommentRegex.test(commentNodesHaveTextProperty ? node.text:node.nodeValue);
}
function isEndComment(node){
return (node.nodeType==8)&&endCommentRegex.test(commentNodesHaveTextProperty ? node.text:node.nodeValue);
}
function getVirtualChildren(startComment, allowUnbalanced){
var currentNode=startComment;
var depth=1;
var children=[];
while (currentNode=currentNode.nextSibling){
if(isEndComment(currentNode)){
depth--;
if(depth===0)
return children;
}
children.push(currentNode);
if(isStartComment(currentNode))
depth++;
}
if(!allowUnbalanced)
throw new Error("Cannot find closing comment tag to match: " + startComment.nodeValue);
return null;
}
function getMatchingEndComment(startComment, allowUnbalanced){
var allVirtualChildren=getVirtualChildren(startComment, allowUnbalanced);
if(allVirtualChildren){
if(allVirtualChildren.length > 0)
return allVirtualChildren[allVirtualChildren.length - 1].nextSibling;
return startComment.nextSibling;
} else
return null;
}
function getUnbalancedChildTags(node){
var childNode=node.firstChild, captureRemaining=null;
if(childNode){
do {
if(captureRemaining)
captureRemaining.push(childNode);
else if(isStartComment(childNode)){
var matchingEndComment=getMatchingEndComment(childNode,  true);
if(matchingEndComment)
childNode=matchingEndComment;
else
captureRemaining=[childNode];
}else if(isEndComment(childNode)){
captureRemaining=[childNode];
}} while (childNode=childNode.nextSibling);
}
return captureRemaining;
}
ko.virtualElements={
allowedBindings: {},
childNodes: function(node){
return isStartComment(node) ? getVirtualChildren(node):node.childNodes;
},
emptyNode: function(node){
if(!isStartComment(node))
ko.utils.emptyDomNode(node);
else {
var virtualChildren=ko.virtualElements.childNodes(node);
for (var i=0, j=virtualChildren.length; i < j; i++)
ko.removeNode(virtualChildren[i]);
}},
setDomNodeChildren: function(node, childNodes){
if(!isStartComment(node))
ko.utils.setDomNodeChildren(node, childNodes);
else {
ko.virtualElements.emptyNode(node);
var endCommentNode=node.nextSibling;
for (var i=0, j=childNodes.length; i < j; i++)
endCommentNode.parentNode.insertBefore(childNodes[i], endCommentNode);
}},
prepend: function(containerNode, nodeToPrepend){
if(!isStartComment(containerNode)){
if(containerNode.firstChild)
containerNode.insertBefore(nodeToPrepend, containerNode.firstChild);
else
containerNode.appendChild(nodeToPrepend);
}else{
containerNode.parentNode.insertBefore(nodeToPrepend, containerNode.nextSibling);
}},
insertAfter: function(containerNode, nodeToInsert, insertAfterNode){
if(!insertAfterNode){
ko.virtualElements.prepend(containerNode, nodeToInsert);
}else if(!isStartComment(containerNode)){
if(insertAfterNode.nextSibling)
containerNode.insertBefore(nodeToInsert, insertAfterNode.nextSibling);
else
containerNode.appendChild(nodeToInsert);
}else{
containerNode.parentNode.insertBefore(nodeToInsert, insertAfterNode.nextSibling);
}},
firstChild: function(node){
if(!isStartComment(node))
return node.firstChild;
if(!node.nextSibling||isEndComment(node.nextSibling))
return null;
return node.nextSibling;
},
nextSibling: function(node){
if(isStartComment(node))
node=getMatchingEndComment(node);
if(node.nextSibling&&isEndComment(node.nextSibling))
return null;
return node.nextSibling;
},
hasBindingValue: isStartComment,
virtualNodeBindingValue: function(node){
var regexMatch=(commentNodesHaveTextProperty ? node.text:node.nodeValue).match(startCommentRegex);
return regexMatch ? regexMatch[1]:null;
},
normaliseVirtualElementDomStructure: function(elementVerified){
if(!htmlTagsWithOptionallyClosingChildren[ko.utils.tagNameLower(elementVerified)])
return;
var childNode=elementVerified.firstChild;
if(childNode){
do {
if(childNode.nodeType===1){
var unbalancedTags=getUnbalancedChildTags(childNode);
if(unbalancedTags){
var nodeToInsertBefore=childNode.nextSibling;
for (var i=0; i < unbalancedTags.length; i++){
if(nodeToInsertBefore)
elementVerified.insertBefore(unbalancedTags[i], nodeToInsertBefore);
else
elementVerified.appendChild(unbalancedTags[i]);
}}
}} while (childNode=childNode.nextSibling);
}}
};})();
ko.exportSymbol('virtualElements', ko.virtualElements);
ko.exportSymbol('virtualElements.allowedBindings', ko.virtualElements.allowedBindings);
ko.exportSymbol('virtualElements.emptyNode', ko.virtualElements.emptyNode);
ko.exportSymbol('virtualElements.insertAfter', ko.virtualElements.insertAfter);
ko.exportSymbol('virtualElements.prepend', ko.virtualElements.prepend);
ko.exportSymbol('virtualElements.setDomNodeChildren', ko.virtualElements.setDomNodeChildren);
(function(){
var defaultBindingAttributeName="data-bind";
ko.bindingProvider=function(){
this.bindingCache={};};
ko.utils.extend(ko.bindingProvider.prototype, {
'nodeHasBindings': function(node){
switch (node.nodeType){
case 1:
return node.getAttribute(defaultBindingAttributeName)!=null
|| ko.components['getComponentNameForNode'](node);
case 8:
return ko.virtualElements.hasBindingValue(node);
default: return false;
}},
'getBindings': function(node, bindingContext){
var bindingsString=this['getBindingsString'](node, bindingContext),
parsedBindings=bindingsString ? this['parseBindingsString'](bindingsString, bindingContext, node):null;
return ko.components.addBindingsForCustomElement(parsedBindings, node, bindingContext,  false);
},
'getBindingAccessors': function(node, bindingContext){
var bindingsString=this['getBindingsString'](node, bindingContext),
parsedBindings=bindingsString ? this['parseBindingsString'](bindingsString, bindingContext, node, { 'valueAccessors': true }):null;
return ko.components.addBindingsForCustomElement(parsedBindings, node, bindingContext,  true);
},
'getBindingsString': function(node, bindingContext){
switch (node.nodeType){
case 1: return node.getAttribute(defaultBindingAttributeName);
case 8: return ko.virtualElements.virtualNodeBindingValue(node);
default: return null;
}},
'parseBindingsString': function(bindingsString, bindingContext, node, options){
try {
var bindingFunction=createBindingsStringEvaluatorViaCache(bindingsString, this.bindingCache, options);
return bindingFunction(bindingContext, node);
} catch (ex){
ex.message="Unable to parse bindings.\nBindings value: " + bindingsString + "\nMessage: " + ex.message;
throw ex;
}}
});
ko.bindingProvider['instance']=new ko.bindingProvider();
function createBindingsStringEvaluatorViaCache(bindingsString, cache, options){
var cacheKey=bindingsString + (options&&options['valueAccessors']||'');
return cache[cacheKey]
|| (cache[cacheKey]=createBindingsStringEvaluator(bindingsString, options));
}
function createBindingsStringEvaluator(bindingsString, options){
var rewrittenBindings=ko.expressionRewriting.preProcessBindings(bindingsString, options),
functionBody="with($context){with($data||{}){return{" + rewrittenBindings + "}}}";
return new Function("$context", "$element", functionBody);
}})();
ko.exportSymbol('bindingProvider', ko.bindingProvider);
(function (){
ko.bindingHandlers={};
var bindingDoesNotRecurseIntoElementTypes={
'script': true,
'textarea': true,
'template': true
};
ko['getBindingHandler']=function(bindingKey){
return ko.bindingHandlers[bindingKey];
};
ko.bindingContext=function(dataItemOrAccessor, parentContext, dataItemAlias, extendCallback, options){
function updateContext(){
var dataItemOrObservable=isFunc ? dataItemOrAccessor():dataItemOrAccessor,
dataItem=ko.utils.unwrapObservable(dataItemOrObservable);
if(parentContext){
if(parentContext._subscribable)
parentContext._subscribable();
ko.utils.extend(self, parentContext);
self._subscribable=subscribable;
}else{
self['$parents']=[];
self['$root']=dataItem;
self['ko']=ko;
}
self['$rawData']=dataItemOrObservable;
self['$data']=dataItem;
if(dataItemAlias)
self[dataItemAlias]=dataItem;
if(extendCallback)
extendCallback(self, parentContext, dataItem);
return self['$data'];
}
function disposeWhen(){
return nodes&&!ko.utils.anyDomNodeIsAttachedToDocument(nodes);
}
var self=this,
isFunc=typeof(dataItemOrAccessor)=="function"&&!ko.isObservable(dataItemOrAccessor),
nodes,
subscribable;
if(options&&options['exportDependencies']){
updateContext();
}else{
subscribable=ko.dependentObservable(updateContext, null, { disposeWhen: disposeWhen, disposeWhenNodeIsRemoved: true });
if(subscribable.isActive()){
self._subscribable=subscribable;
subscribable['equalityComparer']=null;
nodes=[];
subscribable._addNode=function(node){
nodes.push(node);
ko.utils.domNodeDisposal.addDisposeCallback(node, function(node){
ko.utils.arrayRemoveItem(nodes, node);
if(!nodes.length){
subscribable.dispose();
self._subscribable=subscribable=undefined;
}});
};}}
}
ko.bindingContext.prototype['createChildContext']=function (dataItemOrAccessor, dataItemAlias, extendCallback, options){
return new ko.bindingContext(dataItemOrAccessor, this, dataItemAlias, function(self, parentContext){
self['$parentContext']=parentContext;
self['$parent']=parentContext['$data'];
self['$parents']=(parentContext['$parents']||[]).slice(0);
self['$parents'].unshift(self['$parent']);
if(extendCallback)
extendCallback(self);
}, options);
};
ko.bindingContext.prototype['extend']=function(properties){
return new ko.bindingContext(this._subscribable||this['$data'], this, null, function(self, parentContext){
self['$rawData']=parentContext['$rawData'];
ko.utils.extend(self, typeof(properties)=="function" ? properties():properties);
});
};
ko.bindingContext.prototype.createStaticChildContext=function (dataItemOrAccessor, dataItemAlias){
return this['createChildContext'](dataItemOrAccessor, dataItemAlias, null, { "exportDependencies": true });
};
function makeValueAccessor(value){
return function(){
return value;
};}
function evaluateValueAccessor(valueAccessor){
return valueAccessor();
}
function makeAccessorsFromFunction(callback){
return ko.utils.objectMap(ko.dependencyDetection.ignore(callback), function(value, key){
return function(){
return callback()[key];
};});
}
function makeBindingAccessors(bindings, context, node){
if(typeof bindings==='function'){
return makeAccessorsFromFunction(bindings.bind(null, context, node));
}else{
return ko.utils.objectMap(bindings, makeValueAccessor);
}}
function getBindingsAndMakeAccessors(node, context){
return makeAccessorsFromFunction(this['getBindings'].bind(this, node, context));
}
function validateThatBindingIsAllowedForVirtualElements(bindingName){
var validator=ko.virtualElements.allowedBindings[bindingName];
if(!validator)
throw new Error("The binding '" + bindingName + "' cannot be used with virtual elements")
}
function applyBindingsToDescendantsInternal (bindingContext, elementOrVirtualElement, bindingContextsMayDifferFromDomParentElement){
var currentChild,
nextInQueue=ko.virtualElements.firstChild(elementOrVirtualElement),
provider=ko.bindingProvider['instance'],
preprocessNode=provider['preprocessNode'];
if(preprocessNode){
while (currentChild=nextInQueue){
nextInQueue=ko.virtualElements.nextSibling(currentChild);
preprocessNode.call(provider, currentChild);
}
nextInQueue=ko.virtualElements.firstChild(elementOrVirtualElement);
}
while (currentChild=nextInQueue){
nextInQueue=ko.virtualElements.nextSibling(currentChild);
applyBindingsToNodeAndDescendantsInternal(bindingContext, currentChild, bindingContextsMayDifferFromDomParentElement);
}}
function applyBindingsToNodeAndDescendantsInternal (bindingContext, nodeVerified, bindingContextMayDifferFromDomParentElement){
var shouldBindDescendants=true;
var isElement=(nodeVerified.nodeType===1);
if(isElement)
ko.virtualElements.normaliseVirtualElementDomStructure(nodeVerified);
var shouldApplyBindings=(isElement&&bindingContextMayDifferFromDomParentElement)
|| ko.bindingProvider['instance']['nodeHasBindings'](nodeVerified);
if(shouldApplyBindings)
shouldBindDescendants=applyBindingsToNodeInternal(nodeVerified, null, bindingContext, bindingContextMayDifferFromDomParentElement)['shouldBindDescendants'];
if(shouldBindDescendants&&!bindingDoesNotRecurseIntoElementTypes[ko.utils.tagNameLower(nodeVerified)]){
applyBindingsToDescendantsInternal(bindingContext, nodeVerified,  !isElement);
}}
var boundElementDomDataKey=ko.utils.domData.nextKey();
function topologicalSortBindings(bindings){
var result=[],
bindingsConsidered={},
cyclicDependencyStack=[];
ko.utils.objectForEach(bindings, function pushBinding(bindingKey){
if(!bindingsConsidered[bindingKey]){
var binding=ko['getBindingHandler'](bindingKey);
if(binding){
if(binding['after']){
cyclicDependencyStack.push(bindingKey);
ko.utils.arrayForEach(binding['after'], function(bindingDependencyKey){
if(bindings[bindingDependencyKey]){
if(ko.utils.arrayIndexOf(cyclicDependencyStack, bindingDependencyKey)!==-1){
throw Error("Cannot combine the following bindings, because they have a cyclic dependency: " + cyclicDependencyStack.join(", "));
}else{
pushBinding(bindingDependencyKey);
}}
});
cyclicDependencyStack.length--;
}
result.push({ key: bindingKey, handler: binding });
}
bindingsConsidered[bindingKey]=true;
}});
return result;
}
function applyBindingsToNodeInternal(node, sourceBindings, bindingContext, bindingContextMayDifferFromDomParentElement){
var alreadyBound=ko.utils.domData.get(node, boundElementDomDataKey);
if(!sourceBindings){
if(alreadyBound){
throw Error("You cannot apply bindings multiple times to the same element.");
}
ko.utils.domData.set(node, boundElementDomDataKey, true);
}
if(!alreadyBound&&bindingContextMayDifferFromDomParentElement)
ko.storedBindingContextForNode(node, bindingContext);
var bindings;
if(sourceBindings&&typeof sourceBindings!=='function'){
bindings=sourceBindings;
}else{
var provider=ko.bindingProvider['instance'],
getBindings=provider['getBindingAccessors']||getBindingsAndMakeAccessors;
var bindingsUpdater=ko.dependentObservable(function(){
bindings=sourceBindings ? sourceBindings(bindingContext, node):getBindings.call(provider, node, bindingContext);
if(bindings&&bindingContext._subscribable)
bindingContext._subscribable();
return bindings;
},
null, { disposeWhenNodeIsRemoved: node }
);
if(!bindings||!bindingsUpdater.isActive())
bindingsUpdater=null;
}
var bindingHandlerThatControlsDescendantBindings;
if(bindings){
var getValueAccessor=bindingsUpdater
? function(bindingKey){
return function(){
return evaluateValueAccessor(bindingsUpdater()[bindingKey]);
};}:function(bindingKey){
return bindings[bindingKey];
};
function allBindings(){
return ko.utils.objectMap(bindingsUpdater ? bindingsUpdater():bindings, evaluateValueAccessor);
}
allBindings['get']=function(key){
return bindings[key]&&evaluateValueAccessor(getValueAccessor(key));
};
allBindings['has']=function(key){
return key in bindings;
};
var orderedBindings=topologicalSortBindings(bindings);
ko.utils.arrayForEach(orderedBindings, function(bindingKeyAndHandler){
var handlerInitFn=bindingKeyAndHandler.handler["init"],
handlerUpdateFn=bindingKeyAndHandler.handler["update"],
bindingKey=bindingKeyAndHandler.key;
if(node.nodeType===8){
validateThatBindingIsAllowedForVirtualElements(bindingKey);
}
try {
if(typeof handlerInitFn=="function"){
ko.dependencyDetection.ignore(function(){
var initResult=handlerInitFn(node, getValueAccessor(bindingKey), allBindings, bindingContext['$data'], bindingContext);
if(initResult&&initResult['controlsDescendantBindings']){
if(bindingHandlerThatControlsDescendantBindings!==undefined)
throw new Error("Multiple bindings (" + bindingHandlerThatControlsDescendantBindings + " and " + bindingKey + ") are trying to control descendant bindings of the same element. You cannot use these bindings together on the same element.");
bindingHandlerThatControlsDescendantBindings=bindingKey;
}});
}
if(typeof handlerUpdateFn=="function"){
ko.dependentObservable(function(){
handlerUpdateFn(node, getValueAccessor(bindingKey), allBindings, bindingContext['$data'], bindingContext);
},
null,
{ disposeWhenNodeIsRemoved: node }
);
}} catch (ex){
ex.message="Unable to process binding \"" + bindingKey + ": " + bindings[bindingKey] + "\"\nMessage: " + ex.message;
throw ex;
}});
}
return {
'shouldBindDescendants': bindingHandlerThatControlsDescendantBindings===undefined
};};
var storedBindingContextDomDataKey=ko.utils.domData.nextKey();
ko.storedBindingContextForNode=function (node, bindingContext){
if(arguments.length==2){
ko.utils.domData.set(node, storedBindingContextDomDataKey, bindingContext);
if(bindingContext._subscribable)
bindingContext._subscribable._addNode(node);
}else{
return ko.utils.domData.get(node, storedBindingContextDomDataKey);
}}
function getBindingContext(viewModelOrBindingContext){
return viewModelOrBindingContext&&(viewModelOrBindingContext instanceof ko.bindingContext)
? viewModelOrBindingContext
: new ko.bindingContext(viewModelOrBindingContext);
}
ko.applyBindingAccessorsToNode=function (node, bindings, viewModelOrBindingContext){
if(node.nodeType===1)
ko.virtualElements.normaliseVirtualElementDomStructure(node);
return applyBindingsToNodeInternal(node, bindings, getBindingContext(viewModelOrBindingContext), true);
};
ko.applyBindingsToNode=function (node, bindings, viewModelOrBindingContext){
var context=getBindingContext(viewModelOrBindingContext);
return ko.applyBindingAccessorsToNode(node, makeBindingAccessors(bindings, context, node), context);
};
ko.applyBindingsToDescendants=function(viewModelOrBindingContext, rootNode){
if(rootNode.nodeType===1||rootNode.nodeType===8)
applyBindingsToDescendantsInternal(getBindingContext(viewModelOrBindingContext), rootNode, true);
};
ko.applyBindings=function (viewModelOrBindingContext, rootNode){
if(!jQueryInstance&&window['jQuery']){
jQueryInstance=window['jQuery'];
}
if(rootNode&&(rootNode.nodeType!==1)&&(rootNode.nodeType!==8))
throw new Error("ko.applyBindings: first parameter should be your view model; second parameter should be a DOM node");
rootNode=rootNode||window.document.body;
applyBindingsToNodeAndDescendantsInternal(getBindingContext(viewModelOrBindingContext), rootNode, true);
};
ko.contextFor=function(node){
switch (node.nodeType){
case 1:
case 8:
var context=ko.storedBindingContextForNode(node);
if(context) return context;
if(node.parentNode) return ko.contextFor(node.parentNode);
break;
}
return undefined;
};
ko.dataFor=function(node){
var context=ko.contextFor(node);
return context ? context['$data']:undefined;
};
ko.exportSymbol('bindingHandlers', ko.bindingHandlers);
ko.exportSymbol('applyBindings', ko.applyBindings);
ko.exportSymbol('applyBindingsToDescendants', ko.applyBindingsToDescendants);
ko.exportSymbol('applyBindingAccessorsToNode', ko.applyBindingAccessorsToNode);
ko.exportSymbol('applyBindingsToNode', ko.applyBindingsToNode);
ko.exportSymbol('contextFor', ko.contextFor);
ko.exportSymbol('dataFor', ko.dataFor);
})();
(function(undefined){
var loadingSubscribablesCache={},
loadedDefinitionsCache={};
ko.components={
get: function(componentName, callback){
var cachedDefinition=getObjectOwnProperty(loadedDefinitionsCache, componentName);
if(cachedDefinition){
if(cachedDefinition.isSynchronousComponent){
ko.dependencyDetection.ignore(function(){
callback(cachedDefinition.definition);
});
}else{
ko.tasks.schedule(function(){ callback(cachedDefinition.definition); });
}}else{
loadComponentAndNotify(componentName, callback);
}},
clearCachedDefinition: function(componentName){
delete loadedDefinitionsCache[componentName];
},
_getFirstResultFromLoaders: getFirstResultFromLoaders
};
function getObjectOwnProperty(obj, propName){
return obj.hasOwnProperty(propName) ? obj[propName]:undefined;
}
function loadComponentAndNotify(componentName, callback){
var subscribable=getObjectOwnProperty(loadingSubscribablesCache, componentName),
completedAsync;
if(!subscribable){
subscribable=loadingSubscribablesCache[componentName]=new ko.subscribable();
subscribable.subscribe(callback);
beginLoadingComponent(componentName, function(definition, config){
var isSynchronousComponent = !!(config&&config['synchronous']);
loadedDefinitionsCache[componentName]={ definition: definition, isSynchronousComponent: isSynchronousComponent };
delete loadingSubscribablesCache[componentName];
if(completedAsync||isSynchronousComponent){
subscribable['notifySubscribers'](definition);
}else{
ko.tasks.schedule(function(){
subscribable['notifySubscribers'](definition);
});
}});
completedAsync=true;
}else{
subscribable.subscribe(callback);
}}
function beginLoadingComponent(componentName, callback){
getFirstResultFromLoaders('getConfig', [componentName], function(config){
if(config){
getFirstResultFromLoaders('loadComponent', [componentName, config], function(definition){
callback(definition, config);
});
}else{
callback(null, null);
}});
}
function getFirstResultFromLoaders(methodName, argsExceptCallback, callback, candidateLoaders){
if(!candidateLoaders){
candidateLoaders=ko.components['loaders'].slice(0);
}
var currentCandidateLoader=candidateLoaders.shift();
if(currentCandidateLoader){
var methodInstance=currentCandidateLoader[methodName];
if(methodInstance){
var wasAborted=false,
synchronousReturnValue=methodInstance.apply(currentCandidateLoader, argsExceptCallback.concat(function(result){
if(wasAborted){
callback(null);
}else if(result!==null){
callback(result);
}else{
getFirstResultFromLoaders(methodName, argsExceptCallback, callback, candidateLoaders);
}}));
if(synchronousReturnValue!==undefined){
wasAborted=true;
if(!currentCandidateLoader['suppressLoaderExceptions']){
throw new Error('Component loaders must supply values by invoking the callback, not by returning values synchronously.');
}}
}else{
getFirstResultFromLoaders(methodName, argsExceptCallback, callback, candidateLoaders);
}}else{
callback(null);
}}
ko.components['loaders']=[];
ko.exportSymbol('components', ko.components);
ko.exportSymbol('components.get', ko.components.get);
ko.exportSymbol('components.clearCachedDefinition', ko.components.clearCachedDefinition);
})();
(function(undefined){
var defaultConfigRegistry={};
ko.components.register=function(componentName, config){
if(!config){
throw new Error('Invalid configuration for ' + componentName);
}
if(ko.components.isRegistered(componentName)){
throw new Error('Component ' + componentName + ' is already registered');
}
defaultConfigRegistry[componentName]=config;
};
ko.components.isRegistered=function(componentName){
return defaultConfigRegistry.hasOwnProperty(componentName);
};
ko.components.unregister=function(componentName){
delete defaultConfigRegistry[componentName];
ko.components.clearCachedDefinition(componentName);
};
ko.components.defaultLoader={
'getConfig': function(componentName, callback){
var result=defaultConfigRegistry.hasOwnProperty(componentName)
? defaultConfigRegistry[componentName]
: null;
callback(result);
},
'loadComponent': function(componentName, config, callback){
var errorCallback=makeErrorCallback(componentName);
possiblyGetConfigFromAmd(errorCallback, config, function(loadedConfig){
resolveConfig(componentName, errorCallback, loadedConfig, callback);
});
},
'loadTemplate': function(componentName, templateConfig, callback){
resolveTemplate(makeErrorCallback(componentName), templateConfig, callback);
},
'loadViewModel': function(componentName, viewModelConfig, callback){
resolveViewModel(makeErrorCallback(componentName), viewModelConfig, callback);
}};
var createViewModelKey='createViewModel';
function resolveConfig(componentName, errorCallback, config, callback){
var result={},
makeCallBackWhenZero=2,
tryIssueCallback=function(){
if(--makeCallBackWhenZero===0){
callback(result);
}},
templateConfig=config['template'],
viewModelConfig=config['viewModel'];
if(templateConfig){
possiblyGetConfigFromAmd(errorCallback, templateConfig, function(loadedConfig){
ko.components._getFirstResultFromLoaders('loadTemplate', [componentName, loadedConfig], function(resolvedTemplate){
result['template']=resolvedTemplate;
tryIssueCallback();
});
});
}else{
tryIssueCallback();
}
if(viewModelConfig){
possiblyGetConfigFromAmd(errorCallback, viewModelConfig, function(loadedConfig){
ko.components._getFirstResultFromLoaders('loadViewModel', [componentName, loadedConfig], function(resolvedViewModel){
result[createViewModelKey]=resolvedViewModel;
tryIssueCallback();
});
});
}else{
tryIssueCallback();
}}
function resolveTemplate(errorCallback, templateConfig, callback){
if(typeof templateConfig==='string'){
callback(ko.utils.parseHtmlFragment(templateConfig));
}else if(templateConfig instanceof Array){
callback(templateConfig);
}else if(isDocumentFragment(templateConfig)){
callback(ko.utils.makeArray(templateConfig.childNodes));
}else if(templateConfig['element']){
var element=templateConfig['element'];
if(isDomElement(element)){
callback(cloneNodesFromTemplateSourceElement(element));
}else if(typeof element==='string'){
var elemInstance=document.getElementById(element);
if(elemInstance){
callback(cloneNodesFromTemplateSourceElement(elemInstance));
}else{
errorCallback('Cannot find element with ID ' + element);
}}else{
errorCallback('Unknown element type: ' + element);
}}else{
errorCallback('Unknown template value: ' + templateConfig);
}}
function resolveViewModel(errorCallback, viewModelConfig, callback){
if(typeof viewModelConfig==='function'){
callback(function (params ){
return new viewModelConfig(params);
});
}else if(typeof viewModelConfig[createViewModelKey]==='function'){
callback(viewModelConfig[createViewModelKey]);
}else if('instance' in viewModelConfig){
var fixedInstance=viewModelConfig['instance'];
callback(function (params, componentInfo){
return fixedInstance;
});
}else if('viewModel' in viewModelConfig){
resolveViewModel(errorCallback, viewModelConfig['viewModel'], callback);
}else{
errorCallback('Unknown viewModel value: ' + viewModelConfig);
}}
function cloneNodesFromTemplateSourceElement(elemInstance){
switch (ko.utils.tagNameLower(elemInstance)){
case 'script':
return ko.utils.parseHtmlFragment(elemInstance.text);
case 'textarea':
return ko.utils.parseHtmlFragment(elemInstance.value);
case 'template':
if(isDocumentFragment(elemInstance.content)){
return ko.utils.cloneNodes(elemInstance.content.childNodes);
}}
return ko.utils.cloneNodes(elemInstance.childNodes);
}
function isDomElement(obj){
if(window['HTMLElement']){
return obj instanceof HTMLElement;
}else{
return obj&&obj.tagName&&obj.nodeType===1;
}}
function isDocumentFragment(obj){
if(window['DocumentFragment']){
return obj instanceof DocumentFragment;
}else{
return obj&&obj.nodeType===11;
}}
function possiblyGetConfigFromAmd(errorCallback, config, callback){
if(typeof config['require']==='string'){
if(amdRequire||window['require']){
(amdRequire||window['require'])([config['require']], callback);
}else{
errorCallback('Uses require, but no AMD loader is present');
}}else{
callback(config);
}}
function makeErrorCallback(componentName){
return function (message){
throw new Error('Component \'' + componentName + '\': ' + message);
};}
ko.exportSymbol('components.register', ko.components.register);
ko.exportSymbol('components.isRegistered', ko.components.isRegistered);
ko.exportSymbol('components.unregister', ko.components.unregister);
ko.exportSymbol('components.defaultLoader', ko.components.defaultLoader);
ko.components['loaders'].push(ko.components.defaultLoader);
ko.components._allRegisteredComponents=defaultConfigRegistry;
})();
(function (undefined){
ko.components['getComponentNameForNode']=function(node){
var tagNameLower=ko.utils.tagNameLower(node);
if(ko.components.isRegistered(tagNameLower)){
if(tagNameLower.indexOf('-')!=-1||('' + node)=="[object HTMLUnknownElement]"||(ko.utils.ieVersion <=8&&node.tagName===tagNameLower)){
return tagNameLower;
}}
};
ko.components.addBindingsForCustomElement=function(allBindings, node, bindingContext, valueAccessors){
if(node.nodeType===1){
var componentName=ko.components['getComponentNameForNode'](node);
if(componentName){
allBindings=allBindings||{};
if(allBindings['component']){
throw new Error('Cannot use the "component" binding on a custom element matching a component');
}
var componentBindingValue={ 'name': componentName, 'params': getComponentParamsFromCustomElement(node, bindingContext) };
allBindings['component']=valueAccessors
? function(){ return componentBindingValue; }
: componentBindingValue;
}}
return allBindings;
}
var nativeBindingProviderInstance=new ko.bindingProvider();
function getComponentParamsFromCustomElement(elem, bindingContext){
var paramsAttribute=elem.getAttribute('params');
if(paramsAttribute){
var params=nativeBindingProviderInstance['parseBindingsString'](paramsAttribute, bindingContext, elem, { 'valueAccessors': true, 'bindingParams': true }),
rawParamComputedValues=ko.utils.objectMap(params, function(paramValue, paramName){
return ko.computed(paramValue, null, { disposeWhenNodeIsRemoved: elem });
}),
result=ko.utils.objectMap(rawParamComputedValues, function(paramValueComputed, paramName){
var paramValue=paramValueComputed.peek();
if(!paramValueComputed.isActive()){
return paramValue;
}else{
return ko.computed({
'read': function(){
return ko.utils.unwrapObservable(paramValueComputed());
},
'write': ko.isWriteableObservable(paramValue)&&function(value){
paramValueComputed()(value);
},
disposeWhenNodeIsRemoved: elem
});
}});
if(!result.hasOwnProperty('$raw')){
result['$raw']=rawParamComputedValues;
}
return result;
}else{
return { '$raw': {}};}}
if(ko.utils.ieVersion < 9){
ko.components['register']=(function(originalFunction){
return function(componentName){
document.createElement(componentName);
return originalFunction.apply(this, arguments);
}})(ko.components['register']);
document.createDocumentFragment=(function(originalFunction){
return function(){
var newDocFrag=originalFunction(),
allComponents=ko.components._allRegisteredComponents;
for (var componentName in allComponents){
if(allComponents.hasOwnProperty(componentName)){
newDocFrag.createElement(componentName);
}}
return newDocFrag;
};})(document.createDocumentFragment);
}})();(function(undefined){
var componentLoadingOperationUniqueId=0;
ko.bindingHandlers['component']={
'init': function(element, valueAccessor, ignored1, ignored2, bindingContext){
var currentViewModel,
currentLoadingOperationId,
disposeAssociatedComponentViewModel=function (){
var currentViewModelDispose=currentViewModel&&currentViewModel['dispose'];
if(typeof currentViewModelDispose==='function'){
currentViewModelDispose.call(currentViewModel);
}
currentViewModel=null;
currentLoadingOperationId=null;
},
originalChildNodes=ko.utils.makeArray(ko.virtualElements.childNodes(element));
ko.utils.domNodeDisposal.addDisposeCallback(element, disposeAssociatedComponentViewModel);
ko.computed(function (){
var value=ko.utils.unwrapObservable(valueAccessor()),
componentName, componentParams;
if(typeof value==='string'){
componentName=value;
}else{
componentName=ko.utils.unwrapObservable(value['name']);
componentParams=ko.utils.unwrapObservable(value['params']);
}
if(!componentName){
throw new Error('No component name specified');
}
var loadingOperationId=currentLoadingOperationId=++componentLoadingOperationUniqueId;
ko.components.get(componentName, function(componentDefinition){
if(currentLoadingOperationId!==loadingOperationId){
return;
}
disposeAssociatedComponentViewModel();
if(!componentDefinition){
throw new Error('Unknown component \'' + componentName + '\'');
}
cloneTemplateIntoElement(componentName, componentDefinition, element);
var componentViewModel=createViewModel(componentDefinition, element, originalChildNodes, componentParams),
childBindingContext=bindingContext['createChildContext'](componentViewModel,  undefined, function(ctx){
ctx['$component']=componentViewModel;
ctx['$componentTemplateNodes']=originalChildNodes;
});
currentViewModel=componentViewModel;
ko.applyBindingsToDescendants(childBindingContext, element);
});
}, null, { disposeWhenNodeIsRemoved: element });
return { 'controlsDescendantBindings': true };}};
ko.virtualElements.allowedBindings['component']=true;
function cloneTemplateIntoElement(componentName, componentDefinition, element){
var template=componentDefinition['template'];
if(!template){
throw new Error('Component \'' + componentName + '\' has no template');
}
var clonedNodesArray=ko.utils.cloneNodes(template);
ko.virtualElements.setDomNodeChildren(element, clonedNodesArray);
}
function createViewModel(componentDefinition, element, originalChildNodes, componentParams){
var componentViewModelFactory=componentDefinition['createViewModel'];
return componentViewModelFactory
? componentViewModelFactory.call(componentDefinition, componentParams, { 'element': element, 'templateNodes': originalChildNodes })
: componentParams;
}})();
var attrHtmlToJavascriptMap={ 'class': 'className', 'for': 'htmlFor' };
ko.bindingHandlers['attr']={
'update': function(element, valueAccessor, allBindings){
var value=ko.utils.unwrapObservable(valueAccessor())||{};
ko.utils.objectForEach(value, function(attrName, attrValue){
attrValue=ko.utils.unwrapObservable(attrValue);
var toRemove=(attrValue===false)||(attrValue===null)||(attrValue===undefined);
if(toRemove)
element.removeAttribute(attrName);
if(ko.utils.ieVersion <=8&&attrName in attrHtmlToJavascriptMap){
attrName=attrHtmlToJavascriptMap[attrName];
if(toRemove)
element.removeAttribute(attrName);
else
element[attrName]=attrValue;
}else if(!toRemove){
element.setAttribute(attrName, attrValue.toString());
}
if(attrName==="name"){
ko.utils.setElementName(element, toRemove ? "":attrValue.toString());
}});
}};
(function(){
ko.bindingHandlers['checked']={
'after': ['value', 'attr'],
'init': function (element, valueAccessor, allBindings){
var checkedValue=ko.pureComputed(function(){
if(allBindings['has']('checkedValue')){
return ko.utils.unwrapObservable(allBindings.get('checkedValue'));
}else if(allBindings['has']('value')){
return ko.utils.unwrapObservable(allBindings.get('value'));
}
return element.value;
});
function updateModel(){
var isChecked=element.checked,
elemValue=useCheckedValue ? checkedValue():isChecked;
if(ko.computedContext.isInitial()){
return;
}
if(isRadio&&!isChecked){
return;
}
var modelValue=ko.dependencyDetection.ignore(valueAccessor);
if(valueIsArray){
var writableValue=rawValueIsNonArrayObservable ? modelValue.peek():modelValue;
if(oldElemValue!==elemValue){
if(isChecked){
ko.utils.addOrRemoveItem(writableValue, elemValue, true);
ko.utils.addOrRemoveItem(writableValue, oldElemValue, false);
}
oldElemValue=elemValue;
}else{
ko.utils.addOrRemoveItem(writableValue, elemValue, isChecked);
}
if(rawValueIsNonArrayObservable&&ko.isWriteableObservable(modelValue)){
modelValue(writableValue);
}}else{
ko.expressionRewriting.writeValueToProperty(modelValue, allBindings, 'checked', elemValue, true);
}};
function updateView(){
var modelValue=ko.utils.unwrapObservable(valueAccessor());
if(valueIsArray){
element.checked=ko.utils.arrayIndexOf(modelValue, checkedValue()) >=0;
}else if(isCheckbox){
element.checked=modelValue;
}else{
element.checked=(checkedValue()===modelValue);
}};
var isCheckbox=element.type=="checkbox",
isRadio=element.type=="radio";
if(!isCheckbox&&!isRadio){
return;
}
var rawValue=valueAccessor(),
valueIsArray=isCheckbox&&(ko.utils.unwrapObservable(rawValue) instanceof Array),
rawValueIsNonArrayObservable = !(valueIsArray&&rawValue.push&&rawValue.splice),
oldElemValue=valueIsArray ? checkedValue():undefined,
useCheckedValue=isRadio||valueIsArray;
if(isRadio&&!element.name)
ko.bindingHandlers['uniqueName']['init'](element, function(){ return true });
ko.computed(updateModel, null, { disposeWhenNodeIsRemoved: element });
ko.utils.registerEventHandler(element, "click", updateModel);
ko.computed(updateView, null, { disposeWhenNodeIsRemoved: element });
rawValue=undefined;
}};
ko.expressionRewriting.twoWayBindings['checked']=true;
ko.bindingHandlers['checkedValue']={
'update': function (element, valueAccessor){
element.value=ko.utils.unwrapObservable(valueAccessor());
}};})();var classesWrittenByBindingKey='__ko__cssValue';
ko.bindingHandlers['css']={
'update': function (element, valueAccessor){
var value=ko.utils.unwrapObservable(valueAccessor());
if(value!==null&&typeof value=="object"){
ko.utils.objectForEach(value, function(className, shouldHaveClass){
shouldHaveClass=ko.utils.unwrapObservable(shouldHaveClass);
ko.utils.toggleDomNodeCssClass(element, className, shouldHaveClass);
});
}else{
value=ko.utils.stringTrim(String(value||''));
ko.utils.toggleDomNodeCssClass(element, element[classesWrittenByBindingKey], false);
element[classesWrittenByBindingKey]=value;
ko.utils.toggleDomNodeCssClass(element, value, true);
}}
};
ko.bindingHandlers['enable']={
'update': function (element, valueAccessor){
var value=ko.utils.unwrapObservable(valueAccessor());
if(value&&element.disabled)
element.removeAttribute("disabled");
else if((!value)&&(!element.disabled))
element.disabled=true;
}};
ko.bindingHandlers['disable']={
'update': function (element, valueAccessor){
ko.bindingHandlers['enable']['update'](element, function(){ return !ko.utils.unwrapObservable(valueAccessor()) });
}};
function makeEventHandlerShortcut(eventName){
ko.bindingHandlers[eventName]={
'init': function(element, valueAccessor, allBindings, viewModel, bindingContext){
var newValueAccessor=function (){
var result={};
result[eventName]=valueAccessor();
return result;
};
return ko.bindingHandlers['event']['init'].call(this, element, newValueAccessor, allBindings, viewModel, bindingContext);
}}
}
ko.bindingHandlers['event']={
'init':function (element, valueAccessor, allBindings, viewModel, bindingContext){
var eventsToHandle=valueAccessor()||{};
ko.utils.objectForEach(eventsToHandle, function(eventName){
if(typeof eventName=="string"){
ko.utils.registerEventHandler(element, eventName, function (event){
var handlerReturnValue;
var handlerFunction=valueAccessor()[eventName];
if(!handlerFunction)
return;
try {
var argsForHandler=ko.utils.makeArray(arguments);
viewModel=bindingContext['$data'];
argsForHandler.unshift(viewModel);
handlerReturnValue=handlerFunction.apply(viewModel, argsForHandler);
} finally {
if(handlerReturnValue!==true){
if(event.preventDefault)
event.preventDefault();
else
event.returnValue=false;
}}
var bubble=allBindings.get(eventName + 'Bubble')!==false;
if(!bubble){
event.cancelBubble=true;
if(event.stopPropagation)
event.stopPropagation();
}});
}});
}};
ko.bindingHandlers['foreach']={
makeTemplateValueAccessor: function(valueAccessor){
return function(){
var modelValue=valueAccessor(),
unwrappedValue=ko.utils.peekObservable(modelValue);
if((!unwrappedValue)||typeof unwrappedValue.length=="number")
return { 'foreach': modelValue, 'templateEngine': ko.nativeTemplateEngine.instance };
ko.utils.unwrapObservable(modelValue);
return {
'foreach': unwrappedValue['data'],
'as': unwrappedValue['as'],
'includeDestroyed': unwrappedValue['includeDestroyed'],
'afterAdd': unwrappedValue['afterAdd'],
'beforeRemove': unwrappedValue['beforeRemove'],
'afterRender': unwrappedValue['afterRender'],
'beforeMove': unwrappedValue['beforeMove'],
'afterMove': unwrappedValue['afterMove'],
'templateEngine': ko.nativeTemplateEngine.instance
};};
},
'init': function(element, valueAccessor, allBindings, viewModel, bindingContext){
return ko.bindingHandlers['template']['init'](element, ko.bindingHandlers['foreach'].makeTemplateValueAccessor(valueAccessor));
},
'update': function(element, valueAccessor, allBindings, viewModel, bindingContext){
return ko.bindingHandlers['template']['update'](element, ko.bindingHandlers['foreach'].makeTemplateValueAccessor(valueAccessor), allBindings, viewModel, bindingContext);
}};
ko.expressionRewriting.bindingRewriteValidators['foreach']=false;
ko.virtualElements.allowedBindings['foreach']=true;
var hasfocusUpdatingProperty='__ko_hasfocusUpdating';
var hasfocusLastValue='__ko_hasfocusLastValue';
ko.bindingHandlers['hasfocus']={
'init': function(element, valueAccessor, allBindings){
var handleElementFocusChange=function(isFocused){
element[hasfocusUpdatingProperty]=true;
var ownerDoc=element.ownerDocument;
if("activeElement" in ownerDoc){
var active;
try {
active=ownerDoc.activeElement;
} catch(e){
active=ownerDoc.body;
}
isFocused=(active===element);
}
var modelValue=valueAccessor();
ko.expressionRewriting.writeValueToProperty(modelValue, allBindings, 'hasfocus', isFocused, true);
element[hasfocusLastValue]=isFocused;
element[hasfocusUpdatingProperty]=false;
};
var handleElementFocusIn=handleElementFocusChange.bind(null, true);
var handleElementFocusOut=handleElementFocusChange.bind(null, false);
ko.utils.registerEventHandler(element, "focus", handleElementFocusIn);
ko.utils.registerEventHandler(element, "focusin", handleElementFocusIn);
ko.utils.registerEventHandler(element, "blur",  handleElementFocusOut);
ko.utils.registerEventHandler(element, "focusout",  handleElementFocusOut);
},
'update': function(element, valueAccessor){
var value = !!ko.utils.unwrapObservable(valueAccessor());
if(!element[hasfocusUpdatingProperty]&&element[hasfocusLastValue]!==value){
value ? element.focus():element.blur();
if(!value&&element[hasfocusLastValue]){
element.ownerDocument.body.focus();
}
ko.dependencyDetection.ignore(ko.utils.triggerEvent, null, [element, value ? "focusin":"focusout"]);
}}
};
ko.expressionRewriting.twoWayBindings['hasfocus']=true;
ko.bindingHandlers['hasFocus']=ko.bindingHandlers['hasfocus'];
ko.expressionRewriting.twoWayBindings['hasFocus']=true;
ko.bindingHandlers['html']={
'init': function(){
return { 'controlsDescendantBindings': true };},
'update': function (element, valueAccessor){
ko.utils.setHtml(element, valueAccessor());
}};
function makeWithIfBinding(bindingKey, isWith, isNot, makeContextCallback){
ko.bindingHandlers[bindingKey]={
'init': function(element, valueAccessor, allBindings, viewModel, bindingContext){
var didDisplayOnLastUpdate,
savedNodes;
ko.computed(function(){
var rawValue=valueAccessor(),
dataValue=ko.utils.unwrapObservable(rawValue),
shouldDisplay = !isNot!==!dataValue,
isFirstRender = !savedNodes,
needsRefresh=isFirstRender||isWith||(shouldDisplay!==didDisplayOnLastUpdate);
if(needsRefresh){
if(isFirstRender&&ko.computedContext.getDependenciesCount()){
savedNodes=ko.utils.cloneNodes(ko.virtualElements.childNodes(element), true );
}
if(shouldDisplay){
if(!isFirstRender){
ko.virtualElements.setDomNodeChildren(element, ko.utils.cloneNodes(savedNodes));
}
ko.applyBindingsToDescendants(makeContextCallback ? makeContextCallback(bindingContext, rawValue):bindingContext, element);
}else{
ko.virtualElements.emptyNode(element);
}
didDisplayOnLastUpdate=shouldDisplay;
}}, null, { disposeWhenNodeIsRemoved: element });
return { 'controlsDescendantBindings': true };}};
ko.expressionRewriting.bindingRewriteValidators[bindingKey]=false;
ko.virtualElements.allowedBindings[bindingKey]=true;
}
makeWithIfBinding('if');
makeWithIfBinding('ifnot', false , true );
makeWithIfBinding('with', true , false ,
function(bindingContext, dataValue){
return bindingContext.createStaticChildContext(dataValue);
}
);
var captionPlaceholder={};
ko.bindingHandlers['options']={
'init': function(element){
if(ko.utils.tagNameLower(element)!=="select")
throw new Error("options binding applies only to SELECT elements");
while (element.length > 0){
element.remove(0);
}
return { 'controlsDescendantBindings': true };},
'update': function (element, valueAccessor, allBindings){
function selectedOptions(){
return ko.utils.arrayFilter(element.options, function (node){ return node.selected; });
}
var selectWasPreviouslyEmpty=element.length==0,
multiple=element.multiple,
previousScrollTop=(!selectWasPreviouslyEmpty&&multiple) ? element.scrollTop:null,
unwrappedArray=ko.utils.unwrapObservable(valueAccessor()),
valueAllowUnset=allBindings.get('valueAllowUnset')&&allBindings['has']('value'),
includeDestroyed=allBindings.get('optionsIncludeDestroyed'),
arrayToDomNodeChildrenOptions={},
captionValue,
filteredArray,
previousSelectedValues=[];
if(!valueAllowUnset){
if(multiple){
previousSelectedValues=ko.utils.arrayMap(selectedOptions(), ko.selectExtensions.readValue);
}else if(element.selectedIndex >=0){
previousSelectedValues.push(ko.selectExtensions.readValue(element.options[element.selectedIndex]));
}}
if(unwrappedArray){
if(typeof unwrappedArray.length=="undefined")
unwrappedArray=[unwrappedArray];
filteredArray=ko.utils.arrayFilter(unwrappedArray, function(item){
return includeDestroyed||item===undefined||item===null||!ko.utils.unwrapObservable(item['_destroy']);
});
if(allBindings['has']('optionsCaption')){
captionValue=ko.utils.unwrapObservable(allBindings.get('optionsCaption'));
if(captionValue!==null&&captionValue!==undefined){
filteredArray.unshift(captionPlaceholder);
}}
}else{
}
function applyToObject(object, predicate, defaultValue){
var predicateType=typeof predicate;
if(predicateType=="function")
return predicate(object);
else if(predicateType=="string")
return object[predicate];
else
return defaultValue;
}
var itemUpdate=false;
function optionForArrayItem(arrayEntry, index, oldOptions){
if(oldOptions.length){
previousSelectedValues = !valueAllowUnset&&oldOptions[0].selected ? [ ko.selectExtensions.readValue(oldOptions[0]) ]:[];
itemUpdate=true;
}
var option=element.ownerDocument.createElement("option");
if(arrayEntry===captionPlaceholder){
ko.utils.setTextContent(option, allBindings.get('optionsCaption'));
ko.selectExtensions.writeValue(option, undefined);
}else{
var optionValue=applyToObject(arrayEntry, allBindings.get('optionsValue'), arrayEntry);
ko.selectExtensions.writeValue(option, ko.utils.unwrapObservable(optionValue));
var optionText=applyToObject(arrayEntry, allBindings.get('optionsText'), optionValue);
ko.utils.setTextContent(option, optionText);
}
return [option];
}
arrayToDomNodeChildrenOptions['beforeRemove'] =
function (option){
element.removeChild(option);
};
function setSelectionCallback(arrayEntry, newOptions){
if(itemUpdate&&valueAllowUnset){
ko.selectExtensions.writeValue(element, ko.utils.unwrapObservable(allBindings.get('value')), true );
}else if(previousSelectedValues.length){
var isSelected=ko.utils.arrayIndexOf(previousSelectedValues, ko.selectExtensions.readValue(newOptions[0])) >=0;
ko.utils.setOptionNodeSelectionState(newOptions[0], isSelected);
if(itemUpdate&&!isSelected){
ko.dependencyDetection.ignore(ko.utils.triggerEvent, null, [element, "change"]);
}}
}
var callback=setSelectionCallback;
if(allBindings['has']('optionsAfterRender')&&typeof allBindings.get('optionsAfterRender')=="function"){
callback=function(arrayEntry, newOptions){
setSelectionCallback(arrayEntry, newOptions);
ko.dependencyDetection.ignore(allBindings.get('optionsAfterRender'), null, [newOptions[0], arrayEntry!==captionPlaceholder ? arrayEntry:undefined]);
}}
ko.utils.setDomNodeChildrenFromArrayMapping(element, filteredArray, optionForArrayItem, arrayToDomNodeChildrenOptions, callback);
ko.dependencyDetection.ignore(function (){
if(valueAllowUnset){
ko.selectExtensions.writeValue(element, ko.utils.unwrapObservable(allBindings.get('value')), true );
}else{
var selectionChanged;
if(multiple){
selectionChanged=previousSelectedValues.length&&selectedOptions().length < previousSelectedValues.length;
}else{
selectionChanged=(previousSelectedValues.length&&element.selectedIndex >=0)
? (ko.selectExtensions.readValue(element.options[element.selectedIndex])!==previousSelectedValues[0])
: (previousSelectedValues.length||element.selectedIndex >=0);
}
if(selectionChanged){
ko.utils.triggerEvent(element, "change");
}}
});
ko.utils.ensureSelectElementIsRenderedCorrectly(element);
if(previousScrollTop&&Math.abs(previousScrollTop - element.scrollTop) > 20)
element.scrollTop=previousScrollTop;
}};
ko.bindingHandlers['options'].optionValueDomDataKey=ko.utils.domData.nextKey();
ko.bindingHandlers['selectedOptions']={
'after': ['options', 'foreach'],
'init': function (element, valueAccessor, allBindings){
ko.utils.registerEventHandler(element, "change", function (){
var value=valueAccessor(), valueToWrite=[];
ko.utils.arrayForEach(element.getElementsByTagName("option"), function(node){
if(node.selected)
valueToWrite.push(ko.selectExtensions.readValue(node));
});
ko.expressionRewriting.writeValueToProperty(value, allBindings, 'selectedOptions', valueToWrite);
});
},
'update': function (element, valueAccessor){
if(ko.utils.tagNameLower(element)!="select")
throw new Error("values binding applies only to SELECT elements");
var newValue=ko.utils.unwrapObservable(valueAccessor()),
previousScrollTop=element.scrollTop;
if(newValue&&typeof newValue.length=="number"){
ko.utils.arrayForEach(element.getElementsByTagName("option"), function(node){
var isSelected=ko.utils.arrayIndexOf(newValue, ko.selectExtensions.readValue(node)) >=0;
if(node.selected!=isSelected){
ko.utils.setOptionNodeSelectionState(node, isSelected);
}});
}
element.scrollTop=previousScrollTop;
}};
ko.expressionRewriting.twoWayBindings['selectedOptions']=true;
ko.bindingHandlers['style']={
'update': function (element, valueAccessor){
var value=ko.utils.unwrapObservable(valueAccessor()||{});
ko.utils.objectForEach(value, function(styleName, styleValue){
styleValue=ko.utils.unwrapObservable(styleValue);
if(styleValue===null||styleValue===undefined||styleValue===false){
styleValue="";
}
element.style[styleName]=styleValue;
});
}};
ko.bindingHandlers['submit']={
'init': function (element, valueAccessor, allBindings, viewModel, bindingContext){
if(typeof valueAccessor()!="function")
throw new Error("The value for a submit binding must be a function");
ko.utils.registerEventHandler(element, "submit", function (event){
var handlerReturnValue;
var value=valueAccessor();
try { handlerReturnValue=value.call(bindingContext['$data'], element); }
finally {
if(handlerReturnValue!==true){
if(event.preventDefault)
event.preventDefault();
else
event.returnValue=false;
}}
});
}};
ko.bindingHandlers['text']={
'init': function(){
return { 'controlsDescendantBindings': true };},
'update': function (element, valueAccessor){
ko.utils.setTextContent(element, valueAccessor());
}};
ko.virtualElements.allowedBindings['text']=true;
(function (){
if(window&&window.navigator){
var parseVersion=function (matches){
if(matches){
return parseFloat(matches[1]);
}};
var operaVersion=window.opera&&window.opera.version&&parseInt(window.opera.version()),
userAgent=window.navigator.userAgent,
safariVersion=parseVersion(userAgent.match(/^(?:(?!chrome).)*version\/([^ ]*) safari/i)),
firefoxVersion=parseVersion(userAgent.match(/Firefox\/([^ ]*)/));
}
if(ko.utils.ieVersion < 10){
var selectionChangeRegisteredName=ko.utils.domData.nextKey(),
selectionChangeHandlerName=ko.utils.domData.nextKey();
var selectionChangeHandler=function(event){
var target=this.activeElement,
handler=target&&ko.utils.domData.get(target, selectionChangeHandlerName);
if(handler){
handler(event);
}};
var registerForSelectionChangeEvent=function (element, handler){
var ownerDoc=element.ownerDocument;
if(!ko.utils.domData.get(ownerDoc, selectionChangeRegisteredName)){
ko.utils.domData.set(ownerDoc, selectionChangeRegisteredName, true);
ko.utils.registerEventHandler(ownerDoc, 'selectionchange', selectionChangeHandler);
}
ko.utils.domData.set(element, selectionChangeHandlerName, handler);
};}
ko.bindingHandlers['textInput']={
'init': function (element, valueAccessor, allBindings){
var previousElementValue=element.value,
timeoutHandle,
elementValueBeforeEvent;
var updateModel=function (event){
clearTimeout(timeoutHandle);
elementValueBeforeEvent=timeoutHandle=undefined;
var elementValue=element.value;
if(previousElementValue!==elementValue){
if(DEBUG&&event) element['_ko_textInputProcessedEvent']=event.type;
previousElementValue=elementValue;
ko.expressionRewriting.writeValueToProperty(valueAccessor(), allBindings, 'textInput', elementValue);
}};
var deferUpdateModel=function (event){
if(!timeoutHandle){
elementValueBeforeEvent=element.value;
var handler=DEBUG ? updateModel.bind(element, {type: event.type}):updateModel;
timeoutHandle=ko.utils.setTimeout(handler, 4);
}};
var ieUpdateModel=ko.utils.ieVersion==9 ? deferUpdateModel:updateModel;
var updateView=function (){
var modelValue=ko.utils.unwrapObservable(valueAccessor());
if(modelValue===null||modelValue===undefined){
modelValue='';
}
if(elementValueBeforeEvent!==undefined&&modelValue===elementValueBeforeEvent){
ko.utils.setTimeout(updateView, 4);
return;
}
if(element.value!==modelValue){
previousElementValue=modelValue;
element.value=modelValue;
}};
var onEvent=function (event, handler){
ko.utils.registerEventHandler(element, event, handler);
};
if(DEBUG&&ko.bindingHandlers['textInput']['_forceUpdateOn']){
ko.utils.arrayForEach(ko.bindingHandlers['textInput']['_forceUpdateOn'], function(eventName){
if(eventName.slice(0,5)=='after'){
onEvent(eventName.slice(5), deferUpdateModel);
}else{
onEvent(eventName, updateModel);
}});
}else{
if(ko.utils.ieVersion < 10){
onEvent('propertychange', function(event){
if(event.propertyName==='value'){
ieUpdateModel(event);
}});
if(ko.utils.ieVersion==8){
onEvent('keyup', updateModel);
onEvent('keydown', updateModel);
}
if(ko.utils.ieVersion >=8){
registerForSelectionChangeEvent(element, ieUpdateModel);  // 'selectionchange' covers cut, paste, drop, delete, etc.
onEvent('dragend', deferUpdateModel);
}}else{
onEvent('input', updateModel);
if(safariVersion < 5&&ko.utils.tagNameLower(element)==="textarea"){
onEvent('keydown', deferUpdateModel);
onEvent('paste', deferUpdateModel);
onEvent('cut', deferUpdateModel);
}else if(operaVersion < 11){
onEvent('keydown', deferUpdateModel);
}else if(firefoxVersion < 4.0){
onEvent('DOMAutoComplete', updateModel);
onEvent('dragdrop', updateModel);
onEvent('drop', updateModel);
}}
}
onEvent('change', updateModel);
ko.computed(updateView, null, { disposeWhenNodeIsRemoved: element });
}};
ko.expressionRewriting.twoWayBindings['textInput']=true;
ko.bindingHandlers['textinput']={
'preprocess': function (value, name, addBinding){
addBinding('textInput', value);
}};})();ko.bindingHandlers['uniqueName']={
'init': function (element, valueAccessor){
if(valueAccessor()){
var name="ko_unique_" + (++ko.bindingHandlers['uniqueName'].currentIndex);
ko.utils.setElementName(element, name);
}}
};
ko.bindingHandlers['uniqueName'].currentIndex=0;
ko.bindingHandlers['value']={
'after': ['options', 'foreach'],
'init': function (element, valueAccessor, allBindings){
if(element.tagName.toLowerCase()=="input"&&(element.type=="checkbox"||element.type=="radio")){
ko.applyBindingAccessorsToNode(element, { 'checkedValue': valueAccessor });
return;
}
var eventsToCatch=["change"];
var requestedEventsToCatch=allBindings.get("valueUpdate");
var propertyChangedFired=false;
var elementValueBeforeEvent=null;
if(requestedEventsToCatch){
if(typeof requestedEventsToCatch=="string")
requestedEventsToCatch=[requestedEventsToCatch];
ko.utils.arrayPushAll(eventsToCatch, requestedEventsToCatch);
eventsToCatch=ko.utils.arrayGetDistinctValues(eventsToCatch);
}
var valueUpdateHandler=function(){
elementValueBeforeEvent=null;
propertyChangedFired=false;
var modelValue=valueAccessor();
var elementValue=ko.selectExtensions.readValue(element);
ko.expressionRewriting.writeValueToProperty(modelValue, allBindings, 'value', elementValue);
}
var ieAutoCompleteHackNeeded=ko.utils.ieVersion&&element.tagName.toLowerCase()=="input"&&element.type=="text"
&& element.autocomplete!="off"&&(!element.form||element.form.autocomplete!="off");
if(ieAutoCompleteHackNeeded&&ko.utils.arrayIndexOf(eventsToCatch, "propertychange")==-1){
ko.utils.registerEventHandler(element, "propertychange", function (){ propertyChangedFired=true });
ko.utils.registerEventHandler(element, "focus", function (){ propertyChangedFired=false });
ko.utils.registerEventHandler(element, "blur", function(){
if(propertyChangedFired){
valueUpdateHandler();
}});
}
ko.utils.arrayForEach(eventsToCatch, function(eventName){
var handler=valueUpdateHandler;
if(ko.utils.stringStartsWith(eventName, "after")){
handler=function(){
elementValueBeforeEvent=ko.selectExtensions.readValue(element);
ko.utils.setTimeout(valueUpdateHandler, 0);
};
eventName=eventName.substring("after".length);
}
ko.utils.registerEventHandler(element, eventName, handler);
});
var updateFromModel=function (){
var newValue=ko.utils.unwrapObservable(valueAccessor());
var elementValue=ko.selectExtensions.readValue(element);
if(elementValueBeforeEvent!==null&&newValue===elementValueBeforeEvent){
ko.utils.setTimeout(updateFromModel, 0);
return;
}
var valueHasChanged=(newValue!==elementValue);
if(valueHasChanged){
if(ko.utils.tagNameLower(element)==="select"){
var allowUnset=allBindings.get('valueAllowUnset');
var applyValueAction=function (){
ko.selectExtensions.writeValue(element, newValue, allowUnset);
};
applyValueAction();
if(!allowUnset&&newValue!==ko.selectExtensions.readValue(element)){
ko.dependencyDetection.ignore(ko.utils.triggerEvent, null, [element, "change"]);
}else{
ko.utils.setTimeout(applyValueAction, 0);
}}else{
ko.selectExtensions.writeValue(element, newValue);
}}
};
ko.computed(updateFromModel, null, { disposeWhenNodeIsRemoved: element });
},
'update': function(){}};
ko.expressionRewriting.twoWayBindings['value']=true;
ko.bindingHandlers['visible']={
'update': function (element, valueAccessor){
var value=ko.utils.unwrapObservable(valueAccessor());
var isCurrentlyVisible = !(element.style.display=="none");
if(value&&!isCurrentlyVisible)
element.style.display="";
else if((!value)&&isCurrentlyVisible)
element.style.display="none";
}};
makeEventHandlerShortcut('click');
ko.templateEngine=function (){ };
ko.templateEngine.prototype['renderTemplateSource']=function (templateSource, bindingContext, options, templateDocument){
throw new Error("Override renderTemplateSource");
};
ko.templateEngine.prototype['createJavaScriptEvaluatorBlock']=function (script){
throw new Error("Override createJavaScriptEvaluatorBlock");
};
ko.templateEngine.prototype['makeTemplateSource']=function(template, templateDocument){
if(typeof template=="string"){
templateDocument=templateDocument||document;
var elem=templateDocument.getElementById(template);
if(!elem)
throw new Error("Cannot find template with ID " + template);
return new ko.templateSources.domElement(elem);
}else if((template.nodeType==1)||(template.nodeType==8)){
return new ko.templateSources.anonymousTemplate(template);
} else
throw new Error("Unknown template type: " + template);
};
ko.templateEngine.prototype['renderTemplate']=function (template, bindingContext, options, templateDocument){
var templateSource=this['makeTemplateSource'](template, templateDocument);
return this['renderTemplateSource'](templateSource, bindingContext, options, templateDocument);
};
ko.templateEngine.prototype['isTemplateRewritten']=function (template, templateDocument){
if(this['allowTemplateRewriting']===false)
return true;
return this['makeTemplateSource'](template, templateDocument)['data']("isRewritten");
};
ko.templateEngine.prototype['rewriteTemplate']=function (template, rewriterCallback, templateDocument){
var templateSource=this['makeTemplateSource'](template, templateDocument);
var rewritten=rewriterCallback(templateSource['text']());
templateSource['text'](rewritten);
templateSource['data']("isRewritten", true);
};
ko.exportSymbol('templateEngine', ko.templateEngine);
ko.templateRewriting=(function (){
var memoizeDataBindingAttributeSyntaxRegex=/(<([a-z]+\d*)(?:\s+(?!data-bind\s*=\s*)[a-z0-9\-]+(?:=(?:\"[^\"]*\"|\'[^\']*\'|[^>]*))?)*\s+)data-bind\s*=\s*(["'])([\s\S]*?)\3/gi;
var memoizeVirtualContainerBindingSyntaxRegex=/<!--\s*ko\b\s*([\s\S]*?)\s*-->/g;
function validateDataBindValuesForRewriting(keyValueArray){
var allValidators=ko.expressionRewriting.bindingRewriteValidators;
for (var i=0; i < keyValueArray.length; i++){
var key=keyValueArray[i]['key'];
if(allValidators.hasOwnProperty(key)){
var validator=allValidators[key];
if(typeof validator==="function"){
var possibleErrorMessage=validator(keyValueArray[i]['value']);
if(possibleErrorMessage)
throw new Error(possibleErrorMessage);
}else if(!validator){
throw new Error("This template engine does not support the '" + key + "' binding within its templates");
}}
}}
function constructMemoizedTagReplacement(dataBindAttributeValue, tagToRetain, nodeName, templateEngine){
var dataBindKeyValueArray=ko.expressionRewriting.parseObjectLiteral(dataBindAttributeValue);
validateDataBindValuesForRewriting(dataBindKeyValueArray);
var rewrittenDataBindAttributeValue=ko.expressionRewriting.preProcessBindings(dataBindKeyValueArray, {'valueAccessors':true});
var applyBindingsToNextSiblingScript =
"ko.__tr_ambtns(function($context,$element){return(function(){return{ " + rewrittenDataBindAttributeValue + " }})()},'" + nodeName.toLowerCase() + "')";
return templateEngine['createJavaScriptEvaluatorBlock'](applyBindingsToNextSiblingScript) + tagToRetain;
}
return {
ensureTemplateIsRewritten: function (template, templateEngine, templateDocument){
if(!templateEngine['isTemplateRewritten'](template, templateDocument))
templateEngine['rewriteTemplate'](template, function (htmlString){
return ko.templateRewriting.memoizeBindingAttributeSyntax(htmlString, templateEngine);
}, templateDocument);
},
memoizeBindingAttributeSyntax: function (htmlString, templateEngine){
return htmlString.replace(memoizeDataBindingAttributeSyntaxRegex, function (){
return constructMemoizedTagReplacement( arguments[4],  arguments[1],  arguments[2], templateEngine);
}).replace(memoizeVirtualContainerBindingSyntaxRegex, function(){
return constructMemoizedTagReplacement( arguments[1],  "<!-- ko -->",  "#comment", templateEngine);
});
},
applyMemoizedBindingsToNextSibling: function (bindings, nodeName){
return ko.memoization.memoize(function (domNode, bindingContext){
var nodeToBind=domNode.nextSibling;
if(nodeToBind&&nodeToBind.nodeName.toLowerCase()===nodeName){
ko.applyBindingAccessorsToNode(nodeToBind, bindings, bindingContext);
}});
}}
})();
ko.exportSymbol('__tr_ambtns', ko.templateRewriting.applyMemoizedBindingsToNextSibling);
(function(){
ko.templateSources={};
var templateScript=1,
templateTextArea=2,
templateTemplate=3,
templateElement=4;
ko.templateSources.domElement=function(element){
this.domElement=element;
if(element){
var tagNameLower=ko.utils.tagNameLower(element);
this.templateType =
tagNameLower==="script" ? templateScript :
tagNameLower==="textarea" ? templateTextArea :
tagNameLower=="template"&&element.content&&element.content.nodeType===11 ? templateTemplate :
templateElement;
}}
ko.templateSources.domElement.prototype['text']=function(){
var elemContentsProperty=this.templateType===templateScript ? "text"
: this.templateType===templateTextArea ? "value"
: "innerHTML";
if(arguments.length==0){
return this.domElement[elemContentsProperty];
}else{
var valueToWrite=arguments[0];
if(elemContentsProperty==="innerHTML")
ko.utils.setHtml(this.domElement, valueToWrite);
else
this.domElement[elemContentsProperty]=valueToWrite;
}};
var dataDomDataPrefix=ko.utils.domData.nextKey() + "_";
ko.templateSources.domElement.prototype['data']=function(key ){
if(arguments.length===1){
return ko.utils.domData.get(this.domElement, dataDomDataPrefix + key);
}else{
ko.utils.domData.set(this.domElement, dataDomDataPrefix + key, arguments[1]);
}};
var templatesDomDataKey=ko.utils.domData.nextKey();
function getTemplateDomData(element){
return ko.utils.domData.get(element, templatesDomDataKey)||{};}
function setTemplateDomData(element, data){
ko.utils.domData.set(element, templatesDomDataKey, data);
}
ko.templateSources.domElement.prototype['nodes']=function(){
var element=this.domElement;
if(arguments.length==0){
var templateData=getTemplateDomData(element),
containerData=templateData.containerData;
return containerData||(
this.templateType===templateTemplate ? element.content :
this.templateType===templateElement ? element :
undefined);
}else{
var valueToWrite=arguments[0];
setTemplateDomData(element, {containerData: valueToWrite});
}};
ko.templateSources.anonymousTemplate=function(element){
this.domElement=element;
}
ko.templateSources.anonymousTemplate.prototype=new ko.templateSources.domElement();
ko.templateSources.anonymousTemplate.prototype.constructor=ko.templateSources.anonymousTemplate;
ko.templateSources.anonymousTemplate.prototype['text']=function(){
if(arguments.length==0){
var templateData=getTemplateDomData(this.domElement);
if(templateData.textData===undefined&&templateData.containerData)
templateData.textData=templateData.containerData.innerHTML;
return templateData.textData;
}else{
var valueToWrite=arguments[0];
setTemplateDomData(this.domElement, {textData: valueToWrite});
}};
ko.exportSymbol('templateSources', ko.templateSources);
ko.exportSymbol('templateSources.domElement', ko.templateSources.domElement);
ko.exportSymbol('templateSources.anonymousTemplate', ko.templateSources.anonymousTemplate);
})();
(function (){
var _templateEngine;
ko.setTemplateEngine=function (templateEngine){
if((templateEngine!=undefined)&&!(templateEngine instanceof ko.templateEngine))
throw new Error("templateEngine must inherit from ko.templateEngine");
_templateEngine=templateEngine;
}
function invokeForEachNodeInContinuousRange(firstNode, lastNode, action){
var node, nextInQueue=firstNode, firstOutOfRangeNode=ko.virtualElements.nextSibling(lastNode);
while (nextInQueue&&((node=nextInQueue)!==firstOutOfRangeNode)){
nextInQueue=ko.virtualElements.nextSibling(node);
action(node, nextInQueue);
}}
function activateBindingsOnContinuousNodeArray(continuousNodeArray, bindingContext){
if(continuousNodeArray.length){
var firstNode=continuousNodeArray[0],
lastNode=continuousNodeArray[continuousNodeArray.length - 1],
parentNode=firstNode.parentNode,
provider=ko.bindingProvider['instance'],
preprocessNode=provider['preprocessNode'];
if(preprocessNode){
invokeForEachNodeInContinuousRange(firstNode, lastNode, function(node, nextNodeInRange){
var nodePreviousSibling=node.previousSibling;
var newNodes=preprocessNode.call(provider, node);
if(newNodes){
if(node===firstNode)
firstNode=newNodes[0]||nextNodeInRange;
if(node===lastNode)
lastNode=newNodes[newNodes.length - 1]||nodePreviousSibling;
}});
continuousNodeArray.length=0;
if(!firstNode){
return;
}
if(firstNode===lastNode){
continuousNodeArray.push(firstNode);
}else{
continuousNodeArray.push(firstNode, lastNode);
ko.utils.fixUpContinuousNodeArray(continuousNodeArray, parentNode);
}}
invokeForEachNodeInContinuousRange(firstNode, lastNode, function(node){
if(node.nodeType===1||node.nodeType===8)
ko.applyBindings(bindingContext, node);
});
invokeForEachNodeInContinuousRange(firstNode, lastNode, function(node){
if(node.nodeType===1||node.nodeType===8)
ko.memoization.unmemoizeDomNodeAndDescendants(node, [bindingContext]);
});
ko.utils.fixUpContinuousNodeArray(continuousNodeArray, parentNode);
}}
function getFirstNodeFromPossibleArray(nodeOrNodeArray){
return nodeOrNodeArray.nodeType ? nodeOrNodeArray
: nodeOrNodeArray.length > 0 ? nodeOrNodeArray[0]
: null;
}
function executeTemplate(targetNodeOrNodeArray, renderMode, template, bindingContext, options){
options=options||{};
var firstTargetNode=targetNodeOrNodeArray&&getFirstNodeFromPossibleArray(targetNodeOrNodeArray);
var templateDocument=(firstTargetNode||template||{}).ownerDocument;
var templateEngineToUse=(options['templateEngine']||_templateEngine);
ko.templateRewriting.ensureTemplateIsRewritten(template, templateEngineToUse, templateDocument);
var renderedNodesArray=templateEngineToUse['renderTemplate'](template, bindingContext, options, templateDocument);
if((typeof renderedNodesArray.length!="number")||(renderedNodesArray.length > 0&&typeof renderedNodesArray[0].nodeType!="number"))
throw new Error("Template engine must return an array of DOM nodes");
var haveAddedNodesToParent=false;
switch (renderMode){
case "replaceChildren":
ko.virtualElements.setDomNodeChildren(targetNodeOrNodeArray, renderedNodesArray);
haveAddedNodesToParent=true;
break;
case "replaceNode":
ko.utils.replaceDomNodes(targetNodeOrNodeArray, renderedNodesArray);
haveAddedNodesToParent=true;
break;
case "ignoreTargetNode": break;
default:
throw new Error("Unknown renderMode: " + renderMode);
}
if(haveAddedNodesToParent){
activateBindingsOnContinuousNodeArray(renderedNodesArray, bindingContext);
if(options['afterRender'])
ko.dependencyDetection.ignore(options['afterRender'], null, [renderedNodesArray, bindingContext['$data']]);
}
return renderedNodesArray;
}
function resolveTemplateName(template, data, context){
if(ko.isObservable(template)){
return template();
}else if(typeof template==='function'){
return template(data, context);
}else{
return template;
}}
ko.renderTemplate=function (template, dataOrBindingContext, options, targetNodeOrNodeArray, renderMode){
options=options||{};
if((options['templateEngine']||_templateEngine)==undefined)
throw new Error("Set a template engine before calling renderTemplate");
renderMode=renderMode||"replaceChildren";
if(targetNodeOrNodeArray){
var firstTargetNode=getFirstNodeFromPossibleArray(targetNodeOrNodeArray);
var whenToDispose=function (){ return (!firstTargetNode)||!ko.utils.domNodeIsAttachedToDocument(firstTargetNode); };
var activelyDisposeWhenNodeIsRemoved=(firstTargetNode&&renderMode=="replaceNode") ? firstTargetNode.parentNode:firstTargetNode;
return ko.dependentObservable(function (){
var bindingContext=(dataOrBindingContext&&(dataOrBindingContext instanceof ko.bindingContext))
? dataOrBindingContext
: new ko.bindingContext(dataOrBindingContext, null, null, null, { "exportDependencies": true });
var templateName=resolveTemplateName(template, bindingContext['$data'], bindingContext),
renderedNodesArray=executeTemplate(targetNodeOrNodeArray, renderMode, templateName, bindingContext, options);
if(renderMode=="replaceNode"){
targetNodeOrNodeArray=renderedNodesArray;
firstTargetNode=getFirstNodeFromPossibleArray(targetNodeOrNodeArray);
}},
null,
{ disposeWhen: whenToDispose, disposeWhenNodeIsRemoved: activelyDisposeWhenNodeIsRemoved }
);
}else{
return ko.memoization.memoize(function (domNode){
ko.renderTemplate(template, dataOrBindingContext, options, domNode, "replaceNode");
});
}};
ko.renderTemplateForEach=function (template, arrayOrObservableArray, options, targetNode, parentBindingContext){
var arrayItemContext;
var executeTemplateForArrayItem=function (arrayValue, index){
arrayItemContext=parentBindingContext['createChildContext'](arrayValue, options['as'], function(context){
context['$index']=index;
});
var templateName=resolveTemplateName(template, arrayValue, arrayItemContext);
return executeTemplate(null, "ignoreTargetNode", templateName, arrayItemContext, options);
}
var activateBindingsCallback=function(arrayValue, addedNodesArray, index){
activateBindingsOnContinuousNodeArray(addedNodesArray, arrayItemContext);
if(options['afterRender'])
options['afterRender'](addedNodesArray, arrayValue);
arrayItemContext=null;
};
return ko.dependentObservable(function (){
var unwrappedArray=ko.utils.unwrapObservable(arrayOrObservableArray)||[];
if(typeof unwrappedArray.length=="undefined")
unwrappedArray=[unwrappedArray];
var filteredArray=ko.utils.arrayFilter(unwrappedArray, function(item){
return options['includeDestroyed']||item===undefined||item===null||!ko.utils.unwrapObservable(item['_destroy']);
});
ko.dependencyDetection.ignore(ko.utils.setDomNodeChildrenFromArrayMapping, null, [targetNode, filteredArray, executeTemplateForArrayItem, options, activateBindingsCallback]);
}, null, { disposeWhenNodeIsRemoved: targetNode });
};
var templateComputedDomDataKey=ko.utils.domData.nextKey();
function disposeOldComputedAndStoreNewOne(element, newComputed){
var oldComputed=ko.utils.domData.get(element, templateComputedDomDataKey);
if(oldComputed&&(typeof(oldComputed.dispose)=='function'))
oldComputed.dispose();
ko.utils.domData.set(element, templateComputedDomDataKey, (newComputed&&newComputed.isActive()) ? newComputed:undefined);
}
ko.bindingHandlers['template']={
'init': function(element, valueAccessor){
var bindingValue=ko.utils.unwrapObservable(valueAccessor());
if(typeof bindingValue=="string"||bindingValue['name']){
ko.virtualElements.emptyNode(element);
}else if('nodes' in bindingValue){
var nodes=bindingValue['nodes']||[];
if(ko.isObservable(nodes)){
throw new Error('The "nodes" option must be a plain, non-observable array.');
}
var container=ko.utils.moveCleanedNodesToContainerElement(nodes);
new ko.templateSources.anonymousTemplate(element)['nodes'](container);
}else{
var templateNodes=ko.virtualElements.childNodes(element),
container=ko.utils.moveCleanedNodesToContainerElement(templateNodes);
new ko.templateSources.anonymousTemplate(element)['nodes'](container);
}
return { 'controlsDescendantBindings': true };},
'update': function (element, valueAccessor, allBindings, viewModel, bindingContext){
var value=valueAccessor(),
options=ko.utils.unwrapObservable(value),
shouldDisplay=true,
templateComputed=null,
templateName;
if(typeof options=="string"){
templateName=value;
options={};}else{
templateName=options['name'];
if('if' in options)
shouldDisplay=ko.utils.unwrapObservable(options['if']);
if(shouldDisplay&&'ifnot' in options)
shouldDisplay = !ko.utils.unwrapObservable(options['ifnot']);
}
if('foreach' in options){
var dataArray=(shouldDisplay&&options['foreach'])||[];
templateComputed=ko.renderTemplateForEach(templateName||element, dataArray, options, element, bindingContext);
}else if(!shouldDisplay){
ko.virtualElements.emptyNode(element);
}else{
var innerBindingContext=('data' in options) ?
bindingContext.createStaticChildContext(options['data'], options['as']) :
bindingContext;
templateComputed=ko.renderTemplate(templateName||element, innerBindingContext, options, element);
}
disposeOldComputedAndStoreNewOne(element, templateComputed);
}};
ko.expressionRewriting.bindingRewriteValidators['template']=function(bindingValue){
var parsedBindingValue=ko.expressionRewriting.parseObjectLiteral(bindingValue);
if((parsedBindingValue.length==1)&&parsedBindingValue[0]['unknown'])
return null;
if(ko.expressionRewriting.keyValueArrayContainsKey(parsedBindingValue, "name"))
return null;
return "This template engine does not support anonymous templates nested within its templates";
};
ko.virtualElements.allowedBindings['template']=true;
})();
ko.exportSymbol('setTemplateEngine', ko.setTemplateEngine);
ko.exportSymbol('renderTemplate', ko.renderTemplate);
ko.utils.findMovesInArrayComparison=function (left, right, limitFailedCompares){
if(left.length&&right.length){
var failedCompares, l, r, leftItem, rightItem;
for (failedCompares=l = 0; (!limitFailedCompares||failedCompares < limitFailedCompares)&&(leftItem=left[l]); ++l){
for (r=0; rightItem=right[r]; ++r){
if(leftItem['value']===rightItem['value']){
leftItem['moved']=rightItem['index'];
rightItem['moved']=leftItem['index'];
right.splice(r, 1);
failedCompares=r = 0;
break;
}}
failedCompares +=r;
}}
};
ko.utils.compareArrays=(function (){
var statusNotInOld='added', statusNotInNew='deleted';
function compareArrays(oldArray, newArray, options){
options=(typeof options==='boolean') ? { 'dontLimitMoves': options }:(options||{});
oldArray=oldArray||[];
newArray=newArray||[];
if(oldArray.length < newArray.length)
return compareSmallArrayToBigArray(oldArray, newArray, statusNotInOld, statusNotInNew, options);
else
return compareSmallArrayToBigArray(newArray, oldArray, statusNotInNew, statusNotInOld, options);
}
function compareSmallArrayToBigArray(smlArray, bigArray, statusNotInSml, statusNotInBig, options){
var myMin=Math.min,
myMax=Math.max,
editDistanceMatrix=[],
smlIndex, smlIndexMax=smlArray.length,
bigIndex, bigIndexMax=bigArray.length,
compareRange=(bigIndexMax - smlIndexMax)||1,
maxDistance=smlIndexMax + bigIndexMax + 1,
thisRow, lastRow,
bigIndexMaxForRow, bigIndexMinForRow;
for (smlIndex=0; smlIndex <=smlIndexMax; smlIndex++){
lastRow=thisRow;
editDistanceMatrix.push(thisRow=[]);
bigIndexMaxForRow=myMin(bigIndexMax, smlIndex + compareRange);
bigIndexMinForRow=myMax(0, smlIndex - 1);
for (bigIndex=bigIndexMinForRow; bigIndex <=bigIndexMaxForRow; bigIndex++){
if(!bigIndex)
thisRow[bigIndex]=smlIndex + 1;
else if(!smlIndex)
thisRow[bigIndex]=bigIndex + 1;
else if(smlArray[smlIndex - 1]===bigArray[bigIndex - 1])
thisRow[bigIndex]=lastRow[bigIndex - 1];
else {
var northDistance=lastRow[bigIndex]||maxDistance;
var westDistance=thisRow[bigIndex - 1]||maxDistance;
thisRow[bigIndex]=myMin(northDistance, westDistance) + 1;
}}
}
var editScript=[], meMinusOne, notInSml=[], notInBig=[];
for (smlIndex=smlIndexMax, bigIndex=bigIndexMax; smlIndex||bigIndex;){
meMinusOne=editDistanceMatrix[smlIndex][bigIndex] - 1;
if(bigIndex&&meMinusOne===editDistanceMatrix[smlIndex][bigIndex-1]){
notInSml.push(editScript[editScript.length]={
'status': statusNotInSml,
'value': bigArray[--bigIndex],
'index': bigIndex });
}else if(smlIndex&&meMinusOne===editDistanceMatrix[smlIndex - 1][bigIndex]){
notInBig.push(editScript[editScript.length]={
'status': statusNotInBig,
'value': smlArray[--smlIndex],
'index': smlIndex });
}else{
--bigIndex;
--smlIndex;
if(!options['sparse']){
editScript.push({
'status': "retained",
'value': bigArray[bigIndex] });
}}
}
ko.utils.findMovesInArrayComparison(notInBig, notInSml, !options['dontLimitMoves']&&smlIndexMax * 10);
return editScript.reverse();
}
return compareArrays;
})();
ko.exportSymbol('utils.compareArrays', ko.utils.compareArrays);
(function (){
function mapNodeAndRefreshWhenChanged(containerNode, mapping, valueToMap, callbackAfterAddingNodes, index){
var mappedNodes=[];
var dependentObservable=ko.dependentObservable(function(){
var newMappedNodes=mapping(valueToMap, index, ko.utils.fixUpContinuousNodeArray(mappedNodes, containerNode))||[];
if(mappedNodes.length > 0){
ko.utils.replaceDomNodes(mappedNodes, newMappedNodes);
if(callbackAfterAddingNodes)
ko.dependencyDetection.ignore(callbackAfterAddingNodes, null, [valueToMap, newMappedNodes, index]);
}
mappedNodes.length=0;
ko.utils.arrayPushAll(mappedNodes, newMappedNodes);
}, null, { disposeWhenNodeIsRemoved: containerNode, disposeWhen: function(){ return !ko.utils.anyDomNodeIsAttachedToDocument(mappedNodes); }});
return { mappedNodes:mappedNodes, dependentObservable:(dependentObservable.isActive() ? dependentObservable:undefined) };}
var lastMappingResultDomDataKey=ko.utils.domData.nextKey(),
deletedItemDummyValue=ko.utils.domData.nextKey();
ko.utils.setDomNodeChildrenFromArrayMapping=function (domNode, array, mapping, options, callbackAfterAddingNodes){
array=array||[];
options=options||{};
var isFirstExecution=ko.utils.domData.get(domNode, lastMappingResultDomDataKey)===undefined;
var lastMappingResult=ko.utils.domData.get(domNode, lastMappingResultDomDataKey)||[];
var lastArray=ko.utils.arrayMap(lastMappingResult, function (x){ return x.arrayEntry; });
var editScript=ko.utils.compareArrays(lastArray, array, options['dontLimitMoves']);
var newMappingResult=[];
var lastMappingResultIndex=0;
var newMappingResultIndex=0;
var nodesToDelete=[];
var itemsToProcess=[];
var itemsForBeforeRemoveCallbacks=[];
var itemsForMoveCallbacks=[];
var itemsForAfterAddCallbacks=[];
var mapData;
function itemMovedOrRetained(editScriptIndex, oldPosition){
mapData=lastMappingResult[oldPosition];
if(newMappingResultIndex!==oldPosition)
itemsForMoveCallbacks[editScriptIndex]=mapData;
mapData.indexObservable(newMappingResultIndex++);
ko.utils.fixUpContinuousNodeArray(mapData.mappedNodes, domNode);
newMappingResult.push(mapData);
itemsToProcess.push(mapData);
}
function callCallback(callback, items){
if(callback){
for (var i=0, n=items.length; i < n; i++){
if(items[i]){
ko.utils.arrayForEach(items[i].mappedNodes, function(node){
callback(node, i, items[i].arrayEntry);
});
}}
}}
for (var i=0, editScriptItem, movedIndex; editScriptItem=editScript[i]; i++){
movedIndex=editScriptItem['moved'];
switch (editScriptItem['status']){
case "deleted":
if(movedIndex===undefined){
mapData=lastMappingResult[lastMappingResultIndex];
if(mapData.dependentObservable){
mapData.dependentObservable.dispose();
mapData.dependentObservable=undefined;
}
if(ko.utils.fixUpContinuousNodeArray(mapData.mappedNodes, domNode).length){
if(options['beforeRemove']){
newMappingResult.push(mapData);
itemsToProcess.push(mapData);
if(mapData.arrayEntry===deletedItemDummyValue){
mapData=null;
}else{
itemsForBeforeRemoveCallbacks[i]=mapData;
}}
if(mapData){
nodesToDelete.push.apply(nodesToDelete, mapData.mappedNodes);
}}
}
lastMappingResultIndex++;
break;
case "retained":
itemMovedOrRetained(i, lastMappingResultIndex++);
break;
case "added":
if(movedIndex!==undefined){
itemMovedOrRetained(i, movedIndex);
}else{
mapData={ arrayEntry: editScriptItem['value'], indexObservable: ko.observable(newMappingResultIndex++) };
newMappingResult.push(mapData);
itemsToProcess.push(mapData);
if(!isFirstExecution)
itemsForAfterAddCallbacks[i]=mapData;
}
break;
}}
ko.utils.domData.set(domNode, lastMappingResultDomDataKey, newMappingResult);
callCallback(options['beforeMove'], itemsForMoveCallbacks);
ko.utils.arrayForEach(nodesToDelete, options['beforeRemove'] ? ko.cleanNode:ko.removeNode);
for (var i=0, nextNode=ko.virtualElements.firstChild(domNode), lastNode, node; mapData=itemsToProcess[i]; i++){
if(!mapData.mappedNodes)
ko.utils.extend(mapData, mapNodeAndRefreshWhenChanged(domNode, mapping, mapData.arrayEntry, callbackAfterAddingNodes, mapData.indexObservable));
for (var j=0; node=mapData.mappedNodes[j]; nextNode=node.nextSibling, lastNode=node, j++){
if(node!==nextNode)
ko.virtualElements.insertAfter(domNode, node, lastNode);
}
if(!mapData.initialized&&callbackAfterAddingNodes){
callbackAfterAddingNodes(mapData.arrayEntry, mapData.mappedNodes, mapData.indexObservable);
mapData.initialized=true;
}}
callCallback(options['beforeRemove'], itemsForBeforeRemoveCallbacks);
for (i=0; i < itemsForBeforeRemoveCallbacks.length; ++i){
if(itemsForBeforeRemoveCallbacks[i]){
itemsForBeforeRemoveCallbacks[i].arrayEntry=deletedItemDummyValue;
}}
callCallback(options['afterMove'], itemsForMoveCallbacks);
callCallback(options['afterAdd'], itemsForAfterAddCallbacks);
}})();
ko.exportSymbol('utils.setDomNodeChildrenFromArrayMapping', ko.utils.setDomNodeChildrenFromArrayMapping);
ko.nativeTemplateEngine=function (){
this['allowTemplateRewriting']=false;
}
ko.nativeTemplateEngine.prototype=new ko.templateEngine();
ko.nativeTemplateEngine.prototype.constructor=ko.nativeTemplateEngine;
ko.nativeTemplateEngine.prototype['renderTemplateSource']=function (templateSource, bindingContext, options, templateDocument){
var useNodesIfAvailable = !(ko.utils.ieVersion < 9),
templateNodesFunc=useNodesIfAvailable ? templateSource['nodes']:null,
templateNodes=templateNodesFunc ? templateSource['nodes']():null;
if(templateNodes){
return ko.utils.makeArray(templateNodes.cloneNode(true).childNodes);
}else{
var templateText=templateSource['text']();
return ko.utils.parseHtmlFragment(templateText, templateDocument);
}};
ko.nativeTemplateEngine.instance=new ko.nativeTemplateEngine();
ko.setTemplateEngine(ko.nativeTemplateEngine.instance);
ko.exportSymbol('nativeTemplateEngine', ko.nativeTemplateEngine);
(function(){
ko.jqueryTmplTemplateEngine=function (){
var jQueryTmplVersion=this.jQueryTmplVersion=(function(){
if(!jQueryInstance||!(jQueryInstance['tmpl']))
return 0;
try {
if(jQueryInstance['tmpl']['tag']['tmpl']['open'].toString().indexOf('__') >=0){
return 2;
}} catch(ex){  }
return 1;
})();
function ensureHasReferencedJQueryTemplates(){
if(jQueryTmplVersion < 2)
throw new Error("Your version of jQuery.tmpl is too old. Please upgrade to jQuery.tmpl 1.0.0pre or later.");
}
function executeTemplate(compiledTemplate, data, jQueryTemplateOptions){
return jQueryInstance['tmpl'](compiledTemplate, data, jQueryTemplateOptions);
}
this['renderTemplateSource']=function(templateSource, bindingContext, options, templateDocument){
templateDocument=templateDocument||document;
options=options||{};
ensureHasReferencedJQueryTemplates();
var precompiled=templateSource['data']('precompiled');
if(!precompiled){
var templateText=templateSource['text']()||"";
templateText="{{ko_with $item.koBindingContext}}" + templateText + "{{/ko_with}}";
precompiled=jQueryInstance['template'](null, templateText);
templateSource['data']('precompiled', precompiled);
}
var data=[bindingContext['$data']];
var jQueryTemplateOptions=jQueryInstance['extend']({ 'koBindingContext': bindingContext }, options['templateOptions']);
var resultNodes=executeTemplate(precompiled, data, jQueryTemplateOptions);
resultNodes['appendTo'](templateDocument.createElement("div"));
jQueryInstance['fragments']={};
return resultNodes;
};
this['createJavaScriptEvaluatorBlock']=function(script){
return "{{ko_code ((function(){ return " + script + " })()) }}";
};
this['addTemplate']=function(templateName, templateMarkup){
document.write("<script type='text/html' id='" + templateName + "'>" + templateMarkup + "<" + "/script>");
};
if(jQueryTmplVersion > 0){
jQueryInstance['tmpl']['tag']['ko_code']={
open: "__.push($1||'');"
};
jQueryInstance['tmpl']['tag']['ko_with']={
open: "with($1){",
close: "} "
};}};
ko.jqueryTmplTemplateEngine.prototype=new ko.templateEngine();
ko.jqueryTmplTemplateEngine.prototype.constructor=ko.jqueryTmplTemplateEngine;
var jqueryTmplTemplateEngineInstance=new ko.jqueryTmplTemplateEngine();
if(jqueryTmplTemplateEngineInstance.jQueryTmplVersion > 0)
ko.setTemplateEngine(jqueryTmplTemplateEngineInstance);
ko.exportSymbol('jqueryTmplTemplateEngine', ko.jqueryTmplTemplateEngine);
})();
}));
}());
})();
},{}],3:[function(require,module,exports){
;(function(factory){
'use strict';
if(typeof define==='function'&&define.amd){
define(['jquery'], factory);
}else if(typeof exports!=='undefined'){
module.exports=factory(require('jquery'));
}else{
factory(jQuery);
}}(function($){
'use strict';
var Slick=window.Slick||{};
Slick=(function(){
var instanceUid=0;
function Slick(element, settings){
var _=this, dataSettings;
_.defaults={
accessibility: true,
adaptiveHeight: false,
appendArrows: $(element),
appendDots: $(element),
arrows: true,
asNavFor: null,
prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
autoplay: false,
autoplaySpeed: 3000,
centerMode: false,
centerPadding: '50px',
cssEase: 'ease',
customPaging: function(slider, i){
return $('<button type="button" />').text(i + 1);
},
dots: false,
dotsClass: 'slick-dots',
draggable: true,
easing: 'linear',
edgeFriction: 0.35,
fade: false,
focusOnSelect: false,
focusOnChange: false,
infinite: true,
initialSlide: 0,
lazyLoad: 'ondemand',
mobileFirst: false,
pauseOnHover: true,
pauseOnFocus: true,
pauseOnDotsHover: false,
respondTo: 'window',
responsive: null,
rows: 1,
rtl: false,
slide: '',
slidesPerRow: 1,
slidesToShow: 1,
slidesToScroll: 1,
speed: 500,
swipe: true,
swipeToSlide: false,
touchMove: true,
touchThreshold: 5,
useCSS: true,
useTransform: true,
variableWidth: false,
vertical: false,
verticalSwiping: false,
waitForAnimate: true,
zIndex: 1000
};
_.initials={
animating: false,
dragging: false,
autoPlayTimer: null,
currentDirection: 0,
currentLeft: null,
currentSlide: 0,
direction: 1,
$dots: null,
listWidth: null,
listHeight: null,
loadIndex: 0,
$nextArrow: null,
$prevArrow: null,
scrolling: false,
slideCount: null,
slideWidth: null,
$slideTrack: null,
$slides: null,
sliding: false,
slideOffset: 0,
swipeLeft: null,
swiping: false,
$list: null,
touchObject: {},
transformsEnabled: false,
unslicked: false
};
$.extend(_, _.initials);
_.activeBreakpoint=null;
_.animType=null;
_.animProp=null;
_.breakpoints=[];
_.breakpointSettings=[];
_.cssTransitions=false;
_.focussed=false;
_.interrupted=false;
_.hidden='hidden';
_.paused=true;
_.positionProp=null;
_.respondTo=null;
_.rowCount=1;
_.shouldClick=true;
_.$slider=$(element);
_.$slidesCache=null;
_.transformType=null;
_.transitionType=null;
_.visibilityChange='visibilitychange';
_.windowWidth=0;
_.windowTimer=null;
dataSettings=$(element).data('slick')||{};
_.options=$.extend({}, _.defaults, settings, dataSettings);
_.currentSlide=_.options.initialSlide;
_.originalSettings=_.options;
if(typeof document.mozHidden!=='undefined'){
_.hidden='mozHidden';
_.visibilityChange='mozvisibilitychange';
}else if(typeof document.webkitHidden!=='undefined'){
_.hidden='webkitHidden';
_.visibilityChange='webkitvisibilitychange';
}
_.autoPlay=$.proxy(_.autoPlay, _);
_.autoPlayClear=$.proxy(_.autoPlayClear, _);
_.autoPlayIterator=$.proxy(_.autoPlayIterator, _);
_.changeSlide=$.proxy(_.changeSlide, _);
_.clickHandler=$.proxy(_.clickHandler, _);
_.selectHandler=$.proxy(_.selectHandler, _);
_.setPosition=$.proxy(_.setPosition, _);
_.swipeHandler=$.proxy(_.swipeHandler, _);
_.dragHandler=$.proxy(_.dragHandler, _);
_.keyHandler=$.proxy(_.keyHandler, _);
_.instanceUid=instanceUid++;
_.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/;
_.registerBreakpoints();
_.init(true);
}
return Slick;
}());
Slick.prototype.activateADA=function(){
var _=this;
_.$slideTrack.find('.slick-active').attr({
'aria-hidden': 'false'
}).find('a, input, button, select').attr({
'tabindex': '0'
});
};
Slick.prototype.addSlide=Slick.prototype.slickAdd=function(markup, index, addBefore){
var _=this;
if(typeof(index)==='boolean'){
addBefore=index;
index=null;
}else if(index < 0||(index >=_.slideCount)){
return false;
}
_.unload();
if(typeof(index)==='number'){
if(index===0&&_.$slides.length===0){
$(markup).appendTo(_.$slideTrack);
}else if(addBefore){
$(markup).insertBefore(_.$slides.eq(index));
}else{
$(markup).insertAfter(_.$slides.eq(index));
}}else{
if(addBefore===true){
$(markup).prependTo(_.$slideTrack);
}else{
$(markup).appendTo(_.$slideTrack);
}}
_.$slides=_.$slideTrack.children(this.options.slide);
_.$slideTrack.children(this.options.slide).detach();
_.$slideTrack.append(_.$slides);
_.$slides.each(function(index, element){
$(element).attr('data-slick-index', index);
});
_.$slidesCache=_.$slides;
_.reinit();
};
Slick.prototype.animateHeight=function(){
var _=this;
if(_.options.slidesToShow===1&&_.options.adaptiveHeight===true&&_.options.vertical===false){
var targetHeight=_.$slides.eq(_.currentSlide).outerHeight(true);
_.$list.animate({
height: targetHeight
}, _.options.speed);
}};
Slick.prototype.animateSlide=function(targetLeft, callback){
var animProps={},
_=this;
_.animateHeight();
if(_.options.rtl===true&&_.options.vertical===false){
targetLeft=-targetLeft;
}
if(_.transformsEnabled===false){
if(_.options.vertical===false){
_.$slideTrack.animate({
left: targetLeft
}, _.options.speed, _.options.easing, callback);
}else{
_.$slideTrack.animate({
top: targetLeft
}, _.options.speed, _.options.easing, callback);
}}else{
if(_.cssTransitions===false){
if(_.options.rtl===true){
_.currentLeft=-(_.currentLeft);
}
$({
animStart: _.currentLeft
}).animate({
animStart: targetLeft
}, {
duration: _.options.speed,
easing: _.options.easing,
step: function(now){
now=Math.ceil(now);
if(_.options.vertical===false){
animProps[_.animType]='translate(' +
now + 'px, 0px)';
_.$slideTrack.css(animProps);
}else{
animProps[_.animType]='translate(0px,' +
now + 'px)';
_.$slideTrack.css(animProps);
}},
complete: function(){
if(callback){
callback.call();
}}
});
}else{
_.applyTransition();
targetLeft=Math.ceil(targetLeft);
if(_.options.vertical===false){
animProps[_.animType]='translate3d(' + targetLeft + 'px, 0px, 0px)';
}else{
animProps[_.animType]='translate3d(0px,' + targetLeft + 'px, 0px)';
}
_.$slideTrack.css(animProps);
if(callback){
setTimeout(function(){
_.disableTransition();
callback.call();
}, _.options.speed);
}}
}};
Slick.prototype.getNavTarget=function(){
var _=this,
asNavFor=_.options.asNavFor;
if(asNavFor&&asNavFor!==null){
asNavFor=$(asNavFor).not(_.$slider);
}
return asNavFor;
};
Slick.prototype.asNavFor=function(index){
var _=this,
asNavFor=_.getNavTarget();
if(asNavFor!==null&&typeof asNavFor==='object'){
asNavFor.each(function(){
var target=$(this).slick('getSlick');
if(!target.unslicked){
target.slideHandler(index, true);
}});
}};
Slick.prototype.applyTransition=function(slide){
var _=this,
transition={};
if(_.options.fade===false){
transition[_.transitionType]=_.transformType + ' ' + _.options.speed + 'ms ' + _.options.cssEase;
}else{
transition[_.transitionType]='opacity ' + _.options.speed + 'ms ' + _.options.cssEase;
}
if(_.options.fade===false){
_.$slideTrack.css(transition);
}else{
_.$slides.eq(slide).css(transition);
}};
Slick.prototype.autoPlay=function(){
var _=this;
_.autoPlayClear();
if(_.slideCount > _.options.slidesToShow){
_.autoPlayTimer=setInterval(_.autoPlayIterator, _.options.autoplaySpeed);
}};
Slick.prototype.autoPlayClear=function(){
var _=this;
if(_.autoPlayTimer){
clearInterval(_.autoPlayTimer);
}};
Slick.prototype.autoPlayIterator=function(){
var _=this,
slideTo=_.currentSlide + _.options.slidesToScroll;
if(!_.paused&&!_.interrupted&&!_.focussed){
if(_.options.infinite===false){
if(_.direction===1&&(_.currentSlide + 1)===(_.slideCount - 1)){
_.direction=0;
}
else if(_.direction===0){
slideTo=_.currentSlide - _.options.slidesToScroll;
if(_.currentSlide - 1===0){
_.direction=1;
}}
}
_.slideHandler(slideTo);
}};
Slick.prototype.buildArrows=function(){
var _=this;
if(_.options.arrows===true){
_.$prevArrow=$(_.options.prevArrow).addClass('slick-arrow');
_.$nextArrow=$(_.options.nextArrow).addClass('slick-arrow');
if(_.slideCount > _.options.slidesToShow){
_.$prevArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');
_.$nextArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');
if(_.htmlExpr.test(_.options.prevArrow)){
_.$prevArrow.prependTo(_.options.appendArrows);
}
if(_.htmlExpr.test(_.options.nextArrow)){
_.$nextArrow.appendTo(_.options.appendArrows);
}
if(_.options.infinite!==true){
_.$prevArrow
.addClass('slick-disabled')
.attr('aria-disabled', 'true');
}}else{
_.$prevArrow.add(_.$nextArrow)
.addClass('slick-hidden')
.attr({
'aria-disabled': 'true',
'tabindex': '-1'
});
}}
};
Slick.prototype.buildDots=function(){
var _=this,
i, dot;
if(_.options.dots===true&&_.slideCount > _.options.slidesToShow){
_.$slider.addClass('slick-dotted');
dot=$('<ul />').addClass(_.options.dotsClass);
for (i=0; i <=_.getDotCount(); i +=1){
dot.append($('<li />').append(_.options.customPaging.call(this, _, i)));
}
_.$dots=dot.appendTo(_.options.appendDots);
_.$dots.find('li').first().addClass('slick-active');
}};
Slick.prototype.buildOut=function(){
var _=this;
_.$slides =
_.$slider
.children(_.options.slide + ':not(.slick-cloned)')
.addClass('slick-slide');
_.slideCount=_.$slides.length;
_.$slides.each(function(index, element){
$(element)
.attr('data-slick-index', index)
.data('originalStyling', $(element).attr('style')||'');
});
_.$slider.addClass('slick-slider');
_.$slideTrack=(_.slideCount===0) ?
$('<div class="slick-track"/>').appendTo(_.$slider) :
_.$slides.wrapAll('<div class="slick-track"/>').parent();
_.$list=_.$slideTrack.wrap('<div class="slick-list"/>').parent();
_.$slideTrack.css('opacity', 0);
if(_.options.centerMode===true||_.options.swipeToSlide===true){
_.options.slidesToScroll=1;
}
$('img[data-lazy]', _.$slider).not('[src]').addClass('slick-loading');
_.setupInfinite();
_.buildArrows();
_.buildDots();
_.updateDots();
_.setSlideClasses(typeof _.currentSlide==='number' ? _.currentSlide:0);
if(_.options.draggable===true){
_.$list.addClass('draggable');
}};
Slick.prototype.buildRows=function(){
var _=this, a, b, c, newSlides, numOfSlides, originalSlides,slidesPerSection;
newSlides=document.createDocumentFragment();
originalSlides=_.$slider.children();
if(_.options.rows > 0){
slidesPerSection=_.options.slidesPerRow * _.options.rows;
numOfSlides=Math.ceil(originalSlides.length / slidesPerSection
);
for(a=0; a < numOfSlides; a++){
var slide=document.createElement('div');
for(b=0; b < _.options.rows; b++){
var row=document.createElement('div');
for(c=0; c < _.options.slidesPerRow; c++){
var target=(a * slidesPerSection + ((b * _.options.slidesPerRow) + c));
if(originalSlides.get(target)){
row.appendChild(originalSlides.get(target));
}}
slide.appendChild(row);
}
newSlides.appendChild(slide);
}
_.$slider.empty().append(newSlides);
_.$slider.children().children().children()
.css({
'width':(100 / _.options.slidesPerRow) + '%',
'display': 'inline-block'
});
}};
Slick.prototype.checkResponsive=function(initial, forceUpdate){
var _=this,
breakpoint, targetBreakpoint, respondToWidth, triggerBreakpoint=false;
var sliderWidth=_.$slider.width();
var windowWidth=window.innerWidth||$(window).width();
if(_.respondTo==='window'){
respondToWidth=windowWidth;
}else if(_.respondTo==='slider'){
respondToWidth=sliderWidth;
}else if(_.respondTo==='min'){
respondToWidth=Math.min(windowWidth, sliderWidth);
}
if(_.options.responsive &&
_.options.responsive.length &&
_.options.responsive!==null){
targetBreakpoint=null;
for (breakpoint in _.breakpoints){
if(_.breakpoints.hasOwnProperty(breakpoint)){
if(_.originalSettings.mobileFirst===false){
if(respondToWidth < _.breakpoints[breakpoint]){
targetBreakpoint=_.breakpoints[breakpoint];
}}else{
if(respondToWidth > _.breakpoints[breakpoint]){
targetBreakpoint=_.breakpoints[breakpoint];
}}
}}
if(targetBreakpoint!==null){
if(_.activeBreakpoint!==null){
if(targetBreakpoint!==_.activeBreakpoint||forceUpdate){
_.activeBreakpoint =
targetBreakpoint;
if(_.breakpointSettings[targetBreakpoint]==='unslick'){
_.unslick(targetBreakpoint);
}else{
_.options=$.extend({}, _.originalSettings,
_.breakpointSettings[
targetBreakpoint]);
if(initial===true){
_.currentSlide=_.options.initialSlide;
}
_.refresh(initial);
}
triggerBreakpoint=targetBreakpoint;
}}else{
_.activeBreakpoint=targetBreakpoint;
if(_.breakpointSettings[targetBreakpoint]==='unslick'){
_.unslick(targetBreakpoint);
}else{
_.options=$.extend({}, _.originalSettings,
_.breakpointSettings[
targetBreakpoint]);
if(initial===true){
_.currentSlide=_.options.initialSlide;
}
_.refresh(initial);
}
triggerBreakpoint=targetBreakpoint;
}}else{
if(_.activeBreakpoint!==null){
_.activeBreakpoint=null;
_.options=_.originalSettings;
if(initial===true){
_.currentSlide=_.options.initialSlide;
}
_.refresh(initial);
triggerBreakpoint=targetBreakpoint;
}}
if(!initial&&triggerBreakpoint!==false){
_.$slider.trigger('breakpoint', [_, triggerBreakpoint]);
}}
};
Slick.prototype.changeSlide=function(event, dontAnimate){
var _=this,
$target=$(event.currentTarget),
indexOffset, slideOffset, unevenOffset;
if($target.is('a')){
event.preventDefault();
}
if(!$target.is('li')){
$target=$target.closest('li');
}
unevenOffset=(_.slideCount % _.options.slidesToScroll!==0);
indexOffset=unevenOffset ? 0:(_.slideCount - _.currentSlide) % _.options.slidesToScroll;
switch (event.data.message){
case 'previous':
slideOffset=indexOffset===0 ? _.options.slidesToScroll:_.options.slidesToShow - indexOffset;
if(_.slideCount > _.options.slidesToShow){
_.slideHandler(_.currentSlide - slideOffset, false, dontAnimate);
}
break;
case 'next':
slideOffset=indexOffset===0 ? _.options.slidesToScroll:indexOffset;
if(_.slideCount > _.options.slidesToShow){
_.slideHandler(_.currentSlide + slideOffset, false, dontAnimate);
}
break;
case 'index':
var index=event.data.index===0 ? 0 :
event.data.index||$target.index() * _.options.slidesToScroll;
_.slideHandler(_.checkNavigable(index), false, dontAnimate);
$target.children().trigger('focus');
break;
default:
return;
}};
Slick.prototype.checkNavigable=function(index){
var _=this,
navigables, prevNavigable;
navigables=_.getNavigableIndexes();
prevNavigable=0;
if(index > navigables[navigables.length - 1]){
index=navigables[navigables.length - 1];
}else{
for (var n in navigables){
if(index < navigables[n]){
index=prevNavigable;
break;
}
prevNavigable=navigables[n];
}}
return index;
};
Slick.prototype.cleanUpEvents=function(){
var _=this;
if(_.options.dots&&_.$dots!==null){
$('li', _.$dots)
.off('click.slick', _.changeSlide)
.off('mouseenter.slick', $.proxy(_.interrupt, _, true))
.off('mouseleave.slick', $.proxy(_.interrupt, _, false));
if(_.options.accessibility===true){
_.$dots.off('keydown.slick', _.keyHandler);
}}
_.$slider.off('focus.slick blur.slick');
if(_.options.arrows===true&&_.slideCount > _.options.slidesToShow){
_.$prevArrow&&_.$prevArrow.off('click.slick', _.changeSlide);
_.$nextArrow&&_.$nextArrow.off('click.slick', _.changeSlide);
if(_.options.accessibility===true){
_.$prevArrow&&_.$prevArrow.off('keydown.slick', _.keyHandler);
_.$nextArrow&&_.$nextArrow.off('keydown.slick', _.keyHandler);
}}
_.$list.off('touchstart.slick mousedown.slick', _.swipeHandler);
_.$list.off('touchmove.slick mousemove.slick', _.swipeHandler);
_.$list.off('touchend.slick mouseup.slick', _.swipeHandler);
_.$list.off('touchcancel.slick mouseleave.slick', _.swipeHandler);
_.$list.off('click.slick', _.clickHandler);
$(document).off(_.visibilityChange, _.visibility);
_.cleanUpSlideEvents();
if(_.options.accessibility===true){
_.$list.off('keydown.slick', _.keyHandler);
}
if(_.options.focusOnSelect===true){
$(_.$slideTrack).children().off('click.slick', _.selectHandler);
}
$(window).off('orientationchange.slick.slick-' + _.instanceUid, _.orientationChange);
$(window).off('resize.slick.slick-' + _.instanceUid, _.resize);
$('[draggable!=true]', _.$slideTrack).off('dragstart', _.preventDefault);
$(window).off('load.slick.slick-' + _.instanceUid, _.setPosition);
};
Slick.prototype.cleanUpSlideEvents=function(){
var _=this;
_.$list.off('mouseenter.slick', $.proxy(_.interrupt, _, true));
_.$list.off('mouseleave.slick', $.proxy(_.interrupt, _, false));
};
Slick.prototype.cleanUpRows=function(){
var _=this, originalSlides;
if(_.options.rows > 0){
originalSlides=_.$slides.children().children();
originalSlides.removeAttr('style');
_.$slider.empty().append(originalSlides);
}};
Slick.prototype.clickHandler=function(event){
var _=this;
if(_.shouldClick===false){
event.stopImmediatePropagation();
event.stopPropagation();
event.preventDefault();
}};
Slick.prototype.destroy=function(refresh){
var _=this;
_.autoPlayClear();
_.touchObject={};
_.cleanUpEvents();
$('.slick-cloned', _.$slider).detach();
if(_.$dots){
_.$dots.remove();
}
if(_.$prevArrow&&_.$prevArrow.length){
_.$prevArrow
.removeClass('slick-disabled slick-arrow slick-hidden')
.removeAttr('aria-hidden aria-disabled tabindex')
.css('display','');
if(_.htmlExpr.test(_.options.prevArrow)){
_.$prevArrow.remove();
}}
if(_.$nextArrow&&_.$nextArrow.length){
_.$nextArrow
.removeClass('slick-disabled slick-arrow slick-hidden')
.removeAttr('aria-hidden aria-disabled tabindex')
.css('display','');
if(_.htmlExpr.test(_.options.nextArrow)){
_.$nextArrow.remove();
}}
if(_.$slides){
_.$slides
.removeClass('slick-slide slick-active slick-center slick-visible slick-current')
.removeAttr('aria-hidden')
.removeAttr('data-slick-index')
.each(function(){
$(this).attr('style', $(this).data('originalStyling'));
});
_.$slideTrack.children(this.options.slide).detach();
_.$slideTrack.detach();
_.$list.detach();
_.$slider.append(_.$slides);
}
_.cleanUpRows();
_.$slider.removeClass('slick-slider');
_.$slider.removeClass('slick-initialized');
_.$slider.removeClass('slick-dotted');
_.unslicked=true;
if(!refresh){
_.$slider.trigger('destroy', [_]);
}};
Slick.prototype.disableTransition=function(slide){
var _=this,
transition={};
transition[_.transitionType]='';
if(_.options.fade===false){
_.$slideTrack.css(transition);
}else{
_.$slides.eq(slide).css(transition);
}};
Slick.prototype.fadeSlide=function(slideIndex, callback){
var _=this;
if(_.cssTransitions===false){
_.$slides.eq(slideIndex).css({
zIndex: _.options.zIndex
});
_.$slides.eq(slideIndex).animate({
opacity: 1
}, _.options.speed, _.options.easing, callback);
}else{
_.applyTransition(slideIndex);
_.$slides.eq(slideIndex).css({
opacity: 1,
zIndex: _.options.zIndex
});
if(callback){
setTimeout(function(){
_.disableTransition(slideIndex);
callback.call();
}, _.options.speed);
}}
};
Slick.prototype.fadeSlideOut=function(slideIndex){
var _=this;
if(_.cssTransitions===false){
_.$slides.eq(slideIndex).animate({
opacity: 0,
zIndex: _.options.zIndex - 2
}, _.options.speed, _.options.easing);
}else{
_.applyTransition(slideIndex);
_.$slides.eq(slideIndex).css({
opacity: 0,
zIndex: _.options.zIndex - 2
});
}};
Slick.prototype.filterSlides=Slick.prototype.slickFilter=function(filter){
var _=this;
if(filter!==null){
_.$slidesCache=_.$slides;
_.unload();
_.$slideTrack.children(this.options.slide).detach();
_.$slidesCache.filter(filter).appendTo(_.$slideTrack);
_.reinit();
}};
Slick.prototype.focusHandler=function(){
var _=this;
_.$slider
.off('focus.slick blur.slick')
.on('focus.slick blur.slick', '*', function(event){
event.stopImmediatePropagation();
var $sf=$(this);
setTimeout(function(){
if(_.options.pauseOnFocus){
_.focussed=$sf.is(':focus');
_.autoPlay();
}}, 0);
});
};
Slick.prototype.getCurrent=Slick.prototype.slickCurrentSlide=function(){
var _=this;
return _.currentSlide;
};
Slick.prototype.getDotCount=function(){
var _=this;
var breakPoint=0;
var counter=0;
var pagerQty=0;
if(_.options.infinite===true){
if(_.slideCount <=_.options.slidesToShow){
++pagerQty;
}else{
while (breakPoint < _.slideCount){
++pagerQty;
breakPoint=counter + _.options.slidesToScroll;
counter +=_.options.slidesToScroll <=_.options.slidesToShow ? _.options.slidesToScroll:_.options.slidesToShow;
}}
}else if(_.options.centerMode===true){
pagerQty=_.slideCount;
}else if(!_.options.asNavFor){
pagerQty=1 + Math.ceil((_.slideCount - _.options.slidesToShow) / _.options.slidesToScroll);
}else{
while (breakPoint < _.slideCount){
++pagerQty;
breakPoint=counter + _.options.slidesToScroll;
counter +=_.options.slidesToScroll <=_.options.slidesToShow ? _.options.slidesToScroll:_.options.slidesToShow;
}}
return pagerQty - 1;
};
Slick.prototype.getLeft=function(slideIndex){
var _=this,
targetLeft,
verticalHeight,
verticalOffset=0,
targetSlide,
coef;
_.slideOffset=0;
verticalHeight=_.$slides.first().outerHeight(true);
if(_.options.infinite===true){
if(_.slideCount > _.options.slidesToShow){
_.slideOffset=(_.slideWidth * _.options.slidesToShow) * -1;
coef=-1
if(_.options.vertical===true&&_.options.centerMode===true){
if(_.options.slidesToShow===2){
coef=-1.5;
}else if(_.options.slidesToShow===1){
coef=-2
}}
verticalOffset=(verticalHeight * _.options.slidesToShow) * coef;
}
if(_.slideCount % _.options.slidesToScroll!==0){
if(slideIndex + _.options.slidesToScroll > _.slideCount&&_.slideCount > _.options.slidesToShow){
if(slideIndex > _.slideCount){
_.slideOffset=((_.options.slidesToShow - (slideIndex - _.slideCount)) * _.slideWidth) * -1;
verticalOffset=((_.options.slidesToShow - (slideIndex - _.slideCount)) * verticalHeight) * -1;
}else{
_.slideOffset=((_.slideCount % _.options.slidesToScroll) * _.slideWidth) * -1;
verticalOffset=((_.slideCount % _.options.slidesToScroll) * verticalHeight) * -1;
}}
}}else{
if(slideIndex + _.options.slidesToShow > _.slideCount){
_.slideOffset=((slideIndex + _.options.slidesToShow) - _.slideCount) * _.slideWidth;
verticalOffset=((slideIndex + _.options.slidesToShow) - _.slideCount) * verticalHeight;
}}
if(_.slideCount <=_.options.slidesToShow){
_.slideOffset=0;
verticalOffset=0;
}
if(_.options.centerMode===true&&_.slideCount <=_.options.slidesToShow){
_.slideOffset=((_.slideWidth * Math.floor(_.options.slidesToShow)) / 2) - ((_.slideWidth * _.slideCount) / 2);
}else if(_.options.centerMode===true&&_.options.infinite===true){
_.slideOffset +=_.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
}else if(_.options.centerMode===true){
_.slideOffset=0;
_.slideOffset +=_.slideWidth * Math.floor(_.options.slidesToShow / 2);
}
if(_.options.vertical===false){
targetLeft=((slideIndex * _.slideWidth) * -1) + _.slideOffset;
}else{
targetLeft=((slideIndex * verticalHeight) * -1) + verticalOffset;
}
if(_.options.variableWidth===true){
if(_.slideCount <=_.options.slidesToShow||_.options.infinite===false){
targetSlide=_.$slideTrack.children('.slick-slide').eq(slideIndex);
}else{
targetSlide=_.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow);
}
if(_.options.rtl===true){
if(targetSlide[0]){
targetLeft=(_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
}else{
targetLeft=0;
}}else{
targetLeft=targetSlide[0] ? targetSlide[0].offsetLeft * -1:0;
}
if(_.options.centerMode===true){
if(_.slideCount <=_.options.slidesToShow||_.options.infinite===false){
targetSlide=_.$slideTrack.children('.slick-slide').eq(slideIndex);
}else{
targetSlide=_.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow + 1);
}
if(_.options.rtl===true){
if(targetSlide[0]){
targetLeft=(_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
}else{
targetLeft=0;
}}else{
targetLeft=targetSlide[0] ? targetSlide[0].offsetLeft * -1:0;
}
targetLeft +=(_.$list.width() - targetSlide.outerWidth()) / 2;
}}
return targetLeft;
};
Slick.prototype.getOption=Slick.prototype.slickGetOption=function(option){
var _=this;
return _.options[option];
};
Slick.prototype.getNavigableIndexes=function(){
var _=this,
breakPoint=0,
counter=0,
indexes=[],
max;
if(_.options.infinite===false){
max=_.slideCount;
}else{
breakPoint=_.options.slidesToScroll * -1;
counter=_.options.slidesToScroll * -1;
max=_.slideCount * 2;
}
while (breakPoint < max){
indexes.push(breakPoint);
breakPoint=counter + _.options.slidesToScroll;
counter +=_.options.slidesToScroll <=_.options.slidesToShow ? _.options.slidesToScroll:_.options.slidesToShow;
}
return indexes;
};
Slick.prototype.getSlick=function(){
return this;
};
Slick.prototype.getSlideCount=function(){
var _=this,
slidesTraversed, swipedSlide, centerOffset;
centerOffset=_.options.centerMode===true ? _.slideWidth * Math.floor(_.options.slidesToShow / 2):0;
if(_.options.swipeToSlide===true){
_.$slideTrack.find('.slick-slide').each(function(index, slide){
if(slide.offsetLeft - centerOffset + ($(slide).outerWidth() / 2) > (_.swipeLeft * -1)){
swipedSlide=slide;
return false;
}});
slidesTraversed=Math.abs($(swipedSlide).attr('data-slick-index') - _.currentSlide)||1;
return slidesTraversed;
}else{
return _.options.slidesToScroll;
}};
Slick.prototype.goTo=Slick.prototype.slickGoTo=function(slide, dontAnimate){
var _=this;
_.changeSlide({
data: {
message: 'index',
index: parseInt(slide)
}}, dontAnimate);
};
Slick.prototype.init=function(creation){
var _=this;
if(!$(_.$slider).hasClass('slick-initialized')){
$(_.$slider).addClass('slick-initialized');
_.buildRows();
_.buildOut();
_.setProps();
_.startLoad();
_.loadSlider();
_.initializeEvents();
_.updateArrows();
_.updateDots();
_.checkResponsive(true);
_.focusHandler();
}
if(creation){
_.$slider.trigger('init', [_]);
}
if(_.options.accessibility===true){
_.initADA();
}
if(_.options.autoplay){
_.paused=false;
_.autoPlay();
}};
Slick.prototype.initADA=function(){
var _=this,
numDotGroups=Math.ceil(_.slideCount / _.options.slidesToShow),
tabControlIndexes=_.getNavigableIndexes().filter(function(val){
return (val >=0)&&(val < _.slideCount);
});
_.$slides.add(_.$slideTrack.find('.slick-cloned')).attr({
'aria-hidden': 'true',
'tabindex': '-1'
}).find('a, input, button, select').attr({
'tabindex': '-1'
});
if(_.$dots!==null){
_.$slides.not(_.$slideTrack.find('.slick-cloned')).each(function(i){
var slideControlIndex=tabControlIndexes.indexOf(i);
$(this).attr({
'role': 'tabpanel',
'id': 'slick-slide' + _.instanceUid + i,
'tabindex': -1
});
if(slideControlIndex!==-1){
var ariaButtonControl='slick-slide-control' + _.instanceUid + slideControlIndex
if($('#' + ariaButtonControl).length){
$(this).attr({
'aria-describedby': ariaButtonControl
});
}}
});
_.$dots.attr('role', 'tablist').find('li').each(function(i){
var mappedSlideIndex=tabControlIndexes[i];
$(this).attr({
'role': 'presentation'
});
$(this).find('button').first().attr({
'role': 'tab',
'id': 'slick-slide-control' + _.instanceUid + i,
'aria-controls': 'slick-slide' + _.instanceUid + mappedSlideIndex,
'aria-label': (i + 1) + ' of ' + numDotGroups,
'aria-selected': null,
'tabindex': '-1'
});
}).eq(_.currentSlide).find('button').attr({
'aria-selected': 'true',
'tabindex': '0'
}).end();
}
for (var i=_.currentSlide, max=i+_.options.slidesToShow; i < max; i++){
if(_.options.focusOnChange){
_.$slides.eq(i).attr({'tabindex': '0'});
}else{
_.$slides.eq(i).removeAttr('tabindex');
}}
_.activateADA();
};
Slick.prototype.initArrowEvents=function(){
var _=this;
if(_.options.arrows===true&&_.slideCount > _.options.slidesToShow){
_.$prevArrow
.off('click.slick')
.on('click.slick', {
message: 'previous'
}, _.changeSlide);
_.$nextArrow
.off('click.slick')
.on('click.slick', {
message: 'next'
}, _.changeSlide);
if(_.options.accessibility===true){
_.$prevArrow.on('keydown.slick', _.keyHandler);
_.$nextArrow.on('keydown.slick', _.keyHandler);
}}
};
Slick.prototype.initDotEvents=function(){
var _=this;
if(_.options.dots===true&&_.slideCount > _.options.slidesToShow){
$('li', _.$dots).on('click.slick', {
message: 'index'
}, _.changeSlide);
if(_.options.accessibility===true){
_.$dots.on('keydown.slick', _.keyHandler);
}}
if(_.options.dots===true&&_.options.pauseOnDotsHover===true&&_.slideCount > _.options.slidesToShow){
$('li', _.$dots)
.on('mouseenter.slick', $.proxy(_.interrupt, _, true))
.on('mouseleave.slick', $.proxy(_.interrupt, _, false));
}};
Slick.prototype.initSlideEvents=function(){
var _=this;
if(_.options.pauseOnHover){
_.$list.on('mouseenter.slick', $.proxy(_.interrupt, _, true));
_.$list.on('mouseleave.slick', $.proxy(_.interrupt, _, false));
}};
Slick.prototype.initializeEvents=function(){
var _=this;
_.initArrowEvents();
_.initDotEvents();
_.initSlideEvents();
_.$list.on('touchstart.slick mousedown.slick', {
action: 'start'
}, _.swipeHandler);
_.$list.on('touchmove.slick mousemove.slick', {
action: 'move'
}, _.swipeHandler);
_.$list.on('touchend.slick mouseup.slick', {
action: 'end'
}, _.swipeHandler);
_.$list.on('touchcancel.slick mouseleave.slick', {
action: 'end'
}, _.swipeHandler);
_.$list.on('click.slick', _.clickHandler);
$(document).on(_.visibilityChange, $.proxy(_.visibility, _));
if(_.options.accessibility===true){
_.$list.on('keydown.slick', _.keyHandler);
}
if(_.options.focusOnSelect===true){
$(_.$slideTrack).children().on('click.slick', _.selectHandler);
}
$(window).on('orientationchange.slick.slick-' + _.instanceUid, $.proxy(_.orientationChange, _));
$(window).on('resize.slick.slick-' + _.instanceUid, $.proxy(_.resize, _));
$('[draggable!=true]', _.$slideTrack).on('dragstart', _.preventDefault);
$(window).on('load.slick.slick-' + _.instanceUid, _.setPosition);
$(_.setPosition);
};
Slick.prototype.initUI=function(){
var _=this;
if(_.options.arrows===true&&_.slideCount > _.options.slidesToShow){
_.$prevArrow.show();
_.$nextArrow.show();
}
if(_.options.dots===true&&_.slideCount > _.options.slidesToShow){
_.$dots.show();
}};
Slick.prototype.keyHandler=function(event){
var _=this;
if(!event.target.tagName.match('TEXTAREA|INPUT|SELECT')){
if(event.keyCode===37&&_.options.accessibility===true){
_.changeSlide({
data: {
message: _.options.rtl===true ? 'next':'previous'
}});
}else if(event.keyCode===39&&_.options.accessibility===true){
_.changeSlide({
data: {
message: _.options.rtl===true ? 'previous':'next'
}});
}}
};
Slick.prototype.lazyLoad=function(){
var _=this,
loadRange, cloneRange, rangeStart, rangeEnd;
function loadImages(imagesScope){
$('img[data-lazy]', imagesScope).each(function(){
var image=$(this),
imageSource=$(this).attr('data-lazy'),
imageSrcSet=$(this).attr('data-srcset'),
imageSizes=$(this).attr('data-sizes')||_.$slider.attr('data-sizes'),
imageToLoad=document.createElement('img');
imageToLoad.onload=function(){
image
.animate({ opacity: 0 }, 100, function(){
if(imageSrcSet){
image
.attr('srcset', imageSrcSet);
if(imageSizes){
image
.attr('sizes', imageSizes);
}}
image
.attr('src', imageSource)
.animate({ opacity: 1 }, 200, function(){
image
.removeAttr('data-lazy data-srcset data-sizes')
.removeClass('slick-loading');
});
_.$slider.trigger('lazyLoaded', [_, image, imageSource]);
});
};
imageToLoad.onerror=function(){
image
.removeAttr('data-lazy')
.removeClass('slick-loading')
.addClass('slick-lazyload-error');
_.$slider.trigger('lazyLoadError', [ _, image, imageSource ]);
};
imageToLoad.src=imageSource;
});
}
if(_.options.centerMode===true){
if(_.options.infinite===true){
rangeStart=_.currentSlide + (_.options.slidesToShow / 2 + 1);
rangeEnd=rangeStart + _.options.slidesToShow + 2;
}else{
rangeStart=Math.max(0, _.currentSlide - (_.options.slidesToShow / 2 + 1));
rangeEnd=2 + (_.options.slidesToShow / 2 + 1) + _.currentSlide;
}}else{
rangeStart=_.options.infinite ? _.options.slidesToShow + _.currentSlide:_.currentSlide;
rangeEnd=Math.ceil(rangeStart + _.options.slidesToShow);
if(_.options.fade===true){
if(rangeStart > 0) rangeStart--;
if(rangeEnd <=_.slideCount) rangeEnd++;
}}
loadRange=_.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);
if(_.options.lazyLoad==='anticipated'){
var prevSlide=rangeStart - 1,
nextSlide=rangeEnd,
$slides=_.$slider.find('.slick-slide');
for (var i=0; i < _.options.slidesToScroll; i++){
if(prevSlide < 0) prevSlide=_.slideCount - 1;
loadRange=loadRange.add($slides.eq(prevSlide));
loadRange=loadRange.add($slides.eq(nextSlide));
prevSlide--;
nextSlide++;
}}
loadImages(loadRange);
if(_.slideCount <=_.options.slidesToShow){
cloneRange=_.$slider.find('.slick-slide');
loadImages(cloneRange);
} else
if(_.currentSlide >=_.slideCount - _.options.slidesToShow){
cloneRange=_.$slider.find('.slick-cloned').slice(0, _.options.slidesToShow);
loadImages(cloneRange);
}else if(_.currentSlide===0){
cloneRange=_.$slider.find('.slick-cloned').slice(_.options.slidesToShow * -1);
loadImages(cloneRange);
}};
Slick.prototype.loadSlider=function(){
var _=this;
_.setPosition();
_.$slideTrack.css({
opacity: 1
});
_.$slider.removeClass('slick-loading');
_.initUI();
if(_.options.lazyLoad==='progressive'){
_.progressiveLazyLoad();
}};
Slick.prototype.next=Slick.prototype.slickNext=function(){
var _=this;
_.changeSlide({
data: {
message: 'next'
}});
};
Slick.prototype.orientationChange=function(){
var _=this;
_.checkResponsive();
_.setPosition();
};
Slick.prototype.pause=Slick.prototype.slickPause=function(){
var _=this;
_.autoPlayClear();
_.paused=true;
};
Slick.prototype.play=Slick.prototype.slickPlay=function(){
var _=this;
_.autoPlay();
_.options.autoplay=true;
_.paused=false;
_.focussed=false;
_.interrupted=false;
};
Slick.prototype.postSlide=function(index){
var _=this;
if(!_.unslicked){
_.$slider.trigger('afterChange', [_, index]);
_.animating=false;
if(_.slideCount > _.options.slidesToShow){
_.setPosition();
}
_.swipeLeft=null;
if(_.options.autoplay){
_.autoPlay();
}
if(_.options.accessibility===true){
_.initADA();
if(_.options.focusOnChange){
var $currentSlide=$(_.$slides.get(_.currentSlide));
$currentSlide.attr('tabindex', 0).focus();
}}
}};
Slick.prototype.prev=Slick.prototype.slickPrev=function(){
var _=this;
_.changeSlide({
data: {
message: 'previous'
}});
};
Slick.prototype.preventDefault=function(event){
event.preventDefault();
};
Slick.prototype.progressiveLazyLoad=function(tryCount){
tryCount=tryCount||1;
var _=this,
$imgsToLoad=$('img[data-lazy]', _.$slider),
image,
imageSource,
imageSrcSet,
imageSizes,
imageToLoad;
if($imgsToLoad.length){
image=$imgsToLoad.first();
imageSource=image.attr('data-lazy');
imageSrcSet=image.attr('data-srcset');
imageSizes=image.attr('data-sizes')||_.$slider.attr('data-sizes');
imageToLoad=document.createElement('img');
imageToLoad.onload=function(){
if(imageSrcSet){
image
.attr('srcset', imageSrcSet);
if(imageSizes){
image
.attr('sizes', imageSizes);
}}
image
.attr('src', imageSource)
.removeAttr('data-lazy data-srcset data-sizes')
.removeClass('slick-loading');
if(_.options.adaptiveHeight===true){
_.setPosition();
}
_.$slider.trigger('lazyLoaded', [ _, image, imageSource ]);
_.progressiveLazyLoad();
};
imageToLoad.onerror=function(){
if(tryCount < 3){
setTimeout(function(){
_.progressiveLazyLoad(tryCount + 1);
}, 500);
}else{
image
.removeAttr('data-lazy')
.removeClass('slick-loading')
.addClass('slick-lazyload-error');
_.$slider.trigger('lazyLoadError', [ _, image, imageSource ]);
_.progressiveLazyLoad();
}};
imageToLoad.src=imageSource;
}else{
_.$slider.trigger('allImagesLoaded', [ _ ]);
}};
Slick.prototype.refresh=function(initializing){
var _=this, currentSlide, lastVisibleIndex;
lastVisibleIndex=_.slideCount - _.options.slidesToShow;
if(!_.options.infinite&&(_.currentSlide > lastVisibleIndex)){
_.currentSlide=lastVisibleIndex;
}
if(_.slideCount <=_.options.slidesToShow){
_.currentSlide=0;
}
currentSlide=_.currentSlide;
_.destroy(true);
$.extend(_, _.initials, { currentSlide: currentSlide });
_.init();
if(!initializing){
_.changeSlide({
data: {
message: 'index',
index: currentSlide
}}, false);
}};
Slick.prototype.registerBreakpoints=function(){
var _=this, breakpoint, currentBreakpoint, l,
responsiveSettings=_.options.responsive||null;
if($.type(responsiveSettings)==='array'&&responsiveSettings.length){
_.respondTo=_.options.respondTo||'window';
for(breakpoint in responsiveSettings){
l=_.breakpoints.length-1;
if(responsiveSettings.hasOwnProperty(breakpoint)){
currentBreakpoint=responsiveSettings[breakpoint].breakpoint;
while(l >=0){
if(_.breakpoints[l]&&_.breakpoints[l]===currentBreakpoint){
_.breakpoints.splice(l,1);
}
l--;
}
_.breakpoints.push(currentBreakpoint);
_.breakpointSettings[currentBreakpoint]=responsiveSettings[breakpoint].settings;
}}
_.breakpoints.sort(function(a, b){
return(_.options.mobileFirst) ? a-b:b-a;
});
}};
Slick.prototype.reinit=function(){
var _=this;
_.$slides =
_.$slideTrack
.children(_.options.slide)
.addClass('slick-slide');
_.slideCount=_.$slides.length;
if(_.currentSlide >=_.slideCount&&_.currentSlide!==0){
_.currentSlide=_.currentSlide - _.options.slidesToScroll;
}
if(_.slideCount <=_.options.slidesToShow){
_.currentSlide=0;
}
_.registerBreakpoints();
_.setProps();
_.setupInfinite();
_.buildArrows();
_.updateArrows();
_.initArrowEvents();
_.buildDots();
_.updateDots();
_.initDotEvents();
_.cleanUpSlideEvents();
_.initSlideEvents();
_.checkResponsive(false, true);
if(_.options.focusOnSelect===true){
$(_.$slideTrack).children().on('click.slick', _.selectHandler);
}
_.setSlideClasses(typeof _.currentSlide==='number' ? _.currentSlide:0);
_.setPosition();
_.focusHandler();
_.paused = !_.options.autoplay;
_.autoPlay();
_.$slider.trigger('reInit', [_]);
};
Slick.prototype.resize=function(){
var _=this;
if($(window).width()!==_.windowWidth){
clearTimeout(_.windowDelay);
_.windowDelay=window.setTimeout(function(){
_.windowWidth=$(window).width();
_.checkResponsive();
if(!_.unslicked){ _.setPosition(); }}, 50);
}};
Slick.prototype.removeSlide=Slick.prototype.slickRemove=function(index, removeBefore, removeAll){
var _=this;
if(typeof(index)==='boolean'){
removeBefore=index;
index=removeBefore===true ? 0:_.slideCount - 1;
}else{
index=removeBefore===true ? --index:index;
}
if(_.slideCount < 1||index < 0||index > _.slideCount - 1){
return false;
}
_.unload();
if(removeAll===true){
_.$slideTrack.children().remove();
}else{
_.$slideTrack.children(this.options.slide).eq(index).remove();
}
_.$slides=_.$slideTrack.children(this.options.slide);
_.$slideTrack.children(this.options.slide).detach();
_.$slideTrack.append(_.$slides);
_.$slidesCache=_.$slides;
_.reinit();
};
Slick.prototype.setCSS=function(position){
var _=this,
positionProps={},
x, y;
if(_.options.rtl===true){
position=-position;
}
x=_.positionProp=='left' ? Math.ceil(position) + 'px':'0px';
y=_.positionProp=='top' ? Math.ceil(position) + 'px':'0px';
positionProps[_.positionProp]=position;
if(_.transformsEnabled===false){
_.$slideTrack.css(positionProps);
}else{
positionProps={};
if(_.cssTransitions===false){
positionProps[_.animType]='translate(' + x + ', ' + y + ')';
_.$slideTrack.css(positionProps);
}else{
positionProps[_.animType]='translate3d(' + x + ', ' + y + ', 0px)';
_.$slideTrack.css(positionProps);
}}
};
Slick.prototype.setDimensions=function(){
var _=this;
if(_.options.vertical===false){
if(_.options.centerMode===true){
_.$list.css({
padding: ('0px ' + _.options.centerPadding)
});
}}else{
_.$list.height(_.$slides.first().outerHeight(true) * _.options.slidesToShow);
if(_.options.centerMode===true){
_.$list.css({
padding: (_.options.centerPadding + ' 0px')
});
}}
_.listWidth=_.$list.width();
_.listHeight=_.$list.height();
if(_.options.vertical===false&&_.options.variableWidth===false){
_.slideWidth=Math.ceil(_.listWidth / _.options.slidesToShow);
_.$slideTrack.width(Math.ceil((_.slideWidth * _.$slideTrack.children('.slick-slide').length)));
}else if(_.options.variableWidth===true){
_.$slideTrack.width(5000 * _.slideCount);
}else{
_.slideWidth=Math.ceil(_.listWidth);
_.$slideTrack.height(Math.ceil((_.$slides.first().outerHeight(true) * _.$slideTrack.children('.slick-slide').length)));
}
var offset=_.$slides.first().outerWidth(true) - _.$slides.first().width();
if(_.options.variableWidth===false) _.$slideTrack.children('.slick-slide').width(_.slideWidth - offset);
};
Slick.prototype.setFade=function(){
var _=this,
targetLeft;
_.$slides.each(function(index, element){
targetLeft=(_.slideWidth * index) * -1;
if(_.options.rtl===true){
$(element).css({
position: 'relative',
right: targetLeft,
top: 0,
zIndex: _.options.zIndex - 2,
opacity: 0
});
}else{
$(element).css({
position: 'relative',
left: targetLeft,
top: 0,
zIndex: _.options.zIndex - 2,
opacity: 0
});
}});
_.$slides.eq(_.currentSlide).css({
zIndex: _.options.zIndex - 1,
opacity: 1
});
};
Slick.prototype.setHeight=function(){
var _=this;
if(_.options.slidesToShow===1&&_.options.adaptiveHeight===true&&_.options.vertical===false){
var targetHeight=_.$slides.eq(_.currentSlide).outerHeight(true);
_.$list.css('height', targetHeight);
}};
Slick.prototype.setOption =
Slick.prototype.slickSetOption=function(){
var _=this, l, item, option, value, refresh=false, type;
if($.type(arguments[0])==='object'){
option=arguments[0];
refresh=arguments[1];
type='multiple';
}else if($.type(arguments[0])==='string'){
option=arguments[0];
value=arguments[1];
refresh=arguments[2];
if(arguments[0]==='responsive'&&$.type(arguments[1])==='array'){
type='responsive';
}else if(typeof arguments[1]!=='undefined'){
type='single';
}}
if(type==='single'){
_.options[option]=value;
}else if(type==='multiple'){
$.each(option , function(opt, val){
_.options[opt]=val;
});
}else if(type==='responsive'){
for(item in value){
if($.type(_.options.responsive)!=='array'){
_.options.responsive=[ value[item] ];
}else{
l=_.options.responsive.length-1;
while(l >=0){
if(_.options.responsive[l].breakpoint===value[item].breakpoint){
_.options.responsive.splice(l,1);
}
l--;
}
_.options.responsive.push(value[item]);
}}
}
if(refresh){
_.unload();
_.reinit();
}};
Slick.prototype.setPosition=function(){
var _=this;
_.setDimensions();
_.setHeight();
if(_.options.fade===false){
_.setCSS(_.getLeft(_.currentSlide));
}else{
_.setFade();
}
_.$slider.trigger('setPosition', [_]);
};
Slick.prototype.setProps=function(){
var _=this,
bodyStyle=document.body.style;
_.positionProp=_.options.vertical===true ? 'top':'left';
if(_.positionProp==='top'){
_.$slider.addClass('slick-vertical');
}else{
_.$slider.removeClass('slick-vertical');
}
if(bodyStyle.WebkitTransition!==undefined ||
bodyStyle.MozTransition!==undefined ||
bodyStyle.msTransition!==undefined){
if(_.options.useCSS===true){
_.cssTransitions=true;
}}
if(_.options.fade){
if(typeof _.options.zIndex==='number'){
if(_.options.zIndex < 3){
_.options.zIndex=3;
}}else{
_.options.zIndex=_.defaults.zIndex;
}}
if(bodyStyle.OTransform!==undefined){
_.animType='OTransform';
_.transformType='-o-transform';
_.transitionType='OTransition';
if(bodyStyle.perspectiveProperty===undefined&&bodyStyle.webkitPerspective===undefined) _.animType=false;
}
if(bodyStyle.MozTransform!==undefined){
_.animType='MozTransform';
_.transformType='-moz-transform';
_.transitionType='MozTransition';
if(bodyStyle.perspectiveProperty===undefined&&bodyStyle.MozPerspective===undefined) _.animType=false;
}
if(bodyStyle.webkitTransform!==undefined){
_.animType='webkitTransform';
_.transformType='-webkit-transform';
_.transitionType='webkitTransition';
if(bodyStyle.perspectiveProperty===undefined&&bodyStyle.webkitPerspective===undefined) _.animType=false;
}
if(bodyStyle.msTransform!==undefined){
_.animType='msTransform';
_.transformType='-ms-transform';
_.transitionType='msTransition';
if(bodyStyle.msTransform===undefined) _.animType=false;
}
if(bodyStyle.transform!==undefined&&_.animType!==false){
_.animType='transform';
_.transformType='transform';
_.transitionType='transition';
}
_.transformsEnabled=_.options.useTransform&&(_.animType!==null&&_.animType!==false);
};
Slick.prototype.setSlideClasses=function(index){
var _=this,
centerOffset, allSlides, indexOffset, remainder;
allSlides=_.$slider
.find('.slick-slide')
.removeClass('slick-active slick-center slick-current')
.attr('aria-hidden', 'true');
_.$slides
.eq(index)
.addClass('slick-current');
if(_.options.centerMode===true){
var evenCoef=_.options.slidesToShow % 2===0 ? 1:0;
centerOffset=Math.floor(_.options.slidesToShow / 2);
if(_.options.infinite===true){
if(index >=centerOffset&&index <=(_.slideCount - 1) - centerOffset){
_.$slides
.slice(index - centerOffset + evenCoef, index + centerOffset + 1)
.addClass('slick-active')
.attr('aria-hidden', 'false');
}else{
indexOffset=_.options.slidesToShow + index;
allSlides
.slice(indexOffset - centerOffset + 1 + evenCoef, indexOffset + centerOffset + 2)
.addClass('slick-active')
.attr('aria-hidden', 'false');
}
if(index===0){
allSlides
.eq(allSlides.length - 1 - _.options.slidesToShow)
.addClass('slick-center');
}else if(index===_.slideCount - 1){
allSlides
.eq(_.options.slidesToShow)
.addClass('slick-center');
}}
_.$slides
.eq(index)
.addClass('slick-center');
}else{
if(index >=0&&index <=(_.slideCount - _.options.slidesToShow)){
_.$slides
.slice(index, index + _.options.slidesToShow)
.addClass('slick-active')
.attr('aria-hidden', 'false');
}else if(allSlides.length <=_.options.slidesToShow){
allSlides
.addClass('slick-active')
.attr('aria-hidden', 'false');
}else{
remainder=_.slideCount % _.options.slidesToShow;
indexOffset=_.options.infinite===true ? _.options.slidesToShow + index:index;
if(_.options.slidesToShow==_.options.slidesToScroll&&(_.slideCount - index) < _.options.slidesToShow){
allSlides
.slice(indexOffset - (_.options.slidesToShow - remainder), indexOffset + remainder)
.addClass('slick-active')
.attr('aria-hidden', 'false');
}else{
allSlides
.slice(indexOffset, indexOffset + _.options.slidesToShow)
.addClass('slick-active')
.attr('aria-hidden', 'false');
}}
}
if(_.options.lazyLoad==='ondemand'||_.options.lazyLoad==='anticipated'){
_.lazyLoad();
}};
Slick.prototype.setupInfinite=function(){
var _=this,
i, slideIndex, infiniteCount;
if(_.options.fade===true){
_.options.centerMode=false;
}
if(_.options.infinite===true&&_.options.fade===false){
slideIndex=null;
if(_.slideCount > _.options.slidesToShow){
if(_.options.centerMode===true){
infiniteCount=_.options.slidesToShow + 1;
}else{
infiniteCount=_.options.slidesToShow;
}
for (i=_.slideCount; i > (_.slideCount -
infiniteCount); i -=1){
slideIndex=i - 1;
$(_.$slides[slideIndex]).clone(true).attr('id', '')
.attr('data-slick-index', slideIndex - _.slideCount)
.prependTo(_.$slideTrack).addClass('slick-cloned');
}
for (i=0; i < infiniteCount  + _.slideCount; i +=1){
slideIndex=i;
$(_.$slides[slideIndex]).clone(true).attr('id', '')
.attr('data-slick-index', slideIndex + _.slideCount)
.appendTo(_.$slideTrack).addClass('slick-cloned');
}
_.$slideTrack.find('.slick-cloned').find('[id]').each(function(){
$(this).attr('id', '');
});
}}
};
Slick.prototype.interrupt=function(toggle){
var _=this;
if(!toggle){
_.autoPlay();
}
_.interrupted=toggle;
};
Slick.prototype.selectHandler=function(event){
var _=this;
var targetElement =
$(event.target).is('.slick-slide') ?
$(event.target) :
$(event.target).parents('.slick-slide');
var index=parseInt(targetElement.attr('data-slick-index'));
if(!index) index=0;
if(_.slideCount <=_.options.slidesToShow){
_.slideHandler(index, false, true);
return;
}
_.slideHandler(index);
};
Slick.prototype.slideHandler=function(index, sync, dontAnimate){
var targetSlide, animSlide, oldSlide, slideLeft, targetLeft=null,
_=this, navTarget;
sync=sync||false;
if(_.animating===true&&_.options.waitForAnimate===true){
return;
}
if(_.options.fade===true&&_.currentSlide===index){
return;
}
if(sync===false){
_.asNavFor(index);
}
targetSlide=index;
targetLeft=_.getLeft(targetSlide);
slideLeft=_.getLeft(_.currentSlide);
_.currentLeft=_.swipeLeft===null ? slideLeft:_.swipeLeft;
if(_.options.infinite===false&&_.options.centerMode===false&&(index < 0||index > _.getDotCount() * _.options.slidesToScroll)){
if(_.options.fade===false){
targetSlide=_.currentSlide;
if(dontAnimate!==true&&_.slideCount > _.options.slidesToShow){
_.animateSlide(slideLeft, function(){
_.postSlide(targetSlide);
});
}else{
_.postSlide(targetSlide);
}}
return;
}else if(_.options.infinite===false&&_.options.centerMode===true&&(index < 0||index > (_.slideCount - _.options.slidesToScroll))){
if(_.options.fade===false){
targetSlide=_.currentSlide;
if(dontAnimate!==true&&_.slideCount > _.options.slidesToShow){
_.animateSlide(slideLeft, function(){
_.postSlide(targetSlide);
});
}else{
_.postSlide(targetSlide);
}}
return;
}
if(_.options.autoplay){
clearInterval(_.autoPlayTimer);
}
if(targetSlide < 0){
if(_.slideCount % _.options.slidesToScroll!==0){
animSlide=_.slideCount - (_.slideCount % _.options.slidesToScroll);
}else{
animSlide=_.slideCount + targetSlide;
}}else if(targetSlide >=_.slideCount){
if(_.slideCount % _.options.slidesToScroll!==0){
animSlide=0;
}else{
animSlide=targetSlide - _.slideCount;
}}else{
animSlide=targetSlide;
}
_.animating=true;
_.$slider.trigger('beforeChange', [_, _.currentSlide, animSlide]);
oldSlide=_.currentSlide;
_.currentSlide=animSlide;
_.setSlideClasses(_.currentSlide);
if(_.options.asNavFor){
navTarget=_.getNavTarget();
navTarget=navTarget.slick('getSlick');
if(navTarget.slideCount <=navTarget.options.slidesToShow){
navTarget.setSlideClasses(_.currentSlide);
}}
_.updateDots();
_.updateArrows();
if(_.options.fade===true){
if(dontAnimate!==true){
_.fadeSlideOut(oldSlide);
_.fadeSlide(animSlide, function(){
_.postSlide(animSlide);
});
}else{
_.postSlide(animSlide);
}
_.animateHeight();
return;
}
if(dontAnimate!==true&&_.slideCount > _.options.slidesToShow){
_.animateSlide(targetLeft, function(){
_.postSlide(animSlide);
});
}else{
_.postSlide(animSlide);
}};
Slick.prototype.startLoad=function(){
var _=this;
if(_.options.arrows===true&&_.slideCount > _.options.slidesToShow){
_.$prevArrow.hide();
_.$nextArrow.hide();
}
if(_.options.dots===true&&_.slideCount > _.options.slidesToShow){
_.$dots.hide();
}
_.$slider.addClass('slick-loading');
};
Slick.prototype.swipeDirection=function(){
var xDist, yDist, r, swipeAngle, _=this;
xDist=_.touchObject.startX - _.touchObject.curX;
yDist=_.touchObject.startY - _.touchObject.curY;
r=Math.atan2(yDist, xDist);
swipeAngle=Math.round(r * 180 / Math.PI);
if(swipeAngle < 0){
swipeAngle=360 - Math.abs(swipeAngle);
}
if((swipeAngle <=45)&&(swipeAngle >=0)){
return (_.options.rtl===false ? 'left':'right');
}
if((swipeAngle <=360)&&(swipeAngle >=315)){
return (_.options.rtl===false ? 'left':'right');
}
if((swipeAngle >=135)&&(swipeAngle <=225)){
return (_.options.rtl===false ? 'right':'left');
}
if(_.options.verticalSwiping===true){
if((swipeAngle >=35)&&(swipeAngle <=135)){
return 'down';
}else{
return 'up';
}}
return 'vertical';
};
Slick.prototype.swipeEnd=function(event){
var _=this,
slideCount,
direction;
_.dragging=false;
_.swiping=false;
if(_.scrolling){
_.scrolling=false;
return false;
}
_.interrupted=false;
_.shouldClick=(_.touchObject.swipeLength > 10) ? false:true;
if(_.touchObject.curX===undefined){
return false;
}
if(_.touchObject.edgeHit===true){
_.$slider.trigger('edge', [_, _.swipeDirection() ]);
}
if(_.touchObject.swipeLength >=_.touchObject.minSwipe){
direction=_.swipeDirection();
switch(direction){
case 'left':
case 'down':
slideCount =
_.options.swipeToSlide ?
_.checkNavigable(_.currentSlide + _.getSlideCount()) :
_.currentSlide + _.getSlideCount();
_.currentDirection=0;
break;
case 'right':
case 'up':
slideCount =
_.options.swipeToSlide ?
_.checkNavigable(_.currentSlide - _.getSlideCount()) :
_.currentSlide - _.getSlideCount();
_.currentDirection=1;
break;
default:
}
if(direction!='vertical'){
_.slideHandler(slideCount);
_.touchObject={};
_.$slider.trigger('swipe', [_, direction ]);
}}else{
if(_.touchObject.startX!==_.touchObject.curX){
_.slideHandler(_.currentSlide);
_.touchObject={};}}
};
Slick.prototype.swipeHandler=function(event){
var _=this;
if((_.options.swipe===false)||('ontouchend' in document&&_.options.swipe===false)){
return;
}else if(_.options.draggable===false&&event.type.indexOf('mouse')!==-1){
return;
}
_.touchObject.fingerCount=event.originalEvent&&event.originalEvent.touches!==undefined ?
event.originalEvent.touches.length:1;
_.touchObject.minSwipe=_.listWidth / _.options
.touchThreshold;
if(_.options.verticalSwiping===true){
_.touchObject.minSwipe=_.listHeight / _.options
.touchThreshold;
}
switch (event.data.action){
case 'start':
_.swipeStart(event);
break;
case 'move':
_.swipeMove(event);
break;
case 'end':
_.swipeEnd(event);
break;
}};
Slick.prototype.swipeMove=function(event){
var _=this,
edgeWasHit=false,
curLeft, swipeDirection, swipeLength, positionOffset, touches, verticalSwipeLength;
touches=event.originalEvent!==undefined ? event.originalEvent.touches:null;
if(!_.dragging||_.scrolling||touches&&touches.length!==1){
return false;
}
curLeft=_.getLeft(_.currentSlide);
_.touchObject.curX=touches!==undefined ? touches[0].pageX:event.clientX;
_.touchObject.curY=touches!==undefined ? touches[0].pageY:event.clientY;
_.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(_.touchObject.curX - _.touchObject.startX, 2)));
verticalSwipeLength=Math.round(Math.sqrt(Math.pow(_.touchObject.curY - _.touchObject.startY, 2)));
if(!_.options.verticalSwiping&&!_.swiping&&verticalSwipeLength > 4){
_.scrolling=true;
return false;
}
if(_.options.verticalSwiping===true){
_.touchObject.swipeLength=verticalSwipeLength;
}
swipeDirection=_.swipeDirection();
if(event.originalEvent!==undefined&&_.touchObject.swipeLength > 4){
_.swiping=true;
event.preventDefault();
}
positionOffset=(_.options.rtl===false ? 1:-1) * (_.touchObject.curX > _.touchObject.startX ? 1:-1);
if(_.options.verticalSwiping===true){
positionOffset=_.touchObject.curY > _.touchObject.startY ? 1:-1;
}
swipeLength=_.touchObject.swipeLength;
_.touchObject.edgeHit=false;
if(_.options.infinite===false){
if((_.currentSlide===0&&swipeDirection==='right')||(_.currentSlide >=_.getDotCount()&&swipeDirection==='left')){
swipeLength=_.touchObject.swipeLength * _.options.edgeFriction;
_.touchObject.edgeHit=true;
}}
if(_.options.vertical===false){
_.swipeLeft=curLeft + swipeLength * positionOffset;
}else{
_.swipeLeft=curLeft + (swipeLength * (_.$list.height() / _.listWidth)) * positionOffset;
}
if(_.options.verticalSwiping===true){
_.swipeLeft=curLeft + swipeLength * positionOffset;
}
if(_.options.fade===true||_.options.touchMove===false){
return false;
}
if(_.animating===true){
_.swipeLeft=null;
return false;
}
_.setCSS(_.swipeLeft);
};
Slick.prototype.swipeStart=function(event){
var _=this,
touches;
_.interrupted=true;
if(_.touchObject.fingerCount!==1||_.slideCount <=_.options.slidesToShow){
_.touchObject={};
return false;
}
if(event.originalEvent!==undefined&&event.originalEvent.touches!==undefined){
touches=event.originalEvent.touches[0];
}
_.touchObject.startX=_.touchObject.curX=touches!==undefined ? touches.pageX:event.clientX;
_.touchObject.startY=_.touchObject.curY=touches!==undefined ? touches.pageY:event.clientY;
_.dragging=true;
};
Slick.prototype.unfilterSlides=Slick.prototype.slickUnfilter=function(){
var _=this;
if(_.$slidesCache!==null){
_.unload();
_.$slideTrack.children(this.options.slide).detach();
_.$slidesCache.appendTo(_.$slideTrack);
_.reinit();
}};
Slick.prototype.unload=function(){
var _=this;
$('.slick-cloned', _.$slider).remove();
if(_.$dots){
_.$dots.remove();
}
if(_.$prevArrow&&_.htmlExpr.test(_.options.prevArrow)){
_.$prevArrow.remove();
}
if(_.$nextArrow&&_.htmlExpr.test(_.options.nextArrow)){
_.$nextArrow.remove();
}
_.$slides
.removeClass('slick-slide slick-active slick-visible slick-current')
.attr('aria-hidden', 'true')
.css('width', '');
};
Slick.prototype.unslick=function(fromBreakpoint){
var _=this;
_.$slider.trigger('unslick', [_, fromBreakpoint]);
_.destroy();
};
Slick.prototype.updateArrows=function(){
var _=this,
centerOffset;
centerOffset=Math.floor(_.options.slidesToShow / 2);
if(_.options.arrows===true &&
_.slideCount > _.options.slidesToShow &&
!_.options.infinite){
_.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
_.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
if(_.currentSlide===0){
_.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
_.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
}else if(_.currentSlide >=_.slideCount - _.options.slidesToShow&&_.options.centerMode===false){
_.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
_.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
}else if(_.currentSlide >=_.slideCount - 1&&_.options.centerMode===true){
_.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
_.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
}}
};
Slick.prototype.updateDots=function(){
var _=this;
if(_.$dots!==null){
_.$dots
.find('li')
.removeClass('slick-active')
.end();
_.$dots
.find('li')
.eq(Math.floor(_.currentSlide / _.options.slidesToScroll))
.addClass('slick-active');
}};
Slick.prototype.visibility=function(){
var _=this;
if(_.options.autoplay){
if(document[_.hidden]){
_.interrupted=true;
}else{
_.interrupted=false;
}}
};
$.fn.slick=function(){
var _=this,
opt=arguments[0],
args=Array.prototype.slice.call(arguments, 1),
l=_.length,
i,
ret;
for (i=0; i < l; i++){
if(typeof opt=='object'||typeof opt=='undefined')
_[i].slick=new Slick(_[i], opt);
else
ret=_[i].slick[opt].apply(_[i].slick, args);
if(typeof ret!='undefined') return ret;
}
return _;
};}));
},{"jquery":1}],4:[function(require,module,exports){
"use strict";
var _jquery=_interopRequireDefault(require("jquery"));
function _interopRequireDefault(obj){ return obj&&obj.__esModule ? obj:{ default: obj };}
window.jQuery=_jquery.default;
window.$=_jquery.default;
},{"jquery":1}],5:[function(require,module,exports){
"use strict";
require("./_jquery");
var ko=_interopRequireWildcard(require("knockout"));
require("slick-carousel");
function _interopRequireWildcard(obj){ if(obj&&obj.__esModule){ return obj; }else{ var newObj={}; if(obj!=null){ for (var key in obj){ if(Object.prototype.hasOwnProperty.call(obj, key)){ var desc=Object.defineProperty&&Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key):{}; if(desc.get||desc.set){ Object.defineProperty(newObj, key, desc); }else{ newObj[key]=obj[key]; }} }} newObj.default=obj; return newObj; }}
function _classCallCheck(instance, Constructor){ if(!(instance instanceof Constructor)){ throw new TypeError("Cannot call a class as a function"); }}
function _defineProperties(target, props){ for (var i=0; i < props.length; i++){ var descriptor=props[i]; descriptor.enumerable=descriptor.enumerable||false; descriptor.configurable=true; if("value" in descriptor) descriptor.writable=true; Object.defineProperty(target, descriptor.key, descriptor); }}
function _createClass(Constructor, protoProps, staticProps){ if(protoProps) _defineProperties(Constructor.prototype, protoProps); if(staticProps) _defineProperties(Constructor, staticProps); return Constructor; }
var App =
function (){
function App(){
var _this=this;
_classCallCheck(this, App);
this.scrolledTop=ko.observable(0);
this.menuOpen=ko.observable(false);
this.membershiptype=ko.observable('monthly');
this.loading=ko.observable(false);
this.firstname=ko.observable("");
this.lastname=ko.observable("");
this.email=ko.observable("");
this.phone=ko.observable("");
this.jobtitle=ko.observable("");
this.company=ko.observable("");
this.message=ko.observable("");
this.toc=ko.observable(false);
this.membership=ko.observable("");
this.formSubmitted=ko.observable(false);
this.formError=ko.observable(false);
this.formErrorMessage=ko.observable("");
$(window).on('scroll', function (){
return _this.scrolledTop($(window).scrollTop());
});
this.scrolledTop($(window).scrollTop());
$(".js-membership-elite .elementor-button-text").on("click", function (){
return _this.membership("membership-elite");
});
$(".js-membership-pro .elementor-button-text").on("click", function (){
return _this.membership("membership-pro");
});
$(".js-membership-starter .elementor-button-text").on("click", function (){
return _this.membership("membership-starter");
});
$(window).on('submit', function (){
_this.loading(true);
});
$(window).on('wpcf7mailsent', function (){
$('.form__wrapper').remove();
});
$(window).on('wpcf7invalid', function (){
_this.loading(false);
});
this.formSubmitted.subscribe(function (){
if($('.modal.fade.in')){
$('.modal .hide-on-form-submit').hide();
$('.modal .show-on-form-submit').show();
}else{
$('.hide-on-form-submit').hide();
$('.show-on-form-submit').show();
}});
}
_createClass(App, [{
key: "setLoading",
value: function setLoading(){
this.loading(true);
}}, {
key: "toggleMenu",
value: function toggleMenu(){
this.menuOpen(!this.menuOpen());
}}, {
key: "submit",
value: function submit(url, listid){
var _this2=this;
this.loading(true);
var info={
listid: listid,
firstname: this.firstname(),
lastname: this.lastname(),
email: this.email(),
phone: this.phone(),
jobtitle: this.jobtitle(),
company: this.company()
};
if(listid==2){
info['tags']=this.membership() + "," + this.membershiptype();
}
form(url, info).then(function (){
_this2.formSubmitted(true);
}).catch(function (response){
_this2.formErrorMessage(response);
_this2.formError(true);
}).finally(function (){
_this2.loading(false);
});
}}]);
return App;
}();
var form=function form(url, info){
return new Promise(function (resolve, reject){
var newXHR=new XMLHttpRequest();
newXHR.addEventListener('load', function (e){
if(e.currentTarget.response==="SUCCESS"){
resolve();
}else{
reject(e.currentTarget.response);
}});
newXHR.open('POST', url);
var data=new FormData();
Object.keys(info).forEach(function (obj){
data.append(obj, info[obj]);
});
newXHR.send(data);
});
};
ko.applyBindings(new App());
},{"./_jquery":4,"knockout":2,"slick-carousel":3}]},{},[5]);
var addComment={moveForm:function(a,b,c,d){var e,f,g,h,i=this,j=i.I(a),k=i.I(c),l=i.I("cancel-comment-reply-link"),m=i.I("comment_parent"),n=i.I("comment_post_ID"),o=k.getElementsByTagName("form")[0];if(j&&k&&l&&m&&o){i.respondId=c,d=d||!1,i.I("wp-temp-form-div")||(e=document.createElement("div"),e.id="wp-temp-form-div",e.style.display="none",k.parentNode.insertBefore(e,k)),j.parentNode.insertBefore(k,j.nextSibling),n&&d&&(n.value=d),m.value=b,l.style.display="",l.onclick=function(){var a=addComment,b=a.I("wp-temp-form-div"),c=a.I(a.respondId);if(b&&c)return a.I("comment_parent").value="0",b.parentNode.insertBefore(c,b),b.parentNode.removeChild(b),this.style.display="none",this.onclick=null,!1};try{for(var p=0;p<o.elements.length;p++)if(f=o.elements[p],h=!1,"getComputedStyle"in window?g=window.getComputedStyle(f):document.documentElement.currentStyle&&(g=f.currentStyle),(f.offsetWidth<=0&&f.offsetHeight<=0||"hidden"===g.visibility)&&(h=!0),"hidden"!==f.type&&!f.disabled&&!h){f.focus();break}}catch(q){}return!1}},I:function(a){return document.getElementById(a)}};
jQuery(document).ready(function ($){
$('.zoom-social_icons-list__link').on({
'mouseenter': function (e){
e.preventDefault();
var $this=$(this).find('.zoom-social_icons-list-span');
var $rule=$this.data('hover-rule');
var $color=$this.data('hover-color');
if($color!==undefined){
$this.attr('data-old-color', $this.css($rule));
$this.css($rule, $color);
}},
'mouseleave': function (e){
e.preventDefault();
var $this=$(this).find('.zoom-social_icons-list-span');
var $rule=$this.data('hover-rule');
var $oldColor=$this.data('old-color');
if($oldColor!==undefined){
$this.css($rule, $oldColor);
}}
});
});
!function(a,b){"use strict";function c(){if(!e){e=!0;var a,c,d,f,g=-1!==navigator.appVersion.indexOf("MSIE 10"),h=!!navigator.userAgent.match(/Trident.*rv:11\./),i=b.querySelectorAll("iframe.wp-embedded-content");for(c=0;c<i.length;c++){if(d=i[c],!d.getAttribute("data-secret"))f=Math.random().toString(36).substr(2,10),d.src+="#?secret="+f,d.setAttribute("data-secret",f);if(g||h)a=d.cloneNode(!0),a.removeAttribute("security"),d.parentNode.replaceChild(a,d)}}}var d=!1,e=!1;if(b.querySelector)if(a.addEventListener)d=!0;if(a.wp=a.wp||{},!a.wp.receiveEmbedMessage)if(a.wp.receiveEmbedMessage=function(c){var d=c.data;if(d)if(d.secret||d.message||d.value)if(!/[^a-zA-Z0-9]/.test(d.secret)){var e,f,g,h,i,j=b.querySelectorAll('iframe[data-secret="'+d.secret+'"]'),k=b.querySelectorAll('blockquote[data-secret="'+d.secret+'"]');for(e=0;e<k.length;e++)k[e].style.display="none";for(e=0;e<j.length;e++)if(f=j[e],c.source===f.contentWindow){if(f.removeAttribute("style"),"height"===d.message){if(g=parseInt(d.value,10),g>1e3)g=1e3;else if(~~g<200)g=200;f.height=g}if("link"===d.message)if(h=b.createElement("a"),i=b.createElement("a"),h.href=f.getAttribute("src"),i.href=d.value,i.host===h.host)if(b.activeElement===f)a.top.location.href=d.value}else;}},d)a.addEventListener("message",a.wp.receiveEmbedMessage,!1),b.addEventListener("DOMContentLoaded",c,!1),a.addEventListener("load",c,!1)}(window,document);