<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cvx');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'p  G=)l7 tDrJZMI6qN=(T,pd);i]3Bm!WgGvhxRGavX`p_Fgr`5EXI@GQmKnO+7');
define('SECURE_AUTH_KEY',  'x>Tyq:(0V:({YxnGdt1}0B2Kz.c*Y~D_dcV=Gh[ZC/gVQAo;6q1fyiJH?.m#=?HL');
define('LOGGED_IN_KEY',    ':R2#mu8!YV yg)IN,Fp ^(/X DV>C*?5UBjXNVndpB{K$Va>-JIY>ca r{;p5T1#');
define('NONCE_KEY',        'PD#uH<B[[cFF?WZ4RR#>fEj9WPgpThesw,_XeZ3F>_1p>/D<V*.F8Ybf/I7J28FS');
define('AUTH_SALT',        'ZoVejyi#4GP#Nw=_))7;8*T}(&=e0n+>#Lr GP)H,tw:o?WasJ(n%XXcI@?=LZ3e');
define('SECURE_AUTH_SALT', 'F:@g?z~lpLiv^Krrk~ch45b_:l {*)pP7E1I/Zv+>>Ap#P(-2K!k/3S]p%dx41iQ');
define('LOGGED_IN_SALT',   'c%Q$/_StM`OyG|Qm[2F_%B9Zd_5-S(24+4G)vw~kNt%;{V Q>zcD;B c[S&V[Y<U');
define('NONCE_SALT',       'Pd}?lb;|O_S#c#<I{|/wC4fTNwJ&T`%zg&?8dz~TlC<3 42]mjeR]RgWh7~hr#cF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cvx_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');